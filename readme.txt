To run these tests, you must have the following:
    Python3
    pip
    pipenv (installed using pip)

Running the project for the first time, enter the directory containing the Pipfile with a command prompt, and run "pipenv sync".

To then run these tests, run "pipenv run python runtests.py".
Add "--live" to use live server licenses - when testing a release.
Add "--suite=" with a folder name in the tests dir to only test that folder. ie. pipenv run python runtests.py --suite=licenses

