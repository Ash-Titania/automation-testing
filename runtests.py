import unittest
import sys
import junit_xml
import os
import re


def test_case_list_to_junit_xml_test_case_list(input_list, add_info_func):
    """Given a python unittest.TestCase input list, converts to a junit_xml.TestCase list
       :param input_list - the list of python unittest.TestCase objects to convert.
       :param add_info_func - one of two junit_xml.TestCase member functions to call to set Error or failure info,
       required because as of 1.8, junit_xml.TestCase ctor() does not have support for distinguishing between
       erroneous and failure TestCases
    """
    return_list = []
    for item in input_list:
        junit_test_case = junit_xml.TestCase(item[0].id(), type(item[0]), 0, '', '')
        msg_list = re.findall(R"^[^:\s]+:\s.+$", item[1], flags=re.MULTILINE)
        msg = ''
        if not len(msg_list) == 0:
            msg = msg_list[0]

        add_info_func(junit_test_case, msg, item[1])
        return_list.append(junit_test_case)

    return return_list


def get_test_loader_directory():
    suite_cmd = '--suite='
    loader_dir = 'tests'
    for arg in sys.argv:
        if suite_cmd in arg:
            loader_dir += f".{arg.replace(suite_cmd, '')}"

    return loader_dir


def run_tests():
    """
    first finds files inside of install directory with format '*install.py'
    and executes any tests found, if none of these fail
    then find files inside of settings directory, with the format of '*test.py',
    and execute any unit tests found in them.
    """
    loader = unittest.TestLoader()
    runner = unittest.TextTestRunner(verbosity=2)

    if runner.run(loader.discover("tests.install", pattern="*install.py")).wasSuccessful():
        test_result = runner.run(loader.discover(get_test_loader_directory(), pattern="*test.py"))

        junit_fails = test_case_list_to_junit_xml_test_case_list(test_result.failures,
                                                                 junit_xml.TestCase.add_failure_info)
        junit_errors = test_case_list_to_junit_xml_test_case_list(test_result.errors, junit_xml.TestCase.add_error_info)
        junit_skipped = test_case_list_to_junit_xml_test_case_list(test_result.skipped,
                                                                   junit_xml.TestCase.add_skipped_info)
        pass_count = test_result.testsRun - len(junit_fails) - len(junit_errors) - len(junit_skipped)
        junit_passes = []

        for i in range(pass_count):
            junit_passes.append(junit_xml.TestCase('Pass'))
        jts = junit_xml.TestSuite("junit test suite", junit_fails + junit_errors + junit_skipped + junit_passes)
        out_dir = os.path.join(os.getcwd(), "test-reports")
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        junit_file = os.path.join(out_dir, 'junit_output.xml')

        if os.path.exists(junit_file):
            os.remove(junit_file)

        with open(junit_file, mode="w") as file_handle:
            junit_xml.TestSuite.to_file(file_handle, [jts])

        return (len(junit_fails) + len(junit_errors))


if __name__ == "__main__":
    sys.exit(run_tests())
