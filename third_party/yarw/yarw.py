"""
YARW (Yet Another Registry Wrapper).
Credit: code.activestate.com

Provides functionality to recursively delete registry keys,
not currently supported by python's builtin winreg module.

Also provides easy iteration of registry keys, values and items(key, value pairs).

Example usage:
To recursively delete each key and subkey from HKCU\Software\Titania down:

yarw.Registry().recursive_delete("HKEY_CURRENT_USER", "Software\\Titania")

To Set the value of a key at HIVE\PATH\KEY to VAL:

yarw.Registry().set_value(HIVE, PATH, KEY, VAL)

To remove a KEY/VAL pair from HIVE\PATH\KEY:

yarw.Registry().delete_value(HIVE, PATH, KEY)
"""
import winreg


class RegistryError(Exception):
    """ An exception class for Registry """
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return str(self.value)


class Registry:
    """ A class which abstracts away operations on the Windows
    registry. This class provides a way to work with the Windows
    registry without using winreg directly.

    A Windows registry key is represented by the internal
    class named RegistryKey. A number of operations of this
    class return and work with instances of RegistryKey.
    """

    def __init__(self):
        pass

    def map_key(key):
        """ Map key names to Windows registry key constants """
        if type(key) is not str:
            raise RegistryError("Key must be a string object.")
        else:
            try:
                return eval('winreg.' + key)
            except AttributeError:
                return None

    class RegistryKey:
        """ An internal class of Registry which abstracts a Windows
        registry key """
        def __init__(self):
            # The actual PyHKEY object returned by the Python winreg module
            self.hkey = None
            # The name of the key
            self.key_name = ''

        def open(self, key, sub_key, access=winreg.KEY_READ):
            """ Opens a Windows registry key. The first param is one of the
            Windows HKEY names or an already open key. The second param
            is the actual key to open. """
            if type(key) is str:
                hkey = Registry.map_key(key)
            else:
                hkey = key

            if not hkey:
                raise RegistryError(f"Could not find registry key for {key}")

            try:
                self.hkey = winreg.OpenKey(hkey, sub_key, 0, access)
                self.key_name = sub_key
            except EnvironmentError as e:
                raise RegistryError(e)

        def create(self, key, sub_key):
            """ Creates or opens a Windows registry key. The first param
            is one of the Windows HKEY names or an already open key. The
            second param is the actual key to create/open. """
            if type(key) is str:
                hkey = Registry.map_key(key)
            else:
                hkey = key

            if not hkey:
                raise RegistryError(f"Could not find registry key for {key}")

            try:
                self.hkey = winreg.CreateKey(hkey, sub_key)
                self.key_name = sub_key
            except EnvironmentError as e:
                raise RegistryError(e)

        def open_key(self, index):
            """ Open the sub-key at the given index and return
            the created RegistryKey object """
            # NOTE: The index is starting from 1,not zero!
            sub_key = self.enum_key(index - 1)
            if sub_key:
                r = Registry.RegistryKey()
                r.set_key(winreg.OpenKey(self.hkey, sub_key))
                r.set_key_name(sub_key)
                return r
            else:
                raise RegistryError(f"No sub-key found at index {index}")

        def enum_key(self, index):
            """ Enumerate the subkeys of the currently open key """
            if not self.hkey:
                raise RegistryError("Error: null key")

            try:
                return winreg.EnumKey(self.hkey, index)
            except EnvironmentError as e:
                raise RegistryError(e)

        def enum_value(self, index):
            """ Enumerate the values of the currently open key """
            if not self.hkey:
                raise RegistryError("Error: null key")
            try:
                return winreg.EnumValue(self.hkey, index)
            except EnvironmentError as e:
                raise RegistryError(e)

        def keys(self):
            """ Return the subkeys of the current key as a list """
            # This method works just like the 'keys' method
            # of a dictionary object.
            key_list = []
            index = 0
            while True:
                try:
                    sub_key = self.enum_key(index)
                    key_list.append(sub_key)
                    index += 1
                except RegistryError:
                    break

            return key_list

        def values(self):
            """ Return the subvalues of the current key as a list """
            # This method works just like the 'values' method
            # of a dictionary object.
            value_list = []
            index = 0
            while True:
                try:
                    value = self.enum_value(index)
                    value_list.append(value)
                    index += 1
                except RegistryError:
                    break

            return value_list

        def items(self):
            """ Return a list of (key,value) pairs for each subkey in the
            current key """
            # This method works just like the 'items' method
            # of a dictionary object.
            items = []
            index = 0
            while True:
                try:
                    key = self.enum_key(index)
                    value = self.enum_value(index)
                    items.append((key, value))
                    index += 1
                except RegistryError:
                    break

            return items

        def iter_keys(self):
            """ Return an iterator over the list of subkeys of the current key """
            # Note: this is a generator
            # This method works just like the 'iterkeys' method
            # of a dictionary object.
            index = 0
            while True:
                try:
                    yield self.enum_key(index)
                    index += 1
                except RegistryError:
                    break

        def iter_values(self):
            """ Return an iterator over the list of subvalues of the current key """
            # Note: this is a generator
            # This method works just like the 'itervalues' method
            # of a dictionary object.
            index = 0
            while True:
                try:
                    yield self.enum_value(index)
                    index += 1
                except RegistryError:
                    break

        def iter_items(self):
            """ Return an iterator over the (subkey,subvalue) pairs of the current key"""
            # Note: this is a generator
            # This method works just like the 'iteritems' method
            # of a dictionary object.
            index = 0
            while True:
                try:
                    yield (self.enum_key(index), self.enum_value(index))
                    index += 1
                except RegistryError:
                    break

        def get_value(self, name=''):
            """ Return the value of an item inside the current key,
            given its name """
            try:
                if name:
                    return winreg.QueryValueEx(self.hkey, name)
                else:
                    return winreg.QueryValue(self.hkey, '')
            except WindowsError as we:
                raise RegistryError(we)
            except EnvironmentError as ee:
                raise RegistryError(ee)

        def has_value(self, name=''):
            """ Return True if the current key has a value named
            'name', False otherwise """
            try:
                if name:
                    winreg.QueryValueEx(self.hkey, name)
                else:
                    winreg.QueryValue(self.hkey, '')
                return True
            except WindowsError:
                return False
            except EnvironmentError:
                return False

        def get_key(self):
            """ Return the embedded PyHKEY object of this key """
            return self.hkey

        def get_key_name(self):
            """ Return the name of this key """
            return self.key_name

        def set_key(self, hkey):
            """ Set the PyHKEY object of this key """
            self.hkey = hkey

        def set_key_name(self, key_name):
            """ Set the keyname for this key """
            self.key_name = key_name

        def close(self):
            """ Close this key """
            if self.hkey:
                winreg.CloseKey(self.hkey)

    def open(self, key, sub_key, access=winreg.KEY_READ):
        """ Open a windows registry key. Same
        as OpenKey of winreg, defaults to read-only access"""
        reg_key = Registry.RegistryKey()
        reg_key.open(key, sub_key, access)
        return reg_key

    def create(self, key, sub_key):
        """ Create a windows registry key. Same
        as CreateKey of winreg """
        reg_key = Registry.RegistryKey()
        reg_key.create(key, sub_key)
        return reg_key

    def delete(self, key, sub_key):
        """ Deletes a windows registry key. Same
        as DeleteKey of winreg """
        if type(key) is str:
            hkey = self.map_key(key)
        else:
            hkey = key

        if not hkey:
            raise RegistryError(f"Could not find registry key for {key}")

        try:
            winreg.DeleteKey(hkey, sub_key)
        except EnvironmentError as e:
            raise RegistryError(e)

    def delete_value(self, key, sub_key, field):
        """ Deletes 'field' from location key\\sub_key """
        open_key = self.open(key, sub_key, winreg.KEY_ALL_ACCESS)
        py_key = open_key.get_key()
        winreg.DeleteValue(py_key, field)

    def set_value(self, key, sub_key, field, value, reg_type=winreg.REG_SZ):
        """ Sets 'field' at location key\\sub_key to have the value and type supplied """
        open_key = self.open(key, sub_key, winreg.KEY_ALL_ACCESS)
        py_key = open_key.get_key()
        winreg.SetValueEx(py_key, field, 0, reg_type, value)

    def recursive_delete(self, key, sub_key):
        """ Recursively delete a Windows registry key.
        This function will remove a key, even if it
        has child keys. There is no equivalent in
        winreg. """
        if type(key) is str:
            hkey = Registry.map_key(key)
        elif type(key) is Registry.RegistryKey:
            hkey = key.get_key()
        else:
            hkey = key

        if type(sub_key) is str:
            sub_key = self.open(hkey, sub_key)

        child_keys = sub_key.keys()
        for key_name in child_keys:
            child_key = self.open(sub_key.get_key(), key_name)
            self.recursive_delete(sub_key, child_key)

        winreg.DeleteKey(hkey, sub_key.get_key_name())

