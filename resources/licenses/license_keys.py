import sys
"""License automation test serial numbers and activation codes"""


# Run tests with arg --live to use the live licenses. This can be done in Pycharm in
# Run->Edit Configurations...->Parameters: --live
if '--live' in sys.argv:
    import resources.licenses.live_licenses as licenses
else:
     import resources.licenses.staging_licenses as licenses

ENTERPRISE_LICENSE_SERIAL = licenses.ENTERPRISE_SERIAL
ENTERPRISE_LICENSE_ACTIVATION = licenses.ENTERPRISE_ACTIVATION

TINY_ENTERPRISE_SERIAL = licenses.TINY_ENT_SERIAL
TINY_ENTERPRISE_ACTIVATION = licenses.TINY_ENT_ACTIVATION

HOME_LICENSE_SERIAL = licenses.HOME_SERIAL
HOME_LICENSE_ACTIVATION = licenses.HOME_ACTIVATION

OEM_LICENSE_SERIAL = licenses.OEM_SERIAL
OEM_LICENSE_ACTIVATION = licenses.OEM_ACTIVATION

AUDITOR_LICENSE_SERIAL = licenses.AUDITOR_SERIAL
AUDITOR_LICENSE_ACTIVATION = licenses.AUDITOR_ACTIVATION

TRAINING_LICENSE_SERIAL = licenses.TRAINING_SERIAL
TRAINING_LICENSE_ACTIVATION = licenses.TRAINING_ACTIVATION

BAD_LICENSE_SERIAL = "123BADSERIALISME"
BAD_LICENSE_ACTIVATION = "789BADACTIVATIONTIME"
