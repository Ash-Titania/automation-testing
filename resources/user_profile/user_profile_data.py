"""User profile data consists of user profiles, file_paths which are stored in
user profile testing data in the resources folder"""
import os
from resources.create_report.create_report_data import resource_files_path

profile_path = os.path.join(resource_files_path, 'user profile testing folder')

user1_append = 'user1.xml'
user2_append = 'user2.xml'
nipper_profile_2_path = os.path.join(profile_path, user2_append)
