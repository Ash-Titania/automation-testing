"""resource data consisting of resource name, resource category and resource file path which is available in T:\\ Drive
for performing resource tests"""
import os
from resources.create_report.create_report_data import resource_files_path

resource_testing_files = os.path.join(resource_files_path, 'Resource Testing Files')

STIG_NAME = "[Super Special STIG]"
STIG_CATEGORY = "STIG"
stig_append = 'tempStig.xml'
STIG_FILE = os.path.join(resource_testing_files, stig_append)

WORD_LIST_NAME = "Word-list-test"
WORD_LIST_CATEGORY = r"Word Lists"
words_append = 'Word-list-test.txt'
WORD_LIST_FILE = os.path.join(resource_testing_files, words_append)

CPE_NAME = "CPE"
CPE_CATEGORY = "CPE"
cpe_append = 'official-cpe-dictionary_v2.3.xml'
CPE_FILE = os.path.join(resource_testing_files, cpe_append)

NVD_NAME = "CVE 1.1 3000"
NVD_CATEGORY = "NVD"
nvd_append = 'nvdcve-1.1-3000.json'
NVD_FILE = os.path.join(resource_testing_files, nvd_append)

CSS_NAME = "test_css"
CSS_CATEGORY = "CSS"
css_append = 'test_css.css'
CSS_FILE = os.path.join(resource_testing_files, css_append)

BAD_FILE_NAME = "corrupted_resource_file.xml"
BAD_FILE_CATEGORY = "NVD"
bad_append = 'corrupted_resource_file.xml'
BAD_FILE = os.path.join(resource_testing_files, bad_append)
