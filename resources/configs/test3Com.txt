#
 sysname 5500-EI
#
 password-control login-attempt 3 exceed lock-time 120
#
 super password level 3 simple magla114bhr
#
 ip host chicken 10.10.10.10
#
 dhcp-server 19 ip  168.172.12.14 168.172.12.15
#
 router id 1.2.3.7
#
 lldp enable
#
 port-security enable
#
 am enable
#
 vrrp ping-enable
#
 multicast routing-enable
#
 dns server 8.8.8.8
 dns domain titania.co.uk
#
 system-guard ip enable
#
ssl server-policy testssl
 ciphersuite rsa_des_cbc_sha rsa_aes_256_cbc_sha
ssl server-policy 1
#
 web-authentication web-server ip 10.10.10.10 port 80
#
radius scheme system
#
domain system
domain titania.ad
#
local-user admin
 service-type telnet
local-user andy
#
acl number 2222
 description a ttest basic acl
 rule 10 permit source 10.10.10.10 0
 rule 20 permit source 0.0.0.20 255.255.255.0
 rule 30 permit
 rule 31 deny source 1.2.3.4 0
#
acl number 3333
 description beans advanced acl
 rule 0 permit ip source 10.10.10.10 0 destination 20.20.20.0 0.0.0.255
 rule 1 permit tcp source 2.3.0.0 0.0.255.255 source-port lt 56 destination 1.2.3.4 0 destination-port gt 4
 rule 2 deny ospf
 rule 3 deny icmp
#
acl number 4444
 rule 0 permit 802.3 source 0014-0014-0014 0055-0055-0055
#
vlan 1
#
 ntp-service access server 2222
 ntp-service authentication-keyid 12 authentication-mode md5 S;IKAY5^0NWQ=^Q`MAF4<1!!
#
 undo xrn-fabric authentication-mode
#
ospf 1
 import-route rip
 import-route static
 peer 1.2.3.4
 peer 192.168.0.34
 area 0.0.0.42
  network 10.10.10.0 0.0.0.255
  authentication-mode simple
 #
 area 0.0.0.43
  network 3.4.5.0 0.0.0.255
  authentication-mode md5
#
rip
 import-route ospf 1
 import-route static
#
 telnet-server source-interface Vlan-interface1
#
user-interface aux 0 7
user-interface vty 0 4
#
return
