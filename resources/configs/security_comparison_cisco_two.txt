Using 2213 out of 196600 bytes
!
! Last configuration change at 12:11:15 UTC Thu Apr 4 2013
!
version 15.0
no service pad
service tcp-keepalives-in
service timestamps debug datetime msec
service timestamps log datetime msec
service password-encryption
!
hostname Router
!
boot-start-marker
boot-end-marker
!
security passwords min-length 8
enable secret 5 $1$ThCA$bNCffAGEfPtH1Vuk4aASj/
!
aaa new-model
!
aaa user profile test
!
!
!
!
!
!
aaa session-id common
!
!
!
dot11 syslog
no ip source-route
!
!
!
!
ip cef
no ip bootp server
no ip domain lookup
no ipv6 cef
!
multilink bundle-name authenticated
!
!
crypto pki trustpoint TP-self-signed-834679690
 enrollment selfsigned
 subject-name cn=IOS-Self-Signed-Certificate-834679690
 revocation-check none
 rsakeypair TP-self-signed-834679690
!
!
crypto pki certificate chain TP-self-signed-834679690
 certificate self-signed 01 nvram:IOS-Self-Sig#11.cer
!
!
license udi pid CISCO1841 sn FCZ130693M0
vtp mode transparent
username admin password 7 111918160405041E00
!
redundancy
!
!
ip ssh stricthostkeycheck
ip ssh pubkey-chain
 username admin
!
!
!
!
!
!
!
interface FastEthernet0/0
 ip address 10.200.14.120 255.255.255.0
 shutdown
 duplex auto
 speed auto
 !
!
interface FastEthernet0/1
 ip address 10.200.4.200 255.255.255.0
 no ip redirects
 no ip unreachables
 no ip proxy-arp
 duplex auto
 speed auto
 no mop enabled
 !
!
interface ATM0/1/0
 no ip address
 shutdown
 no atm ilmi-keepalive
 !
!
interface Async0/0/0
 no ip address
 encapsulation slip
 shutdown
 !
!
no ip classless
ip forward-protocol nd
no ip http server
ip http secure-server
ip http secure-ciphersuite 3des-ede-cbc-sha
!
!
!
logging 192.168.1.1
access-list 51 deny   14.2.9.7 log
access-list 51 permit 14.2.9.6
no cdp run

!
!
!
!
!
!
control-plane
 !
!
banner exec ^CHello^C
banner login ^Cwarning - access is restricted by ninja donkeys.  Unauthorised access will result in immediate kippering and lonely Wednesdays^C
banner motd ^C Test Banner ^C
!
line con 0
 exec-timeout 5 0
 password 7 00141215174C04140B
line aux 0
 exec-timeout 0 10
 password 7 00141215174C04140B
 no exec
line 0/0/0
 stopbits 1
 speed 115200
 flowcontrol hardware
line vty 0 4
 password 7 08314D5D1A0E0A0516
!
scheduler allocate 20000 1000
end
