"""
A file to contain all of the data to be used when checking STIG reports.
"""

"""
A list of each of the current stig profiles, contains the cli command and the profile title
"""
stig_profiles = {"1mcp": "I - Mission Critical Public", "1mcs": "I - Mission Critical Sensitive",
                 "1mcc": "I - Mission Critical Classified", "2msp": "II - Mission Support Public",
                 "2mss": "II - Mission Support Sensitive", "2msc": "II - Mission Support Classified",
                 "3ap": "III - Administrative Public", "3as": "III - Administrative Sensitive",
                 "3ac": "III - Administrative Classified"}

"""
STIG Profiles Test Data
"""
profile_report_data = ['Cisco Router', 'CiscoIOS15', 'Infrastructure Router Security',
                       'Vulnerability Severity Code Definition', 'V-3000', 'NET1020', 'V-31285', 'NET0408',
                       'Conclusions', 'DISA STIG device compliance summary', 'Recommendations']
