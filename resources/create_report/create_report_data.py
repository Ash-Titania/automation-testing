import os

"""
create_report_data consists of device config files path, baseline file path, Input directory, compare file path 
along with details for remote network device
"""

resource_files_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), r'Report_Testing_Files')

cisco_nexus_config = os.path.join(resource_files_path, 'Input_files/TS_130-A-Nexus.txt')

baseline_file_path = os.path.join(resource_files_path, 'Input_files/TS_130-A-Nexus.fbl')

compare_file_path = os.path.join(resource_files_path, 'Input_files/TS_130-A-Nexus.xml')

file_dir = os.path.join(resource_files_path + '/Add_dir')

top_ten_devices_dir = os.path.join(resource_files_path, 'config_dir_top_ten_devices')

all_device_types = os.path.join(resource_files_path, 'all-device-types')

stress_test = os.path.join(resource_files_path, 'stress-test - config collection')

"""
list of devices along with their relative config files in top_ten_devices_dir
top_ten_device_config_files_data consists of top ten device config files directory
Cisco Catalyst (IOS)                       (Cisco_Catalyst_3550.txt)
Cisco Router (IOS)                         (CisconTS-120 C - Router (IOS).txt)
Cisco Security Appliance (ASA)             (CiscoTS_109 A - Security Appliance (PIX).txt)
CheckPoint                                 (TS_128 XML - CheckPoint Appliance)
Cisco Nexus                                (Cisco_Nexus_5010.txt)
Fortinet FortiGate UTM Firewall            (TS_165-FortigateVM_Utm_Firewall_20191105_0758.conf)
Juniper SRX Firewall                       (Juniper_SRX_10.txt)
Cisco Security Appliance (ASA) Context     (used one for Cisco Security Appliance (ASA)* put command --device-type for --ASAcontext)
Palo Alto Firewall                         (Palo_Alto_PanOS_7.xml)
Fortinet FortiGate Firewall                (TS_04 A - Fortinet Fortigate Firewall.txt)
"""

"""
 list of device network information in the order given below:
network_device = [ip_address, device_type, protocol, port, username, password, privilege_password]
"""
cisco_nexus_device = ["10.200.8.195", "--cisco-nexus", "ssh", "22", "admin", "password", "password"]
juniper_ssg_device = ["10.200.8.209", "--juniper-ssg", "ssh", "22", "netscreen", "netscreen", None]
juniper_srx_device = ["10.200.8.210", "--juniper-srx", "ssh", "22", "admin", "password1", None]
cisco_ios_xe_device = ["10.200.8.167", "--ios-xe", "ssh", "22", "Test", "password", "password"]
panorama_9_device = ["10.200.8.233", "https", "AutomationTestAdmin", "Password!111"]

"""
A list of report information to be used by test_add_device_directory in create_report_test.
This all of the information that should be within the report.
"""
dir_report_data = ['Extreme Networks Summit300-48', 'Extreme Networks Summit Switch', 'Extreme Networks Alpine Switch',
                   'Summit300-48', 'TS_42 A - Summit Switch (ExtremeWare).txt',
                   'TS_42 B - Summit Switch (ExtremeWare).txt', 'Summit48', 'TS_44 A - Alpine Switch (XOS).txt',
                   'Security Audit Summary', 'Vulnerability Audit', 'Configuration Report', 'Appendix']

"""
A list of report information to be used when checking a cisco nexus connection in test_add_device_network in 
create_report_test.
"""
nexus_report_data = ['Cisco Nexus', 'TitaniaNexus', 'Security Audit Summary', 'Vulnerability Audit Summary',
                     'Configuration Report', 'Appendix']

"""
A list of report information to be used when checking a cisco IOS XE connection in test_add_device_network in
create_report_test
"""
cisco_xe_report_data = ['Cisco XE', 'Security Audit Summary', 'Vulnerability Audit Summary',
                        'Configuration Report', 'Appendix']

"""
List of managed devices of the Panorama
"""
panorama_9_managed_devices = ["Panorama", "PA-VM_8.1", "PA-VM-9"]
