!Current Configuration:
!
!System Description "FSM7328S 24+4 L3 Stackable Switch"
!System Description 7.3.1.7
!
set prompt "FSM7328S"
network protocol none
vlan database
vlan routing 1
exit

ip http secure-server
no ip http server
configure
sntp client mode unicast
! sntp server status is active

--More-- or (q)uit

sntp server time-d.netgear.com
stack
exit

logging buffered
logging syslog
slot 1/0 3
set slot power 1/0
no set slot disable 1/0
ip routing
lineconfig
serial baudrate 115200
exit

spanning-tree configuration name 00-1B-2F-B3-C1-0D
router rip
exit


--More-- or (q)uit

port-security
no ip domain-lookup
interface  1/0/1
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/2
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt

--More-- or (q)uit

exit

interface  1/0/3
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/4
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit


--More-- or (q)uit

interface  1/0/5
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/6
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/7

--More-- or (q)uit

lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/8
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/9
lldp transmit-tlv port-desc

--More-- or (q)uit

lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/10
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/11
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name

--More-- or (q)uit

lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/12
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/13
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc

--More-- or (q)uit

lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/14
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/15
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap

--More-- or (q)uit

lldp transmit-mgmt
exit

interface  1/0/16
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/17
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt

--More-- or (q)uit

exit

interface  1/0/18
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/19
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit


--More-- or (q)uit

interface  1/0/20
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/21
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/22

--More-- or (q)uit

lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/23
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/24
lldp transmit-tlv port-desc

--More-- or (q)uit

lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/25
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/26
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name

--More-- or (q)uit

lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/27
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc
lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface  1/0/28
lldp transmit-tlv port-desc
lldp transmit-tlv sys-name
lldp transmit-tlv sys-desc

--More-- or (q)uit

lldp transmit-tlv sys-cap
lldp transmit-mgmt
exit

interface vlan 1
routing
ip address  10.200.4.223  255.255.255.0
exit

exit
