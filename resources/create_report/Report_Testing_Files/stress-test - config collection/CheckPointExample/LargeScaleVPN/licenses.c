(
	:version (6.0)
	: (dhdkJYSFZDoCkabCJkDMRLXRL3EAfnkjwwbb
		:AdminInfo (
			:chkpf_uid ("{65A4652D-6B14-463F-A16C-5B0FBB800E99}")
			:ClassName (cp_license)
			:table (licenses)
			:LastModified (
				:Time ("Thu Feb 10 14:40:31 2005")
				:By (aa)
				:From (AZELINSKY)
			)
		)
		:account ()
		:comment ()
		:eligible_for_upgrade (false)
		:expiration_date (11May2006)
		:host (1.1.1.1)
		:license_name ("mgmt license")
		:limit (1)
		:mode (central)
		:network_object (
			: (ReferenceObject
				:Table (network_objects)
				:Name (Management)
				:Uid ("{DDE690F5-CBB8-4563-A290-5ADC056A5691}")
			)
		)
		:new_license ()
		:option_pack (9)
		:owner ()
		:plug_n_play (false)
		:signature (dhdkJYSFZDoCkabCJkDMRLXRL3EAfnkjwwbb)
		:sku (
			: (CPMP-MEDIA-1-NG)
			: (CK-CHECK-POINT-INTERNAL-USE-ONLY)
		)
		:state (installed)
		:status ()
		:type (license)
		:version (5.0)
	)
	: (axxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		:AdminInfo (
			:chkpf_uid ("{E6A3A778-5DB1-4C15-A5FE-5743BAF4C571}")
			:ClassName (cp_license)
			:LastModified (
				:Time ("Thu Feb 10 14:44:02 2005")
				:By (aa)
				:From (AZELINSKY)
			)
			:table (licenses)
		)
		:account ()
		:comment ()
		:eligible_for_upgrade (false)
		:expiration_date (8Feb2006)
		:host (eval)
		:license_name ("eval license")
		:limit (2147483647)
		:mode (central)
		:network_object (
			: (ReferenceObject
				:Table (network_objects)
				:Name (Corporate-WA-proxy-server)
				:Uid ("{189945E8-7D05-4878-B857-CD0BDFA08A0B}")
			)
		)
		:new_license ()
		:option_pack (9)
		:owner ()
		:plug_n_play (false)
		:signature (axxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx)
		:sku (
			: (CPMP-PNP-1-NG)
		)
		:state (installed)
		:status ()
		:type (license)
		:version (5.0)
	)
	: (dxpdfEn45J3JWWaPAA8xEr6DXkjynP3yJk9x
		:AdminInfo (
			:chkpf_uid ("{5C3E5044-A522-4B96-A1DC-BDC156E5AE32}")
			:ClassName (cp_license)
			:table (licenses)
			:LastModified (
				:Time ("Thu Feb 10 14:44:02 2005")
				:By (aa)
				:From (AZELINSKY)
			)
		)
		:account ()
		:comment ()
		:eligible_for_upgrade (false)
		:expiration_date (03Mar2006)
		:host (1.1.1.1)
		:license_name ("license 3")
		:limit (1)
		:mode (central)
		:network_object (
			: (ReferenceObject
				:Table (network_objects)
				:Name (Corporate-mail-server)
				:Uid ("{CB0C156F-FFAD-4B5B-BF70-54A0FBB26310}")
			)
		)
		:new_license ()
		:option_pack (9)
		:owner ()
		:plug_n_play (false)
		:signature (dxpdfEn45J3JWWaPAA8xEr6DXkjynP3yJk9x)
		:sku (
			: (CPMP-MEDIA-1-NGX)
			: (CK-CHECK-POINT-INTERNAL-USE-ONLY)
		)
		:state (installed)
		:status ()
		:type (license)
		:version (6.0)
	)
	: (d9LJmqibq4xVffFB8MaLMFWHwUhKc94zbJXc
		:AdminInfo (
			:chkpf_uid ("{3946E54A-7341-454F-B696-6648C678DF21}")
			:ClassName (cp_license)
			:LastModified (
				:Time ("Thu Feb 10 14:44:02 2005")
				:By (aa)
				:From (AZELINSKY)
			)
			:table (licenses)
		)
		:account ()
		:comment ()
		:eligible_for_upgrade (false)
		:expiration_date (20Jan2005)
		:host (1.1.1.1)
		:license_name ("license 1")
		:limit (1)
		:mode (central)
		:network_object ()
		:new_license ()
		:option_pack (9)
		:owner ()
		:plug_n_play (false)
		:signature (d9LJmqibq4xVffFB8MaLMFWHwUhKc94zbJXc)
		:sku (
			: (CPMP-MEDIA-1-NG)
			: (CK-CHECK-POINT-INTERNAL-USE-ONLY)
		)
		:state (not-installed)
		:status ()
		:type (license)
		:version (5.0)
	)
	: (dKGoPGMVJNKP2ffcqFD3Hz3dBpHCxeFKnhAi
		:AdminInfo (
			:chkpf_uid ("{51B6D8D1-B1D1-4F18-A274-B623292EB2C5}")
			:ClassName (cp_license)
			:table (licenses)
			:LastModified (
				:Time ("Thu Feb 10 14:44:02 2005")
				:By (aa)
				:From (AZELINSKY)
			)
		)
		:account ()
		:comment ()
		:eligible_for_upgrade (false)
		:expiration_date (09Mar2005)
		:host (1.1.1.1)
		:license_name ("license 2")
		:limit (1)
		:mode (central)
		:network_object (
			: (ReferenceObject
				:Table (network_objects)
				:Name (Corporate-gw)
				:Uid ("{A034500E-4076-479E-A9D1-F1B3D9ABEBC3}")
			)
		)
		:new_license ()
		:option_pack (9)
		:owner ()
		:plug_n_play (false)
		:signature (dKGoPGMVJNKP2ffcqFD3Hz3dBpHCxeFKnhAi)
		:sku (
			: (CPMP-MEDIA-1-NGX)
			: (CK-CHECK-POINT-INTERNAL-USE-ONLY)
		)
		:state (installed)
		:status ()
		:type (license)
		:version (6.0)
	)
	: (dfw77sp88U7dWEcz8d9srz2h4bFjpBNxrmnm
		:AdminInfo (
			:chkpf_uid ("{9C8FA3E1-B82A-40F0-83D8-9B9310F236D3}")
			:ClassName (cp_license)
			:LastModified (
				:Time ("Thu Feb 10 14:44:25 2005")
				:By (aa)
				:From (AZELINSKY)
			)
			:table (licenses)
		)
		:account ()
		:comment ()
		:eligible_for_upgrade (false)
		:expiration_date (19Apr2006)
		:host (1.1.1.1)
		:license_name ("license 5")
		:limit (1)
		:mode (central)
		:network_object (
			: (ReferenceObject
				:Table (network_objects)
				:Name (Corporate-Cluster-2-member-A)
				:Uid ("{74F523F6-20B3-4C74-B07B-BC153AD89801}")
			)
		)
		:new_license ()
		:option_pack (9)
		:owner ()
		:plug_n_play (false)
		:signature (dfw77sp88U7dWEcz8d9srz2h4bFjpBNxrmnm)
		:sku (
			: (CPMP-MEDIA-1-NGX)
			: (CK-CHECK-POINT-INTERNAL-USE-ONLY)
		)
		:state (installed)
		:status ()
		:type (license)
		:version (6.0)
	)
	: (wqeyquiwyei47898971731238182
		:AdminInfo (
			:chkpf_uid ("{F2041EB0-18B8-45BC-8640-309092EA424C}")
			:ClassName (cp_license)
			:table (licenses)
			:LastModified (
				:Time ("Thu Feb 10 16:02:41 2005")
				:By (aa)
				:From (AZELINSKY)
			)
		)
		:account ()
		:comment ()
		:eligible_for_upgrade (false)
		:expiration_date (12May2006)
		:host (1.1.1.1)
		:license_name ("my license")
		:limit (1)
		:mode (central)
		:network_object (
			: (ReferenceObject
				:Table (network_objects)
				:Name (Corporate-Cluster-1-member-A)
				:Uid ("{6AE28F83-4CBF-44D9-9E17-D2957818A752}")
			)
		)
		:new_license ()
		:option_pack (9)
		:owner ()
		:plug_n_play (false)
		:signature (31313123eqreqrqwrqr)
		:sku (
			: (CP-SUITE-SAMPLE-LIC)
			: (CK-12345678)
		)
		:state (installed)
		:status ()
		:type (license)
		:version (6.0)
	)
	: (a34242rwerwtw345345twertertt
		:AdminInfo (
			:chkpf_uid ("{4957FC9C-229D-463C-BFE9-E6B924144FD4}")
			:ClassName (cp_license)
			:table (licenses)
			:LastModified (
				:Time ("Thu Feb 10 16:02:41 2005")
				:By (aa)
				:From (AZELINSKY)
			)
		)
		:account ()
		:comment ()
		:eligible_for_upgrade (false)
		:expiration_date (30Jan2006)
		:host (1.1.1.1)
		:license_name ("my license 2")
		:limit (1)
		:mode (central)
		:network_object (
			: (ReferenceObject
				:Table (network_objects)
				:Name (Corporate-Cluster-1-member-B)
				:Uid ("{C89D8B91-16F8-4E1F-95F2-6DB0F7B9B8D9}")
			)
		)
		:new_license ()
		:option_pack (9)
		:owner ()
		:plug_n_play (false)
		:signature (rw453224234werwre)
		:sku (
			: (CP-SUITE-SAMPLE-LIC)
			: (CK-123456789)
		)
		:state (installed)
		:status ()
		:type (license)
		:version (6.0)
	)
)
