(
	:version (5.0)
	: (configurable_attributes_obj
		:AdminInfo (
			:chkpf_uid ("{2562F32C-E275-4457-A53C-DB058C73B888}")
			:ClassName (configurable_attributes)
			:table (uncovered_global_props)
		)
		:attribs (
			: (CP_attribs
				:AdminInfo (
					:chkpf_uid ("{33F185A9-F39A-4D71-B6CF-87A458EC6340}")
					:ClassName (list_of_attributes_groups)
				)
				:attributes_group (
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (send_clear_traffic_between_encryption_domains)
							: (sdl_netlogon_timeout)
							: (add_radius_groups)
						)
						:group_title ("SecuRemote/SecureClient")
						:level (0)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (desktop_ike_p2_prop_size)
							: (force_udp_encapsulation_gw)
							: (udp_encapsulation_by_qm_id)
							: (om_extended_dhcp_params)
							: (desktop_disable_MEP)
						)
						:group_title ("IKE/IPSec Settings")
						:level (1)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name ()
						:group_title ("VPN Advanced Properties")
						:level (0)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (ike_handle_initial_contact)
							: (ike_send_initial_contact)
							: (keep_IKE_SAs)
						)
						:group_title ("VPN IKE properties")
						:level (1)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (resolve_interface_ranges_nated_gw)
							: (enable_if_resolving_third_party_clusters)
							: (use_on_demand_links)
							: (on_demand_metric_min)
						)
						:group_title ("Link Selection")
						:level (1)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (vpn_restrict_client_phase2_id)
							: (ike_support_transport_mode)
							: (ie_proxy_replacement)
							: (ie_proxy_replacement_limit_to_tcpt)
						)
						:group_title ("Remote Access VPN")
						:level (1)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (life_sign_timeout)
							: (life_sign_transmitter_interval)
							: (life_sign_retransmissions_count)
							: (life_sign_retransmissions_interval)
							: (cluster_status_polling_interval)
							: (RIM_inject_peer_interfaces)
						)
						:group_title ("Tunnel Management")
						:level (1)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (cert_req_ext_key_usage)
							: (flush_crl_cache_file_on_install)
							: (flush_crl_cache_on_install)
							: (host_certs_key_size)
							: (prefetch_crls_duration)
							: (prefetch_crls_on_install)
							: (trust_all_capi_trusted_root_cas)
							: (user_certs_key_size)
							: (warncertexpiry)
							: (use_dkm_cert_by_default)
							: (add_ip_alt_name_for_opsec_certs)
							: (add_ip_alt_name_for_ICA_certs)
						)
						:group_title ("Certificates and PKI properties")
						:level (0)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name ()
						:group_title (FireWall-1)
						:level (0)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (http_activate_ss_protections)
						)
						:group_title ("Web Security")
						:level (1)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (http_avoid_keep_alive)
							: (http_connection_method_proxy)
							: (http_connection_method_transparent)
							: (http_failed_resolve_timeout)
							: (http_connection_method_tunneling)
							: (http_force_down_to_10)
							: (http_handle_proxy_pw)
							: (http_session_timeout)
							: (http_use_host_h_as_dst)
							: (http_use_cache_hdr)
						)
						:group_title ("HTTP Protocol")
						:level (2)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (http_allow_double_slash)
							: (http_enable_uri_queries)
							: (http_use_default_schemes)
						)
						:group_title (URL)
						:level (3)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (enable_propfind_method)
							: (http_allow_content_disposition)
							: (http_web_encoding)
						)
						:group_title (Security)
						:level (2)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (cvp_keep_alive)
							: (http_allow_ranges)
							: (http_block_java_allow_chunked)
							: (http_cvp_allow_chunked)
							: (http_disable_content_enc)
							: (http_disable_content_type)
							: (http_use_cvp_reply_safe)
							: (http_weeding_allow_chunked)
						)
						:group_title ("Content Security")
						:level (3)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (http_dns_cache_timeout)
							: (http_dont_dns_when_star_port)
							: (http_enable_resolve_by_ip)
						)
						:group_title (DNS)
						:level (2)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (http_buffers_size)
							: (sn_connect_timeout)
							: (http_process_timeout)
							: (http_max_concurrent_connections)
							: (http_auto_calc_capacity_limits)
							: (http_session_pool_size)
							: (http_report_pool_size)
							: (http_large_header_connection_percent)
						)
						:group_title (Tuning)
						:level (2)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (undo_msg)
							: (respawn_process_forever)
							: (respawn_process_interval)
						)
						:group_title ("Security Servers")
						:level (1)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (mdq_run_multi_threaded)
							: (smtp_encoded_content_field)
							: (smtp_enforce_hex_encoding)
							: (smtp_exact_str_match)
							: (smtp_force_no_uu_begin_after_decode)
							: (smtp_force_no_uu_begin_before_decode)
							: (smtp_force_no_uu_begin_in_prolog_epilog)
							: (smtp_force_uu_syntax_check)
							: (smtp_mail_encoding)
							: (smtp_max_file_name_length)
							: (smtp_max_global_headers_size)
							: (smtp_max_user_name_length)
							: (smtp_rfc821)
							: (smtp_rfc822)
							: (smtp_strict_mime_header)
							: (smtp_limit_content_buf_size)
							: (smtp_force_sender_domain)
							: (smtp_enforce_hex_encoding)
							: (smtp_allow_extended_relay)
							: (mdq_error_mail_send_body)
							: (mdq_mx_ignore_nonauth_response)
						)
						:group_title ("SMTP Security Server")
						:level (2)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (new_ftp_interface)
							: (ftp_msg_max_lines)
						)
						:group_title ("FTP Security Server")
						:level (2)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (allow_all_options)
							: (suppress_dont_echo)
						)
						:group_title ("Telnet Security Server")
						:level (2)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (au_connect_timeout)
							: (add_nt_groups)
						)
						:group_title (Authentication)
						:level (1)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (radius_ignore)
							: (radius_retrant_num)
							: (radius_retrant_timeout)
							: (radius_user_timeout)
							: (radius_connect_timeout)
							: (radius_send_framed)
						)
						:group_title (RADIUS)
						:level (2)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (securid_timeout)
						)
						:group_title (SecurID)
						:level (2)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (automatically_open_ca_rules)
							: (clauth_no_log_errors)
							: (clauth_no_resolve)
							: (clauth_tolower_users)
						)
						:group_title ("Client Authentication")
						:level (2)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (ahttpclientd_redirected_url)
							: (hclient_enable_new_interface)
							: (http_query_server_for_authorization)
							: (http_use_fwnetso)
							: (http_use_proxy_auth_for_other)
							: (http_max_auth_redirect_num)
							: (http_allow_store_reply)
							: (http_skip_redirect_free)
						)
						:group_title (HTTP)
						:level (3)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (sn_connect_timeout)
							: (sn_timeout)
							: (snauth_old_clients_message)
							: (snauth_protocol)
						)
						:group_title ("Session Authentication")
						:level (2)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (stack_size)
						)
						:group_title (System)
						:level (1)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (reject_x11_in_any)
							: (log_data_conns)
							: (enable_ip_options)
							: (conn_limit_reached_log)
							: (conn_limit_notify_interval)
							: (fw_clamp_tcp_mss)
							: (fw_trust_suspicious_rst)
							: (fw_trust_suspicious_estab)
						)
						:group_title ("Stateful Inspection")
						:level (1)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (voip_allow_no_from)
						)
						:group_title (VoIP)
						:level (1)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (resolver_session_interval)
							: (resolver_ttl)
						)
						:group_title (Resolver)
						:level (1)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (hide_max_high_port)
							: (hide_min_high_port)
							: (hide_alloc_attempts)
						)
						:group_title (NAT)
						:level (1)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (dynlog_exc_default_log_filter)
							: (dynlog_exc_default_should_report)
							: (dynlog_exc_default_threshold)
							: (dynlog_exc_default_timeout)
						)
						:group_title ("INSPECT logs")
						:level (1)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (rulebase_uids_in_log)
							: (iiop_1_0_and_1_1_only)
						)
						:group_title (General)
						:level (1)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (EnableUserMonitoring)
							: (EnableNewUserMonitoring)
						)
						:group_title (SmartCenter)
						:level (0)
					)
					: (
						:AdminInfo (
							:ClassName (attributes_group)
						)
						:attributes_name (
							: (sofaware_stealth)
							: (skip_swcmd_updatenowall)
						)
						:group_title ("VPN-1 UTM Edge/Embedded Gateway")
						:level (0)
					)
				)
			)
			: (Customer_attribs
				:AdminInfo (
					:chkpf_uid ("{FBDBC62A-AAB8-4A07-9E5C-EB91CF38D0A5}")
					:ClassName (list_of_attributes_groups)
				)
				:attributes_group (
					: (
						:AdminInfo (
							:chkpf_uid ("{097A7B0B-165D-47D5-9005-C7F69DD3D337}")
							:ClassName (attributes_group)
						)
						:attributes_name ()
						:group_title ("User Defined")
						:level (0)
					)
				)
			)
		)
	)
)
