(
	:version (5.9)
	: (Standard_User
		:AdminInfo (
			:chkpf_uid ("{29B9C668-4BD7-4DE7-B399-9E30F0662310}")
			:ClassName (user_template)
			:table (users)
			:Deleteable (false)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{025593D3-E9E9-42C8-A452-EF6D880113F8}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:destinations (
			: (ReferenceObject
				:Table (globals)
				:Name (Any)
				:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
			)
		)
		:groups ()
		:sources (
			: (ReferenceObject
				:Table (globals)
				:Name (Any)
				:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
			)
		)
		:auth_method (radius)
		:color (black)
		:comments ()
		:days (127)
		:expiration_date (31-dec-2008)
		:fromhour ("00:00")
		:name (Standard_User)
		:notdelete (false)
		:radius_server (ReferenceObject
			:Name (Any)
			:Table (globals)
			:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
		)
		:tacacs_server (ReferenceObject
			:Table (globals)
			:Name (Any)
			:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
		)
		:tohour ("23:59")
		:type (template)
		:userc (
			:AdminInfo (
				:chkpf_uid ("{BCC1FC4D-8F61-4F86-B9BC-04528414C217}")
				:ClassName (userc)
			)
			:use_global_encryption_values (false)
			:FWZ ()
			:IKE ()
			:accept_track ()
		)
	)
	: (Dallas_Users
		:AdminInfo (
			:chkpf_uid ("{B14DBCF4-DF13-4C3F-ADD3-BAD54D6C0393}")
			:ClassName (user_group)
			:table (users)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{DDF1AB05-D9C1-49E3-ACEB-C3F730EEED8F}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:groups ()
		:class (SDBM)
		:color (blue1)
		:comments ()
		:type (usrgroup)
	)
	: (Berlin_users
		:AdminInfo (
			:chkpf_uid ("{04CBDD1B-40D1-4954-B319-92F0392E9012}")
			:ClassName (user_group)
			:table (users)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{2B8656C2-3628-4596-9E47-0152C91B3C64}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:groups ()
		:class (SDBM)
		:color ("dark orchid")
		:comments ()
		:type (usrgroup)
	)
	: (London_users
		:AdminInfo (
			:chkpf_uid ("{82C4C650-F2F5-4EC2-889B-CD5159E61E80}")
			:ClassName (user_group)
			:table (users)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{717E8321-A4DC-4234-B0D2-9E66C4FAD348}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:groups ()
		:class (SDBM)
		:color ("dark orchid")
		:comments ()
		:type (usrgroup)
	)
	: (Paris_users
		:AdminInfo (
			:chkpf_uid ("{9A699D81-EF34-4B25-BC1F-4C8AA7AC7064}")
			:ClassName (user_group)
			:table (users)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{F48165E7-24AD-405A-9830-E375BB2F437C}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:groups ()
		:class (SDBM)
		:color (black)
		:comments ()
		:type (usrgroup)
	)
	: (Tokyo_users
		:AdminInfo (
			:chkpf_uid ("{AC82ACDB-0817-491C-836D-9948618D689D}")
			:ClassName (user_group)
			:table (users)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{31F825F4-083A-4BA5-A1AD-3BDCD1714ED7}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:groups ()
		:class (SDBM)
		:color (green)
		:comments ()
		:type (usrgroup)
	)
	: (contractor_users
		:AdminInfo (
			:chkpf_uid ("{1D6971FB-CB8A-4AC8-A581-FB3A11D7228C}")
			:ClassName (user_group)
			:table (users)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{ED355818-2711-4EB8-B06A-B44416E09CCD}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:groups ()
		:class (SDBM)
		:color ("light coral")
		:comments ()
		:type (usrgroup)
	)
	: (Administrators
		:AdminInfo (
			:chkpf_uid ("{6278F7A3-8C6D-466C-914B-D2905EC3EC2D}")
			:ClassName (user_group)
			:table (users)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{A639EADE-9F51-473B-B211-D8C8889DC505}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:groups ()
		:class (SDBM)
		:color (blue1)
		:comments ("FW&VPN admins")
		:type (usrgroup)
	)
	: (UsersGroup_for_Dallas
		:AdminInfo (
			:chkpf_uid ("{F70BD937-F652-4FDF-BA5A-B039D8E36440}")
			:ClassName (user_group)
			:table (users)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{276A28A1-CBF3-45C0-981E-F0C2DCD47C17}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:groups ()
		: (ReferenceObject
			:Name (Administrators)
			:Table (users)
			:Uid ("{6278F7A3-8C6D-466C-914B-D2905EC3EC2D}")
		)
		: (ReferenceObject
			:Name (contractor_users)
			:Table (users)
			:Uid ("{1D6971FB-CB8A-4AC8-A581-FB3A11D7228C}")
		)
		: (ReferenceObject
			:Name (Dallas_Users)
			:Table (users)
			:Uid ("{B14DBCF4-DF13-4C3F-ADD3-BAD54D6C0393}")
		)
		:class (SDBM)
		:color (cyan)
		:comments ()
		:type (usrgroup)
	)
	: (Dallas_users
		:AdminInfo (
			:chkpf_uid ("{988D7B49-57F1-4F1C-9E12-67FAF34148D9}")
			:ClassName (user_group)
			:table (users)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{D5C2C4B2-79D7-4CDC-BEC4-E41A11765442}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:groups ()
		:class (SDBM)
		:color (blue1)
		:comments ()
		:type (usrgroup)
	)
	: (Dallas_Administrators
		:AdminInfo (
			:chkpf_uid ("{A3727987-AB5F-4A3F-B571-32A2709B9382}")
			:ClassName (user_group)
			:table (users)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{1BF551F6-0BCF-4F44-9677-5BE7915357DC}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:groups ()
		:class (SDBM)
		:color ("dark orchid")
		:comments ()
		:type (usrgroup)
	)
	: (Berlin_Users
		:AdminInfo (
			:chkpf_uid ("{7C45BFB1-591B-4CB6-9ACB-600B10386D1B}")
			:ClassName (user_group)
			:table (users)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{878A57ED-A70C-418E-BB05-A78BD8056EF5}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:groups ()
		:class (SDBM)
		:color ("medium slate blue")
		:comments ()
		:type (usrgroup)
	)
	: (London_Users
		:AdminInfo (
			:chkpf_uid ("{3F1B41B2-7086-44F7-9CEA-B42E6111997E}")
			:ClassName (user_group)
			:table (users)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{DECAD09E-1B98-4B3D-B22A-17B29CE4FCCC}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:groups ()
		:class (SDBM)
		:color (blue)
		:comments ()
		:type (usrgroup)
	)
	: (Tokyo_Users
		:AdminInfo (
			:chkpf_uid ("{51E2C766-0CE9-4772-B56B-6AA6A1754285}")
			:ClassName (user_group)
			:table (users)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{E67E2F27-9B54-440C-BF74-C82E6082E7CB}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:groups ()
		:class (SDBM)
		:color (gold)
		:comments ()
		:type (usrgroup)
	)
	: (Dallas_rnd_Users
		:AdminInfo (
			:chkpf_uid ("{805EE3B8-C3A7-4050-8976-589EE0EA35EC}")
			:ClassName (external_group)
			:table (users)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{626DAE5E-2848-4D85-9089-F3AFD3A4EFF0}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:groups ()
		:Group_Scope (All_Branches)
		:au (ReferenceObject
			:Name (Dallas_AD)
			:Table (servers)
			:Uid ("{7BF4C65F-4062-484A-AB03-E1AC6E20BED2}")
		)
		:branch ("dn=rnd, O=iNTERNAL")
		:color (gold)
		:comments ()
		:ldap_filter ()
		:ldap_groupname ()
		:subtree_prefix ()
		:type (extusrgroup)
	)
	: (Paris_Users
		:AdminInfo (
			:chkpf_uid ("{16D3FB14-C3B0-48C7-83F8-4E861613BF94}")
			:ClassName (user_group)
			:table (users)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{32506601-1D04-4436-9032-3119343C0426}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:groups ()
		:class (SDBM)
		:color (black)
		:comments ()
		:type (usrgroup)
	)
	: (Dallas_All_Users
		:AdminInfo (
			:chkpf_uid ("{49D8F2FF-B30E-4634-AE6F-5FF1E4B1CC87}")
			:ClassName (user_group)
			:table (users)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{2ABDA8FD-D982-4CA0-BB0C-DFE5363A4B26}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:groups ()
		: (ReferenceObject
			:Name (Dallas_Administrators)
			:Table (users)
			:Uid ("{A3727987-AB5F-4A3F-B571-32A2709B9382}")
		)
		: (ReferenceObject
			:Name (Dallas_rnd_Users)
			:Table (users)
			:Uid ("{805EE3B8-C3A7-4050-8976-589EE0EA35EC}")
		)
		: (ReferenceObject
			:Name (Dallas_users)
			:Table (users)
			:Uid ("{988D7B49-57F1-4F1C-9E12-67FAF34148D9}")
		)
		:class (SDBM)
		:color (black)
		:comments ()
		:type (usrgroup)
	)
	: (Dallas_mngt_users
		:AdminInfo (
			:chkpf_uid ("{8F651FB7-850D-442B-865B-9FE1E0AAA7BF}")
			:ClassName (user_group)
			:table (users)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{B35000D1-6836-4019-AA4B-12D38BF99E64}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:groups ()
		:class (SDBM)
		:color (gold)
		:comments ()
		:type (usrgroup)
	)
	: (Business_Partners
		:AdminInfo (
			:chkpf_uid ("{DD58B931-DE7A-43C6-8E51-1F903F2414C5}")
			:ClassName (user_group)
			:table (users)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{FB0DDA8B-D399-4A7C-9568-3FD28A1EF167}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:groups ()
		:class (SDBM)
		:color (black)
		:comments ()
		:type (usrgroup)
	)
	: (Dallas_HR_Admin
		:AdminInfo (
			:chkpf_uid ("{2CD30156-BA5E-4779-B325-581108FEE0B2}")
			:ClassName (user_group)
			:table (users)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{F9DA6348-2638-4975-BAFF-DBBF0D3FC6BD}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:groups ()
		:class (SDBM)
		:color (black)
		:comments ()
		:type (usrgroup)
	)
	: (WIN_Dallas_Sales
		:AdminInfo (
			:chkpf_uid ("{C8300ADF-B0A7-49C4-BEB2-2D30A2E07167}")
			:ClassName (user_group)
			:table (users)
			:object_permissions (
				:AdminInfo (
					:chkpf_uid ("{01764BB4-DB59-4390-BD0A-988BFFF2B9E6}")
					:ClassName (object_permissions)
				)
				:manage (
					: (ReferenceObject
						:Table (globals)
						:Name (Any)
						:Uid ("{97AEB369-9AEA-11D5-BD16-0090272CCB30}")
					)
				)
				:read (
					: (any)
				)
				:use (
					: (any)
				)
				:write (
					: ()
				)
				:owner ()
			)
			:LastModified (
				:Time ("Sun Dec 14 16:17:45 2003")
				:By ("VPN-1 & Firewall-1 SmartCenter Server Upgrade process")
				:From (localhost)
			)
		)
		:groups ()
		:class (SDBM)
		:color (black)
		:comments ()
		:type (usrgroup)
	)
)
