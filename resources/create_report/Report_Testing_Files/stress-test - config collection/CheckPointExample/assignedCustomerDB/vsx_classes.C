# This is a scheme file. Each object in objects.c, rulebases.fws, fwauth.ndb or whatever must
# match one of the classes listed below.
#
# Each class defined here may have the following attributes: 
# abstract		indicates the class is virtual
# cpp_itf       indicates that a C++ interface should be generated for this class (in the ClassGen utility)
# baseobj		classes to inherit fields and table from
# fields		list of field that object of that class have.
# owned         indicates this class is owned by other classes
# querystring	a condition that objects of this class must meet.
# validfunc     a function which validates objects of this class
# 
# Each field have name (may be empty) and a type from fields.c (see fields.c for details).
# 
# the classes are:
#
# vs_slot_objects
# ===============
# vs_slot_base
# vs_slot_obj

# macros:
# int_min   -2147483648
# int_max    2147483647
# uint_max   4294967295
# int64_min -9223372036854775808
# int64_max  9223372036854775807
# uint64_max 18446744073709551615
(

############################################################################
# VS SLOT OBJECTS                                                          #
############################################################################

# owned objects:

	: (route_obj
		:owned ()
		:fields (
			: (source :type (owned_object) :validvalues ("{subnet,NULL}"))
			: (destination	:type (owned_object)	:validvalues ("{subnet,NULL}"))
			: (next_hop			:type (ip_address_or_empty))
			: (metric	:type (uint)	:defvalue (0)	:validvalues (0~10000))
			: (interface_name	:type (string))
			: (export			:type (boolean))
			: (exported_from_vs	:type (int) :defvalue (-1))
			: (source_route_support :type (boolean))
			: (network	:type (linked_object)	:validvalues ("{network,NULL}"))
			: (group	:type (linked_object)	:validvalues ("{network_object_group,NULL}"))
			: (temp_in_db :type (string) :defvalue (none))
			: (automatic :type (boolean))
		)
	)
	: (vs_interface
		:owned ()
		:querystring ("vs_interface = 'true' & !(vsx_interface = 'true') & !(table = 'amz_slots')")
		:baseobj (
			: (subnet)
		)
		:fields (
			: (vs_interface	:type (boolean)	:validvalues (true)	:defvalue (true))		
			: (name	:type (string)) # name of physical interface 
			: (unique_id	:type (owned_object)	:validvalues ("{vlan_id,NULL}"))
			: (mtu	:type (uint)	:defvalue (1500))
			: (nat_hide	:type (boolean))
			: (junction	:type (linked_object)	:validvalues ("{vs_slot_base,NULL}"))
			: (export	:type (boolean))
			: (source_route_support	:type (boolean))
			: (group	:type (linked_object)	:validvalues ("{network_object_group,NULL}"))
			: (temp_in_db :type (string) :defvalue (none))
			: (connected_vs_id :type (int) :defvalue (-1))
			: (automatic :type (boolean))
			: (MAC_id :type (int) :defvalue (-1))
			: (unnumbered_borrow :type (uid_or_empty))
			: (proxy_arp_to	:type (ip_address) :defvalue (0.0.0.0))  #IP address to which proxy ARP should be enabled on this interface
		)
	)
	: (vsx_interface
		:owned ()
		:querystring ("vsx_interface = 'true'")
		:baseobj (
			: (vs_interface)
		)
		:fields (
			: (vsx_interface :type (boolean) :validvalues (true) :defvalue (true))	
			: (automatic_member_ip :type (boolean) :defvalue (true))
		)
	)
	: (vs_slot_resource
		:owned ()
		:fields (
			: (connections_limit	:type (uint)    :validvalues (1000~10000000)	:defvalue (15000))
			: (connections_hashsize	:type (uint)    :validvalues (1024~33554432)  :defvalue (65536))
			: (fw_hmem_size			:type (uint)	:validvalues (2~1000)          :defvalue (6)) # used in lib/fwobj/filter.cc
			: (fw_hmem_maxsize		:type (uint)    :validvalues (2~uint_max)      :defvalue (24))
			: (max_concurrent_vpn_tunnels	:type (int)	:defvalue (200)	:validvalues (200~1000000))
			: (cpu_limit				:type (uint)	:validvalues (0~10000))
			: (bandwidth_limit			:type (int)     :defvalue (0))
			: (bandwidth_guarantee		:type (int)     :defvalue (0))
		)
	)
	: (vlan_id
		:owned ()
		:querystring ("vlan_id = 'true'")
		:fields (
			: (id	:type (uint)	:validvalues (2~4094))
			: (vlan_id	:type (boolean)	:validvalues (true)	:defvalue (true))
		)
	)

  	: (vsx_shadow_bridge
                :owned ()
                :fields (
                        : (id :type (int) :defvalue (-1))
                        : (attached_vlan_trunks :type (string) :multiple (0~int_max))
                        : (vlan_id :type (int) :defvalue (0) :validvalues (0~4094))
                )
        )

	: (vs_operation_status
		:owned ()
		:fields (
			: (operation_type :type (string))
			: (status	:type (string))
		)
	)
# vs slot objects:

	: (vs_slot_base
		:table (vs_slot_objects)
		:querystring ("table = 'vs_slot_objects' & type = 'vs_slot_base' & !(type = 'vs_slot_obj')")
		:fields (
			: (type				    :type (string)	        :validvalues (vs_slot_base)      :defvalue (vs_slot_base))
			: (routes				:type (owned_object)	:validvalues (route_obj)	:ordered (0~int_max))
			: (sic_activation_key	:type (string))
			: (interfaces			:type (owned_object)	:validvalues (vs_interface) :multiple (0~int_max))
			: (mgmt_ip				:type (ip_address_or_empty))  #this will be the same as the main ip of the vs_netobj
			: (mgmt_netmask         :type (net_mask_or_empty)) 
			: (routes_installed	:type (owned_object)	:validvalues (route_obj)	:ordered (0~int_max))	#this field has no gui, internal vsx usage
			: (interfaces_installed	:type (owned_object)	:validvalues (vs_interface)	:multiple (0~int_max)) #this field has no gui, internal vsx usage
			: (owner				:type (linked_object)    :validvalues ("{pv1_customer,NULL}"))
			: (vsx_gateway	:type (linked_object)	:validvalues ("{vsx_netobj,vsx_cluster_netobj,vsx_cluster_member}"))
			: (junction				:type (boolean))
			: (is_bridge			:type (boolean))
			: (vs_uid				:type (uid_or_empty))   # Virtual System unique id
			: (shadow_bridges :type (owned_object) :validvalues (vsx_shadow_bridge) :multiple (0~int_max))
			: (shadow_bridges_installed :type (owned_object) :validvalues (vsx_shadow_bridge) :multiple (0~int_max))
			: (op_status	:type (owned_object)	:validvalues (vs_operation_status)	:multiple (0~int_max))
			: (sic_name		:type (string))
		)
	)
	: (vs_slot_obj
		:querystring ("table = 'vs_slot_objects' & type = 'vs_slot_obj' & !(type = 'vsx_slot_obj')")
		:baseobj (
			: (vs_slot_base)
		)
		:fields (
			: (type				    :type (string)	        :validvalues (vs_slot_obj)      :defvalue (vs_slot_obj))
			: (vsid					:type (int))
			: (vrid					:type (int)) 
			: (resources            :type (owned_object)	:validvalues (vs_slot_resource)	:defvalue (vs_slot_resource))
            : (externaly_managed	:type (boolean)) 
			: (target_customer		:type (string))		
			: (vs_slots_members		:type (linked_object) :validvalues (vs_slot_base)		:ordered (0~int_max))
            : (slot_modified_time	:type (string)) #timestamp of when the object was modified ???
			: (NAT_ip_ranges        :type (owned_object)	:validvalues (first_and_last_IP)	:multiple (0~int_max))
			: (comments		     	:type (string))
			: (masters_addresses	:type (ip_address_or_empty)		:multiple (0~int_max))
		)
	)
	: (vlan_range_vector
		:owned ()
		:querystring ("bits_array = '*'")
		:fields (
			: (vector_size	:type (uint))
			: (bits_array	:type (int)	:ordered (0~int_max))
		)
	)
	: (physical_interface
		:owned ()
		:fields (
			: (name					:type (string))
			: (vlan_support         :type (boolean))
			: (sync_interface       :type (boolean))
			: (available :type (boolean) :defvalue (true) :display_str	("Available for using in VSs")) #Specifies whether the physical interface may be used in a VS definition
			: (vlan_range		:type (owned_object)	:validvalues ("{vlan_range_vector,NULL}"))
			: (mgmt_interface	:type (boolean))
		)
	)
	: (creation_template
     	:owned ()
     	:fields (
          	: (type :type (string) :defvalue (Custom) :validvalues ("{SeparateVS,SharedIF,Custom}"))
          	: (name :type (string))
          	: (external_ifc_name :type (string))
          	: (internal_ifc_name :type (string))
          	: (default_gw :type (ip_address_or_empty))
          	: (external_netmask :type (net_mask_or_empty))
        )
    )	
	: (vsx_slot_obj
		:querystring ("table = 'vs_slot_objects' & type = 'vsx_slot_obj'")
		:baseobj (
			: (vs_slot_obj)
		)
		:fields (
			: (type :type (string) :validvalues (vsx_slot_obj) :defvalue (vsx_slot_obj))
			: (creation_templates :type (owned_object) :validvalues (creation_template) :multiple (0~int_max))
			: (physical_interfaces :type (owned_object) :validvalues (physical_interface) :multiple (0~int_max))
			: (interfaces :type (owned_object) :validvalues (vsx_interface) :multiple (0~int_max))
 			: (interfaces_installed	:type (owned_object) :validvalues (vsx_interface) :multiple (0~int_max))
		)
	)
)
