#############################
# This is a scheme file. Each field type in classes.c must be defined here.
# 
# A field may have the followind attributes::
# defvalue      a value this field accepts by default.
# multiple		range of number of occurences this field may have
# ordered		similar to multiple. indicates ordered container will be created
# size			maximal length of this fields value (for numbers and strings)
# type			boolean, linked-object, member-object, number, owned-object or string 
# validfunc		a function which validates values of this fields.
# validvalues	list or range of values this field can accept as valid.
# empty_val     indicates that the field value can be set to empty 
# 
# field attributes defined here may be override in classes.c, with these exceptions:
# validfunc - can be defined only in fields.c
# multiple, ordered, empty_val - can be defined only in classes.c
# macros:
# int_max    2147483647
# int_min   -2147483648
# uint_max   4294967295
# int64_max  9223372036854775807
# int64_min -9223372036854775808
# uint64_max 18446744073709551615
(
 : (alert_command
		:type (string)
		:size (30)
		:defvalue (none)
		:validvalues ("{none,log,alert,mail,snmptrap,useralert,useralert2,useralert3}")
		:validfunc (validate_str_values)
	)
	: (boolean
		:type (boolean)
		:defvalue (false)
		:validvalues ("{true,false}")
	)
	: (choice
		:type (string)
		:size (8)
		:defvalue (none)
		:validvalues ("{none,known,prompt}")
		:validfunc (validate_str_values)
	)
	: (color
		:type (string)
		:size (50)
		:defvalue (black)
		#:validvalues ("*{black,Medium Slate Blue,Red,Navy Blue,yellow,Dark Green,Dark Orchid,Foreground,FireBrick,Forest Green,Magenta,blue,Cyan,sienna}")
		:validfunc (validate_str_values)
	)
	: (date
		:type (string)
		:size (11)
		:defvalue ()
		:validfunc (validate_date) 
	)
	: (direction
		:type (string)
		:size (16)
		:defvalue (inbound)
		:validvalues ("{inbound,outbound,eitherbound}")
		:validfunc (validate_str_values)
	)
	: (dn
		:type (string)
		:size (256)
		:validfunc (validate_dn) 
		:defvalue ()
	)
	: (espah
		:type (string)
		:size (3)
		:defvalue (ESP)
		:validvalues ("{ESP,AH}")
	)			
	: (filename
		:type (string)
		:size (256)
		:defvalue ()
		:validfunc (validate_filename) 
	)
	: (FWZ_encryption_method
		:type (string)	
		:size (5)
		:defvalue (DES)
		:validvalues ("{FWZ1,DES,CLEAR,Any}")
	)
	: (hex
		:type (string) # Shilo, check this
		:size (16)
		:defvalue (0)
		:validfunc (validate_hex) 
	)
	: (hour
		:type (number)
		:size (1)
		:validvalues (0~23)
		:validfunc (validate_int_ranges)
		:defvalue (0)
	)
	: (installation_status
		:type (string)
		:size (20)
		:defvalue (not-installed)
		:validvalues ("{installed,not-installed}")
		:validfunc (validate_str_values)
	)
	: (int
		:type (number)
		:size (4)
		:defvalue (0)
		:validvalues (int_min~int_max)
		:validfunc (validate_int_ranges)
	)
	: (uint
		:type (u_number)
		:size (4)
		:defvalue (0)
		:validvalues (0~uint_max)
		:validfunc (validate_uint_ranges)
	)
	: (uint_or_empty_string
		:type (string)
		:size (4)
		:defvalue ()
		:validvalues (0~uint_max)
		:validfunc (validate_uint_or_empty_string)
	)
	: (int64
		:type (number64bit)
		:size (8)
		:defvalue (0)
		:validvalues (int64_min~int64_max)
		:validfunc (validate_int64_ranges)
	)
	: (uint64
		:type (u_number64bit)
		:size (8)
		:defvalue (0)
		:validvalues (0~uint64_max)
		:validfunc (validate_uint64_ranges)
	)

	: (ip_address
		:type (string)
		:size (16)
		:defvalue ()
		:validvalues ()
		:validfunc (validate_ipaddr)
	)
	
	: (ip_address_or_empty
		:type (string)
		:size (16)
		:defvalue ()
		:validvalues ()
		:validfunc (validate_ipaddr_or_empty)
	)
	
	: (ipv6_address
		:type (string)
		:size (16)
		:defvalue ()
		:validvalues ()
	)

	: (key
		:type (string)
		:size (200)
		:defvalue ()
		:validfunc (validate_key) 
	)
	: (key_location
		:type (string)
		:defvalue (local)
		:validvalues ("{local,none,remote}")
		:size (6)
		:validfunc (validate_str_values)
	)
	: (key_manager
		:type (member-object)
		:defvalue (NULL)
		:validvalues ("{my_sign,workstation,NULL}") 
		:validfunc (validate_linked_object)
	)
	: (MAC_address_or_empty
		:type (string)
		:size (17)
		:defvalue ()
		:validvalues ()
		:validfunc (validate_str_values_or_empty)
	)
	: (md5/sha1
		:type (string)
		:size (4)
		:defvalue (MD5)
		:validvalues ("{MD5,SHA1}")
		:validfunc (validate_str_values)
	)
	: (member_object
		:type (member-object)
		:defvalue (NULL)
		:validfunc (validate_member_object)
	)
	: (minute
		:type (number)
		:size (1)
		:defvalue (0)
		:validvalues (0~59)
		:validfunc (validate_int_ranges)
	)
	: (net_mask
		:type (string)
		:size (16)
		:validvalues ()
		:validfunc (validate_netmask)
		:defvalue ()
	)
	: (net_mask_or_empty
		:type (string)
		:size (16)
		:defvalue ()
		:validvalues ()
		:validfunc (validate_netmask_or_empty)
	)
	: (non_empty_string
		:type (string)
		:size (256)
		:defvalue ()
		:validvalues ()
		:validfunc (validate_non_empty_string)
	)
	: (owned_object
		:type (owned-object)
		:validfunc (validate_object_class)
		:defvalue (NULL)
	)
	: (permissions
		:type (string)
		:size (14)
		:defvalue (read_only)
		:validvalues ("{read_and_write,read_only}")
		:validfunc (validate_str_values) 
	)
	: (ports_range	
		:type (string)
		:size (32)
		:validfunc (validate_ports_range) 
		:defvalue ()
	)
	: (ports_range_or_empty
		:type (string)
		:size (32)
		:validfunc (validate_ports_range_or_empty) 
		:defvalue ()
	)
	: (position
		:type (string)
		:size (13)
		:defvalue (first)
		:validvalues ("{first,last,before last}")
		:validfunc (validate_str_values) 
	)
	: (priority
		:type (number)
		:size (1)
		:defvalue (1)
		:validvalues (1~100)
		:validfunc (validate_int_ranges)
	)
	: (rate
		:type (string)
		:size (20)
		:defvalue (192000)		
		:validfunc (validate_str_values) #list of integers but treated as string
	)
	: (linked_object
		:type (linked-object)
		:defvalue (NULL)
		:validfunc (validate_linked_object)
	)
	: (rule_type
		:type (string)	
		:size (20)
		:defvalue ()
		:validvalues ("*{Standard Sign-on,Specific Sign-on,standard sign on,specific sign on}") 
		:validfunc (validate_str_values_or_empty)
	)
	: (ssl_strength
		:type (string)
		:size (6)
		:validvalues ("{auth,export,strong}")
		:validfunc (validate_str_values)
		:defvalue (auth)
	)
#: (string256
#		:type (string)
#		:size (256)
#		:defvalue ()
#		:validvalues ()
#		:validfunc (validate_str_values)
#	)
	: (string
		:type (string)
		:size (4)
		:defvalue ()
		:validvalues ()
		:validfunc (validate_str_values)
	)
	: (string_or_empty
		:type (string)
		:size (80)
		:defvalue ()
		:validvalues ()
		:validfunc (validate_str_values_or_empty)
	)
	: (string_15_digit
		:type (string)
		:size (15)
		:defvalue (1)
		:validfunc (validate_str_15_digits)
	)
	: (string_for_citrix_app_name
		:type (string)
		:size (39)
		:defvalue ()
		:validvalues ()
		:validfunc (validate_str_values)
	)
	: (string_non_validate
		:type (string)
		:size (4)
		:defvalue ()
		:validvalues ()
	)
	: (time
		:type (string)
		:size (7)
		:validfunc (validate_time) 
		:defvalue ()	
	)
	: (track
		:type (linked-object)
		:defvalue (tracks,None)
		:validvalues ("{empty_track,log,alert,account}")
		:validfunc (validate_linked_object)
		:ref (4)
	)
	: (uid
		:type (string)
		:size (40)
		:validfunc (validate_uid) 
		:defvalue ()
	)
	: (uid_or_empty
		:type (string)
		:size (40)
		:validfunc (validate_str_values_or_empty) 
		:defvalue ()
	)
	: (user_db_option
		:type (string)	
		:size (30)
		:defvalue ("Intersect with User Database")
		:validvalues ("*{intersect with user database,ignore user database}")
		:validfunc (validate_str_values)
	)
	: (user_password
		:type (string)
		:size (8)
		:defvalue ()
		:validfunc (validate_user_password) 
	)
	: (regular_expression
		:type (string)
		:size (256)
		:defvalue ()
		:validvalues ()
		:validfunc (validate_regular_expression)
	)
	: (amz_action
		:type (string)
		:size (15)
		:defvalue (inspect)
		:validvalues ("{allow,inspect,block,quarantine}")
	)
	: (edge_conf_script
		:type (string)
		:defvalue ()
		:validvalues ()
		:validfunc ("{476EAA70-F8BD-41d9-A253-C781037E4933}")
	)
)
