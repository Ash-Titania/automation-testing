(
	:version (5.9)
	: (ApplicationEnginesList
		:AdminInfo (
			:chkpf_uid ("{824A24FD-A63E-4718-951B-CCF3F8D8FAA6}")
			:ClassName (defaults_application_engines_list)
			:table (defaults_for_reset)
			:LastModified (
				:Time ("Thu Jun 03 13:26:47 2004")
				:By ()
				:From (mbrown)
			)
		)
		:type (application_engines_list)
		:application_engines (
			: (
				:AdminInfo (
					:chkpf_uid ("{FB8B8189-2E40-4E5E-8C38-0C8FCFC5B331}")
					:ClassName (application_engine)
				)
				:application_engine_error_concealment (
					:AdminInfo (
						:chkpf_uid ("{42C36846-C972-4422-8920-4974A5A2378B}")
						:ClassName (application_engine_error_concealment)
					)
					:error_concealment_patterns_list (
						: (
							:AdminInfo (
								:chkpf_uid ("{BBBFDDB5-109B-470D-92D2-20F36B369CCC}")
								:ClassName (error_concealment_engine_pattern)
							)
							:engine_pattern_cp_cmd (true)
							:engine_pattern_enforce (true)
							:engine_pattern_max_distance (500)
							:engine_pattern_name ("HTTP, 500.100, Internal, Server, Error, ASP, error, <br>, Internet, Information, Services")
						)
					)
					:enforce_application_engine_patterns (true)
				)
				:application_engine_name (ASP)
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{6EAC26AA-B81E-444D-A8DC-5528BC5F9B14}")
					:ClassName (application_engine)
				)
				:application_engine_error_concealment (
					:AdminInfo (
						:chkpf_uid ("{1229D943-702E-4E0E-9EAD-46AC065B5C76}")
						:ClassName (application_engine_error_concealment)
					)
					:error_concealment_patterns_list (
						: (
							:AdminInfo (
								:chkpf_uid ("{DD9C67CF-B2A1-4879-8B4E-14D1D1D1E754}")
								:ClassName (error_concealment_engine_pattern)
							)
							:engine_pattern_cp_cmd (true)
							:engine_pattern_enforce (true)
							:engine_pattern_max_distance (500)
							:engine_pattern_name ("Server, Error, in, Application., <b>, Description:, </b>, <b>, Exception Details:, </b>, <b>, Source Error:, </b>, <b>, Stack Trace:, </b>")
						)
					)
					:enforce_application_engine_patterns (true)
				)
				:application_engine_name (ASPX)
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{99A18762-82D4-4E73-9A74-DDEF092F1863}")
					:ClassName (application_engine)
				)
				:application_engine_error_concealment (
					:AdminInfo (
						:chkpf_uid ("{D066497C-4E5A-4E09-95F2-0015D5171ADD}")
						:ClassName (application_engine_error_concealment)
					)
					:error_concealment_patterns_list (
						: (
							:AdminInfo (
								:chkpf_uid ("{78520B9C-91D4-4831-8B61-7611D351AB46}")
								:ClassName (error_concealment_engine_pattern)
							)
							:engine_pattern_cp_cmd (true)
							:engine_pattern_enforce (true)
							:engine_pattern_max_distance (500)
							:engine_pattern_name ("Internal, Server, Error")
						)
					)
					:enforce_application_engine_patterns (true)
				)
				:application_engine_name (General)
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{1C5F6E9F-25F9-40d6-8641-B31A5D602C9D}")
					:ClassName (application_engine)
				)
				:application_engine_error_concealment (
					:AdminInfo (
						:chkpf_uid ("{23DFBA2C-A157-4c43-B575-E12BF25DA66A}")
						:ClassName (application_engine_error_concealment)
					)
					:error_concealment_patterns_list (
						: (
							:AdminInfo (
								:chkpf_uid ("{7245ADDA-E6EE-4fce-A9C7-4E9BB4ADDF21}")
								:ClassName (error_concealment_engine_pattern)
							)
							:engine_pattern_cp_cmd (true)
							:engine_pattern_enforce (true)
							:engine_pattern_max_distance (500)
							:engine_pattern_name ("Microsoft, JET, Database, Engine, error")
						)
					)
					:enforce_application_engine_patterns (true)
				)
				:application_engine_name (JET)
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{85C1FA97-827C-43C5-95EA-CD4F1DBE2445}")
					:ClassName (application_engine)
				)
				:application_engine_error_concealment (
					:AdminInfo (
						:chkpf_uid ("{DE1AE032-061E-4FF2-B9ED-FD62367B99E7}")
						:ClassName (application_engine_error_concealment)
					)
					:error_concealment_patterns_list (
						: (
							:AdminInfo (
								:chkpf_uid ("{9F3988F4-12B8-427F-9704-948A15224539}")
								:ClassName (error_concealment_engine_pattern)
							)
							:engine_pattern_cp_cmd (true)
							:engine_pattern_enforce (true)
							:engine_pattern_max_distance (500)
							:engine_pattern_name ("<p>, Microsoft, OLE, DB, Provider, for, ODBC, Drivers, </font>, error")
						)
					)
					:enforce_application_engine_patterns (true)
				)
				:application_engine_name (ODBC)
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{42656C48-7462-4468-A29C-1BE0E01B0DE3}")
					:ClassName (application_engine)
				)
				:application_engine_error_concealment (
					:AdminInfo (
						:chkpf_uid ("{ADF23788-7378-49be-8715-CEE777E5C101}")
						:ClassName (application_engine_error_concealment)
					)
					:error_concealment_patterns_list (
						: (
							:AdminInfo (
								:chkpf_uid ("{8AF03FE1-00D1-4f1d-BAA2-01D638F6AC42}")
								:ClassName (error_concealment_engine_pattern)
							)
							:engine_pattern_cp_cmd (true)
							:engine_pattern_enforce (true)
							:engine_pattern_max_distance (500)
							:engine_pattern_name ("<p>, Microsoft, OLE, DB, Provider, for, SQL, Server")
						)
					)
					:enforce_application_engine_patterns (true)
				)
				:application_engine_name ("SQL Server")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{FE9A1F4D-7016-4D4D-8322-CFF044600105}")
					:ClassName (application_engine)
				)
				:application_engine_error_concealment (
					:AdminInfo (
						:chkpf_uid ("{5A683911-B255-4EFA-B5E1-3DC36968D82B}")
						:ClassName (application_engine_error_concealment)
					)
					:error_concealment_patterns_list (
						: (
							:AdminInfo (
								:chkpf_uid ("{0A044F7F-AF36-4526-86EA-2A65F9CD25A3}")
								:ClassName (error_concealment_engine_pattern)
							)
							:engine_pattern_cp_cmd (true)
							:engine_pattern_enforce (true)
							:engine_pattern_max_distance (500)
							:engine_pattern_name ("ORA-[0-9][0-9][0-9][0-9][0-9]:")
						)
					)
					:enforce_application_engine_patterns (true)
				)
				:application_engine_name (Oracle)
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{132BC862-3708-428D-97F6-55C273F2D59A}")
					:ClassName (application_engine)
				)
				:application_engine_error_concealment (
					:AdminInfo (
						:chkpf_uid ("{FB33B9C8-C600-4A3D-B2A3-9A74D655F3D5}")
						:ClassName (application_engine_error_concealment)
					)
					:error_concealment_patterns_list (
						: (
							:AdminInfo (
								:chkpf_uid ("{033AA8FE-9D4E-4E95-A7C1-27B205EB7239}")
								:ClassName (error_concealment_engine_pattern)
							)
							:engine_pattern_cp_cmd (true)
							:engine_pattern_enforce (true)
							:engine_pattern_max_distance (500)
							:engine_pattern_name ("Can't, locate, in")
						)
					)
					:enforce_application_engine_patterns (true)
				)
				:application_engine_name (Perl)
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{0379C96C-82BC-48e7-B12D-536AE9B09AF5}")
					:ClassName (application_engine)
				)
				:application_engine_error_concealment (
					:AdminInfo (
						:chkpf_uid ("{C9616709-F89B-41A9-8CC7-1C0271AE75AC}")
						:ClassName (application_engine_error_concealment)
					)
					:error_concealment_patterns_list (
						: (
							:AdminInfo (
								:chkpf_uid ("{88084615-E212-4EC9-8042-0FF63D48254A}")
								:ClassName (error_concealment_engine_pattern)
							)
							:engine_pattern_cp_cmd (true)
							:engine_pattern_enforce (true)
							:engine_pattern_max_distance (500)
							:engine_pattern_name ("<b>, error, </b>, on, line")
						)
						: (
							:AdminInfo (
								:chkpf_uid ("{F6BB9156-8FCB-45DC-BE06-4A0FBEC5EB62}")
								:ClassName (error_concealment_engine_pattern)
							)
							:engine_pattern_cp_cmd (true)
							:engine_pattern_enforce (true)
							:engine_pattern_max_distance (500)
							:engine_pattern_name ("<b>, warning, </b>, on, line")
						)
					)
					:enforce_application_engine_patterns (true)
				)
				:application_engine_name (PHP)
			)
		)
	)
	: (HttpErrorConcealmentStatusCodes
		:AdminInfo (
			:chkpf_uid ("{1264043F-CD6C-432F-AE40-94AF88B235AC}")
			:ClassName (defaults_http_error_concealment_codes_list)
			:table (defaults_for_reset)
			:LastModified (
				:Time ("Thu Jun 03 13:50:23 2004")
				:By ()
				:From (mbrown)
			)
		)
		:type (http_error_concealment_codes_list)
		:response_status_codes (
			: (
				:AdminInfo (
					:chkpf_uid ("{0EF66305-0F7B-4ABC-BAA0-84EA38D257A2}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (422)
				:status_code_enforce (false)
				:status_code_description ("Unprocessable Entity")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{AF7E9DF0-4065-46C6-8588-5C90B99525AC}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (423)
				:status_code_enforce (false)
				:status_code_description (Locked)
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{356B7D84-4CC1-4169-848E-24D064F3F225}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (424)
				:status_code_enforce (false)
				:status_code_description ("Method Failure")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{EC7EE613-CBAA-4373-AE2A-E6849FC407FB}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (425)
				:status_code_enforce (false)
				:status_code_description ("Insufficient Space on Resource")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{88032378-6683-4Ef9-9A86-CAAC40D02BF1}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (506)
				:status_code_enforce (false)
				:status_code_description ("Loop Detected, Variant Also Varies, Partial Update Not Implemented, Redirection Failed")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{F76CE624-59A0-4D59-972F-0F5A39579F6D}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (507)
				:status_code_enforce (false)
				:status_code_description ("Insufficient Storage")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{266279A7-E861-43B5-BCAD-8B863C285504}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (510)
				:status_code_enforce (false)
				:status_code_description ("Not Extended")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{FB5AB68F-F955-43EB-9B22-121B34229DCA}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (410)
				:status_code_enforce (false)
				:status_code_description (Gone)
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{F9C39D0B-FBB2-4DF4-8B67-5D3378E0A858}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (411)
				:status_code_enforce (false)
				:status_code_description ("Length Required")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{E9769E68-7F0C-44D5-8502-6194DA54491A}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (400)
				:status_code_enforce (false)
				:status_code_description ("Bad Request")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{D6368A42-B07D-4FF2-BDF3-D1678D02B34E}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (500)
				:status_code_enforce (true)
				:status_code_description ("Internal Server Error")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{09E996EF-67E6-41DE-9895-12A71B27D2F7}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (412)
				:status_code_enforce (false)
				:status_code_description ("Precondition Failed")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{4254C3CA-92F3-4D6E-ABA9-2FF7CEA3DA4C}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (401)
				:status_code_enforce (false)
				:status_code_description (Unauthorized)
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{182EF805-1DF5-4FF5-9B60-827265F97F78}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (501)
				:status_code_enforce (true)
				:status_code_description ("Not Implemented")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{E3EAB590-DC07-4BF1-BF89-3B84991CC047}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (413)
				:status_code_enforce (true)
				:status_code_description ("Request Entity Too Large")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{0C47FA96-98DA-4b3A-BE4C-C280FC541231}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (402)
				:status_code_enforce (false)
				:status_code_description ("Payment Required")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{C46DEC89-3C20-4CA6-B084-F9F3C3EDAB5A}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (502)
				:status_code_enforce (true)
				:status_code_description ("Bad Gateway")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{EACA7A3C-B88C-4634-B67D-4B003B6F265E}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (414)
				:status_code_enforce (true)
				:status_code_description ("Request-URI Too Long")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{ECC37932-9156-45C1-8EE0-B201DE320DA2}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (403)
				:status_code_enforce (false)
				:status_code_description (Forbidden)
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{E58BA3BF-A239-4EFd-9840-5EC036A79D23}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (503)
				:status_code_enforce (true)
				:status_code_description ("Service Unavailable")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{7750799F-DDA6-410E-B06A-D766FDEEE797}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (415)
				:status_code_enforce (true)
				:status_code_description ("Unsupported Media Type")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{3564E657-D8F7-4249-BDE4-8E1D69C7C266}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (404)
				:status_code_enforce (false)
				:status_code_description ("Not Found")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{E83FB3DE-1D76-4A83-BD53-1F2514F4F105}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (504)
				:status_code_enforce (false)
				:status_code_description ("Gateway Timeout")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{44889309-9F8A-47D9-9EFD-B575E7FD4D08}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (416)
				:status_code_enforce (true)
				:status_code_description ("Requested Range Not Satisfiable")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{565C56E6-4842-4CB2-81C6-FE9F31B6E10C}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (405)
				:status_code_enforce (false)
				:status_code_description ("Method Not Allowed")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{F56EA961-033B-4E45-BFF9-9305F09C2B0D}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (505)
				:status_code_enforce (false)
				:status_code_description ("HTTP Version Not Supported")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{4688AACC-69DD-4DF7-8314-8FB1B5A85554}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (417)
				:status_code_enforce (true)
				:status_code_description ("Expectation Failed")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{12CF88ED-FD83-41fA-BDE6-18ED2C74C902}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (406)
				:status_code_enforce (false)
				:status_code_description ("Not Acceptable")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{90D857A8-9847-44CD-AA81-CB211C642655}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (407)
				:status_code_enforce (false)
				:status_code_description ("Proxy Authentication Required")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{EDA7D242-9DD0-4705-99E3-E4CF913C22DB}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (408)
				:status_code_enforce (false)
				:status_code_description ("Request Timeout")
			)
			: (
				:AdminInfo (
					:chkpf_uid ("{D6437E69-EF3D-4897-A8BA-C968CC0941D6}")
					:ClassName (http_error_concealment_status_code)
				)
				:status_code_name (409)
				:status_code_enforce (false)
				:status_code_description (Conflict)
			)
		)
	)
)
