(
	:schema_major_ver (6)
	:schema_minor_ver (4)
	:classes (
			: (classes.C
				:mandatory (true)
			)
			: (rtmclasses.C
				:mandatory (true)
			)
			: (vsx_classes.C
				:mandatory (true)
			)
			: (commands_classes.C
				:mandatory (false)
			)
	)
	:fields (
			: (fields.C
				:mandatory (true)
			)
	)
	:tables (
			: (tables.C)
	)
)
