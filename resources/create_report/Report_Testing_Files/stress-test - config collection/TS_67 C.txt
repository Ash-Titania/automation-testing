Total Config size 3202:
set auth-server "Local" id 0
set auth-server "Local" server-name "Local"
set auth default auth server "Local"
set clock "timezone" 0
set admin format dos
set admin name "admin"
set admin password nMjFM0rdC9iOc+xIFsGEm3LtAeGZhn
set admin user "rolfharris" password "nCLcLyrwHnBJcfeP4sII23At4RBQpn" privilege                                                                                                                                                              "read-only"
set admin auth timeout 10
set admin auth server "Local"
set admin auth banner telnet login "TS_67"
set vrouter trust-vr sharable
unset vrouter "trust-vr" auto-route-export
set zone "Trust" vrouter "trust-vr"
set zone "Untrust" vrouter "trust-vr"
set zone "Trust" tcp-rst
set zone "Untrust" block
unset zone "Untrust" tcp-rst
set zone "MGT" block
set zone "MGT" tcp-rst
set zone Trust screen icmp-flood
--- more ---
set zone Trust screen ip-spoofing
set zone Trust screen land
set zone Trust screen mal-url code-red
set zone Untrust screen tear-drop
set zone Untrust screen syn-flood
set zone Untrust screen ping-death
set zone Untrust screen ip-filter-src
set zone Untrust screen land
set zone V1-Untrust screen tear-drop
set zone V1-Untrust screen syn-flood
set zone V1-Untrust screen ping-death
set zone V1-Untrust screen ip-filter-src
set zone V1-Untrust screen land
set interface "trust" zone "Trust"
set interface "untrust" zone "Untrust"
unset interface vlan1 ip
set interface trust ip 10.200.4.130/24
set interface trust nat
unset interface vlan1 bypass-others-ipsec
unset interface vlan1 bypass-non-ip
set interface trust manage-ip 10.200.4.131
set interface vlan1 ip manageable
--- more ---
unset interface trust ip manageable
set interface untrust ip manageable
set interface trust dhcp server service
set interface trust dhcp server option gateway 192.168.1.1
set interface trust dhcp server option netmask 255.255.255.0
set interface trust dhcp server ip 192.168.1.33 to 192.168.1.126
set flow tcp-mss
set hostname ns5xp
set address "Trust" "10.200.10.200/24" 10.200.10.200 255.255.255.0
set address "Trust" "192.168.1.1/10" 192.168.1.1 255.192.0.0
set address "Untrust" "10.200.20.156/20" 10.200.20.156 255.255.240.0
set address "Untrust" "192.168.10.120/21" 192.168.10.120 255.255.248.0
set snmp name "ns5xp"
set ike policy-checking
set ike respond-bad-spi 1
set ike id-mode subnet
set xauth lifetime 480
set xauth default auth server Local
set policy id 0 from "Trust" to "Untrust"  "Any" "Any" "ANY" Permit log
set policy id 1 from "Trust" to "Untrust"  "Any" "Any" "ICMP-INFO" Permit log
set policy id 2 from "Trust" to "Untrust"  "Any" "Any" "TELNET" nat dip-id 2 Per                                                                                                                                                             mit WebAuth log
set policy id 3 from "Trust" to "Untrust"  "10.200.10.200/24" "Any" "NTP" nat di                                                                                                                                                             p-id 2 Permit WebAuth log
--- more ---
set policy id 4 from "Trust" to "Untrust"  "10.200.10.200/24" "10.200.20.156/20"                                                                                                                                                              "ANY" Permit
set policy id 5 from "Trust" to "Untrust"  "192.168.1.1/10" "192.168.10.120/21"                                                                                                                                                              "POP3" Deny
set pki authority default scep mode "auto"
set pki x509 default cert-path "partial"
set global-pro policy-manager primary outgoing-interface untrust
set global-pro policy-manager secondary outgoing-interface untrust
set ssl encrypt 3des sha-1
set vrouter "untrust-vr"
exit
set vrouter "trust-vr"
unset add-default-route
exit
