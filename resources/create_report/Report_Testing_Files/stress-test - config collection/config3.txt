> export
# Username
# Password
#         
# ########################################
# HP JetDirect      J6057A                
# Firmware Version  R.25.09               
# Manufacturing ID  22014412902201        
# Hardware Address  00:30:6E:FE:25:21     
# System Up Time    353:23:05             
# ########################################
#   Edit this file then send to a compatible
#   HP JetDirect product via TELNET or      
#   BOOTP/TFTP to configure its parameters. 
#                                           
#   Comments have a "# " in columns 1 and 2 
#   1 = enable, 0 = disable                 
# ########################################  
#     GENERAL____________________________________
# passwd            Not Specified                
sys-location                                     
sys-contact                                      
ssl-state         2                              
security-reset    0                              
#                                                
#     TCP/IP MAIN________________________________
host-name         wetherby                       
ip-config         DHCP                           
ip                10.0.1.13                      
# subnet-mask       255.255.255.0                
# default-gw        10.0.1.253                   
# DHCP Server:      10.0.0.15                    
# tftp-server       0.0.0.0                      
# tftp-filename                                  
parm-file                                        
# domain-name       uk365office.co.uk            
dns-svr           10.0.0.15                      
# pri-wins-svr      10.0.0.15                    
sec-wins-svr      0.0.0.0                        
#                                                
#     TCP/IP PRINT OPTIONS_______________________
9100-printing     1                              
ftp-printing      1                              
ipp-printing      1                              
lpd-printing      1                              
banner            1                              
#                                                
addq              ???                            
defaultq          AUTO                           
addstring         ???                            
#                                                
#     TCP/IP RAW PRINT PORTS_____________________
raw-port                                         
#                                                
#     TCP/IP ACCESS CONTROL______________________
allow                                            
#                                                
#     TCP/IP OTHER_______________________________
syslog-config     1                              
syslog-svr        0.0.0.0                        
syslog-max        10                             
syslog-priority   7                              
slp-config        1                              
mdns-config       1                              
mdns-service-name hp LaserJet 2300 series (00306EFE2521)
# mdns-domain-name  wetherby.local.                     
mdns-pri-svc      1                                     
ttl-slp           4                                     
ipv4-multicast    1                                     
idle-timeout      270                                   
user-timeout      900                                   
cold-reset        0                                     
ews-config        1                                     
tcp-mss           0                                     
default-ip        Auto IP                               
default-ip-dhcp   1                                     
#                                                       
#     SNMP_______________________________________       
snmp-config       1                                     
# get-cmnty-name    Not Specified                       
# set-cmnty-name    Not Specified                       
default-get-cmnty 1                                     
#                                                       
#     SNMP TRAPS_________________________________       
auth-trap         Enabled                               
trap-dest         ???                                   
#                                                       
#     IPX/SPX____________________________________       
ipx-config        0                                     
ipx-unitname      NPIFE2521                             
# ipx-netnum        0.00306EFE2521                      
ipx-frametype     AUTO                                  
ipx-sapinterval   60
# ipx-mode          NONE
#
ipx-nds-tree
ipx-nds-context
ipx-job-poll      2
pjl-banner        0
pjl-eoj           0
pjl-toner-low     0
#
#     APPLETALK__________________________________
appletalk         0
# name
# at-zone           *
# at-type1          HP LaserJet
# at-type2          LaserWriter
# at-type3
# Phase             2
# at-dev-status     NOT IN USE
#
#     DLC/LLC____________________________________
dlc/llc-config    1
strict-8022       0
#
#     OTHER______________________________________
panic-behavior    DUMP_AND_HALT
#     OTHER______________________________________
link-type         AUTO
laa               00306EFE2521
#
#     SUPPORT____________________________________
support-contact
support-number
support-url       http://www.hp.com/go/jetdirect
tech-support-url  http://www.hp.com/go/support
#
# save
# Y

>

