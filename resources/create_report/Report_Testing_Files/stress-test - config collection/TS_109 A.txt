: Saved
:
PIX Version 8.0(4)28 <system>
!
hostname testpix
domain-name titania.local
enable password 8Ry2YjIyt7RRXU24 encrypted
no mac-address auto
!
interface Ethernet0
 shutdown
!
interface Ethernet1
!
interface Ethernet2
 shutdown
!
interface Ethernet3
 shutdown
!
interface Ethernet4
 shutdown
!
interface Ethernet5
 shutdown
!
class default
  limit-resource All 0
  limit-resource ASDM 5
  limit-resource SSH 5
  limit-resource Telnet 5
!

ftp mode passive
pager lines 24
no failover
asdm history enable
arp timeout 14400
console timeout 0

admin-context admin
context admin
  description Admin Context
  allocate-interface Ethernet0
  allocate-interface Ethernet1
  allocate-interface Ethernet2
  config-url flash:/admin.cfg
!

context Something
  allocate-interface Ethernet3
  allocate-interface Ethernet4
  allocate-interface Ethernet5
  config-url flash:/another.cfg
!

username admin password 7KKG/zg/Wo8c.YfN encrypted privilege 15
prompt hostname context
Cryptochecksum:55d160b36ff9704d669e66ef3158379b
: end