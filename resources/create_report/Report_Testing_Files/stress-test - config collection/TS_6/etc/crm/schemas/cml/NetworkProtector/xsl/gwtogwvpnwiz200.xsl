<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fwm="http://www.iss.net/cml/NetworkProtector/fwm" xmlns:npcommon="http://www.iss.net/cml/NetworkProtector/common" xmlns:wiz="http://www.iss.net/cml/NetworkProtector/vpnwizgwgw">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="wizxml" select=" 'vpnwizgwgw1_0.xml' "/>
	<xsl:variable name="vpnwiz" select="document($wizxml)/wiz:policy"/>
	<xsl:template match="fwm:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:call-template name="insertaccessrules"/>
			<xsl:apply-templates select="fwm:Policy"/>
			<xsl:call-template name="insertipsecrules"/>
			<xsl:apply-templates select="fwm:IPSECRules"/>
			<xsl:apply-templates select="fwm:CommonLists"/>
			<xsl:choose>
				<xsl:when test="count(fwm:SecurityGateways)>0">
					<xsl:apply-templates select="fwm:SecurityGateways"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:element name="SecurityGateways" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:call-template name="insertsecuritygw"/>
					</xsl:element>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="fwm:MessageConfig"/>
			<xsl:apply-templates select="fwm:ProxyRedirectRule"/>
			<xsl:apply-templates select="fwm:VpnAdvancedSettings"/>
			<xsl:apply-templates select="fwm:TuningSetting"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:param name="ruleType"/>
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*">
					<xsl:with-param name="ruleType" select="$ruleType"/>
				</xsl:apply-templates>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<xsl:template match="fwm:IPSECRules">
		<xsl:element name="IPSECRules" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="RuleId"><xsl:value-of select="position()+2"/></xsl:attribute>
			<xsl:attribute name="GUID">
				<xsl:choose>
					<xsl:when test="string-length(@GUID) > 0"><xsl:value-of select="@GUID"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="generate-id(node())"/></xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:copy-of select="@*[name() != 'RuleId' and name() != 'GUID']"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:Policy">
		<!-- Enable the ISAKAMP Self policy if it is not enabled already -->
		<xsl:element name="Policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="RuleId"><xsl:value-of select="position()+2"/></xsl:attribute>
			<xsl:attribute name="GUID">
				<xsl:choose>
					<xsl:when test="string-length(@GUID) > 0"><xsl:value-of select="@GUID"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="generate-id(node())"/></xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="fwm:DestPort/fwm:Ports/fwm:PortEntry/fwm:PortName/@value = 'ISAKMP_UDP' ">
					<xsl:attribute name="Enabled">true</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="Enabled"><xsl:value-of select="@Enabled"/></xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:copy-of select="@*[name() != 'Enabled'  and name() != 'RuleId' and name() != 'GUID']"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:GlobalNatBindList">
		<xsl:element name="GlobalNatBindList" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="RuleId"><xsl:value-of select="position()+1"/></xsl:attribute>
			<xsl:attribute name="GUID">
				<xsl:choose>
					<xsl:when test="string-length(@GUID) > 0"><xsl:value-of select="@GUID"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="generate-id(node())"/></xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:copy-of select="@*[name() != 'RuleId' and name() != 'GUID']"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>	
	<xsl:template match="fwm:SecurityGateways">
		<xsl:element name="SecurityGateways" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:call-template name="insertsecuritygw"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:CommonLists">
		<xsl:element name="CommonLists" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:call-template name="insertsnatrules"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template name="insertaccessrules">
		<!-- Insert necessary access rules based on values in the vpn wizard policy -->
		<xsl:element name="Policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="RuleId">0</xsl:attribute>
			<xsl:attribute name="GUID"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@GUID1"/></xsl:attribute>
			<xsl:attribute name="Enabled">true</xsl:attribute>
			<xsl:attribute name="Action">Allow</xsl:attribute>
			<xsl:attribute name="LogEnabled"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@LogEnabled"/></xsl:attribute>
			<xsl:attribute name="Comment"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@Comment"/></xsl:attribute>
			<xsl:element name="Protocol" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyProto" namespace="http://www.iss.net/cml/NetworkProtector/fwm" />
			</xsl:element>
			<xsl:element name="SourceAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:apply-templates select="$vpnwiz/wiz:DestinationSettings/wiz:DestAddress/*">
					<xsl:with-param name="ruleType" select="'Access'"/>
				</xsl:apply-templates>
			</xsl:element>
			<xsl:element name="SourcePort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
			<xsl:element name="DestAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:apply-templates select="$vpnwiz/wiz:SourceSettings/wiz:SourceAddress/*">
					<xsl:with-param name="ruleType" select="'Access'"/>
				</xsl:apply-templates>
			</xsl:element>
			<xsl:element name="DestPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
		</xsl:element>
		<xsl:element name="Policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="RuleId">1</xsl:attribute>
			<xsl:attribute name="GUID"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@GUID2"/></xsl:attribute>
			<xsl:attribute name="Enabled">true</xsl:attribute>
			<xsl:attribute name="Action">Allow</xsl:attribute>
			<xsl:attribute name="LogEnabled"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@LogEnabled"/></xsl:attribute>
			<xsl:attribute name="Comment"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@Comment"/></xsl:attribute>
			<xsl:element name="Protocol" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyProto" namespace="http://www.iss.net/cml/NetworkProtector/fwm" />
			</xsl:element>
			<xsl:apply-templates select="$vpnwiz/wiz:SourceSettings/wiz:SourceAddress">
				<xsl:with-param name="ruleType" select="'Access'"/>
			</xsl:apply-templates>
			<xsl:element name="SourcePort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
			<xsl:apply-templates select="$vpnwiz/wiz:DestinationSettings/wiz:DestAddress">
				<xsl:with-param name="ruleType" select="'Access'"/>
			</xsl:apply-templates>
			<xsl:element name="DestPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
		</xsl:element>
		<xsl:element name="Policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="RuleId">2</xsl:attribute>
			<xsl:attribute name="GUID"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@GUID3"/></xsl:attribute>
			<xsl:attribute name="Enabled">true</xsl:attribute>
			<xsl:attribute name="Action">Allow</xsl:attribute>
			<xsl:attribute name="LogEnabled"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@LogEnabled"/></xsl:attribute>
			<xsl:attribute name="Comment"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@Comment"/></xsl:attribute>
			<xsl:element name="Protocol" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyProto" namespace="http://www.iss.net/cml/NetworkProtector/fwm" />
			</xsl:element>
			<xsl:element name="SourceAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:choose>
					<xsl:when test="count($vpnwiz/wiz:VPNSettings/wiz:ExistSecGw)>0">
						<xsl:variable name="SecGw" select="/fwm:policy/fwm:SecurityGateways/fwm:AutoIkeSecurityGatewayList[@Name=$vpnwiz/wiz:VPNSettings/wiz:ExistSecGw/wiz:SecGw/@value]"/>
						<xsl:variable name="Address" select="$SecGw/fwm:IkeConfig/fwm:RemoteIpAddress" />
						<xsl:call-template name="insert-policy-address">
							<xsl:with-param name="Address" select="$Address"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="Address" select="$vpnwiz/wiz:VPNSettings/wiz:NewSecGw/wiz:Addresses/wiz:RemoteIpAddress" />
						<xsl:call-template name="insert-policy-address">
							<xsl:with-param name="Address" select="$Address"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
			<xsl:element name="SourcePort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
			<xsl:element name="DestAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:apply-templates select="$vpnwiz/wiz:SourceSettings/wiz:SourceAddress/*">
					<xsl:with-param name="ruleType" select="'Access'"/>
				</xsl:apply-templates>
			</xsl:element>
			<xsl:element name="DestPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
		</xsl:element>		
	</xsl:template>
	<xsl:template name="insertipsecrules">
		<!-- Insert ipsec rules based on values in the vpn wizard policy. -->
		<xsl:element name="IPSECRules" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="Name"><xsl:value-of select="concat($vpnwiz/wiz:GeneralSettings/@Name,'_1')"/></xsl:attribute>
			<xsl:attribute name="GUID"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@GUID1"/></xsl:attribute>
			<xsl:attribute name="RuleId">0</xsl:attribute>
			<xsl:attribute name="Enabled">true</xsl:attribute>
			<xsl:attribute name="Comment"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@Comment"/></xsl:attribute>
			<xsl:attribute name="SecurityProcess">APPLY</xsl:attribute>
			<xsl:attribute name="Protocol">ALL</xsl:attribute>
			<xsl:element name="SecurityGateway" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AutoIkeSecGw" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:choose>
						<xsl:when test="count($vpnwiz/wiz:VPNSettings/wiz:ExistSecGw)>0">
							<xsl:attribute name="value"><xsl:value-of select="$vpnwiz/wiz:VPNSettings/wiz:ExistSecGw/wiz:SecGw/@value"/></xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="value"><xsl:value-of select="$vpnwiz/wiz:VPNSettings/wiz:NewSecGw/wiz:name/@name"/></xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:element>
			<xsl:apply-templates select="$vpnwiz/wiz:SourceSettings/wiz:SourceAddress"/>
			<xsl:element name="SourcePort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
			<xsl:apply-templates select="$vpnwiz/wiz:DestinationSettings/wiz:DestAddress"/>
			<xsl:element name="DestPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
		</xsl:element>
		<!-- Insert IPSEC Rule to ensure that traffic to and from the remote VPN subnet B gets analyzed by AV software -->
		<xsl:element name="IPSECRules" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="Name"><xsl:value-of select="concat($vpnwiz/wiz:GeneralSettings/@Name,'_2')"/></xsl:attribute>
			<xsl:attribute name="GUID"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@GUID2"/></xsl:attribute>
			<xsl:attribute name="RuleId">1</xsl:attribute>
			<xsl:attribute name="Enabled">true</xsl:attribute>
			<xsl:attribute name="Comment"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@Comment"/></xsl:attribute>
			<xsl:attribute name="SecurityProcess">APPLY</xsl:attribute>
			<xsl:attribute name="Protocol">ALL</xsl:attribute>
			<xsl:element name="SecurityGateway" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AutoIkeSecGw" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:choose>
						<xsl:when test="count($vpnwiz/wiz:VPNSettings/wiz:ExistSecGw)>0">
							<xsl:attribute name="value"><xsl:value-of select="$vpnwiz/wiz:VPNSettings/wiz:ExistSecGw/wiz:SecGw/@value"/></xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="value"><xsl:value-of select="$vpnwiz/wiz:VPNSettings/wiz:NewSecGw/wiz:name/@name"/></xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:element>
			<xsl:element name="SourceAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:choose>
					<xsl:when test="count($vpnwiz/wiz:VPNSettings/wiz:ExistSecGw)>0">
						<xsl:variable name="SecGw" select="/fwm:policy/fwm:SecurityGateways/fwm:AutoIkeSecurityGatewayList[@Name=$vpnwiz/wiz:VPNSettings/wiz:ExistSecGw/wiz:SecGw/@value]"/>
						<xsl:variable name="Address" select="$SecGw/fwm:IkeConfig/fwm:LocalIpAddress" />
						<xsl:call-template name="insert-ipsec-address">
							<xsl:with-param name="Address" select="$Address"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="Address" select="$vpnwiz/wiz:VPNSettings/wiz:NewSecGw/wiz:Addresses/wiz:LocalIpAddress" />
						<xsl:call-template name="insert-ipsec-address">
							<xsl:with-param name="Address" select="$Address"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
			<xsl:element name="SourcePort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
			<xsl:apply-templates select="$vpnwiz/wiz:DestinationSettings/wiz:DestAddress"/>
			<xsl:element name="DestPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
		</xsl:element>
		<!-- Insert IPSEC Rule to ensure that traffic to and from the remote VPN subnet B gets analyzed by AV software -->
		<xsl:element name="IPSECRules" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="Name"><xsl:value-of select="concat($vpnwiz/wiz:GeneralSettings/@Name,'_3')"/></xsl:attribute>
			<xsl:attribute name="GUID"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@GUID3"/></xsl:attribute>
			<xsl:attribute name="RuleId">2</xsl:attribute>
			<xsl:attribute name="Enabled">true</xsl:attribute>
			<xsl:attribute name="Comment"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@Comment"/></xsl:attribute>
			<xsl:attribute name="SecurityProcess">APPLY</xsl:attribute>
			<xsl:attribute name="Protocol">ALL</xsl:attribute>
			<xsl:element name="SecurityGateway" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AutoIkeSecGw" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:choose>
						<xsl:when test="count($vpnwiz/wiz:VPNSettings/wiz:ExistSecGw)>0">
							<xsl:attribute name="value"><xsl:value-of select="$vpnwiz/wiz:VPNSettings/wiz:ExistSecGw/wiz:SecGw/@value"/></xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="value"><xsl:value-of select="$vpnwiz/wiz:VPNSettings/wiz:NewSecGw/wiz:name/@name"/></xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:element>
			<xsl:apply-templates select="$vpnwiz/wiz:SourceSettings/wiz:SourceAddress"/>
			<xsl:element name="SourcePort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
			<xsl:element name="DestAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:choose>
					<xsl:when test="count($vpnwiz/wiz:VPNSettings/wiz:ExistSecGw)>0">
						<xsl:variable name="SecGw" select="/fwm:policy/fwm:SecurityGateways/fwm:AutoIkeSecurityGatewayList[@Name=$vpnwiz/wiz:VPNSettings/wiz:ExistSecGw/wiz:SecGw/@value]"/>
						<xsl:variable name="Address" select="$SecGw/fwm:IkeConfig/fwm:RemoteIpAddress" />
						<xsl:call-template name="insert-ipsec-address">
							<xsl:with-param name="Address" select="$Address"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="Address" select="$vpnwiz/wiz:VPNSettings/wiz:NewSecGw/wiz:Addresses/wiz:RemoteIpAddress" />
						<xsl:call-template name="insert-ipsec-address">
							<xsl:with-param name="Address" select="$Address"/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
			<xsl:element name="DestPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
		</xsl:element>		
	</xsl:template>
	<xsl:template name="insertsecuritygw">
		<!-- Insert security gateway based on values in the vpn wizard policy if new gateway selected. -->
		<xsl:if test="count($vpnwiz/wiz:VPNSettings/wiz:NewSecGw)>0">
			<xsl:element name="AutoIkeSecurityGatewayList" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:attribute name="Name"><xsl:value-of select="$vpnwiz/wiz:VPNSettings/wiz:NewSecGw/wiz:name/@name"/></xsl:attribute>
				<xsl:attribute name="Comment"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@Comment"/></xsl:attribute>
				<xsl:attribute name="GUID"><xsl:value-of select="$vpnwiz/wiz:VPNSettings/wiz:NewSecGw//@GUID"/></xsl:attribute>
				<xsl:element name="IkeConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="Direction">BOTHDIRECTIONS</xsl:attribute>
					<xsl:attribute name="ExchangeType">MAINMODE</xsl:attribute>
					<xsl:attribute name="LocalIdType">IPADDRESS</xsl:attribute>
					<xsl:attribute name="EncryptionAlg">3DES</xsl:attribute>
					<xsl:attribute name="EncAuthAlg">SHA1</xsl:attribute>
					<xsl:attribute name="LifeTimeSecs">7200</xsl:attribute>
					<xsl:attribute name="LifeTimeKBs">0</xsl:attribute>
					<xsl:attribute name="DHGroup">MODP_1024</xsl:attribute>
					<xsl:attribute name="AuthMode"><xsl:value-of select="$vpnwiz/wiz:VPNSettings/wiz:NewSecGw/wiz:AuthType/@AuthMode"/></xsl:attribute>
					<xsl:if test="$vpnwiz/wiz:VPNSettings/wiz:NewSecGw/wiz:AuthType/@AuthMode='Pre-SharedKey' ">
						<xsl:attribute name="PreSharedKey"><xsl:value-of select="$vpnwiz/wiz:VPNSettings/wiz:NewSecGw/wiz:AuthType/@PreSharedKey"/></xsl:attribute>
					</xsl:if>
					<xsl:apply-templates select="$vpnwiz/wiz:VPNSettings/wiz:NewSecGw/wiz:Addresses/*"/>
					<xsl:element name="DaLocalId" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:choose>
							<xsl:when test="count($vpnwiz/wiz:VPNSettings/wiz:NewSecGw/wiz:Addresses/wiz:LocalIpAddress/npcommon:AddrDA)>0">
								<xsl:element name="DYNADDRRef" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
									<xsl:attribute name="Name"><xsl:value-of select="$vpnwiz/wiz:VPNSettings/wiz:NewSecGw/wiz:Addresses/wiz:LocalIpAddress/npcommon:AddrDA/@Name"/></xsl:attribute>
								</xsl:element>
							</xsl:when>
							<xsl:when test="count($vpnwiz/wiz:VPNSettings/wiz:NewSecGw/wiz:Addresses/wiz:LocalIpAddress/npcommon:AddrAN) >0">
								<xsl:element name="ADDRNAMERef" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
									<xsl:attribute name="Name"><xsl:value-of select="$vpnwiz/wiz:VPNSettings/wiz:NewSecGw/wiz:Addresses/wiz:LocalIpAddress/npcommon:AddrAN/@Name"/></xsl:attribute>
								</xsl:element>
							</xsl:when>
							<xsl:when test="count($vpnwiz/wiz:VPNSettings/wiz:NewSecGw/wiz:Addresses/wiz:LocalIpAddress/npcommon:AddrIP) >0">
								<xsl:element name="IPADDRESS" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
									<xsl:attribute name="IPAddress"><xsl:value-of select="$vpnwiz/wiz:VPNSettings/wiz:NewSecGw/wiz:Addresses/wiz:LocalIpAddress/npcommon:AddrIP/@IPAddress"/></xsl:attribute>
								</xsl:element>
							</xsl:when>
						</xsl:choose>
					</xsl:element>
					<xsl:element name="RemoteId" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:attribute name="GUID"><xsl:value-of select="$vpnwiz/wiz:VPNSettings/wiz:NewSecGw/@REMOTEIDGUID"/></xsl:attribute>
						<xsl:element name="DaRemoteId" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:apply-templates select="$vpnwiz/wiz:VPNSettings/wiz:NewSecGw/wiz:RemoteID/*"/>
						</xsl:element>
					</xsl:element>
					<xsl:element name="IKEXAuth" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:attribute name="Enabled">false</xsl:attribute>
					</xsl:element>
					<xsl:element name="OCSP" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:attribute name="Enabled">false</xsl:attribute>
					</xsl:element>
				</xsl:element>
				<xsl:element name="IpsecConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="EncapsulationMode">TUNNEL</xsl:attribute>
					<xsl:attribute name="PerfectForwardSecrecy">GROUP2</xsl:attribute>
					<xsl:element name="SecurityProposals" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:attribute name="GUID"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@GUID1"/></xsl:attribute>
						<xsl:attribute name="SecurityProtocol">ESPWithAuth</xsl:attribute>
						<xsl:attribute name="AuthAlgorithm">SHA1</xsl:attribute>
						<xsl:attribute name="EspAlgorithm">AES</xsl:attribute>
						<xsl:attribute name="EspAesKeyLength">32</xsl:attribute>
						<xsl:attribute name="LifeTimeSecs">7200</xsl:attribute>
						<xsl:attribute name="LifeTimeKBs">0</xsl:attribute>
					</xsl:element>
					<xsl:element name="Advanced" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:attribute name="Enabled">false</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<xsl:template name="insertsnatrules">
		<xsl:element name="GlobalNatBindList" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="Name"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@Name"/></xsl:attribute>
			<xsl:attribute name="Enabled">true</xsl:attribute>
			<xsl:attribute name="Comment"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@Comment"/></xsl:attribute>
			<xsl:attribute name="GUID"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@GUID1"/></xsl:attribute>
			<xsl:attribute name="RuleId">0</xsl:attribute>
			<xsl:element name="Protocol" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyProto" namespace="http://www.iss.net/cml/NetworkProtector/fwm" />
			</xsl:element>
			<xsl:element name="LocalAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:apply-templates select="$vpnwiz/wiz:SourceSettings/wiz:SourceAddress/*"/>
			</xsl:element>
			<xsl:element name="RemoteAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:apply-templates select="$vpnwiz/wiz:DestinationSettings/wiz:DestAddress/*"/>
			</xsl:element>
			<xsl:element name="ServicePort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
			<xsl:element name="NatIp" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="SameAsIncomingAddr" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template match="wiz:AddressName">
		<xsl:param name="ruleType" select=" ' none' "/>
		<xsl:choose>
			<xsl:when test="$ruleType='Access' ">
				<xsl:element name="Addresses" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:element name="AddressEntry" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:element name="AddressName" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:when>				
			<xsl:otherwise>
				<xsl:element name="AddressName" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="wiz:DynamicAddressName">
		<xsl:param name="ruleType" select=" ' none' "/>
		<xsl:choose>
			<xsl:when test="$ruleType='Access' ">
				<xsl:element name="Addresses" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:element name="AddressEntry" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:element name="DynamicAddressName" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
								<xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute>
							</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:when>				
			<xsl:otherwise>
				<xsl:element name="DynamicAddressName" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="npcommon:*">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/common">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*" />
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<xsl:template name="insert-policy-address">
		<xsl:param name="Address"/>
		<xsl:choose>
			<xsl:when test="count($Address/npcommon:AddrDA)>0">
				<xsl:element name="Addresses" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:element name="AddressEntry" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:element name="DynamicAddressName" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:attribute name="value"><xsl:value-of select="$Address/npcommon:AddrDA/@Name"/></xsl:attribute>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:when test="count($Address/npcommon:AddrAN) >0">
				<xsl:element name="Addresses" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:element name="AddressEntry" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:element name="AddressName" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:attribute name="value"><xsl:value-of select="$Address/npcommon:AddrAN/@Name"/></xsl:attribute>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:when test="count($Address/npcommon:AddrIP) >0">
				<xsl:element name="SingleAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="IPSingleAddress"><xsl:value-of select="$Address/npcommon:AddrIP/@IPAddress"/></xsl:attribute>
				</xsl:element>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="insert-ipsec-address">
		<xsl:param name="Address"/>
		<xsl:choose>
			<xsl:when test="count($Address/npcommon:AddrDA)>0">
				<xsl:element name="DynamicAddressName" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="value"><xsl:value-of select="$Address/npcommon:AddrDA/@Name"/></xsl:attribute>
				</xsl:element>
			</xsl:when>
			<xsl:when test="count($Address/npcommon:AddrAN) >0">
				<xsl:element name="AddressName" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="value"><xsl:value-of select="$Address/npcommon:AddrAN/@Name"/></xsl:attribute>
				</xsl:element>
			</xsl:when>
			<xsl:when test="count($Address/npcommon:AddrIP) >0">
				<xsl:element name="SingleAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="IPSingleAddress"><xsl:value-of select="$Address/npcommon:AddrIP/@IPAddress"/></xsl:attribute>
				</xsl:element>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
