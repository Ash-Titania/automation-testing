<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:upds="http://www.iss.net/cml/NetworkProtector/upds">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="upds:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/upds">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">2.0.0</xsl:attribute>
			<xsl:apply-templates select="upds:UpdateSettings"/>
				<xsl:element name="NotificationConfig" namespace="http://www.iss.net/cml/NetworkProtector/upds">
					<xsl:attribute name="EnableAvailableNotify">true</xsl:attribute>
					<xsl:attribute name="EnableInstallNotify">true</xsl:attribute>
					<xsl:attribute name="EnableErrorNotify">true</xsl:attribute>
					<xsl:element name="AvailableResponse" namespace="http://www.iss.net/cml/NetworkProtector/upds">
						<xsl:attribute name="sendEmail">false</xsl:attribute>
						<xsl:attribute name="emailName">Default</xsl:attribute>
						<xsl:attribute name="sendSnmp">false</xsl:attribute>
						<xsl:attribute name="doPost">false</xsl:attribute>
					</xsl:element>
					<xsl:element name="InstallResponse" namespace="http://www.iss.net/cml/NetworkProtector/upds">
						<xsl:attribute name="sendEmail">false</xsl:attribute>
						<xsl:attribute name="emailName">Default</xsl:attribute>
						<xsl:attribute name="sendSnmp">false</xsl:attribute>
						<xsl:attribute name="doPost">false</xsl:attribute>
					</xsl:element>
					<xsl:element name="ErrorResponse" namespace="http://www.iss.net/cml/NetworkProtector/upds">
						<xsl:attribute name="sendEmail">false</xsl:attribute>
						<xsl:attribute name="emailName">Default</xsl:attribute>
						<xsl:attribute name="sendSnmp">false</xsl:attribute>
						<xsl:attribute name="doPost">false</xsl:attribute>
					</xsl:element>
			</xsl:element>
			<xsl:apply-templates select="upds:TuningSetting"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="upds:UpdateSettings">
		<xsl:element name="UpdateSettings" namespace="http://www.iss.net/cml/NetworkProtector/upds">
			<xsl:apply-templates select="upds:Scheduling"/>
			<xsl:element name="SecurityUpdateSettings" namespace="http://www.iss.net/cml/NetworkProtector/upds">
				<xsl:attribute name="EnableAutoDownload"><xsl:value-of select="@AutoDownload"/></xsl:attribute>
				<xsl:attribute name="EnableAutoInstall"><xsl:value-of select="@AutoInstall"/></xsl:attribute>
			</xsl:element>
			<xsl:element name="WFASDBUpdateSettings" namespace="http://www.iss.net/cml/NetworkProtector/upds">
				<xsl:choose>
					<xsl:when test="@AutoUpdateCFAS='true'">
						<xsl:attribute name="EnableAutoUpdate"><xsl:value-of select="@AutoUpdateCFAS"/></xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="EnableAutoUpdate">false</xsl:attribute>
						</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
			<xsl:element name="FirmwareUpdateSettings" namespace="http://www.iss.net/cml/NetworkProtector/upds">
				<xsl:attribute name="EnableAutoDownload">false</xsl:attribute>
				<xsl:element name="FirmwareInstallOptions" namespace="http://www.iss.net/cml/NetworkProtector/upds">
					<xsl:attribute name="EnableBackup">true</xsl:attribute>
					<xsl:element name="FirmwareInstallMethod" namespace="http://www.iss.net/cml/NetworkProtector/upds">
						<xsl:element name="DisableInstall" namespace="http://www.iss.net/cml/NetworkProtector/upds"/>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template match="upds:Scheduling">
		<xsl:element name="AutoCheckSchedule" namespace="http://www.iss.net/cml/NetworkProtector/upds">
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
