<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:upds="http://www.iss.net/cml/NetworkProtector/upds">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
    <xsl:param name="updateDate"/>
    <xsl:param name="updateTime"/>
    <xsl:param name="updateVersion"/>
    <xsl:template match="upds:policy">
        <xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/upds">
            <xsl:copy-of select="@*"/>
            <xsl:element name="UpdateSettings" namespace="http://www.iss.net/cml/NetworkProtector/upds">
                <xsl:apply-templates select="upds:UpdateSettings"/>
            </xsl:element>
            <xsl:copy-of select="*[name() != 'UpdateSettings']"/>
        </xsl:element>
    </xsl:template>

<!--    <FirmwareUpdateSettings EnableAutoDownload='true' >
      <FirmwareInstallOptions EnableBackup='true' >
        <FirmwareInstallMethod>
          <EnableOneTimeInstall Date='2004-05-12' Time='01:30' >
            <VersionToInstall >
              <SpecificVersion Version='2.2' />
            </VersionToInstall>
          </EnableOneTimeInstall>
        </FirmwareInstallMethod>
      </FirmwareInstallOptions>
    </FirmwareUpdateSettings>-->
    <xsl:template match="upds:UpdateSettings">
        <xsl:copy-of select="@*"/>
        <xsl:copy-of select="*[name() != 'FirmwareUpdateSettings']"/>
        <xsl:element name="FirmwareUpdateSettings" namespace="http://www.iss.net/cml/NetworkProtector/upds">
            <xsl:attribute name="EnableAutoDownload">true</xsl:attribute>
            <xsl:element name="FirmwareInstallOptions" namespace="http://www.iss.net/cml/NetworkProtector/upds">
                <xsl:attribute name="EnableBackup">true</xsl:attribute>
                <xsl:element name="FirmwareInstallMethod" namespace="http://www.iss.net/cml/NetworkProtector/upds">
                    <xsl:element name="EnableOneTimeInstall" namespace="http://www.iss.net/cml/NetworkProtector/upds">
                        <xsl:attribute name="Date"><xsl:value-of select="$updateDate"/></xsl:attribute>
                        <xsl:attribute name="Time"><xsl:value-of select="$updateTime"/></xsl:attribute>
                        <xsl:element name="VersionToInstall" namespace="http://www.iss.net/cml/NetworkProtector/upds">
                            <xsl:element name="SpecificVersion" namespace="http://www.iss.net/cml/NetworkProtector/upds">
                                <xsl:attribute name="Version"><xsl:value-of select="$updateVersion"/></xsl:attribute>
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>
