<schema targetNamespace='http://www.iss.net/cml/NetworkProtector/network'
    xmlns='http://www.w3.org/2001/XMLSchema'
    xmlns:cml='http://www.iss.net/cml'
    xmlns:common='http://www.iss.net/cml/common'
    xmlns:network='http://www.iss.net/cml/NetworkProtector/network'
    xmlns:npcommon='http://www.iss.net/cml/NetworkProtector/common'
    xmlns:cpe='http://www.iss.net/cml/cpe'
    elementFormDefault='qualified'
    version='1.0.0'>

    <annotation>
        <appinfo><agent name='NetworkProtector'/></appinfo>
        <documentation>Schema for NetworkProtector Network Settings.</documentation>
    </annotation>

    <!-- Import the 'cml' namespace.  This namespace defines the
    namespaces, elements and types that are common to all ISS CML documents. -->
    <import namespace='http://www.iss.net/cml' schemaLocation='../../cml2_3_0.xsd'/>
    <import namespace='http://www.iss.net/cml/common' schemaLocation='../../common/common2_1_0.xsd'/>
    <import namespace='http://www.iss.net/cml/NetworkProtector/common' schemaLocation='../common/common1_0_0.xsd'/>

    <!-- NetworkProtector Network Cofiguration Settings. -->

	<simpleType name='InterfaceNameType'>
		<restriction base='string'>
			<enumeration value='eth0'/>
			<enumeration value='eth1'/>
			<enumeration value='eth2'/>
		</restriction>
	</simpleType>

	<simpleType name='LinkActivationType'>
		<restriction base='string'>
			<enumeration value='OnDemand'/>
			<enumeration value='Continuous'/>
		</restriction>
	</simpleType>

    <simpleType name='ServiceNameType'>
        <restriction base='normalizedString'>
            <maxLength value='150'/>
        </restriction>
    </simpleType>

	<simpleType name='MtuType'>
		<restriction base='unsignedInt'>
		   <maxInclusive value='1500'/>
		</restriction>
	</simpleType>

	<simpleType name='DNSCheckboxType'
		cpe:classname='net.iss.cpe.plugins.DNSCheckboxSimpleType'
		cpe:classJar='../plugin.jar' >
		<restriction base='string'/>
	</simpleType>

    <complexType name='DNSServersType'>
        <attribute name='PrimaryDNS' type='common:DottedStringIpv4Address' use='required'/>
        <attribute name='SecondaryDNS' type='common:DottedStringIpv4Address' />
        <attribute name='TertiaryDNS' type='common:DottedStringIpv4Address' />
    </complexType>
    
    <complexType name='DNSSearchPathListType'>
		<attribute name='Id' type='common:OrderedListOrdinal' cpe:hidden='true' use='required'/>
        <attribute name='DomainName' type='npcommon:FQDNType' use='required' />
    </complexType>

	<complexType name='ExternalAddressType'>
		<choice>
			<element name='DHCP'>
				<complexType>
					<attribute name='EnableMacCloning' type='boolean' />
					<attribute name='MacAddr' type='common:MacAddress' cpe:depends='EnableMacCloning' default='00:00:00:00:00:00'/>
				</complexType>
			</element>
			<element name='PPPoE'>
				<complexType>
					<attribute name='UserName' type='npcommon:NameType' use='required' />
					<attribute name='UserPw' type='npcommon:NameType' use='required' />
					<attribute name='LinkActivation' type='network:LinkActivationType' default='Continuous' />
					<attribute name='ClampMSS' type='boolean' />
					<attribute name='ServiceName' type='network:ServiceNameType' />
				</complexType>
			</element>
			<element name='Static'>
                <complexType>
                    <attribute name='IpAddr' type='common:DottedStringIpv4Address' use='required' />
                    <attribute name='SubnetMask' type='common:DottedStringIpv4Address' use='required' />
                    <attribute name='GatewayIpAddr' type='common:DottedStringIpv4Address' use='required' />
				</complexType>
			</element>
		</choice>
	</complexType>

    <complexType name='ExternalDNSType'>
        <sequence>
            <element name='DNSSearchPathList' type='network:DNSSearchPathListType' minOccurs='0' maxOccurs='unbounded' cpe:depends='UseDynamicSettings' cpe:dependsValue='false'/>
        </sequence>
        <attribute name='UseDynamicSettings' type='network:DNSCheckboxType' default='true' />
        <attribute name='PrimaryDNS' type='common:DottedStringIpv4Address' cpe:depends='UseDynamicSettings' cpe:dependsValue='false' cpe:onlyRequiredWhenEnabled='true'/>
        <attribute name='SecondaryDNS' type='common:DottedStringIpv4Address' cpe:depends='UseDynamicSettings' cpe:dependsValue='false' />
        <attribute name='TertiaryDNS' type='common:DottedStringIpv4Address' cpe:depends='UseDynamicSettings' cpe:dependsValue='false'/>
	</complexType>

    <complexType name='ExternalConfigType'>
        <sequence>
            <element name='IPAddress' type='network:ExternalAddressType' minOccurs='0' cpe:depends='Enabled' />
            <element name='ExternalDNS' type='network:ExternalDNSType' minOccurs='0' cpe:depends='Enabled' />
    	</sequence>
        <attribute name='InterfaceName' type='npcommon:NameType' default='eth1' cpe:readOnly='true' />
		<attribute name='Enabled' type='boolean' default='false' />
        <attribute name='HostName' type='npcommon:FQDNType' cpe:depends='Enabled' cpe:onlyRequiredWhenEnabled='true' />
    </complexType>


    <complexType name='InternalConfigType'>
        <attribute name='InterfaceName' type='npcommon:NameType' default='eth0' cpe:readOnly='true'/>
		<attribute name='Enabled' type='boolean' default='true' />
        <attribute name='IPAddress' type='common:DottedStringIpv4Address' cpe:depends='Enabled' cpe:onlyRequiredWhenEnabled='true'/>
        <attribute name='SubnetMask' type='common:DottedStringIpv4Address' cpe:depends='Enabled' cpe:onlyRequiredWhenEnabled='true'/>
    </complexType>

    <complexType name='DMZConfigType'>
        <attribute name='InterfaceName' type='npcommon:NameType' default='eth2' cpe:readOnly='true' />
		<attribute name='Enabled' type='boolean' default='false' />
        <attribute name='IPAddress' type='common:DottedStringIpv4Address' cpe:depends='Enabled' cpe:onlyRequiredWhenEnabled='true' />
        <attribute name='SubnetMask' type='common:DottedStringIpv4Address' cpe:depends='Enabled' cpe:onlyRequiredWhenEnabled='true' />
    </complexType>

    <complexType name='NetworkProtectorNet'>
        <complexContent>
            <extension base='cml:PolicyBase'>
                <sequence>
                	<element name='ExternalInterface' minOccurs='1' maxOccurs='2' cpe:hidden='true'>
						<complexType>
							<attribute name='Name' type='normalizedString' />
						</complexType>
					</element>
        		    <element name='ExternalConfig' type='network:ExternalConfigType' minOccurs='1' maxOccurs='1' />
        		    <element name='InternalConfig' type='network:InternalConfigType' minOccurs='1' maxOccurs='1' />
        		    <element name='DMZConfig' type='network:DMZConfigType' minOccurs='1' maxOccurs='1' />
                </sequence>
            </extension>
        </complexContent>
    </complexType>

    <element name='policy' type='network:NetworkProtectorNet' substitutionGroup='cml:policy-base'/>

</schema>
