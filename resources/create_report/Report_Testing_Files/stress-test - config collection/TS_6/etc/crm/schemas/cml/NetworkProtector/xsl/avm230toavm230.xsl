<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:avm="http://www.iss.net/cml/NetworkProtector/avm"  xmlns:npcommon="http://www.iss.net/cml/NetworkProtector/common">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="avm:TuningSetting[@name='avm.se.scanner.scannerd.max_spam_scan_size']">
		<xsl:element name="TuningSetting" namespace="{namespace-uri(.)}">
			<xsl:attribute name="name">avm.se.smtp.psmtpd.max_spam_scan_size</xsl:attribute>
			<xsl:copy-of select="@*[name() != 'name']"/>
			<xsl:element name="number" namespace="http://www.iss.net/cml/NetworkProtector/common">
				<xsl:attribute name="value"><xsl:value-of select="child::npcommon:number/@value"/></xsl:attribute>
			</xsl:element>
		</xsl:element>
		<xsl:element name="TuningSetting" namespace="{namespace-uri(.)}">
			<xsl:attribute name="name">avm.se.pop3.ppop3d.max_spam_scan_size</xsl:attribute>
			<xsl:copy-of select="@*[name() != 'name']"/>
			<xsl:element name="number" namespace="http://www.iss.net/cml/NetworkProtector/common">
				<xsl:attribute name="value"><xsl:value-of select="child::npcommon:number/@value"/></xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template match="avm:TuningSetting[@name='avm.se.scanner.scannerd.spam_scan_bypass']">
		<xsl:element name="TuningSetting" namespace="{namespace-uri(.)}">
			<xsl:attribute name="name">avm.se.smtp.psmtpd.spam_scan_bypass</xsl:attribute>
			<xsl:copy-of select="@*[name() != 'name']"/>
			<xsl:element name="boolean" namespace="http://www.iss.net/cml/NetworkProtector/common">
				<xsl:attribute name="value"><xsl:value-of select="child::npcommon:boolean/@value"/></xsl:attribute>
			</xsl:element>
		</xsl:element>
		<xsl:element name="TuningSetting" namespace="{namespace-uri(.)}">
			<xsl:attribute name="name">avm.se.pop3.ppop3d.spam_scan_bypass</xsl:attribute>
			<xsl:copy-of select="@*[name() != 'name']"/>
			<xsl:element name="boolean" namespace="http://www.iss.net/cml/NetworkProtector/common">
				<xsl:attribute name="value"><xsl:value-of select="child::npcommon:boolean/@value"/></xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>

