<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:net="http://www.iss.net/cml/NetworkProtector/network">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="net:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/network">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">3.0.0</xsl:attribute>
			<xsl:apply-templates select="net:ExternalInterface"/>
			<xsl:apply-templates select="net:Interface"/>
			<xsl:element name="NetworkingConfig" namespace="http://www.iss.net/cml/NetworkProtector/network">
				<xsl:element name="ModeChangeAction" namespace="http://www.iss.net/cml/NetworkProtector/network">
					<xsl:element name="RebootOnModeChange" namespace="http://www.iss.net/cml/NetworkProtector/network" />
				</xsl:element>
				<xsl:element name="ModeConfig" namespace="http://www.iss.net/cml/NetworkProtector/network">
					<xsl:apply-templates select="net:RoutingModeConfig" />
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template match="net:ExternalConfig">
		<xsl:element name="ExternalConfig" namespace="http://www.iss.net/cml/NetworkProtector/network">
			<xsl:copy-of select="@*"/>
			<xsl:attribute name="PrimaryMgmtInterface">
				<xsl:choose>
					<xsl:when test="current()/net:IPAddress/net:Static/@PrimaryMgmtInterface = 'true' ">true</xsl:when>
					<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="net:Static">
		<xsl:element name="Static" namespace="http://www.iss.net/cml/NetworkProtector/network">
			<xsl:copy-of select="@*[name() != 'PrimaryMgmtInterface']"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
