<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ipm="http://www.iss.net/cml/NetworkProtector/ipm">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:template match="ipm:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/ipm">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">2.0.0</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ipm:DiagnosticMsgs">
		<xsl:element name="DiagnosticMsgs" namespace="http://www.iss.net/cml/NetworkProtector/ipm">
			<xsl:copy-of select="@*[name() != 'PacketDropped']"/>
			<xsl:attribute name="InvalidChecksum"><xsl:value-of select="@PacketDropped"/></xsl:attribute>
			<xsl:attribute name="InvalidProtocol"><xsl:value-of select="@PacketDropped"/></xsl:attribute>
			<xsl:attribute name="ResourceError"><xsl:value-of select="@PacketDropped"/></xsl:attribute>
			<xsl:attribute name="BlockedTCPConnection">false</xsl:attribute>
			<xsl:attribute name="QuarantineRuleMatched">false</xsl:attribute>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
