<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:crm="http://www.iss.net/cml/NetworkProtector/crm" xmlns:srv="http://www.iss.net/cml/NetworkProtector/srv">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="with" select="'npcrm1_0.xml'"/>
	<xsl:template match="srv:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/srv">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">1.0.3</xsl:attribute>
			<xsl:apply-templates/>
			<xsl:apply-templates select="document($with)/crm:policy/crm:ResponseConfig/crm:SnmpNotify"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="srv:DHCPStatus"></xsl:template>
	<xsl:template match="crm:SnmpNotify">
		<xsl:choose>
			<xsl:when test="string-length(@manager) > 0">
				<xsl:element name="SnmpConfig" namespace="http://www.iss.net/cml/NetworkProtector/srv">
					<xsl:attribute name="Enabled">true</xsl:attribute>
					<xsl:attribute name="SystemName">default_system_name</xsl:attribute>
					<xsl:attribute name="SystemLocation">default_system_location</xsl:attribute>
					<xsl:attribute name="TrapAddress"><xsl:value-of select="@manager"/></xsl:attribute>
					<xsl:attribute name="TrapCommunity"><xsl:value-of select="@community"/></xsl:attribute>
				</xsl:element>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>