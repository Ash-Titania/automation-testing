<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:netobj="http://www.iss.net/cml/NetworkObjects">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:template match="netobj:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkObjects">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates/>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth0Ip</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth0 IP Address</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth0Net</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth0 network in network address, subnet mask form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth0Cidr</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth0 network in cidr form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth0Range</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth1 network in range form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth1Ip</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth1 IP Address</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth1Net</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth1 network in network address, subnet mask form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth1Cidr</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth1 network in cidr form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth1Range</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth1 network in range form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth2Ip</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth2 IP Address</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth2Net</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth2 network in network address, subnet mask form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth2Cidr</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth2 network in cidr form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth2Range</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth2 network in range form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth3Ip</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth3 IP Address</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth3Net</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth3 network in network address, subnet mask form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth3Cidr</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth3 network in cidr form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth3Range</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth3 network in range form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth4Ip</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth4 IP Address</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth4Net</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth4 network in network address, subnet mask form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth4Cidr</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth4 network in cidr form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth4Range</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth4 network in range form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth5Ip</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth5 IP Address</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth5Net</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth5 network in network address, subnet mask form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth5Cidr</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth5 network in cidr form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth5Range</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth5 network in range form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth6Ip</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth6 IP Address</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth6Net</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth6 network in network address, subnet mask form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth6Cidr</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth6 network in cidr form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth6Range</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth6 network in range form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth7Ip</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth7 IP Address</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth7Net</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth7 network in network address, subnet mask form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth7Cidr</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth7 network in cidr form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysEth7Range</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for eth7 network in range form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysTransmgmtIp</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for transparent mode mgmt IP Address</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysTransmgmtNet</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for transparent mode mgmt network in network address, subnet mask form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysTransmgmtCidr</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for transparent mode mgmt network in cidr form</xsl:attribute>
			</xsl:element>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">SysTransmgmtRange</xsl:attribute>
				<xsl:attribute name="Comment">System specific dynamic address name for transparent mode mgmt network in range form</xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*" />
			</xsl:element>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
