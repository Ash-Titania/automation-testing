<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:npcommon="http://www.iss.net/cml/NetworkProtector/common" xmlns:fwm="http://www.iss.net/cml/NetworkProtector/fwm" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
	<xsl:output encoding="UTF-8" indent="yes" method="xml"/>
	<xsl:param name="meshxml" select="'C:\Documents and Settings\chubbard.ADMIN\My Documents\mesh.xml'"/>
	<xsl:param name="localComponent" select="1"/>
	<xsl:variable name="firewall" select="/fwm:policy"/>
	<xsl:variable name="mesh" select="document($meshxml)/vpnmesh"/>
	<xsl:variable name="localMesh" select="$mesh/mesh[member[@component=$localComponent and not(@removed)]]"/>
	<xsl:variable name="removedMesh" select="$mesh/mesh[member[@component=$localComponent and @removed]]"/>

	<!--START HERE:  Match the root node-->
	<xsl:template match="fwm:policy">
		<xsl:variable name="nodesToMesh" select="$localMesh/member[not(@component=$localComponent) and not(@removed)]"/>
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates mode="access" select="$nodesToMesh"/>
			<xsl:apply-templates select="fwm:Policy"/>
			<xsl:apply-templates mode="ipsec" select="$nodesToMesh"/>
			<xsl:apply-templates select="fwm:IPSECRules"/>
			<xsl:apply-templates select="fwm:CommonLists"/>
			<xsl:apply-templates select="fwm:SecurityGateways"/>
			<xsl:apply-templates select="fwm:MessageConfig"/>
			<xsl:apply-templates select="fwm:ProxyRedirectRule"/>
			<xsl:apply-templates select="fwm:VpnAdvancedSettings"/>
			<xsl:apply-templates select="fwm:TuningSetting"/>
		</xsl:element>
	</xsl:template> 
	
	<xsl:template match="member" mode="access">
		<xsl:variable name="local" select="../member[@component=$localComponent]"/>
		<xsl:variable name="occursBefore" select="preceding::member[@network=current()/@network and @component=current()/@component and not(@removed)]"/>
		<xsl:variable name="anythingBefore" select="count($occursBefore/preceding-sibling::node()[@component=$localComponent]) + count($occursBefore/following-sibling::node()[@component=$localComponent])"/>
		<xsl:if test="count($firewall/fwm:Policy[@GUID=current()/access/@guid0])=0">
			<xsl:if test="$anythingBefore = 0">
				<xsl:call-template name="insertAccessRule">
					<xsl:with-param name="guid" select="current()/access/@guid0"/>
					<xsl:with-param name="agent" select="current()/@agent-ip"/>
					<xsl:with-param name="src" select="current()"/>
					<xsl:with-param name="dest" select="$local"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
		<xsl:if test="count($firewall/fwm:Policy[@GUID=current()/access/@guid1])=0">
			<xsl:if test="$anythingBefore = 0">
				<xsl:call-template name="insertAccessRule">
					<xsl:with-param name="guid" select="current()/access/@guid1"/>
					<xsl:with-param name="agent" select="current()/@agent-ip"/>
					<xsl:with-param name="src" select="$local"/>
					<xsl:with-param name="dest" select="current()"/>
				</xsl:call-template>
				</xsl:if>
		</xsl:if>
		<xsl:if test="count($firewall/fwm:Policy[@GUID=current()/access/@guid2])=0">
			<xsl:if test="$anythingBefore = 0">
				<xsl:call-template name="insertAccessRule">
					<xsl:with-param name="guid" select="current()/access/@guid2"/>
					<xsl:with-param name="agent" select="current()/@agent-ip"/>
					<xsl:with-param name="src" select="current()/@agent-ip"/>
					<xsl:with-param name="dest" select="$local"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
	</xsl:template>

	<xsl:template match="fwm:Policy">
		<xsl:variable name="local" select="$localMesh/member[@component=$localComponent]"/>
		<xsl:choose>
			<!--remove this node if it's one of the removed nodes OR if we are processing 
			 the removed node then we'll remove any rule within the mesh it was removed from.-->
			<xsl:when test="count($localMesh/member[@removed='true']/access[@guid0=current()/@GUID]) > 0 or ($removedMesh/member[@component=$localComponent]/@removed and $removedMesh/member/access/@guid0=current()/@GUID)">
				<!--Do nothing, we should remove this GUID-->
			</xsl:when>
			<xsl:when test="count($localMesh/member[@removed='true']/access[@guid1=current()/@GUID]) > 0 or ($removedMesh/member[@component=$localComponent]/@removed  and $removedMesh/member/access/@guid1=current()/@GUID)">
				<!-- Do nothing, we should remove this GUID-->
			</xsl:when>
			<xsl:when test="count($localMesh/member[@removed='true']/access[@guid2=current()/@GUID]) > 0 or ($removedMesh/member[@component=$localComponent]/@removed  and $removedMesh/member/access/@guid2=current()/@GUID)">
				<!-- Do nothing, we should remove this GUID-->
			</xsl:when>
			<!--If this rule matches with the GUIDs in the VPN mesh document.  Then output the rule again so
			 any changes made to this rule are written out again.-->
			<xsl:when test="count($localMesh/member/access[@guid0=current()/@GUID])>0">
				<xsl:variable name="cur" select="$localMesh/member[access/@guid0=current()/@GUID]"/>
				<xsl:call-template name="insertAccessRule">
					<xsl:with-param name="guid" select="$cur/access/@guid0"/>
					<xsl:with-param name="agent" select="$cur/@agent-ip"/>
					<xsl:with-param name="src" select="$cur"/>
					<xsl:with-param name="dest" select="$local"/>
				</xsl:call-template>
			</xsl:when>
			<!--If this rule matches with the GUIDs in the VPN mesh document.  Then output the rule again so 
			 any changes made to this rule are written out again.-->
			<xsl:when test="count($localMesh/member/access[@guid1=current()/@GUID])>0">
				<xsl:variable name="cur" select="$localMesh/member[access/@guid1=current()/@GUID]"/>
				<xsl:call-template name="insertAccessRule">
					<xsl:with-param name="guid" select="$cur/access/@guid1"/>
					<xsl:with-param name="agent" select="$cur/@agent-ip"/>
					<xsl:with-param name="src" select="$local"/>
					<xsl:with-param name="dest" select="$cur"/>
				</xsl:call-template>
			</xsl:when>
			<!--If this rule matches with the GUIDs in the VPN mesh document.  Then output the rule again so
		                 any changes made to this rule are written out again.-->
			<xsl:when test="count($localMesh/member/access[@guid2=current()/@GUID])>0">
				<xsl:variable name="cur" select="$localMesh/member[access/@guid2=current()/@GUID]"/>
				<xsl:call-template name="insertAccessRule">
					<xsl:with-param name="guid" select="$cur/access/@guid2"/>
					<xsl:with-param name="agent" select="$cur/@agent-ip"/>
					<xsl:with-param name="src" select="$cur/@agent-ip"/>
					<xsl:with-param name="dest" select="$local"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<!-- Insert rules unchanged unless Enable the ISAKAMP Self policy if it is not enabled already -->
				<xsl:element name="Policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="RuleId">
						<xsl:value-of select="position()+1"/>
					</xsl:attribute>
					<xsl:attribute name="GUID">
						<xsl:choose>
							<xsl:when test="string-length(@GUID) > 0">
								<xsl:value-of select="@GUID"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="generate-id(node())"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:choose>
						<xsl:when test="fwm:DestPort/fwm:Ports/fwm:PortEntry/fwm:PortName/@value = 'ISAKMP_UDP'">
							<xsl:attribute name="Enabled">true</xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="Enabled">
								<xsl:value-of select="@Enabled"/>
							</xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:copy-of select="@*[name() != 'Enabled'  and name() != 'RuleId' and name() != 'GUID']"/>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template  name="insertAccessRule">
		<xsl:param name="guid"/>
		<xsl:param name="agent"/>
		<xsl:param name="src"/>
		<xsl:param name="dest"/>
        <xsl:param name="logEnabled" select="'false'" />
		<xsl:element name="Policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="RuleId">
				<xsl:number value="2*position()-2"/>
			</xsl:attribute>
			<xsl:attribute name="GUID">
				<xsl:value-of select="$guid"/>
			</xsl:attribute>
			<xsl:attribute name="Enabled">true</xsl:attribute>
			<xsl:attribute name="Action">Allow</xsl:attribute>
			<xsl:attribute name="LogEnabled">
				<xsl:value-of select="$logEnabled"/>
			</xsl:attribute>
			<xsl:attribute name="Comment">VPN Mesh Creator rule.  Allow access from VPN <xsl:value-of select="$agent"/></xsl:attribute>
			<xsl:element name="Protocol" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyProto" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
			<xsl:element name="SourceAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:apply-templates select="$src" mode="insertNetworkNode"/>
			</xsl:element>
			<xsl:element name="SourcePort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
			<xsl:element name="DestAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:apply-templates select="$dest" mode="insertNetworkNode"/>
			</xsl:element>
			<xsl:element name="DestPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template match="member" mode="ipsec">
		<xsl:variable name="local" select="../member[@component=$localComponent]"/>
		<xsl:variable name="occursBefore" select="preceding::member[@network=current()/@network and @component=current()/@component and not(@removed)]"/>
		<xsl:variable name="anythingBefore" select="count($occursBefore/preceding-sibling::node()[@component=$localComponent]) + count($occursBefore/following-sibling::node()[@component=$localComponent])"/>
		<xsl:if test="count($firewall/fwm:IPSECRules[@GUID=current()/ipsec/@guid0])=0">
			<xsl:if test="$anythingBefore = 0">
				<xsl:call-template name="insertIPSecRule">
                    <xsl:with-param name="ruleName"><xsl:value-of select="concat( translate( current()/@agent-ip, '.', '_' ), '_network')"/></xsl:with-param>
					<xsl:with-param name="guid" select="current()/ipsec/@guid0"/>
					<xsl:with-param name="agent" select="current()/@agent-ip"/>
					<xsl:with-param name="srcNode" select="$local"/>
					<xsl:with-param name="destNode" select="current()"/>
					<xsl:with-param name="secGwGuid" select="current()/secgw/@guid0"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
		<xsl:if test="count($firewall/fwm:IPSECRules[@GUID=current()/ipsec/@guid1])=0">
			<xsl:if test="$anythingBefore = 0">
				<xsl:call-template name="insertIPSecRule">
                    <xsl:with-param name="ruleName"><xsl:value-of select="concat( translate( current()/@agent-ip, '.', '_' ), '_to_me')"/></xsl:with-param>
					<xsl:with-param name="guid" select="current()/ipsec/@guid1"/>
					<xsl:with-param name="agent" select="current()/@agent-ip"/>
					<xsl:with-param name="srcNode" select="$local/@agent-ip"/>
					<xsl:with-param name="destNode" select="current()"/>
					<xsl:with-param name="secGwGuid" select="current()/secgw/@guid0"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
		<xsl:if test="count($firewall/fwm:IPSECRules[@GUID=current()/ipsec/@guid2])=0">
			<xsl:if test="$anythingBefore = 0">
				<xsl:call-template name="insertIPSecRule">
                    <xsl:with-param name="ruleName"><xsl:value-of select="concat( 'me_to_', translate( current()/@agent-ip, '.', '_' ) )"/></xsl:with-param>
					<xsl:with-param name="guid" select="current()/ipsec/@guid2"/>
					<xsl:with-param name="agent" select="current()/@agent-ip"/>
					<xsl:with-param name="srcNode" select="$local"/>
					<xsl:with-param name="destNode" select="current()/@agent-ip"/>
					<xsl:with-param name="secGwGuid" select="current()/secgw/@guid0"/>
				</xsl:call-template>
			</xsl:if>
		</xsl:if>
	</xsl:template>

	<xsl:template match="fwm:IPSECRules">
		<xsl:variable name="local" select="$localMesh/member[@component=$localComponent]"/>
		<xsl:choose>
			<xsl:when test="count($localMesh/member[@removed='true']/ipsec[@guid0=current()/@GUID])>0 or ($removedMesh/member[@component=$localComponent]/@removed and $removedMesh/member/ipsec/@guid0=current()/@GUID)">
				<!--Do nothing, we should remove this GUID-->
			</xsl:when>
			<xsl:when test="count($localMesh/member[@removed='true']/ipsec[@guid1=current()/@GUID])>0 or ($removedMesh/member[@component=$localComponent]/@removed and $removedMesh/member/ipsec/@guid1=current()/@GUID)">
				<!-- Do nothing, we should remove this GUID-->
			</xsl:when>
			<xsl:when test="count($localMesh/member[@removed='true']/ipsec[@guid2=current()/@GUID])>0 or ($removedMesh/member[@component=$localComponent]/@removed and $removedMesh/member/ipsec/@guid2=current()/@GUID)">
				<!--Do nothing, we should remove this GUID-->
			</xsl:when>
			<!--If this rule matches with the GUIDs in the VPN mesh document.  Then output the rule again so
			 any changes made to this rule are written out again.-->
			<xsl:when test="count($localMesh/member/ipsec[@guid0=current()/@GUID])>0">
				<xsl:variable name="cur" select="$localMesh/member[ipsec/@guid0=current()/@GUID]"/>
				<xsl:call-template name="insertIPSecRule">
                    <xsl:with-param name="ruleName"><xsl:value-of select="concat( translate( $cur/@agent-ip, '.', '_' ), '_network' )"/></xsl:with-param>
					<xsl:with-param name="guid" select="$cur/ipsec/@guid0"/>
					<xsl:with-param name="agent" select="$cur/@agent-ip"/>
					<xsl:with-param name="srcNode" select="$local"/>
					<xsl:with-param name="destNode" select="$cur"/>
					<xsl:with-param name="secGwGuid" select="$cur/secgw/@guid0"/>
				</xsl:call-template>
			</xsl:when>
			<!--If this rule matches with the GUIDs in the VPN mesh document.  Then output the rule again so 
			 any changes made to this rule are written out again.-->
			<xsl:when test="count($localMesh/member/ipsec[@guid1=current()/@GUID])>0">
				<xsl:variable name="cur" select="$localMesh/member[ipsec/@guid1=current()/@GUID]"/>
				<xsl:call-template name="insertIPSecRule">
                    <xsl:with-param name="ruleName"><xsl:value-of select="concat( translate( $cur/@agent-ip, '.', '_' ), '_to_me')"/></xsl:with-param>
					<xsl:with-param name="guid" select="$cur/ipsec/@guid1"/>
					<xsl:with-param name="agent" select="$cur/@agent-ip"/>
					<xsl:with-param name="srcNode" select="$local/@agent-ip"/>
					<xsl:with-param name="destNode" select="$cur"/>
					<xsl:with-param name="secGwGuid" select="$cur/secgw/@guid0"/>
				</xsl:call-template>
			</xsl:when>
			<!--If this rule matches with the GUIDs in the VPN mesh document.  Then output the rule again so
		                 any changes made to this rule are written out again.-->
			<xsl:when test="count($localMesh/member/ipsec[@guid2=current()/@GUID])>0">
				<xsl:variable name="cur" select="$localMesh/member[ipsec/@guid2=current()/@GUID]"/>
				<xsl:call-template name="insertIPSecRule">
                    <xsl:with-param name="ruleName"><xsl:value-of select="concat( 'me_to_', translate( $cur/@agent-ip, '.', '_' ) )"/></xsl:with-param>
					<xsl:with-param name="guid" select="$cur/ipsec/@guid2"/>
					<xsl:with-param name="agent" select="$cur/@agent-ip"/>
					<xsl:with-param name="srcNode" select="$local"/>
					<xsl:with-param name="destNode" select="$cur/@agent-ip"/>
					<xsl:with-param name="secGwGuid" select="$cur/secgw/@guid0"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<!--This must not be a rule that was output by the mesh so just output it as is.-->
				<xsl:element name="IPSECRules" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="RuleId">
						<xsl:value-of select="position()+1"/>
					</xsl:attribute>
					<xsl:attribute name="GUID">
						<xsl:choose>
							<xsl:when test="string-length(@GUID) > 0">
								<xsl:value-of select="@GUID"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="generate-id(node())"/>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:copy-of select="@*[name() != 'RuleId' and name() != 'GUID']"/>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="insertIPSecRule">
        <xsl:param name="ruleName"/>
		<xsl:param name="guid"/>
		<xsl:param name="agent"/>
		<xsl:param name="srcNode"/>
		<xsl:param name="destNode"/>
		<xsl:param name="secGwGuid"/>
		<xsl:element name="IPSECRules" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="Name"><xsl:value-of select="$ruleName"/></xsl:attribute>
			<xsl:attribute name="GUID">
				<xsl:value-of select="$guid"/>
			</xsl:attribute>
			<xsl:attribute name="RuleId">
				<xsl:number value="2*position()-2"/>
			</xsl:attribute>
			<xsl:attribute name="Enabled">true</xsl:attribute>
			<xsl:attribute name="Comment">VPN Mesh Creator rule.  Allow VPN traffic going to <xsl:value-of select="$agent"/></xsl:attribute>
			<xsl:attribute name="SecurityProcess">APPLY</xsl:attribute>
			<xsl:attribute name="Protocol">ALL</xsl:attribute>
			<xsl:element name="SecurityGateway" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AutoIkeSecGw" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:choose>
						<xsl:when test="count(/fwm:policy/fwm:SecurityGateways/fwm:AutoIkeSecurityGatewayList/fwm:IkeConfig[@RemoteIpAddress=$agent])=0">
							<xsl:attribute name="value"><xsl:call-template name="createIPName"><xsl:with-param name="ip" select="$agent"/></xsl:call-template></xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="value">
                                <xsl:value-of select="/fwm:policy/fwm:SecurityGateways/fwm:AutoIkeSecurityGatewayList[fwm:IkeConfig/@RemoteIpAddress=$agent]/@Name"/>
								<!--<xsl:value-of select="/fwm:policy/fwm:SecurityGateways/fwm:AutoIkeSecurityGatewayList[@GUID=$secGwGuid]/@Name"/> this did not work!-->
							</xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:element>
			<xsl:element name="SourceAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:apply-templates select="$srcNode" mode="insertNetworkNode"/>
			</xsl:element>
			<xsl:element name="SourcePort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
			<xsl:element name="DestAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:apply-templates select="$destNode" mode="insertNetworkNode"/>
			</xsl:element>
			<xsl:element name="DestPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template match="fwm:CommonLists">
		<xsl:variable name="nodesToMesh" select="$localMesh/member[not(@component=$localComponent) and not(@removed) and not( @component = preceding::member[not(@removed) and (following-sibling::member/@component=$localComponent or preceding-sibling::member/@component=$localComponent)]/@component )]"/>
		<xsl:element name="CommonLists" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
            <xsl:apply-templates mode="addNat" select="$nodesToMesh"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="member" mode="addNat">
		<xsl:if test="count($firewall/fwm:CommonLists/fwm:GlobalNatBindList[@GUID=current()/access/@guid0])=0">
			<xsl:apply-templates mode="insertNat" select="current()"/>
		</xsl:if>
	</xsl:template>

	<xsl:template match="member" mode="insertNat">
		<xsl:variable name="local" select="../member[@component=$localComponent]"/>
        <xsl:element name="GlobalNatBindList" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
            <xsl:attribute name="Name"><xsl:call-template name="createIPName"><xsl:with-param name="ip" select="@agent-ip"/></xsl:call-template></xsl:attribute>
            <xsl:attribute name="GUID">
                <xsl:value-of select="access/@guid0"/>
            </xsl:attribute>
            <xsl:attribute name="RuleId">
                <xsl:number value="2*position()-1"/>
            </xsl:attribute>
            <xsl:attribute name="Enabled">true</xsl:attribute>
            <xsl:attribute name="Comment">VPN Mesh Creator rule.  Allow VPN traffic from	<xsl:value-of select="@agent-ip"/></xsl:attribute>
            <xsl:element name="Protocol" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
                <xsl:element name="AnyProto" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
            </xsl:element>
            <xsl:element name="LocalAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
                <xsl:apply-templates select="$local" mode="insertNetworkNode"/>
            </xsl:element>
            <xsl:element name="RemoteAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
                <xsl:apply-templates select="current()" mode="insertNetworkNode"/>
            </xsl:element>
            <xsl:element name="ServicePort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
                <xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
            </xsl:element>
            <xsl:element name="NatIp" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
                <xsl:element name="SameAsIncomingAddr" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
            </xsl:element>
        </xsl:element>
	</xsl:template>

	<xsl:template match="fwm:GlobalNatBindList">
		<xsl:choose>
			<xsl:when test="count($localMesh/member[@removed='true']/access[@guid0=current()/@GUID])>0 or ($removedMesh/member[@component=$localComponent]/@removed and $removedMesh/member/access/@guid0=current()/@GUID)"></xsl:when>
				<!--If this rule matches with the GUIDs in the VPN mesh document.  Then output the rule again so
			                 any changes made to this rule are written out again.-->
			<xsl:when test="count($localMesh/member/access[@guid0=current()/@GUID])>0">
				<xsl:apply-templates mode="insertNat" select="$localMesh/member[access/@guid0=current()/@GUID]"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="GlobalNatBindList" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:copy-of select="@*"/>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="fwm:SecurityGateways">
		<xsl:variable name="nodesToMesh" select="$localMesh/member[not(@component=$localComponent) and not(@removed) and not( @component = preceding::member[not(@removed) and (following-sibling::member/@component=$localComponent or preceding-sibling::member/@component=$localComponent)]/@component )]"/>
		<xsl:element name="SecurityGateways" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:apply-templates mode="insertSecgw" select="$nodesToMesh"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="member" mode="insertSecgw">
<!--		<xsl:if test="count($firewall/fwm:Policy[@GUID=current()/access/@guid0])=0 and not(../member[@component=$localComponent]/@removed)">-->
		<xsl:if test="count($firewall/fwm:Policy[@GUID=current()/access/@guid0])=0">
			<xsl:apply-templates mode="secgw" select="current()"/>
		</xsl:if>
	</xsl:template>

	<xsl:template match="member" mode="secgw">
		<xsl:variable name="local" select="../member[@component=$localComponent]"/>
		<xsl:element name="AutoIkeSecurityGatewayList" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="Name"><xsl:call-template name="createIPName"><xsl:with-param name="ip" select="@agent-ip"/></xsl:call-template></xsl:attribute>
			<xsl:attribute name="Comment">VPN Mesh Creator rule.  Security Gateway to <xsl:value-of select="@agent-ip"/></xsl:attribute>
			<xsl:attribute name="GUID">
				<xsl:value-of select="secgw/@guid0"/>
			</xsl:attribute>
			<xsl:element name="IkeConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:attribute name="Direction">BOTHDIRECTIONS</xsl:attribute>
				<xsl:attribute name="ExchangeType">MAINMODE</xsl:attribute>
				<xsl:attribute name="EncryptionAlg">
					<xsl:value-of select="../ike/@EncryptionAlg"/>
				</xsl:attribute>
				<xsl:if test="../ike/@EncryptionAlg='AES'">
					<xsl:attribute name="AESEncKeyLength">
						<xsl:value-of select="../ike/@AESEncKeyLength"/>
					</xsl:attribute>
				</xsl:if>
				<xsl:attribute name="EncAuthAlg">
					<xsl:value-of select="../ike/@EncAuthAlg"/>
				</xsl:attribute>
				<xsl:attribute name="LifeTimeSecs">7200</xsl:attribute>
				<xsl:attribute name="LifeTimeKBs">0</xsl:attribute>
				<xsl:attribute name="DHGroup">
					<xsl:value-of select="../ike/@DHGroup"/>
				</xsl:attribute>
				<xsl:attribute name="AuthMode">
					<xsl:value-of select="../ike/@AuthMode"/>
				</xsl:attribute>
				<xsl:if test="count(../ike[@AuthMode='Pre-SharedKey']) > 0">
					<xsl:attribute name="PreSharedKey">
						<xsl:value-of select="../ike/@PreSharedKey"/>
					</xsl:attribute>
				</xsl:if>
                <!-- If we are 2.0.0 schema then do this -->
                <xsl:choose>
                    <xsl:when test="starts-with($firewall/@schema-version, '3.0')">
                        <xsl:attribute name="LocalIdType">
                            <xsl:value-of select="$local/@localIDType"/>
                        </xsl:attribute>
                        <xsl:attribute name="LocalIdData">
                            <xsl:value-of select="$local/@localID"/>
                        </xsl:attribute>
                        <xsl:attribute name="LocalIpAddress">
                            <xsl:value-of select="$local/@agent-ip"/>
                        </xsl:attribute>
                        <xsl:attribute name="RemoteIpAddress">
                            <xsl:value-of select="@agent-ip"/>
                        </xsl:attribute>
                        <xsl:element name="RemoteId" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
                            <xsl:attribute name="GUID">
                                <xsl:value-of select="$local/secgw/@guid0"/>
                            </xsl:attribute>
                            <xsl:attribute name="RemoteIdType">
                                <xsl:value-of select="@localIDType"/>
                            </xsl:attribute>
                            <xsl:attribute name="RemoteIdData">
                                <xsl:value-of select="@localID"/>
                            </xsl:attribute>
                        </xsl:element>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:element name="LocalIpAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
                            <xsl:element name="AddrIP" namespace="http://www.iss.net/cml/NetworkProtector/common">
                                <xsl:attribute name="IPAddress"><xsl:value-of select="$local/@agent-ip"/></xsl:attribute>
                            </xsl:element>
                        </xsl:element>

                        <xsl:element name="RemoteIpAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
                            <xsl:element name="AddrIP" namespace="http://www.iss.net/cml/NetworkProtector/common">
                                <xsl:attribute name="IPAddress"><xsl:value-of select="@agent-ip"/></xsl:attribute>
                            </xsl:element>
                        </xsl:element>

                        <xsl:element name="DaLocalId" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
                            <xsl:apply-templates select="$local" mode="SecGwId"/>
                            <!-- $local/@localID -->
                            <!-- $local/@localIDType -->
                        </xsl:element>

                        <xsl:element name="RemoteId" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
                            <xsl:attribute name="GUID">
                                <xsl:value-of select="$local/secgw/@guid0"/>
                            </xsl:attribute>
                            <xsl:attribute name="RemoteIdType">
                                <xsl:value-of select="@localIDType"/>
                            </xsl:attribute>
                            <xsl:element name="DaRemoteId" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
                                <xsl:apply-templates select="current()" mode="SecGwId"/>
                            </xsl:element>
                        </xsl:element>
                    </xsl:otherwise>
                </xsl:choose>
				<xsl:element name="IKEXAuth" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="Enabled">false</xsl:attribute>
				</xsl:element>
				<xsl:element name="OCSP" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="Enabled">false</xsl:attribute>
				</xsl:element>
			</xsl:element>
			<xsl:element name="IpsecConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:attribute name="EncapsulationMode">TUNNEL</xsl:attribute>
				<xsl:attribute name="PerfectForwardSecrecy">
					<xsl:value-of select="../ipsec/@PerfectForwardSecrecy"/>
				</xsl:attribute>
				<xsl:element name="SecurityProposals" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="GUID">
						<xsl:value-of select="ipsec/@guid0"/>
					</xsl:attribute>
					<xsl:attribute name="SecurityProtocol">
						<xsl:value-of select="../ipsec/@SecurityProtocol"/>
					</xsl:attribute>
					<xsl:attribute name="AuthAlgorithm">
						<xsl:value-of select="../ipsec/@AuthAlgorithm"/>
					</xsl:attribute>
					<xsl:if test="../ipsec/@SecurityProtocol= 'ESPWithAuth'">
						<xsl:attribute name="EspAlgorithm">
							<xsl:value-of select="../ipsec/@EspAlgorithm"/>
						</xsl:attribute>
						<xsl:if test="../ipsec/@EspAlgorithm = 'AES'">
							<xsl:attribute name="EspAesKeyLength">
								<xsl:value-of select="../ipsec/@EspAesKeyLength"/>
							</xsl:attribute>
						</xsl:if>
					</xsl:if>
					<xsl:attribute name="LifeTimeSecs">7200</xsl:attribute>
					<xsl:attribute name="LifeTimeKBs">0</xsl:attribute>
				</xsl:element>
				<xsl:element name="Advanced" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="Enabled">false</xsl:attribute>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>

	<xsl:template match="fwm:AutoIkeSecurityGatewayList">
		<xsl:choose>
			<xsl:when test="count($localMesh/member[@removed='true']/secgw[@guid0=current()/@GUID])>0 or ($removedMesh/member[@component=$localComponent]/@removed and $removedMesh/member/secgw/@guid0=current()/@GUID)"></xsl:when>
				<!--If this rule matches with the GUIDs in the VPN mesh document.  Then output the rule again so 
				 any changes made to this rule are written out again.-->
			<xsl:when test="count($localMesh/member/secgw[@guid0=current()/@GUID])>0">
				<xsl:apply-templates mode="secgw" select="$localMesh/member[secgw/@guid0=current()/@GUID]"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="AutoIkeSecurityGatewayList" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:copy-of select="@*"/>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="@agent-ip" mode="insertNetworkNode">
		<xsl:element name="SingleAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="IPSingleAddress">
				<xsl:value-of select="."/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="member" mode="insertNetworkNode">
		<xsl:choose>
			<xsl:when test="@network">
				<xsl:element name="AddressMask" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="IPAddressMask">
						<xsl:value-of select="@network"/>
					</xsl:attribute>
				</xsl:element>
			</xsl:when>
			<xsl:when test="@range">
				<xsl:element name="AddressRange" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="IPRangeAddress">
						<xsl:value-of select="@range"/>
					</xsl:attribute>
				</xsl:element>
			</xsl:when>
			<xsl:when test="@single">
				<xsl:element name="SingleAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="IPSingleAddress">
						<xsl:value-of select="@single"/>
					</xsl:attribute>
				</xsl:element>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

    <xsl:template match="member" mode="SecGwId">
        <xsl:choose>
            <xsl:when test="@localIDType='IPADDRESS'">
                <xsl:element name="IPADDRESS" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
                    <xsl:attribute name="IPAddress"><xsl:value-of select="@localID"/></xsl:attribute>
                </xsl:element>
            </xsl:when>
            <xsl:when test="@localIDType='FQDN'">
                <xsl:element name="FQDN" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
                    <xsl:attribute name="value"><xsl:value-of select="@localID"/></xsl:attribute>
                </xsl:element>
            </xsl:when>
            <xsl:when test="@localIDType='USERFQDN'">
                <xsl:element name="USERFQDN" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
                    <xsl:attribute name="value"><xsl:value-of select="@localID"/></xsl:attribute>
                </xsl:element>
            </xsl:when>
            <xsl:when test="@localIDType='DERASN1DN'">
                <xsl:element name="DERASN1DN" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
                    <xsl:attribute name="value"><xsl:value-of select="@localID"/></xsl:attribute>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="@localIDType"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


	<!--Copy Element Template-->
	<xsl:template match="@*|node()">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*"/>
			</xsl:element>
		</xsl:if>
	</xsl:template>

	<xsl:template match="npcommon:*">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/common">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*" />
			</xsl:element>
		</xsl:if>
	</xsl:template>

	<xsl:template name="createIPName"><xsl:param name="ip"/>to_<xsl:value-of select="translate( $ip, '.', '_' )"/></xsl:template>
</xsl:stylesheet>
