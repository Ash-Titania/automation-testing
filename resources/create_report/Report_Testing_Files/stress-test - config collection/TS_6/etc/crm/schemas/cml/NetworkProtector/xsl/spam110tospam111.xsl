<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:spam="http://www.iss.net/cml/NetworkProtector/spam">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="spam:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/spam">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">1.1.1</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="spam:SubjectTagFormat">
		<xsl:element name="SubjectTagFormat" namespace="http://www.iss.net/cml/NetworkProtector/spam">
			<xsl:attribute name="SubjBelowThresholdFormat">[SPAM]</xsl:attribute>
			<xsl:attribute name="SubjAboveThresholdFormat">[SPAM+]</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
