<schema targetNamespace='http://www.iss.net/cml/NetworkProtector/srv'
    xmlns='http://www.w3.org/2001/XMLSchema'
    xmlns:PolicyBase='http://www.iss.net/cml/PolicyBase'
    xmlns:common='http://www.iss.net/cml/common'
    xmlns:srv='http://www.iss.net/cml/NetworkProtector/srv'
    xmlns:npcommon='http://www.iss.net/cml/NetworkProtector/common'
    xmlns:cpe='http://www.iss.net/cml/cpe'
    elementFormDefault='qualified'
    version='2.0.0'>

    <annotation>
        <appinfo><agent name='Network Protector'/></appinfo>
        <documentation>Schema for Network Protector Services Information.</documentation>
    </annotation>

    <!-- Import the abstract base type for all configuration documents. -->
    <import namespace='http://www.iss.net/cml/PolicyBase' schemaLocation='../../PolicyBase/PolicyBase2_3_2.xsd'/>

    <!-- Import the common schemas for common elements and attributes. -->
    <import namespace='http://www.iss.net/cml/common' schemaLocation='../../common/common2_1_1.xsd'/>    
    <import namespace='http://www.iss.net/cml/NetworkProtector/common' schemaLocation='../common/common1_1_0.xsd'/>

    <simpleType name='ColonOnlyMacAddress'>
        <restriction base='normalizedString'>
            <pattern value='[0-9a-fA-F]{2}(:[0-9a-fA-F]{2}){5}'/>
        </restriction>
    </simpleType>
	
	<simpleType name='SnmpTrapVersionType'>
		<restriction base='normalizedString'>
			<enumeration value='V1'/>
			<enumeration value='V2C'/>
		</restriction>
	</simpleType>


	<simpleType name='InterfaceChooser'
		cpe:classname='net.iss.cpe.plugins.ExternalLacAndLinkSimpleType'
		cpe:listDocument='npnetwork1_0.xml'
		cpe:namespace='http://www.iss.net/cml/NetworkProtector/network'
		cpe:schemaVersion='2.0' 
		cpe:listParent='.policy' 
		cpe:listName='Interface' 
		cpe:listAttributeName='Name'
		cpe:allowLink='false'>
        <restriction base="string"/>
    </simpleType>

    <complexType name='DNSServersType'>
        <attribute name='PrimaryDNS' type='common:DottedStringIpv4Address' use='required' />
        <attribute name='SecondaryDNS' type='common:DottedStringIpv4Address' />
        <attribute name='TertiaryDNS' type='common:DottedStringIpv4Address' />
    </complexType>
    
    <complexType name='DNSSearchPathListType'>
		<attribute name='Id' type='common:OrderedListOrdinal' cpe:hidden='true' use='required'/>
        <attribute name='DomainName' type='npcommon:FQDNType' use='required' />
    </complexType>

    <complexType name='SshConfigType'>
        <attribute name='enableSSH' type='boolean' default='true' />
    </complexType>

	<complexType name='DHCPAddressRangeType'>
		<attribute name='AddressRange' type='common:Ipv4AddressRange' use='required' />
		<attribute name='SubnetMask' type='common:DottedStringIpv4Address' use='required'/>
        <attribute name='GatewayIpAddress' type='common:DottedStringIpv4Address' use='required' />
	</complexType>
	
	<complexType name='DHCPStaticAssignType'>
		<attribute name='Hostname' type='normalizedString' use='required' />
		<attribute name='MACAddress' type='srv:ColonOnlyMacAddress' default='00:00:00:00:00:00'/>
		<attribute name='IPAddress' type='common:DottedStringIpv4Address' use='required'/>
	</complexType>
	
    <complexType name='DHCPDNSType'>
        <choice>
            <element name='UseDefaultSettings'>
                <complexType/>
            </element>
            <element name='UseSpecificSettings'>
                <complexType>
                    <sequence>
                        <element name='DNSServers' type='srv:DNSServersType' />
                    </sequence>
                </complexType>
            </element>
        </choice>
    </complexType>

    <complexType name='WinsServersType'>
        <attribute name='PreferredWINS' type='common:DottedStringIpv4Address' />
        <attribute name='AlternateWINS' type='common:DottedStringIpv4Address' />
    </complexType>

	<complexType name='DHCPType'>
		<sequence>
			<element name='AddressRanges' type='srv:DHCPAddressRangeType' minOccurs='0' maxOccurs='unbounded' cpe:depends='Enabled' cpe:onlyRequiredWhenEnabled='true' />
            <element name='DNS' type='srv:DHCPDNSType'  cpe:depends='Enabled' />
			<element name='StaticAssigns' type='srv:DHCPStaticAssignType' minOccurs='0' maxOccurs='unbounded'  cpe:depends='Enabled' />
			<element name='WinsServers' type='srv:WinsServersType' minOccurs='0' cpe:depends='Enabled' />
		</sequence>
		<attribute name='Enabled' type='boolean' default='true'/>
		<attribute name='ClientLeaseTime' type='int' default='259200'  cpe:depends='Enabled' />
		<attribute name='DomainNameSuffix' type='npcommon:OptionalFQDNType' cpe:depends='Enabled' />
	</complexType>

	<complexType name='DHCPLeaseType'>
		<attribute name='HostName' type='normalizedString' cpe:readOnly='true'/>
		<attribute name='HostIpAddr' type='normalizedString' cpe:readOnly='true'/>
		<attribute name='HostMacAddr' type='normalizedString' cpe:readOnly='true'/>
		<attribute name='Start' type='normalizedString' cpe:readOnly='true'/>
		<attribute name='End' type='normalizedString' cpe:readOnly='true'/>
	</complexType>

	<complexType name='DHCPServerIPAddressType'>
		<attribute name='DHCPServerIPAddress' type='common:DottedStringIpv4Address' />
	</complexType>

	<complexType name='DHCPRelayType'>
		<sequence>
			<element name='Interface' minOccurs='0' maxOccurs='7' cpe:depends='Enabled' cpe:onlyRequiredWhenEnabled='true'>
				<complexType>
					<attribute name='InterfaceName' type='srv:InterfaceChooser' use='required' />
				</complexType>
			</element>
			<element name='Addresses' type='srv:DHCPServerIPAddressType' minOccurs='0' maxOccurs='unbounded' cpe:depends='Enabled' cpe:onlyRequiredWhenEnabled='true' />
		</sequence>
		<attribute name='Enabled' type='boolean' default='false' />
	</complexType>

    <complexType name='ServiceConfigType'>
		<sequence>
		    <element name='SshConfig' type='srv:SshConfigType' minOccurs='1' />
    		<element name='DHCPRelayConfig' type='srv:DHCPRelayType' minOccurs='1' />
		</sequence>
	</complexType>

	<complexType name="SmtpConfigType">
		<sequence>
			<element name="RelayIPs" minOccurs="0" maxOccurs="unbounded">
				<complexType>
					<attribute name="IPAddressMask" type="common:Ipv4AddressMask" use="required"/>
				</complexType>
			</element>
			<element name="LocalDomains" minOccurs="0" maxOccurs="unbounded">
				<complexType>
					<attribute name="DomainName" type="npcommon:FQDNType" use="required"/>
				</complexType>
			</element>
		</sequence>
		<attribute name="MyDomain" type="npcommon:OptionalFQDNType"/>
	</complexType>

	<simpleType name="PortNumberType">
		<restriction base="unsignedInt">
			<minInclusive value="0"/>
			<maxInclusive value="65535"/>
		</restriction>
	</simpleType>
	
	<complexType name="ProxySettingsType">
		<attribute name="UseProxySettings" type="boolean" default="false"/>
		<attribute name="ProxyAddress" type="npcommon:FQDNType" cpe:depends="UseProxySettings" cpe:dependsValue="true" cpe:onlyRequiredWhenEnabled="true"/>
		<attribute name="ProxyPort" type="srv:PortNumberType" cpe:depends="UseProxySettings" cpe:dependsValue="true" cpe:onlyRequiredWhenEnabled="true"/>
		<attribute name="UseProxyAuth" type="boolean" cpe:depends="UseProxySettings" cpe:onlyRequiredWhenEnabled="true"/>
		<attribute name="ProxyUser" type="npcommon:LongStringType" cpe:depends="UseProxyAuth" cpe:dependsValue="true" cpe:onlyRequiredWhenEnabled="true"/>
		<attribute name="ProxyPwd" type="npcommon:LongStringType" cpe:depends="UseProxyAuth" cpe:dependsValue="true" cpe:onlyRequiredWhenEnabled="true"/>
	</complexType>

	<complexType name="SnmpSettingsType">
		<attribute name="Enabled" type="boolean" default="false"/>
		<attribute name="SystemName" type="npcommon:OptionalNameType" cpe:depends="Enabled" default='Unknown' />
		<attribute name="SystemLocation" type="npcommon:OptionalNameType" cpe:depends="Enabled" default='Unknown' />
		<attribute name="SystemContact" type="npcommon:OptionalNameType" cpe:depends="Enabled" default='Unknown' />
		<attribute name="GetCommunity" type="npcommon:NameType" cpe:depends="Enabled" cpe:onlyRequiredWhenEnabled="true"/>
        <attribute name="TrapEnabled" type="boolean" default="false"/>
		<attribute name='TrapAddress' type='npcommon:NameType' cpe:depends="TrapEnabled" cpe:onlyRequiredWhenEnabled="true"/>
        <attribute name='TrapCommunity' type='npcommon:NameType' cpe:depends="TrapEnabled" cpe:onlyRequiredWhenEnabled="true" />
		<attribute name='TrapVersion' type='srv:SnmpTrapVersionType' cpe:depends="TrapEnabled" default='V1'/>
	</complexType>

    <complexType name='SrvPolicy'>
        <complexContent>
            <extension base='PolicyBase:PolicyBase'>
                <sequence>
        		    <element name='ServiceConfig' type='srv:ServiceConfigType' minOccurs='1' cpe:forceLeaf='true' />
        		    <element name='DHCPConfig' type='srv:DHCPType' minOccurs='1' maxOccurs='1' cpe:forceLeaf='true' cpe:preventCheckboxInTree='true' />
					<element name="SmtpConfig" type="srv:SmtpConfigType" minOccurs="0" cpe:preventCheckboxInTree="true"/>
					<element name="HttpProxy" type="srv:ProxySettingsType" minOccurs="0" cpe:preventCheckboxInTree="true"/>
					<element name="SnmpConfig" type="srv:SnmpSettingsType" minOccurs="0" cpe:preventCheckboxInTree="true"/>
        		    <element name='TuningSetting' type='npcommon:TuningSettingType' minOccurs='0' maxOccurs='unbounded' />
        		    <element name='DHCPStatus' type='srv:DHCPLeaseType' minOccurs='0' maxOccurs='unbounded' cpe:forceLeaf='true' cpe:displayOnlyList='true'/>
                </sequence>
            </extension>
        </complexContent>
    </complexType>

    <element name='policy' type='srv:SrvPolicy' substitutionGroup='PolicyBase:policy-base'/>

</schema>
