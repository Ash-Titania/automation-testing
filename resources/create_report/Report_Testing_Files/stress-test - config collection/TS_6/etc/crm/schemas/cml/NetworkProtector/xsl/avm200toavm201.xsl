<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:avm="http://www.iss.net/cml/NetworkProtector/avm" xmlns:exclude="http://www.iss.net/cml/NetworkProtector/exclude" xmlns:npcommon="http://www.iss.net/cml/NetworkProtector/common">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="extens" select=" 'avm201excludeextensions.xml' "/>
	<xsl:template match="avm:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/avm">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">2.1.0</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="avm:Config">
		<xsl:element name="Config" namespace="http://www.iss.net/cml/NetworkProtector/avm">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates/>
			<xsl:apply-templates select="document($extens)/exclude:ExcludeExtensions"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="exclude:ExcludeExtensions">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="exclude:*">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/avm">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*" />
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<xsl:template match="avm:TuningSetting">
		<xsl:choose>
			<xsl:when test="starts-with(@name, 'ftp_proxy.') or starts-with(@name, 'http_proxy.') or starts-with(@name, 'pop3_proxy.') or starts-with(@name, 'smtp_proxy.') ">
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="TuningSetting" namespace="http://www.iss.net/cml/NetworkProtector/avm">
					<xsl:copy-of select="@*"/>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
