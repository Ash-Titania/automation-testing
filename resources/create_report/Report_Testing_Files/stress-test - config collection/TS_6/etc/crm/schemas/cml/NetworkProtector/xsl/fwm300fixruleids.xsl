<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fwm="http://www.iss.net/cml/NetworkProtector/fwm" >
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:template match="fwm:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:for-each select="fwm:Policy">
				<xsl:element name="Policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="RuleId"><xsl:value-of select="position()-1"/></xsl:attribute>
					<xsl:attribute name="GUID">
						<xsl:choose>
							<xsl:when test="string-length(@GUID) > 0"><xsl:value-of select="@GUID"/></xsl:when>
							<xsl:otherwise><xsl:value-of select="generate-id()"/></xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
					<xsl:copy-of select="@*[name() != 'RuleId' and name() != 'GUID'] "/>
					<xsl:apply-templates/>
				</xsl:element>
				</xsl:for-each>
				<xsl:apply-templates select="fwm:IPSECRules"/>
				<xsl:apply-templates select="fwm:CommonLists"/>
				<xsl:apply-templates select="fwm:SecurityGateways"/>
				<xsl:apply-templates select="fwm:MessageConfig"/>
				<xsl:apply-templates select="fwm:ProxyRedirectRule"/>
				<xsl:apply-templates select="fwm:VpnAdvancedSettings"/>
				<xsl:apply-templates select="fwm:TuningSetting"/>
			</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	</xsl:stylesheet>
