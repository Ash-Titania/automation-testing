<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:crm="http://www.iss.net/cml/NetworkProtector/crm"
xmlns:common="http://www.iss.net/cml/NetworkProtector/common" >
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="crm:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/crm">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">2.0.1</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>	<xsl:template match="common:string">
		<xsl:element name="string" namespace="http://www.iss.net/cml/NetworkProtector/common">
			<xsl:copy-of select="@*[name()!='value'] "/>
			<xsl:attribute name="value">
				<xsl:choose>
					<xsl:when test="@value='/tmp/CrmTrace.txt'">/var/spool/crm/CrmTrace.txt</xsl:when>
					<xsl:when test="@value='/tmp/CrmCommTrace.txt'">/var/spool/crm/CrmCommTrace.txt</xsl:when>
					<xsl:otherwise><xsl:value-of select="@value"/></xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>				
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
