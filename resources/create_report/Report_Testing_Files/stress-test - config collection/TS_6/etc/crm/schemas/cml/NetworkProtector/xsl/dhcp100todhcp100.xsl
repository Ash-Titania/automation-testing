<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:dhcp="http://www.iss.net/cml/NetworkProtector/dhcp" xmlns:srv="http://www.iss.net/cml/NetworkProtector/srv" xmlns:npcommon="http://www.iss.net/cml/NetworkProtector/common">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="with" select="'npsrv1_0.xml'"/>
	<xsl:template match="dhcp:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/dhcp">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates select="document($with)/srv:policy/srv:ServiceConfig/srv:DHCPRelayConfig"/>
			<xsl:apply-templates select="document($with)/srv:policy/srv:DHCPConfig"/>
			<xsl:apply-templates select="document($with)/srv:policy/srv:TuningSetting"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/dhcp">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*" />
			</xsl:element>
		</xsl:if>
	</xsl:template>	
	<xsl:template match="npcommon:*">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/common">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*" />
			</xsl:element>
		</xsl:if>
	</xsl:template>	
	<xsl:template match="srv:DHCPConfig">
		<xsl:element name="DHCPConfig" namespace="http://www.iss.net/cml/NetworkProtector/dhcp">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="srv:DHCPRelayConfig">
		<xsl:element name="ServiceConfig" namespace="http://www.iss.net/cml/NetworkProtector/dhcp">
			<xsl:element name="DHCPRelayConfig" namespace="http://www.iss.net/cml/NetworkProtector/dhcp">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates/>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template match="srv:TunningSetting">
		<xsl:element name="TunningSetting" namespace="http://www.iss.net/cml/NetworkProtector/dhcp">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
