<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:crm="http://www.iss.net/cml/NetworkProtector/crm" xmlns:srv="http://www.iss.net/cml/NetworkProtector/srv">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:variable name="RelayControl">
		<xsl:choose>
			<xsl:when test="count(srv:policy/srv:SmtpConfig/srv:LocalDomains)>0 and count(srv:policy/srv:SmtpConfig/srv:RelayIPs)>0 ">true</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:template match="srv:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/srv">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">2.1.0</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="srv:SmtpConfig">
		<xsl:element name="SmtpConfig" namespace="http://www.iss.net/cml/NetworkProtector/srv">
			<xsl:copy-of select="@*"/>
			<xsl:attribute name="RelayControl"><xsl:value-of select="$RelayControl"/></xsl:attribute>
			<xsl:attribute name="SMTPLocalAuthentication">false</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="srv:DHCPStatus"/>
	<xsl:template match="srv:DHCPConfig"/>
	<xsl:template match="srv:DHCPRelayConfig"/>
	<xsl:template match="srv:TuningSetting"/>
</xsl:stylesheet>
