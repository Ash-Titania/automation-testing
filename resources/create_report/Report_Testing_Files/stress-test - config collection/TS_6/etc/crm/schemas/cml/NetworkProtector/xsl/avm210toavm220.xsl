<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:avm="http://www.iss.net/cml/NetworkProtector/avm" xmlns:srv="http://www.iss.net/cml/NetworkProtector/srv" xmlns:npcommon="http://www.iss.net/cml/NetworkProtector/common">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="srvxml" select=" 'npsrv1_0.xml' "/>
	<!-- Handle tuning parms that are no longer protocol specific -->
	<xsl:variable name="httpmaxsub" select="avm:policy/avm:TuningSetting/npcommon:number/@value[../../@name='http.av_max_subfiles'] "/>
	<xsl:variable name="ftpmaxsub" select="avm:policy/avm:TuningSetting/npcommon:number/@value[../../@name='ftp.av_max_subfiles'] "/>
	<xsl:variable name="smtpmaxsub" select="avm:policy/avm:TuningSetting/npcommon:number/@value[../../@name='smtp.av_max_subfiles'] "/>
	<xsl:variable name="pop3maxsub" select="avm:policy/avm:TuningSetting/npcommon:number/@value[../../@name='pop3.av_max_subfiles'] "/>
	<xsl:variable name="maxsub">
		<xsl:call-template name="maxvalue">
			<xsl:with-param name="v1" select="$httpmaxsub"/>
			<xsl:with-param name="v2" select="$ftpmaxsub"/>
			<xsl:with-param name="v3" select="$smtpmaxsub"/>
			<xsl:with-param name="v4" select="$pop3maxsub"/>
			<xsl:with-param name="default" select=" '1000' "/>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="httpmaxkexpand" select="avm:policy/avm:TuningSetting/npcommon:number/@value[../../@name='http.av_max_k_expand'] "/>
	<xsl:variable name="ftpmaxkexpand" select="avm:policy/avm:TuningSetting/npcommon:number/@value[../../@name='ftp.av_max_k_expand'] "/>
	<xsl:variable name="smtpmaxkexpand" select="avm:policy/avm:TuningSetting/npcommon:number/@value[../../@name='smtp.av_max_k_expand'] "/>
	<xsl:variable name="pop3maxkexpand" select="avm:policy/avm:TuningSetting/npcommon:number/@value[../../@name='pop3.av_max_k_expand'] "/>
	<xsl:variable name="maxkexpand">
		<xsl:call-template name="maxvalue">
			<xsl:with-param name="v1" select="$httpmaxkexpand"/>
			<xsl:with-param name="v2" select="$ftpmaxkexpand"/>
			<xsl:with-param name="v3" select="$smtpmaxkexpand"/>
			<xsl:with-param name="v4" select="$pop3maxkexpand"/>
			<xsl:with-param name="default" select=" '20480' "/>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="httpmaxkclassifloops" select="avm:policy/avm:TuningSetting/npcommon:number/@value[../../@name='http.av_max_k_classifloops'] "/>
	<xsl:variable name="ftpmaxkclassifloops" select="avm:policy/avm:TuningSetting/npcommon:number/@value[../../@name='ftp.av_max_k_classifloops'] "/>
	<xsl:variable name="smtpmaxkclassifloops" select="avm:policy/avm:TuningSetting/npcommon:number/@value[../../@name='smtp.av_max_k_classifloops'] "/>
	<xsl:variable name="pop3maxkclassifloops" select="avm:policy/avm:TuningSetting/npcommon:number/@value[../../@name='pop3.av_max_k_classifloops'] "/>
	<xsl:variable name="maxkclassifloops">
		<xsl:call-template name="maxvalue">
			<xsl:with-param name="v1" select="$httpmaxkclassifloops"/>
			<xsl:with-param name="v2" select="$ftpmaxkclassifloops"/>
			<xsl:with-param name="v3" select="$smtpmaxkclassifloops"/>
			<xsl:with-param name="v4" select="$pop3maxkclassifloops"/>
			<xsl:with-param name="default" select=" '500' "/>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="httpmaxscantime" select="avm:policy/avm:TuningSetting/npcommon:number/@value[../../@name='http.av_max_scan_time'] "/>
	<xsl:variable name="ftpmaxscantime" select="avm:policy/avm:TuningSetting/npcommon:number/@value[../../@name='ftp.av_max_scan_time'] "/>
	<xsl:variable name="smtpmaxscantime" select="avm:policy/avm:TuningSetting/npcommon:number/@value[../../@name='smtp.av_max_scan_time'] "/>
	<xsl:variable name="pop3maxscantime" select="avm:policy/avm:TuningSetting/npcommon:number/@value[../../@name='pop3.av_max_scan_time'] "/>
	<xsl:variable name="maxscantime">
		<xsl:call-template name="maxvalue">
			<xsl:with-param name="v1" select="$httpmaxscantime"/>
			<xsl:with-param name="v2" select="$ftpmaxscantime"/>
			<xsl:with-param name="v3" select="$smtpmaxscantime"/>
			<xsl:with-param name="v4" select="$pop3maxscantime"/>
			<xsl:with-param name="default" select=" '30' "/>
		</xsl:call-template>
	</xsl:variable>
	<xsl:template match="avm:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/avm">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">2.2.0</xsl:attribute>
			<xsl:apply-templates/>
			<xsl:element name="TuningSetting" namespace="{namespace-uri(.)}">
				<xsl:attribute name="name">avm.se.smtp.psmtpd.address_transparent</xsl:attribute>
				<xsl:element name="boolean" namespace="http://www.iss.net/cml/NetworkProtector/common">
					<xsl:attribute name="value">false</xsl:attribute>
				</xsl:element>
			</xsl:element>
			<xsl:element name="TuningSetting" namespace="{namespace-uri(.)}">
				<xsl:attribute name="name">avm.se.pop3.ppop3d.address_transparent</xsl:attribute>
				<xsl:element name="boolean" namespace="http://www.iss.net/cml/NetworkProtector/common">
					<xsl:attribute name="value">false</xsl:attribute>
				</xsl:element>
			</xsl:element>
			<xsl:element name="TuningSetting" namespace="{namespace-uri(.)}">
				<xsl:attribute name="name">avm.se.ftp.pftpd.address_transparent</xsl:attribute>
				<xsl:element name="boolean" namespace="http://www.iss.net/cml/NetworkProtector/common">
					<xsl:attribute name="value">false</xsl:attribute>
				</xsl:element>
			</xsl:element>
			<xsl:element name="TuningSetting" namespace="{namespace-uri(.)}">
				<xsl:attribute name="name">avm.se.http.phttpd.address_transparent</xsl:attribute>
				<xsl:element name="boolean" namespace="http://www.iss.net/cml/NetworkProtector/common">
					<xsl:attribute name="value">false</xsl:attribute>
				</xsl:element>
			</xsl:element>
			<xsl:if test="$maxsub != 'noneset' ">
				<xsl:element name="TuningSetting" namespace="{namespace-uri(.)}">
					<xsl:attribute name="name">avm.se.scanner.scannerd.max_subfiles</xsl:attribute>
					<xsl:element name="number" namespace="http://www.iss.net/cml/NetworkProtector/common">
						<xsl:attribute name="value"><xsl:value-of select="$maxsub"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:if>
			<xsl:if test="$maxkexpand != 'noneset' ">
				<xsl:element name="TuningSetting" namespace="{namespace-uri(.)}">
					<xsl:attribute name="name">avm.se.scanner.scannerd.max_k_expand</xsl:attribute>
					<xsl:element name="number" namespace="http://www.iss.net/cml/NetworkProtector/common">
						<xsl:attribute name="value"><xsl:value-of select="$maxkexpand"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:if>
			<xsl:if test="$maxkclassifloops != 'noneset' ">
				<xsl:element name="TuningSetting" namespace="{namespace-uri(.)}">
					<xsl:attribute name="name">avm.se.scanner.scannerd.max_k_classifloops</xsl:attribute>
					<xsl:element name="number" namespace="http://www.iss.net/cml/NetworkProtector/common">
						<xsl:attribute name="value"><xsl:value-of select="$maxkclassifloops"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:if>
			<xsl:if test="$maxscantime != 'noneset' ">
				<xsl:element name="TuningSetting" namespace="{namespace-uri(.)}">
					<xsl:attribute name="name">avm.se.scanner.scannerd.max_scan_time</xsl:attribute>
					<xsl:element name="number" namespace="http://www.iss.net/cml/NetworkProtector/common">
						<xsl:attribute name="value"><xsl:value-of select="$maxscantime"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:if>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="avm:TuningSetting">
		<xsl:variable name="tunename">
			<xsl:choose>
				<xsl:when test="@name='issavd.cache_entries' ">avm.se.scanner.scannerd.file_cache_size</xsl:when>
				<xsl:when test="@name='issavd.cache_timeout' ">avm.se.scanner.scannerd.file_cache_timeout</xsl:when>
				<xsl:when test="@name='issavd.ip_cache_entries' ">avm.se.scanner.scannerd.ip_cache_size</xsl:when>
				<xsl:when test="@name='issavd.ip_cache_timeout' ">avm.se.scanner.scannerd.ip_cache_timeout</xsl:when>
				<xsl:when test="@name='issavd.virus_timeout' ">avm.se.scanner.scannerd.virus_cache_timeout</xsl:when>
				<xsl:when test="@name='issavd. internal_if' ">avm.se.scanner.scannerd.internal_interfaces</xsl:when>
				<xsl:when test="@name='http.prefix' ">avm.se.http.phttpd.scan_prefix</xsl:when>
				<xsl:when test="@name='ftp.max_size' ">avm.se.ftp.pftpd.max_msg_size</xsl:when>
				<xsl:when test="@name='smtp.prefix' ">avm.se.smtp.psmtpd.scan_prefix</xsl:when>
				<xsl:when test="@name='quarantine.max_age' ">avm.se.quarantine.quarantine.max_age</xsl:when>
				<xsl:when test="@name='quarantine.dir_size' ">avm.se.quarantine.quarantine.dir_size</xsl:when>
				<xsl:when test="@name='quarantine.active' ">avm.se.quarantine.quarantine.active</xsl:when>
				<xsl:when test="@name='quarantine.scan_frequency' ">avm.se.quarantine.quarantine.scan_frequency</xsl:when>
				<xsl:when test="@name='quarantine.quarantine_dir' ">avm.se.common.quarantine.quarantine_dir</xsl:when>
				<xsl:when test="@name='MessageWall.max_message_size' ">avm.se.smtp.psmtpd.max_msg_size</xsl:when>
				<xsl:when test="@name='MessageWall.max_rcpt' ">avm.se.smtp.psmtpd.max_rcpt</xsl:when>
				<xsl:otherwise>notset</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$tunename = 'avm.se.ftp.pftpd.max_msg_size' ">
				<xsl:element name="TuningSetting" namespace="{namespace-uri(.)}">
					<xsl:attribute name="name"><xsl:value-of select="$tunename"/></xsl:attribute>
					<xsl:copy-of select="@*[name() != 'name']"/>
					<xsl:element name="number" namespace="http://www.iss.net/cml/NetworkProtector/common">
						<xsl:attribute name="value"><xsl:value-of select="child::npcommon:number/@value*1048576"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
				<xsl:element name="TuningSetting" namespace="{namespace-uri(.)}">
					<xsl:attribute name="name">avm.se.ftp.pftpd.max_scan_size</xsl:attribute>
					<xsl:copy-of select="@*[name() != 'name']"/>
					<xsl:element name="number" namespace="http://www.iss.net/cml/NetworkProtector/common">
						<xsl:attribute name="value"><xsl:value-of select="child::npcommon:number/@value*1048576"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:when test="$tunename != 'notset' ">
				<xsl:element name="TuningSetting" namespace="{namespace-uri(.)}">
					<xsl:attribute name="name"><xsl:value-of select="$tunename"/></xsl:attribute>
					<xsl:copy-of select="@*[name() != 'name']"/>
					<xsl:apply-templates/>
				</xsl:element>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="maxvalue">
		<xsl:param name="v1"/>
		<xsl:param name="v2"/>
		<xsl:param name="v3"/>
		<xsl:param name="v4"/>
		<xsl:param name="default" />
		<xsl:choose>
			<xsl:when test="number($v1)>number($v2) and number($v1)>number($v3) and number($v1)>number($v4) and number($v1)>number($default)">
				<xsl:value-of select="$v1"/>
			</xsl:when>
			<xsl:when test="number($v2)>number($v1) and number($v2)>number($v3) and number($v2)>number($v4) and number($v2)>number($default)">
				<xsl:value-of select="$v2"/>
			</xsl:when>
			<xsl:when test="number($v3)>number($v1) and number($v3)>number($v2) and number($v3)>number($v4) and number($v3)>number($default)">
				<xsl:value-of select="$v3"/>
			</xsl:when>
			<xsl:when test="number($v4)>number($v1) and number($v4)>number($v2) and number($v4)>number($v3) and number($v4)>number($default)">
				<xsl:value-of select="$v4"/>
			</xsl:when>
			<xsl:otherwise>noneset</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
