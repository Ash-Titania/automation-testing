<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:net="http://www.iss.net/cml/NetworkProtector/network" xmlns:route="http://www.iss.net/cml/NetworkProtector/route" xmlns:dynobj="http://www.iss.net/cml/NetworkProtector/dynobj">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="routexml" select="'nproute1_0.xml'"/>
	<xsl:variable name="routes" select="document($routexml)/route:policy/route:RoutingConfig"/>
	<xsl:template match="net:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/dynobj">
			<xsl:copy-of select="@*"/>
			<xsl:call-template name="insertdefaultdynamicaddressobjects"/>
			<!-- Handle Old Network Policy Schema Format (the normal case) -->
			<xsl:apply-templates select="/net:policy/net:InternalConfig"/>
			<xsl:apply-templates select="/net:policy/net:DMZConfig"/>
			<!-- On the off chance they have not edited the old network policy handle the new format as it will be the format of the network file we are reading. -->
			<xsl:apply-templates select="/net:policy/net:RoutingModeConfig/net:InternalConfig/net:InternalInterface"/>
			<xsl:call-template name="processroutes"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template name="insertdefaultdynamicaddressobjects">
		<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkProtector/dynobj">
			<xsl:attribute name="Name">CORP</xsl:attribute>
			<xsl:attribute name="type">CORP</xsl:attribute>
		</xsl:element>
		<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkProtector/dynobj">
			<xsl:attribute name="Name">DMZ</xsl:attribute>
			<xsl:attribute name="type">DMZ</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="net:InternalConfig">
		<xsl:choose>
			<xsl:when test="@Enabled='true'">
				<xsl:element name="DynamicAddressList" namespace="http://www.iss.net/cml/NetworkProtector/dynobj">
					<xsl:attribute name="Name">CORP</xsl:attribute>
					<xsl:attribute name="GUID"><xsl:value-of select="generate-id()"/></xsl:attribute>
					<xsl:element name="AddressSubnet" namespace="http://www.iss.net/cml/NetworkProtector/dynobj">
						<xsl:attribute name="IPAddress"><xsl:value-of select="@IPAddress"/></xsl:attribute>
						<xsl:attribute name="SubnetMask"><xsl:value-of select="@SubnetMask"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="net:DMZConfig">
		<xsl:choose>
			<xsl:when test="@Enabled='true'">
				<xsl:element name="DynamicAddressList" namespace="http://www.iss.net/cml/NetworkProtector/dynobj">
					<xsl:attribute name="Name">DMZ</xsl:attribute>
					<xsl:attribute name="GUID"><xsl:value-of select="generate-id()"/></xsl:attribute>
					<xsl:element name="AddressSubnet" namespace="http://www.iss.net/cml/NetworkProtector/dynobj">
						<xsl:attribute name="IPAddress"><xsl:value-of select="@IPAddress"/></xsl:attribute>
						<xsl:attribute name="SubnetMask"><xsl:value-of select="@SubnetMask"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="net:InternalInterface">
		<xsl:element name="DynamicAddressList" namespace="http://www.iss.net/cml/NetworkProtector/dynobj">
			<xsl:attribute name="Name">CORP</xsl:attribute>
			<xsl:attribute name="GUID"><xsl:value-of select="generate-id()"/></xsl:attribute>
			<xsl:element name="AddressSubnet" namespace="http://www.iss.net/cml/NetworkProtector/dynobj">
				<xsl:attribute name="IPAddress"><xsl:value-of select="@IPAddress"/></xsl:attribute>
				<xsl:attribute name="SubnetMask"><xsl:value-of select="@SubnetMask"/></xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="processroutes">
		<xsl:variable name="InternalEnabled" select="/net:policy/net:InternalConfig/@Enabled"/>
		<xsl:variable name="InternalIp" select="/net:policy/net:InternalConfig/@IPAddress"/>
		<xsl:variable name="InternalSubnet" select="/net:policy/net:InternalConfig/@SubnetMask"/>
		<xsl:variable name="DmzIp" select="/net:policy/net:DMZConfig/@IPAddress"/>
		<xsl:variable name="DmzSubnet" select="/net:policy/net:DMZConfig/@SubnetMask"/>
		<xsl:variable name="DmzEnabled" select="/net:policy/net:DMZConfig/@Enabled"/>
		<xsl:for-each select="$routes">
			<!-- Check to see if the GW of the route is in the CORP network and if so add necessary content -->
			<xsl:if test="count($InternalIp)>0 and count($InternalSubnet)>0">
				<xsl:call-template name="CheckGwInNetwork">
					<xsl:with-param name="NetIp" select="$InternalIp"/>
					<xsl:with-param name="NetMask" select="$InternalSubnet"/>
					<xsl:with-param name="NetEnabled" select="$InternalEnabled"/>
					<xsl:with-param name="NetName" select="'CORP' "/>
					<xsl:with-param name="GwIp" select="@GatewayIpAddress"/>
					<xsl:with-param name="DestIpAddress" select="@DestIpAddress"/>
					<xsl:with-param name="DestNetMask" select="@SubnetMask"/>
					<xsl:with-param name="pos" select="position()"/>
				</xsl:call-template>
			</xsl:if>
			<!-- Check to see if the GW of the route is in the DMZ network and if so add necessary content -->
			<xsl:if test="count($DmzIp)>0 and count($DmzSubnet)>0">
				<xsl:call-template name="CheckGwInNetwork">
					<xsl:with-param name="NetIp" select="$DmzIp"/>
					<xsl:with-param name="NetMask" select="$DmzSubnet"/>
					<xsl:with-param name="NetEnabled" select="$DmzEnabled"/>
					<xsl:with-param name="NetName" select="'DMZ' "/>
					<xsl:with-param name="GwIp" select="@GatewayIpAddress"/>
					<xsl:with-param name="DestIpAddress" select="@DestIpAddress"/>
					<xsl:with-param name="DestNetMask" select="@SubnetMask"/>
					<xsl:with-param name="pos" select="position()"/>
				</xsl:call-template>
			</xsl:if>			
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="CheckGwInNetwork">
		<xsl:param name="NetIp" />
		<xsl:param name="NetMask" />
		<xsl:param name="NetEnabled" />
		<xsl:param name="NetName" />
		<xsl:param name="GwIp" />
		<xsl:param name="DestIpAddress" />
		<xsl:param name="DestNetMask" />
		<xsl:param name="pos" />
		<xsl:variable name="ipo1">
			<xsl:value-of select="substring-before($NetIp, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ipo2to4">
			<xsl:value-of select="substring-after($NetIp, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ipo2">
			<xsl:value-of select="substring-before($ipo2to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ipo3to4">
			<xsl:value-of select="substring-after($ipo2to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ipo3">
			<xsl:value-of select="substring-before($ipo3to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ipo4">
			<xsl:value-of select="substring-after($ipo3to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="sno1">
			<xsl:value-of select="substring-before($NetMask, '.' )"/>
		</xsl:variable>
		<xsl:variable name="sno2to4">
			<xsl:value-of select="substring-after($NetMask, '.' )"/>
		</xsl:variable>
		<xsl:variable name="sno2">
			<xsl:value-of select="substring-before($sno2to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="sno3to4">
			<xsl:value-of select="substring-after($sno2to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="sno3">
			<xsl:value-of select="substring-before($sno3to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="sno4">
			<xsl:value-of select="substring-after($sno3to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ripo1">
			<xsl:value-of select="substring-before($GwIp, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ripo2to4">
			<xsl:value-of select="substring-after($GwIp, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ripo2">
			<xsl:value-of select="substring-before($ripo2to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ripo3to4">
			<xsl:value-of select="substring-after($ripo2to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ripo3">
			<xsl:value-of select="substring-before($ripo3to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ripo4">
			<xsl:value-of select="substring-after($ripo3to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="o1ok">
			<xsl:call-template name="checkOctet">
				<xsl:with-param name="sno" select="$sno1"/>
				<xsl:with-param name="ipo" select="$ipo1"/>
				<xsl:with-param name="gwo" select="$ripo1"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="o2ok">
			<xsl:call-template name="checkOctet">
				<xsl:with-param name="sno" select="$sno2"/>
				<xsl:with-param name="ipo" select="$ipo2"/>
				<xsl:with-param name="gwo" select="$ripo2"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="o3ok">
			<xsl:call-template name="checkOctet">
				<xsl:with-param name="sno" select="$sno3"/>
				<xsl:with-param name="ipo" select="$ipo3"/>
				<xsl:with-param name="gwo" select="$ripo3"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="o4ok">
			<xsl:call-template name="checkOctet">
				<xsl:with-param name="sno" select="$sno4"/>
				<xsl:with-param name="ipo" select="$ipo4"/>
				<xsl:with-param name="gwo" select="$ripo4"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:if test="$o1ok='true' and $o2ok='true' and $o3ok='true' and $o4ok='true' ">
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkProtector/dynobj">
				<xsl:attribute name="Name"><xsl:value-of select="concat($NetName,$pos)"/></xsl:attribute>
				<xsl:attribute name="type"><xsl:value-of select="$NetName"/></xsl:attribute>
			</xsl:element>
			<xsl:if test="$NetEnabled = 'true' ">
				<xsl:element name="DynamicAddressList" namespace="http://www.iss.net/cml/NetworkProtector/dynobj">
					<xsl:attribute name="Name"><xsl:value-of select="concat($NetName,position())"/></xsl:attribute>
					<xsl:attribute name="GUID"><xsl:value-of select="generate-id()"/></xsl:attribute>
					<xsl:element name="AddressSubnet" namespace="http://www.iss.net/cml/NetworkProtector/dynobj">
						<xsl:attribute name="IPAddress"><xsl:value-of select="$DestIpAddress"/></xsl:attribute>
						<xsl:attribute name="SubnetMask"><xsl:value-of select="$DestNetMask"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:if>
		</xsl:if>			
	</xsl:template>
	<xsl:template name="checkOctet">
		<xsl:param name="sno"/>
		<xsl:param name="ipo" />
		<xsl:param name="gwo"/>
		<xsl:choose>
			<xsl:when test="$sno='0' ">true</xsl:when>
			<xsl:when test="$sno='255'">
				<xsl:choose>
					<xsl:when test="$ipo = $gwo">true</xsl:when>
					<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<!-- Need to get the bit values of each ip address and based on the subnet make sure the relvant ones match -->
				<xsl:variable name="ip8"><xsl:value-of select="floor( $ipo div 128) mod 2"/></xsl:variable>
				<xsl:variable name="gw8"><xsl:value-of select="floor($gwo div 128) mod 2"/></xsl:variable>
				<xsl:variable name="sn8"><xsl:value-of select="floor($sno div 128) mod 2"/></xsl:variable>
				<xsl:variable name="ip7"><xsl:value-of select="floor($ipo div 64) mod 2"/></xsl:variable>
				<xsl:variable name="gw7"><xsl:value-of select="floor($gwo div 64) mod 2"/></xsl:variable>
				<xsl:variable name="sn7"><xsl:value-of select="floor($sno div 64) mod 2"/></xsl:variable>
				<xsl:variable name="ip6"><xsl:value-of select="floor($ipo div 32) mod 2"/></xsl:variable>
				<xsl:variable name="gw6"><xsl:value-of select="floor($gwo div 32) mod 2"/></xsl:variable>
				<xsl:variable name="sn6"><xsl:value-of select="floor($sno div 32) mod 2"/></xsl:variable>
				<xsl:variable name="ip5"><xsl:value-of select="floor($ipo div 16) mod 2"/></xsl:variable>
				<xsl:variable name="gw5"><xsl:value-of select="floor($gwo div 16) mod 2"/></xsl:variable>
				<xsl:variable name="sn5"><xsl:value-of select="floor($sno div 16) mod 2"/></xsl:variable>
				<xsl:variable name="ip4"><xsl:value-of select="floor($ipo div 8) mod 2"/></xsl:variable>
				<xsl:variable name="gw4"><xsl:value-of select="floor($gwo div 8) mod 2"/></xsl:variable>
				<xsl:variable name="sn4"><xsl:value-of select="floor($sno div 8) mod 2"/></xsl:variable>
				<xsl:variable name="ip3"><xsl:value-of select="floor($ipo div 4) mod 2"/></xsl:variable>
				<xsl:variable name="gw3"><xsl:value-of select="floor($gwo div 4) mod 2"/></xsl:variable>
				<xsl:variable name="sn3"><xsl:value-of select="floor($sno div 4) mod 2"/></xsl:variable>
				<xsl:variable name="ip2"><xsl:value-of select="floor($ipo div 2) mod 2"/></xsl:variable>
				<xsl:variable name="gw2"><xsl:value-of select="floor($gwo div 2) mod 2"/></xsl:variable>
				<xsl:variable name="sn2"><xsl:value-of select="floor($sno div 2) mod 2"/></xsl:variable>
				<xsl:variable name="ip1"><xsl:value-of select="$ipo mod 2"/></xsl:variable>
				<xsl:variable name="gw1"><xsl:value-of select="$gwo mod 2"/></xsl:variable>
				<xsl:variable name="sn1"><xsl:value-of select="$sno mod 2"/></xsl:variable>
				<xsl:variable name="b1ok">
					<xsl:call-template name="testbit">
						<xsl:with-param name="snb" select="$sn1"/>
						<xsl:with-param name="ipb" select="$ip1"/>
						<xsl:with-param name="gwb" select="$gw1"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="b2ok">
					<xsl:call-template name="testbit">
						<xsl:with-param name="snb" select="$sn2"/>
						<xsl:with-param name="ipb" select="$ip2"/>
						<xsl:with-param name="gwb" select="$gw2"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="b3ok">
					<xsl:call-template name="testbit">
						<xsl:with-param name="snb" select="$sn3"/>
						<xsl:with-param name="ipb" select="$ip3"/>
						<xsl:with-param name="gwb" select="$gw3"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="b4ok">
					<xsl:call-template name="testbit">
						<xsl:with-param name="snb" select="$sn4"/>
						<xsl:with-param name="ipb" select="$ip4"/>
						<xsl:with-param name="gwb" select="$gw4"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="b5ok">
					<xsl:call-template name="testbit">
						<xsl:with-param name="snb" select="$sn5"/>
						<xsl:with-param name="ipb" select="$ip5"/>
						<xsl:with-param name="gwb" select="$gw5"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="b6ok">
					<xsl:call-template name="testbit">
						<xsl:with-param name="snb" select="$sn6"/>
						<xsl:with-param name="ipb" select="$ip6"/>
						<xsl:with-param name="gwb" select="$gw6"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="b7ok">
					<xsl:call-template name="testbit">
						<xsl:with-param name="snb" select="$sn7"/>
						<xsl:with-param name="ipb" select="$ip7"/>
						<xsl:with-param name="gwb" select="$gw7"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="b8ok">
					<xsl:call-template name="testbit">
						<xsl:with-param name="snb" select="$sn8"/>
						<xsl:with-param name="ipb" select="$ip8"/>
						<xsl:with-param name="gwb" select="$gw8"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="$b1ok='true' and $b2ok='true' and $b3ok='true' and $b4ok='true' and $b5ok='true' and $b6ok='true' and $b7ok='true' and $b8ok='true' ">true</xsl:when>
					<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="testbit">
		<xsl:param name="snb"/>
		<xsl:param name="ipb" />
		<xsl:param name="gwb" />
		<xsl:choose>
			<xsl:when test="$snb=1">
				<xsl:choose>
					<xsl:when test="$ipb=$gwb">true</xsl:when>
					<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>true</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
