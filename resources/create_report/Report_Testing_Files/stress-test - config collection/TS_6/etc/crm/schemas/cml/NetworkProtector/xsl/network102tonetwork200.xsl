<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:net="http://www.iss.net/cml/NetworkProtector/network">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="net:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/network">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">2.0.0</xsl:attribute>
			<xsl:apply-templates select="net:ExternalInterface"/>
			<!--
			<xsl:element name="Interface" namespace="http://www.iss.net/cml/NetworkProtector/network"><xsl:attribute name="Name">eth0</xsl:attribute></xsl:element>
			<xsl:element name="Interface" namespace="http://www.iss.net/cml/NetworkProtector/network"><xsl:attribute name="Name">eth1</xsl:attribute></xsl:element>
			<xsl:element name="Interface" namespace="http://www.iss.net/cml/NetworkProtector/network"><xsl:attribute name="Name">eth2</xsl:attribute></xsl:element>
			-->
			<xsl:element name="RoutingModeConfig" namespace="http://www.iss.net/cml/NetworkProtector/network">
				<xsl:apply-templates select="net:ExternalConfig" />
				<xsl:element name="InternalConfig" namespace="http://www.iss.net/cml/NetworkProtector/network">
					<xsl:apply-templates select="net:InternalConfig" />
					<xsl:apply-templates select="net:DMZConfig"/>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template match="net:ExternalConfigNew">
		<xsl:element name="ExternalConfig" namespace="http://www.iss.net/cml/NetworkProtector/network">
				<xsl:attribute name="HostName"><xsl:value-of select="@HostName"/></xsl:attribute>
				<xsl:element name="ExternalInterface" namespace="http://www.iss.net/cml/NetworkProtector/network">
					<xsl:copy-of select="@*[name() != 'HostName']"/>
					<xsl:apply-templates select="net:IPAddress"/>
				</xsl:element>
				<xsl:apply-templates select="net:ExternalDNS"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="net:InternalConfig | net:DMZConfig">
		<xsl:element name="InternalInterface" namespace="http://www.iss.net/cml/NetworkProtector/network">
			<xsl:copy-of select="@*"/>
			<xsl:choose>
				<xsl:when test="@InterfaceName='eth0' ">
					<xsl:attribute name="noDelete">true</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="noDelete">false</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
