<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:dynobj="http://www.iss.net/cml/NetworkProtector/dynobj"
xmlns:my="http://www.iss.net/cml/NetworkObjects">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="dynobjxml" select=" 'npdynobj.xml' " />
	<xsl:variable name="dynaddrobjs" select="document($dynobjxml)/dynobj:policy/dynobj:DynamicAddressObjectList"/>
	<xsl:template match="my:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkObjects">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">1.1.0</xsl:attribute>
			<xsl:call-template name="insertgroup">
				<xsl:with-param name="type" select=" 'CORP' "/>
			</xsl:call-template>
			<xsl:call-template name="insertgroup">
				<xsl:with-param name="type" select=" 'DMZ' "/>
			</xsl:call-template>
			<xsl:apply-templates/>
			<xsl:apply-templates select="$dynaddrobjs"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="my:DynamicAddressObjectList"/>
	<xsl:template match="@*|node()">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*" />
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<xsl:template match="dynobj:DynamicAddressObjectList">
		<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
			<xsl:attribute name="Name"><xsl:value-of select="@Name"/></xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template name="insertgroup">
		<xsl:param name="type" />
	<xsl:variable name="objsingroup" select="$dynaddrobjs[@type=$type]"/>
	<xsl:if test="count($objsingroup) > 1" >
		<xsl:element name="AddressGroupList" namespace="http://www.iss.net/cml/NetworkObjects">
			<xsl:attribute name="Name"><xsl:value-of select="$objsingroup[1]/@Name"/></xsl:attribute>
			<xsl:for-each select="$objsingroup">
				<xsl:element name="AddressGroup" namespace="http://www.iss.net/cml/NetworkObjects">
					<xsl:element name="DynamicAddressName" namespace="http://www.iss.net/cml/NetworkObjects">
						<xsl:attribute name="value"><xsl:value-of select="@Name"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:for-each>
		</xsl:element>
	</xsl:if>
	</xsl:template>
</xsl:stylesheet>
