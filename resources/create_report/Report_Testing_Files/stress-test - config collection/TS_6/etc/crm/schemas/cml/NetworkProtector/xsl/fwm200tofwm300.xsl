<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:fwm="http://www.iss.net/cml/NetworkProtector/fwm" 
xmlns:dynobj="http://www.iss.net/cml/NetworkProtector/dynobj" 
xmlns:net="http://www.iss.net/cml/NetworkProtector/network" 
xmlns:avm="http://www.iss.net/cml/NetworkProtector/avm" 
xmlns:common="http://www.iss.net/cml/NetworkProtector/common" 
xmlns:self="http://www.iss.net/cml/NetworkProtector/self" 
xmlns:proxy="http://www.iss.net/cml/NetworkProtector/proxy"  
xmlns:cflts="http://www.iss.net/cml/NetworkProtector/cflts"  
xmlns:spam="http://www.iss.net/cml/NetworkProtector/spam" 
xmlns:tune="http://www.iss.net/cml/NetworkProtector/tune" >
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="netxml" select="'npnetwork1_0.xml'"/>
	<xsl:param name="avmxml" select=" 'npavm1_0.xml' "/>
	<xsl:param name="cfltsxml" select=" 'npcflts1_0.xml' " />
	<xsl:param name="spamxml" select=" 'npspam1_0.xml' " />
	<xsl:param name="dynobjxml" select=" 'npdynobj.xml' "/>
	<xsl:param name="newself" select=" 'fwm300selfpoliciestoinsert.xml' "/>
	<xsl:param name="newproxy" select=" 'fwm300proxyredirectrules.xml' "/>
	<xsl:param name="proxytune" select=" 'fwm300proxyredirecttuning.xml' "/>
	<xsl:param name="newtune" select=" 'fwm300tuningparmstoinsert.xml' "/>
	<xsl:param name="miggroup" select=" 'false' "/>
	<xsl:variable name="dmzEnabled">
		<xsl:choose>
			<xsl:when test="$miggroup='true' ">true</xsl:when>
			<xsl:when test="count(document($netxml)/net:policy/net:DMZConfig)=0">false</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="document($netxml)/net:policy/net:DMZConfig/@Enabled"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="corpEnabled">
		<xsl:choose>
			<xsl:when test="$miggroup='true' ">true</xsl:when>
			<xsl:when test="count(document($netxml)/net:policy/net:InternalConfig)=0">false</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="document($netxml)/net:policy/net:InternalConfig/@Enabled"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="httpProxyEnabled">
		<xsl:choose>
			<xsl:when test="document($cfltsxml)/cflts:policy/cflts:Config/@cfltEnabled='true' ">true</xsl:when>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@httpEnabled='true' ">true</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="smtpProxyEnabled">
		<xsl:choose>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@smtpEnabled='true' ">true</xsl:when>
			<xsl:when test="document($spamxml)/spam:policy/spam:Config/@aspmEnabled='true' ">true</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="ftpProxyEnabled">
		<xsl:choose>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@ftpEnabled='true' ">true</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="pop3ProxyEnabled">
		<xsl:choose>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@pop3Enabled='true' ">true</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="dynaddrobjs" select="document($dynobjxml)/dynobj:policy/dynobj:DynamicAddressObjectList"/>
	<xsl:variable name="objsincorp" select="$dynaddrobjs[@type='CORP']"/>
	<xsl:variable name="objsindmz" select="$dynaddrobjs[@type='DMZ']"/>
	<xsl:variable name="corpaddr">
		<xsl:choose>
			<xsl:when test="count($objsincorp) > 1">
				<xsl:element name="DynamicAddressName" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="value"><xsl:value-of select=" 'CORP' "/></xsl:attribute>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="AddressGroup" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="value"><xsl:value-of select=" 'CORP' "/></xsl:attribute>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:template match="fwm:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">3.0.0</xsl:attribute>
			<!-- We migrate all Inbound Rules including Self so that they take precedence over Outbound Rules since we discard Direction -->
			<xsl:apply-templates select="fwm:InternalPolicy/fwm:Inbound"/>
			<xsl:apply-templates select="fwm:DmzPolicy/fwm:Inbound"/>
			<xsl:apply-templates select="fwm:SelfPolicy"/>
			<xsl:apply-templates select="fwm:InternalPolicy/fwm:Outbound"/>
			<xsl:apply-templates select="fwm:DmzPolicy/fwm:Outbound"/>
			<xsl:apply-templates select="document($newself)/self:selfpoliciestoinsert"/>
			<xsl:apply-templates select="fwm:IPSECRules"/>
			<xsl:element name="CommonLists" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<!-- Insert Global NAT Bind Rules for any Outbound policy with NAT enabled on it -->
				<!-- Removed in place of deprecated element usage.
				<xsl:variable name="internaloutboundnat" select="/fwm:policy/fwm:InternalPolicy/fwm:Outbound[fwm:NatName/@NatEnabled='true']"/>
				<xsl:call-template name="insertnatbindrules">
					<xsl:with-param name="rules" select="$internaloutboundnat"/>
					<xsl:with-param name="nameprefix" select=" 'nbinternal' "/>
					<xsl:with-param name="dynaddr" select=" 'CORP' " />
					<xsl:with-param name="objsindynaddr" select="$objsincorp" />
				</xsl:call-template>
				<xsl:variable name="dmzoutboundnat" select="/fwm:policy/fwm:DmzPolicy/fwm:Outbound[fwm:NatName/@NatEnabled='true']"/>
				<xsl:call-template name="insertnatbindrules">
					<xsl:with-param name="rules" select="$dmzoutboundnat"/>
					<xsl:with-param name="nameprefix" select=" 'nbdmz' "/>
					<xsl:with-param name="dynaddr" select=" 'DMZ' " />
					<xsl:with-param name="objsindynaddr" select="$objsindmz" />
				</xsl:call-template>
				-->
				<!--Insert Global Reverse NAT Rules for any Inbound policy with NAT enabled on it -->
				<!-- Removed in place of deprecated element usage.
				<xsl:variable name="internalinboundnat" select="/fwm:policy/fwm:InternalPolicy/fwm:Inbound[fwm:NatName/@NatEnabled='true']"/>
				<xsl:call-template name="insertreversenatrules">
					<xsl:with-param name="rules" select="$internalinboundnat"/>
					<xsl:with-param name="nameprefix" select=" 'rninternal' "/>
					<xsl:with-param name="dynaddr" select=" 'CORP' " />
					<xsl:with-param name="objsindynaddr" select="$objsincorp" />
				</xsl:call-template>
				<xsl:variable name="dmzinboundnat" select="/fwm:policy/fwm:DmzPolicy/fwm:Inbound[fwm:NatName/@NatEnabled='true']"/>
				<xsl:call-template name="insertreversenatrules">
					<xsl:with-param name="rules" select="$dmzinboundnat"/>
					<xsl:with-param name="nameprefix" select=" 'rndmz' "/>
					<xsl:with-param name="dynaddr" select=" 'DMZ' " />
					<xsl:with-param name="objsindynaddr" select="$objsindmz" />
				</xsl:call-template>
				-->
				<!-- Now add the NAT Configuration List -->
				<xsl:apply-templates select="fwm:CommonLists/fwm:NatList"/>
			</xsl:element>
			<xsl:element name="SecurityGateways" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:variable name="manualsecgwdata" select="/fwm:policy/fwm:IPSECRules[count(fwm:ManualAutoParms/fwm:ManualIpsec) > 0]"/>
				<xsl:variable name="autosecgwdata" select="/fwm:policy/fwm:IPSECRules[count(fwm:ManualAutoParms/fwm:AutomaticIpsec) > 0]"/>
				<xsl:call-template name="insertautosecgws2"/>
				<!--
				<xsl:call-template name="insertautosecgws">
					<xsl:with-param name="autosecgwdata" select="$autosecgwdata"/>
				</xsl:call-template>
				<xsl:call-template name="insertextraautoikegws">
				</xsl:call-template>
				-->
				<xsl:call-template name="insertvpnaddrsecgws"/>
				<xsl:call-template name="insert-manualsecgws">
					<xsl:with-param name="manualsecgwdata" select="$manualsecgwdata"/>
				</xsl:call-template>
			</xsl:element>
			<xsl:apply-templates select="fwm:MessageConfig"/>
			<!-- Do not migrate proxy tuning parms now just insert defaults <xsl:call-template name="insert-proxyredirectrules"/> -->
			<xsl:apply-templates select="document($newproxy)/proxy:proxyredirectrules"/>
			<xsl:element name="VpnAdvancedSettings" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:apply-templates select="fwm:RadiusConfig"/>
				<xsl:apply-templates select="fwm:CommonLists/fwm:UserList"/>
				<!-- <xsl:call-template name="insert-ocspprovs"/> -->
			</xsl:element>
			<xsl:apply-templates select="fwm:TuningSetting"/>
			<xsl:apply-templates select="document($newtune)/tune:tuning"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:param name="dynaddr"/>
		<xsl:param name="objsindynaddr"/>
		<xsl:param name="dir"/>
		<xsl:param name="addrType"/>
		<xsl:param name="ruleType"/>
		<xsl:param name="protocol"/>
		<xsl:param name="portNameInUse"/>
		<xsl:param name="natEnabled" />
		<xsl:copy>
			<xsl:apply-templates select="@*|node()">
				<xsl:with-param name="dynaddr" select="$dynaddr"/>
				<xsl:with-param name="objsindynaddr" select="$objsindynaddr"/>
				<xsl:with-param name="dir" select="$dir"/>
				<xsl:with-param name="addrType" select="$addrType"/>
				<xsl:with-param name="ruleType" select="$ruleType"/>
				<xsl:with-param name="protocol" select="$protocol"/>
				<xsl:with-param name="portNameInUse" select="$portNameInUse"/>
				<xsl:with-param name="natEnabled" select="$natEnabled"/>
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="fwm:InternalPolicy/fwm:Inbound">
		<xsl:call-template name="copy-policy-rule">
			<xsl:with-param name="dynaddr" select=" 'CORP' "/>
			<xsl:with-param name="objsindynaddr" select="$objsincorp"/>
			<xsl:with-param name="dir" select=" 'Inbound' "/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template match="fwm:InternalPolicy/fwm:Outbound">
		<xsl:call-template name="copy-policy-rule">
			<xsl:with-param name="dynaddr" select=" 'CORP' "/>
			<xsl:with-param name="objsindynaddr" select="$objsincorp"/>
			<xsl:with-param name="dir" select=" 'Outbound' "/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template match="fwm:DmzPolicy/fwm:Inbound">
		<xsl:call-template name="copy-policy-rule">
			<xsl:with-param name="dynaddr" select=" 'DMZ' "/>
			<xsl:with-param name="objsindynaddr" select="$objsindmz"/>
			<xsl:with-param name="dir" select=" 'Inbound' "/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template match="fwm:DmzPolicy/fwm:Outbound">
		<xsl:call-template name="copy-policy-rule">
			<xsl:with-param name="dynaddr" select=" 'DMZ' "/>
			<xsl:with-param name="objsindynaddr" select="$objsindmz"/>
			<xsl:with-param name="dir" select=" 'Outbound' "/>
		</xsl:call-template>
	</xsl:template>
	<xsl:template match="fwm:SelfPolicy">
		<xsl:if test="count(fwm:DestAddress/fwm:AnyAddress) = 0">
			<xsl:message>WARNING: Encountered a Self Rule with a Dest Address that was set to something other than Any.  In the migrated Access Rule rule the Dest Address is being ignored is set to Self . Please verfiy that the resulting rule does not cause any problems.</xsl:message>
		</xsl:if>
		<xsl:element name="Policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*[name() != 'Action' and name() != 'Network' and name() != 'Enabled' ]"/>
			<xsl:attribute name="Action"><xsl:choose><xsl:when test="@Action = 'Accept'">Allow</xsl:when><xsl:otherwise>Reject</xsl:otherwise></xsl:choose></xsl:attribute>
			<xsl:attribute name="Enabled">
				<xsl:choose>
					<xsl:when test="@Network='EXT' "><xsl:value-of select="@Enabled"/></xsl:when>
					<xsl:when test="@Network='CORP' and $corpEnabled = 'true' and @Enabled='true' ">true</xsl:when>
					<xsl:when test="@Network='DMZ' and $dmzEnabled = 'true' and @Enabled='true' ">true</xsl:when>
					<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:variable name="portNameInUse">
				<xsl:choose>
					<xsl:when test="count(fwm:DestPort/fwm:PortName)>0">true</xsl:when>
					<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:apply-templates select="fwm:Protocol">
				<xsl:with-param name="portNameInUse" select="$portNameInUse"/>
			</xsl:apply-templates>
			<xsl:choose>
				<xsl:when test="@Network='CORP' ">
					<xsl:apply-templates select="fwm:SourceAddress">
						<xsl:with-param name="dynaddr" select=" 'CORP' "/>
						<xsl:with-param name="objsindynaddr" select="$objsincorp"/>
						<xsl:with-param name="dir" select=" 'Inbound' "/>
						<xsl:with-param name="addrType" select=" 'Source' "/>
						<xsl:with-param name="ruleType" select=" 'Self' "/>
					</xsl:apply-templates>
				</xsl:when>
				<xsl:when test="@Network='DMZ' ">
					<xsl:apply-templates select="fwm:SourceAddress">
						<xsl:with-param name="dynaddr" select=" 'DMZ' "/>
						<xsl:with-param name="objsindynaddr" select="$objsindmz"/>
						<xsl:with-param name="dir" select=" 'Inbound' "/>
						<xsl:with-param name="addrType" select=" 'Source' "/>
						<xsl:with-param name="ruleType" select=" 'Self' "/>
					</xsl:apply-templates>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="fwm:SourceAddress">
						<xsl:with-param name="ruleType" select=" 'Access' "/>
					</xsl:apply-templates>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="fwm:SourcePort">
				<xsl:with-param name="ruleType" select=" 'Access' "/>
			</xsl:apply-templates>
			<xsl:element name="DestAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="Self" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
			<xsl:apply-templates select="fwm:DestPort">
				<xsl:with-param name="ruleType" select=" 'Access' "/>
			</xsl:apply-templates>
			<!-- Add deprecated element -->
			<xsl:element name="Deprecated" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:attribute name="Enabled">false</xsl:attribute>
				<xsl:attribute name="Direction">Inbound</xsl:attribute>
				<xsl:element name="NatName" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="NatEnabled">false</xsl:attribute>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template name="copy-policy-rule">
		<xsl:param name="dynaddr"/>
		<xsl:param name="objsindynaddr"/>
		<xsl:param name="dir"/>
		<xsl:element name="Policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*[name() != 'Action' and name() !='Enabled' ]"/>
			<xsl:attribute name="Action"><xsl:choose><xsl:when test="@Action = 'Accept'">Allow</xsl:when><xsl:otherwise>Reject</xsl:otherwise></xsl:choose></xsl:attribute>
			<xsl:attribute name="Enabled">
				<xsl:choose>
					<xsl:when test="$dynaddr = 'CORP' and $corpEnabled = 'true' and @Enabled='true'">true</xsl:when>
					<xsl:when test="$dynaddr = 'DMZ' and $dmzEnabled = 'true' and @Enabled='true'">true</xsl:when>
					<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:variable name="portNameInUse">
				<xsl:choose>
					<xsl:when test="count(fwm:DestPort/fwm:PortName)>0">true</xsl:when>
					<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="natEnabled">
				<xsl:choose>
					<xsl:when test="fwm:NatName/@NatEnabled='true' ">true</xsl:when>
					<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:apply-templates select="fwm:Protocol">
				<xsl:with-param name="portNameInUse" select="$portNameInUse"/>
			</xsl:apply-templates>
			<xsl:apply-templates select="fwm:SourceAddress">
				<xsl:with-param name="dynaddr" select="$dynaddr"/>
				<xsl:with-param name="objsindynaddr" select="$objsindynaddr"/>
				<xsl:with-param name="dir" select="$dir"/>
				<xsl:with-param name="ruleType" select=" 'Access' "/>
				<xsl:with-param name="natEnabled" select="$natEnabled"/>
			</xsl:apply-templates>
			<xsl:apply-templates select="fwm:SourcePort">
				<xsl:with-param name="ruleType" select=" 'Access' "/>
			</xsl:apply-templates>
			<xsl:apply-templates select="fwm:DestAddress">
				<xsl:with-param name="dynaddr" select="$dynaddr"/>
				<xsl:with-param name="objsindynaddr" select="$objsindynaddr"/>
				<xsl:with-param name="dir" select="$dir"/>
				<xsl:with-param name="ruleType" select=" 'Access' "/>
				<xsl:with-param name="natEnabled" select="$natEnabled"/>
			</xsl:apply-templates>
			<xsl:apply-templates select="fwm:DestPort">
				<xsl:with-param name="ruleType" select=" 'Access' "/>
			</xsl:apply-templates>
			<!-- Add deprecated element -->
			<xsl:element name="Deprecated" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:attribute name="Enabled">true</xsl:attribute>
				<xsl:attribute name="Direction"><xsl:value-of select="$dir"/></xsl:attribute>
				<xsl:apply-templates select="fwm:NatName"/>
			</xsl:element>			
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:Protocol">
		<xsl:param name="portNameInUse" select=" 'false' "/>
		<xsl:element name="Protocol" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:choose>
				<xsl:when test="$portNameInUse = 'true'">
					<xsl:element name="AnyProto" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:SourceAddress">
		<xsl:param name="dynaddr" select=" 'none' "/>
		<xsl:param name="objsindynaddr" select=" '0' "/>
		<xsl:param name="dir" select=" 'none' "/>
		<xsl:param name="ruleType" select=" 'none' "/>
		<xsl:param name="natEnabled" select=" 'false' "/>
		<xsl:element name="SourceAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates>
				<xsl:with-param name="dynaddr" select="$dynaddr"/>
				<xsl:with-param name="objsindynaddr" select="$objsindynaddr"/>
				<xsl:with-param name="dir" select="$dir"/>
				<xsl:with-param name="ruleType" select="$ruleType"/>
				<xsl:with-param name="addrType" select=" 'Source' "/>
				<xsl:with-param name="natEnabled" select="$natEnabled"/>
			</xsl:apply-templates>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:DestAddress">
		<xsl:param name="dynaddr" select=" 'none' "/>
		<xsl:param name="objsindynaddr" select=" '0' "/>
		<xsl:param name="dir" select=" 'none' "/>
		<xsl:param name="ruleType" select=" 'none' "/>
		<xsl:param name="natEnabled" select=" 'false' "/>
		<xsl:element name="DestAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates>
				<xsl:with-param name="dynaddr" select="$dynaddr"/>
				<xsl:with-param name="objsindynaddr" select="$objsindynaddr"/>
				<xsl:with-param name="dir" select="$dir"/>
				<xsl:with-param name="ruleType" select="$ruleType"/>
				<xsl:with-param name="addrType" select=" 'Dest' "/>
				<xsl:with-param name="natEnabled" select="$natEnabled"/>
			</xsl:apply-templates>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:AnyAddress">
		<xsl:param name="dynaddr" select="'none' "/>
		<xsl:param name="objsindynaddr" select=" '0' "/>
		<xsl:param name="dir" select=" 'none' "/>
		<xsl:param name="ruleType" select=" 'none' "/>
		<xsl:param name="addrType" select=" ' none' "/>
		<xsl:param name="natEnabled" select=" 'false' "/>
		<xsl:choose>
			<xsl:when test="$dir='Outbound' and $addrType='Source'  and $ruleType='Access' ">
				<xsl:call-template name="create-dynaddrRef">
					<xsl:with-param name="dynaddr" select="$dynaddr"/>
					<xsl:with-param name="objsindynaddr" select="$objsindynaddr"/>
					<xsl:with-param name="ruleType" select="$ruleType"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$dir='Inbound' and $addrType='Dest' and $ruleType='Access' and $natEnabled='false' ">
				<xsl:call-template name="create-dynaddrRef">
					<xsl:with-param name="dynaddr" select="$dynaddr"/>
					<xsl:with-param name="objsindynaddr" select="$objsindynaddr"/>
					<xsl:with-param name="ruleType" select="$ruleType"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$dir='Outbound' and $addrType='Source'  and $ruleType='Nat' ">
				<xsl:call-template name="create-dynaddrRef">
					<xsl:with-param name="dynaddr" select="$dynaddr"/>
					<xsl:with-param name="objsindynaddr" select="$objsindynaddr"/>
					<xsl:with-param name="ruleType" select="$ruleType"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$dir='Inbound' and $addrType='Dest' and $ruleType='Nat'">
				<xsl:call-template name="create-dynaddrRef">
					<xsl:with-param name="dynaddr" select="$dynaddr"/>
					<xsl:with-param name="objsindynaddr" select="$objsindynaddr"/>
					<xsl:with-param name="ruleType" select="$ruleType"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$dir='Inbound' and $addrType='Source' and $ruleType='Self' ">
				<xsl:call-template name="create-dynaddrRef">
					<xsl:with-param name="dynaddr" select="$dynaddr"/>
					<xsl:with-param name="objsindynaddr" select="$objsindynaddr"/>
					<xsl:with-param name="ruleType" select="$ruleType"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="AnyAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="fwm:AddressName">
		<xsl:param name="dynaddr" select="'none' "/>
		<xsl:param name="objsindynaddr" select=" '0' "/>
		<xsl:param name="dir" select=" 'none' "/>
		<xsl:param name="ruleType" select=" ' none' "/>
		<xsl:param name="addrType" select=" ' none' "/>
		<xsl:choose>
			<xsl:when test="$ruleType='Access' or $ruleType='Self' ">
				<xsl:element name="Addresses" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:element name="AddressEntry" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:element name="AddressName" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="AddressName" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="fwm:PortName">
		<xsl:param name="ruleType" select=" ' none' "/>
		<xsl:choose>
			<xsl:when test="$ruleType='Access' ">
				<xsl:element name="Ports" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:element name="PortEntry" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:element name="PortName" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute>
						</xsl:element>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="PortName" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="create-dynaddrRef">
		<xsl:param name="dynaddr"/>
		<xsl:param name="objsindynaddr" select=" '0' "/>
		<xsl:param name="ruleType" select=" 'none' "/>
		<xsl:choose>
			<xsl:when test="$ruleType='Access' or $ruleType='Self' ">
				<xsl:element name="Addresses" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:element name="AddressEntry" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:choose>
							<xsl:when test="count($objsindynaddr)=1">
								<xsl:element name="DynamicAddressName" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
									<xsl:attribute name="value"><xsl:value-of select="$dynaddr"/></xsl:attribute>
								</xsl:element>
							</xsl:when>
							<xsl:otherwise>
								<xsl:element name="AddressGroup" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
									<xsl:attribute name="value"><xsl:value-of select="$dynaddr"/></xsl:attribute>
								</xsl:element>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:element>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="count($objsindynaddr)=1">
						<xsl:element name="DynamicAddressName" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:attribute name="value"><xsl:value-of select="$dynaddr"/></xsl:attribute>
						</xsl:element>
					</xsl:when>
					<xsl:otherwise>
						<xsl:element name="AddressGroup" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:attribute name="value"><xsl:value-of select="$dynaddr"/></xsl:attribute>
						</xsl:element>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="self:selfpoliciestoinsert">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="self:*">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*" />
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<xsl:template name="insert-proxyredirectrules">
		<xsl:variable name="avmtune" select="document($avmxml)/avm:policy/avm:TuningSetting[contains(@name,'inspect')]"/>
		<xsl:choose>
			<xsl:when test="count($avmtune)=0 ">
				<xsl:apply-templates select="document($newproxy)/proxy:proxyredirectrules"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:for-each select="document($proxytune)/policy/TuningSetting">
					<xsl:variable name="avmtuningnode" select="$avmtune[@name= current()/@name ] "/>
					<xsl:choose>
						<xsl:when test="count($avmtuningnode)">
							<xsl:if test="$avmtuningnode/common:boolean/@value='true' ">
								<xsl:call-template name="insert-proxyredirectrule">
									<xsl:with-param name="rulename" select="$avmtuningnode/@name"/>
								</xsl:call-template>
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<xsl:if test="current()/boolean/@value='true' ">
								<xsl:call-template name="insert-proxyredirectrule">
									<xsl:with-param name="rulename" select="current()/@name"/>
								</xsl:call-template>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="proxy:proxyredirectrules">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="proxy:ProxyRedirectRule">
		<xsl:element name="ProxyRedirectRule" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*[name() != 'Enabled']"/>
			<xsl:attribute name="Enabled">
				<xsl:choose>
					<xsl:when test="@Proxy='HTTP' and $httpProxyEnabled='true' ">true</xsl:when>
					<xsl:when test="@Proxy='FTP' and $ftpProxyEnabled='true' ">true</xsl:when>
					<xsl:when test="@Proxy='SMTP' and $smtpProxyEnabled='true' ">true</xsl:when>
					<xsl:when test="@Proxy='POP3' and $pop3ProxyEnabled='true' ">true</xsl:when>
					<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="proxy:*">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*" />
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<xsl:template name="insert-proxyredirectrule">
		<xsl:param name="rulename"/>
		<xsl:variable name="direction">
			<xsl:choose>
				<xsl:when test="contains($rulename, 'corpto') or contains($rulename, 'dmzto') ">Outbound</xsl:when>
				<xsl:otherwise>Inbound</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="proxy">
			<xsl:choose>
				<xsl:when test="starts-with($rulename, 'ftp')">FTP</xsl:when>
				<xsl:when test="starts-with($rulename, 'http')">HTTP</xsl:when>
				<xsl:when test="starts-with($rulename, 'pop3')">POP3</xsl:when>
				<xsl:when test="starts-with($rulename, 'smtp')">SMTP</xsl:when>
				<xsl:otherwise/>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="srcaddr">
			<xsl:choose>
				<xsl:when test="contains($rulename, 'corpto') ">CORP</xsl:when>
				<xsl:when test="contains($rulename, 'dmzto') ">DMZ</xsl:when>
				<xsl:otherwise>Any</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="destaddr">
			<xsl:choose>
				<xsl:when test="contains($rulename, 'tocorp') ">CORP</xsl:when>
				<xsl:when test="contains($rulename, 'todmz') ">DMZ</xsl:when>
				<xsl:otherwise>Any</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="destport">
			<xsl:choose>
				<xsl:when test="starts-with($rulename, 'ftp')">21</xsl:when>
				<xsl:when test="starts-with($rulename, 'http')">80</xsl:when>
				<xsl:when test="starts-with($rulename, 'pop3')">110</xsl:when>
				<xsl:when test="starts-with($rulename, 'smtp')">25</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<!-- TODO: Add stuff here to handle what I need to -->
		<xsl:element name="ProxyRedirectRule" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="Name"><xsl:value-of select="concat($proxy,$srcaddr,'to',$destaddr)"/></xsl:attribute>
			<xsl:attribute name="GUID"><xsl:value-of select="generate-id()"/></xsl:attribute>
			<xsl:variable name="SnetEnabled">
				<xsl:choose>
					<xsl:when test="$srcaddr='CORP' and $corpEnabled='false' ">false</xsl:when>
					<xsl:when test="$destaddr='CORP' and $corpEnabled='false' ">false</xsl:when>
					<xsl:when test="$srcaddr='DMZ' and $dmzEnabled='false' ">false</xsl:when>
					<xsl:when test="$destaddr='DMZ' and $dmzEnabled='false' ">false</xsl:when>
					<xsl:otherwise>true</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:variable name="ProxyEnabled">
				<xsl:choose>
					<xsl:when test="$proxy='HTTP' and $httpProxyEnabled='true' ">true</xsl:when>
					<xsl:when test="$proxy='FTP' and $ftpProxyEnabled='true' ">true</xsl:when>
					<xsl:when test="$proxy='SMTP' and $smtpProxyEnabled='true' ">true</xsl:when>
					<xsl:when test="$proxy='POP3' and $pop3ProxyEnabled='true' ">true</xsl:when>
					<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:variable>
			<xsl:attribute name="Enabled">
			<xsl:choose>
				<xsl:when test="$ProxyEnabled='true' and $SnetEnabled='true' ">true</xsl:when>
				<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="UserEnabled"><xsl:value-of select="$SnetEnabled"/></xsl:attribute>
			<xsl:attribute name="Direction"><xsl:value-of select="$direction"/></xsl:attribute>
			<xsl:attribute name="Comment"><xsl:value-of select="$rulename"/></xsl:attribute>
			<xsl:attribute name="Proxy"><xsl:value-of select="$proxy"/></xsl:attribute>
			<xsl:element name="SourceAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:choose>
					<xsl:when test="$srcaddr='Any' ">
						<xsl:element name="AnyAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
					</xsl:when>
					<xsl:when test="$srcaddr='CORP' and count($objsincorp)>1 or $srcaddr='DMZ' and count($objsindmz)>1">
						<xsl:element name="AddressGroup" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:attribute name="value"><xsl:value-of select="$srcaddr"/></xsl:attribute>
						</xsl:element>
					</xsl:when>
					<xsl:otherwise>
						<xsl:element name="DynamicAddressName" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:attribute name="value"><xsl:value-of select="$srcaddr"/></xsl:attribute>
						</xsl:element>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
			<xsl:element name="DestAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:choose>
					<xsl:when test="$destaddr='Any' ">
						<xsl:element name="AnyAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
					</xsl:when>
					<xsl:when test="$destaddr='CORP' and count($objsincorp)>1 or $destaddr='DMZ' and count($objsindmz)>1">
						<xsl:element name="AddressGroup" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:attribute name="value"><xsl:value-of select="$destaddr"/></xsl:attribute>
						</xsl:element>
					</xsl:when>
					<xsl:otherwise>
						<xsl:element name="DynamicAddressName" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:attribute name="value"><xsl:value-of select="$destaddr"/></xsl:attribute>
						</xsl:element>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
			<xsl:element name="DestPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="SinglePort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="SinglePort"><xsl:value-of select="$destport"/></xsl:attribute>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template match="proxyredirectrules">
		<xsl:copy-of select="*"/>
	</xsl:template>
	<xsl:template name="insert-ocspprovs">
		<xsl:for-each select="fwm:IKERules">
			<xsl:if test="current()/fwm:OSCP/@Enabled='true'">
				<xsl:element name="OCSPPROV" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="Name"><xsl:value-of select="@Name"/></xsl:attribute>
					<xsl:attribute name="GUID"><xsl:value-of select="@GUID"/></xsl:attribute>
					<xsl:copy-of select="current()/fwm:OSCP/@*[name() != 'Enabled']"/>
				</xsl:element>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="fwm:IPSECRules">
		<xsl:variable name="manualnode" select="fwm:ManualAutoParms/fwm:ManualIpsec"/>
		<xsl:element name="IPSECRules" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*[name() != 'EncapsulationMode' and name() != 'Protocol']"/>
			<xsl:attribute name="Protocol">
				<xsl:choose>
					<xsl:when test="count(fwm:DestPort/fwm:PortName)>0">ALL</xsl:when>
					<xsl:otherwise><xsl:value-of select="@Protocol"/></xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:element name="SecurityGateway" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:choose>
					<xsl:when test="count($manualnode) > 0">
						<xsl:element name="ManualSecGw" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:attribute name="value"><xsl:value-of select="@Name"/></xsl:attribute>
						</xsl:element>
					</xsl:when>
					<xsl:otherwise>
						<xsl:element name="AutoIkeSecGw" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:variable name="SecGwName">
								<xsl:choose>
									<xsl:when test="fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:Advanced/@Enabled='true' and string-length(fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:Advanced/@IkePolicyName) > 0">
										<xsl:variable name="ikeRule" select="/fwm:policy/fwm:IKERules[@Name = current()/fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:Advanced/@IkePolicyName]" />
										<xsl:value-of select="$ikeRule/@Name" />
									</xsl:when>
									<xsl:when test="fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:Advanced/@Enabled='false'">
										<xsl:variable name="ikeRule" select="/fwm:policy/fwm:IKERules[@RemoteIpAddress = current()/fwm:ManualAutoParms/fwm:AutomaticIpsec/@PeerSecurityGateway]" />
										<xsl:value-of select="$ikeRule/@Name" />
									</xsl:when>
								</xsl:choose>
							</xsl:variable> 
							<xsl:attribute name="value"><xsl:value-of select="$SecGwName"/></xsl:attribute>
						</xsl:element>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
			<xsl:apply-templates select="node()[name() != 'ManualAutoParms' ]"/>
		</xsl:element>
	</xsl:template>
	<xsl:template name="insert-manualsecgws">
		<xsl:param name="manualsecgwdata"/>
		<!--This template outputs manual mode security gateway data from a list of IPSEC rules containing manual mode data-->
		<xsl:for-each select="$manualsecgwdata">
			<xsl:element name="ManualSecurityGatewayList" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:attribute name="Name"><xsl:value-of select="@Name"/></xsl:attribute>
				<xsl:attribute name="Comment"><xsl:value-of select="@Comment"/></xsl:attribute>
				<xsl:attribute name="GUID"><xsl:value-of select="@GUID"/></xsl:attribute>
				<xsl:attribute name="EncapsulationMode"><xsl:value-of select="@EncapsulationMode"/></xsl:attribute>
				<xsl:attribute name="PeerSecurityGateway"><xsl:value-of select="current()/fwm:ManualAutoParms/fwm:ManualIpsec/@PeerSecurityGateway"/></xsl:attribute>
				<xsl:apply-templates select="current()/fwm:ManualAutoParms/fwm:ManualIpsec/node()"/>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="insertvpnaddrsecgws">
		<xsl:for-each select="/fwm:policy/fwm:AddressRanges">
			<xsl:element name="VpnAddrIkeSecurityGatewayList" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:attribute name="Name"><xsl:value-of select="@Name"/></xsl:attribute>
				<xsl:attribute name="GUID"><xsl:value-of select="@GUID"/></xsl:attribute>
				<xsl:variable name="associatedPolicies" select="current()/fwm:AssociatedPolicies"/>
				<xsl:if test="count(current()/fwm:AssociatedPolicies)>1">
					<xsl:message>INFO: Multiple Associated IKE rules encountered while processing VPN AddressRange <xsl:value-of select="@Name"/>. Settings from the first entry are the only ones used to create the new Security Gateway object. All other Associated IKE Rules are ignored.</xsl:message>
				</xsl:if>
				<xsl:element name="IkeConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:apply-templates select="/fwm:policy/fwm:IKERules[@Name=current()/fwm:AssociatedPolicies[1]/@PolicyName]"/>
				</xsl:element>
				<xsl:element name="IpsecConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:copy-of select="@*[name() != 'Name' and name() != 'GUID' ]"/>
					<xsl:apply-templates select="current()/fwm:TemplateProposal"/>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="fwm:IKERules">
		<xsl:copy-of select="@*[name() != 'Name' and name() != 'GUID' and name() != 'Enabled' and name() != 'Comment' ] "/>
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="fwm:OSCP">
		<xsl:element name="OCSP" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="Enabled">false</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template name="insertautosecgws2">
		<xsl:variable name="ipsecnoadv" select="/fwm:policy/fwm:IPSECRules[fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:Advanced/@Enabled='false']"/>
		<xsl:variable name="ipsecadv" select="/fwm:policy/fwm:IPSECRules[fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:Advanced/@Enabled='true' and string-length(/fwm:policy/fwm:IPSECRules/fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:Advanced/@IkePolicyName) > 0] "/>
		<xsl:for-each select="/fwm:policy/fwm:IKERules">
			<xsl:variable name="inpeersgs" select="@RemoteIpAddress = $ipsecnoadv/fwm:ManualAutoParms/fwm:AutomaticIpsec/@PeerSecurityGateway"/>
			<xsl:variable name="inadv" select="@Name = $ipsecadv/fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:Advanced/@IkePolicyName"/>
			<xsl:variable name="invpnaddr" select="@Name = /fwm:policy/fwm:AddressRanges/fwm:AssociatedPolicies[1]/@PolicyName"/>
			<xsl:if test="$invpnaddr = false" >
				<!-- Now insert the Auto IKE Security Gateway -->
				<xsl:element name="AutoIkeSecurityGatewayList" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="Name"><xsl:value-of select="@Name"/></xsl:attribute>
					<xsl:attribute name="Comment"><xsl:value-of select="@Comment"/></xsl:attribute>
					<xsl:attribute name="GUID"><xsl:value-of select="@GUID"/></xsl:attribute>
					<!-- Now we need to get the corresponding IKE Rule for this IPSEC Rule and output its contents-->
					<xsl:element name="IkeConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:copy-of select="@*[name() != 'Name' and name() != 'GUID' and name() != 'Enabled' and name() != 'Comment' ] "/>
						<xsl:apply-templates select="node()"/>
					</xsl:element>
					<xsl:choose>
						<xsl:when test="$inpeersgs or $inadv ">
							<!--IKE Policy used by some ipsec rule .-->				
							<xsl:variable name="ipsecrule" select="/fwm:policy/fwm:IPSECRules[fwm:ManualAutoParms/fwm:AutomaticIpsec/@PeerSecurityGateway=current()/@RemoteIpAddress or fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:Advanced/@IkePolicyName=current()/@Name] "/>
							<xsl:element name="IpsecConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
								<xsl:attribute name="EncapsulationMode"><xsl:value-of select="$ipsecrule[1]/@EncapsulationMode"/></xsl:attribute>
								<xsl:copy-of select="$ipsecrule[1]/fwm:ManualAutoParms/fwm:AutomaticIpsec/@*[name() != 'PeerSecurityGateway'] "/>
								<xsl:apply-templates select="$ipsecrule[1]/fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:SecurityProposals"/>
								<xsl:element name="Advanced" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
									<xsl:copy-of select="$ipsecrule[1]/fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:Advanced/@*[name() != 'IkePolicyName'] "/>
								</xsl:element>
							</xsl:element>
						</xsl:when>
						<xsl:otherwise>
								<!-- Not in use by anything so emit a message and insert reasonable IPSEC defaults -->
								<xsl:message>INFO: IKE Policy <xsl:value-of select="@Name"/> Not in use by either an IPSEC policy or a VPN AddressRange. Generating AutoIKE Security Gateway with reasonable defaults.</xsl:message>
								<xsl:element name="IpsecConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
									<xsl:element name="SecurityProposals" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
										<xsl:attribute name="GUID"><xsl:value-of select="generate-id()"/></xsl:attribute>
										<xsl:attribute name="SecurityProtocol">ESPWithAuth</xsl:attribute>
										<xsl:attribute name="AuthAlgorithm">MD5</xsl:attribute>
										<xsl:attribute name="EspAlgorithm">3DES</xsl:attribute>
									</xsl:element>
									<xsl:element name="Advanced" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
								</xsl:element>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="insertnatbindrules">
		<xsl:param name="rules"/>
		<xsl:param name="nameprefix"/>
		<xsl:param name="dynaddr" />
		<xsl:param name="objsindynaddr" />
		<xsl:for-each select="$rules">
			<xsl:element name="GlobalNatBindList" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:attribute name="Name"><xsl:value-of select="concat($nameprefix, position()-1)"/></xsl:attribute>
				<xsl:attribute name="Enabled">true</xsl:attribute>
				<xsl:attribute name="GUID"><xsl:value-of select="@GUID"/></xsl:attribute>
				<xsl:attribute name="RuleId"><xsl:value-of select="position()-1"/></xsl:attribute>
				<xsl:apply-templates select="fwm:Protocol"/>
				<xsl:element name="LocalAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:apply-templates select="fwm:SourceAddress/*">
						<xsl:with-param name="dynaddr" select="$dynaddr"/>
						<xsl:with-param name="objsindynaddr" select="$objsindynaddr"/>
						<xsl:with-param name="dir" select=" 'Outbound' "/>
						<xsl:with-param name="ruleType" select="'Nat'"/>
						<xsl:with-param name="addrType" select="'Source'" />
					</xsl:apply-templates>
				</xsl:element>
				<xsl:element name="RemoteAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:apply-templates select="fwm:DestAddress/*"/>
				</xsl:element>
				<xsl:element name="ServicePort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:variable name="protocol">
						<xsl:choose>
							<xsl:when test="count(fwm:Protocol/fwm:ProtocolName)>0">
								<xsl:value-of select="string(fwm:Protocol/fwm:ProtocolName/@Name)"/>
							</xsl:when>
							<xsl:otherwise>TCP</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:apply-templates select="fwm:DestPort/*">
						<xsl:with-param name="protocol" select="$protocol">
						</xsl:with-param>
					</xsl:apply-templates>
				</xsl:element>
				<xsl:element name="NatIp" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:element name="NatRecord" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:attribute name="NatRecordName"><xsl:value-of select="fwm:NatName/@value"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="insertreversenatrules">
		<xsl:param name="rules"/>
		<xsl:param name="nameprefix"/>
		<xsl:param name="dynaddr" />
		<xsl:param name="objsindynaddr" />
		<xsl:for-each select="$rules">
			<xsl:element name="GlobalReverseNatList" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:attribute name="Name"><xsl:value-of select="concat($nameprefix, position()-1)"/></xsl:attribute>
				<xsl:attribute name="Enabled">true</xsl:attribute>
				<xsl:attribute name="GUID"><xsl:value-of select="@GUID"/></xsl:attribute>
				<xsl:attribute name="RuleId"><xsl:value-of select="position()-1"/></xsl:attribute>
				<xsl:apply-templates select="fwm:Protocol"/>
				<xsl:element name="RemoteAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:apply-templates select="fwm:SourceAddress/*"/>
				</xsl:element>
				<xsl:element name="DestinationAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:apply-templates select="fwm:DestAddress/*">
					<xsl:with-param name="dynaddr" select="$dynaddr"/>
					<xsl:with-param name="objsindynaddr" select="$objsindynaddr"/>
					<xsl:with-param name="dir" select=" 'Inbound' "/>
					<xsl:with-param name="ruleType" select="'Nat'"/>
					<xsl:with-param name="addrType" select="'Dest'" />
					</xsl:apply-templates>
				</xsl:element>
				<xsl:element name="IncomingService" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:variable name="protocol">
						<xsl:choose>
							<xsl:when test="count(fwm:Protocol/fwm:ProtocolName)>0">
								<xsl:value-of select="string(fwm:Protocol/fwm:ProtocolName/@Name)"/>
							</xsl:when>
							<xsl:otherwise>TCP</xsl:otherwise>
						</xsl:choose>
					</xsl:variable>
					<xsl:apply-templates select="fwm:DestPort/*">
						<xsl:with-param name="protocol" select="$protocol">
						</xsl:with-param>
					</xsl:apply-templates>
				</xsl:element>
				<xsl:element name="ReverseNatIp" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:element name="NatRecord" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:attribute name="NatRecordName"><xsl:value-of select="fwm:NatName/@value"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
				<xsl:element name="ReverseNatPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:element name="SameAsIncomingPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="fwm:SinglePort">
		<xsl:param name="protocol" />
		<xsl:element name="SinglePort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="SinglePort"><xsl:value-of select="@SinglePort"/></xsl:attribute>
			<!-- No longer needed for right now.
			<xsl:if test="string-length($protocol) > 0">
				<xsl:attribute name="Protocol"><xsl:value-of select="$protocol"/></xsl:attribute> 
			</xsl:if>
			-->
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:PortRange">
		<xsl:param name="protocol" />
		<xsl:element name="PortRange" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="PortRange"><xsl:value-of select="@PortRange"/></xsl:attribute>
			<!-- No longer needed for right now.
			<xsl:if test="string-length($protocol) > 0">
				<xsl:attribute name="Protocol"><xsl:value-of select="$protocol"/></xsl:attribute>
			</xsl:if>
			-->
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:UserList">
		<xsl:element name="UserList" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="GUID"><xsl:value-of select="generate-id()"/></xsl:attribute>
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	<!-- The template below is obsolete and does not work quite right.  It creates an auto ike security gateway for each IPSEC rule which is incorrect  
          since a  single IPSEC rule can refer to the same IKE Rule -->
	<xsl:template name="insertautosecgws">
		<xsl:param name="autosecgwdata"/>
		<xsl:for-each select="$autosecgwdata">
			<xsl:element name="AutoIkeSecurityGatewayList" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:attribute name="Name"><xsl:value-of select="@Name"/></xsl:attribute>
				<xsl:attribute name="Comment"><xsl:value-of select="@Comment"/></xsl:attribute>
				<xsl:attribute name="GUID"><xsl:value-of select="generate-id()"/></xsl:attribute>
				<!-- Now we need to get the corresponding IKE Rule for this IPSEC Rule and output its contents-->
				<xsl:element name="IkeConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:choose>
						<xsl:when test="current()/fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:Advanced/@Enabled='false' ">
							<xsl:apply-templates select="/fwm:policy/fwm:IKERules[@RemoteIpAddress=current()/fwm:ManualAutoParms/fwm:AutomaticIpsec/@PeerSecurityGateway]"/>
						</xsl:when>
						<xsl:when test="count(current()/fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:Advanced/@IkePolicyName)>0">
							<xsl:apply-templates select="/fwm:policy/fwm:IKERules[@Name=current()/fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:Advanced/@IkePolicyName]"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:apply-templates select="/fwm:policy/fwm:IKERules[@RemoteIpAddress=current()/fwm:ManualAutoParms/fwm:AutomaticIpsec/@PeerSecurityGateway]"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
				<xsl:element name="IpsecConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="EncapsulationMode"><xsl:value-of select="@EncapsulationMode"/></xsl:attribute>
					<xsl:copy-of select="current()/fwm:ManualAutoParms/fwm:AutomaticIpsec/@*[name() != 'PeerSecurityGateway'] "/>
					<xsl:apply-templates select="current()/fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:SecurityProposals"/>
					<xsl:element name="Advanced" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:copy-of select="current()/fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:Advanced/@*[name() != 'IkePolicyName'] "/>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<!-- The template below is obsolete. Its duties are handled by the insertautoikegw2 above.  -->
	<xsl:template name="insertextraautoikegws">
		<xsl:variable name="ipsecnoadv" select="/fwm:policy/fwm:IPSECRules[fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:Advanced/@Enabled='false']"/>
		<xsl:variable name="ipsecadv" select="/fwm:policy/fwm:IPSECRules[fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:Advanced/@Enabled='true' and string-length(/fwm:policy/fwm:IPSECRules/fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:Advanced/@IkePolicyName) > 0] "/>
		<!--
		<xsl:for-each select="$ipsecnoadv">
			<xsl:message>IPSEC rule without Adv diabled name=<xsl:value-of select="@Name" /> </xsl:message>
		</xsl:for-each>
		<xsl:for-each select="$ipsecadv">
			<xsl:message>IPSEC rule with Adv enabled and a specified IKE policy name - name=<xsl:value-of select="@Name" /> </xsl:message>
		</xsl:for-each>
		<xsl:variable name="ikeusedbyvpnaddr" select="/fwm:policy/fwm:IKERules[ @Name = /fwm:policy/fwm:AddressRanges/fwm:AssociatedPolicies[1]/@PolicyName]"/>
		<xsl:for-each select="$ikeusedbyvpnaddr">
			<xsl:message>IKE rule <xsl:value-of select="@Name" /> is associated with a VPN AddressRange</xsl:message>
		</xsl:for-each>
		<xsl:variable name="ikeusedbyipsecadv" select="/fwm:policy/fwm:IKERules[ @Name = $ipsecadv/fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:Advanced/@IkePolicyName]"/>
		<xsl:for-each select="$ikeusedbyipsecadv">
			<xsl:message>IKE rule <xsl:value-of select="@Name" /> is associated with a IPSEC Rule Advanced IKE Policy Parm</xsl:message>
		</xsl:for-each>
		<xsl:variable name="ikeusedbyipsecpeersg" select="/fwm:policy/fwm:IKERules[ @RemoteIpAddress = $ipsecnoadv/fwm:ManualAutoParms/fwm:AutomaticIpsec/@PeerSecurityGateway]"/>
	<xsl:for-each select="$ikeusedbyipsecpeersg">
		<xsl:message>IKE rule <xsl:value-of select="@Name" /> is associated with an IPSEC Rule by Peer SG. RemoteIpAddress is <xsl:value-of select="@RemoteIpAddress"/></xsl:message>
	</xsl:for-each>
		<xsl:variable name="notusedikerules" select="/fwm/policy/fwm:IKERules[@Name != $ikeusedbyvpnaddr/@Name and @Name != $ikeusedbyipsecadv/@Name and @Name != $ikeusedbyipsecpeersg/@Name]"/>
	<xsl:for-each select="$notusedikerules">
		<xsl:message>IKE rule <xsl:value-of select="@Name" /> is in notusedikerules and is not associated with an IPSEC Rule or AddressRange rule.RemoteIpAddress is <xsl:value-of select="@RemoteIpAddress"/></xsl:message>
	</xsl:for-each>
		<xsl:variable name="extraikerules" select="/fwm:policy/fwm:IKERules[ @RemoteIpAddress != $ipsecnoadv/fwm:ManualAutoParms/fwm:AutomaticIpsec/@PeerSecurityGateway and @Name != $ipsecadv/fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:Advanced/@IkePolicyName and @Name != /fwm:policy/fwm:AddressRanges/fwm:AssociatedPolicies[1]/@PolicyName]"/>
-->
		<xsl:for-each select="/fwm:policy/fwm:IKERules">
			<xsl:variable name="inpeersgs" select="@RemoteIpAddress = $ipsecnoadv/fwm:ManualAutoParms/fwm:AutomaticIpsec/@PeerSecurityGateway"/>
			<xsl:variable name="inadv" select="@Name = $ipsecadv/fwm:ManualAutoParms/fwm:AutomaticIpsec/fwm:Advanced/@IkePolicyName"/>
			<xsl:variable name="invpnaddr" select="@Name = /fwm:policy/fwm:AddressRanges/fwm:AssociatedPolicies[1]/@PolicyName"/>
			<xsl:choose>
				<xsl:when test="$inpeersgs">
					<!--<xsl:message>IKE Policy <xsl:value-of select="@Name"/> used by an ipsec based on peersg match.</xsl:message>-->
				</xsl:when>
				<xsl:when test="$inadv">
					<!--<xsl:message>IKE Policy <xsl:value-of select="@Name"/> used by an ipsec based on advanced setting match.</xsl:message>-->
				</xsl:when>
				<xsl:when test="$invpnaddr">
					<!--<xsl:message>IKE Policy <xsl:value-of select="@Name"/> used by a VPN AddressRange.</xsl:message>-->
				</xsl:when>
				<xsl:otherwise>
					<xsl:message>INFO: IKE Policy <xsl:value-of select="@Name"/> Not in use by either an IPSEC policy or a VPN AddressRange. Generating AutoIKE Security Gateway with reasonable defaults.</xsl:message>
					<xsl:element name="AutoIkeSecurityGatewayList" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:attribute name="Name"><xsl:value-of select="@Name"/></xsl:attribute>
						<xsl:attribute name="Comment"><xsl:value-of select="@Comment"/></xsl:attribute>
						<xsl:attribute name="GUID"><xsl:value-of select="generate-id()"/></xsl:attribute>
						<!-- Now we need to get the corresponding IKE Rule for this IPSEC Rule and output its contents-->
						<xsl:element name="IkeConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:copy-of select="@*[name() != 'Name' and name() != 'GUID' and name() != 'Enabled' and name() != 'Comment' ] "/>
							<xsl:apply-templates select="node()"/>
						</xsl:element>
						<xsl:element name="IpsecConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:element name="SecurityProposals" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
								<xsl:attribute name="GUID"><xsl:value-of select="generate-id()"/></xsl:attribute>
								<xsl:attribute name="SecurityProtocol">ESPWithAuth</xsl:attribute>
								<xsl:attribute name="AuthAlgorithm">MD5</xsl:attribute>
								<xsl:attribute name="EspAlgorithm">3DES</xsl:attribute>
							</xsl:element>
							<xsl:element name="Advanced" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
						</xsl:element>
					</xsl:element>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="tune:tuning">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="tune:*">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*" />
			</xsl:element>
		</xsl:if>
	</xsl:template>	
</xsl:stylesheet>
