<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:fwm="http://www.iss.net/cml/NetworkProtector/fwm" 
xmlns:common="http://www.iss.net/cml/NetworkProtector/common" >
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<!-- Generic Copy Template -->
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<!-- Update schema version -->
	<xsl:template match="fwm:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">4.0.0</xsl:attribute>
			<xsl:apply-templates />
		</xsl:element>
	</xsl:template>
	<!-- We need to create the elements anywhere we are now supporting netobj refs. -->
	<xsl:template match="fwm:RadiusConfig">
		<xsl:element name="RadiusConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:attribute name="Enabled"><xsl:value-of select="@Enabled"/></xsl:attribute>
				<xsl:attribute name="PrimaryServerAuthPort"><xsl:value-of select="@PrimaryServerAuthPort"/></xsl:attribute>
				<xsl:attribute name="PrimaryServerAcctPort"><xsl:value-of select="@PrimaryServerAcctPort"/></xsl:attribute>
				<xsl:attribute name="PrimarySecret"><xsl:value-of select="@PrimarySecret"/></xsl:attribute>
				<xsl:attribute name="PrimaryServerNASID"><xsl:value-of select="@PrimaryServerNASID"/></xsl:attribute>
				<xsl:if test="count(@PrimaryServerIPAddress)>0">
					<xsl:attribute name="PrimaryServerIPAddress"><xsl:value-of select="@PrimaryServerIPAddress"/></xsl:attribute>
					<xsl:attribute name="PrimaryServerSubnetMask"><xsl:value-of select="@PrimaryServerSubnetMask"/></xsl:attribute>
					<xsl:element name="PrimaryServerIPAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:element name="SubnetIP" namespace="http://www.iss.net/cml/NetworkProtector/common">
							<xsl:attribute name="IPAddress"><xsl:value-of select="@PrimaryServerIPAddress"/></xsl:attribute>
							<xsl:attribute name="SubnetMask"><xsl:value-of select="@PrimaryServerSubnetMask"/></xsl:attribute>
						</xsl:element>
					</xsl:element>
				</xsl:if>
				<xsl:element name="BackupServer" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="UseBackupServer"><xsl:value-of select="@UseBackupServer"/></xsl:attribute>
					<xsl:attribute name="BackupServerAuthPort"><xsl:value-of select="@BackupServerAuthPort"/></xsl:attribute>
					<xsl:attribute name="BackupServerAcctPort"><xsl:value-of select="@BackupServerAcctPort"/></xsl:attribute>
					<xsl:attribute name="BackupSecret"><xsl:value-of select="@BackupSecret"/></xsl:attribute>
					<xsl:attribute name="BackupServerNASID"><xsl:value-of select="@BackupServerNASID"/></xsl:attribute>
					<xsl:if test="count(@BackupServerIPAddress)>0">
						<xsl:attribute name="BackupServerIPAddress"><xsl:value-of select="@BackupServerIPAddress"/></xsl:attribute>
						<xsl:attribute name="BackupServerSubnetMask"><xsl:value-of select="@BackupServerSubnetMask"/></xsl:attribute>
						<xsl:element name="BackupServerIPAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:element name="SubnetIP" namespace="http://www.iss.net/cml/NetworkProtector/common">
								<xsl:attribute name="IPAddress"><xsl:value-of select="@BackupServerIPAddress"/></xsl:attribute>
								<xsl:attribute name="SubnetMask"><xsl:value-of select="@BackupServerSubnetMask"/></xsl:attribute>
							</xsl:element>
						</xsl:element>
					</xsl:if>
				</xsl:element>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:IkeConfig">
		<xsl:element name="IkeConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<!-- TODO: Need to handle RemoteId Type and Data and LocalId Type and Data -->
			<xsl:element name="LocalIpAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AddrIP" namespace="http://www.iss.net/cml/NetworkProtector/common">
					<xsl:attribute name="IPAddress"><xsl:value-of select="@LocalIpAddress"/></xsl:attribute>
				</xsl:element>
			</xsl:element>
			<xsl:element name="RemoteIpAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm" >
				<xsl:element name="AddrIP" namespace="http://www.iss.net/cml/NetworkProtector/common">
					<xsl:attribute name="IPAddress"><xsl:value-of select="@RemoteIpAddress"/></xsl:attribute>
				</xsl:element>
			</xsl:element>
			<xsl:element name="DaLocalId" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:call-template name="InsertVpnId">
					<xsl:with-param name="IdType" select="@LocalIdType"/>
					<xsl:with-param name="IdValue" select="@LocalIdData"/>
				</xsl:call-template>
			</xsl:element>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:TemplateProposal">
		<xsl:element name="TemplateProposal" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:element name="SourceAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AddrcidrIP" namespace="http://www.iss.net/cml/NetworkProtector/common">
					<xsl:attribute name="IPAddress"><xsl:value-of select="@SourceAddress"/></xsl:attribute>
				</xsl:element>
			</xsl:element>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:L2TPIPSECSecGwList">
		<xsl:element name="L2TPIPSECSecGwList" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name='Name'><xsl:value-of select="@Name"/></xsl:attribute>
			<xsl:attribute name='Comment'><xsl:value-of select="@Comment"/></xsl:attribute>
			<xsl:attribute name='GUID'><xsl:value-of select="@GUID"/></xsl:attribute>
			<xsl:element name="L2TPSecGwGeneralConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:attribute name='L2TPEndPoint'><xsl:value-of select="@L2TPEndPoint"/></xsl:attribute>
				<xsl:attribute name='L2TPDisableTunnelAuth'><xsl:value-of select="@L2TPDisableTunnelAuth"/></xsl:attribute>
				<xsl:attribute name='L2TPHostName'><xsl:value-of select="@L2TPHostName"/></xsl:attribute>
				<xsl:element name = "L2TPEndPoint" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:element name="AddrIP" namespace="http://www.iss.net/cml/NetworkProtector/common">
						<xsl:attribute name="IPAddress"><xsl:value-of select="@L2TPEndPoint"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:ManualSecurityGatewayList">
		<xsl:element name="ManualSecurityGatewayList" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name='Name'><xsl:value-of select="@Name"/></xsl:attribute>
			<xsl:attribute name='Comment'><xsl:value-of select="@Comment"/></xsl:attribute>
			<xsl:attribute name='GUID'><xsl:value-of select="@GUID"/></xsl:attribute>
			<xsl:element name="ManSecGwGeneralConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:attribute name="EncapsulationMode"><xsl:value-of select="@EncapsulationMode"/></xsl:attribute>
				<xsl:attribute name="PeerSecurityGateway"><xsl:value-of select="@PeerSecurityGateway"/></xsl:attribute>
				<xsl:apply-templates select="fwm:LocalSecurityGateway"/>
				<xsl:element name="PeerSecurityGateway" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:element name="AddrIP" namespace="http://www.iss.net/cml/NetworkProtector/common">
						<xsl:attribute name="IPAddress"><xsl:value-of select="@PeerSecurityGateway"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
			<xsl:apply-templates select="fwm:AHConfiguration"/>
			<xsl:apply-templates select="fwm:ESPConfiguration"/>
			<xsl:apply-templates select="fwm:Advanced"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:VpnAddrIkeSecurityGatewayList/fwm:IpsecConfig">
		<xsl:element name="IpsecConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:element name="IPRange1" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AddrrngIP" namespace="http://www.iss.net/cml/NetworkProtector/common">
					<xsl:attribute name="IPAddress"><xsl:value-of select="@IPRange1"/></xsl:attribute>
				</xsl:element>
			</xsl:element>
			<xsl:if test="count(@IPRANGE2)>0">
				<xsl:element name="IPRange2" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:element name="AddrrngIP" namespace="http://www.iss.net/cml/NetworkProtector/common">
						<xsl:attribute name="IPAddress"><xsl:value-of select="@IPRange2"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:if>			
			<xsl:if test="count(@IPRANGE3)>0">
				<xsl:element name="IPRange3" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:element name="AddrrngIP" namespace="http://www.iss.net/cml/NetworkProtector/common">
						<xsl:attribute name="IPAddress"><xsl:value-of select="@IPRange3"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:if>
			<xsl:if test="count(@WINS1)>0">
				<xsl:element name="WINS1" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:element name="AddrIP" namespace="http://www.iss.net/cml/NetworkProtector/common">
						<xsl:attribute name="IPAddress"><xsl:value-of select="@WINS1"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:if>
			<xsl:if test="count(@WINS2)>0">
				<xsl:element name="WINS2" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:element name="AddrIP" namespace="http://www.iss.net/cml/NetworkProtector/common">
						<xsl:attribute name="IPAddress"><xsl:value-of select="@WINS2"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:if>
			<xsl:if test="count(@DNS1)>0">
				<xsl:element name="DNS1" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:element name="AddrIP" namespace="http://www.iss.net/cml/NetworkProtector/common">
						<xsl:attribute name="IPAddress"><xsl:value-of select="@DNS1"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:if>
			<xsl:if test="count(@DNS2)>0">
				<xsl:element name="DNS2" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:element name="AddrIP" namespace="http://www.iss.net/cml/NetworkProtector/common">
						<xsl:attribute name="IPAddress"><xsl:value-of select="@DNS2"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:if>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:L2TPIPPool">
		<xsl:element name="L2TPIPPool" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:element name="IPRange" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AddrrngIP" namespace="http://www.iss.net/cml/NetworkProtector/common">
					<xsl:attribute name="IPAddress"><xsl:value-of select="@IPRange"/></xsl:attribute>
				</xsl:element>
			</xsl:element>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:L2TPIKEAttr">
		<xsl:element name="L2TPIKEAttr" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<!-- TODO: Need to handle RemoteId Type and Data and LocalId Type and Data -->
			<xsl:element name="DaLocalId" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:call-template name="InsertVpnId">
					<xsl:with-param name="IdType" select="@LocalIdType"/>
					<xsl:with-param name="IdValue" select="@LocalIdData"/>
				</xsl:call-template>
			</xsl:element>
			<xsl:element name="DaRemoteId" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:call-template name="InsertVpnId">
					<xsl:with-param name="IdType" select="@RemoteIdType"/>
					<xsl:with-param name="IdValue" select="@RemoteIdData"/>
				</xsl:call-template>
			</xsl:element>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>			
	<xsl:template match="fwm:RemoteId">
		<xsl:element name="RemoteId" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:element name="DaRemoteId" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:call-template name="InsertVpnId">
					<xsl:with-param name="IdType" select="@RemoteIdType"/>
					<xsl:with-param name="IdValue" select="@RemoteIdData"/>
				</xsl:call-template>
			</xsl:element>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>	
	<!-- Template for Handling VPNIds -->
	<xsl:template name="InsertVpnId">
		<xsl:param name="IdType"/>
		<xsl:param name="IdValue"/>
		<xsl:choose>
			<xsl:when test="$IdType='IPADDRESS'">
				<xsl:element name="IPADDRESS" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="IPAddress"><xsl:value-of select="$IdValue"/></xsl:attribute>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
				<xsl:element name="{$IdType}" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="value"><xsl:value-of select="$IdValue"/></xsl:attribute>
				</xsl:element>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
