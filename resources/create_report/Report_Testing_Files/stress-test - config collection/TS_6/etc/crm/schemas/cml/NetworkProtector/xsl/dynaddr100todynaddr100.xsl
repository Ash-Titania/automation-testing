<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:dynobj="http://www.iss.net/cml/NetworkProtector/dynobj"
xmlns:dynaddr="http://www.iss.net/cml/NetworkProtector/dynaddr">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="dynobjxml" select="'npdynobj.xml'"/>
	<xsl:template match="dynaddr:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/dynaddr">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates select="document($dynobjxml)/dynobj:policy/dynobj:DynamicAddressList"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/dynaddr">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*" />
			</xsl:element>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
