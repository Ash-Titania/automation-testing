#
# Script to run policy conversion to convert to new Galaga format for policies.
#
exitstatus=0
# Log all output and errors to policy_conversion.result
#exec > policy_conversion.result 2>&1

Log() { logger -t firmxpu $* ;}

# Display commands as they execute
set -x

# Create dynobject policy from the network xml and the route xml.
# network102todynobject uses nproute1_0.xml
#
Log Creating dynamic object xml.
Xalan -o npdynobj.xml -p routexml \'/etc/crm/policies/cml/NetworkProtector/route/nproute1_0.xml\' /etc/crm/policies/cml/NetworkProtector/network/npnetwork1_0.xml network102todynobjects.xsl

# Create new firewall policy if it is not the new default one.
# fwm200to300.xsl uses npnetwork1_0.xml and npavm1_0.xml and npcflts1_0.xml and npspam1_0.xml and npdynobj.xml
# If it is the new default one update the proxy redirection defaults it with the proper values.
#
Log Converting fwm policy
if [ -f /etc/crm/policies/cml/NetworkProtector/fwm/npfwm1_0.xml.rpmnew ] ; then
    Xalan -o npfwmstep1.xml -p dynobjxml \'npdynobj.xml\' -p netxml \'/etc/crm/policies/cml/NetworkProtector/network/npnetwork1_0.xml\' -p avmxml \'/etc/crm/policies/cml/NetworkProtector/avm/npavm1_0.xml\' -p cfltsxml \'/etc/crm/policies/cml/NetworkProtector/cflts/npcflts1_0.xml\' -p spamxml \'/etc/crm/policies/cml/NetworkProtector/spam/npspam1_0.xml\' /etc/crm/policies/cml/NetworkProtector/fwm/npfwm1_0.xml fwm200tofwm300.xsl
    Xalan -o npfwm1_0.xml npfwmstep1.xml fwm300fixruleids.xsl
    validator -n http://www.iss.net/cml/NetworkProtector/fwm 3.0.0 -s /etc/crm/schemas/ npfwm1_0.xml > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
        Log ERROR - Firewall policy migration resulted in an invalid policy so an empty firewall policy is being used in its place.
        exitstatus=1
	    cp /etc/crm/policies/cml/NetworkProtector/fwm/npfwm1_0.xml /etc/crm/policies/cml/NetworkProtector/fwm/npfwm1_0.xml.bak
	    cp npfwm1_0.xml /etc/crm/policies/cml/NetworkProtector/fwm/npfwm1_0.xml.migbad
	    cp empty_npfwm1_0.xml /etc/crm/policies/cml/NetworkProtector/fwm/npfwm1_0.xml
    else
	    cp /etc/crm/policies/cml/NetworkProtector/fwm/npfwm1_0.xml /etc/crm/policies/cml/NetworkProtector/fwm/npfwm1_0.xml.bak
	    cp npfwm1_0.xml /etc/crm/policies/cml/NetworkProtector/fwm/npfwm1_0.xml
    fi
else
    Xalan -o npfwm1_0.xml -p avmxml \'/etc/crm/policies/cml/NetworkProtector/avm/npavm1_0.xml\' -p cfltsxml \'/etc/crm/policies/cml/NetworkProtector/cflts/npcflts1_0.xml\' -p spamxml \'/etc/crm/policies/cml/NetworkProtector/spam/npspam1_0.xml\' /etc/crm/policies/cml/NetworkProtector/fwm/npfwm1_0.xml fwm300tofwm300.xsl
    validator -n http://www.iss.net/cml/NetworkProtector/fwm 3.0.0 -s /etc/crm/schemas/ npfwm1_0.xml > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
        Log ERROR - Firewall policy migration resulted in an invalid policy so an empty firewall policy is being used in its place.
        exitstatus=1
	    cp /etc/crm/policies/cml/NetworkProtector/fwm/npfwm1_0.xml /etc/crm/policies/cml/NetworkProtector/fwm/npfwm1_0.xml.bak
    	cp npfwm1_0.xml /etc/crm/policies/cml/NetworkProtector/fwm/npfwm1_0.xml.migbad
	    cp empty_npfwm1_0.xml /etc/crm/policies/cml/NetworkProtector/fwm/npfwm1_0.xml
    else
	    cp /etc/crm/policies/cml/NetworkProtector/fwm/npfwm1_0.xml /etc/crm/policies/cml/NetworkProtector/fwm/npfwm1_0.xml.bak
    	cp npfwm1_0.xml /etc/crm/policies/cml/NetworkProtector/fwm/npfwm1_0.xml
    fi
fi
#
# Update dynaddr default policy with info from network policy if the network policy is not the new default one.
# dynaddr100todynaddr100.xsl uses npdynobj.xml
#
Log Converting dynaddr policy
if [ -f /etc/crm/policies/cml/NetworkProtector/network/npnetwork1_0.xml.rpmnew ] ; then
	Xalan -o npdynaddr1_0.xml -p dynobjxml \'npdynobj.xml\' /etc/crm/policies/cml/NetworkProtector/dynaddr/npdynaddr1_0.xml dynaddr100todynaddr100.xsl
    validator -n http://www.iss.net/cml/NetworkProtector/dynaddr 1.0.0 -s /etc/crm/schemas/ npdynaddr1_0.xml > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
        Log ERROR - Dynamic Address policy migration resulted in an invalid policy so the default dynamic address policy is being used in its place.
        exitstatus=1
	    cp npdynaddr1_0.xml /etc/crm/policies/cml/NetworkProtector/dynaddr/npdynaddr1_0.xml.migbad
    else
    	cp /etc/crm/policies/cml/NetworkProtector/dynaddr/npdynaddr1_0.xml /etc/crm/policies/cml/NetworkProtector/dynaddr/npdynaddr1_0.xml.bak
	    cp npdynaddr1_0.xml /etc/crm/policies/cml/NetworkProtector/dynaddr/npdynaddr1_0.xml
    fi
fi
#
# Create new srv policy if it is not the new default one.
# If it is the new default one go ahead and merge in snmp config if set in CRM.
# srv102tosrv200.xsl uses npcrm1_0.xml
# It works with either and old 1.0.2 or a new 2.0.0 version of the srv xml file.
#
Log Converting srv policy
Xalan -o npsrv1_0.xml -p with \'/etc/crm/policies/cml/NetworkProtector/crm/npcrm1_0.xml\' /etc/crm/policies/cml/NetworkProtector/srv/npsrv1_0.xml srv102tosrv200.xsl
validator -n http://www.iss.net/cml/NetworkProtector/srv 2.0.0 -s /etc/crm/schemas/ npsrv1_0.xml > /dev/null 2>&1
if [ $? -ne 0 ]
then
    Log ERROR - Services policy migration resulted in an invalid policy so the default services policy is being used in its place.
    exitstatus=1
    if [ -f /etc/crm/policies/cml/NetworkProtector/srv/npsrv1_0.xml.rpmnew ] ; then
        cp /etc/crm/policies/cml/NetworkProtector/srv/npsrv1_0.xml /etc/crm/policies/cml/NetworkProtector/srv/npsrv1_0.xml.bak
        cp /etc/crm/policies/cml/NetworkProtector/srv/npsrv1_0.xml.rpmnew /etc/crm/policies/cml/NetworkProtector/srv/npsrv1_0.xml
    fi
    cp npsrv1_0.xml /etc/crm/policies/cml/NetworkProtector/srv/npsrv1_0.xml.migbad
else
    cp /etc/crm/policies/cml/NetworkProtector/srv/npsrv1_0.xml /etc/crm/policies/cml/NetworkProtector/srv/npsrv1_0.xml.bak
    cp npsrv1_0.xml /etc/crm/policies/cml/NetworkProtector/srv/npsrv1_0.xml
fi
#
# Create new network objects policy if not the new default one.
# If it is the new default one go ahead an merge in the dynobj stuff if there is any.
# netobj100tonetobj101.xsl uses npdynobj.xml
# It works with either the old 1.0.0 or the new 1.1.0 version of the xml file.
#
Log Converting network objects
Xalan -o networkobjects1_0.xml -p dynobjxml \'npdynobj.xml\' /etc/crm/policies/cml/NetworkObjects/networkobjects1_0.xml netobj100tonetobj101.xsl
validator -n http://www.iss.net/cml/NetworkObjects 1.1.0 -s /etc/crm/schemas/ networkobjects1_0.xml > /dev/null 2>&1
if [ $? -ne 0 ]
then
    Log ERROR - NetworkObject policy migration resulted in an invalid policy so the default NetworkObjects policy is being used in its place.
    exitstatus=1
    if [ -f /etc/crm/policies/cml/NetworkObjects/networkobjects1_0.xml.rpmnew ] ; then
        cp /etc/crm/policies/cml/NetworkObjects/networkobjects1_0.xml /etc/crm/policies/cml/NetworkObjects/networkobjects1_0.xml.bak
        cp /etc/crm/policies/cml/NetworkObjects/networkobjects1_0.xml.rpmnew /etc/crm/policies/cml/NetworkObjects/networkobjects1_0.xml
    fi
    cp networkobjects1_0.xml /etc/crm/policies/cml/NetworkObjects/networkobjects1_0.xml.migbad
else
    cp /etc/crm/policies/cml/NetworkObjects/networkobjects1_0.xml /etc/crm/policies/cml/NetworkObjects/networkobjects1_0.xml.bak
	cp networkobjects1_0.xml /etc/crm/policies/cml/NetworkObjects/networkobjects1_0.xml
fi
#
# Create new avm policy only if its not the new default one.
#
Log Converting avm
if [ -f /etc/crm/policies/cml/NetworkProtector/avm/npavm1_0.xml.rpmnew ] ; then
	Xalan -o npavm1_0.xml /etc/crm/policies/cml/NetworkProtector/avm/npavm1_0.xml avm200toavm201.xsl
    validator -n http://www.iss.net/cml/NetworkProtector/avm 2.1.0 -s /etc/crm/schemas/ npavm1_0.xml > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
        Log ERROR - Antivirus policy migration resulted in an invalid policy so the default Antivirus policy is being used in its place.
        exitstatus=1
    	cp /etc/crm/policies/cml/NetworkProtector/avm/npavm1_0.xml /etc/crm/policies/cml/NetworkProtector/avm/npavm1_0.xml.bak
	    cp npavm1_0.xml /etc/crm/policies/cml/NetworkProtector/avm/npavm1_0.xml.migbad
    	cp /etc/crm/policies/cml/NetworkProtector/avm/npavm1_0.xml.rpmnew /etc/crm/policies/cml/NetworkProtector/avm/npavm1_0.xml
    else
    	cp /etc/crm/policies/cml/NetworkProtector/avm/npavm1_0.xml /etc/crm/policies/cml/NetworkProtector/avm/npavm1_0.xml.bak
	    cp npavm1_0.xml /etc/crm/policies/cml/NetworkProtector/avm/npavm1_0.xml
    fi
fi
#
# Create new upds policy if not the new default one.
#
Log Converting upds
if [ -f /etc/crm/policies/cml/NetworkProtector/upds/npupds1_0.xml.rpmnew ] ; then
    Xalan -o npupds1_0.xml /etc/crm/policies/cml/NetworkProtector/upds/npupds1_0.xml upds120toupds200.xsl
    validator -n http://www.iss.net/cml/NetworkProtector/upds 2.0.0 -s /etc/crm/schemas/ npupds1_0.xml > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
        Log ERROR - Update Settings policy migration resulted in an invalid policy so the default Update Settings policy is being used in its place.
        exitstatus=1
    	cp /etc/crm/policies/cml/NetworkProtector/upds/npupds1_0.xml /etc/crm/policies/cml/NetworkProtector/upds/npupds1_0.xml.bak
	    cp npupds1_0.xml /etc/crm/policies/cml/NetworkProtector/upds/npupds1_0.xml.migbad
    	cp /etc/crm/policies/cml/NetworkProtector/upds/npupds1_0.xml.rpmnew /etc/crm/policies/cml/NetworkProtector/upds/npupds1_0.xml.bak
    else
    	cp /etc/crm/policies/cml/NetworkProtector/upds/npupds1_0.xml /etc/crm/policies/cml/NetworkProtector/upds/npupds1_0.xml.bak
	    cp npupds1_0.xml /etc/crm/policies/cml/NetworkProtector/upds/npupds1_0.xml
    fi
fi
#
# Create new crm policy if it is not the new default one.
#
Log Converting crm
if [ -f /etc/crm/policies/cml/NetworkProtector/crm/npcrm1_0.xml.rpmnew ] ; then
	Xalan -o npcrm1_0.xml /etc/crm/policies/cml/NetworkProtector/crm/npcrm1_0.xml crm102tocrm200.xsl
    validator -n http://www.iss.net/cml/NetworkProtector/crm 2.0.0 -s /etc/crm/schemas/ npcrm1_0.xml > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
        Log ERROR - Notification policy migration resulted in an invalid policy so the default Notification policy is being used in its place.
        exitstatus=1
    	cp /etc/crm/policies/cml/NetworkProtector/crm/npcrm1_0.xml /etc/crm/policies/cml/NetworkProtector/crm/npcrm1_0.xml.bak
	    cp npcrm1_0.xml /etc/crm/policies/cml/NetworkProtector/crm/npcrm1_0.xml.migbad
    	cp /etc/crm/policies/cml/NetworkProtector/crm/npcrm1_0.xml.rpmnew /etc/crm/policies/cml/NetworkProtector/crm/npcrm1_0.xml
    else
    	cp /etc/crm/policies/cml/NetworkProtector/crm/npcrm1_0.xml /etc/crm/policies/cml/NetworkProtector/crm/npcrm1_0.xml.bak
	    cp npcrm1_0.xml /etc/crm/policies/cml/NetworkProtector/crm/npcrm1_0.xml
    fi
fi
#
# Create new network policy if it is not the new default one.
#
Log Converting network
if [ -f /etc/crm/policies/cml/NetworkProtector/network/npnetwork1_0.xml.rpmnew ] ; then
	Xalan -o npnetwork1_0.xml /etc/crm/policies/cml/NetworkProtector/network/npnetwork1_0.xml network102tonetwork200.xsl
    validator -n http://www.iss.net/cml/NetworkProtector/network 2.0.0 -s /etc/crm/schemas/ npnetwork1_0.xml > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
        Log ERROR - Network policy migration resulted in an invalid policy so the default Network policy is being used in its place.
        exitstatus=1
    	cp /etc/crm/policies/cml/NetworkProtector/network/npnetwork1_0.xml /etc/crm/policies/cml/NetworkProtector/network/npnetwork1_0.xml.bak
	    cp npnetwork1_0.xml /etc/crm/policies/cml/NetworkProtector/network/npnetwork1_0.xml.migbad
    	cp /etc/crm/policies/cml/NetworkProtector/network/npnetwork1_0.xml.rpmnew /etc/crm/policies/cml/NetworkProtector/network/npnetwork1_0.xml
    else
    	cp /etc/crm/policies/cml/NetworkProtector/network/npnetwork1_0.xml /etc/crm/policies/cml/NetworkProtector/network/npnetwork1_0.xml.bak
	    cp npnetwork1_0.xml /etc/crm/policies/cml/NetworkProtector/network/npnetwork1_0.xml
    fi
fi
#
# Create new ipm policy if it is not the new default one.
#
Log Converting ipm
if [ -f /etc/crm/policies/cml/NetworkProtector/ipm/npipm1_0.xml.rpmnew ] ; then
	Xalan -o npipm1_0.xml /etc/crm/policies/cml/NetworkProtector/ipm/npipm1_0.xml ipm102toipm200.xsl
    validator -n http://www.iss.net/cml/NetworkProtector/ipm 2.0.0 -s /etc/crm/schemas/ npipm1_0.xml > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
        Log ERROR - Intrusion Prevention policy migration resulted in an invalid policy so the default Intrusion Prevention policy is being used in its place.
        exitstatus=1
    	cp /etc/crm/policies/cml/NetworkProtector/ipm/npipm1_0.xml /etc/crm/policies/cml/NetworkProtector/ipm/npipm1_0.xml.bak
	    cp npipm1_0.xml /etc/crm/policies/cml/NetworkProtector/ipm/npipm1_0.xml.migbad
    	cp /etc/crm/policies/cml/NetworkProtector/ipm/npipm1_0.xml.rpmnew /etc/crm/policies/cml/NetworkProtector/ipm/npipm1_0.xml
    else
    	cp /etc/crm/policies/cml/NetworkProtector/ipm/npipm1_0.xml /etc/crm/policies/cml/NetworkProtector/ipm/npipm1_0.xml.bak
	    cp npipm1_0.xml /etc/crm/policies/cml/NetworkProtector/ipm/npipm1_0.xml
    fi
fi
#
# Create new cflt policy if it is not the new default one.
#
Log Converting cflt
if [ -f /etc/crm/policies/cml/NetworkProtector/cflt/npcflt1_0.xml.rpmnew ] ; then
	Xalan -o npcflt1_0.xml /etc/crm/policies/cml/NetworkProtector/cflt/npcflt1_0.xml cflt100tocflt101.xsl
    validator -n http://www.iss.net/cml/NetworkProtector/cflt 1.1.0 -s /etc/crm/schemas/ npcflt1_0.xml > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
        Log ERROR - Web Filter Categories policy migration resulted in an invalid policy so the default Web Filter Categories policy is being used in its place.
        exitstatus=1
    	cp /etc/crm/policies/cml/NetworkProtector/cflt/npcflt1_0.xml /etc/crm/policies/cml/NetworkProtector/cflt/npcflt1_0.xml.bak
	    cp npcflt1_0.xml /etc/crm/policies/cml/NetworkProtector/cflt/npcflt1_0.xml.migbad
    	cp /etc/crm/policies/cml/NetworkProtector/cflt/npcflt1_0.xml.rpmnew /etc/crm/policies/cml/NetworkProtector/cflt/npcflt1_0.xml
    else
    	cp /etc/crm/policies/cml/NetworkProtector/cflt/npcflt1_0.xml /etc/crm/policies/cml/NetworkProtector/cflt/npcflt1_0.xml.bak
	    cp npcflt1_0.xml /etc/crm/policies/cml/NetworkProtector/cflt/npcflt1_0.xml
    fi
fi
#
# Create new access policy if it is not the new default one.
#
Log Converting access
if [ -f /etc/crm/policies/cml/NetworkProtector/access/npaccess1_0.xml.rpmnew ] ; then
	Xalan -o npaccess1_0.xml /etc/crm/policies/cml/NetworkProtector/access/npaccess1_0.xml access103toaccess110.xsl
    validator -n http://www.iss.net/cml/NetworkProtector/access 1.1.0 -s /etc/crm/schemas/ npaccess1_0.xml > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
        Log ERROR - Access policy migration resulted in an invalid policy so the default Access policy is being used in its place.
        exitstatus=1
    	cp /etc/crm/policies/cml/NetworkProtector/access/npaccess1_0.xml /etc/crm/policies/cml/NetworkProtector/access/npaccess1_0.xml.bak
	    cp npaccess1_0.xml /etc/crm/policies/cml/NetworkProtector/access/npaccess1_0.xml.migbad
    	cp /etc/crm/policies/cml/NetworkProtector/access/npaccess1_0.xml.rpmnew /etc/crm/policies/cml/NetworkProtector/access/npaccess1_0.xml
    else
    	cp /etc/crm/policies/cml/NetworkProtector/access/npaccess1_0.xml /etc/crm/policies/cml/NetworkProtector/access/npaccess1_0.xml.bak
	    cp npaccess1_0.xml /etc/crm/policies/cml/NetworkProtector/access/npaccess1_0.xml
    fi
fi
#
# Create new cfasdb policy if it is not the new default one.
#
Log Converting cfasdb
if [ -f /etc/crm/policies/cml/NetworkProtector/cfasdb/npcfasdb1_0.xml.rpmnew ] ; then
	Xalan -o npcfasdb1_0.xml /etc/crm/policies/cml/NetworkProtector/cfasdb/npcfasdb1_0.xml cfasdb100tocfasdb110.xsl
    validator -n http://www.iss.net/cml/NetworkProtector/cfasdb 1.1.0 -s /etc/crm/schemas/ npcfasdb1_0.xml > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
        Log ERROR - Web Filter Antispam Database policy migration resulted in an invalid policy so the default Web Filter Antispam database policy is being used in its place.
        exitstatus=1
    	cp /etc/crm/policies/cml/NetworkProtector/cfasdb/npcfasdb1_0.xml /etc/crm/policies/cml/NetworkProtector/cfasdb/npcfasdb1_0.xml.bak
	    cp npcfasdb1_0.xml /etc/crm/policies/cml/NetworkProtector/cfasdb/npcfasdb1_0.xml.migbad
    	cp /etc/crm/policies/cml/NetworkProtector/cfasdb/npcfasdb1_0.xml.rpmnew /etc/crm/policies/cml/NetworkProtector/cfasdb/npcfasdb1_0.xml
    else
    	cp /etc/crm/policies/cml/NetworkProtector/cfasdb/npcfasdb1_0.xml /etc/crm/policies/cml/NetworkProtector/cfasdb/npcfasdb1_0.xml.bak
	    cp npcfasdb1_0.xml /etc/crm/policies/cml/NetworkProtector/cfasdb/npcfasdb1_0.xml
    fi
fi
#
# Create new cflts policy if it is not the new default one.
#
Log Converting cflts
if [ -f /etc/crm/policies/cml/NetworkProtector/cflts/npcflts1_0.xml.rpmnew ] ; then
	Xalan -o npcflts1_0.xml /etc/crm/policies/cml/NetworkProtector/cflts/npcflts1_0.xml cflts100tocflts110.xsl
    validator -n http://www.iss.net/cml/NetworkProtector/cflts 1.1.0 -s /etc/crm/schemas/ npcflts1_0.xml > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
        Log ERROR - Web Filter Settings policy migration resulted in an invalid policy so the default Web Filter Settings policy is being used in its place.
        exitstatus=1
    	cp /etc/crm/policies/cml/NetworkProtector/cflts/npcflts1_0.xml /etc/crm/policies/cml/NetworkProtector/cflts/npcflts1_0.xml.bak
	    cp npcflts1_0.xml /etc/crm/policies/cml/NetworkProtector/cflts/npcflts1_0.xml.migbad
    	cp /etc/crm/policies/cml/NetworkProtector/cflts/npcflts1_0.xml.rpmnew /etc/crm/policies/cml/NetworkProtector/cflts/npcflts1_0.xml
    else
    	cp /etc/crm/policies/cml/NetworkProtector/cflts/npcflts1_0.xml /etc/crm/policies/cml/NetworkProtector/cflts/npcflts1_0.xml.bak
	    cp npcflts1_0.xml /etc/crm/policies/cml/NetworkProtector/cflts/npcflts1_0.xml
    fi
fi
#
# Create new mgmt policy if it is not the new default one.
#
Log Converting mgmt
if [ -f /etc/crm/policies/cml/NetworkProtector/mgmt/npmgmt1_0.xml.rpmnew ] ; then
	Xalan -o npmgmt1_0.xml /etc/crm/policies/cml/NetworkProtector/mgmt/npmgmt1_0.xml mgmt200tomgmt210.xsl
    validator -n http://www.iss.net/cml/NetworkProtector/mgmt 2.1.0 -s /etc/crm/schemas/ npmgmt1_0.xml > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
        Log ERROR - Management Settings policy migration resulted in an invalid policy so the default Management Settings policy is being used in its place.
        exitstatus=1
    	cp /etc/crm/policies/cml/NetworkProtector/mgmt/npmgmt1_0.xml /etc/crm/policies/cml/NetworkProtector/mgmt/npmgmt1_0.xml.bak
	    cp npmgmt1_0.xml /etc/crm/policies/cml/NetworkProtector/mgmt/npmgmt1_0.xml.migbad
    	cp /etc/crm/policies/cml/NetworkProtector/mgmt/npmgmt1_0.xml.rpmnew /etc/crm/policies/cml/NetworkProtector/mgmt/npmgmt1_0.xml
    else
    	cp /etc/crm/policies/cml/NetworkProtector/mgmt/npmgmt1_0.xml /etc/crm/policies/cml/NetworkProtector/mgmt/npmgmt1_0.xml.bak
	    cp npmgmt1_0.xml /etc/crm/policies/cml/NetworkProtector/mgmt/npmgmt1_0.xml
    fi
fi
#
# Create new route policy if it is not the new default one.
#
Log Converting route
if [ -f /etc/crm/policies/cml/NetworkProtector/route/nproute1_0.xml.rpmnew ] ; then
	Xalan -o nproute1_0.xml /etc/crm/policies/cml/NetworkProtector/route/nproute1_0.xml route102toroute110.xsl
    validator -n http://www.iss.net/cml/NetworkProtector/route 1.1.0 -s /etc/crm/schemas/ nproute1_0.xml > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
        Log ERROR - Route Settings policy migration resulted in an invalid policy so the default Route Settings policy is being used in its place.
        exitstatus=1
    	cp /etc/crm/policies/cml/NetworkProtector/route/nproute1_0.xml /etc/crm/policies/cml/NetworkProtector/route/nproute1_0.xml.bak
	    cp nproute1_0.xml /etc/crm/policies/cml/NetworkProtector/route/nproute1_0.xml.migbad
    	cp /etc/crm/policies/cml/NetworkProtector/route/nproute1_0.xml.rpmnew /etc/crm/policies/cml/NetworkProtector/route/nproute1_0.xml
    else
    	cp /etc/crm/policies/cml/NetworkProtector/route/nproute1_0.xml /etc/crm/policies/cml/NetworkProtector/route/nproute1_0.xml.bak
	    cp nproute1_0.xml /etc/crm/policies/cml/NetworkProtector/route/nproute1_0.xml
    fi
fi
#
# Create new spam policy if it is not the new default one.
#
Log Converting spam
if [ -f /etc/crm/policies/cml/NetworkProtector/spam/npspam1_0.xml.rpmnew ] ; then
	Xalan -o npspam1_0.xml /etc/crm/policies/cml/NetworkProtector/spam/npspam1_0.xml spam100tospam110.xsl
    validator -n http://www.iss.net/cml/NetworkProtector/spam 1.1.0 -s /etc/crm/schemas/ npspam1_0.xml > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
        Log ERROR - Spam policy migration resulted in an invalid policy so the default Spam Settings policy is being used in its place.
        exitstatus=1
    	cp /etc/crm/policies/cml/NetworkProtector/spam/npspam1_0.xml /etc/crm/policies/cml/NetworkProtector/spam/npspam1_0.xml.bak
	    cp npspam1_0.xml /etc/crm/policies/cml/NetworkProtector/spam/npspam1_0.xml.migbad
    	cp /etc/crm/policies/cml/NetworkProtector/spam/npspam1_0.xml.rpmnew /etc/crm/policies/cml/NetworkProtector/spam/npspam1_0.xml
    else
    	cp /etc/crm/policies/cml/NetworkProtector/spam/npspam1_0.xml /etc/crm/policies/cml/NetworkProtector/spam/npspam1_0.xml.bak
	    cp npspam1_0.xml /etc/crm/policies/cml/NetworkProtector/spam/npspam1_0.xml
    fi
fi
#
# Create new time policy if it is not the new default one.
#
Log Converting time
if [ -f /etc/crm/policies/cml/NetworkProtector/time/nptime1_0.xml.rpmnew ] ; then
	Xalan -o nptime1_0.xml /etc/crm/policies/cml/NetworkProtector/time/nptime1_0.xml time102totime110.xsl
    validator -n http://www.iss.net/cml/NetworkProtector/time 1.1.0 -s /etc/crm/schemas/ nptime1_0.xml > /dev/null 2>&1
    if [ $? -ne 0 ]
    then
        Log ERROR - Time policy migration resulted in an invalid policy so the default Time policy is being used in its place.
        exitstatus=1
    	cp /etc/crm/policies/cml/NetworkProtector/time/nptime1_0.xml /etc/crm/policies/cml/NetworkProtector/time/nptime1_0.xml.bak
	    cp nptime1_0.xml /etc/crm/policies/cml/NetworkProtector/time/nptime1_0.xml.migbad
    	cp /etc/crm/policies/cml/NetworkProtector/time/nptime1_0.xml.rpmnew /etc/crm/policies/cml/NetworkProtector/time/nptime1_0.xml
    else
    	cp /etc/crm/policies/cml/NetworkProtector/time/nptime1_0.xml /etc/crm/policies/cml/NetworkProtector/time/nptime1_0.xml.bak
	    cp nptime1_0.xml /etc/crm/policies/cml/NetworkProtector/time/nptime1_0.xml
    fi
fi


Log Conversion Complete
exit $exitstatus
