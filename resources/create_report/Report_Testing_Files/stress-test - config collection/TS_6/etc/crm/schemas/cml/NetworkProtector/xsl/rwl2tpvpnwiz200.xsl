<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fwm="http://www.iss.net/cml/NetworkProtector/fwm" xmlns:wiz="http://www.iss.net/cml/NetworkProtector/vpnwizrwl2tp" xmlns:npcommon="http://www.iss.net/cml/NetworkProtector/common" >
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="wizxml" select=" 'vpnwizrwl2tp1_0.xml' "/>
	<xsl:variable name="vpnwiz" select="document($wizxml)/wiz:policy"/>
	<xsl:template match="fwm:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:call-template name="insertaccessrules"/>
			<xsl:apply-templates select="fwm:Policy"/>
			<xsl:apply-templates select="fwm:IPSECRules"/>
			<xsl:apply-templates select="fwm:CommonLists"/>
			<xsl:apply-templates select="fwm:SecurityGateways"/>
			<xsl:apply-templates select="fwm:MessageConfig"/>
			<xsl:apply-templates select="fwm:ProxyRedirectRule"/>
			<xsl:apply-templates select="fwm:VpnAdvancedSettings"/>
			<xsl:apply-templates select="fwm:TuningSetting"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:if test="string-length(name(.)) > 0">
		  <xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates select="current()/*" />
		  </xsl:element>
		</xsl:if>
	 </xsl:template>
	<xsl:template match="fwm:Policy">
		<!-- Enable the ISAKAMP and L2TP Self policy if it is not enabled already -->
		<xsl:element name="Policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:choose>
				<xsl:when test="fwm:DestPort/fwm:Ports/fwm:PortEntry/fwm:PortName/@value = 'ISAKMP_UDP' ">
					<xsl:attribute name="Enabled">true</xsl:attribute>
				</xsl:when>
				<xsl:when test="fwm:DestPort/fwm:Ports/fwm:PortEntry/fwm:PortName/@value = 'L2TP' ">
					<xsl:attribute name="Enabled">true</xsl:attribute>
				</xsl:when>
				<xsl:when test="fwm:DestPort/fwm:SinglePort/@SinglePort='1701'  ">
					<xsl:attribute name="Enabled">true</xsl:attribute>
				</xsl:when>			
				<xsl:when test="fwm:DestPort/fwm:Ports/fwm:PortEntry/fwm:PortName/@value = 'IPSEC_NAT-T' ">
					<xsl:attribute name="Enabled">true</xsl:attribute>
				</xsl:when>
				<xsl:when test="fwm:DestPort/fwm:SinglePort/@SinglePort='4500'  ">
					<xsl:attribute name="Enabled">true</xsl:attribute>
				</xsl:when>			
				<xsl:otherwise>
					<xsl:attribute name="Enabled"><xsl:value-of select="@Enabled"/></xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:copy-of select="@*[name() != 'Enabled']"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:SecurityGateways">
		<xsl:element name="SecurityGateways" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:apply-templates select="fwm:AutoIkeSecurityGatewayList"/>
			<xsl:apply-templates select="fwm:VpnAddrIkeSecurityGatewayList"/>
			<xsl:call-template name="insertsecuritygw"/>
			<xsl:apply-templates select="fwm:ManualSecurityGatewayList"/>
		</xsl:element>
	</xsl:template>
	<xsl:template name="insertaccessrules">
		<!-- Insert ipsec rules based on values in the vpn wizard policy. -->
		<!--
		<xsl:variable name="vpnwiz" select="document($wizxml)/wiz:policy"/>
		<xsl:element name="Policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="Name"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@Name"/></xsl:attribute>
			<xsl:attribute name="GUID"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@GUID"/></xsl:attribute>
			<xsl:attribute name="RuleId">0</xsl:attribute>
			<xsl:attribute name="Enabled">true</xsl:attribute>
			<xsl:attribute name="Comment"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@Comment"/></xsl:attribute>
			<xsl:attribute name="Protocol"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/@Protocol"/></xsl:attribute>
			<xsl:apply-templates select="$vpnwiz/wiz:SourceSettings/wiz:SourceAddress"/>
			<xsl:apply-templates select="$vpnwiz/wiz:SourceSettings/wiz:SourcePort"/>
			<xsl:apply-templates select="$vpnwiz/wiz:DestinationSettings/wiz:DestAddress"/>
			<xsl:apply-templates select="$vpnwiz/wiz:DestinationSettings/wiz:DestPort"/>
		</xsl:element>
		-->
	</xsl:template>
	<xsl:template name="insertsecuritygw">
		<!-- Insert security gateway based on values in the vpn wizard policy if new gateway selected. -->
		<xsl:element name="L2TPIPSECSecGwList" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="Name"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:GeneralConfig/@name"/></xsl:attribute>
			<xsl:attribute name="Comment"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:GeneralConfig/@Comment"/></xsl:attribute>
			<xsl:attribute name="GUID"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:GeneralConfig/@GUID"/></xsl:attribute>
			<xsl:element name="L2TPSecGwGeneralConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:attribute name="L2TPDisableTunnelAuth">false</xsl:attribute>
				<xsl:element name="L2TPEndPoint" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:apply-templates select="$vpnwiz/wiz:GeneralSettings/wiz:GeneralConfig/wiz:L2TPEndPoint/*"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="L2TPIKEAttr" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:attribute name="ExchangeType">MAINMODE</xsl:attribute>
				<xsl:attribute name="EncryptionAlg">3DES</xsl:attribute>
				<xsl:attribute name="EncAuthAlg">SHA1</xsl:attribute>
				<xsl:attribute name="LifeTimeSecs">7200</xsl:attribute>
				<xsl:attribute name="DHGroup">MODP_1024</xsl:attribute>
				<xsl:attribute name="AuthMode"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:AuthConfig/@AuthMode"/></xsl:attribute>
				<xsl:if test="$vpnwiz/wiz:GeneralSettings/wiz:AuthConfig/@AuthMode='Pre-SharedKey' ">
					<xsl:attribute name="PreSharedKey"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:AuthConfig/@PreSharedKey"/></xsl:attribute>
				</xsl:if>
				<xsl:element name="DaLocalId" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:apply-templates select="$vpnwiz/wiz:GeneralSettings/wiz:LocalConfig/wiz:LocalID/*"/>
				</xsl:element>
				<xsl:element name="DaRemoteId" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:apply-templates select="$vpnwiz/wiz:GeneralSettings/wiz:RemoteConfig/wiz:RemoteID/*"/>
				</xsl:element>
			</xsl:element>
			<xsl:element name="L2TPIPSECAttr" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:attribute name="EncapsulationMode">TRANSPORT</xsl:attribute>
				<xsl:attribute name="Protocol">ESPWithAuth</xsl:attribute>
				<xsl:attribute name="AuthAlgorithm">SHA1</xsl:attribute>
				<xsl:attribute name="EncAlgorithm">3DES</xsl:attribute>
				<xsl:attribute name="LifeTimeSecs">3600</xsl:attribute>
				<xsl:attribute name="LifeTimeKBs">25000</xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template match="npcommon:*">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/common">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*" />
			</xsl:element>
		</xsl:if>
	</xsl:template>	
</xsl:stylesheet>
