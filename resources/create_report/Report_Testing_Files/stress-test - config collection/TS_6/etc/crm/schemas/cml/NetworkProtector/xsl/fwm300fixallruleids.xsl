<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.iss.net/cml/NetworkProtector/fwm" xmlns:fwm="http://www.iss.net/cml/NetworkProtector/fwm">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:template match="fwm:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
            <xsl:apply-templates select="fwm:Policy"/>
            <xsl:apply-templates select="fwm:IPSECRules"/>
			<xsl:apply-templates select="fwm:CommonLists"/>
			<xsl:apply-templates select="fwm:SecurityGateways"/>
			<xsl:apply-templates select="fwm:MessageConfig"/>
			<xsl:apply-templates select="fwm:ProxyRedirectRule"/>
			<xsl:apply-templates select="fwm:VpnAdvancedSettings"/>
			<xsl:apply-templates select="fwm:TuningSetting"/>
        </xsl:element>
	</xsl:template>

    <xsl:template match="fwm:Policy">
        <xsl:element name="Policy">
            <xsl:attribute name="RuleId"><xsl:value-of select="position()-1"/></xsl:attribute>
            <xsl:copy-of select="@*[name() != 'RuleId'] "/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="fwm:IPSECRules">
        <xsl:element name="IPSECRules">
            <xsl:attribute name="RuleId"><xsl:value-of select="position()-1"/></xsl:attribute>
            <xsl:copy-of select="@*[name() != 'RuleId'] "/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="fwm:GlobalNatBindList">
        <xsl:element name="GlobalNatBindList">
            <xsl:attribute name="RuleId"><xsl:value-of select="position()-1"/></xsl:attribute>
            <xsl:copy-of select="@*[name() != 'RuleId'] "/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

<!--    <xsl:template match="@*|node()">-->
<!--        <xsl:if test="string-length(name(.)) > 0">-->
<!--            <xsl:element name="{name(.)}">-->
<!--                <xsl:copy-of select="@*"/>-->
<!--                <xsl:apply-templates select="current()/*"/>-->
<!--            </xsl:element>-->
<!--        </xsl:if>-->
<!--    </xsl:template>-->

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
