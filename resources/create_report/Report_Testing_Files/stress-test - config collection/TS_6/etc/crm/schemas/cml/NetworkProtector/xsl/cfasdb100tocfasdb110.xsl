<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:myns="http://www.iss.net/cml/NetworkProtector/cfasdb" >
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:template match="myns:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/cfasdb">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">1.1.0</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>