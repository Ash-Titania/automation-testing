<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:fwm="http://www.iss.net/cml/NetworkProtector/fwm" 
xmlns:common="http://www.iss.net/cml/NetworkProtector/common" >
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<!-- Generic Copy Template -->
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<!-- Update schema version -->
	<xsl:template match="fwm:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">4.1.0</xsl:attribute>
			<xsl:apply-templates select="fwm:Policy" />
			<xsl:apply-templates select="fwm:IPSECRules" />
			<xsl:apply-templates select="fwm:CommonLists" />
			<xsl:apply-templates select="fwm:SecurityGateways" />
			<xsl:apply-templates select="fwm:MessageConfig" />
			<xsl:variable name="startpos" select="position()"/>
			<xsl:apply-templates select="fwm:ProxyRedirectRule"><xsl:with-param name="startpos" select="$startpos"/></xsl:apply-templates>
			<xsl:apply-templates select="fwm:ProtectionBypassRule"/>
			<xsl:apply-templates select="fwm:VpnAdvancedSettings"/>
			<xsl:apply-templates select="fwm:TuningSetting"/>
		</xsl:element>
	</xsl:template>
	<!-- Add in new Attributes Proxy Redirection Rules -->
	<xsl:template match="fwm:ProxyRedirectRule">
		<xsl:param name="startpos" select="'0'"/>
		<xsl:element name="ProxyRedirectRule" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:attribute name="RuleId"><xsl:value-of select="position()-$startpos"/></xsl:attribute>
			<xsl:attribute name="Action">PROXY</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
