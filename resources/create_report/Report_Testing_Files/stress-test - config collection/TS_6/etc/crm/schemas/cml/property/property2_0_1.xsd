<!-- iss/cml/property schema -->
<!-- This schema is EXPERIMENTAL.  There are Sensor Independence issues to be resolved
before this schema is ready for prime time.  DO NOT USE this schema unless you are
prepared to handle subsequent non-compatible revisions.  -->
<schema targetNamespace='http://www.iss.net/cml/property'
    xmlns='http://www.w3.org/2001/XMLSchema'
    xmlns:cml='http://www.iss.net/cml'
    xmlns:common='http://www.iss.net/cml/common'
    xmlns:property='http://www.iss.net/cml/property'
    elementFormDefault='qualified'
    version='2.0.1'>

	<annotation>
		<appinfo>
			<agent name='CML Property Base'/>
		</appinfo>
		<documentation>Base schema for ISS's Common Messaging Language (CML) policy/property.</documentation>
	</annotation>

	<!-- Include basic policy types.  -->
	<import namespace='http://www.iss.net/cml' schemaLocation='../PolicyBase2_0_1.xsd'/>
	<import namespace='http://www.iss.net/cml/common' schemaLocation='../common/common2_0_1.xsd'/>

	<attributeGroup name='AgentEnvironmentItems'>
		<!-- IssDk property stuff. -->
		<attribute name='transfer-path' type='normalizedString'/>
		<attribute name='license-path' type='normalizedString'/>
		<attribute name='key-container-path' type='normalizedString'/>
		<attribute name='crypt-config-file' type='normalizedString'/>
		<attribute name='keyfile-basename' type='normalizedString'/>
		<attribute name='reconnect-interval' type='unsignedInt'/>
		<attribute name='event-channel-port' type='unsignedInt'/>
		<attribute name='control-channel-port' type='unsignedInt'/>
	</attributeGroup>

	<simpleType name='DiagnosticValue'>
		<restriction base='normalizedString'>
			<enumeration value='none'/>
			<enumeration value='error'/>
			<enumeration value='warning'/>
			<enumeration value='notice'/>
			<enumeration value='info'/>
			<enumeration value='debug'/>
			<enumeration value='full'/>
		</restriction>
	</simpleType>

	<attributeGroup name='AgentDiagnosticsItems'>
		<!-- Configuration parameters for diagnostics. If missing, default to ErrorTrace. -->
		<!-- TODO:  are the app-trace-file and communications-trace-file allowed to be the same?
                    if not, do we want the parser to ensure that these values are different?
                    Also, identify other dependencies:  for instance, if a value other than 'none'
                    is specified for the app-trace-level, is an app-trace-path now mandatory?  -->
		<attribute name='app-trace-level' type='property:DiagnosticValue' default='error'/>
		<attribute name='app-trace-file' type='normalizedString'/>
		<attribute name='communications-trace-level' type='property:DiagnosticValue' default='error'/>
		<attribute name='communications-trace-file' type='normalizedString'/>
		<attribute name='system-trace-level' type='property:DiagnosticValue' default='error'/>
		<attribute name='log-performance-stats' type='boolean' default='false'/>
		<attribute name='performance-trace-file' type='normalizedString'/>
	</attributeGroup>

	<complexType name='AgentDiagnosticsBase'>
		<attributeGroup ref='property:AgentDiagnosticsItems'/>
	</complexType>

	<complexType name='AgentEnvironmentBase'>
		<attributeGroup ref='property:AgentEnvironmentItems'/>
	</complexType>
	
	<group name='PropertyGroup'>
		<sequence>
			<element name='agent-settings' type='property:AgentEnvironmentBase' minOccurs='0'/>
			<element name='agent-diagnostics' type='property:AgentDiagnosticsBase' minOccurs='0'/>
		</sequence>
	</group>
	
	<simpleType name='PosterTrustValue'>
		<restriction base='normalizedString'>
			<enumeration value='all'/>
			<enumeration value='first-time'/>
			<enumeration value='match'/>
		</restriction>
	</simpleType>

	<attributeGroup name='PosterInfoItems'>
		<attribute name='heartbeat-interval' type='unsignedInt' use='required'/>
		<attribute name='trust-level' type='property:PosterTrustValue' use='required'/>
		<attribute name='last-heartbeat' type='dateTime' use='required'/>
		<attribute name='last-config-change' type='dateTime' use='required'/>
		<attribute name='clock-difference' type='unsignedInt' use='required'/>
		<attribute name='ping-listener-port' type='unsignedInt' default='9099'/>
	</attributeGroup>

	<complexType name='PosterInfoBase'>
		<attributeGroup ref='property:PosterInfoItems'/>
	</complexType>
		
	<simpleType name='ControllerServiceValue'>
		<restriction base='normalizedString'>
			<enumeration value='default'/>
			<enumeration value='sensor_services'/>
			<enumeration value='event_services'/>
			<enumeration value='license_services'/>
			<enumeration value='update_services'/>
		</restriction>
	</simpleType>

	<attributeGroup name='ControllerInfoItems'>
		<attribute name='name' type='normalizedString' use='required'/>
		<attribute name='host-name' type='normalizedString' use='required'/>
		<attribute name='port' type='unsignedInt' use='required'/>
		<attribute name='service' type='property:ControllerServiceValue'/>
		<attribute name='proxy-host' type='normalizedString'/>
		<attribute name='proxy-port' type='unsignedInt'/>
		<attribute name='user-name' type='normalizedString'/>
		<attribute name='passwd' type='normalizedString'/>
	</attributeGroup>	

	<complexType name='ControllerInfoBase'>
		<attributeGroup ref='property:ControllerInfoItems'/>
	</complexType>

	<complexType name='ControllerListBase'>
		<sequence>
			<element name='controller-info' type='property:ControllerInfoBase' maxOccurs='unbounded'/>
		</sequence>
	</complexType>

	<attributeGroup name='AccepterInfoItems'>
		<attribute name='port' type='unsignedInt' use='required'/>
		<attribute name='ip-address' type='common:DottedStringIpv4Address'/>
	</attributeGroup>

	<complexType name='AccepterInfoBase'>
		<attributeGroup ref='property:AccepterInfoItems'/>
	</complexType>

	<complexType name='AccepterListBase'>
		<sequence>
			<element name='accepter-info' type='property:AccepterInfoBase' minOccurs='0' maxOccurs='unbounded'/>
		</sequence>
	</complexType>

	<group name='CommunicationGroup'>
		<sequence>
			<element name='poster-info' type='property:PosterInfoBase'/>
			<element name='controller-list' type='property:ControllerListBase'/>
			<element name='accepter-list' type='property:AccepterListBase' minOccurs='0'/>
		</sequence>
	</group>

	<!-- Base type for property information. -->
	<complexType name='PropertyBase'>
		<complexContent>
			<extension base='cml:PolicyBase'>
				<sequence>
					<group ref='property:PropertyGroup' minOccurs='0'/>
					<group ref='property:CommunicationGroup' minOccurs='0'/>
				</sequence>
			</extension>
		</complexContent>
	</complexType>
	<element name='policy' type='property:PropertyBase' substitutionGroup='cml:policy-base'/>
</schema>
