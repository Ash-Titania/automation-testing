<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:cflt="http://www.iss.net/cml/NetworkProtector/cflt" xmlns:cat="http://www.iss.net/cml/NetworkProtector/cat">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="newcat" select=" 'cflt101categoriestoinsert.xml' "/>
	<xsl:template match="cflt:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/cflt">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">1.1.0</xsl:attribute>
			<xsl:apply-templates/>
			<xsl:apply-templates select="document($newcat)/cat:categoriestoinsert"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="cat:categoriestoinsert">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="cat:*">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/cflt">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*"/>
			</xsl:element>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
