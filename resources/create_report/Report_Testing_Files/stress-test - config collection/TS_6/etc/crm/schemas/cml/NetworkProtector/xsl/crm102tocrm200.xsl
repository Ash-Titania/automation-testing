<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:my="http://www.iss.net/cml/NetworkProtector/crm">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:template match="my:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/crm">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">2.0.0</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="my:SnmpNotify">
	</xsl:template>
</xsl:stylesheet>
