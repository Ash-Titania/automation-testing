<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:netobj="http://www.iss.net/cml/NetworkObjects">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:template match="netobj:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkObjects">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">1.2.0</xsl:attribute>
			<xsl:apply-templates/>
			<xsl:element name="DynamicAddressObjectList" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:attribute name="Name">CORPTRANS</xsl:attribute>
				<xsl:attribute name="Comment">Use to define Mgmt Network when in Transparent Network Mode</xsl:attribute>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkObjects">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*" />
			</xsl:element>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
