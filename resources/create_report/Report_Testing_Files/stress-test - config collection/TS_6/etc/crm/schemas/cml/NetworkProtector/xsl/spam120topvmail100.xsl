<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:spam="http://www.iss.net/cml/NetworkProtector/spam"
xmlns:pvm="http://www.iss.net/cml/NetworkProtector/pvmail">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:param name="spamxml" select=" 'npspam1_0.xml' " />
	<!-- Generic Copy Template -->
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:variable name="enabled" select="document($spamxml)/spam:policy/spam:Config/@aspmEnabled"/>
	<!-- Handle values in the settings node. -->
	<xsl:template match="pvm:policy/pvm:settings">
		<xsl:element name="settings" namespace="http://www.iss.net/cml/NetworkProtector/pvmail">
			<xsl:copy-of select="@*[name() != 'enabled' or name() != mode]"/>
			<xsl:attribute name="enabled"><xsl:value-of select="$enabled"/></xsl:attribute>
			<xsl:attribute name="mode">0</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="pvm:easy_protected_protocols">
		<xsl:element name="easy_protected_protocols" namespace="http://www.iss.net/cml/NetworkProtector/pvmail">
			<xsl:attribute name="smtpEnabled">
				<xsl:choose>
					<xsl:when test="count(document($spamxml)/spam:policy/spam:Config/spam:ProtectedProtocols/@smtpEnabled) > 0">
						<xsl:value-of select="document($spamxml)/spam:policy/spam:Config/spam:ProtectedProtocols/@smtpEnabled"/>
					</xsl:when>
					<xsl:otherwise>true</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="pop3Enabled">
				<xsl:choose>
					<xsl:when test="count(document($spamxml)/spam:policy/spam:Config/spam:ProtectedProtocols/@pop3Enabled) > 0">
						<xsl:value-of select="document($spamxml)/spam:policy/spam:Config/spam:ProtectedProtocols/@pop3Enabled"/>
					</xsl:when>
					<xsl:otherwise>true</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="pvm:easy_SpamTaggingSensitivity">
		<xsl:element name="easy_SpamTaggingSensitivity" namespace="http://www.iss.net/cml/NetworkProtector/pvmail">
			<xsl:attribute name="ConfidenceLevel">
				<xsl:value-of select="document($spamxml)/spam:policy/spam:Config/spam:SpamTaggingSensitivity/@ConfidenceLevel"/>
			</xsl:attribute>
			<!--- Handle Learning or Delete Mode settings -->
			<xsl:choose>
				<xsl:when test="count(document($spamxml)/spam:policy/spam:Config/spam:SpamTaggingSensitivity/spam:LearningMode)>0">
					<xsl:element name="AntiSpamModeAllow" namespace="http://www.iss.net/cml/NetworkProtector/pvmail">
						<xsl:attribute name="default_blocking_mode">0</xsl:attribute>
					</xsl:element>
				</xsl:when>
				<xsl:otherwise>
					<xsl:element name="AntiSpamModeBlock" namespace="http://www.iss.net/cml/NetworkProtector/pvmail">
						<xsl:attribute name="default_blocking_mode">1</xsl:attribute>
					</xsl:element>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:element>
	</xsl:template>
	<xsl:template match="pvm:easy_SubjectTagFormat">
		<xsl:element name="easy_SubjectTagFormat" namespace="http://www.iss.net/cml/NetworkProtector/pvmail">
			<xsl:attribute name="SubjBelowThresholdFormat">
				<xsl:value-of select="document($spamxml)/spam:policy/spam:Config/spam:SubjectTagFormat/@SubjBelowThresholdFormat"/>
			</xsl:attribute>
			<xsl:attribute name="SubjAboveThresholdFormat">
				<xsl:value-of select="document($spamxml)/spam:policy/spam:Config/spam:SubjectTagFormat/@SubjAboveThresholdFormat"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="pvm:easy_balist">
		<xsl:element name="easy_balist" namespace="http://www.iss.net/cml/NetworkProtector/pvmail">
			<xsl:for-each select="document($spamxml)/spam:policy/spam:Config/spam:EmlSenderBlackList">
				<xsl:element name="easy_blocks" namespace="http://www.iss.net/cml/NetworkProtector/pvmail">
					<xsl:attribute name="type"/>
					<xsl:attribute name="email"><xsl:value-of select="@Name"/></xsl:attribute>
				</xsl:element>
			</xsl:for-each>
			<xsl:for-each select="document($spamxml)/spam:policy/spam:Config/spam:EmlSenderWhiteList">
				<xsl:element name="easy_allows" namespace="http://www.iss.net/cml/NetworkProtector/pvmail">
					<xsl:attribute name="type"/>
					<xsl:attribute name="email"><xsl:value-of select="@Name"/></xsl:attribute>
				</xsl:element>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
	<xsl:template match="pvm:notification_config">
		<xsl:element name="notification_config" namespace="http://www.iss.net/cml/NetworkProtector/pvmail">
			<xsl:copy-of select="@*[name() != 'pvm_stats_enabled']"/>
			<xsl:attribute name="pvm_stats_enabled">
				<xsl:choose>
					<xsl:when test="count(document($spamxml)/spam:policy/spam:NotificationConfig/@enableStatsEvents)>0">
							<xsl:value-of select="document($spamxml)/spam:policy/spam:NotificationConfig/@enableStatsEvents"/>
					</xsl:when>
					<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="pvm:pvm_stats_config_type_resp">
		<xsl:element name="pvm_stats_config_type_resp" namespace="http://www.iss.net/cml/NetworkProtector/pvmail">
			<xsl:copy-of select="@*[name() = 'sendEmail' or name() = 'emailName']"/>
			<xsl:attribute name="sendSnmp">
				<xsl:choose>
					<xsl:when test="count(document($spamxml)/spam:policy/spam:NotificationConfig/spam:StatsResponse/@sendSnmp)>0">
							<xsl:value-of select="document($spamxml)/spam:policy/spam:NotificationConfig/spam:StatsResponse/@sendSnmp"/>
					</xsl:when>
					<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="doPost">
				<xsl:choose>
					<xsl:when test="count(document($spamxml)/spam:policy/spam:NotificationConfig/spam:StatsResponse/@doPost)>0">
							<xsl:value-of select="document($spamxml)/spam:policy/spam:NotificationConfig/spam:StatsResponse/@doPost"/>
					</xsl:when>
					<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
