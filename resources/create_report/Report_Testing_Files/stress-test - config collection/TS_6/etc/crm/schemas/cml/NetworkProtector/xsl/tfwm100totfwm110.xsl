<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:tfwm="http://www.iss.net/cml/NetworkProtector/tfwm" 
xmlns:common="http://www.iss.net/cml/NetworkProtector/common" >
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<!-- Generic Copy Template -->
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<!-- Update schema version -->
	<xsl:template match="tfwm:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/tfwm">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">1.1.0</xsl:attribute>
			<xsl:apply-templates select="tfwm:Policy" />
			<xsl:apply-templates select="tfwm:MessageConfig" />
			<xsl:variable name="startpos" select="position()"/>
			<xsl:apply-templates select="tfwm:ProxyRedirectRule"><xsl:with-param name="startpos" select="$startpos"/></xsl:apply-templates>
			<xsl:apply-templates select="tfwm:L2AccessControl"/>
			<xsl:apply-templates select="tfwm:LocalRouter"/>
			<xsl:apply-templates select="tfwm:TuningSetting"/>
		</xsl:element>
	</xsl:template>
	<!-- Add in new Attributes Proxy Redirection Rules -->
	<xsl:template match="tfwm:ProxyRedirectRule">
		<xsl:param name="startpos" select="'0'"/>
		<xsl:element name="ProxyRedirectRule" namespace="http://www.iss.net/cml/NetworkProtector/tfwm">
			<xsl:copy-of select="@*"/>
			<xsl:attribute name="RuleId"><xsl:value-of select="position()-$startpos"/></xsl:attribute>
			<xsl:attribute name="Action">PROXY</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
