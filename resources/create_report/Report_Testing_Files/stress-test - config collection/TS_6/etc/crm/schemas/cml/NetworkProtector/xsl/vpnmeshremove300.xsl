<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fwm="http://www.iss.net/cml/NetworkProtector/fwm">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
    <xsl:param name="meshxml"/>
    <xsl:param name="meshName"/>
    <xsl:param name="localComponent"/>
    <xsl:variable name="localMesh" select="document($meshxml)/vpnmesh/mesh[@name=$meshName]/member[@component=$localComponent]"/>
    <xsl:variable name="mesh" select="document($meshxml)/vpnmesh/mesh[@name=$meshName]"/>
	<xsl:template match="fwm:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
            <xsl:apply-templates select="fwm:Policy"/>
			<xsl:apply-templates select="fwm:IPSECRules"/>
			<xsl:apply-templates select="fwm:CommonLists"/>
			<xsl:apply-templates select="fwm:SecurityGateways"/>
			<xsl:apply-templates select="fwm:MessageConfig"/>
			<xsl:apply-templates select="fwm:ProxyRedirectRule"/>
			<xsl:apply-templates select="fwm:VpnAdvancedSettings"/>
			<xsl:apply-templates select="fwm:TuningSetting"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*" />
			</xsl:element>
		</xsl:if>
	</xsl:template>

	<xsl:template match="fwm:Policy">
        <xsl:if test="not(@GUID=$mesh/member/access/@guid0) and not(@GUID=$mesh/member/access/@guid1)">
            <xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
                <xsl:copy-of select="@*"/>
                <xsl:apply-templates/>
            </xsl:element>
        </xsl:if>
	</xsl:template>

    <xsl:template match="fwm:IPSECRules">
        <xsl:if test="not(@GUID=$mesh/member/ipsec/@guid0) and not(@GUID=$mesh/member/ipsec/@guid1)">
            <xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
                <xsl:copy-of select="@*"/>
                <xsl:apply-templates/>
            </xsl:element>
        </xsl:if>
    </xsl:template>

	<xsl:template match="fwm:SecurityGateways">
		<xsl:element name="SecurityGateways" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:for-each select="./*">
				<xsl:choose>
					<xsl:when test="name(.)='AutoIkeSecurityGatewayList' and not( @GUID=$mesh/member/secgw/@guid0 )">
						<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:copy-of select="@*"/>
							<xsl:apply-templates/>
						</xsl:element>
					</xsl:when>
					<xsl:when test="not(name(.)='AutoIkeSecurityGatewayList')">
						<xsl:apply-templates/>
					</xsl:when>
				</xsl:choose>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>
