<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:cflt="http://www.iss.net/cml/NetworkProtector/cflt">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:template match="cflt:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/cflt">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">1.2.0</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="cflt:Spam">
		<xsl:element name="Spam" namespace="http://www.iss.net/cml/NetworkProtector/cflt">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates/>
			<xsl:element name="phishing" namespace="http://www.iss.net/cml/NetworkProtector/cflt">
				<xsl:attribute name="enabled">false</xsl:attribute>
				<xsl:attribute name="category">61</xsl:attribute>
			</xsl:element>
		</xsl:element>
</xsl:template>
</xsl:stylesheet>
