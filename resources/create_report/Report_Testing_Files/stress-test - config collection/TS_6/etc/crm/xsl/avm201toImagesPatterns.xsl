<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:avm="http://www.iss.net/cml/NetworkProtector/avm" >
<xsl:output method="text" encoding="UTF-8" />
<xsl:template match="avm:policy">
	<xsl:apply-templates select="avm:Config/avm:ExcludeExtension"/>
</xsl:template>	
<xsl:template match="avm:ExcludeExtension">
	<xsl:value-of select="@Extension"/><xsl:text>&#10;</xsl:text>
</xsl:template>
</xsl:stylesheet>
