<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:net="http://www.iss.net/cml/NetworkProtector/network" xmlns:ham="http://www.iss.net/cml/NetworkProtector/ham" xmlns:intip="http://www.iss.net/cml/NetworkProtector/intip">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="hamxml" select=" 'npham1_0.xml' "/>
	<xsl:param name="netxml" select=" 'npnetwork1_0.xml' "/>
	<xsl:param name="eth1IP" select="''"/>
	<xsl:param name="eth1Mask" select="''"/>
	<!-- This style sheet generates npintip from based on ham and network settings.  
		  It assumes that network object references have been resolved prior to running this style sheet -->
	<xsl:variable name="haEnabled" select="document($hamxml)/ham:policy/ham:baseConfig/@enabled"/>
	<xsl:variable name="netext" select="document($netxml)/net:policy/net:NetworkingConfig/net:ModeConfig/net:RoutingModeConfig/net:ExternalConfig/net:IPAddress/net:Static"/>
	<xsl:variable name="netint" select="document($netxml)/net:policy/net:NetworkingConfig/net:ModeConfig/net:RoutingModeConfig/net:InternalConfig/net:InternalInterface[@Enabled='true']"/>
	<xsl:variable name="netmgmt" select="document($netxml)/net:policy/net:NetworkingConfig/net:ModeConfig/net:TransparentModeConfig/net:TransparentMgmtConfig"/>
	<xsl:variable name="netextenabled" select="document($netxml)/net:policy/net:NetworkingConfig/net:ModeConfig/net:RoutingModeConfig/net:ExternalConfig/@Enabled"/>
	<xsl:variable name="haext" select="document($hamxml)/ham:policy/ham:baseConfig/ham:virtualIpAddress[@interfaceName='eth1']"/>
	<xsl:variable name="haint" select="document($hamxml)/ham:policy//ham:baseConfig/ham:virtualIpAddress[@interfaceName!='eth1' and @enabled='true']"/>
	<xsl:variable name="haintname" select="document($hamxml)/ham:policy/ham:baseConfig/@haInterfaceName"/>
	<xsl:template match="intip:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/intip">
			<xsl:attribute name="valid">true</xsl:attribute>
			<xsl:attribute name="version">1.0.0</xsl:attribute>
			<xsl:attribute name="overwrite">true</xsl:attribute>
			<xsl:attribute name="schema-version">1.0.0</xsl:attribute>
			<xsl:choose>
				<xsl:when test="count($netmgmt)>0">
					<!-- Handle mgmt interface if its defined - namely for transparent mode. -->
					<xsl:element name="realinterface" namespace="http://www.iss.net/cml/NetworkProtector/intip">
						<xsl:attribute name="ip"><xsl:value-of select="$netmgmt/@IpAddr"/></xsl:attribute>
						<xsl:attribute name="nm"><xsl:value-of select="$netmgmt/@SubnetMask"/></xsl:attribute>
						<xsl:attribute name="name">transmgmt</xsl:attribute>
						<xsl:attribute name="type">mgmt</xsl:attribute>
					</xsl:element>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="$haEnabled='true'">
							<xsl:if test="count($haext)>0">
								<xsl:element name="interface" namespace="http://www.iss.net/cml/NetworkProtector/intip">
									<xsl:attribute name="ip"><xsl:value-of select="$haext[1]/@ipAddress"/></xsl:attribute>
									<xsl:attribute name="nm"><xsl:value-of select="$haext[1]/@subnetMask"/></xsl:attribute>
									<xsl:attribute name="name">eth1</xsl:attribute>
									<xsl:attribute name="type">ext</xsl:attribute>
								</xsl:element>
							</xsl:if>
							<xsl:for-each select="$haint">
								<xsl:variable name="matchints" select="$haint[@interfaceName=current()/@interfaceName]"/>
								<xsl:if test="count($matchints) = 1 or $matchints[1]/@ipAddress = current()/@ipAddress">
									<xsl:element name="interface" namespace="http://www.iss.net/cml/NetworkProtector/intip">
										<xsl:attribute name="ip"><xsl:value-of select="@ipAddress"/></xsl:attribute>
										<xsl:attribute name="nm"><xsl:value-of select="@subnetMask"/></xsl:attribute>
										<xsl:attribute name="name"><xsl:value-of select="@interfaceName"/></xsl:attribute>
										<xsl:attribute name="type">int</xsl:attribute>
									</xsl:element>
								</xsl:if>
							</xsl:for-each>
							<!-- Add in hainterface information -->
							<xsl:element name="hainterface" namespace="http://www.iss.net/cml/NetworkProtector/intip">
								<xsl:attribute name="type">local</xsl:attribute>
								<!-- get the ipaddress of the local hainterface from the network xml file -->
								<xsl:variable name="halocalint" select="document($netxml)/net:policy/net:NetworkingConfig/net:ModeConfig/net:RoutingModeConfig/net:InternalConfig/net:InternalInterface[@InterfaceName=$haintname]"/>
								<xsl:attribute name="ip"><xsl:value-of select="$halocalint/@IPAddress"/></xsl:attribute>
							</xsl:element>
							<xsl:element name="hainterface" namespace="http://www.iss.net/cml/NetworkProtector/intip">
								<xsl:attribute name="type">remote</xsl:attribute>
								<xsl:variable name="haremoteint" select="document($hamxml)/ham:policy/ham:baseConfig/ham:remoteNodeInterface[@interfaceName=$haintname]"/>
								<xsl:attribute name="ip"><xsl:value-of select="$haremoteint/@ipAddress"/></xsl:attribute>
							</xsl:element>
						</xsl:when>
						<xsl:otherwise>
							<xsl:if test="$netextenabled='true' ">
								<xsl:choose>
									<xsl:when test="count($netext)>0">
										<xsl:element name="interface" namespace="http://www.iss.net/cml/NetworkProtector/intip">
											<xsl:attribute name="ip"><xsl:value-of select="$netext/@IpAddr"/></xsl:attribute>
											<xsl:attribute name="nm"><xsl:value-of select="$netext/@SubnetMask"/></xsl:attribute>
											<xsl:attribute name="name">eth1</xsl:attribute>
											<xsl:attribute name="type">ext</xsl:attribute>
										</xsl:element>
									</xsl:when>
									<xsl:when test="string-length($eth1IP)>0">
										<xsl:element name="interface" namespace="http://www.iss.net/cml/NetworkProtector/intip">
											<xsl:attribute name="ip"><xsl:value-of select="$eth1IP"/></xsl:attribute>
											<xsl:attribute name="nm"><xsl:value-of select="$eth1Mask"/></xsl:attribute>
											<xsl:attribute name="name">eth1</xsl:attribute>
											<xsl:attribute name="type">ext</xsl:attribute>
										</xsl:element>
									</xsl:when>
									<xsl:otherwise/>
								</xsl:choose>
							</xsl:if>							
							<xsl:for-each select="$netint">
								<xsl:element name="interface" namespace="http://www.iss.net/cml/NetworkProtector/intip">
									<xsl:attribute name="ip"><xsl:value-of select="@IPAddress"/></xsl:attribute>
									<xsl:attribute name="nm"><xsl:value-of select="@SubnetMask"/></xsl:attribute>
									<xsl:attribute name="name"><xsl:value-of select="@InterfaceName"/></xsl:attribute>
									<xsl:attribute name="type">int</xsl:attribute>
								</xsl:element>
							</xsl:for-each>
						</xsl:otherwise>
					</xsl:choose>
					<!-- Insert Real Interface Information for the box -->
					<!--
					<xsl:choose>
						<xsl:when test="count($netext)>0">
							<xsl:element name="realinterface" namespace="http://www.iss.net/cml/NetworkProtector/intip">
								<xsl:attribute name="ip"><xsl:value-of select="$netext/@IpAddr"/></xsl:attribute>
								<xsl:attribute name="nm"><xsl:value-of select="$netext/@SubnetMask"/></xsl:attribute>
								<xsl:attribute name="name">eth1</xsl:attribute>
								<xsl:attribute name="type">ext</xsl:attribute>
							</xsl:element>
						</xsl:when>
						<xsl:otherwise>
							<xsl:element name="realinterface" namespace="http://www.iss.net/cml/NetworkProtector/intip">
								<xsl:attribute name="ip"><xsl:value-of select="$eth1IP"/></xsl:attribute>
								<xsl:attribute name="nm"><xsl:value-of select="$eth1Mask"/></xsl:attribute>
								<xsl:attribute name="name">eth1</xsl:attribute>
								<xsl:attribute name="type">ext</xsl:attribute>
							</xsl:element>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:for-each select="$netint">
						<xsl:element name="realinterface" namespace="http://www.iss.net/cml/NetworkProtector/intip">
							<xsl:attribute name="ip"><xsl:value-of select="@IPAddress"/></xsl:attribute>
							<xsl:attribute name="nm"><xsl:value-of select="@SubnetMask"/></xsl:attribute>
							<xsl:attribute name="name"><xsl:value-of select="@InterfaceName"/></xsl:attribute>
							<xsl:attribute name="type">int</xsl:attribute>
						</xsl:element>
					</xsl:for-each>
					-->
				</xsl:otherwise>
			</xsl:choose>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
