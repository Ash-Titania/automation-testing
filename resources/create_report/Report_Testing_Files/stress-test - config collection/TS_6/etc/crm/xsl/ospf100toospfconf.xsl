<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ospf="http://www.iss.net/cml/NetworkProtector/ospf" xmlns:net="http://www.iss.net/cml/NetworkProtector/network">
	<xsl:output method="text" encoding="UTF-8"/>
	<xsl:param name="netxml" select="'npnetwork1_0.xml'"/>
	<xsl:template match="ospf:policy">
		<xsl:text>!&#10;log syslog&#10;</xsl:text>
		<xsl:apply-templates select="ospf:OspfConfig/ospf:Interface"/>
		<xsl:apply-templates select="ospf:OspfConfig/ospf:Router"/>
	</xsl:template>
	<xsl:template match="ospf:Interface">
		<xsl:text>!&#10;</xsl:text>
		<xsl:text>interface </xsl:text>
		<xsl:value-of select="@InterfaceName"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:choose>
			<xsl:when test="@Authentication='MD5' ">
				<xsl:text> ip ospf message-digest-key </xsl:text>
				<xsl:value-of select="@MD5KeyId"/>
				<xsl:text> md5 </xsl:text>
				<xsl:value-of select="@MD5Key"/>
				<xsl:text>&#10;</xsl:text>
			</xsl:when>
			<xsl:when test="@Authentication='Plain' ">
				<xsl:text> ip ospf authentication-key </xsl:text>
				<xsl:value-of select="@PlainPw"/>
				<xsl:text>&#10;</xsl:text>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text> ip ospf cost </xsl:text>
		<xsl:value-of select="@Cost"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:text> ip ospf dead-interval </xsl:text>
		<xsl:value-of select="@DeadInterval"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:text> ip ospf hello-interval </xsl:text>
		<xsl:value-of select="@HelloInterval"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:text> ip ospf priority </xsl:text>
		<xsl:value-of select="@Priority"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:text> ip ospf retransmit-interval </xsl:text>
		<xsl:choose>
			<xsl:when test="@RetransmitInterval > 2">
				<xsl:value-of select="@RetransmitInterval"/>
			</xsl:when>
			<xsl:otherwise>3</xsl:otherwise>
		</xsl:choose>
		<xsl:text>&#10;</xsl:text>
		<xsl:text> ip ospf transmit-delay </xsl:text>
		<xsl:value-of select="@TransmitDelay"/>
		<xsl:text>&#10;</xsl:text>
	</xsl:template>
	<xsl:template match="ospf:*">
		<xsl:text>!&#10;</xsl:text>
		<xsl:text>router ospf&#10;</xsl:text>
		<xsl:text> ospf router-id </xsl:text>
		<xsl:value-of select="../@RouterId"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:if test="string-length(@DefaultMetric)>0">
			<xsl:text> default-metric </xsl:text>
			<xsl:value-of select="@DefaultMetric"/>
			<xsl:text>&#10;</xsl:text>
		</xsl:if>
		<xsl:if test="@DefaultOriginate='true' ">
			<xsl:choose>
				<xsl:when test="Always='true' ">
					<xsl:text> default-information originate alwways metric </xsl:text>
					<xsl:value-of select="@DefaultOriginateMetric"/>
					<xsl:text>&#10;</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text> default-information originate metric </xsl:text>
					<xsl:value-of select="@DefaultOriginateMetric"/>
					<xsl:text>&#10;</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
		<xsl:if test="@RedistributeSytemRoutes='true' ">
			<xsl:text> redistribute kernel metric </xsl:text>
			<xsl:value-of select="@SystemRouteMetric"/>
			<xsl:text>&#10;</xsl:text>
		</xsl:if>
		<xsl:if test="@RedistributeConnectedRoutes='true' ">
			<xsl:text> redistribute connected metric </xsl:text>
			<xsl:value-of select="@ConnectedRouteMetric"/>
			<xsl:text>&#10;</xsl:text>
		</xsl:if>		
		<xsl:apply-templates select="document($netxml)/net:policy/net:Interface">
			<xsl:with-param name="ospfint" select="/ospf:policy/ospf:OspfConfig/ospf:Interface"/>
		</xsl:apply-templates>
		<xsl:apply-templates select="/ospf:policy/ospf:OspfConfig/ospf:Area/ospf:Network"/>
		<xsl:apply-templates select="/ospf:policy/ospf:OspfConfig/ospf:Area"/>
		<xsl:apply-templates select="/ospf:policy/ospf:OspfConfig/ospf:VirtualLink"/>
	</xsl:template>
	<xsl:template match="ospf:Area/ospf:Network">
		<xsl:text> network </xsl:text>
		<xsl:value-of select="@cidr"/>
		<xsl:text> area </xsl:text>
		<xsl:choose>
			<xsl:when test="count(../ospf:AreaId/ospf:AreadId)>0">
				<xsl:value-of select="../ospf:AreaId/ospf:AreadId/@value"/>
				<xsl:text>&#10;</xsl:text>
			</xsl:when>
			<xsl:when test="count(../ospf:AreaId/ospf:IPAddress)>0">
				<xsl:value-of select="../ospf:AreaId/ospf:IPAddress/@value"/>
				<xsl:text>&#10;</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>&#10;</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="ospf:Area">
		<xsl:variable name="areaid">
			<xsl:choose>
				<xsl:when test="count(ospf:AreaId/ospf:AreadId)>0">
					<xsl:value-of select="ospf:AreaId/ospf:AreadId/@value"/>
				</xsl:when>
				<xsl:when test="count(ospf:AreaId/ospf:IPAddress)>0">
					<xsl:value-of select="ospf:AreaId/ospf:IPAddress/@value"/>
				</xsl:when>
				<xsl:otherwise>
					undefined
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:if test="@Stub='true'">
			<xsl:text> area </xsl:text>
			<xsl:value-of select="$areaid"/>
			<xsl:text> stub </xsl:text>
			<xsl:if test="@DisableSummary='true' ">
				<xsl:text>no-summary</xsl:text>
			</xsl:if>
			<xsl:text>&#10;</xsl:text>
			<xsl:text> area </xsl:text>
			<xsl:value-of select="$areaid"/>
			<xsl:text> default-cost </xsl:text>
			<xsl:value-of select="@DefaultCost"/>
			<xsl:text>&#10;</xsl:text>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="@Authentication='MD5' ">
				<xsl:text> area </xsl:text>
				<xsl:value-of select="$areaid"/>
				<xsl:text> authentication message-digest</xsl:text>
				<xsl:text>&#10;</xsl:text>
			</xsl:when>
			<xsl:when test="@Authentication='Plain' ">
				<xsl:text> area </xsl:text>
				<xsl:value-of select="$areaid"/>
				<xsl:text> authentication</xsl:text>
				<xsl:text>&#10;</xsl:text>
			</xsl:when>
			<xsl:otherwise>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="ospf:VirtualLink">
		<xsl:variable name="areaid">
			<xsl:choose>
				<xsl:when test="count(ospf:AreaId/ospf:AreadId)>0">
					<xsl:value-of select="ospf:AreaId/ospf:AreadId/@value"/>
				</xsl:when>
				<xsl:when test="count(ospf:AreaId/ospf:IPAddress)>0">
					<xsl:value-of select="ospf:AreaId/ospf:IPAddress/@value"/>
				</xsl:when>
				<xsl:otherwise>
					undefined
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:text> area </xsl:text>
		<xsl:value-of select="$areaid"/>
		<xsl:text> virtual-link </xsl:text>
		<xsl:value-of select="@RemoteRouterId"/>
		<xsl:text> hello-interval </xsl:text>
		<xsl:value-of select="@HelloInterval"/>
		<xsl:text> retransmit-interval </xsl:text>
		<xsl:choose>
			<xsl:when test="@RetransmitInterval > 2">
				<xsl:value-of select="@RetransmitInterval"/>
			</xsl:when>
			<xsl:otherwise>3</xsl:otherwise>
		</xsl:choose>
		<xsl:text> transmit-delay </xsl:text>
		<xsl:value-of select="@TransmitDelay"/>
		<xsl:text> dead-interval </xsl:text>
		<xsl:value-of select="@DeadInterval"/>
		<xsl:text>&#10;</xsl:text>
		<xsl:text> area </xsl:text>
		<xsl:value-of select="$areaid"/>
		<xsl:text> virtual-link </xsl:text>
		<xsl:value-of select="@RemoteRouterId"/>
		<xsl:choose>
			<xsl:when test="@Authentication='MD5' ">
				<xsl:text> message-digest-key </xsl:text>
				<xsl:value-of select="@MD5KeyId"/>
				<xsl:text> md5 </xsl:text>
				<xsl:value-of select="@MD5Key"/>
				<xsl:text>&#10;</xsl:text>
			</xsl:when>
			<xsl:when test="@Authentication='Plain' ">
				<xsl:text> authentication-key </xsl:text>
				<xsl:value-of select="@PlainPw"/>
				<xsl:text>&#10;</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>&#10;</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="net:Interface">
		<xsl:param name="ospfint"/>
		<xsl:variable name="inuse" select="$ospfint[@InterfaceName=current()/@Name]"/>
		<xsl:choose>
			<xsl:when test="count($inuse) = 0">
				<xsl:text> passive-interface </xsl:text>
				<xsl:value-of select="@Name"/>
				<xsl:text>&#10;</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:if test="$inuse/@DisableOspf='true' ">
					<xsl:text> passive-interface </xsl:text>
					<xsl:value-of select="@Name"/>
					<xsl:text>&#10;</xsl:text>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
