<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:mgmt="http://www.iss.net/cml/NetworkProtector/mgmt">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<!-- This style sheet sets the registered boolean to true -->
		<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="mgmt:Config">
		<xsl:element name="Config" namespace="http://www.iss.net/cml/NetworkProtector/mgmt">
			<xsl:choose>
				<xsl:when test="@Enabled = 'false' ">
					<xsl:choose>
						<xsl:when test="count(mgmt:AgentManager)>0">
							<xsl:attribute name="Enabled">true</xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="Enabled">false</xsl:attribute>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="Enabled">true</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:copy-of select="@*[name() != 'Enabled']"/>
			<xsl:apply-templates />
		</xsl:element>
		</xsl:template>
</xsl:stylesheet>
