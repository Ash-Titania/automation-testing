<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:network="http://www.iss.net/cml/NetworkProtector/network"
	xmlns:ham="http://www.iss.net/cml/NetworkProtector/ham" >
	<!-- This style sheet replaces the value of each interfaces IPAddress in of the network xml xml file with the value of the ipAddress attribute for the corresponding interface in the remoteNodeInterface element of the ham xml file. -->
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="hamxml" select=" 'npham1_0.xml' "/>
	<xsl:variable name="remoteNodes" select="document($hamxml)/ham:policy/ham:baseConfig/ham:remoteNodeInterface"/>
	<xsl:variable name="haInterface" select="document($hamxml)/ham:policy/ham:baseConfig/@haInterfaceName"/>
	<xsl:variable name="haremoteNode" select="$remoteNodes[@interfaceName=$haInterface]"/>
	<xsl:variable name="internalHaInterface" select="network:policy/network:RoutingModeConfig/network:InternalConfig/network:InternalInterface[@InterfaceName=$haremoteNode/@interfaceName]"/>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="network:InternalInterface">
		<xsl:element name="InternalInterface" namespace="http://www.iss.net/cml/NetworkProtector/network">
			<xsl:copy-of select="@*[name() != 'IPAddress' ]"/>
			<xsl:attribute name="IPAddress">
				<xsl:variable name="ipaddr" select="$remoteNodes[@interfaceName=current()/@InterfaceName]"/>
				<xsl:choose>
					<xsl:when test="count($ipaddr)>0"><xsl:value-of select="$ipaddr/@ipAddress"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="current()/@IPAddress"/></xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="network:Static">
		<xsl:element name="Static" namespace="http://www.iss.net/cml/NetworkProtector/network">
			<xsl:copy-of select="@*[name() != 'IpAddr' or name() != 'GatewayIpAddr' ]"/>
			<xsl:attribute name="IpAddr">
				<xsl:variable name="ipaddr" select="$remoteNodes[@interfaceName='eth1']"/>
				<xsl:choose>
					<xsl:when test="count($ipaddr)>0"><xsl:value-of select="$ipaddr/@ipAddress"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="current()/@IpAddr"/></xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:attribute name="GatewayIpAddr">
				<xsl:choose>
					<xsl:when test="@GatewayIpAddr=$haremoteNode/@ipAddress">
						<xsl:value-of select="$internalHaInterface/@IPAddress"/>
					</xsl:when>
					<xsl:otherwise><xsl:value-of select="@GatewayIpAddr"/></xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
