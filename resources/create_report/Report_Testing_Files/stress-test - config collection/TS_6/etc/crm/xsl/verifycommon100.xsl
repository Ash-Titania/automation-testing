<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:common="http://www.iss.net/cml/NetworkProtector/common" 
xmlns:netobj="http://www.iss.net/cml/NetworkObjects"
xmlns:dynaddr="http://www.iss.net/cml/NetworkProtector/dynaddr" >
	<!-- Define common parms that need to be set -->
	<xsl:param name="netobjxml" select="'networkobjects1_0.xml' "/>
	<xsl:param name="dynaddrxml" select="'npdynaddr1_0.xml' "/>
	<!-- These variables presume that dynaddrxml and netobjxml already set. -->
	<xsl:variable name="dynaddrs" select="document($dynaddrxml)/dynaddr:policy/dynaddr:DynamicAddressList"/>
	<xsl:variable name="addrnames" select="document($netobjxml)/netobj:policy/netobj:AddressList"/>
	<!-- Generic Verify Templates -->
	<xsl:template name="checkDynaddrIpAddr">
		<xsl:param name="Name"/>
		<xsl:param name="ruleEnabled" select="'true'"/>
		<xsl:variable name="dynaddr" select="$dynaddrs[@Name=$Name]"/>
		<xsl:if test="count($dynaddr) = 0 and $ruleEnabled='true' ">
			<xsl:text>ERROR: Dynaddr </xsl:text>
			<xsl:value-of select="$Name"/>
			<xsl:text> is referenced in the </xsl:text>
			<xsl:value-of select="$policyName"/>
			<xsl:text> policy but not defined in the dynamic address policy. If </xsl:text>
			<xsl:value-of select="$Name"/>
			<xsl:text> is a built-in dynamic address then it is not available in the network mode (transparent or routing) you are trying to use it in. You must correct the reference to that built-in dynamic address in the </xsl:text>
			<xsl:value-of select="$policyName"/>
			<xsl:text> policy.</xsl:text>
			<xsl:text>&#10;</xsl:text>
		</xsl:if>
		<!-- Now check to make sure it is a simple IP Address -->
		<xsl:variable name="check" select="$dynaddr/dynaddr:SingleAddress/@IPSingleAddress"/>
		<xsl:if test="count($check) = 0">
			<xsl:text>ERROR: Dynaddr </xsl:text>
			<xsl:value-of select="$Name"/>
			<xsl:text> is referenced in the </xsl:text>
			<xsl:value-of select="$policyName"/>
			<xsl:text> policy as a simple IP but is not defined in the dynamic address policy as a simple IP type.</xsl:text><xsl:text>&#10;</xsl:text>
		</xsl:if>		
	</xsl:template>
	<xsl:template name="checkDynaddrSubnet">
		<xsl:param name="Name"/>
		<xsl:param name="ruleEnabled" select="'true'"/>
		<xsl:variable name="dynaddr" select="$dynaddrs[@Name=$Name]"/>
		<xsl:if test="count($dynaddr) = 0 and $ruleEnabled='true' ">
			<xsl:text>ERROR: Dynaddr </xsl:text>
			<xsl:value-of select="$Name"/>
			<xsl:text> is referenced in the </xsl:text>
			<xsl:value-of select="$policyName"/>
			<xsl:text> policy but not defined in the dynamic address policy. If </xsl:text>
			<xsl:value-of select="$Name"/>
			<xsl:text> is a built-in dynamic address then it is not available in the network mode (transparent or routing) you are trying to use it in. You must correct the reference to that built-in dynamic address in the </xsl:text>
			<xsl:value-of select="$policyName"/>
			<xsl:text> policy.</xsl:text>
			<xsl:text>&#10;</xsl:text>
		</xsl:if>
		<!-- Now check to make sure it is a IPAddress / Subnet Mask e.g. 123.123.123.123 and 255.255.255.0 -->
		<xsl:variable name="check" select="$dynaddr/dynaddr:AddressSubnet/@IPAddress"/>
		<xsl:if test="count($check) = 0">
			<xsl:text>ERROR: Dynaddr </xsl:text>
			<xsl:value-of select="$Name"/>
			<xsl:text> is referenced in the </xsl:text>
			<xsl:value-of select="$policyName"/>
			<xsl:text> policy as a Network Address and Subnet Mask but is not defined in the dynamic address policy as Network Address and Subnet Mask type.</xsl:text><xsl:text>&#10;</xsl:text>
		</xsl:if>		
	</xsl:template>
	<xsl:template name="checkDynaddrRange">
		<xsl:param name="Name"/>
		<xsl:param name="ruleEnabled" select="'true'"/>
		<xsl:variable name="dynaddr" select="$dynaddrs[@Name=$Name]"/>
		<xsl:if test="count($dynaddr) = 0 and $ruleEnabled='true' ">
			<xsl:text>ERROR: Dynaddr </xsl:text>
			<xsl:value-of select="$Name"/>
			<xsl:text> is referenced in the </xsl:text>
			<xsl:value-of select="$policyName"/>
			<xsl:text> policy but not defined in the dynamic address policy. If </xsl:text>
			<xsl:value-of select="$Name"/>
			<xsl:text> is a built-in dynamic Address then it is not available in the network mode (transparent or routing) you are trying to use it in. You must correct the reference to that built-in dynamic address in the </xsl:text>
			<xsl:value-of select="$policyName"/>
			<xsl:text> policy.</xsl:text>
			<xsl:text>&#10;</xsl:text>
		</xsl:if>
		<!-- Now check to make sure it is an Address Range eg 123.123.123.1-123.123.123.255 -->
		<xsl:variable name="check" select="$dynaddr/dynaddr:AddressRange/@IPRangeAddress"/>
		<xsl:if test="count($check) = 0">
			<xsl:text>ERROR: Dynaddr </xsl:text>
			<xsl:value-of select="$Name"/>
			<xsl:text> is referenced in the </xsl:text>
			<xsl:value-of select="$policyName"/>
			<xsl:text> policy as an Address Range but is not defined in the dynamic address policy as an Address Range type.</xsl:text><xsl:text>&#10;</xsl:text>
		</xsl:if>		
	</xsl:template>
	<xsl:template name="checkDynaddrCidr">
		<xsl:param name="Name"/>
		<xsl:param name="ruleEnabled" select="'true'"/>
		<xsl:variable name="dynaddr" select="$dynaddrs[@Name=$Name]"/>
		<xsl:if test="count($dynaddr) = 0 and $ruleEnabled='true' ">
			<xsl:text>ERROR: Dynaddr </xsl:text>
			<xsl:value-of select="$Name"/>
			<xsl:text> is referenced in the </xsl:text>
			<xsl:value-of select="$policyName"/>
			<xsl:text> policy but not defined in the dynamic address policy. If </xsl:text>
			<xsl:value-of select="$Name"/>
			<xsl:text> is a built-in dynamic address then it is not available in the network mode (transparent or routing) you are trying to use it in. You must correct the reference to that built-in dynamic address in the </xsl:text>
			<xsl:value-of select="$policyName"/>
			<xsl:text> policy.</xsl:text>
			<xsl:text>&#10;</xsl:text>
		</xsl:if>
		<!-- Now check to make sure it is a CIDR Address/Mask 12.12.12.0/24 -->
		<xsl:variable name="check" select="$dynaddr/dynaddr:AddressMask/@IPAddressMask"/>
		<xsl:if test="count($check) = 0">
			<xsl:text>ERROR: Dynaddr </xsl:text>
			<xsl:value-of select="$Name"/>
			<xsl:text> is referenced in the </xsl:text>
			<xsl:value-of select="$policyName"/>
			<xsl:text> policy as an Address/Mask but is not defined in the dynamic address policy as an Address/Mask type.</xsl:text><xsl:text>&#10;</xsl:text>
		</xsl:if>		
	</xsl:template>
	<!-- Templates for verfiying address name references -->
	<xsl:template name="checkAddrnameIpAddr">
		<xsl:param name="Name"/>
		<xsl:param name="ruleEnabled" select="'true' "/>
		<xsl:variable name="addrname" select="$addrnames[@Name=$Name]"/>
		<xsl:if test="count($addrname) = 0 and $ruleEnabled='true' ">
			<xsl:text>ERROR: Address Name </xsl:text>
			<xsl:value-of select="$Name"/>
			<xsl:text> is referenced in the </xsl:text>
			<xsl:value-of select="$policyName"/>
			<xsl:text> policy but not defined in the network objects policy.</xsl:text>
			<xsl:text>Please make sure that all network objects used in all policies are defined in the network objects policy.</xsl:text>
			<xsl:text>&#10;</xsl:text>
		</xsl:if>
		<!-- Now check to make sure it is a simple IP Address -->
		<xsl:variable name="check" select="$addrname/netobj:SingleAddress/@IPSingleAddress"/>
		<xsl:if test="count($check) = 0">
			<xsl:text>ERROR: Address Name </xsl:text>
			<xsl:value-of select="$Name"/>
			<xsl:text> is referenced in the </xsl:text>
			<xsl:value-of select="$policyName"/>
			<xsl:text> policy as a simple IP but is not defined in the network object policy as a simple IP type.</xsl:text><xsl:text>&#10;</xsl:text>
		</xsl:if>		
	</xsl:template>
	<xsl:template name="checkAddrnameSubnet">
		<xsl:param name="Name"/>
		<xsl:param name="ruleEnabled" select="'true' "/>
		<xsl:variable name="addrname" select="$addrnames[@Name=$Name]"/>
		<xsl:if test="count($addrname) = 0 and $ruleEnabled='true' ">
			<xsl:text>ERROR: Address Name </xsl:text>
			<xsl:value-of select="$Name"/>
			<xsl:text> is referenced in the </xsl:text>
			<xsl:value-of select="$policyName"/>
			<xsl:text> policy but not defined in the network objects policy.</xsl:text>
			<xsl:text>Please make sure that all network objects used in all policies are defined in the network objects policy.</xsl:text>
			<xsl:text>&#10;</xsl:text>
		</xsl:if>
		<!-- Now check to make sure it is a IP Address / Subnet Mask-->
		<xsl:variable name="check" select="$addrname/netobj:AddressSubnet/@IPAddress"/>
		<xsl:if test="count($check) = 0">
			<xsl:text>ERROR: Address Name </xsl:text>
			<xsl:value-of select="$Name"/>
			<xsl:text> is referenced in the </xsl:text>
			<xsl:value-of select="$policyName"/>
			<xsl:text> policy as a Network Address and Subnet Mask but is not defined in the network object policy as a Network Address and Subnet Mask type.</xsl:text><xsl:text>&#10;</xsl:text>
		</xsl:if>		
	</xsl:template>
	<xsl:template name="checkAddrnameRange">
		<xsl:param name="Name"/>
		<xsl:param name="ruleEnabled" select="'true' "/>
		<xsl:variable name="addrname" select="$addrnames[@Name=$Name]"/>
		<xsl:if test="count($addrname) = 0 and $ruleEnabled='true' ">
			<xsl:text>ERROR: Address Name </xsl:text>
			<xsl:value-of select="$Name"/>
			<xsl:text> is referenced in the </xsl:text>
			<xsl:value-of select="$policyName"/>
			<xsl:text> policy but not defined in the network objects policy.</xsl:text>
			<xsl:text>Please make sure that all network objects used in all policies are defined in the network objects policy.</xsl:text>
			<xsl:text>&#10;</xsl:text>
		</xsl:if>
		<!-- Now check to make sure it is a IP Address Range -->
		<xsl:variable name="check" select="$addrname/netobj:AddressRange/@IPRangeAddress"/>
		<xsl:if test="count($check) = 0">
			<xsl:text>ERROR: Address Name </xsl:text>
			<xsl:value-of select="$Name"/>
			<xsl:text> is referenced in the </xsl:text>
			<xsl:value-of select="$policyName"/>
			<xsl:text> policy as an Address Range but is not defined in the network object policy as an Address Range type.</xsl:text><xsl:text>&#10;</xsl:text>
		</xsl:if>		
	</xsl:template>
	<xsl:template name="checkAddrnameCidr">
		<xsl:param name="Name"/>
		<xsl:param name="ruleEnabled" select="'true' "/>
		<xsl:variable name="addrname" select="$addrnames[@Name=$Name]"/>
		<xsl:if test="count($addrname) = 0 and $ruleEnabled='true' ">
			<xsl:text>ERROR: Address Name </xsl:text>
			<xsl:value-of select="$Name"/>
			<xsl:text> is referenced in the </xsl:text>
			<xsl:value-of select="$policyName"/>
			<xsl:text> policy but not defined in the network objects policy.</xsl:text>
			<xsl:text>Please make sure that all network objects used in all policies are defined in the network objects policy.</xsl:text>
			<xsl:text>&#10;</xsl:text>
		</xsl:if>
		<!-- Now check to make sure it is a IP Address / bitvalue -->
		<xsl:variable name="check" select="$addrname/netobj:AddressMask/@IPAddressMask"/>
		<xsl:if test="count($check) = 0">
			<xsl:text>ERROR: Address Name </xsl:text>
			<xsl:value-of select="$Name"/>
			<xsl:text> is referenced in the </xsl:text>
			<xsl:value-of select="$policyName"/>
			<xsl:text> policy as an Address/Mask but is not defined in the network object policy as an Address/Mask type.</xsl:text><xsl:text>&#10;</xsl:text>
		</xsl:if>		
	</xsl:template>
	<!-- Handle checking to make sure the common elements can be resolved. In the case of dynaddrs verify they are also the correct type too. -->
	<xsl:template match="common:AddrAN" >
		<xsl:param name="ruleEnabled" select="'true' "/>
		<xsl:call-template name="checkAddrnameIpAddr">
			<xsl:with-param name="Name" select="current()/@Name"/>
			<xsl:with-param name="ruleEnabled" select="$ruleEnabled"/>
		</xsl:call-template>		
	</xsl:template>
	<xsl:template match="common:AddrDA" >
		<xsl:param name="ruleEnabled" select="'true' "/>
		<xsl:call-template name="checkDynaddrIpAddr">
			<xsl:with-param name="Name" select="current()/@Name"/>
			<xsl:with-param name="ruleEnabled" select="$ruleEnabled"/>
		</xsl:call-template>		
	</xsl:template>
	<!-- IP/Subnet Mask Elements -->
	<xsl:template match="common:SubnetAN" >
		<xsl:param name="ruleEnabled" select="'true' "/>
		<xsl:call-template name="checkAddrnameSubnet">
			<xsl:with-param name="Name" select="current()/@Name"/>
			<xsl:with-param name="ruleEnabled" select="$ruleEnabled"/>
		</xsl:call-template>		
	</xsl:template>
	<xsl:template match="common:SubnetDA" >
		<xsl:param name="ruleEnabled" select="'true' "/>
		<xsl:call-template name="checkDynaddrSubnet">
			<xsl:with-param name="Name" select="current()/@Name"/>
			<xsl:with-param name="ruleEnabled" select="$ruleEnabled"/>
		</xsl:call-template>		
	</xsl:template>
	<!-- IP Range Elements -->
	<xsl:template match="common:AddrrngAN" >
		<xsl:param name="ruleEnabled" select="'true' "/>
		<xsl:call-template name="checkAddrnameRange">
			<xsl:with-param name="Name" select="current()/@Name"/>
			<xsl:with-param name="ruleEnabled" select="$ruleEnabled"/>
		</xsl:call-template>		
	</xsl:template>
	<xsl:template match="common:AddrrngDA" >
		<xsl:param name="ruleEnabled" select="'true' "/>
		<xsl:call-template name="checkDynaddrRange">
			<xsl:with-param name="Name" select="current()/@Name"/>
			<xsl:with-param name="ruleEnabled" select="$ruleEnabled"/>
		</xsl:call-template>		
	</xsl:template>
	<!-- CIDR Elements -->
	<xsl:template match="common:AddrcidrAN" >
		<xsl:param name="ruleEnabled" select="'true' "/>
		<xsl:call-template name="checkAddrnameCidr">
			<xsl:with-param name="Name" select="current()/@Name"/>
			<xsl:with-param name="ruleEnabled" select="$ruleEnabled"/>
		</xsl:call-template>		
	</xsl:template>
	<xsl:template match="common:AddrcidrDA" >
		<xsl:param name="ruleEnabled" select="'true' "/>
		<xsl:call-template name="checkDynaddrCidr">
			<xsl:with-param name="Name" select="current()/@Name"/>
			<xsl:with-param name="ruleEnabled" select="$ruleEnabled"/>
		</xsl:call-template>		
	</xsl:template>	
</xsl:stylesheet>
