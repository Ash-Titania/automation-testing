<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:avm="http://www.iss.net/cml/NetworkProtector/avm" xmlns:npcommon="http://www.iss.net/cml/NetworkProtector/common">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:template match="avm:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/avm">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">2.3.0</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="avm:ProtectedProtocols">
		<xsl:element name="ProtectedProtocols" namespace="{namespace-uri(.)}">
			<xsl:copy-of select="@*[name() !='vpshttpEnabled' or name() !='vpsftpEnabled' or name() != 'vpspop3Enabled' or name() != 'vpssmtpEnabled']"/>
			<xsl:attribute name="vpshttpEnabled">false</xsl:attribute>
			<xsl:attribute name="vpsftpEnabled">false</xsl:attribute>
			<xsl:attribute name="vpspop3Enabled">false</xsl:attribute>
			<xsl:attribute name="vpssmtpEnabled">false</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
