<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:fwm="http://www.iss.net/cml/NetworkProtector/fwm" 
xmlns:tfwm="http://www.iss.net/cml/NetworkProtector/tfwm" 
xmlns:dynaddr="http://www.iss.net/cml/NetworkProtector/dynaddr"
xmlns:netobj="http://www.iss.net/cml/NetworkObjects"
xmlns:intip="http://www.iss.net/cml/NetworkProtector/intip"
xmlns:network="http://www.iss.net/cml/NetworkProtector/network"
xmlns:common="http://www.iss.net/cml/NetworkProtector/common">
	<xsl:output method="text" encoding="UTF-8" indent="no"/>
	<xsl:strip-space elements="*"/>
	<!-- In order for this script to function properly dynaddr and netobj references need to be resolved prior to running this script -->
	<xsl:include href="fwm400verifyvalid.xsl" />
	<xsl:param name="fwmxml" select="'npfwm1_0.xml'"/>
	<xsl:param name="tfwmxml" select="'nptfwm1_0.xml' "/>
	<xsl:template match="network:policy/network:NetworkingConfig/network:ModeConfig/network:RoutingModeConfig">
		<xsl:apply-templates select="document($fwmxml)/fwm:policy"/>
		<xsl:apply-templates select="document($netobjxml)/netobj:policy/netobj:AddressGroupList"/>
	</xsl:template>
	<xsl:template match="network:policy/network:NetworkingConfig/network:ModeConfig/network:TransparentModeConfig">
		<xsl:apply-templates select="document($tfwmxml)/tfwm:policy"/>
		<xsl:apply-templates select="document($netobjxml)/netobj:policy/netobj:AddressGroupList"/>
	</xsl:template>
</xsl:stylesheet>
