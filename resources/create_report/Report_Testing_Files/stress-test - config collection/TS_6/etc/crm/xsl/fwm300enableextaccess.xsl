<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:fwm="http://www.iss.net/cml/NetworkProtector/fwm" >
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:template match="fwm:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="fwm:Policy">
		<!-- Enable the Self policies that Allow HTTPS and SSH access for any -->
		<xsl:element name="Policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:choose>
				<xsl:when test="count(fwm:SourceAddress/fwm:AnyAddress) > 0 and fwm:DestPort/fwm:Ports/fwm:PortEntry/fwm:PortName/@value = 'HTTPS' ">
					<xsl:attribute name="Enabled">true</xsl:attribute>
				</xsl:when>
				<xsl:when test="count(fwm:SourceAddress/fwm:AnyAddress) > 0 and fwm:DestPort/fwm:Ports/fwm:PortEntry/fwm:PortName/@value = 'SSH' ">
					<xsl:attribute name="Enabled">true</xsl:attribute>
				</xsl:when>
				<xsl:when test="count(fwm:SourceAddress/fwm:AnyAddress) > 0 and fwm:DestPort/fwm:SinglePort/@SinglePort='443'  ">
					<xsl:attribute name="Enabled">true</xsl:attribute>
				</xsl:when>			
				<xsl:when test="count(fwm:SourceAddress/fwm:AnyAddress) > 0 and fwm:DestPort/fwm:SinglePort/@SinglePort='22'  ">
					<xsl:attribute name="Enabled">true</xsl:attribute>
				</xsl:when>			
				<xsl:otherwise>
					<xsl:attribute name="Enabled"><xsl:value-of select="@Enabled"/></xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:copy-of select="@*[name() != 'Enabled']"/>
		<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
