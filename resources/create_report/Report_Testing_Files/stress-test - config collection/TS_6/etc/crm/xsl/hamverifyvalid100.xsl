<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:net="http://www.iss.net/cml/NetworkProtector/network" 
xmlns:ham="http://www.iss.net/cml/NetworkProtector/ham"
xmlns:ospf="http://www.iss.net/cml/NetworkProtector/ospf">
	<xsl:output method="text" encoding="UTF-8" indent="no"/>
	<xsl:strip-space elements="*"/>
	<!-- In order for this script to function properly dynaddr and netobj references need to be resolved prior to running this script currently. 
		  Should I rework this script to work off of the non-resolved variant? -->
	<xsl:include href="verifycommon100.xsl" />

	<xsl:variable name="policyName" select="'high availability'"/>
			
</xsl:stylesheet>
