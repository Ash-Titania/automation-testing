<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:avm="http://www.iss.net/cml/NetworkProtector/avm" 
xmlns:pvmail="http://www.iss.net/cml/NetworkProtector/pvmail" 
xmlns:net="http://www.iss.net/cml/NetworkProtector/network" 
xmlns:srv="http://www.iss.net/cml/NetworkProtector/srv"
xmlns:common="http://www.iss.net/cml/NetworkProtector/common"  >
	<xsl:output method="text" encoding="UTF-8" indent="no"/>
	<xsl:strip-space elements="*"/>
	<!-- In order for this script to function properly dynaddr and netobj references need to be resolved prior to running this script currently. 
		  Should I rework this script to work off of the non-resolved variant? -->
	<!-- <xsl:include href="verifycommon100.xsl" /> -->
	<xsl:param name="avmxml" select=" 'npavm1_0.xml' "/>
	<xsl:param name="spamxml" select=" 'pvmail1_0.xml' "/>
	<xsl:param name="netxml" select=" 'npnetwork1_0.xml' " />
	<!-- This variable is used by some of the templates in the verifycommon100.xsl script -->
	<xsl:variable name="policyName" select="'services'"/>
	<xsl:variable name="smtpProxyEnabled">
		<xsl:choose>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@smtpEnabled='true' ">true</xsl:when>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@vpssmtpEnabled='true' ">true</xsl:when>
			<xsl:when test="document($spamxml)/pvmail:policy/pvmail:settings/@enabled='true' ">
				<xsl:choose>
					<xsl:when test="document($spamxml)/pvmail:policy/pvmail:settings/@mode='0' ">
						<xsl:choose>
							<xsl:when test="document($spamxml)/pvmail:policy/pvmail:settings/pvmail:easy_spam_settings/pvmail:easy_protected_protocols/@smtpEnabled='true' ">true</xsl:when>
							<xsl:otherwise>false</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="document($spamxml)/pvmail:policy/pvmail:settings/@mode='1' ">
						<xsl:choose>
							<xsl:when test="document($spamxml)/pvmail:policy/pvmail:settings/pvmail:general_tab/pvmail:general_settings/pvmail:smtp_mode/pvmail:transparent_mode/@smtpEnabled='true' ">true</xsl:when>
							<xsl:otherwise>false</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="tuningparm1" select="document($avmxml)/avm:policy/avm:TuningSetting[@name='avm.se.smtp.psmtpd.address_transparent']"/>
	<xsl:variable name="smtptrans">
		<xsl:choose>
			<xsl:when test="count($tuningparm1) > 0">
				<xsl:value-of select="$tuningparm1/common:boolean/@value"/>
			</xsl:when>
			<xsl:otherwise>true</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="tuningparm2" select="document($avmxml)/avm:policy/avm:TuningSetting[@name='avm.se.pop3.ppop3d.address_transparent']"/>
	<xsl:variable name="pop3trans">
		<xsl:choose>
			<xsl:when test="count($tuningparm2) > 0">
				<xsl:value-of select="$tuningparm2/common:boolean/@value"/>
			</xsl:when>
			<xsl:otherwise>true</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>	

	<xsl:variable name="tuningparm3" select="document($avmxml)/avm:policy/avm:TuningSetting[@name='avm.se.ftp.pftpd.address_transparent']"/>
	<xsl:variable name="ftptrans">
		<xsl:choose>
			<xsl:when test="count($tuningparm3) > 0">
				<xsl:value-of select="$tuningparm3/common:boolean/@value"/>
			</xsl:when>
			<xsl:otherwise>true</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>	

	<xsl:variable name="tuningparm4" select="document($avmxml)/avm:policy/avm:TuningSetting[@name='avm.se.http.phttpd.address_transparent']"/>
	<xsl:variable name="httptrans">
		<xsl:choose>
			<xsl:when test="count($tuningparm4) > 0">
				<xsl:value-of select="$tuningparm4/common:boolean/@value"/>
			</xsl:when>
			<xsl:otherwise>true</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>	

	<xsl:variable name="trans" select="document($netxml)/net:policy/net:NetworkingConfig/net:ModeConfig/net:TransparentModeConfig"/>
	<xsl:variable name="transModeEnabled">
		<xsl:choose>
			<xsl:when test="count($trans)>0">true</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:template match="srv:SmtpConfig">
	<!--
		<xsl:if test="$smtpProxyEnabled='true' and string-length(@MyDomain)=0">
				<xsl:text>ERROR: In order to enable SMTP as a protected protocol you must specify a value for My Domain.&#10;</xsl:text>
		</xsl:if>
	--> 
		<xsl:variable name="relayControl">
			<xsl:choose>
				<xsl:when test="count(@RelayControl)>0"><xsl:value-of select="@RelayControl"/></xsl:when>
				<xsl:otherwise>false</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
	
		<xsl:if test="$smtpProxyEnabled='true' and  $smtptrans='false' and $relayControl='false' and $transModeEnabled='false' ">
				<xsl:text>ERROR: In order to enable SMTP in Non Transparent Mode as a protected protocol you must enable RelayControl and completely configure SMTP, which includes defining at least one RelayIP and at least one Local Domain.&#10;</xsl:text>
		</xsl:if>
		<xsl:if test="$smtpProxyEnabled='true' and $relayControl='true' ">
			<xsl:if test="count(srv:RelayIPs)=0 or count(srv:LocalDomains)=0">
				<xsl:text>ERROR: In order to enable SMTP as a protected protocol you must completely configure SMTP, which includes defining, at least one RelayIP and at least one Local Domain.&#10;</xsl:text>
			</xsl:if>
		</xsl:if>
		<!-- Verify that if in transMode that all avm transparency tuning parms are true-->
		<!--  These checks removed per CR.
		<xsl:if test="$transModeEnabled='true'">
			<xsl:if test="$smtptrans='false'">
				<xsl:text>ERROR: If the Network Mode is Transparent the value for avm.se.smtp.psmtpd.address_transparent must be true. Please correct this problem in the Advanced Tab of Antivirus settings page.&#10;</xsl:text>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$transModeEnabled='true'">
			<xsl:if test="$pop3trans='false'">
				<xsl:text>ERROR: If the Network Mode is Transparent the value for avm.se.pop3.ppop3d.address_transparent must be true. Please correct this problem in the Advanced Tab of Antivirus settings page.&#10;</xsl:text>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$transModeEnabled='true'">
			<xsl:if test="$ftptrans='false'">
				<xsl:text>ERROR: If the Network Mode is Transparent the value for avm.se.ftp.pftpd.address_transparent must be true. Please correct this problem in the Advanced Tab of Antivirus settings page.&#10;</xsl:text>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$transModeEnabled='true'">
			<xsl:if test="$httptrans='false'">
				<xsl:text>ERROR: If the Network Mode is Transparent the value for avm.se.http.phttpd.address_transparent must be true. Please correct this problem in the Advanced Tab of Antivirus settings page.&#10;</xsl:text>
			</xsl:if>
		</xsl:if>
		-->
	</xsl:template>
</xsl:stylesheet>
