<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:cfasdb="http://www.iss.net/cml/NetworkProtector/cfasdb" 
xmlns:common="http://www.iss.net/cml/NetworkProtector/common" 
xmlns:spam="http://www.iss.net/cml/NetworkProtector/spam"
xmlns:cflts="http://www.iss.net/cml/NetworkProtector/cflts">
	<xsl:output method="text" encoding="UTF-8" indent="no"/>
	<xsl:strip-space elements="*"/>
	<xsl:param name="spamxml" select=" 'npspam1_0.xml' "/>
	<xsl:param name="cfltsxml" select=" 'npcflts1_0.xml' "/>
	<xsl:param name="localdbinstalled" select=" 'false' "/>
	<xsl:variable name="spamEnabled">
		<xsl:value-of select="document($spamxml)/spam:policy/spam:Config/@aspmEnabled"/>
	</xsl:variable>
	<xsl:variable name="cfltsEnabled">
		<xsl:value-of select="document($cfltsxml)/cflts:policy/cflts:Config/@cfltEnabled"/>
	</xsl:variable>
	<xsl:variable name="tuningparm" select="cfasdb:policy/cfasdb:TuningSetting[@name='remote_db'] "/>
	<xsl:variable name="remoteDbEnabled">
		<xsl:choose>
			<xsl:when test="count($tuningparm) > 0">
				<xsl:value-of select="$tuningparm/common:boolean/@value"/>
			</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:template match="cfasdb:policy">
		<!-- Verify that if spam or cfltsm is enabled that either a local or remote db is installed -->
		<xsl:if test="$spamEnabled='true' or $cfltsEnabled='true' ">
			<xsl:if test=" $localdbinstalled='false' and $remoteDbEnabled='false' ">
				<xsl:text>ERROR: A Filter DB is required when Antispam or Web Filter is enabled. You can not enable Antispam or Web Filter without a Filter DB and you can not disable the Filter DB if Antispam or Web Filter is enabled. &#10;</xsl:text>
			</xsl:if>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
