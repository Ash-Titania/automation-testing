<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:fwm="http://www.iss.net/cml/NetworkProtector/fwm" 
xmlns:dynaddr="http://www.iss.net/cml/NetworkProtector/dynaddr" 
xmlns:netobj="http://www.iss.net/cml/NetworkObjects">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<!-- Include the resolve common templates that handle the common elements first always because it contains a couple global variables. -->
	<xsl:include href="resolvecommon100.xsl" />
	<!-- Main Processing Template -->
	<xsl:template match="fwm:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<!-- 
		Templates for the parents of all elements containing a reference are handled here. .
		The parents need to apply templates twice, one using mode=resolve and one using the default mode.
		The first apply using mode=resolve populates attributes in the element with the resolved values.
		The second apply templates copies the actual child elements and their attributes.
		The name of the element must match the name of the attribute that is an attribute of the parent element 
        that contains the reference element.
	-->
	<xsl:template match="fwm:RadiusConfig">
		<xsl:element name="RadiusConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:if test="@Enabled='true' ">
				<xsl:apply-templates mode="resolve"/>
			</xsl:if>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:BackupServer">
		<xsl:element name="BackupServer" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:if test="@UseBackupServer='true' ">
				<xsl:apply-templates mode="resolve"/>
			</xsl:if>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>	
	<xsl:template match="fwm:BackupServer" mode="resolve">
	</xsl:template>	
	<xsl:template match="fwm:IkeConfig">
		<xsl:element name="IkeConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates mode="resolve"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:TemplateProposal">
		<xsl:element name="TemplateProposal" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates mode="resolve"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:TemplateProposal" mode="resolve"/>
	<xsl:template match="fwm:L2TPSecGwGeneralConfig">
		<xsl:element name="L2TPSecGwGeneralConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates mode="resolve"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:ManSecGwGeneralConfig">
		<xsl:element name="ManSecGwGeneralConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates mode="resolve"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:VpnAddrIkeSecurityGatewayList/fwm:IpsecConfig">
		<xsl:element name="IpsecConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates mode="resolve"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:L2TPIPPool">
		<xsl:element name="L2TPIPPool" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates mode="resolve"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:L2TPIKEAttr">
		<xsl:element name="L2TPIKEAttr" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates mode="resolve"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>		
	<!-- 
		Any element that contains an element that needs resolving is handled here.
		These templates only handle mode=resolve.  This mode avoids putting the element tag out here so that the 
		Templates that do the resolving below simply output the attributes to the current parent being processed.
	-->
	<xsl:template match="fwm:RadiusConfig/fwm:PrimaryServerIPAddress" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="fwm:BackupServer/fwm:BackupServerIPAddress" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="fwm:LocalIpAddress" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="fwm:RemoteIpAddress" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="fwm:IpsecConfig/fwm:TemplateProposal/fwm:SourceAddress" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="fwm:L2TPSecGwGeneralConfig/fwm:L2TPEndPoint" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="fwm:ManSecGwGeneralConfig/fwm:LocalSecurityGateway" mode="resolve">
	</xsl:template>
	<xsl:template match="fwm:ManSecGwGeneralConfig/fwm:PeerSecurityGateway" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="fwm:IpsecConfig/fwm:IPRange1" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="fwm:IpsecConfig/fwm:IPRange2" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="fwm:IpsecConfig/fwm:IPRange3" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="fwm:IpsecConfig/fwm:WINS1" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="fwm:IpsecConfig/fwm:WINS2" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="fwm:IpsecConfig/fwm:DNS1" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="fwm:IpsecConfig/fwm:DNS2" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="fwm:L2TPIPPool/fwm:IPRange" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<!-- Templates for elements that require special handling -->
	<!-- Template for handling DaLocalId elements -->
	<xsl:template match="fwm:DaLocalId" mode="resolve">
		<xsl:choose>
			<xsl:when test="name(*[1]) ='ADDRNAMERef' or name(*[1]) ='DYNADDRRef' ">
				<xsl:attribute name="LocalIdType">IPADDRESS</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="LocalIdType"><xsl:value-of select="name(*[1])"/></xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<!-- Template for handling remoteids that are in a list. -->
	<xsl:template match="fwm:RemoteId">
		<xsl:element name="RemoteId" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates mode="resolve"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>	
	<xsl:template match="fwm:RemoteId" mode="resolve">
	</xsl:template>
	<!-- Template for Handling DaRemoteIds that are in a remote id list. -->
	<xsl:template match="fwm:DaRemoteId" mode="resolve">
			<xsl:choose>
			<xsl:when test="name(*[1]) ='ADDRNAMERef' or name(*[1]) ='DYNADDRRef' ">
				<xsl:attribute name="RemoteIdType">IPADDRESS</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="RemoteIdType"><xsl:value-of select="name(*[1])"/></xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="fwm:DynamicAddressNameRef" mode="resolve">
		<xsl:variable name="dynaddr" select="$dynaddrs[@Name=current()/@Name]"/>
		<xsl:attribute name="{concat(substring-after(name(..),'Da'),'Data')}"><xsl:value-of select="$dynaddr/dynaddr:SingleAddress/@IPSingleAddress"/></xsl:attribute>
	</xsl:template>

	<!-- Templates for handling VPN IDs and IDData -->
	<xsl:template match="fwm:FQDN" mode="resolve">
		<xsl:attribute name="{concat(substring-after(name(..),'Da'),'Data')}"><xsl:value-of select="@value"/></xsl:attribute>
	</xsl:template>
	<xsl:template match="fwm:USERFQDN" mode="resolve">
		<xsl:attribute name="{concat(substring-after(name(..),'Da'),'Data')}"><xsl:value-of select="@value"/></xsl:attribute>
	</xsl:template>
	<xsl:template match="fwm:DERASN1DN" mode="resolve">
		<xsl:attribute name="{concat(substring-after(name(..),'Da'),'Data')}"><xsl:value-of select="@value"/></xsl:attribute>
	</xsl:template>
	<xsl:template match="fwm:IPADDRESS" mode="resolve">
		<xsl:attribute name="{concat(substring-after(name(..),'Da'),'Data')}"><xsl:value-of select="@IPAddress"/></xsl:attribute>
	</xsl:template>
	<xsl:template match="fwm:ADDRNAMERef" mode="resolve">
		<xsl:variable name="addrname" select="$addrnames[@Name=current()/@Name]"/>
		<xsl:attribute name="{concat(substring-after(name(..),'Da'),'Data')}"><xsl:value-of select="$addrname/netobj:SingleAddress/@IPSingleAddress"/></xsl:attribute>
	</xsl:template>
	<xsl:template match="fwm:DYNADDRRef" mode="resolve">
		<xsl:variable name="dynaddr" select="$dynaddrs[@Name=current()/@Name]"/>
		<xsl:attribute name="{concat(substring-after(name(..),'Da'),'Data')}"><xsl:value-of select="$dynaddr/dynaddr:SingleAddress/@IPSingleAddress"/></xsl:attribute>
	</xsl:template>

	<!-- Templates for Handling Special Trans: stuff -->
	<xsl:template match="fwm:DynamicAddressLocalGatewayRef">
		<xsl:element name="StaticLocalGateway" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:variable name="dynaddr" select="$dynaddrs[@Name=current()/@Name]"/>
			<xsl:attribute name="LocalSecurityGateway"><xsl:value-of select="$dynaddr/dynaddr:SingleAddress/@IPSingleAddress"/></xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:AddressNameLocalGatewayRef">
		<xsl:element name="StaticLocalGateway" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:variable name="addrname" select="$addrnames[@Name=current()/@Name]"/>
			<xsl:attribute name="LocalSecurityGateway"><xsl:value-of select="$addrname/netobj:SingleAddress/@IPSingleAddress"/></xsl:attribute>
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="fwm:DynamicAddressNameRef">
		<xsl:element name="AddressRange" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:variable name="dynaddr" select="$dynaddrs[@Name=current()/@Name]"/>
			<xsl:attribute name="IPRangeAddress"><xsl:value-of select="$dynaddr/dynaddr:AddressRange/@IPRangeAddress"/></xsl:attribute>
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="fwm:DynamicAddressWANRef">
		<xsl:element name="StaticWAN" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:variable name="dynaddr" select="$dynaddrs[@Name=current()/@Name]"/>
			<xsl:attribute name="StaticIPAddress"><xsl:value-of select="$dynaddr/dynaddr:SingleAddress/@IPSingleAddress"/></xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:AddressNameWANRef">
		<xsl:element name="StaticWAN" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:variable name="addrname" select="$addrnames[@Name=current()/@Name]"/>
			<xsl:attribute name="StaticIPAddress"><xsl:value-of select="$addrname/netobj:SingleAddress/@IPSingleAddress"/></xsl:attribute>
		</xsl:element>
	</xsl:template>

</xsl:stylesheet>
