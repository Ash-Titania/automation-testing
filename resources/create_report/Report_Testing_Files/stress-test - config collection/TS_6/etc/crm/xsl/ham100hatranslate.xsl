<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:network="http://www.iss.net/cml/NetworkProtector/network"
	xmlns:ham="http://www.iss.net/cml/NetworkProtector/ham" >
	<!-- This style sheet replaces the value of each ipAddress in each remoteNodeInterface element of the ham xml file with the value of the ipAddress for the corresponding interface in the network xml file. -->
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="networkxml" select=" 'npnetwork1_0.xml' "/>
	<xsl:variable name="externalInterface" select="document($networkxml)/network:policy/network:RoutingModeConfig/network:ExternalConfig/network:IPAddress/network:Static"/>
	<xsl:variable name="internalInterfaces" select="document($networkxml)/network:policy/network:RoutingModeConfig/network:InternalConfig/network:InternalInterface"/>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ham:remoteNodeInterface">
		<xsl:element name="remoteNodeInterface" namespace="http://www.iss.net/cml/NetworkProtector/ham">
			<xsl:copy-of select="@*[name() != 'ipAddress' ]"/>
			<xsl:attribute name="ipAddress">
				<xsl:choose>
					<xsl:when test="current()/@interfaceName = 'eth1' ">
						<xsl:choose>
							<xsl:when test="count($externalInterface)>0"><xsl:value-of select="$externalInterface/@IpAddr"/></xsl:when>
							<xsl:otherwise><xsl:value-of select="current()/@ipAddress"/></xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="ipaddr" select="$internalInterfaces[@InterfaceName = current()/@interfaceName]"/>
						<xsl:choose>
							<xsl:when test="count($ipaddr)>0"><xsl:value-of select="$ipaddr/@IPAddress"/></xsl:when>
							<xsl:otherwise><xsl:value-of select="current()/@ipAddress"/></xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="ham:baseConfig">
		<xsl:element name="baseConfig" namespace="http://www.iss.net/cml/NetworkProtector/ham">
			<xsl:copy-of select="@*[name() != 'defaultPrimaryNode' ]"/>
				<xsl:choose>
					<xsl:when test="@defaultPrimaryNode = 'true'">
						<xsl:attribute name="defaultPrimaryNode">false</xsl:attribute>
					</xsl:when>
					<xsl:when test="@defaultPrimaryNode = 'false'">
						<xsl:attribute name="defaultPrimaryNode">true</xsl:attribute>
					</xsl:when>
				</xsl:choose>
			<xsl:apply-templates/>	
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
