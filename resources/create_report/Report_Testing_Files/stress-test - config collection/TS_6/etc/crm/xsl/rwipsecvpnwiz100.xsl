<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fwm="http://www.iss.net/cml/NetworkProtector/fwm" xmlns:wiz="http://www.iss.net/cml/NetworkProtector/vpnwizrwipsec" xmlns:npcommon="http://www.iss.net/cml/NetworkProtector/common" >
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="wizxml" select=" 'vpnwizrwipsec1_0.xml' "/>
	<xsl:variable name="vpnwiz" select="document($wizxml)/wiz:policy"/>
	<xsl:variable name="ruleidinc">
		<xsl:choose>
			<xsl:when test="count($vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:VPNAddressPool/@IPRange3) > 0">5</xsl:when>
			<xsl:when test="count($vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:VPNAddressPool/@IPRange2) > 0">3</xsl:when>
			<xsl:otherwise>1</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:template match="fwm:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:call-template name="insertaccessrules"/>
			<xsl:apply-templates select="fwm:Policy"/>
			<xsl:apply-templates select="fwm:IPSECRules"/>
			<xsl:apply-templates select="fwm:CommonLists"/>
			<xsl:apply-templates select="fwm:SecurityGateways"/>
			<xsl:apply-templates select="fwm:MessageConfig"/>
			<xsl:apply-templates select="fwm:ProxyRedirectRule"/>
			<xsl:apply-templates select="fwm:VpnAdvancedSettings"/>
			<xsl:apply-templates select="fwm:TuningSetting"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:if test="string-length(name(.)) > 0">
		  <xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates select="current()/*" />
		  </xsl:element>
		</xsl:if>
	 </xsl:template>
	<xsl:template match="fwm:Policy">
		<!-- Enable the ISAKAMP Self policy if it is not enabled already -->
		<xsl:element name="Policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="RuleId"><xsl:value-of select="position()+$ruleidinc"/></xsl:attribute>
			<xsl:attribute name="GUID">
				<xsl:choose>
					<xsl:when test="string-length(@GUID) > 0"><xsl:value-of select="@GUID"/></xsl:when>
					<xsl:otherwise><xsl:value-of select="generate-id()"/></xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="fwm:DestPort/fwm:Ports/fwm:PortEntry/fwm:PortName/@value = 'ISAKMP_UDP' ">
					<xsl:attribute name="Enabled">true</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="Enabled"><xsl:value-of select="@Enabled"/></xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:copy-of select="@*[name() != 'Enabled'  and name() != 'RuleId' and name() != 'GUID']"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:SecurityGateways">
		<xsl:element name="SecurityGateways" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:apply-templates select="fwm:AutoIkeSecurityGatewayList"/>
			<xsl:call-template name="insertsecuritygw"/>
			<xsl:apply-templates select="fwm:VpnAddrIkeSecurityGatewayList"/>
			<xsl:apply-templates select="fwm:L2TPIPSECSecGwType"/>
			<xsl:apply-templates select="fwm:ManualSecurityGatewayList"/>
		</xsl:element>
	</xsl:template>
	<xsl:template name="insertaccessrules">
		<!-- Insert necessary access rules based on values in the vpn wizard policy -->
		<xsl:call-template name="insertaccessrulesforiprange">
			<xsl:with-param name="addrrange" select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:VPNAddressPool/@IPRange1"/>
			<xsl:with-param name="firstid" select="'0'"/>
			<xsl:with-param name="guid1" select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:VPNAddressPool/@GUIDRange1a"/>
			<xsl:with-param name="guid2" select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:VPNAddressPool/@GUIDRange1b"/>
		</xsl:call-template>
		<xsl:if test="count($vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:VPNAddressPool/@IPRange2) > 0">
			<xsl:call-template name="insertaccessrulesforiprange">
				<xsl:with-param name="addrrange" select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:VPNAddressPool/@IPRange2"/>
				<xsl:with-param name="firstid" select="'2'"/>
				<xsl:with-param name="guid1" select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:VPNAddressPool/@GUIDRange2a"/>
				<xsl:with-param name="guid2" select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:VPNAddressPool/@GUIDRange2b"/>
			</xsl:call-template>
		</xsl:if>
		<xsl:if test="count($vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:VPNAddressPool/@IPRange3) > 0">
			<xsl:call-template name="insertaccessrulesforiprange">
				<xsl:with-param name="addrrange" select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:VPNAddressPool/@IPRange3"/>
				<xsl:with-param name="firstid" select="'4'"/>
				<xsl:with-param name="guid1" select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:VPNAddressPool/@GUIDRange3a"/>
				<xsl:with-param name="guid2" select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:VPNAddressPool/@GUIDRange3b"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<xsl:template name="insertaccessrulesforiprange">
		<xsl:param name="addrrange" />
		<xsl:param name="firstid"/>
		<xsl:param name="guid1"/>
		<xsl:param name="guid2"/>
		<!--Insert Inbound poilcy -->
		<xsl:element name="Policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="RuleId"><xsl:value-of select="$firstid"/></xsl:attribute>
			<xsl:attribute name="GUID"><xsl:value-of select="$guid1"/></xsl:attribute>
			<xsl:attribute name="Enabled">true</xsl:attribute>
			<xsl:attribute name="Action">Allow</xsl:attribute>
			<xsl:attribute name="LogEnabled"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:GeneralConfig/@LogEnabled"/></xsl:attribute>
			<xsl:attribute name="Comment"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:GeneralConfig/@Comment"/></xsl:attribute>
			<xsl:element name="Protocol" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyProto" namespace="http://www.iss.net/cml/NetworkProtector/fwm" />
			</xsl:element>
			<xsl:element name="SourceAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AddressRange" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="IPRangeAddress"><xsl:value-of select="$addrrange"/></xsl:attribute>
				</xsl:element>
			</xsl:element>
			<xsl:element name="SourcePort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
			<xsl:element name="DestAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AddressMask" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="IPAddressMask" ><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:LocalConfig/@IPAddressMask"/></xsl:attribute>
				</xsl:element>
			</xsl:element>		
			<xsl:element name="DestPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
		</xsl:element>
		<!-- Insert Outbound policy -->
		<xsl:element name="Policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:attribute name="RuleId"><xsl:value-of select="$firstid+1"/></xsl:attribute>
			<xsl:attribute name="GUID"><xsl:value-of select="$guid2"/></xsl:attribute>
			<xsl:attribute name="Enabled">true</xsl:attribute>
			<xsl:attribute name="Action">Allow</xsl:attribute>
			<xsl:attribute name="LogEnabled"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:GeneralConfig/@LogEnabled"/></xsl:attribute>
			<xsl:attribute name="Comment"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:GeneralConfig/@Comment"/></xsl:attribute>
			<xsl:element name="Protocol" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyProto" namespace="http://www.iss.net/cml/NetworkProtector/fwm" />
			</xsl:element>
			<xsl:element name="SourceAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AddressMask" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="IPAddressMask">
						<xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:LocalConfig/@IPAddressMask"/>
					</xsl:attribute>
				</xsl:element>
			</xsl:element>		
			<xsl:element name="SourcePort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
			<xsl:element name="DestAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AddressRange" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="IPRangeAddress" >
						<xsl:value-of select="$addrrange"/>
					</xsl:attribute>
				</xsl:element>
			</xsl:element>
			<xsl:element name="DestPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:element name="AnyPort" namespace="http://www.iss.net/cml/NetworkProtector/fwm"/>
			</xsl:element>
		</xsl:element>			
	</xsl:template>
	<xsl:template name="insertsecuritygw">
		<!-- Insert security gateway based on values in the vpn wizard policy if new gateway selected. -->
		<xsl:if test="count($vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw)>0">
			<xsl:element name="VpnAddrIkeSecurityGatewayList" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:attribute name="Name"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:name/@name"/></xsl:attribute>
				<xsl:attribute name="Comment"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:GeneralConfig/@Comment"/></xsl:attribute>
				<xsl:attribute name="GUID"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw//@GUID"/></xsl:attribute>
				<xsl:element name="IkeConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="Direction">RESPONDERONLY</xsl:attribute>
					<xsl:attribute name="ExchangeType">AGGRESSIVEMODE</xsl:attribute>
					<xsl:attribute name="LocalIdType">IPADDRESS</xsl:attribute>
					<xsl:attribute name="EncryptionAlg">3DES</xsl:attribute>
					<xsl:attribute name="EncAuthAlg">SHA1</xsl:attribute>
					<xsl:attribute name="LifeTimeSecs">7200</xsl:attribute>
					<xsl:attribute name="LifeTimeKBs">0</xsl:attribute>
					<xsl:attribute name="DHGroup">MODP_1024</xsl:attribute>
					<xsl:attribute name="LocalIdData"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:LocalID/@LocalIpAddress"/></xsl:attribute>
					<xsl:attribute name="LocalIpAddress"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:LocalID/@LocalIpAddress"/></xsl:attribute>
					<xsl:attribute name="RemoteIpAddress">0.0.0.0</xsl:attribute>
					<xsl:attribute name="AuthMode"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:AuthType/@AuthMode"/></xsl:attribute>
					<xsl:if test="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:AuthType/@AuthMode='Pre-SharedKey' ">
						<xsl:attribute name="PreSharedKey"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:AuthType/@PreSharedKey"/></xsl:attribute>
					</xsl:if>
					<xsl:element name="RemoteId" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:attribute name="GUID"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:RemoteID/@GUID"/></xsl:attribute>
						<xsl:attribute name="RemoteIdType">FQDN</xsl:attribute>
						<xsl:attribute name="RemoteIdData"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:RemoteID/@RemoteIdData"/></xsl:attribute>
					</xsl:element>
					<xsl:choose>
						<xsl:when test="$vpnwiz/wiz:GeneralSettings/wiz:UserAuth/@UserAuth='false'">
							<xsl:element name="IKEXAuth" namespace="http://www.iss.net/cml/NetworkProtector/fwm"><xsl:attribute name="Enabled">false</xsl:attribute></xsl:element>
						</xsl:when>
						<xsl:otherwise>
							<xsl:element name="IKEXAuth" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
								<xsl:attribute name="Enabled">true</xsl:attribute>
								<xsl:attribute name="DeviceType">EDGEDEVICE</xsl:attribute>
								<xsl:attribute name="XAuthAuthenticationType">Generic</xsl:attribute>
								</xsl:element>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:element name="OCSP" namespace="http://www.iss.net/cml/NetworkProtector/fwm"><xsl:attribute name="Enabled">false</xsl:attribute></xsl:element>
				</xsl:element>
				<xsl:element name="IpsecConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="IPRange1"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:VPNAddressPool/@IPRange1"/></xsl:attribute>
					<xsl:if test="count($vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:VPNAddressPool/@IPRange2) > 0">
							<xsl:attribute name="IPRange2"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:VPNAddressPool/@IPRange2"/></xsl:attribute>
					</xsl:if>
					<xsl:if test="count($vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:VPNAddressPool/@IPRange3) > 0">
							<xsl:attribute name="IPRange3"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw/wiz:VPNAddressPool/@IPRange3"/></xsl:attribute>
					</xsl:if>
					<xsl:element name="TemplateProposal" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:attribute name="GUID"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:SecGw/wiz:NewSecGw//@GUID"/></xsl:attribute>
						<xsl:attribute name="SourceAddress"><xsl:value-of select="$vpnwiz/wiz:GeneralSettings/wiz:LocalConfig/@IPAddressMask"/></xsl:attribute>
						<xsl:attribute name="GroupID">Group2</xsl:attribute>
						<xsl:attribute name="SecurityProtocol">ESPWithAuth</xsl:attribute>
						<xsl:attribute name="AuthAlgorithm">SHA1</xsl:attribute>
						<xsl:attribute name="EncAlgorithm">3DES</xsl:attribute>
						<xsl:attribute name="LifeTimeSecs">7200</xsl:attribute>
						<xsl:attribute name="LifeTimeKBs">0</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<xsl:template match="npcommon:*">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/common">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*" />
			</xsl:element>
		</xsl:if>
	</xsl:template>	
</xsl:stylesheet>
