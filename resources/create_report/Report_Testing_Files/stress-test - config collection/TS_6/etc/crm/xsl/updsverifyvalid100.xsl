<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:upds="http://www.iss.net/cml/NetworkProtector/upds" xmlns:ham="http://www.iss.net/cml/NetworkProtector/ham" >
	<xsl:output method="text" encoding="UTF-8" indent="no"/>
	<xsl:strip-space elements="*"/>
	<!-- In order for this script to function properly dynaddr and netobj references need to be resolved prior to running this script currently. 
		  Should I rework this script to work off of the non-resolved variant? -->
	<xsl:include href="verifycommon100.xsl"/>
	<xsl:param name="hamxml" select=" 'npham1_0.xml' "/>
	<xsl:variable name="policyName" select="'UpdateSettings'"/>
	<xsl:variable name="hamenabled" select="document($hamxml)/ham:policy/ham:baseConfig/@enabled"/>
	<xsl:template match="upds:EnableAutoInstall | upds:EnableOneTimeInstall">
		<!-- Verify that if Auto Firmware Install or scheduled firmware installed is enabled that HA is not.-->
		<xsl:if test="$hamenabled='true' ">
			<xsl:text>ERROR: Automatic or scheduled firmware update installations cannot be enabled while the appliance is in High Availability mode. When High Availability mode is enabled firmware updates can only be manually installed.  Please see the online Help or User Guide.&#10;</xsl:text>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
