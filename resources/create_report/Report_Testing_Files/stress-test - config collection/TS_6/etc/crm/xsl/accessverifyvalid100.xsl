<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:access="http://www.iss.net/cml/NetworkProtector/access">
	<xsl:output method="text" encoding="UTF-8" indent="no"/>
	<xsl:strip-space elements="*"/>
	<xsl:template match="access:root">
		<xsl:if test="string-length(@old)=0 and string-length(@new)>0">
			<xsl:text>ERROR: You must enter a Current Root Password value. The password was not changed..&#10;</xsl:text>
		</xsl:if>
		<xsl:if test="string-length(@old)>0 and string-length(@new)=0">
			<xsl:text>ERROR: You must enter a New Root Password value. The password was not changed..&#10;</xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template match="access:admin">
		<xsl:if test="string-length(@old)=0 and string-length(@new)>0">
			<xsl:text>ERROR: You must enter a Current Admin Password value. The password was not changed..&#10;</xsl:text>
		</xsl:if>
		<xsl:if test="string-length(@old)>0 and string-length(@new)=0">
			<xsl:text>ERROR: You must enter a New Admin Password value. The password was not changed..&#10;</xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template match="access:webAdmin">
		<xsl:if test="string-length(@old)=0 and string-length(@new)>0">
			<xsl:text>ERROR: You must enter a Current Proventia Manager Password value. The password was not changed..&#10;</xsl:text>
		</xsl:if>
		<xsl:if test="string-length(@old)>0 and string-length(@new)=0">
			<xsl:text>ERROR: You must enter a New Proventia Manager Password value. The password was not changed..&#10;</xsl:text>
		</xsl:if>
	</xsl:template>
	
</xsl:stylesheet>
