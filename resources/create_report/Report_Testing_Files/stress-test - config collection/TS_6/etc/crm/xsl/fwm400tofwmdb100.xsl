<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:fwm="http://www.iss.net/cml/NetworkProtector/fwm" 
xmlns:tfwm="http://www.iss.net/cml/NetworkProtector/tfwm" 
xmlns:avm="http://www.iss.net/cml/NetworkProtector/avm" 
xmlns:cflts="http://www.iss.net/cml/NetworkProtector/cflts" 
xmlns:spam="http://www.iss.net/cml/NetworkProtector/spam" 
xmlns:network="http://www.iss.net/cml/NetworkProtector/network" 
xmlns:common="http://www.iss.net/cml/NetworkProtector/common" 
xmlns:intip="http://www.iss.net/cml/NetworkProtector/intip" 
xmlns:netobj="http://www.iss.net/cml/NetworkObjects" 
xmlns:dynaddr="http://www.iss.net/cml/NetworkProtector/dynaddr"
xmlns:sysmodel="http://www.iss.net/cml/NetworkProtector/sysmodeltuning"
xmlns:admin="http://www.iss.net/cml/NetworkProtector/admin">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="avmxml" select=" 'npavm1_0.xml' "/>
	<xsl:param name="cfltsxml" select=" 'npcflts1_0.xml' "/>
	<xsl:param name="spamxml" select=" 'npspam1_0.xml' "/>
	<xsl:param name="netxml" select=" '' "/>
	<xsl:param name="pppInt" select=" 'ppp0' "/>
	<xsl:param name="hamode" select=" 'none' "/>
	<xsl:param name="intipxml" select=" 'npintip1_0.xml' "/>
	<xsl:param name="netobjxml" select="'networkobjects1_0.xml' "/>
	<xsl:param name="dynaddrxml" select="'npdynaddr1_0.xml' "/>
	<xsl:param name="tunexml" select="''"/>
	<xsl:param name="adminxml" select="'npadmin1_0.xml' "/>
	<xsl:param name="manageraddresslistname" select=" 'manageraddresslist_sys' "/>
	<xsl:variable name="dynaddrs" select="document($dynaddrxml)/dynaddr:policy/dynaddr:DynamicAddressList"/>
	<xsl:variable name="addrnames" select="document($netobjxml)/netobj:policy/netobj:AddressList"/>
	<xsl:variable name="managerpolicy" select="document($adminxml)/admin:policy/admin:ManagerPolicy"/>
	<!-- Handle tfwm protocol and mac event notification settings for rule not found and allow message config.-->
	<!-- Protocol allow rule matched msgs -->
	<xsl:variable name="disableprotocolallowmsgs">
		<xsl:choose>
			<xsl:when test="count(tfwm:policy) = 0">false</xsl:when>
			<xsl:when test="tfwm:policy/tfwm:MessageConfig/tfwm:GeneralMessages/@ProtocolAllowRules='true' ">false</xsl:when>
			<xsl:otherwise>true</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="protocolallowtuningparm" select="tfwm:policy/tfwm:TuningSetting[@name='syslog.log_override.disable.2144'] "/>
	<xsl:variable name="insertprotocolallowtuningparm">
		<xsl:choose>
			<xsl:when test="count($protocolallowtuningparm) > 0  or $disableprotocolallowmsgs='false' ">false</xsl:when>
			<xsl:otherwise>true</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!-- Protocol Rule Not Found Msgs -->
	<xsl:variable name="disableprotocolunavailmsgs">
		<xsl:choose>
			<xsl:when test="count(tfwm:policy) = 0">false</xsl:when>
			<xsl:when test="tfwm:policy/tfwm:MessageConfig/tfwm:GeneralMessages/@ProtocolUnavailableRules='true' ">false</xsl:when>
			<xsl:otherwise>true</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="protocolunavailtuningparm" select="tfwm:policy/tfwm:TuningSetting[@name='syslog.log_override.disable.2143'] "/>
	<xsl:variable name="insertprotocolunavailtuningparm">
		<xsl:choose>
			<xsl:when test="count($protocolunavailtuningparm) > 0 or $disableprotocolunavailmsgs='false' ">false</xsl:when>
			<xsl:otherwise>true</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!-- MAC Unavail Msgs -->
	<xsl:variable name="disablemacunavailmsgs">
		<xsl:choose>
			<xsl:when test="count(tfwm:policy) = 0">false</xsl:when>
			<xsl:when test="tfwm:policy/tfwm:MessageConfig/tfwm:GeneralMessages/@MACUnavailableRules='true' ">false</xsl:when>
			<xsl:otherwise>true</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="macunavailsrctuningparm" select="tfwm:policy/tfwm:TuningSetting[@name='syslog.log_override.disable.2141'] "/>
	<xsl:variable name="macunavaildsttuningparm" select="tfwm:policy/tfwm:TuningSetting[@name='syslog.log_override.disable.2142'] "/>
	<xsl:variable name="insertmacunavailtuningparm">
		<xsl:choose>
			<xsl:when test="count($macunavailsrctuningparm) > 0 or count($macunavaildsttuningparm) or $disablemacunavailmsgs='false' ">false</xsl:when>
			<xsl:otherwise>true</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!-- Determine whether or not each proxy is enabled .-->
	<xsl:variable name="httpProxyEnabled">
		<xsl:choose>
			<xsl:when test="document($cfltsxml)/cflts:policy/cflts:Config/@cfltEnabled='true' ">true</xsl:when>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@httpEnabled='true' ">true</xsl:when>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@vpshttpEnabled='true' ">true</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="smtpProxyEnabled">
		<xsl:choose>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@smtpEnabled='true' ">true</xsl:when>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@vpssmtpEnabled='true' ">true</xsl:when>
			<xsl:when test="document($spamxml)/spam:policy/spam:Config/@aspmEnabled='true' and
document($spamxml)/spam:policy/spam:Config/spam:ProtectedProtocols/@smtpEnabled='true' ">true</xsl:when>
			<xsl:when test="document($spamxml)/spam:policy/spam:Config/@aspmEnabled='true' and
count(document($spamxml)/spam:policy/spam:Config/spam:ProtectedProtocols)=0 ">true</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="ftpProxyEnabled">
		<xsl:choose>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@ftpEnabled='true' ">true</xsl:when>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@vpsftpEnabled='true' ">true</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="pop3ProxyEnabled">
		<xsl:choose>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@pop3Enabled='true' ">true</xsl:when>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@vpspop3Enabled='true' ">true</xsl:when>
			<xsl:when test="document($spamxml)/spam:policy/spam:Config/@aspmEnabled='true' and
document($spamxml)/spam:policy/spam:Config/spam:ProtectedProtocols/@pop3Enabled='true' ">true</xsl:when>
			<xsl:when test="document($spamxml)/spam:policy/spam:Config/@aspmEnabled='true' and
count(document($spamxml)/spam:policy/spam:Config/spam:ProtectedProtocols)=0 ">true</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="externalInterfaceName">
		<xsl:choose>
			<xsl:when test="string-length($netxml) = 0">none</xsl:when>
			<xsl:when test="count(document($netxml)/network:policy/network:NetworkingConfig/network:ModeConfig/network:RoutingModeConfig/network:ExternalConfig/network:IPAddress/network:PPPoE) > 0">
				<xsl:value-of select="$pppInt"/>
			</xsl:when>
			<xsl:otherwise>eth1</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!-- Determine if we need to substitute the eth1ip address for any LocalIpAddress value in any Security Gateway -->
	<xsl:variable name="tuningparm" select="fwm:policy/fwm:TuningSetting[@name='override.localid.with.eth1'] "/>
	<xsl:variable name="overrideLocal">
		<xsl:choose>
			<xsl:when test="count($tuningparm) > 0">
				<xsl:value-of select="$tuningparm/common:boolean/@value"/>
			</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!-- Get the IP Address for eth1 out of the intip document -->
	<xsl:variable name="interfaces" select="document($intipxml)/intip:policy/intip:interface"/>
	<xsl:variable name="eth1Node" select="$interfaces[@name='eth1']"/>
	<xsl:variable name="eth1IP" select="$eth1Node/@ip"/>
	<xsl:variable name="fwmBeingProcessed">
		<xsl:choose>
			<xsl:when test="count(fwm:policy)>0">true</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:template match="fwm:policy|tfwm:policy">
		<xsl:element name="policy" namespace="{namespace-uri(.)}">
			<xsl:copy-of select="@*"/>
			<!-- Insert the rule to allow acess to the managers of the box before all other rules. -->
			<xsl:call-template name="insertManagerAccessRule"/>
			<xsl:apply-templates select="fwm:Policy|tfwm:Policy"/>
			<xsl:if test="$hamode!='secondary' ">
				<xsl:call-template name="createIKERules"/>
				<xsl:call-template name="createIPSECRules"/>
				<xsl:call-template name="createVPNAddressRules"/>
			</xsl:if>
			<xsl:apply-templates select="fwm:CommonLists"/>
			<xsl:apply-templates select="fwm:MessageConfig|tfwm:MessageConfig"/>
			<xsl:apply-templates select="fwm:ProxyRedirectRule|tfwm:ProxyRedirectRule"/>
			<xsl:apply-templates select="fwm:ProtectionBypassRule"/>
			<xsl:if test="$hamode!='secondary' ">
				<xsl:apply-templates select="fwm:VpnAdvancedSettings"/>
			</xsl:if>
			<xsl:if test="$tunexml != '' ">
				<xsl:apply-templates select="document($tunexml)/sysmodel:sysmodeltuning"/>
			</xsl:if>
			<xsl:apply-templates select="fwm:TuningSetting|tfwm:TuningSetting"/>
			<xsl:call-template name="insertTuningParms"/>
			<xsl:if test="$hamode!='secondary' ">
				<xsl:apply-templates select="fwm:SecurityGateways/fwm:L2TPIPSECSecGwList"/>
			</xsl:if>
			<!-- Handle nodes in the tfwm policy -->
			<xsl:apply-templates select="tfwm:L2AccessControl"/>
			<xsl:apply-templates select="tfwm:LocalRouter"/>
			<!-- Create the manager address list from the nodes in the admin policy -->
			<xsl:call-template name="insertManagerAddressList"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template name="createIPSECRules">
		<!-- Need to take each IPSEC Rule and merge in items from referenced security gateway to create old style IPSEC rules -->
		<xsl:for-each select="fwm:IPSECRules">
			<xsl:element name="IPSECRules" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:copy-of select="@*[name() != 'Enabled']"/>
				<xsl:choose>
					<xsl:when test="count(fwm:SecurityGateway/fwm:AutoIkeSecGw) > 0">
						<!-- Automatic Security Gateway -->
						<xsl:variable name="matchgw" select="/fwm:policy/fwm:SecurityGateways/fwm:AutoIkeSecurityGatewayList[@Name=current()/fwm:SecurityGateway/fwm:AutoIkeSecGw/@value]"/>
						<xsl:attribute name="Enabled">
							<xsl:choose>
								<xsl:when test="$matchgw/@Enabled='false' ">false</xsl:when>
								<xsl:otherwise><xsl:value-of select="current()/@Enabled"/></xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="EncapsulationMode"><xsl:value-of select="$matchgw/fwm:IpsecConfig/@EncapsulationMode"/></xsl:attribute>
						<xsl:apply-templates select="fwm:SourceAddress"/>
						<xsl:apply-templates select="fwm:SourcePort"/>
						<xsl:apply-templates select="fwm:DestAddress"/>
						<xsl:apply-templates select="fwm:DestPort"/>
						<xsl:element name="ManualAutoParms" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:element name="AutomaticIpsec" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
								<xsl:attribute name="PeerSecurityGateway"><xsl:value-of select="$matchgw/fwm:IkeConfig/@RemoteIpAddress"/></xsl:attribute>
								<xsl:apply-templates select="$matchgw/fwm:IpsecConfig"/>
							</xsl:element>
						</xsl:element>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="matchgw" select="/fwm:policy/fwm:SecurityGateways/fwm:ManualSecurityGatewayList[@Name=current()/fwm:SecurityGateway/fwm:ManualSecGw/@value]"/>
						<xsl:attribute name="Enabled">
							<xsl:choose>
								<xsl:when test="$matchgw/@Enabled='false' ">false</xsl:when>
								<xsl:otherwise><xsl:value-of select="current()/@Enabled"/></xsl:otherwise>
							</xsl:choose>
						</xsl:attribute>
						<xsl:attribute name="EncapsulationMode"><xsl:value-of select="$matchgw/fwm:ManSecGwGeneralConfig/@EncapsulationMode"/></xsl:attribute>
						<xsl:apply-templates select="fwm:SourceAddress"/>
						<xsl:apply-templates select="fwm:SourcePort"/>
						<xsl:apply-templates select="fwm:DestAddress"/>
						<xsl:apply-templates select="fwm:DestPort"/>
						<xsl:element name="ManualAutoParms" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:element name="ManualIpsec" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
								<xsl:attribute name="PeerSecurityGateway"><xsl:value-of select="$matchgw/fwm:ManSecGwGeneralConfig/@PeerSecurityGateway"/></xsl:attribute>
								<xsl:apply-templates select="$matchgw/*"/>
							</xsl:element>
						</xsl:element>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="createVPNAddressRules">
		<!-- Need to take each VpnAddr Security Gateway and create a VPN Address -->
		<xsl:for-each select="fwm:SecurityGateways/fwm:VpnAddrIkeSecurityGatewayList">
			<xsl:if test="@Enabled='true' or count(@Enabled)=0">
				<xsl:element name="AddressRanges" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="Name"><xsl:value-of select="@Name"/></xsl:attribute>
					<xsl:attribute name="GUID"><xsl:value-of select="@GUID"/></xsl:attribute>
					<xsl:apply-templates select="fwm:IpsecConfig"/>
					<xsl:element name="AssociatedPolicies" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:attribute name="GUID"><xsl:value-of select="@GUID"/></xsl:attribute>
						<xsl:attribute name="PolicyName"><xsl:value-of select="concat('va',@Name)"/></xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:if>			
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="createIKERules">
		<!-- Need to take each Enabled AutoIke Security Gateway and create an IKE rule for it. -->
		<!-- Need to take each Enabled VpnAddr Security Gateway and create an IKE Rule for it first then a VPN Address. -->
		<xsl:for-each select="fwm:SecurityGateways/fwm:AutoIkeSecurityGatewayList">
			<xsl:if test="@Enabled='true' or count(@Enabled)=0">
				<xsl:element name="IKERules" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="Name"><xsl:value-of select="concat('ai',@Name)"/></xsl:attribute>
					<xsl:attribute name="GUID"><xsl:value-of select="@GUID"/></xsl:attribute>
					<xsl:attribute name="Enabled">true</xsl:attribute>
					<xsl:attribute name="Comment"><xsl:value-of select="@Comment"/></xsl:attribute>
					<xsl:apply-templates select="fwm:IkeConfig"/>
				</xsl:element>
			</xsl:if>
		</xsl:for-each>
		<xsl:for-each select="fwm:SecurityGateways/fwm:VpnAddrIkeSecurityGatewayList">
			<xsl:if test="@Enabled='true' or count(@Enabled)=0 ">
				<xsl:element name="IKERules" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="Name"><xsl:value-of select="concat('va',@Name)"/></xsl:attribute>
					<xsl:attribute name="GUID"><xsl:value-of select="@GUID"/></xsl:attribute>
					<xsl:attribute name="Enabled">true</xsl:attribute>
					<xsl:attribute name="Comment"><xsl:value-of select="@Comment"/></xsl:attribute>
					<xsl:apply-templates select="fwm:IkeConfig"/>
				</xsl:element>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="fwm:IkeConfig">
		<xsl:copy-of select="@*[name() != 'LocalIdData' or name() != 'LocalIpAddress']"/>
		<xsl:if test="name(..) = 'AutoIkeSecurityGatewayList' ">
			<xsl:if test="count(@DPDEnabled)=0">
				<xsl:attribute name="DPDEnabled">true</xsl:attribute>
			</xsl:if>
			<xsl:if test="count(@UseOldDPDMode)=0">
				<xsl:attribute name="DPDEnabled">true</xsl:attribute>
			</xsl:if>
		</xsl:if>
		<xsl:choose>
			<xsl:when test="$overrideLocal='true' and @LocalIdData='0.0.0.0' ">
				<xsl:attribute name="LocalIdData"><xsl:value-of select="$eth1IP"/></xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="LocalIdData"><xsl:value-of select="@LocalIdData"/></xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="$overrideLocal='true' and @LocalIpAddress='0.0.0.0' ">
				<xsl:attribute name="LocalIpAddress"><xsl:value-of select="$eth1IP"/></xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="LocalIpAddress"><xsl:value-of select="@LocalIpAddress"/></xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="fwm:IpsecConfig">
		<xsl:copy-of select="@*[name() != 'EncapsulationMode']"/>
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="fwm:ManSecGwGeneralConfig">
		<xsl:apply-templates select="fwm:LocalSecurityGateway"/>
	</xsl:template>
	<xsl:template match="fwm:ProxyRedirectRule|tfwm:ProxyRedirectRule">
		<xsl:element name="ProxyRedirectRule" namespace="{namespace-uri(.)}">
			<xsl:copy-of select="@*[name()!= 'Enabled' ] "/>
			<!-- Set enabled based upon whether the proxy needs is enabled in avm, cflts, or spam -->
			<xsl:attribute name="Enabled"><xsl:choose><xsl:when test="@Proxy='HTTP' and $httpProxyEnabled='true' and @UserEnabled='true' ">true</xsl:when><xsl:when test="@Proxy='FTP' and $ftpProxyEnabled='true' and @UserEnabled='true'  ">true</xsl:when><xsl:when test="@Proxy='SMTP' and $smtpProxyEnabled='true' and @UserEnabled='true'  ">true</xsl:when><xsl:when test="@Proxy='POP3' and $pop3ProxyEnabled='true' and @UserEnabled='true'  ">true</xsl:when><xsl:otherwise>false</xsl:otherwise></xsl:choose></xsl:attribute>
			<xsl:attribute name="interface"><xsl:value-of select="$externalInterfaceName"/></xsl:attribute>			
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:DynamicLocalGateway">
		<xsl:element name="DynamicLocalGateway" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*[name() != 'DynamicInterface' ]"/>
			<xsl:attribute name="DynamicInterface"><xsl:choose><xsl:when test="$externalInterfaceName != 'none'"><xsl:value-of select="$externalInterfaceName"/></xsl:when><xsl:otherwise><xsl:value-of select="@DynamicInterface"/></xsl:otherwise></xsl:choose></xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:DynamicWAN">
		<xsl:element name="DynamicWAN" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*[name() != 'InterfaceName'  ]"/>
			<xsl:attribute name="InterfaceName"><xsl:choose><xsl:when test="$externalInterfaceName != 'none'"><xsl:value-of select="$externalInterfaceName"/></xsl:when><xsl:otherwise><xsl:value-of select="@InterfaceName"/></xsl:otherwise></xsl:choose></xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:SpecificInterfaceAddress">
		<xsl:element name="SpecificInterfaceAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*[name() != 'InterfaceName'  ]"/>
			<xsl:attribute name="InterfaceName"><xsl:choose><xsl:when test="$externalInterfaceName != 'none'"><xsl:value-of select="$externalInterfaceName"/></xsl:when><xsl:otherwise><xsl:value-of select="@InterfaceName"/></xsl:otherwise></xsl:choose></xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:L2TPIPSECSecGwList">
		<xsl:if test="@Enabled='true' or count(@Enabled)=0">
			<xsl:element name="L2TPIPSECSecGwList" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:copy-of select="@*"/>
				<!-- This is kludge since Xalan on Linux does not handle the template for fwm:L2TPSecGwGeneralConfig below properly -->
				<xsl:attribute name="L2TPDisableTunnelAuth"><xsl:value-of select="child::fwm:L2TPSecGwGeneralConfig/@L2TPDisableTunnelAuth"/></xsl:attribute>
				<xsl:attribute name="L2TPHostName"><xsl:value-of select="child::fwm:L2TPSecGwGeneralConfig/@L2TPHostName"/></xsl:attribute>
				<xsl:attribute name="L2TPEndPoint"><xsl:value-of select="child::fwm:L2TPSecGwGeneralConfig/@L2TPEndPoint"/></xsl:attribute>
				<xsl:apply-templates />
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<xsl:template match="fwm:L2TPIKEAttr">
		<xsl:element name="L2TPIKEAttr" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*[name() != 'LocalIdData' ]"/>
			<xsl:choose>
				<xsl:when test="$overrideLocal='true' and @LocalIdData='0.0.0.0' ">
					<xsl:attribute name="LocalIdData"><xsl:value-of select="$eth1IP"/></xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="LocalIdData"><xsl:value-of select="@LocalIdData"/></xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="$hamode='primary'">
				<xsl:attribute name="AliasIp"><xsl:value-of select="$eth1IP"/></xsl:attribute>
			</xsl:if>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:L2TPSecGwGeneralConfig">
	<!-- Work around Xalan problem of ignoring these attributes 
		<xsl:copy-of select="@*"/>
		<xsl:apply-templates />
	-->
	</xsl:template>
	<xsl:template match="fwm:TuningSetting|tfwm:TuningSetting">
		<xsl:if test="@name!='override.localid.with.eth1'">
			<xsl:element name="TuningSetting" namespace="{namespace-uri(.)}">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates/>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<xsl:template name="insertTuningParms">
		<xsl:if test="$insertprotocolallowtuningparm='true' ">
			<xsl:element name="TuningSetting" namespace="{namespace-uri(.)}">
				<xsl:attribute name="name">syslog.log_override.disable.2144</xsl:attribute>
				<xsl:element name="boolean" namespace="http://www.iss.net/cml/NetworkProtector/common">
					<xsl:attribute name="value">true</xsl:attribute>
				</xsl:element>
			</xsl:element>
		</xsl:if>
		<xsl:if test="$insertprotocolunavailtuningparm='true' ">
			<xsl:element name="TuningSetting" namespace="{namespace-uri(.)}">
				<xsl:attribute name="name">syslog.log_override.disable.2143</xsl:attribute>
				<xsl:element name="boolean" namespace="http://www.iss.net/cml/NetworkProtector/common">
					<xsl:attribute name="value">true</xsl:attribute>
				</xsl:element>
			</xsl:element>
		</xsl:if>
		<xsl:if test="$insertmacunavailtuningparm='true' ">
			<xsl:element name="TuningSetting" namespace="{namespace-uri(.)}">
				<xsl:attribute name="name">syslog.log_override.disable.2141</xsl:attribute>
				<xsl:element name="boolean" namespace="http://www.iss.net/cml/NetworkProtector/common">
					<xsl:attribute name="value">true</xsl:attribute>
				</xsl:element>
			</xsl:element>
			<xsl:element name="TuningSetting" namespace="{namespace-uri(.)}">
				<xsl:attribute name="name">syslog.log_override.disable.2142</xsl:attribute>
				<xsl:element name="boolean" namespace="http://www.iss.net/cml/NetworkProtector/common">
					<xsl:attribute name="value">true</xsl:attribute>
				</xsl:element>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<!-- Create the correct flavor of Radius Server Node. -->
	<!-- This is kludge since Xalan on Linux does not handle the template for fwm:BackupServer below properly -->
	<xsl:template match="fwm:RadiusConfig">
		<xsl:element name="RadiusConfig" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*" />
			<xsl:attribute name="UseBackupServer"><xsl:value-of select="child::fwm:BackupServer/@UseBackupServer"/></xsl:attribute>
			<xsl:attribute name="BackupServerIPAddress"><xsl:value-of select="child::fwm:BackupServer/@BackupServerIPAddress"/></xsl:attribute>
			<xsl:attribute name="BackupServerSubnetMask"><xsl:value-of select="child::fwm:BackupServer/@BackupServerSubnetMask"/></xsl:attribute>
			<xsl:attribute name="BackupSecret"><xsl:value-of select="child::fwm:BackupServer/@BackupSecret"/></xsl:attribute>
			<xsl:attribute name="BackupServerNASID"><xsl:value-of select="child::fwm:BackupServer/@BackupServerNASID"/></xsl:attribute>
			<xsl:attribute name="BackupServerAcctPort"><xsl:value-of select="child::fwm:BackupServer/@BackupServerAcctPort"/></xsl:attribute>
			<xsl:attribute name="BackupServerAuthPort"><xsl:value-of select="child::fwm:BackupServer/@BackupServerAuthPort"/></xsl:attribute>
			<!--- xsl:apply-templates/> -->
		</xsl:element>
	</xsl:template>
<!--	
	<xsl:template match="fwm:BackupServer">
		<xsl:copy-of select="@*"/>
	</xsl:template>
-->
	<!-- Templates that eat all the unneeded dynamic address/network object ref elements -->
	<xsl:template match="fwm:RadiusConfig/fwm:PrimaryServerIPAddress" >
	</xsl:template>
	<xsl:template match="fwm:BackupServer/fwm:BackupServerIPAddress" >
	</xsl:template>
	<xsl:template match="fwm:LocalIpAddress" >
	</xsl:template>
	<xsl:template match="fwm:RemoteIpAddress" >
	</xsl:template>
	<xsl:template match="fwm:IpsecConfig/fwm:TemplateProposal/fwm:SourceAddress">
	</xsl:template>
	<xsl:template match="fwm:L2TPSecGwGeneralConfig/fwm:L2TPEndPoint" >
	</xsl:template>
	<xsl:template match="fwm:ManSecGwGeneralConfig/fwm:PeerSecurityGateway" >
	</xsl:template>
	<xsl:template match="fwm:IpsecConfig/fwm:IPRange1" >
	</xsl:template>
	<xsl:template match="fwm:IpsecConfig/fwm:IPRange2" >
	</xsl:template>
	<xsl:template match="fwm:IpsecConfig/fwm:IPRange3" >
	</xsl:template>
	<xsl:template match="fwm:IpsecConfig/fwm:WINS1" >
	</xsl:template>
	<xsl:template match="fwm:IpsecConfig/fwm:WINS2" >
	</xsl:template>
	<xsl:template match="fwm:IpsecConfig/fwm:DNS1" >
	</xsl:template>
	<xsl:template match="fwm:IpsecConfig/fwm:DNS1" >
	</xsl:template>
	<xsl:template match="fwm:L2TPIPPool/fwm:IPRange" >
	</xsl:template>	
	<xsl:template match="fwm:DaLocalId" >
	</xsl:template>	
	<xsl:template match="fwm:DaRemoteId" >
	</xsl:template>	
	<!-- Template the inserts the sysmodel specific tuning parms -->
	<xsl:template match="sysmodel:sysmodeltuning">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="sysmodel:*">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:choose>
				<!-- Really being picky here when it comes to getting namespaces right. -->
				<xsl:when test="$fwmBeingProcessed='true'">
					<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
						<xsl:copy-of select="@*"/>
						<xsl:apply-templates select="current()/*" />
					</xsl:element>
				</xsl:when>
				<xsl:otherwise>
					<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkProtector/tfwm">
						<xsl:copy-of select="@*"/>
						<xsl:apply-templates select="current()/*" />
					</xsl:element>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<!-- Really being picky here when it comes to getting namespaces right. -->
	<xsl:template match="sysmodel:number">
		<xsl:element name="number" namespace="http://www.iss.net/cml/NetworkProtector/common">
			<xsl:copy-of select="@*"/>
		</xsl:element>
	</xsl:template>
	<!--Handle the nodes in the admin policy -->
	<xsl:template name="insertManagerAccessRule">
		<xsl:if test="count($managerpolicy/admin:Manager)>0">
			<xsl:element name="Policy" namespace="{namespace-uri(.)}">
				<xsl:attribute name="RuleId">9999</xsl:attribute>
				<xsl:attribute name="GUID"><xsl:value-of select="$managerpolicy/@GUID"/></xsl:attribute>
				<xsl:attribute name="Enabled">true</xsl:attribute>
				<xsl:attribute name="Action">Allow</xsl:attribute>
				<xsl:attribute name="LogEnabled"><xsl:value-of select="$managerpolicy/@LogEnabled"/></xsl:attribute>
				<xsl:attribute name="Comment">System Generated Manager Rule</xsl:attribute>
				<xsl:element name="Protocol" namespace="{namespace-uri(.)}">
					<xsl:element name="AnyProto" namespace="{namespace-uri(.)}"/>
				</xsl:element>
				<xsl:element name="SourceAddress" namespace="{namespace-uri(.)}">
					<xsl:element name="Addresses" namespace="{namespace-uri(.)}">
						<xsl:element name="AddressEntry" namespace="{namespace-uri(.)}">
							<xsl:element name="AddressName" namespace="{namespace-uri(.)}">
								<xsl:attribute name="value"><xsl:value-of select="$manageraddresslistname"/></xsl:attribute>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="SourcePort" namespace="{namespace-uri(.)}">
					<xsl:element name="AnyPort" namespace="{namespace-uri(.)}"/>
				</xsl:element>
				<xsl:element name="DestAddress" namespace="{namespace-uri(.)}">
					<xsl:element name="Self" namespace="{namespace-uri(.)}"/>
				</xsl:element>
				<xsl:element name="DestPort" namespace="{namespace-uri(.)}">
					<xsl:element name="Ports" namespace="{namespace-uri(.)}">
						<xsl:element name="PortEntry" namespace="{namespace-uri(.)}">
							<xsl:element name="PortName" namespace="{namespace-uri(.)}">
								<xsl:attribute name="value">HTTPS</xsl:attribute>
							</xsl:element>
						</xsl:element>
						<xsl:element name="PortEntry" namespace="{namespace-uri(.)}">
							<xsl:element name="PortName" namespace="{namespace-uri(.)}">
								<xsl:attribute name="value">SSH</xsl:attribute>
							</xsl:element>
						</xsl:element>
					</xsl:element>
				</xsl:element>
				<xsl:element name="Deprecated" namespace="{namespace-uri(.)}">
					<xsl:attribute name="Enabled">false</xsl:attribute>
					<xsl:element name="NatName" namespace="{namespace-uri(.)}">
						<xsl:attribute name="NatEnabled">false</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	<xsl:template name="insertManagerAddressList">
		<xsl:if test="count($managerpolicy/admin:Manager)>0">
			<xsl:element name="AddressList" namespace="{namespace-uri(.)}">
				<xsl:attribute name="Name"><xsl:value-of select="$manageraddresslistname"/></xsl:attribute>
				<xsl:attribute name="GUID"><xsl:value-of select="$managerpolicy/@GUID"/></xsl:attribute>
				<xsl:attribute name="Comment">System Generated Manager Address List</xsl:attribute>
				<xsl:element name="IPAddressList" namespace="{namespace-uri(.)}">
					<xsl:for-each select="$managerpolicy/admin:Manager">
					<xsl:choose>
						<xsl:when test="$fwmBeingProcessed='true'">
							<xsl:element name="AddressRange" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
								<xsl:attribute name="IPRangeAddress">
									<xsl:if test="count(@IPRange)>0"><xsl:value-of select="@IPRange"/></xsl:if>
									<xsl:if test="count(@IPAddr)>0"><xsl:value-of select="concat(@IPAddr,'-',@IPAddr)"/></xsl:if>
								</xsl:attribute>
							</xsl:element>
						</xsl:when>
						<xsl:otherwise>
							<xsl:element name="AddressRange" namespace="http://www.iss.net/cml/NetworkProtector/tfwm">
								<xsl:attribute name="IPRangeAddress">
									<xsl:if test="count(@IPRange)>0"><xsl:value-of select="@IPRange"/></xsl:if>
									<xsl:if test="count(@IPAddr)>0"><xsl:value-of select="concat(@IPAddr,'-',@IPAddr)"/></xsl:if>
								</xsl:attribute>
							</xsl:element>
						</xsl:otherwise>
					</xsl:choose>
					</xsl:for-each>
				</xsl:element>
			</xsl:element>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
