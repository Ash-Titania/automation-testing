<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:network="http://www.iss.net/cml/NetworkProtector/network" >
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<!-- Include the resolve common templates that handle the common elements first always because it contains a couple global variables. -->
	<xsl:include href="resolvecommon100.xsl" />
	<!-- Main Processing Template -->
	<xsl:template match="network:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/network">
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<!-- 
		Templates for the parents of all elements containing a reference are handled here. .
		The parents need to apply templates twice, one using mode=resolve and one using the default mode.
		The first apply using mode=resolve populates attributes in the element with the resolved values.
		The second apply templates copies the actual child elements and their attributes.
		The name of the element must match the name of the attribute that is an attribute of the parent element 
        that contains the reference element.
	-->
	<xsl:template match="network:Static">
		<xsl:element name="Static" namespace="http://www.iss.net/cml/NetworkProtector/network">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates mode="resolve"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="network:ExternalDNS">
		<xsl:element name="ExternalDNS" namespace="http://www.iss.net/cml/NetworkProtector/network">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates mode="resolve"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="network:InternalInterface">
		<xsl:element name="InternalInterface" namespace="http://www.iss.net/cml/NetworkProtector/network">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates mode="resolve"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="network:TransparentMgmtConfig">
		<xsl:element name="TransparentMgmtConfig" namespace="http://www.iss.net/cml/NetworkProtector/network">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates mode="resolve"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<!-- 
		Any element that contains an element that needs resolving is handled here.
		These templates only handle mode=resolve.  This mode avoids putting the element tag out here so that the 
		Templates that do the resolving below simply output the attributes to the current parent being processed.
	-->
	<xsl:template match="network:Static/network:IpAddr" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="network:Static/network:GatewayIpAddr" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="network:ExternalDNS/network:PrimaryDNS" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="network:ExternalDNS/network:SecondaryDNS" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="network:ExternalDNS/network:TertiaryDNS" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="network:InternalInterface/network:IpAddr" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="network:TransparentMgmtConfig/network:IpAddr" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="network:TransparentMgmtConfig/network:GatewayIpAddr" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="network:TransparentMgmtConfig/network:PrimaryDNS" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="network:TransparentMgmtConfig/network:SecondaryDNS" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="network:TransparentMgmtConfig/network:TertiaryDNS" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
</xsl:stylesheet>
