<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:fwm="http://www.iss.net/cml/NetworkProtector/fwm"
	xmlns:avm="http://www.iss.net/cml/NetworkProtector/avm" 
	xmlns:cflts="http://www.iss.net/cml/NetworkProtector/cflts" 
	xmlns:spam="http://www.iss.net/cml/NetworkProtector/spam" 
	xmlns:network="http://www.iss.net/cml/NetworkProtector/network" 
	xmlns:common="http://www.iss.net/cml/NetworkProtector/common"
	xmlns:intip="http://www.iss.net/cml/NetworkProtector/intip" >
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:param name="avmxml" select=" 'npavmPhoenix1_0.xml' "/>
	<xsl:param name="cfltsxml" select=" 'npcflts1_0.xml' " />
	<xsl:param name="spamxml" select=" 'npspamPhoenix1_0.xml' " />
	<xsl:param name="netxml" select=" '' " />
	<xsl:param name="pppInt" select=" 'ppp0' "/>
	<xsl:param name="hamode" select=" 'none' " />
	<xsl:param name="intipxml" select=" 'npintip1_0.xml' "/>
	<xsl:variable name="httpProxyEnabled">
		<xsl:choose>
			<xsl:when test="document($cfltsxml)/cflts:policy/cflts:Config/@cfltEnabled='true' ">true</xsl:when>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@httpEnabled='true' ">true</xsl:when>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@vpshttpEnabled='true' ">true</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="smtpProxyEnabled">
		<xsl:choose>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@smtpEnabled='true' ">true</xsl:when>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@vpssmtpEnabled='true' ">true</xsl:when>
			<xsl:when test="document($spamxml)/spam:policy/spam:Config/@aspmEnabled='true' and
document($spamxml)/spam:policy/spam:Config/spam:ProtectedProtocols/@smtpEnabled='true' ">true</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="ftpProxyEnabled">
		<xsl:choose>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@ftpEnabled='true' ">true</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="pop3ProxyEnabled">
		<xsl:choose>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@pop3Enabled='true' ">true</xsl:when>
			<xsl:when test="document($avmxml)/avm:policy/avm:Config/@AvmEnabled='true' and document($avmxml)/avm:policy/avm:Config/avm:ProtectedProtocols/@vpspop3Enabled='true' ">true</xsl:when>
			<xsl:when test="document($spamxml)/spam:policy/spam:Config/@aspmEnabled='true' and
document($spamxml)/spam:policy/spam:Config/spam:ProtectedProtocols/@pop3Enabled='true' ">true</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="externalInterfaceName">
		<xsl:choose>
			<xsl:when test="string-length($netxml) = 0">none</xsl:when>
			<xsl:when test="count(document($netxml)/network:policy/network:RoutingModeConfig/network:ExternalConfig/network:IPAddress/network:PPPoE) > 0">
				<xsl:value-of select="$pppInt"/>
			</xsl:when>
			<xsl:otherwise>eth1</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!-- Determine if we need to substitute the eth1ip address for any LocalIpAddress value in any Security Gateway -->
	<xsl:variable name="tuningparm" select="fwm:policy/fwm:TuningSetting[@name='override.localid.with.eth1'] "/>
	<xsl:variable name="overrideLocal">
		<xsl:choose>
			<xsl:when test="count($tuningparm) > 0"><xsl:value-of select="$tuningparm/common:boolean/@value"/></xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<!-- Get the IP Address for eth1 out of the intip document -->
	<xsl:variable name="interfaces" select="document($intipxml)/intip:policy/intip:interface"/>
	<xsl:variable name="eth1Node" select="$interfaces[@name='eth1']" />
	<xsl:variable name="eth1IP" select="$eth1Node/@ip"/>
	<xsl:template match="fwm:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:apply-templates select="fwm:Policy"/>
				<xsl:if test="$hamode!='secondary' ">
					<xsl:call-template name="createIKERules"/>
					<xsl:call-template name="createIPSECRules"/>
					<xsl:call-template name="createVPNAddressRules"/>
				</xsl:if>
				<xsl:apply-templates select="fwm:CommonLists"/>
				<xsl:apply-templates select="fwm:MessageConfig"/>
				<xsl:apply-templates select="fwm:ProxyRedirectRule"/>
				<xsl:if test="$hamode!='secondary' ">
					<xsl:apply-templates select="fwm:VpnAdvancedSettings"/>
				</xsl:if>
				<xsl:apply-templates select="fwm:TuningSetting"/>
				<xsl:if test="$hamode!='secondary' ">
					<xsl:apply-templates select="fwm:SecurityGateways/fwm:L2TPIPSECSecGwList" />
				</xsl:if>
				<!-- Handle nodes in the tfwm policy -->
				<xsl:apply-templates select="fwm:L2AccessControl"/>
				<xsl:apply-templates select="fwm:LocalRouter"/>
				
			</xsl:element>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template name="createIPSECRules">
	<!-- Need to take each IPSEC Rule and merge in items from referenced security gateway to create old style IPSEC rules -->
		<xsl:for-each select="fwm:IPSECRules">
			<xsl:element name="IPSECRules" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:copy-of select="@*"/>
				<xsl:choose>
					<xsl:when test="count(fwm:SecurityGateway/fwm:AutoIkeSecGw) > 0">
						<!-- Automatic Security Gateway -->
						<xsl:variable name="matchgw" select="/fwm:policy/fwm:SecurityGateways/fwm:AutoIkeSecurityGatewayList[@Name=current()/fwm:SecurityGateway/fwm:AutoIkeSecGw/@value]"/>
						<xsl:attribute name="EncapsulationMode"><xsl:value-of select="$matchgw/fwm:IpsecConfig/@EncapsulationMode"></xsl:value-of></xsl:attribute>
						<xsl:apply-templates select="fwm:SourceAddress"/>
						<xsl:apply-templates select="fwm:SourcePort"/>
						<xsl:apply-templates select="fwm:DestAddress"/>
						<xsl:apply-templates select="fwm:DestPort"/>
						<xsl:element name="ManualAutoParms" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:element name="AutomaticIpsec" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
								<xsl:attribute name="PeerSecurityGateway"><xsl:value-of select="$matchgw/fwm:IkeConfig/@RemoteIpAddress"/></xsl:attribute>
								<xsl:apply-templates select="$matchgw/fwm:IpsecConfig"/>
							</xsl:element>
						</xsl:element>
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="matchgw" select="/fwm:policy/fwm:SecurityGateways/fwm:ManualSecurityGatewayList[@Name=current()/fwm:SecurityGateway/fwm:ManualSecGw/@value]"/>
						<xsl:attribute name="EncapsulationMode"><xsl:value-of select="$matchgw/@EncapsulationMode"></xsl:value-of></xsl:attribute>
						<xsl:apply-templates select="fwm:SourceAddress"/>
						<xsl:apply-templates select="fwm:SourcePort"/>
						<xsl:apply-templates select="fwm:DestAddress"/>
						<xsl:apply-templates select="fwm:DestPort"/>
						<xsl:element name="ManualAutoParms" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
							<xsl:element name="ManualIpsec" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
								<xsl:attribute name="PeerSecurityGateway"><xsl:value-of select="$matchgw/@PeerSecurityGateway"/></xsl:attribute>
								<xsl:apply-templates select="$matchgw/*"/>
							</xsl:element>
						</xsl:element>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="createVPNAddressRules">
	<!-- Need to take each VpnAddr Security Gateway and create a VPN Address -->
		<xsl:for-each select="fwm:SecurityGateways/fwm:VpnAddrIkeSecurityGatewayList">
			<xsl:element name="AddressRanges" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:attribute name="Name"><xsl:value-of select="@Name"/></xsl:attribute>
				<xsl:attribute name="GUID"><xsl:value-of select="@GUID"/></xsl:attribute>
				<xsl:apply-templates select="fwm:IpsecConfig"/>
				<xsl:element name="AssociatedPolicies" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
					<xsl:attribute name="GUID"><xsl:value-of select="@GUID"/></xsl:attribute>
					<xsl:attribute name="PolicyName"><xsl:value-of select="concat('va',@Name)"/></xsl:attribute>
				</xsl:element>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<xsl:template name="createIKERules">
	<!-- Need to take each AutoIke Security Gateway and create an IKE rule for it. -->
	<!-- Need to take each VpnAddr Security Gateway and create an IKE Rule for it first then a VPN Address. -->
		<xsl:for-each select="fwm:SecurityGateways/fwm:AutoIkeSecurityGatewayList">
			<xsl:element name="IKERules" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:attribute name="Name"><xsl:value-of select="concat('ai',@Name)"/></xsl:attribute>
				<xsl:attribute name="GUID"><xsl:value-of select="@GUID"/></xsl:attribute>
				<xsl:attribute name="Enabled">true</xsl:attribute>
				<xsl:attribute name="Comment"><xsl:value-of select="@Comment"/></xsl:attribute>
				<xsl:apply-templates select="fwm:IkeConfig"/>
			</xsl:element>
		</xsl:for-each>
		<xsl:for-each select="fwm:SecurityGateways/fwm:VpnAddrIkeSecurityGatewayList">
			<xsl:element name="IKERules" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:attribute name="Name"><xsl:value-of select="concat('va',@Name)"/></xsl:attribute>
				<xsl:attribute name="GUID"><xsl:value-of select="@GUID"/></xsl:attribute>
				<xsl:attribute name="Enabled">true</xsl:attribute>
				<xsl:attribute name="Comment"><xsl:value-of select="@Comment"/></xsl:attribute>
				<xsl:apply-templates select="fwm:IkeConfig"/>
			</xsl:element>
		</xsl:for-each>
	</xsl:template>
	<xsl:template match="fwm:IkeConfig">
		<xsl:copy-of select="@*[name() != 'LocalIdData' or name() != 'LocalIpAddress']"/>
		<xsl:choose>
			<xsl:when test="$overrideLocal='true' and @LocalIdData='0.0.0.0' "><xsl:attribute name="LocalIdData"><xsl:value-of select="$eth1IP"/></xsl:attribute></xsl:when>
			<xsl:otherwise><xsl:attribute name="LocalIdData"><xsl:value-of select="@LocalIdData"/></xsl:attribute></xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="$overrideLocal='true' and @LocalIpAddress='0.0.0.0' "><xsl:attribute name="LocalIpAddress"><xsl:value-of select="$eth1IP"/></xsl:attribute></xsl:when>
			<xsl:otherwise><xsl:attribute name="LocalIpAddress"><xsl:value-of select="@LocalIpAddress"/></xsl:attribute></xsl:otherwise>
		</xsl:choose>
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="fwm:IpsecConfig">
		<xsl:copy-of select="@*[name() != 'EncapsulationMode']"/>
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="fwm:ProxyRedirectRule">
		<xsl:element name="ProxyRedirectRule" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*[name()!= 'Enabled' ] " />
			<!-- Set enabled based upon whether the proxy needs is enabled in avm, cflts, or spam -->
			<xsl:attribute name="Enabled">
				<xsl:choose>
					<xsl:when test="@Proxy='HTTP' and $httpProxyEnabled='true' and @UserEnabled='true' ">true</xsl:when>
					<xsl:when test="@Proxy='FTP' and $ftpProxyEnabled='true' and @UserEnabled='true'  ">true</xsl:when>
					<xsl:when test="@Proxy='SMTP' and $smtpProxyEnabled='true' and @UserEnabled='true'  ">true</xsl:when>
					<xsl:when test="@Proxy='POP3' and $pop3ProxyEnabled='true' and @UserEnabled='true'  ">true</xsl:when>
					<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>		
	</xsl:template>
	<xsl:template match="fwm:DynamicLocalGateway">
		<xsl:element name="DynamicLocalGateway" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*[name() != 'DynamicInterface' ]"/>
			<xsl:attribute name="DynamicInterface">
				<xsl:choose>
					<xsl:when test="$externalInterfaceName != 'none'">
						<xsl:value-of select="$externalInterfaceName"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@DynamicInterface"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:DynamicWAN">
		<xsl:element name="DynamicWAN" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*[name() != 'InterfaceName'  ]"/>
			<xsl:attribute name="InterfaceName">
				<xsl:choose>
					<xsl:when test="$externalInterfaceName != 'none'">
						<xsl:value-of select="$externalInterfaceName"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@InterfaceName"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:SpecificInterfaceAddress">
		<xsl:element name="SpecificInterfaceAddress" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*[name() != 'InterfaceName'  ]"/>
			<xsl:attribute name="InterfaceName">
				<xsl:choose>
					<xsl:when test="$externalInterfaceName != 'none'">
						<xsl:value-of select="$externalInterfaceName"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="@InterfaceName"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:L2TPIKEAttr">
		<xsl:element name="L2TPIKEAttr" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
			<xsl:copy-of select="@*[name() != 'LocalIdData' ]"/>
			<xsl:choose>
				<xsl:when test="$overrideLocal='true' and @LocalIdData='0.0.0.0' "><xsl:attribute name="LocalIdData"><xsl:value-of select="$eth1IP"/></xsl:attribute></xsl:when>
				<xsl:otherwise><xsl:attribute name="LocalIdData"><xsl:value-of select="@LocalIdData"/></xsl:attribute></xsl:otherwise>
			</xsl:choose>
			<xsl:if test="$hamode='primary'">
				<xsl:attribute name="AliasIp"><xsl:value-of select="$eth1IP"/></xsl:attribute>
			</xsl:if>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="fwm:TuningSetting">
		<xsl:if test="@name!='override.localid.with.eth1'">
			<xsl:element name="TuningSetting" namespace="http://www.iss.net/cml/NetworkProtector/fwm">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates/>
			</xsl:element>
		</xsl:if>
	</xsl:template>
	</xsl:stylesheet>
