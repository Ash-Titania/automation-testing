<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:net="http://www.iss.net/cml/NetworkProtector/network" xmlns:ham="http://www.iss.net/cml/NetworkProtector/ham" xmlns:ospf="http://www.iss.net/cml/NetworkProtector/ospf" xmlns:dhcp="http://www.iss.net/cml/NetworkProtector/dhcp">
	<xsl:output method="text" encoding="UTF-8" indent="no"/>
	<xsl:strip-space elements="*"/>
	<!-- In order for this script to function properly dynaddr and netobj references need to be resolved prior to running this script currently. 
		  Should I rework this script to work off of the non-resolved variant? -->
	<xsl:include href="verifycommon100.xsl"/>
	<xsl:param name="hamxml" select=" 'npham1_0.xml' "/>
	<xsl:param name="ospfxml" select=" 'npospf1_0.xml' "/>
	<xsl:param name="dhcpxml" select=" 'npdhcp1_0.xml' "/>
	<xsl:param name="networkchanged" select=" 'false' "/>
	<xsl:variable name="policyName" select="'network'"/>
	<xsl:variable name="hamenabled" select="document($hamxml)/ham:policy/ham:baseConfig/@enabled"/>
	<xsl:variable name="ospfenabled" select="document($ospfxml)/ospf:policy/ospf:OspfConfig/@Enabled"/>
	<xsl:variable name="ospfinterfaces" select="document($ospfxml)/ospf:policy/ospf:OspfConfig/ospf:Interface"/>
	<xsl:variable name="dhcprelayenabled" select="document($dhcpxml)/dhcp:policy/dhcp:ServiceConfig/dhcp:DHCPRelayConfig/@Enabled"/>
	<xsl:variable name="dhcprelayinterfaces" select="document($dhcpxml)/dhcp:policy/dhcp:ServiceConfig/dhcp:DHCPRelayConfig/dhcp:Interface"/>
	<xsl:variable name="dhcpsrvenabled" select="document($dhcpxml)/dhcp:policy/dhcp:DHCPConfig/@Enabled"/>
	<xsl:variable name="extisprimary" select="net:policy/net:NetworkingConfig/net:ModeConfig/net:RoutingModeConfig/net:ExternalConfig/@PrimaryMgmtInterface"/>
	<xsl:variable name="eth0enabled">
		<xsl:choose>
			<xsl:when test="count(net:policy/net:NetworkingConfig/net:ModeConfig/net:RoutingModeConfig)>0">
				<xsl:choose>
					<xsl:when test="count(net:policy/net:NetworkingConfig/net:ModeConfig/net:RoutingModeConfig/net:InternalConfig/net:InternalInterface[@InterfaceName='eth0'])>0">
						<xsl:value-of select="net:policy/net:NetworkingConfig/net:ModeConfig/net:RoutingModeConfig/net:InternalConfig/net:InternalInterface[@InterfaceName='eth0']/@Enabled"/>
					</xsl:when>
					<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>true</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:template match="net:TransparentModeConfig">
		<!-- Verify that if Transparent Network Mode is Enabled HA Mode is NOT Enabled.-->
		<xsl:if test="$hamenabled='true' ">
			<xsl:text>ERROR: Transparent Network Mode and High Availability Mode can not be enabled at the same time.  If you are trying to switch from Routing  Network Mode to Transparent Network Mode please disable High Availability Mode first.  If you are trying to enable High Availability Mode  please change Network Mode to Routing Network Mode first.&#10;</xsl:text>
		</xsl:if>
		<!-- Verify that if Transparent Network Mode is Enabled OSPF is NOT Enabled.-->
		<xsl:if test="$ospfenabled='true' ">
			<xsl:text>ERROR: Transparent Network Mode and OSPF can not be enabled at the same time.  If you are trying to switch from Routing Network Mode to Transparent Network Mode please disable OSPF first.  If you are trying to enable OSPF please change Network Mode to Routing Network Mode first.&#10;</xsl:text>
		</xsl:if>
		<!-- Verify that if Transparent Network Mode is Enabled DHCP Relay is NOT Enabled.-->
		<xsl:if test="$dhcprelayenabled='true' ">
			<xsl:text>ERROR: Transparent Network Mode and DHCP Relay can not be enabled at the same time.  If you are trying to switch from Routing Network Mode to Transparent Network Mode please disable DHCP Relay first.  If you are trying to enable DHCP Relay please change Network Mode to Routing Network Mode first.&#10;</xsl:text>
		</xsl:if>
		<!-- Verify that if Transparent Network Mode is Enabled DHCP Server is NOT Enabled.-->
		<xsl:if test="$dhcpsrvenabled='true' ">
			<xsl:text>ERROR: Transparent Network Mode and DHCP Server can not be enabled at the same time.  If you are trying to switch from Routing Network Mode to Transparent Network Mode please disable DHCP Server first.  If you are trying to enable DHCP Server please change Network Mode to Routing Network Mode first.&#10;</xsl:text>
		</xsl:if>
		<xsl:apply-templates/>
	</xsl:template>
	<!-- Verify that no network interface is defined with an all zero address or subnet mask. -->
	<xsl:template match="net:TransparentMgmtConfig">
		<xsl:if test="@IpAddr = '0.0.0.0' ">
			<xsl:text>ERROR: The IP Address defined for the Transparent Mode Mgmt Interface can not be 0.0.0.0. Please correct the IP Address.&#10;</xsl:text>
		</xsl:if>
		<xsl:if test="@SubnetMask = '0.0.0.0'">
			<xsl:text>ERROR: The Subnet Mask defined for the Transparent Mode Mgmt Interface can not be 0.0.0.0. Please correct the Subnet Mask.&#10;</xsl:text>
		</xsl:if>
		<xsl:variable name="gwok">
			<xsl:call-template name="CheckGwInNetwork">
				<xsl:with-param name="NetIp" select="@IpAddr"/>
				<xsl:with-param name="NetMask" select="@SubnetMask"/>
				<xsl:with-param name="GwIp" select="@GatewayIpAddr"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:if test="$gwok = 'false' ">
			<xsl:text>ERROR: The Transparent Mode Mgmt Gateway must be on the same network as the Transparent Mode Mgmt IP Address.&#10;</xsl:text>
		</xsl:if>
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="net:Static">
		<xsl:if test="@IpAddr='0.0.0.0'">
			<xsl:text>ERROR: The IP Address defined for the External Interface can not be 0.0.0.0. Please correct the IP Address.&#10;</xsl:text>
		</xsl:if>
		<xsl:if test="@SubnetMask= '0.0.0.0'">
			<xsl:text>ERROR: The Subnet Mask defined for the External Interface can not be 0.0.0.0. Please correct the Subnet Mask.&#10;</xsl:text>
		</xsl:if>
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="net:InternalInterface">
		<xsl:if test="@IPAddress = '0.0.0.0' ">
			<xsl:text>ERROR: The IP Address defined for any Internal Interface can not be 0.0.0.0. Please correct the IP Address.&#10;</xsl:text>
		</xsl:if>
		<xsl:if test="@SubnetMask= '0.0.0.0'">
			<xsl:text>ERROR: The Subnet Mask defined for any Internal Interface can not be 0.0.0.0. Please correct the Subnet Mask.&#10;</xsl:text>
		</xsl:if>
		<xsl:if test="@PrimaryMgmtInterface='true' and $extisprimary='true'">
			<xsl:text>ERROR: Both the external interface and an internal interface are selected as the Primary Management Interface. Please select either one or the other, but not both.&#10;</xsl:text>
		</xsl:if>
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="net:policy">
		<xsl:if test="$networkchanged='true' and $hamenabled='true' ">
			<xsl:text>ERROR: You can not change Network Settings while the High-Availability feature is enabled. If managing with Site Protector, please edit the Network Settings and undo your change..&#10;</xsl:text>
		</xsl:if>
		<xsl:if test="$eth0enabled='false'">
			<xsl:text>ERROR: Eth0 is disabled.  In order for Antivirus, Content Filtering and Mail Security to function properly eth0 must always be enabled. It can be configured with a nonroutable IP address and not connected to anything, but it should always be enabled.&#10;</xsl:text>
		</xsl:if>
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="net:RoutingModeConfig">
		<xsl:variable name="eth1enabled">
			<xsl:value-of select="/net:policy/net:NetworkingConfig/net:ModeConfig/net:RoutingModeConfig/net:ExternalConfig/@Enabled"/>
		</xsl:variable>
		<xsl:variable name="enabledintinterfaces" select="/net:policy/net:NetworkingConfig/net:ModeConfig/net:RoutingModeConfig/net:InternalConfig/net:InternalInterface[@Enabled='true']"/>
		<xsl:if test="$dhcprelayenabled='true'">
			<!-- Verify that each interface defined in dhcp relay is defined and enabled in network interfaces. -->
			<xsl:for-each select="$dhcprelayinterfaces">
				<xsl:variable name="intenabled">
					<xsl:choose>
						<xsl:when test="current()/@InterfaceName='eth1'">
							<xsl:value-of select="$eth1enabled"/>
						</xsl:when>
						<xsl:when test="count($enabledintinterfaces[@InterfaceName=current()/@InterfaceName])>0">true</xsl:when>
						<xsl:otherwise>false</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:if test="$intenabled='false'">
					<xsl:text>ERROR:Interface </xsl:text>
					<xsl:value-of select="current()/@InterfaceName"/>
					<xsl:text> is in use in DHCP Relay Configuration but not enabled in Interface Configuration. Please enable the interface in Interface Configuration or remove it from DHCP Relay Configuration and save again.</xsl:text><xsl:text>&#10;</xsl:text>
				</xsl:if>
			</xsl:for-each>
		</xsl:if>
		<xsl:if test="$ospfenabled='true'">
			<!-- Verify that each interface defined in ospf interfaces is defined and enabled in network interfaces. -->
			<xsl:for-each select="$ospfinterfaces">
				<xsl:variable name="intenabled">
					<xsl:choose>
						<xsl:when test="current()/@InterfaceName='eth1'">
							<xsl:value-of select="$eth1enabled"/>
						</xsl:when>
						<xsl:when test="count($enabledintinterfaces[@InterfaceName=current()/@InterfaceName])>0">true</xsl:when>
						<xsl:otherwise>false</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:if test="$intenabled='false'">
					<xsl:text>ERROR:Interface </xsl:text>
					<xsl:value-of select="current()/@InterfaceName"/>
					<xsl:text> is in use in OSPF Configuration but not enabled in Interface Configuration. Please enable the interface in Interface Configuration or remove it from OSPF Configuration and save again.</xsl:text><xsl:text>&#10;</xsl:text>
				</xsl:if>
			</xsl:for-each>
		</xsl:if>
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template name="CheckGwInNetwork">
		<xsl:param name="NetIp" />
		<xsl:param name="NetMask" />
		<xsl:param name="GwIp" />
		<xsl:variable name="ipo1">
			<xsl:value-of select="substring-before($NetIp, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ipo2to4">
			<xsl:value-of select="substring-after($NetIp, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ipo2">
			<xsl:value-of select="substring-before($ipo2to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ipo3to4">
			<xsl:value-of select="substring-after($ipo2to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ipo3">
			<xsl:value-of select="substring-before($ipo3to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ipo4">
			<xsl:value-of select="substring-after($ipo3to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="sno1">
			<xsl:value-of select="substring-before($NetMask, '.' )"/>
		</xsl:variable>
		<xsl:variable name="sno2to4">
			<xsl:value-of select="substring-after($NetMask, '.' )"/>
		</xsl:variable>
		<xsl:variable name="sno2">
			<xsl:value-of select="substring-before($sno2to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="sno3to4">
			<xsl:value-of select="substring-after($sno2to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="sno3">
			<xsl:value-of select="substring-before($sno3to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="sno4">
			<xsl:value-of select="substring-after($sno3to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ripo1">
			<xsl:value-of select="substring-before($GwIp, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ripo2to4">
			<xsl:value-of select="substring-after($GwIp, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ripo2">
			<xsl:value-of select="substring-before($ripo2to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ripo3to4">
			<xsl:value-of select="substring-after($ripo2to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ripo3">
			<xsl:value-of select="substring-before($ripo3to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="ripo4">
			<xsl:value-of select="substring-after($ripo3to4, '.' )"/>
		</xsl:variable>
		<xsl:variable name="o1ok">
			<xsl:call-template name="checkOctet">
				<xsl:with-param name="sno" select="$sno1"/>
				<xsl:with-param name="ipo" select="$ipo1"/>
				<xsl:with-param name="gwo" select="$ripo1"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="o2ok">
			<xsl:call-template name="checkOctet">
				<xsl:with-param name="sno" select="$sno2"/>
				<xsl:with-param name="ipo" select="$ipo2"/>
				<xsl:with-param name="gwo" select="$ripo2"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="o3ok">
			<xsl:call-template name="checkOctet">
				<xsl:with-param name="sno" select="$sno3"/>
				<xsl:with-param name="ipo" select="$ipo3"/>
				<xsl:with-param name="gwo" select="$ripo3"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="o4ok">
			<xsl:call-template name="checkOctet">
				<xsl:with-param name="sno" select="$sno4"/>
				<xsl:with-param name="ipo" select="$ipo4"/>
				<xsl:with-param name="gwo" select="$ripo4"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$o1ok='true' and $o2ok='true' and $o3ok='true' and $o4ok='true' ">true</xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="checkOctet">
		<xsl:param name="sno"/>
		<xsl:param name="ipo" />
		<xsl:param name="gwo"/>
		<xsl:choose>
			<xsl:when test="$sno='0' ">true</xsl:when>
			<xsl:when test="$sno='255'">
				<xsl:choose>
					<xsl:when test="$ipo = $gwo">true</xsl:when>
					<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<!-- Need to get the bit values of each ip address and based on the subnet make sure the relvant ones match -->
				<xsl:variable name="ip8"><xsl:value-of select="floor( $ipo div 128) mod 2"/></xsl:variable>
				<xsl:variable name="gw8"><xsl:value-of select="floor($gwo div 128) mod 2"/></xsl:variable>
				<xsl:variable name="sn8"><xsl:value-of select="floor($sno div 128) mod 2"/></xsl:variable>
				<xsl:variable name="ip7"><xsl:value-of select="floor($ipo div 64) mod 2"/></xsl:variable>
				<xsl:variable name="gw7"><xsl:value-of select="floor($gwo div 64) mod 2"/></xsl:variable>
				<xsl:variable name="sn7"><xsl:value-of select="floor($sno div 64) mod 2"/></xsl:variable>
				<xsl:variable name="ip6"><xsl:value-of select="floor($ipo div 32) mod 2"/></xsl:variable>
				<xsl:variable name="gw6"><xsl:value-of select="floor($gwo div 32) mod 2"/></xsl:variable>
				<xsl:variable name="sn6"><xsl:value-of select="floor($sno div 32) mod 2"/></xsl:variable>
				<xsl:variable name="ip5"><xsl:value-of select="floor($ipo div 16) mod 2"/></xsl:variable>
				<xsl:variable name="gw5"><xsl:value-of select="floor($gwo div 16) mod 2"/></xsl:variable>
				<xsl:variable name="sn5"><xsl:value-of select="floor($sno div 16) mod 2"/></xsl:variable>
				<xsl:variable name="ip4"><xsl:value-of select="floor($ipo div 8) mod 2"/></xsl:variable>
				<xsl:variable name="gw4"><xsl:value-of select="floor($gwo div 8) mod 2"/></xsl:variable>
				<xsl:variable name="sn4"><xsl:value-of select="floor($sno div 8) mod 2"/></xsl:variable>
				<xsl:variable name="ip3"><xsl:value-of select="floor($ipo div 4) mod 2"/></xsl:variable>
				<xsl:variable name="gw3"><xsl:value-of select="floor($gwo div 4) mod 2"/></xsl:variable>
				<xsl:variable name="sn3"><xsl:value-of select="floor($sno div 4) mod 2"/></xsl:variable>
				<xsl:variable name="ip2"><xsl:value-of select="floor($ipo div 2) mod 2"/></xsl:variable>
				<xsl:variable name="gw2"><xsl:value-of select="floor($gwo div 2) mod 2"/></xsl:variable>
				<xsl:variable name="sn2"><xsl:value-of select="floor($sno div 2) mod 2"/></xsl:variable>
				<xsl:variable name="ip1"><xsl:value-of select="$ipo mod 2"/></xsl:variable>
				<xsl:variable name="gw1"><xsl:value-of select="$gwo mod 2"/></xsl:variable>
				<xsl:variable name="sn1"><xsl:value-of select="$sno mod 2"/></xsl:variable>
				<xsl:variable name="b1ok">
					<xsl:call-template name="testbit">
						<xsl:with-param name="snb" select="$sn1"/>
						<xsl:with-param name="ipb" select="$ip1"/>
						<xsl:with-param name="gwb" select="$gw1"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="b2ok">
					<xsl:call-template name="testbit">
						<xsl:with-param name="snb" select="$sn2"/>
						<xsl:with-param name="ipb" select="$ip2"/>
						<xsl:with-param name="gwb" select="$gw2"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="b3ok">
					<xsl:call-template name="testbit">
						<xsl:with-param name="snb" select="$sn3"/>
						<xsl:with-param name="ipb" select="$ip3"/>
						<xsl:with-param name="gwb" select="$gw3"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="b4ok">
					<xsl:call-template name="testbit">
						<xsl:with-param name="snb" select="$sn4"/>
						<xsl:with-param name="ipb" select="$ip4"/>
						<xsl:with-param name="gwb" select="$gw4"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="b5ok">
					<xsl:call-template name="testbit">
						<xsl:with-param name="snb" select="$sn5"/>
						<xsl:with-param name="ipb" select="$ip5"/>
						<xsl:with-param name="gwb" select="$gw5"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="b6ok">
					<xsl:call-template name="testbit">
						<xsl:with-param name="snb" select="$sn6"/>
						<xsl:with-param name="ipb" select="$ip6"/>
						<xsl:with-param name="gwb" select="$gw6"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="b7ok">
					<xsl:call-template name="testbit">
						<xsl:with-param name="snb" select="$sn7"/>
						<xsl:with-param name="ipb" select="$ip7"/>
						<xsl:with-param name="gwb" select="$gw7"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="b8ok">
					<xsl:call-template name="testbit">
						<xsl:with-param name="snb" select="$sn8"/>
						<xsl:with-param name="ipb" select="$ip8"/>
						<xsl:with-param name="gwb" select="$gw8"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:choose>
					<xsl:when test="$b1ok='true' and $b2ok='true' and $b3ok='true' and $b4ok='true' and $b5ok='true' and $b6ok='true' and $b7ok='true' and $b8ok='true' ">true</xsl:when>
					<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template name="testbit">
		<xsl:param name="snb"/>
		<xsl:param name="ipb" />
		<xsl:param name="gwb" />
		<xsl:choose>
			<xsl:when test="$snb=1">
				<xsl:choose>
					<xsl:when test="$ipb=$gwb">true</xsl:when>
					<xsl:otherwise>false</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>true</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
