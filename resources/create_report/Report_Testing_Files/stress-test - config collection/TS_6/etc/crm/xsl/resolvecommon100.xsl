<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:common="http://www.iss.net/cml/NetworkProtector/common" 
xmlns:netobj="http://www.iss.net/cml/NetworkObjects"
xmlns:dynaddr="http://www.iss.net/cml/NetworkProtector/dynaddr" >
	<!-- Define common parms that need to be set -->
	<xsl:param name="netobjxml" select="'networkobjects1_0.xml' "/>
	<xsl:param name="dynaddrxml" select="'npdynaddr1_0.xml' "/>
	<!-- These variables presume that dynaddrxml and netobjxml already set. -->
	<xsl:variable name="dynaddrs" select="document($dynaddrxml)/dynaddr:policy/dynaddr:DynamicAddressList"/>
	<xsl:variable name="addrnames" select="document($netobjxml)/netobj:policy/netobj:AddressList"/>
	<!-- Simple Template Copy elements not matched in another template. -->
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="@*|node()" mode="resolve" >
		<xsl:apply-templates/>
	</xsl:template>
	<!-- 
		Generic Templates that can be used anywhere these types get used.
		These templates work only in resolve mode and create attributes in the current element being processed
		named the same as their parent element.
	-->
	<!-- Ignore this element -->
	<xsl:template match="common:None" mode="resolve"/>
	<!-- IP Elements -->
	<xsl:template match="common:AddrIP" mode="resolve">
		<xsl:attribute name="{name(..)}"><xsl:value-of select="@IPAddress"/></xsl:attribute>
	</xsl:template>
	<xsl:template match="common:AddrAN" mode="resolve">
		<xsl:variable name="addrname" select="$addrnames[@Name=current()/@Name]"/>
		<xsl:if test="count($addrname/netobj:SingleAddress/@IPSingleAddress)>0">
			<xsl:attribute name="{name(..)}"><xsl:value-of select="$addrname/netobj:SingleAddress/@IPSingleAddress"/></xsl:attribute>
		</xsl:if>
	</xsl:template>
	<xsl:template match="common:AddrDA" mode="resolve">
		<xsl:variable name="dynaddr" select="$dynaddrs[@Name=current()/@Name]"/>
		<xsl:if test="count($dynaddr/dynaddr:SingleAddress/@IPSingleAddress)>0">
			<xsl:attribute name="{name(..)}"><xsl:value-of select="$dynaddr/dynaddr:SingleAddress/@IPSingleAddress"/></xsl:attribute>
		</xsl:if>
	</xsl:template>
	<!-- IP/Subnet Mask Elements -->
	<xsl:template match="common:SubnetIP" mode="resolve">
		<xsl:if test="count(@IPAddress)>0">
			<xsl:attribute name="{name(..)}"><xsl:value-of select="@IPAddress"/></xsl:attribute>
			<xsl:attribute name="{concat(substring-before(name(..),'IPAddress'),'SubnetMask')}"><xsl:value-of select="@SubnetMask"/></xsl:attribute>
		</xsl:if>
	</xsl:template>
	<xsl:template match="common:SubnetAN" mode="resolve">
		<xsl:variable name="addrname" select="$addrnames[@Name=current()/@Name]"/>
		<xsl:if test="count($addrname/netobj:SingleAddress/@IPSingleAddress)>0">
			<xsl:attribute name="{name(..)}"><xsl:value-of select="$addrname/netobj:SingleAddress/@IPSingleAddress"/></xsl:attribute>
			<xsl:attribute name="{concat(substring-before(name(..),'IPAddress'),'SubnetMask')}"><xsl:value-of select="$addrname/netobj:AddressSubnet/@SubnetMask"/></xsl:attribute>
		</xsl:if>
	</xsl:template>
	<xsl:template match="common:SubnetDA" mode="resolve">
		<xsl:variable name="dynaddr" select="$dynaddrs[@Name=current()/@Name]"/>
		<xsl:if test="count($dynaddr/dynaddr:AddressSubnet/@IPAddress)>0">
			<xsl:attribute name="{name(..)}"><xsl:value-of select="$dynaddr/dynaddr:AddressSubnet/@IPAddress"/></xsl:attribute>
			<xsl:attribute name="{concat(substring-before(name(..),'IPAddress'),'SubnetMask')}"><xsl:value-of select="$dynaddr/dynaddr:AddressSubnet/@SubnetMask"/></xsl:attribute>
		</xsl:if>
	</xsl:template>
	<!-- IP Range Elements -->
	<xsl:template match="common:AddrrngIP" mode="resolve">
		<xsl:if test="count(@IPAddress)>0">
			<xsl:attribute name="{name(..)}"><xsl:value-of select="@IPAddress"/></xsl:attribute>
		</xsl:if>
	</xsl:template>
	<xsl:template match="common:AddrrngAN" mode="resolve">
		<xsl:variable name="addrname" select="$addrnames[@Name=current()/@Name]"/>
		<xsl:if test="count($addrname/netobj:AddressRange/@IPRangeAddress)>0">
			<xsl:attribute name="{name(..)}"><xsl:value-of select="$addrname/netobj:AddressRange/@IPRangeAddress"/></xsl:attribute>
		</xsl:if>
	</xsl:template>
	<xsl:template match="common:AddrrngDA" mode="resolve">
		<xsl:variable name="dynaddr" select="$dynaddrs[@Name=current()/@Name]"/>
		<xsl:if test="count($dynaddr/dynaddr:AddressRange/@IPRangeAddress)>0">
			<xsl:attribute name="{name(..)}"><xsl:value-of select="$dynaddr/dynaddr:AddressRange/@IPRangeAddress"/></xsl:attribute>
		</xsl:if>
	</xsl:template>
	<!-- CIDR Elements -->
	<xsl:template match="common:AddrcidrIP" mode="resolve">
		<xsl:if test="count(@IPAddress)>0">
			<xsl:attribute name="{name(..)}"><xsl:value-of select="@IPAddress"/></xsl:attribute>
		</xsl:if>
	</xsl:template>
	<xsl:template match="common:AddrcidrAN" mode="resolve">
		<xsl:variable name="addrname" select="$addrnames[@Name=current()/@Name]"/>
		<xsl:if test="count($addrname/netobj:AddressMask/@IPAddressMask)>0">
			<xsl:attribute name="{name(..)}"><xsl:value-of select="$addrname/netobj:AddressMask/@IPAddressMask"/></xsl:attribute>
		</xsl:if>
	</xsl:template>
	<xsl:template match="common:AddrcidrDA" mode="resolve">
		<xsl:variable name="dynaddr" select="$dynaddrs[@Name=current()/@Name]"/>
		<xsl:if test="count($dynaddr/dynaddr:AddressMask/@IPAddressMask)>0">
			<xsl:attribute name="{name(..)}"><xsl:value-of select="$dynaddr/dynaddr:AddressMask/@IPAddressMask"/></xsl:attribute>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
