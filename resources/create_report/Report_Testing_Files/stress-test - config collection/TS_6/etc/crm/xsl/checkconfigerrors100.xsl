<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:net="http://www.iss.net/cml/NetworkProtector/network" 
xmlns:ham="http://www.iss.net/cml/NetworkProtector/ham"
xmlns:ospf="http://www.iss.net/cml/NetworkProtector/ospf">
	<xsl:output method="text" encoding="UTF-8" indent="no"/>
	<xsl:strip-space elements="*"/>
	<xsl:param name="hamxml" select=" 'npham1_0.xml' "/>
	<xsl:param name="ospfxml" select=" 'npospf1_0.xml' "/>
	<xsl:variable name="hamenabled" select="document($hamxml)/ham:policy/ham:baseConfig/@enabled"/>
	<xsl:variable name="ospfenabled" select="document($ospfxml)/ospf:policy/ospf:OspfConfig/@Enabled"/>
	<xsl:template match="net:TransparentMgmtConfig">
	<!-- Verify that if Transparent Network Mode is Enabled HA Mode is NOT Enabled.-->
	<xsl:if test="$hamenabled='true' ">
		<xsl:text>ERROR: Transparent Network Mode and High Availability Mode can not be enabled at the same time.  If you are trying to switch from Routing  Network Mode to Transparent Network Mode please disable High Availability Mode first.  If you are trying to enable High Availability Mode  please change Network Mode to Routing Network Mode first.&#10;</xsl:text>
	</xsl:if>
	<!-- Verify that if Transparent Network Mode is Enabled OSPF is NOT Enabled.-->
	<xsl:if test="$ospfenabled='true' ">
		<xsl:text>ERROR: Transparent Network Mode and OSPF can not be enabled at the same time.  If you are trying to switch from Routing Network Mode to Transparent Network Mode please disable OSPF first.  If you are trying to enable OSPF please change Network Mode to Routing Network Mode first.&#10;</xsl:text>
	</xsl:if>
	</xsl:template>
	<!-- Verify that no network interface is defined with an all zero address or subnet mask. -->
	<xsl:template match="net:TransparentMgmtConfig">
		<xsl:if test="@IpAddr = '0.0.0.0' ">
			<xsl:text>ERROR: The IP Address defined for the Transparent Mode Mgmt Interface can not be 0.0.0.0. Please correct the IP Address.&#10;</xsl:text>
		</xsl:if>
		<xsl:if test="@SubnetMask = '0.0.0.0'">
			<xsl:text>ERROR: The Subnet Mask defined for the Transparent Mode Mgmt Interface can not be 0.0.0.0. Please correct the Subnet Mask.&#10;</xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template match="net:Static">
		<xsl:if test="@IpAddr='0.0.0.0'">
			<xsl:text>ERROR: The IP Address defined for the External Interface can not be 0.0.0.0. Please correct the IP Address.&#10;</xsl:text>
		</xsl:if>
		<xsl:if test="@SubnetMask= '0.0.0.0'">
			<xsl:text>ERROR: The Subnet Mask defined for the External Interface can not be 0.0.0.0. Please correct the Subnet Mask.&#10;</xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template match="net:InternalInterface">
		<xsl:if test="@IPAddress = '0.0.0.0' ">
			<xsl:text>ERROR: The IP Address defined for any Internal Interface can not be 0.0.0.0. Please correct the IP Address.&#10;</xsl:text>
		</xsl:if>
		<xsl:if test="@SubnetMask= '0.0.0.0'">
			<xsl:text>ERROR: The Subnet Mask defined for any Internal Interface can not be 0.0.0.0. Please correct the Subnet Mask.&#10;</xsl:text>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
