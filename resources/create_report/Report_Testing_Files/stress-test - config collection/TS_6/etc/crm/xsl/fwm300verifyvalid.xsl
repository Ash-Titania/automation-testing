<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:fwm="http://www.iss.net/cml/NetworkProtector/fwm" 
xmlns:dynaddr="http://www.iss.net/cml/NetworkProtector/dynaddr"
xmlns:netobj="http://www.iss.net/cml/NetworkObjects"
xmlns:intip="http://www.iss.net/cml/NetworkProtector/intip"
xmlns:common="http://www.iss.net/cml/NetworkProtector/common">
	<xsl:output method="text" encoding="UTF-8"/>
	<xsl:param name="dynaddrxml" select=" 'npdynaddr1_0.xml' "/>
	<xsl:param name="intipxml" select=" 'npintip1_0.xml' "/>
	<xsl:param name="netobjxml" select="'networkobjects1_0.xml' "/>
	<xsl:variable name="dynaddrs" select="document($dynaddrxml)/dynaddr:policy/dynaddr:DynamicAddressList"/>
	<xsl:variable name="addrnames" select="document($netobjxml)/netobj:policy/netobj:AddressList"/>
	<xsl:variable name="addrgroups" select="document($netobjxml)/netobj:policy/netobj:AddressGroupList"/>
	<xsl:variable name="portnames" select="document($netobjxml)/netobj:policy/netobj:PortList"/>
	<xsl:variable name="portgroups" select="document($netobjxml)/netobj:policy/netobj:PortGroupList"/>
	<!--<xsl:variable name="externalinterfaces" select="document($networkxml)/network:policy/network:RoutingModeConfig/network:ExternalConfig/network:IPAddress/network:Static"/>
	<xsl:variable name="internalinterfaces" select="document($networkxml)/network:policy/network:RoutingModeConfig/network:InternalConfig/network:InternalInterface[@Enabled='true']"/>
	-->
	<xsl:variable name="interfaces" select="document($intipxml)/intip:policy/intip:interface"/>
	<!-- Determine if we need to ignore LocalIpAddress and LocaIIdData checks -->
	<xsl:variable name="tuningparm" select="fwm:policy/fwm:TuningSetting[@name='override.localid.with.eth1'] "/>
	<xsl:variable name="overrideLocal">
		<xsl:choose>
			<xsl:when test="count($tuningparm) > 0"><xsl:value-of select="$tuningparm/common:boolean/@value"/></xsl:when>
			<xsl:otherwise>false</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:template match="@*|node()">
		<xsl:param name="ruleEnabled"/>
		<xsl:apply-templates select="@*|node()">
			<xsl:with-param name="ruleEnabled" select="$ruleEnabled"/>
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template match="fwm:policy">
		<xsl:apply-templates/>
		<xsl:apply-templates select="document($netobjxml)/netobj:policy/netobj:AddressGroupList"/>
	</xsl:template>
	<xsl:template match="fwm:Policy">
		<xsl:variable name="anyProtocol">
			<xsl:choose>
				<xsl:when test="count(fwm:Protocol/fwm:ProtocolName)>0">false</xsl:when>
				<xsl:otherwise>true</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="destIsNetobj">
			<xsl:choose>
				<xsl:when test="count(fwm:DestPort/fwm:Ports)>0">true</xsl:when>
				<xsl:otherwise>false</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:if test="@Enabled='true' and $anyProtocol='false' and $destIsNetobj='true' ">
			<xsl:text>ERROR: On an Access Rule Protocol is set to TCP or UDP and Destination Port is set to one or more Network Objects. Protocol must be Any when using a Network Object for Dest Port.&#10;</xsl:text>
		</xsl:if>
		<xsl:apply-templates>
			<xsl:with-param name="ruleEnabled" select="@Enabled"/>
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template match="fwm:IPSECRules">
		<xsl:variable name="anyProtocol">
			<xsl:choose>
				<xsl:when test="@Protocol='ALL' ">true</xsl:when>
				<xsl:otherwise>false</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="destIsNetobj">
			<xsl:choose>
				<xsl:when test="count(fwm:DestPort/fwm:PortName)>0">true</xsl:when>
				<xsl:otherwise>false</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:if test="@Enabled='true' and $anyProtocol='false' and $destIsNetobj='true' ">
			<xsl:text>ERROR: On an IPSEC Rule Protocol is not set to All when Destination Port is set to a Port Name. Protocol must be All when using a Port Name for Dest Port.&#10;</xsl:text>
		</xsl:if>
		<xsl:apply-templates>
			<xsl:with-param name="ruleEnabled" select="@Enabled"/>
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template match="fwm:ProxyRedirectRule">
		<xsl:apply-templates>
			<xsl:with-param name="ruleEnabled" select="@UserEnabled"/>
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template match="fwm:GlobalNatBindList">
		<xsl:variable name="anyProtocol">
			<xsl:choose>
				<xsl:when test="count(fwm:Protocol/fwm:ProtocolName)>0">false</xsl:when>
				<xsl:otherwise>true</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="destIsNetobj">
			<xsl:choose>
				<xsl:when test="count(fwm:ServicePort/fwm:PortName)>0">true</xsl:when>
				<xsl:when test="count(fwm:ServicePort/fwm:PortGroup)>0">true</xsl:when>
				<xsl:otherwise>false</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:if test="@Enabled='true' and $anyProtocol='false' and $destIsNetobj='true' ">
			<xsl:text>ERROR: On an Source NAT Rule Protocol is set to TCP or UDP and Destination Port is set to a Network Object. Protocol must be Any when using a Network Object for Dest Port.&#10;</xsl:text>
		</xsl:if>		
		<xsl:apply-templates>
			<xsl:with-param name="ruleEnabled" select="@Enabled"/>
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template match="fwm:GlobalReverseNatList">
		<!-- Verify that when Dest Port is a Network object that Protocol is Any.-->
		<xsl:variable name="anyProtocol">
			<xsl:choose>
				<xsl:when test="count(fwm:Protocol/fwm:ProtocolName)>0">false</xsl:when>
				<xsl:otherwise>true</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="destIsNetobj">
			<xsl:choose>
				<xsl:when test="count(fwm:IncomingService/fwm:PortName)>0">true</xsl:when>
				<xsl:when test="count(fwm:IncomingService/fwm:PortGroup)>0">true</xsl:when>
				<xsl:otherwise>false</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:if test="@Enabled='true' and $anyProtocol='false' and $destIsNetobj='true' ">
			<xsl:text>ERROR: On an Destination NAT Rule Protocol is set to TCP or UDP and Destination Port is set to a Network Object. Protocol must be Any when using a Network Object for Dest Port.&#10;</xsl:text>
		</xsl:if>		
		<!-- Verify that when a DNAT rule refers to a Many to One NAT Config Item that the Many to One NAT Config Item contains a Static IP Address only -->
		<xsl:if test="@Enabled='true' and count(fwm:ReverseNatIp/fwm:NatRecord)>0">
			<xsl:variable name="NatRecord" select="/fwm:policy/fwm:CommonLists/fwm:NatList[@Name=current()/fwm:ReverseNatIp/fwm:NatRecord/@NatRecordName]"/>
			<xsl:if test="count($NatRecord/fwm:ManyToOne/fwm:DynamicWAN)>0">
				<xsl:text>ERROR:Destination NAT Rule </xsl:text>
				<xsl:value-of select="@Name"/>
				<xsl:text> refers to a Many To One NAT Configuration Item that uses a Dynamic Interface which is not permitted.  Please adjust the Destination NAT Rule as needed.</xsl:text><xsl:text>&#10;</xsl:text>
			</xsl:if>
		</xsl:if>
		<!-- Verify that when the Translated Port of a DNAT rule is set to Single Port that the Translated Address does not refer to a NAT Config Item that is Many to Many or One to One -->
		<xsl:if test="@Enabled='true' and count(fwm:ReverseNatPort/fwm:SinglePort) > 0">
			<xsl:if test="count(fwm:ReverseNatIp/fwm:NatRecord)>0">
				<xsl:variable name="NatRecord" select="/fwm:policy/fwm:CommonLists/fwm:NatList[@Name=current()/fwm:ReverseNatIp/fwm:NatRecord/@NatRecordName]"/>
				<xsl:if test="count($NatRecord/fwm:ManyToOne)=0">
					<xsl:text>ERROR:Destination NAT Rule </xsl:text>
					<xsl:value-of select="@Name"/>
					<xsl:text> specifies a Translated Port and refers to a One to One NAT Configuration Item which is not permitted. When a Translated Port value is specified a Many To One NAT Config Item is all that can be used. Please adjust the Destination NAT Rule as needed.</xsl:text><xsl:text>&#10;</xsl:text>
				</xsl:if>
			</xsl:if>
		</xsl:if>
		<xsl:apply-templates>
			<xsl:with-param name="ruleEnabled" select="@Enabled"/>
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template match="NatList">
		<xsl:apply-templates>
			<xsl:with-param name="ruleEnabled" select="'true'"/>
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template match="fwm:DynamicAddressName">
	<!-- Verify that each dynamic address name referenced in the fwm policy is defined in the dynaddr policy -->
		<xsl:param name="ruleEnabled"/>
		<xsl:variable name="dynaddr" select="$dynaddrs[@Name=current()/@value]"/>
		<xsl:if test="count($dynaddr) = 0 and $ruleEnabled='true' ">
			<xsl:text>ERROR: Dynaddr </xsl:text>
			<xsl:value-of select="@value"/>
			<xsl:text> is referenced in the firewall policy but not defined in the dynamic address list policy.</xsl:text><xsl:text>&#10;</xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template match="fwm:AddressName">
	<!-- Verify that each Address name referenced in the fwm policy is defined in the network objects policy -->
		<xsl:param name="ruleEnabled"/>
		<xsl:variable name="addrname" select="$addrnames[@Name=current()/@value]"/>
		<xsl:if test="count($addrname) = 0 and $ruleEnabled='true' ">
			<xsl:text>ERROR: Address Name </xsl:text>
			<xsl:value-of select="@value"/>
			<xsl:text> is referenced in the firewall policy but not defined in the network objects policy.</xsl:text><xsl:text>&#10;</xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template match="fwm:AddressGroup">
	<!-- Verify that each Address Group referenced in the fwm policy is defined in the network objects policy -->
		<xsl:param name="ruleEnabled"/>
		<xsl:variable name="addrgroup" select="$addrgroups[@Name=current()/@value]"/>
		<xsl:if test="count($addrgroup) = 0 and $ruleEnabled='true' ">
			<xsl:text>ERROR: Address Group </xsl:text>
			<xsl:value-of select="@value"/>
			<xsl:text> is referenced in the firewall policy but not defined in the network objects policy.</xsl:text><xsl:text>&#10;</xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template match="fwm:PortName">
	<!-- Verify that each Port Name referenced in the fwm policy is defined in the network objects policy -->
		<xsl:param name="ruleEnabled"/>
		<xsl:variable name="portname" select="$portnames[@Name=current()/@value]"/>
		<xsl:if test="count($portname) = 0 and $ruleEnabled='true' ">
			<xsl:text>ERROR: Port Name </xsl:text>
			<xsl:value-of select="@value"/>
			<xsl:text> is referenced in the firewall policy but not defined in the network objects policy.</xsl:text><xsl:text>&#10;</xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template match="fwm:PortGroup">
	<!-- Verify that each Port Group referenced in the fwm policy is defined in the network objects policy -->
		<xsl:param name="ruleEnabled"/>
		<xsl:variable name="portgroup" select="$portgroups[@Name=current()/@value]"/>
		<xsl:if test="count($portgroup) = 0 and $ruleEnabled='true' ">
			<xsl:text>ERROR: Port Group </xsl:text>
			<xsl:value-of select="@value"/>
			<xsl:text> is referenced in the firewall policy but not defined in the network objects policy.</xsl:text><xsl:text>&#10;</xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template match="fwm:IkeConfig">
	<!-- <xsl:variable name="externalMatch" select="$externalinterfaces[@IpAddr=current()/@LocalIpAddress]"/>
		<xsl:variable name="internalMatch" select="$internalinterfaces[@IPAddress = current()/@LocalIpAddress]"/> 
		<xsl:if test="count($externalMatch) = 0 and count($internalMatch)=0"> -->
		<xsl:choose>
			<xsl:when test="$overrideLocal='true' and current()/@LocalIpAddress='0.0.0.0' "></xsl:when>
			<xsl:otherwise>
				<xsl:variable name="interfaceMatch" select="$interfaces[@ip = current()/@LocalIpAddress]"/> 
				<!-- Verify that the local ip address matches an ip address on the box. -->
				<xsl:if test="count($interfaceMatch) = 0"> 
					<xsl:text>ERROR:Security Gateway </xsl:text>
					<xsl:value-of select="../@Name"/>
					<xsl:text> contains a Local IP Address value that is not configured on any interface.</xsl:text><xsl:text>&#10;</xsl:text>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>	
		<!-- Verify that if localidtype is IPADDRESS that the localiddata matches an ip address on the box. -->
		<xsl:choose>
			<xsl:when test="current()/@LocalIdType='IPADDRESS' and $overrideLocal='true' and current()/@LocalIdData='0.0.0.0' "></xsl:when>
			<xsl:otherwise>
				<xsl:if test="current()/@LocalIdType='IPADDRESS' ">
					<xsl:variable name="idInterfaceMatch" select="$interfaces[@ip = current()/@LocalIdData]"/>
					<xsl:if test="count($idInterfaceMatch) = 0"> 
						<xsl:text>ERROR:Security Gateway </xsl:text>
						<xsl:value-of select="../@Name"/>
						<xsl:text> contains a Local Id Type value set to IP Address and a Local Id Data IP Address value that is not configured on any interface.</xsl:text><xsl:text>&#10;</xsl:text>
					</xsl:if>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>	
		<!-- Verify that the remote ip address only shows up once in all security gateways. -->
		<xsl:variable name="remoteMatch" select="/fwm:policy/fwm:SecurityGateways/fwm:AutoIkeSecurityGatewayList[fwm:IkeConfig/@RemoteIpAddress=current()/@RemoteIpAddress]"/>
		<xsl:variable name="remoteMatch2" select="/fwm:policy/fwm:SecurityGateways/fwm:VpnAddrIkeSecurityGatewayList[fwm:IkeConfig/@RemoteIpAddress=current()/@RemoteIpAddress]"/>
		<xsl:if test="count($remoteMatch) > 1">
			<xsl:text>ERROR:Security Gateway </xsl:text>
			<xsl:value-of select="$remoteMatch[1]/@Name"/>
			<xsl:text> contains a Remote IP Address value that is same as Remote IP Address as </xsl:text>
			<xsl:value-of select="$remoteMatch[2]/@Name"/>
			<xsl:text>&#10;</xsl:text>
		</xsl:if>
		<xsl:if test="count($remoteMatch2) > 1">
			<xsl:text>ERROR:Security Gateway </xsl:text>
			<xsl:value-of select="$remoteMatch2[1]/@Name"/>
			<xsl:text> contains a Remote IP Address value that is same as Remote IP Address as </xsl:text>
			<xsl:value-of select="$remoteMatch2[2]/@Name"/>
			<xsl:text>&#10;</xsl:text>
		</xsl:if>
		<xsl:if test="count($remoteMatch) = 1 and count($remoteMatch2) = 1 ">
			<xsl:text>ERROR:Security Gateway </xsl:text>
			<xsl:value-of select="$remoteMatch[1]/@Name"/>
			<xsl:text> contains a Remote IP Address value that is same as Remote IP Address as </xsl:text>
			<xsl:value-of select="$remoteMatch2[1]/@Name"/>
			<xsl:text>&#10;</xsl:text>
		</xsl:if>
		<!--Verify that no security gateway has an exchange type of Main Mode and a Remote IP Address of  0.0.0.0 -->
		<xsl:if test="@RemoteIpAddress='0.0.0.0' and @ExchangeType='MAINMODE' ">
			<xsl:text>ERROR:Security Gateway </xsl:text>
			<xsl:value-of select="../@Name"/>
			<xsl:text> contains a Remote IP Address of 0.0.0.0 and an ExchangeType of Main Mode.  Exchange Type can only be Aggressive Mode when Remote IP Address is 0.0.0.0.</xsl:text>
			<xsl:value-of select="$remoteMatch[2]/@Name"/>
			<xsl:text>&#10;</xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template match="netobj:DynamicAddressName">
	<!-- Verify that each dynamic address name referenced in the netobj policy is defined in the dynaddr policy -->
		<xsl:variable name="dynaddr" select="$dynaddrs[@Name=current()/@value]"/>
		<xsl:if test="count($dynaddr) = 0">
			<xsl:text>ERROR: Dynaddr </xsl:text>
			<xsl:value-of select="@value"/>
			<xsl:text> is referenced in the network objects policy but not defined in the dynamic address list policy.</xsl:text><xsl:text>&#10;</xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template match="fwm:L2TPIKEAttr">
		<!-- Verify that if localidtype is IPADDRESS that the localiddata matches the external ip address on the box. -->
		<xsl:choose>
			<xsl:when test="current()/@LocalIdType='IPADDRESS' and $overrideLocal='true' and current()/@LocalIdData='0.0.0.0' "></xsl:when>
			<xsl:otherwise>
				<xsl:if test="current()/@LocalIdType='IPADDRESS' ">
					<xsl:variable name="idInterfaceMatch" select="$interfaces[@ip = current()/@LocalIdData and @type='ext' ]"/>
					<xsl:if test="count($idInterfaceMatch) = 0"> 
						<xsl:text>ERROR:Security Gateway </xsl:text>
						<xsl:value-of select="../@Name"/>
						<xsl:text> contains a Local Id Type value set to IP Address and a Local Id Data IP Address value that is not configured on the external interface.</xsl:text><xsl:text>&#10;</xsl:text>
					</xsl:if>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>	
</xsl:stylesheet>
