<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:admin="http://www.iss.net/cml/NetworkProtector/admin" >
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<!-- Include the resolve common templates that handle the common elements first always because it contains a couple global variables. -->
	<xsl:include href="resolvecommon100.xsl" />
	<!-- Main Processing Template -->
	<xsl:template match="admin:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkProtector/admin">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<!-- 
		Templates for the parents of all elements containing a reference are handled here. .
		The parents need to apply templates twice, one using mode=resolve and one using the default mode.
		The first apply using mode=resolve populates attributes in the element with the resolved values.
		The second apply templates copies the actual child elements and their attributes.
		The name of the element must match the name of the attribute that is an attribute of the parent element 
        that contains the reference element.
	-->
	<xsl:template match="admin:Manager">
		<xsl:element name="Manager" namespace="http://www.iss.net/cml/NetworkProtector/admin">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates mode="resolve"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<!-- 
		Any element that contains an element that needs resolving is handled here.
		These templates only handle mode=resolve.  This mode avoids putting the element tag out here so that the 
		Templates that do the resolving below simply output the attributes to the current parent being processed.
	-->
	<xsl:template match="admin:Manager/admin:IPAddr" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
	<xsl:template match="admin:Manager/admin:IPRange" mode="resolve">
		<xsl:apply-templates mode="resolve"/>
	</xsl:template>
</xsl:stylesheet>
