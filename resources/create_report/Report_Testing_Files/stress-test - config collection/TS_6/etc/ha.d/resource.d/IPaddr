#!/bin/sh
#
#	$Id: IPaddr.in,v 1.52.2.6 2004/05/12 01:38:24 alan Exp $
#
#	This script manages IP alias IP addresses
#
#	It can add an IP alias, or remove one.
#
#	usage: $0 ip-address {start|stop|status|monitor}
#
#	The "start" arg adds an IP alias.
#
#	Surprisingly, the "stop" arg removes one.	:-)
#
#

unset LC_ALL; export LC_ALL # Make ifconfig work in France for David Jules :-)
unset LANGUAGE; export LANGUAGE # Make ifconfig work in France for Fabrice :-)
#	make ifconfig work in Austria for Gregor G�stl
#	I have no idea why the previous fix didn't fix it for him.
LC_MESSAGES=C
export LC_MESSAGES

prefix=/usr
exec_prefix=/usr
HA_D=/etc/ha.d
CONF_D=/etc/ha.d/conf
. ${HA_D}/shellfuncs

IFCONFIG=/sbin/ifconfig
IFCONFIG_A_OPT=
VARLIB=/var/lib/heartbeat
VLDIR=$VARLIB/rsctmp/IPaddr
SENDARPPIDDIR=$VARLIB/rsctmp/send_arp
ROUTE=/sbin/route
SENDARP=$HA_BIN/send_arp
FINDIF=$HA_BIN/findif
USAGE="usage: $0 ip-address {start|stop|status|monitor}";

#
#	Find out which alias serves the given IP address
#	The argument is an IP address, and its output
#	is an aliased interface name (e.g., "eth0:0").
#
find_interface_generic() {

  ipaddr="$1";

  $IFCONFIG $IFCONFIG_A_OPT  |
  while read ifname linkstuff
  do
    : Read gave us ifname = $ifname

    read inet addr junk
    : Read gave us inet = $inet addr = $addr

    while
      read line && [ "X$line" != "X" ]
    do
      : Nothing
    done


    : "comparing $ipaddr to $addr (from ifconfig)"
    case $addr in
      addr:$ipaddr)	echo $ifname; return 0;;
      $ipaddr)	echo $ifname; return 0;;
    esac
    continue

  done
  return 1
}

#
#       Find out which alias serves the given IP address
#       The argument is an IP address, and its output
#       is an aliased interface name (e.g., "eth0:0").
#
find_interface() {
  IF=`find_interface_generic $BASEIP`
  echo $IF
  return 0;
}

#
# This routine should handle any type of interface, but has only been
# tested on ethernet-type NICs.
#
ifconfig2sendarp() {
	echo "$1" | sed "s%:%%g"
}


delete_route () {
  ipaddr="$1"

  CMD="$ROUTE -n del -host $ipaddr"

  ha_log "info: Running $CMD"
  $CMD
  rc=$?
  [ $rc -eq 0 ] || ha_log "info: Returned $rc"

  return $rc
}

delete_interface () {
  ipaddr="$1"
  ifname="$2"
  DEV=`echo $ifname | awk -F: '{print $1}'`

  CMD="/sbin/ip addr flush dev $DEV label $ifname"

  ha_log "info: Running $CMD"
  $CMD
  rc=$?
  [ $rc -eq 0 ] || ha_log "info: Returned $rc"

  ( . /etc/sysconfig/network
    DefRoute=`/sbin/ip route list | grep '^default'`
    if [ -z "$DefRoute" -a ! -z "$GATEWAY" ] ; then
      /sbin/route add -net default gw $GATEWAY 2>/dev/null
    fi
  )
  return $rc
}


add_interface () {
  ipaddr="$1"
  ifinfo="$2"
  iface="$3"

  #
  #	On Linux the Alias is named ethx:y
  #	This will remove the "extra" interface Data 
  #	leaving us with just ethx
  #
  IFEXTRA=`echo "$ifinfo" | cut -f2-`
  NETMASK=`echo "$ifinfo" | awk '{print $3'}`
  eval `/bin/ipcalc --prefix $ipaddr $NETMASK`
  eval `/bin/ipcalc --network $ipaddr $NETMASK`
  BROADCAST=`echo "$ifinfo" | awk '{print $5'}`
  DEV=`echo $iface | awk -F: '{print $1}'`

  CMD="/sbin/ip addr add $ipaddr/$PREFIX brd $BROADCAST dev $DEV label $iface"

  ha_log "info: Running $CMD"
  $CMD
  rc=$?
  [ $rc -eq 0 ] || ha_log "info: Returned $rc"

  case $rc in
    0)
    		;;
    *)
    		echo "ERROR: $CMD failed."
		;;
  esac

  /sbin/route add -net $NETWORK/$PREFIX dev $iface

  if [ -f /etc/sysconfig/static-routes ]; then
    egrep "^any |^${DEV} " /etc/sysconfig/static-routes | \
      while read ignore args ; do
      /sbin/route add -$args dev $iface 2>/dev/null
    done
  fi

  (  . /etc/sysconfig/network
     . /etc/sysconfig/hamroute
     if [ ! -z "$VIRTUALGATEWAY" ] ; then
       eval `echo -n VGW ; ipcalc --network $VIRTUALGATEWAY $NETMASK`
       if [ "$NETWORK" == "$VGWNETWORK" ] ; then
         DefRoute=`/sbin/ip route list | grep '^default'`
         while [ ! -z "$DefRoute" ] ; do
           /sbin/route delete -net default
           DefRoute=`/sbin/ip route list | grep '^default'`
         done
         /sbin/route add -net default gw $VIRTUALGATEWAY dev $iface 2>/dev/null
       fi
     elif [ ! -z "$GATEWAY" ] ; then
       eval `echo -n GW ; ipcalc --network $GATEWAY $NETMASK`
       if [ "$NETWORK" == "$GWNETWORK" ] ; then
         DefRoute=`/sbin/ip route list | grep '^default'`
         while [ ! -z "$DefRoute" ] ; do
           /sbin/route delete -net default
           DefRoute=`/sbin/ip route list | grep '^default'`
         done
         /sbin/route add -net default gw $GATEWAY dev $iface 2>/dev/null
       fi
     fi
  )

  return $rc
}

#      On Linux systems the (hidden) loopback interface may
#      conflict with the requested IP address. If so, this
#      unoriginal code will remove the offending loopback address
#      and save it in VLDIR so it can be added back in later
#      when the IPaddr is released.
#
remove_conflicting_loopback() {
	ipaddr="$1"
	ifname="$2"

	ha_log "info: Removing conflicting loopback $ifname."
	if
	  [ -d "$VLDIR/" ] || mkdir -p "$VLDIR/"
	then
	  : Directory $VLDIR now exists
	else
	  ha_log "ERROR: Could not create \"$VLDIR/\" conflicting" \
	  " loopback $ifname cannot be restored."
	fi
	if 
	  echo $ifname > "$VLDIR/$ipaddr"
	then
	  : Saved loopback information in $VLDIR/$ipaddr
	else
	  ha_log "ERROR: Could not save conflicting loopback $ifname." \
	  "it will not be restored."
	fi
	delete_interface "$ipaddr" "$ifname"
	# Forcibly remove the route (if it exists) to the loopback.
	delete_route "$ipaddr"
}

#      On Linux systems the (hidden) loopback interface may
#      need to be restored if it has been taken down previously
#      by remove_conflicting_loopback()
#
restore_loopback() {
	ipaddr="$1"

	if [ -s "$VLDIR/$ipaddr" ]; then
		ifname=`cat "$VLDIR/$ipaddr"`
		ha_log "info: Restoring loopback IP Address " \
			"$ipaddr on $ifname."
		add_interface "$ipaddr" "netmask 255.255.255.255" "$ifname"
		#add_route "$ipaddr" "$ifname"
		rm -f "$VLDIR/$ipaddr"
	fi
}

#
#	Remove the IP alias for the requested IP address...
#
ip_stop() {
  BASEIP=`echo $1 | sed s'%/.*%%'`
  SENDARPPIDFILE="$SENDARPPIDDIR/send_arp-$BASEIP"
  IF=`find_interface $BASEIP`
  case $IF in
    *:*)	;;
      ?*)	ha_log "ERROR: non-aliased Interface [$IF] is active. Nothing stopped."
		exit 0;; # This is to keep the machine from rebooting...
  esac

  if test -f "$SENDARPPIDFILE"
  then
  	cat "$SENDARPPIDFILE" | xargs kill
	rm -f "$SENDARPPIDFILE"
  fi


  if [ -z "$IF" ]; then
    : Requested interface not in use
    exit 0
  else
    case $IF in
      lo*)
        : Requested interface is on loopback
        exit 0
        ;;
    esac
  fi

  if
    [ -x $HA_RCDIR/local_giveip ]
  then
    $HA_RCDIR/local_giveip $*
  fi

  ha_log "info: Disabling IP forwading"
  sysctl -w net.ipv4.ip_forward=0 >/dev/null

  #delete_route "$BASEIP"
  delete_interface "$BASEIP" "$IF"
  rc=$?

  restore_loopback "$BASEIP"
  # remove lock file...
  rm -f "$VLDIR/$IF"

  case $rc in
    	0) 
		ha_log "info: IP Address $BASEIP released"
		;;
    	*) 	
		ha_log "WARN: IP Address $BASEIP NOT released"
		;;
  esac
  return $rc
}


#
#	Find an unused interface/alias name for us to use for new IP alias
#	The argument is an IP address, and the output
#	is an aliased interface name (e.g., "eth0:0", "dc0", "le0:0").
#
find_free_interface() {
  if
    [ ! -d $VLDIR ]
  then
    mkdir -p $VLDIR
  fi

  BASEIP=`echo $1 | sed s'%/.*%%'`
  if
    NICINFO=`$FINDIF $1`
  then
    : OK
  else
    lrc=$?
    ha_log "ERROR: unable to find an interface for $BASEIP"
    return $lrc
  fi

  nicname=`echo "$NICINFO" | cut -f1`
  nicinfo=`echo "$NICINFO" | cut -f2-`
  if
    [ "X$nicname" = "X" ]
  then
    ha_log "ERROR: no interface found for $BASEIP"
    return 1;
  fi

  NICBASE="$VLDIR/$nicname"
  touch "$NICBASE"

  IFLIST=`$IFCONFIG $IFCONFIG_A_OPT | \
    grep "^$nicname:[0-9]" | sed 's% .*%%'`

  IFLIST=" `echo $IFLIST` "

  j=0
  TRYADRCNT=`ls "${NICBASE}:"* | wc -l | tr -d ' ' 2>/dev/null`
  if 
    [ -f "${NICBASE}:${TRYADRCNT}" ]
  then
    : OK
  else
    j="${TRYADRCNT}"
  fi

  while
    [ $j -lt 512 ]
  do
    case $IFLIST in
      *" "$nicname:$j" "*)	;;
      *)			
        NICLINK="$NICBASE:$j"
        if
          ln "$NICBASE" "$NICLINK" 2>/dev/null
        then
          echo "$nicname:$j	$nicinfo"
          return 0
        fi;;
    esac
    j=`expr $j + 1`
  done
  return 1
}


#
#	Add an IP alias for the requested IP address...
#
#	It could be that we already have taken it, in which case it should
#	do nothing.
#

ip_start() {
  #
  #	Do we already service this IP address?
  #
  case `ip_status $1` in
    *unning*)   exit 0;
  esac

  BASEIP=`echo $1 | sed s'%/.*%%'`
  SENDARPPIDFILE="$SENDARPPIDDIR/send_arp-$BASEIP"

  CURRENTIF=`find_interface "$BASEIP"`
  case $CURRENTIF in
    lo*)
        remove_conflicting_loopback "$BASEIP" "$CURRENTIF"
        ;;
    *:*)	;;
    ?*)	ha_log "ERROR: non-aliased Interface [$CURRENTIF] is active"
        exit 1
        ;;
  esac

  if IFINFO=`find_free_interface $1`; then
  	: OK got interface [$IFINFO] for $1
  else
  	exit 1
  fi
  IF=`echo "$IFINFO" | cut -f1`

  if
    [ -x $HA_RCDIR/local_takeip ]
  then
    $HA_RCDIR/local_takeip $*
  fi

  ha_log "info: Disabling IP forwading"
  sysctl -w net.ipv4.ip_forward=0 >/dev/null

  add_interface "$BASEIP" "$IFINFO" "$IF"
  rc=$?
  case $rc in
    0)
    		;;
    *)
		return $rc
		;;
  esac

  # add_route $BASEIP $IF

  TARGET_INTERFACE=`echo $IF | sed 's%:.*%%'`

  ha_log "info: Sending Gratuitous Arp for $BASEIP on $IF [$TARGET_INTERFACE]"

  [ -r ${CONF_D}/arp_config ] && . ${CONF_D}/arp_config
  [ -r "${CONF_D}/arp_config:${TARGET_INTERFACE}" ] && . "${CONF_D}/arp_config:${TARGET_INTERFACE}"

  # Set default values (can be overridden as described above)

  : ${ARP_INTERVAL_MS=1010}	# milliseconds between ARPs
  : ${ARP_REPEAT=5}		# repeat count
  : ${ARP_BACKGROUND=yes}	# no to run in foreground (no longer any reason to do this)
  : ${ARP_NETMASK=ffffffffffff}	# netmask for ARP
  

  ARGS="-i $ARP_INTERVAL_MS -r $ARP_REPEAT -p $SENDARPPIDFILE $TARGET_INTERFACE $BASEIP auto $BASEIP $ARP_NETMASK"
  ha_log "$SENDARP $ARGS"
  case $ARP_BACKGROUND in
    yes) ($SENDARP $ARGS || ha_log "ERROR: Could not send gratuitous arps")&;;
    *)	  $SENDARP $ARGS || ha_log "ERROR: Could not send gratuitous arps";;
  esac
}

ip_status() {
  BASEIP=`echo $1 | sed -e s'%/.*%%'`
  IF=`find_interface $BASEIP`

  if
    [ -z "$IF" ]
  then
    echo "stopped"; return 3
  else
    case $IF in
      lo*)
          echo "loopback"
          ;;
      *)
          echo "running"
          ;;
    esac
  fi
}

#
#	Determine if this IP address is really being served, or not.
#	Note that we don't distinguish if *we're* serving it locally...
#
ip_monitor() {
  BASEIP=`echo $1 | sed s'%/.*%%'`
  TIMEOUT=1 # seconds
  # -c count -t timetolive -q(uiet) -n(umeric) -W timeout
  PINGARGS="-c 1 -q -n -W $TIMEOUT $BASEIP"
  for j in 1 2 3
  do
    if
      /bin/ping $PINGARGS >/dev/null 2>&1
    then
      echo "OK"
      return 0
    fi
  done
  echo "down"
  return 1
}

usage() {
  echo $USAGE >&2
  echo "$Id: IPaddr.in,v 1.52.2.6 2004/05/12 01:38:24 alan Exp $"
}

#
#	Add or remove IP alias for the given IP address...
#

if
  [ $# -eq 1 ]
then
  case $1 in
    info)	cat <<-!INFO
	Abstract=IP address takeover
	Argument=IP address OR IP address/broadcast address OR IP address/broadcast address/netmaskbits
	Description:
	An IPaddr resource is an IP address which is to be taken over by \\
	the owning node.  An argument is required, and is of this form:
	    nnn.nnn.nnn.nnn/bbb.bbb.bbb.bbb
	Where nnn.nnn.nnn.nnn is the IP address to be taken over, and\\
	bbb.bbb.bbb.bbb is the broadcast address to be used with this address.

	Since IPaddr is the "default" resource type, it is not necessary\\
	to prefix the IP address by "IPaddr::".
	This allows IPaddr::192.2.4.63 to be abbreviated as 192.2.4.63.
	!INFO
	exit 0;;
  esac
fi
if
  [ $# -ne 2 ]
then
  usage
  exit 1
fi

case $2 in
  start)	ip_start $1;;
  stop)		ip_stop $1;;
  status)	ip_status $1;;
  monitor)	ip_monitor $1;;
  *)		usage
 		exit 1
		;;
esac

