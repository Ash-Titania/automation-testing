#!/bin/sh
#
# $Id: issospfd.in,v 1.00 2005/01/20 17:30:14 james Exp $
# 
# issospfd
#      Description: Manages ISS ospfd service
#  Original Author: James Hunter (jhunter@iss.net)
# Original Release: 20 Jan 2005
#          Support: support@iss.net
#
# usage: ./issospfd {start|stop|status}
#
# An example usage in /etc/ha.d/haresources: 
#       node1  issospfd
#
unset LC_ALL; export LC_ALL
unset LANGUAGE; export LANGUAGE

prefix=/usr
exec_prefix=/usr
. /etc/ha.d/shellfuncs

UTIL=/sbin/service
CONFIGFILE=/etc/quagga/ospfd.conf

check_util () {
    if [ ! -x "$1" ] ; then
        ha_log "error: setup problem: Couldn't find utility $1"
        exit 1
    fi
}

usage() {
cat <<EOT
    usage: $0 {start|stop|status}

    $Id: issospfd.in,v 1.00 2005/01/20 17:30:14 james Exp $
EOT
}

# Check to make sure the utility is found
check_util $UTIL
operation=$1

# Look for the 'start', 'stop' or 'status' argument
case "$operation" in

#
# START: start ospfd service
#
start)

    if
        test -f $CONFIGFILE
    then
        $UTIL ospfd status >/dev/null
        if
            test $? -ne 0
        then
            $UTIL ospfd start >/dev/null
            rc=$?
            if
                test $rc -ne 0
            then
                ha_log "error: ospfd failed to start, error=$rc"
                exit 1
            fi
        fi
        ha_log "info: ospfd is started" 2>/dev/null
    else
        ha_log "info: ospfd is not configured (start)" 2>/dev/null
    fi

# end of start)
;;

#
# STOP: stop ospfd service
#
stop)

    if
        test -f $CONFIGFILE
    then
        if
            $UTIL ospfd status >/dev/null
        then
            $UTIL ospfd stop >/dev/null
            rc=$?
            if
                test $rc -ne 0
            then
                ha_log "error: ospfd failed to stop, error=$rc"
                exit 1
            fi
        fi
        ha_log "info: ospfd is stopped" 2>/dev/null
    else
        ha_log "info: ospfd is not configured (stop)" 2>/dev/null
    fi

# end of stop)
;;

#
# STATUS: return ospfd service state {running | stopped}
#
status)

    if
        test -f $CONFIGFILE
    then
        if
            $UTIL ospfd status >/dev/null
        then
            ha_log "info: ospfd status is running" 2>/dev/null
            echo "running"
        else
            ha_log "info: ospfd status is stopped" 2>/dev/null
            echo "stopped" ; exit 3
        fi
    else
        ha_log "info: ospfd status is not configured (status)" 2>/dev/null
        echo "stopped" ; exit 3
    fi

# end of status)
;;

*)
    echo "This script should be run with an argument of 'start', 'stop', or 'status'"
    usage
    exit 1
;;

esac

# If you got to this point, chances are everything is O.K.
exit 0;

