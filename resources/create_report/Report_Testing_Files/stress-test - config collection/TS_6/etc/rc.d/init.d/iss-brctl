#!/bin/sh
#
# chkconfig: 2345 96 89
# description: igateway firewall bridge utility
#
# Copyright (C) 2003 Internet Security Systems, Inc


CONFIG=/etc/sysconfig/bridge

# Source function library.
. /etc/init.d/functions

RETVAL=0

start() {

    test -r "$CONFIG" && . "$CONFIG"

    reload
    reloadstp
    ifconfig bridge0 $IPADDR netmask $NETMASK up
    route add default gw $GATEWAY
    /etc/sysconfig/network-scripts/ifup-routes bridge0



    [ $RETVAL -eq 0 ] && touch /var/lock/subsys/bridge
    echo
}

stop() {

    ifconfig bridge0 down
    /sbin/iss-brctl -destroy bridge0

    [ $RETVAL -eq 0 ] && rm -f /var/lock/subsys/bridge
    echo
}

restart() {
    stop
    start
    RETVAL=$?
}

reloadstp() {
	logger -t iss-brctl "Reloading bridge stp configuration"

	if [ -f /igateway/policy/logbrctlstp ]; then
		mv -f /igateway/policy/logbrctlstp /igateway/policy/logbrctlstp.bak
	fi

	if [ -f /igateway/policy/network/npnetwork1_0.xml ]; then
		/sbin/iss-brctl -d=2 -l=/igateway/policy/logbrctlstp -xmlstp /igateway/policy/network/npnetwork1_0.xml

		if grep ERROR /igateway/policy/logbrctlstp >/dev/null 2>&1 ; then
	                logger -t iss-brctl "Reload failed"
	                RETVAL=1
	        else
	                logger -t iss-brctl "Reload succeeded"
	                RETVAL=0
	        fi
	fi

	return $RETVAL
}

reload() {

	logger -t iss-brctl "Reloading bridge configuration"

	if [ -f /igateway/policy/logbrctl ]; then
		mv -f /igateway/policy/logbrctl /igateway/policy/logbrctl.bak
	fi

	if [ -f /igateway/policy/network/npnetwork1_0.xml ]; then
		/sbin/iss-brctl -d=2 -l=/igateway/policy/logbrctl -xml /igateway/policy/network/npnetwork1_0.xml

		if grep ERROR /igateway/policy/logbrctl >/dev/null 2>&1 ; then
	                logger -t iss-brctl "Reload failed"
	                RETVAL=1
	        else
	                logger -t iss-brctl "Reload succeeded"
	                RETVAL=0
	        fi
	fi

	return $RETVAL
}

condrestart() {
    [ -e /var/lock/subsys/bridge ] && restart || :
}

# See how we were called.
case "$1" in
start)
    start
    ;;
stop)
    stop
    ;;
status)
    dostatus
    ;;
restart)
    restart
    ;;
reload)
    reload
    ;;
reloadstp)
    reloadstp
    ;;
condrestart)
    condrestart
    ;;
*)
    echo "Usage: `basename $0` {start|stop|status|restart|reload|reloadstp|condrestart}"
    exit 1
esac

exit $RETVAL
