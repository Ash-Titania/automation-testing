#!/bin/sh
#
# Init Script for IPMI Kernel Modules
# Copyright(c) 2003-2006 Internet Security Systems, Inc. All rights reserved.
#
# chkconfig: 345 02 98
# description: IPMI Kernel Module loader

PATH=/sbin:/usr/sbin:/bin:/usr/bin

# Source function library
if [ -f /etc/init.d/functions ]; then
  . /etc/init.d/functions
elif [ -f /etc/rc.d/init.d/functions ] ; then
  . /etc/rc.d/init.d/functions
else
  exit 0
fi

module_dev=ipmi_devintf
module_msg=ipmi_msghandler

MODCONF=/etc/modules.conf
[ -f /etc/modprobe.conf ] && MODCONF=/etc/modprobe.conf

IPMI_DRV=`grep ipmi_adapter $MODCONF | awk '{print $3}'`
IPMI_I2CDRV=`grep i2c_adapter $MODCONF | awk '{print $3}'`

startmodule() {
  /sbin/lsmod | grep "^${1} " >/dev/null
  if [ $? -ne 0 ] ; then
    echo -n $"Starting IPMI module ${1}: "
    /sbin/modprobe ${@} && success || failure
    echo
  fi
}

start() {
    RETVAL=0
    echo $"Starting IPMI driver: "
    startmodule $module_msg || RETVAL=1
    startmodule $module_dev || RETVAL=1
    if [ ! -z "$IPMI_I2CDRV" ] ; then
        startmodule $IPMI_I2CDRV || RETVAL=1
    fi
    startmodule ipmi_adapter || RETVAL=1

    [ $RETVAL -eq 0 ] && touch /var/lock/subsys/iss-ipmi
}

stopmodule() {
  /sbin/lsmod | grep "^${1} " >/dev/null
  if [ $? -eq 0 ] ; then
    echo -n $"Removing IPMI module ${1}: "
    /sbin/rmmod ${1} && success || failure
    echo
  fi
}

stop() {
    RETVAL=0
    echo $"Stopping IPMI driver: "
    stopmodule $IPMI_DRV || RETVAL=1
    stopmodule $module_dev || RETVAL=1
    stopmodule $module_msg || RETVAL=1
    if [ ! -z "$IPMI_I2CDRV" ] ; then
        stopmodule $IPMI_I2CDRV || RETVAL=1
    fi
    [ $RETVAL -eq 0 ] && rm -f /var/lock/subsys/iss-ipmi
}

RETVAL=0
case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    restart)
        stop
        start
        ;;
    reload|condrestart)
        if [ -f /proc/ipmi/0/type ] ; then
            stop
            start
        fi
        ;;
    status)
        [ -f /proc/ipmi/0/type ]
        RETVAL=$?
        if [ $RETVAL = 0 ] ; then
            echo $"IPMI is running"
        else
            echo $"IPMI is stopped"
        fi
        ;;
    *)
        echo IPMI Kernel Module init script
        echo Copyright\(c\) 2003-2006 Internet Security Systems, Inc. All rights reserved.
        echo Usage: `basename $0` '[start|stop|restart|condrestart|reload|status]'
        RETVAL=1
        ;;
esac

exit $RETVAL
