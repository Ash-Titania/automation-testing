#!/bin/bash
#
# ntpd		This shell script takes care of starting and stopping
#		ntpd (NTPv4 daemon).
#
# chkconfig: - 98 74
# description: ntpd is the NTPv4 daemon. \
# The Network Time Protocol (NTP) is used to synchronize the time of \
# a computer client or server to another server or reference time source, \
# such as a radio or satellite receiver or modem.

# Source function library.
. /etc/init.d/functions

# Source networking configuration.
. /etc/sysconfig/network

if [ -f /etc/sysconfig/ntpd ];then
        . /etc/sysconfig/ntpd
fi

# Check that networking is up.
[ ${NETWORKING} = "no" ] && exit 0

ntpconf=/etc/ntp.conf
ntpstep=/etc/ntp/step-tickers

[ -x /usr/sbin/ntpd -a -f $ntpconf ] || exit 0

RETVAL=0
prog="ntpd"

FWCHAIN="RH-Firewall-1-INPUT"

# Is there a firewall running, and does it look like one we configured?
FWACTIVE=''
if [ -f /proc/net/ip_tables_names ]; then
	if iptables -L -n 2>/dev/null | grep -q $FWCHAIN ; then
		FWACTIVE=1
	fi
fi

start() {
	# get the servers from step-ticker
        tickers=''
        if [ -s "$ntpstep" ]; then
	    tickers=`/bin/sed -e 's/\#.*$//g' $ntpstep`
        fi
	timeservers=`/bin/awk '$1=="peer"||$1=="server"{print $2}' $ntpconf`

	# check for -x
	OPTIND=0
	dostep=''
	dropstr=''
	while getopts ":aAbc:dD:f:gk:l:LmnN:p:P:qr:s:t:v:V:xU:T:" args $OPTIONS;
	do 
	  if [ "$args" = "x" ]; then
	      dostep='yes' 
	  fi
          if [ "$args" = "U" ]; then
	      dropstr="-U $OPTARG"
          fi

	done
	OPTIND=0

        # Open the firewall for ntp
	if [ -n "$FWACTIVE" -a "$FIREWALL_MODS" != "no" ]; then
	  for server in `echo $tickers $timeservers | tr ' ' '\n' | sort -u`; do
	    echo -n $"$prog: Opening firewall for input from $server port 123"
	    iptables -I $FWCHAIN -m udp -p udp -s $server/32  \
		--sport 123 -d 0/0 --dport 123 -j ACCEPT \
		&& success || failure
	    echo
	  done
	fi

        if [ -z "$tickers" ]; then
	    tickers=$timeservers
        fi

	if [ -s "$ntpstep" -o -n "$dostep" ]; then
	    # Synchronize with servers if step-tickers exists
	    # or the -x option is used
	    echo -n $"$prog: Synchronizing with time server: "
	    /usr/sbin/ntpdate $dropstr -s -b -p 8 $tickers 2>/dev/null >/dev/null
	    RETVAL=$?
	    [ $RETVAL -eq 0 ] && success || failure
	    echo
	    if [ $RETVAL -ne 0 ]; then
	            OPTIONS="$OPTIONS -g"
	    fi
	else
	    # -g can replace the grep for time servers
	    # as it permits ntpd to violate its 1000s limit once.
	    OPTIONS="$OPTIONS -g"
	fi
        # Start daemons.
        echo -n $"Starting $prog: "
        daemon ntpd $OPTIONS
	RETVAL=$?
        echo
        [ $RETVAL -eq 0 ] && touch /var/lock/subsys/ntpd
	return $RETVAL
}

stop() {
        # Stop daemons.

	# get the servers from step-ticker
        tickers=''
        if [ -s "$ntpstep" ]; then
	    tickers=`/bin/sed -e 's/\#.*$//g' $ntpstep`
        fi
        timeservers=`/usr/bin/awk '$1=="peer"||$1=="server"{print $2}' $ntpconf`

        # Remove the firewall opening for ntp
	if [ -n "$FWACTIVE" -a "$FIREWALL_MODS" != "no" ]; then
	  for server in `echo $tickers $timeservers | tr ' ' '\n' | sort -u`; do
	    echo -n $"$prog: Removing firewall opening for $server port 123"
	    iptables -D $FWCHAIN -m udp -p udp -s $server/32  --sport 123 -d 0/0 --dport 123 -j ACCEPT \
		&& success || failure
	    echo
	  done
	fi
        echo -n $"Shutting down $prog: "
	killproc ntpd
	RETVAL=$?
        echo
        [ $RETVAL -eq 0 ] && rm -f /var/lock/subsys/ntpd
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
        ;;
  stop)
	stop
        ;;
  status)
	status ntpd
	RETVAL=$?
	;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  condrestart)
	if [ -f /var/lock/subsys/ntpd ]; then
	    stop
	    start
	    RETVAL=$?
	fi
	;;
  *)
        echo $"Usage: $0 {start|stop|restart|condrestart|status}"
        exit 1
esac

exit $RETVAL
