
BH-X3:3#
BH-X3:3# show config
Preparing to Display Configuration...
#
# THU JAN 24 13:19:28 2013 UTC
# box type             : Passport-8003
# software version     : 3.5.1.0
# monitor version      : 3.5.1.0/000
#

#
# Asic Info :
# SlotNum|Name  |CardType   |MdaType |Parts Description
#
# Slot  1 8632TXE  0x20210120 0x00000000   IOM: PLRO=3  GMAC=5  BFM: OP=3 TMUX=2 RARU=4 CPLD=5
# Slot  2 8608GBE  0x20325108 0x00000000   IOM: GMAC=5  BFM: OP=3 TMUX=2 RARU=4 CPLD=9
# Slot  3 8691SF   0x200e0100 0x00000000  CPU: CPLD=19 SFM: OP=3 TMUX=2 SWIP=3 FAD=1 CF=16
#
#!flags m-mode false
#!flags enhanced-operational-mode false
#!flags vlan-optimization-mode false
#!record-reservation filter 4096
#!record-reservation ipmc 500
#!record-reservation local 2000
#!record-reservation mac 2000
#!record-reservation static-route 200

#!record-reservation vrrp 500

#!end
#
config
mac-flap-time-limit 500

#
# CLI CONFIGURATION
#

cli prompt "BH-X3"

#
# SYSTEM CONFIGURATION
#

sys set contact "NMC x55800"
sys set location "BH - CAA1 - Area 3"
sys set topology off
sys set snmp trap-recv 10.10.10.18 v1 nicam
sys set snmp trap-recv 10.170.24.199 v1 public
sys set snmp trap-recv 10.170.29.6 v1 nicam
sys set snmp trap-recv 132.185.2.6 v1 nicam


#
# LOG CONFIGURATION
#


#
# LINK-FLAP-DETECT CONFIGURATION
#


#
# IEEE VLAN AGING CONFIGURATION
#


#
# ACCESS-POLICY CONFIGURATION
#

sys access-policy policy 1 accesslevel rwa
sys access-policy policy 1 name "Naran WC"
sys access-policy policy 1 precedence 1
sys access-policy policy 1 username ""

sys access-policy policy 1 network 10.164.36.36/255.255.255.255
sys access-policy policy 1 service http disable
sys access-policy policy 1 service telnet disable
sys access-policy policy 3 create
sys access-policy policy 3 name "NTS TVC"
sys access-policy policy 3 precedence 1
sys access-policy policy 3 username ""
sys access-policy policy 3 network 132.185.2.0/255.255.255.0
sys access-policy policy 3 service http enable
sys access-policy policy 3 service rlogin enable
sys access-policy policy 3 service telnet enable
sys access-policy policy 3 service tftp enable
sys access-policy policy 3 service ftp enable
sys access-policy policy 4 create
sys access-policy policy 4 name "TECH spec"
sys access-policy policy 4 precedence 1
sys access-policy policy 4 username ""
sys access-policy policy 4 network 10.54.7.0/255.255.255.0
sys access-policy policy 4 service http enable
sys access-policy policy 4 service rlogin enable
sys access-policy policy 4 service telnet enable
sys access-policy policy 4 service tftp enable
sys access-policy policy 4 service ftp enable

sys access-policy policy 5 create
sys access-policy policy 5 name "Keki (home)"
sys access-policy policy 5 precedence 1
sys access-policy policy 5 username ""
sys access-policy policy 5 network 10.182.232.1/255.255.255.255
sys access-policy policy 5 service http enable
sys access-policy policy 5 service rlogin enable
sys access-policy policy 5 service telnet enable
sys access-policy policy 5 service tftp enable
sys access-policy policy 5 service ftp enable
sys access-policy policy 6 create
sys access-policy policy 6 name "Reithmon TVC"
sys access-policy policy 6 precedence 1
sys access-policy policy 6 username ""
sys access-policy policy 6 network 10.176.2.5/255.255.255.255
sys access-policy policy 6 service http enable
sys access-policy policy 6 service rlogin enable
sys access-policy policy 6 service telnet enable
sys access-policy policy 6 service tftp enable
sys access-policy policy 6 service ftp enable
sys access-policy policy 7 create
sys access-policy policy 7 name "MRTG"
sys access-policy policy 7 precedence 1

sys access-policy policy 7 username ""
sys access-policy policy 7 network 10.48.2.11/255.255.255.255
sys access-policy policy 8 create
sys access-policy policy 8 name "Ehealth"
sys access-policy policy 8 precedence 1
sys access-policy policy 8 username ""
sys access-policy policy 8 network 10.48.2.15/255.255.255.255
sys access-policy policy 9 create
sys access-policy policy 9 name "MRTG2"
sys access-policy policy 9 precedence 1
sys access-policy policy 9 username ""
sys access-policy policy 9 network 10.180.162.43/255.255.255.255
sys access-policy policy 11 create
sys access-policy policy 11 name "NTS"
sys access-policy policy 11 precedence 1
sys access-policy policy 11 username ""
sys access-policy policy 11 network 10.170.24.0/255.255.248.0
sys access-policy policy 11 service http enable
sys access-policy policy 11 service rlogin enable
sys access-policy policy 11 service telnet enable
sys access-policy policy 11 service tftp enable
sys access-policy policy 11 service ftp enable
sys access-policy policy 15 create

sys access-policy policy 15 name "HPOV"
sys access-policy policy 15 username ""
sys access-policy policy 15 network 10.10.10.16/255.255.255.240
sys access-policy policy 16 create
sys access-policy policy 16 name "BH eHealth"
sys access-policy policy 16 username ""
sys access-policy policy 16 network 10.52.69.33/255.255.255.255
sys access-policy policy 17 create
sys access-policy policy 17 name "BH MRTG"
sys access-policy policy 17 username ""
sys access-policy policy 17 network 10.52.69.39/255.255.255.255
sys access-policy policy 19 create
sys access-policy policy 19 name "SBS-TS"
sys access-policy policy 19 precedence 1
sys access-policy policy 19 username ""
sys access-policy policy 19 network 10.199.32.0/255.255.255.0
#
# SSH CONFIGURATION
#


#
# MCAST SOFTWARE FORWARDING CONFIGURATION

#


#
# SNMP V3 GROUP MEMBERSHIP CONFIGURATION
#


#
# SNMP V3 GROUP ACCESS CONFIGURATION
#


#
# SNMP V3 MIB VIEW CONFIGURATION
#


#
# SNMP V3 COMMUNITY TABLE CONFIGURATION
#



#
# SLOT CONFIGURATION
#


#
# WEB CONFIGURATION
#


#
# RMON CONFIGURATION
#


#
# QOS CONFIGURATION
#


#
# TRAFFIC-FILTER CONFIGURATION
#



#
# DVMRP CONFIGURATION
#


#
# PIM CONFIGURATION
#

ip pim enable

#
# IP PREFIX LIST CONFIGURATION
#


#
# SVLAN-CONFIGURATION
#



#
# PORT CONFIGURATION - PHASE I
#

ethernet 1/33 perform-tagging enable

#
# MLT CONFIGURATION
#


#
# STG CONFIGURATION
#


#
# VLAN CONFIGURATION
#

vlan 1 ports remove 1/2-1/15,1/33-1/34 member portmember
vlan 8 create byport 1 name "BH-X3 ~ BH-X1" color 1
vlan 8 ports remove 1/1-1/32,1/34,2/1-2/8 member portmember

vlan 8 ports add 1/33 member portmember
vlan 8 fdb-entry aging-time 21660
vlan 8 ip create 10.54.0.130/255.255.255.252 mac_offset 16
vlan 8 ip directed-broadcast disable
vlan 8 ip ospf enable
vlan 8 ip ospf metric 100
vlan 25 create byport 1 name "BH-X3 ~ BH-X4" color 1
vlan 25 ports remove 1/1-1/33,2/1-2/8 member portmember
vlan 25 ports add 1/34 member portmember
vlan 25 fdb-entry aging-time 21660
vlan 25 ip create 10.10.3.113/255.255.255.252 mac_offset 1
vlan 25 ip directed-broadcast disable
vlan 25 ip ospf enable
vlan 25 ip ospf metric 100
vlan 1133 create byport 1 name "BH-X3 ~ BE-X1" color 1
vlan 1133 ports remove 1/1-1/10,1/12-1/34,2/1-2/8 member portmember
vlan 1133 ports add 1/11 member portmember
vlan 1133 fdb-entry aging-time 21660
vlan 1133 ip create 10.10.3.21/255.255.255.252 mac_offset 2
vlan 1133 ip directed-broadcast disable
vlan 1133 ip ospf enable
vlan 1133 ip ospf metric 2000
vlan 1233 create byport 1 name "BH-X3 ~ BMM-X1" color 1

vlan 1233 ports remove 1/1-1/2,1/4-1/34,2/1-2/8 member portmember
vlan 1233 ports add 1/3 member portmember
vlan 1233 fdb-entry aging-time 21660
vlan 1233 ip create 10.10.3.33/255.255.255.252 mac_offset 3
vlan 1233 ip directed-broadcast disable
vlan 1233 ip ospf enable
vlan 1233 ip ospf metric 2000
vlan 1333 create byport 1 name "BH-X3 ~ BS-X1" color 1
vlan 1333 ports remove 1/1-1/3,1/5-1/34,2/1-2/8 member portmember
vlan 1333 ports add 1/4 member portmember
vlan 1333 fdb-entry aging-time 21660
vlan 1333 ip create 10.10.3.49/255.255.255.252 mac_offset 4
vlan 1333 ip directed-broadcast disable
vlan 1333 ip ospf enable
vlan 1333 ip ospf metric 2000
vlan 1433 create byport 1 name "BH-X3 ~ CB-X1" color 1
vlan 1433 ports remove 1/1-1/6,1/8-1/34,2/1-2/8 member portmember
vlan 1433 ports add 1/7 member portmember
vlan 1433 fdb-entry aging-time 21660
vlan 1433 ip create 10.10.3.81/255.255.255.252 mac_offset 5
vlan 1433 ip directed-broadcast disable
vlan 1433 ip ospf enable
vlan 1433 ip ospf metric 2000

vlan 1533 create byport 1 name "BH-X3 ~ CF-X1" color 1
vlan 1533 ports remove 1/1-1/4,1/6-1/34,2/1-2/8 member portmember
vlan 1533 ports add 1/5 member portmember
vlan 1533 fdb-entry aging-time 21660
vlan 1533 ip create 10.10.3.65/255.255.255.252 mac_offset 6
vlan 1533 ip directed-broadcast disable
vlan 1533 ip ospf enable
vlan 1533 ip ospf metric 2000
vlan 1733 create byport 1 name "BH-3X ~ LS-X1" color 1
vlan 1733 ports remove 1/1-1/12,1/14-1/34,2/1-2/8 member portmember
vlan 1733 ports add 1/13 member portmember
vlan 1733 fdb-entry aging-time 21660
vlan 1733 ip create 10.10.3.17/255.255.255.252 mac_offset 8
vlan 1733 ip directed-broadcast disable
vlan 1733 ip ospf enable
vlan 1733 ip ospf metric 2000
vlan 1833 create byport 1 name "BH-X3 ~ MR-X1" color 1
vlan 1833 ports remove 1/1,1/3-1/34,2/1-2/8 member portmember
vlan 1833 ports add 1/2 member portmember
vlan 1833 fdb-entry aging-time 21660
vlan 1833 ip create 10.10.3.9/255.255.255.252 mac_offset 9
vlan 1833 ip directed-broadcast disable
vlan 1833 ip ospf enable

vlan 1833 ip ospf metric 2000
vlan 1933 create byport 1 name "BH-X3 ~ NT-X1" color 1
vlan 1933 ports remove 1/1-1/13,1/15-1/34,2/1-2/8 member portmember
vlan 1933 ports add 1/14 member portmember
vlan 1933 fdb-entry aging-time 21660
vlan 1933 ip create 10.10.3.29/255.255.255.252 mac_offset 10
vlan 1933 ip directed-broadcast disable
vlan 1933 ip ospf enable
vlan 1933 ip ospf metric 2000
vlan 2033 create byport 1 name "BH-X3 ~ NCF-X1" color 1
vlan 2033 ports remove 1/1-1/7,1/9-1/34,2/1-2/8 member portmember
vlan 2033 ports add 1/8 member portmember
vlan 2033 fdb-entry aging-time 21660
vlan 2033 ip create 10.10.3.69/255.255.255.252 mac_offset 11
vlan 2033 ip directed-broadcast disable
vlan 2033 ip ospf enable
vlan 2033 ip ospf metric 2000
vlan 2133 create byport 1 name "BH-X3 ~ NG-X1" color 1
vlan 2133 ports remove 1/1-1/9,1/11-1/34,2/1-2/8 member portmember
vlan 2133 ports add 1/10 member portmember
vlan 2133 fdb-entry aging-time 21660
vlan 2133 ip create 10.10.3.37/255.255.255.252 mac_offset 12
vlan 2133 ip directed-broadcast disable

vlan 2133 ip ospf enable
vlan 2133 ip ospf metric 2000
vlan 2233 create byport 1 name "BH-X3 ~ PY-X1" color 1
vlan 2233 ports remove 1/1-1/8,1/10-1/34,2/1-2/8 member portmember
vlan 2233 ports add 1/9 member portmember
vlan 2233 fdb-entry aging-time 21660
vlan 2233 ip create 10.10.3.53/255.255.255.252 mac_offset 13
vlan 2233 ip directed-broadcast disable
vlan 2233 ip ospf enable
vlan 2233 ip ospf metric 2000
vlan 2333 create byport 1 name "BH-X3 ~ SO-X1" color 1
vlan 2333 ports remove 1/1-1/5,1/7-1/34,2/1-2/8 member portmember
vlan 2333 ports add 1/6 member portmember
vlan 2333 fdb-entry aging-time 21660
vlan 2333 ip create 10.10.3.73/255.255.255.252 mac_offset 14
vlan 2333 ip directed-broadcast disable
vlan 2333 ip ospf enable
vlan 2333 ip ospf metric 2000
vlan 2433 create byport 1 name "BH-X3 ~ TW-X1" color 1
vlan 2433 ports remove 1/1-1/14,1/16-1/34,2/1-2/8 member portmember
vlan 2433 ports add 1/15 member portmember
vlan 2433 fdb-entry aging-time 21660
vlan 2433 ip create 10.10.3.77/255.255.255.252 mac_offset 15

vlan 2433 ip directed-broadcast disable
vlan 2433 ip ospf enable
vlan 2433 ip ospf metric 2000

#
# PORT CONFIGURATION - PHASE II
#

ethernet 1/1 auto-negotiate disable
ethernet 1/1 speed 100
ethernet 1/1 duplex full
ethernet 1/1 state disable
ethernet 1/1 stg 1 stp disable
ethernet 1/2 auto-negotiate disable
ethernet 1/2 speed 100
ethernet 1/2 duplex full
ethernet 1/2 state disable
ethernet 1/2 stg 1 stp disable
ethernet 1/3 auto-negotiate disable
ethernet 1/3 speed 100
ethernet 1/3 duplex full
ethernet 1/3 stg 1 stp disable
ethernet 1/4 auto-negotiate disable

ethernet 1/4 speed 100
ethernet 1/4 duplex full
ethernet 1/4 state disable
ethernet 1/4 stg 1 stp disable
ethernet 1/5 auto-negotiate disable
ethernet 1/5 speed 100
ethernet 1/5 duplex full
ethernet 1/5 state disable
ethernet 1/5 stg 1 stp disable
ethernet 1/6 auto-negotiate disable
ethernet 1/6 speed 100
ethernet 1/6 duplex full
ethernet 1/6 state disable
ethernet 1/6 stg 1 stp disable
ethernet 1/7 auto-negotiate disable
ethernet 1/7 speed 100
ethernet 1/7 duplex full
ethernet 1/7 enable-diffserv true
ethernet 1/7 qos-level 0
ethernet 1/7 state disable
ethernet 1/7 stg 1 stp disable
ethernet 1/8 auto-negotiate disable
ethernet 1/8 speed 100

BH-X3:3# show config
Preparing to Display Configuration...
#
# THU JAN 24 13:21:16 2013 UTC
# box type             : Passport-8003
# software version     : 3.5.1.0
# monitor version      : 3.5.1.0/000
#

#
# Asic Info :
# SlotNum|Name  |CardType   |MdaType |Parts Description
#
# Slot  1 8632TXE  0x20210120 0x00000000   IOM: PLRO=3  GMAC=5  BFM: OP=3 TMUX=2 RARU=4 CPLD=5
# Slot  2 8608GBE  0x20325108 0x00000000   IOM: GMAC=5  BFM: OP=3 TMUX=2 RARU=4 CPLD=9
# Slot  3 8691SF   0x200e0100 0x00000000  CPU: CPLD=19 SFM: OP=3 TMUX=2 SWIP=3 FAD=1 CF=16
#
#!flags m-mode false
#!flags enhanced-operational-mode false
#!flags vlan-optimization-mode false
#!record-reservation filter 4096
#!record-reservation ipmc 500
#!record-reservation local 2000
#!record-reservation mac 2000
#!record-reservation static-route 200

#!record-reservation vrrp 500
#!end
#
config
mac-flap-time-limit 500

#
# CLI CONFIGURATION
#

cli prompt "BH-X3"

#
# SYSTEM CONFIGURATION
#

sys set contact "NMC x55800"
sys set location "BH - CAA1 - Area 3"
sys set topology off
sys set snmp trap-recv 10.10.10.18 v1 nicam
sys set snmp trap-recv 10.170.24.199 v1 public
sys set snmp trap-recv 10.170.29.6 v1 nicam
sys set snmp trap-recv 132.185.2.6 v1 nicam


#
# LOG CONFIGURATION
#


#
# LINK-FLAP-DETECT CONFIGURATION
#


#
# IEEE VLAN AGING CONFIGURATION
#


#
# ACCESS-POLICY CONFIGURATION
#

sys access-policy policy 1 accesslevel rwa
sys access-policy policy 1 name "Naran WC"
sys access-policy policy 1 precedence 1

sys access-policy policy 1 username ""
sys access-policy policy 1 network 10.164.36.36/255.255.255.255
sys access-policy policy 1 service http disable
sys access-policy policy 1 service telnet disable
sys access-policy policy 3 create
sys access-policy policy 3 name "NTS TVC"
sys access-policy policy 3 precedence 1
sys access-policy policy 3 username ""
sys access-policy policy 3 network 132.185.2.0/255.255.255.0
sys access-policy policy 3 service http enable
sys access-policy policy 3 service rlogin enable
sys access-policy policy 3 service telnet enable
sys access-policy policy 3 service tftp enable
sys access-policy policy 3 service ftp enable
sys access-policy policy 4 create
sys access-policy policy 4 name "TECH spec"
sys access-policy policy 4 precedence 1
sys access-policy policy 4 username ""
sys access-policy policy 4 network 10.54.7.0/255.255.255.0
sys access-policy policy 4 service http enable
sys access-policy policy 4 service rlogin enable
sys access-policy policy 4 service telnet enable
sys access-policy policy 4 service tftp enable

sys access-policy policy 4 service ftp enable
sys access-policy policy 5 create
sys access-policy policy 5 name "Keki (home)"
sys access-policy policy 5 precedence 1
sys access-policy policy 5 username ""
sys access-policy policy 5 network 10.182.232.1/255.255.255.255
sys access-policy policy 5 service http enable
sys access-policy policy 5 service rlogin enable
sys access-policy policy 5 service telnet enable
sys access-policy policy 5 service tftp enable
sys access-policy policy 5 service ftp enable
sys access-policy policy 6 create
sys access-policy policy 6 name "Reithmon TVC"
sys access-policy policy 6 precedence 1
sys access-policy policy 6 username ""
sys access-policy policy 6 network 10.176.2.5/255.255.255.255
sys access-policy policy 6 service http enable
sys access-policy policy 6 service rlogin enable
sys access-policy policy 6 service telnet enable
sys access-policy policy 6 service tftp enable
sys access-policy policy 6 service ftp enable
sys access-policy policy 7 create
sys access-policy policy 7 name "MRTG"

sys access-policy policy 7 precedence 1
sys access-policy policy 7 username ""
sys access-policy policy 7 network 10.48.2.11/255.255.255.255
sys access-policy policy 8 create
sys access-policy policy 8 name "Ehealth"
sys access-policy policy 8 precedence 1
sys access-policy policy 8 username ""
sys access-policy policy 8 network 10.48.2.15/255.255.255.255
sys access-policy policy 9 create
sys access-policy policy 9 name "MRTG2"
sys access-policy policy 9 precedence 1
sys access-policy policy 9 username ""
sys access-policy policy 9 network 10.180.162.43/255.255.255.255
sys access-policy policy 11 create
sys access-policy policy 11 name "NTS"
sys access-policy policy 11 precedence 1
sys access-policy policy 11 username ""
sys access-policy policy 11 network 10.170.24.0/255.255.248.0
sys access-policy policy 11 service http enable
sys access-policy policy 11 service rlogin enable
sys access-policy policy 11 service telnet enable
sys access-policy policy 11 service tftp enable
sys access-policy policy 11 service ftp enable

sys access-policy policy 15 create
sys access-policy policy 15 name "HPOV"
sys access-policy policy 15 username ""
sys access-policy policy 15 network 10.10.10.16/255.255.255.240
sys access-policy policy 16 create
sys access-policy policy 16 name "BH eHealth"
sys access-policy policy 16 username ""
sys access-policy policy 16 network 10.52.69.33/255.255.255.255
sys access-policy policy 17 create
sys access-policy policy 17 name "BH MRTG"
sys access-policy policy 17 username ""
sys access-policy policy 17 network 10.52.69.39/255.255.255.255
sys access-policy policy 19 create
sys access-policy policy 19 name "SBS-TS"
sys access-policy policy 19 precedence 1
sys access-policy policy 19 username ""
sys access-policy policy 19 network 10.199.32.0/255.255.255.0
#
# SSH CONFIGURATION
#


#

# MCAST SOFTWARE FORWARDING CONFIGURATION
#


#
# SNMP V3 GROUP MEMBERSHIP CONFIGURATION
#


#
# SNMP V3 GROUP ACCESS CONFIGURATION
#


#
# SNMP V3 MIB VIEW CONFIGURATION
#


#
# SNMP V3 COMMUNITY TABLE CONFIGURATION
#



#
# SLOT CONFIGURATION
#


#
# WEB CONFIGURATION
#


#
# RMON CONFIGURATION
#


#
# QOS CONFIGURATION
#


#
# TRAFFIC-FILTER CONFIGURATION

#


#
# DVMRP CONFIGURATION
#


#
# PIM CONFIGURATION
#

ip pim enable

#
# IP PREFIX LIST CONFIGURATION
#


#
# SVLAN-CONFIGURATION
#



#
# PORT CONFIGURATION - PHASE I
#

ethernet 1/33 perform-tagging enable

#
# MLT CONFIGURATION
#


#
# STG CONFIGURATION
#


#
# VLAN CONFIGURATION
#

vlan 1 ports remove 1/2-1/15,1/33-1/34 member portmember
vlan 8 create byport 1 name "BH-X3 ~ BH-X1" color 1

vlan 8 ports remove 1/1-1/32,1/34,2/1-2/8 member portmember
vlan 8 ports add 1/33 member portmember
vlan 8 fdb-entry aging-time 21660
vlan 8 ip create 10.54.0.130/255.255.255.252 mac_offset 16
vlan 8 ip directed-broadcast disable
vlan 8 ip ospf enable
vlan 8 ip ospf metric 100
vlan 25 create byport 1 name "BH-X3 ~ BH-X4" color 1
vlan 25 ports remove 1/1-1/33,2/1-2/8 member portmember
vlan 25 ports add 1/34 member portmember
vlan 25 fdb-entry aging-time 21660
vlan 25 ip create 10.10.3.113/255.255.255.252 mac_offset 1
vlan 25 ip directed-broadcast disable
vlan 25 ip ospf enable
vlan 25 ip ospf metric 100
vlan 1133 create byport 1 name "BH-X3 ~ BE-X1" color 1
vlan 1133 ports remove 1/1-1/10,1/12-1/34,2/1-2/8 member portmember
vlan 1133 ports add 1/11 member portmember
vlan 1133 fdb-entry aging-time 21660
vlan 1133 ip create 10.10.3.21/255.255.255.252 mac_offset 2
vlan 1133 ip directed-broadcast disable
vlan 1133 ip ospf enable
vlan 1133 ip ospf metric 2000

vlan 1233 create byport 1 name "BH-X3 ~ BMM-X1" color 1
vlan 1233 ports remove 1/1-1/2,1/4-1/34,2/1-2/8 member portmember
vlan 1233 ports add 1/3 member portmember
vlan 1233 fdb-entry aging-time 21660
vlan 1233 ip create 10.10.3.33/255.255.255.252 mac_offset 3
vlan 1233 ip directed-broadcast disable
vlan 1233 ip ospf enable
vlan 1233 ip ospf metric 2000
vlan 1333 create byport 1 name "BH-X3 ~ BS-X1" color 1
vlan 1333 ports remove 1/1-1/3,1/5-1/34,2/1-2/8 member portmember
vlan 1333 ports add 1/4 member portmember
vlan 1333 fdb-entry aging-time 21660
vlan 1333 ip create 10.10.3.49/255.255.255.252 mac_offset 4
vlan 1333 ip directed-broadcast disable
vlan 1333 ip ospf enable
vlan 1333 ip ospf metric 2000
vlan 1433 create byport 1 name "BH-X3 ~ CB-X1" color 1
vlan 1433 ports remove 1/1-1/6,1/8-1/34,2/1-2/8 member portmember
vlan 1433 ports add 1/7 member portmember
vlan 1433 fdb-entry aging-time 21660
vlan 1433 ip create 10.10.3.81/255.255.255.252 mac_offset 5
vlan 1433 ip directed-broadcast disable
vlan 1433 ip ospf enable

vlan 1433 ip ospf metric 2000
vlan 1533 create byport 1 name "BH-X3 ~ CF-X1" color 1
vlan 1533 ports remove 1/1-1/4,1/6-1/34,2/1-2/8 member portmember
vlan 1533 ports add 1/5 member portmember
vlan 1533 fdb-entry aging-time 21660
vlan 1533 ip create 10.10.3.65/255.255.255.252 mac_offset 6
vlan 1533 ip directed-broadcast disable
vlan 1533 ip ospf enable
vlan 1533 ip ospf metric 2000
vlan 1733 create byport 1 name "BH-3X ~ LS-X1" color 1
vlan 1733 ports remove 1/1-1/12,1/14-1/34,2/1-2/8 member portmember
vlan 1733 ports add 1/13 member portmember
vlan 1733 fdb-entry aging-time 21660
vlan 1733 ip create 10.10.3.17/255.255.255.252 mac_offset 8
vlan 1733 ip directed-broadcast disable
vlan 1733 ip ospf enable
vlan 1733 ip ospf metric 2000
vlan 1833 create byport 1 name "BH-X3 ~ MR-X1" color 1
vlan 1833 ports remove 1/1,1/3-1/34,2/1-2/8 member portmember
vlan 1833 ports add 1/2 member portmember
vlan 1833 fdb-entry aging-time 21660
vlan 1833 ip create 10.10.3.9/255.255.255.252 mac_offset 9
vlan 1833 ip directed-broadcast disable

vlan 1833 ip ospf enable
vlan 1833 ip ospf metric 2000
vlan 1933 create byport 1 name "BH-X3 ~ NT-X1" color 1
vlan 1933 ports remove 1/1-1/13,1/15-1/34,2/1-2/8 member portmember
vlan 1933 ports add 1/14 member portmember
vlan 1933 fdb-entry aging-time 21660
vlan 1933 ip create 10.10.3.29/255.255.255.252 mac_offset 10
vlan 1933 ip directed-broadcast disable
vlan 1933 ip ospf enable
vlan 1933 ip ospf metric 2000
vlan 2033 create byport 1 name "BH-X3 ~ NCF-X1" color 1
vlan 2033 ports remove 1/1-1/7,1/9-1/34,2/1-2/8 member portmember
vlan 2033 ports add 1/8 member portmember
vlan 2033 fdb-entry aging-time 21660
vlan 2033 ip create 10.10.3.69/255.255.255.252 mac_offset 11
vlan 2033 ip directed-broadcast disable
vlan 2033 ip ospf enable
vlan 2033 ip ospf metric 2000
vlan 2133 create byport 1 name "BH-X3 ~ NG-X1" color 1
vlan 2133 ports remove 1/1-1/9,1/11-1/34,2/1-2/8 member portmember
vlan 2133 ports add 1/10 member portmember
vlan 2133 fdb-entry aging-time 21660
vlan 2133 ip create 10.10.3.37/255.255.255.252 mac_offset 12

vlan 2133 ip directed-broadcast disable
vlan 2133 ip ospf enable
vlan 2133 ip ospf metric 2000
vlan 2233 create byport 1 name "BH-X3 ~ PY-X1" color 1
vlan 2233 ports remove 1/1-1/8,1/10-1/34,2/1-2/8 member portmember
vlan 2233 ports add 1/9 member portmember
vlan 2233 fdb-entry aging-time 21660
vlan 2233 ip create 10.10.3.53/255.255.255.252 mac_offset 13
vlan 2233 ip directed-broadcast disable
vlan 2233 ip ospf enable
vlan 2233 ip ospf metric 2000
vlan 2333 create byport 1 name "BH-X3 ~ SO-X1" color 1
vlan 2333 ports remove 1/1-1/5,1/7-1/34,2/1-2/8 member portmember
vlan 2333 ports add 1/6 member portmember
vlan 2333 fdb-entry aging-time 21660
vlan 2333 ip create 10.10.3.73/255.255.255.252 mac_offset 14
vlan 2333 ip directed-broadcast disable
vlan 2333 ip ospf enable
vlan 2333 ip ospf metric 2000
vlan 2433 create byport 1 name "BH-X3 ~ TW-X1" color 1
vlan 2433 ports remove 1/1-1/14,1/16-1/34,2/1-2/8 member portmember
vlan 2433 ports add 1/15 member portmember
vlan 2433 fdb-entry aging-time 21660

vlan 2433 ip create 10.10.3.77/255.255.255.252 mac_offset 15
vlan 2433 ip directed-broadcast disable
vlan 2433 ip ospf enable
vlan 2433 ip ospf metric 2000

#
# PORT CONFIGURATION - PHASE II
#

ethernet 1/1 auto-negotiate disable
ethernet 1/1 speed 100
ethernet 1/1 duplex full
ethernet 1/1 state disable
ethernet 1/1 stg 1 stp disable
ethernet 1/2 auto-negotiate disable
ethernet 1/2 speed 100
ethernet 1/2 duplex full
ethernet 1/2 state disable
ethernet 1/2 stg 1 stp disable
ethernet 1/3 auto-negotiate disable
ethernet 1/3 speed 100
ethernet 1/3 duplex full
ethernet 1/3 stg 1 stp disable

ethernet 1/4 auto-negotiate disable
ethernet 1/4 speed 100
ethernet 1/4 duplex full
ethernet 1/4 state disable
ethernet 1/4 stg 1 stp disable
ethernet 1/5 auto-negotiate disable
ethernet 1/5 speed 100
ethernet 1/5 duplex full
ethernet 1/5 state disable
ethernet 1/5 stg 1 stp disable
ethernet 1/6 auto-negotiate disable
ethernet 1/6 speed 100
ethernet 1/6 duplex full
ethernet 1/6 state disable
ethernet 1/6 stg 1 stp disable
ethernet 1/7 auto-negotiate disable
ethernet 1/7 speed 100
ethernet 1/7 duplex full
ethernet 1/7 enable-diffserv true
ethernet 1/7 qos-level 0
ethernet 1/7 state disable
ethernet 1/7 stg 1 stp disable
ethernet 1/8 auto-negotiate disable

ethernet 1/8 speed 100
ethernet 1/8 duplex full
ethernet 1/8 enable-diffserv true
ethernet 1/8 qos-level 0
ethernet 1/8 state disable
ethernet 1/8 stg 1 stp disable
ethernet 1/9 auto-negotiate disable
ethernet 1/9 speed 100
ethernet 1/9 duplex full
ethernet 1/9 state disable
ethernet 1/9 stg 1 stp disable
ethernet 1/10 auto-negotiate disable
ethernet 1/10 speed 100
ethernet 1/10 duplex full
ethernet 1/10 state disable
ethernet 1/10 stg 1 stp disable
ethernet 1/11 auto-negotiate disable
ethernet 1/11 speed 100
ethernet 1/11 duplex full
ethernet 1/11 state disable
ethernet 1/11 stg 1 stp disable
ethernet 1/12 auto-negotiate disable
ethernet 1/12 speed 100

ethernet 1/12 duplex full
ethernet 1/12 state disable
ethernet 1/12 stg 1 stp disable
ethernet 1/13 auto-negotiate disable
ethernet 1/13 speed 100
ethernet 1/13 duplex full
ethernet 1/13 state disable
ethernet 1/13 stg 1 stp disable
ethernet 1/14 auto-negotiate disable
ethernet 1/14 speed 100
ethernet 1/14 duplex full
ethernet 1/14 state disable
ethernet 1/14 stg 1 stp disable
ethernet 1/15 auto-negotiate disable
ethernet 1/15 speed 100
ethernet 1/15 duplex full
ethernet 1/15 state disable
ethernet 1/15 stg 1 stp disable
ethernet 1/16 auto-negotiate disable
ethernet 1/16 speed 100
ethernet 1/16 duplex full
ethernet 1/16 state disable
ethernet 1/16 stg 1 stp disable

ethernet 1/17 state disable
ethernet 1/17 stg 1 stp disable
ethernet 1/18 state disable
ethernet 1/18 stg 1 stp disable
ethernet 1/19 state disable
ethernet 1/19 stg 1 stp disable
ethernet 1/20 state disable
ethernet 1/20 stg 1 stp disable
ethernet 1/21 state disable
ethernet 1/21 stg 1 stp disable
ethernet 1/22 state disable
ethernet 1/22 stg 1 stp disable
ethernet 1/23 state disable
ethernet 1/23 stg 1 stp disable
ethernet 1/24 auto-negotiate disable
ethernet 1/24 speed 100
ethernet 1/24 duplex full
ethernet 1/24 state disable
ethernet 1/24 stg 1 stp disable
ethernet 1/25 state disable
ethernet 1/25 stg 1 stp disable
ethernet 1/26 state disable
ethernet 1/26 stg 1 stp disable

ethernet 1/27 state disable
ethernet 1/27 stg 1 stp disable
ethernet 1/28 state disable
ethernet 1/28 stg 1 stp disable
ethernet 1/29 state disable
ethernet 1/29 stg 1 stp disable
ethernet 1/30 state disable
ethernet 1/30 stg 1 stp disable
ethernet 1/31 state disable
ethernet 1/31 stg 1 stp disable
ethernet 1/32 state disable
ethernet 1/32 stg 1 stp disable
ethernet 1/33 default-vlan-id 8
ethernet 1/33 stg 1 stp disable
ethernet 1/34 enable-diffserv true
ethernet 1/34 qos-level 0
ethernet 1/34 stg 1 stp disable

#
# IPX CONFIGURATION
#



#
# IP & RIP CONFIGURATION
#


#
# IP AS LIST CONFIGURATION
#


#
# IP COMMUNITY LIST CONFIGURATION
#


#
# IP ROUTE POLICY CONFIGURATION
#

ip route-policy "_rpsLocalOs" seq 21845 create
ip route-policy "_rpsLocalOs" seq 21845 enable
ip route-policy "_rpsLocalOs" seq 21845 action permit
ip route-policy "_rpsLocalOs" seq 21845 set-metric-type type1

ip route-policy "_rpsStaticOs" seq 21845 create
ip route-policy "_rpsStaticOs" seq 21845 enable
ip route-policy "_rpsStaticOs" seq 21845 action permit
ip route-policy "_rpsStaticOs" seq 21845 set-metric-type type1
ip route-policy "_rpsRipOs" seq 21845 create
ip route-policy "_rpsRipOs" seq 21845 disable
ip route-policy "_rpsRipOs" seq 21845 action permit
ip route-policy "_rpsRipOs" seq 21845 set-metric-type type1



#
# CIRCUITLESS IP INTERFACE CONFIGURATION
#



#
# DHCP CONFIGURATION
#


#

# IGMP CONFIGURATION
#


#
# PIM RP CONFIGURATION
#


#
# PIM STATIC RP CONFIGURATION
#


#
# OSPF CONFIGURATION
#

ip ospf admin-state enable
ip ospf as-boundary-router enable
ip ospf router-id 10.10.3.113
ip ospf enable


#
# MROUTE CONFIGURATION
#


#
# MCAST RESOURCE USAGE CONFIGURATION
#


#
# PGM CONFIGURATION
#


#
# MCAST MLT DISTRIBUTION CONFIGURATION
#


#
# TIMED PRUNE CONFIGURATION
#



#
# BGP CONFIGURATION
#


#
# IP REDISTRIBUTION CONFIGURATION
#

ip ospf redistribute static create
ip ospf redistribute static metric-type type1
ip ospf redistribute static route-policy "_rpsStaticOs"
ip ospf redistribute static enable
ip ospf redistribute direct create
ip ospf redistribute direct metric-type type1
ip ospf redistribute direct route-policy "_rpsLocalOs"
ip ospf redistribute direct enable

#
# OSPF ACCEPT CONFIGURATION
#



#
# RIP POLICY CONFIGURATION
#


#
# DVMRP POLICY CONFIGURATION
#


#
# UDP FWD CONFIGURATION
#


#
# VRRP CONFIGURATION
#


#

# POS CONFIGURATION
#


#
# ATM CONFIGURATION
#


#
# DIAG CONFIGURATION
#


#
# RADIUS CONFIGURATION
#


#
# NTP CONFIGURATION
#


ntp enable true
ntp server create 10.152.4.10

back
