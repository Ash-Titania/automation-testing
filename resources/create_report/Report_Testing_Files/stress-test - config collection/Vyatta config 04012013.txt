firewall {
    all-ping enable
    broadcast-ping disable
    conntrack-expect-table-size 4096
    conntrack-hash-size 4096
    conntrack-table-size 32768
    conntrack-tcp-loose enable
    ipv6-receive-redirects disable
    ipv6-src-route disable
    ip-src-route disable
    log-martians enable
    name private-to-serverfarm {
        default-action drop
        description "Private to Server Farm"
        rule 1 {
            action accept
            state {
                established enable
                related enable
            }
        }
        rule 10 {
            action accept
            destination {
                port 21,80,443
            }
            protocol tcp
        }
        rule 20 {
            action accept
            destination {
                address 192.168.90.100
                port 22
            }
            protocol tcp
            source {
                address 192.168.80.10
            }
        }
        rule 30 {
            action accept
            destination {
                address 192.168.90.20
                port 22,25
            }
            protocol tcp
            source {
                address 192.168.80.20
            }
        }
    }
    name public-to-serverfarm {
        default-action drop
        description "Public to Server Farm"
        rule 5 {
            action drop
            destination {
                port 25,80,443
            }
            protocol tcp
            recent {
                count 20
                time 5
            }
        }
        rule 10 {
            action accept
            destination {
                port 80,443
            }
            protocol tcp
        }
        rule 20 {
            action accept
            destination {
                address 192.168.90.20
                port 25
            }
            protocol tcp
        }
    }
    name to-private {
        default-action drop
        description "Allow established to Private zone"
        rule 1 {
            action accept
            state {
                established enable
                related enable
            }
        }
    }
    name to-public {
        default-action drop
        description "Allow authorized traffic to Public Zone"
        rule 1 {
            action accept
            state {
                established enable
                related enable
            }
        }
        rule 10 {
            action accept
            destination {
                port 80,443
            }
            protocol tcp
        }
        rule 20 {
            action accept
            destination {
                address 10.11.2.49-10.11.2.50
                port 53
            }
            protocol udp
        }
    }
    receive-redirects disable
    send-redirects enable
    source-validation disable
    syn-cookies enable
}
interfaces {
    ethernet eth0 {
        address 10.11.0.3/22
        address 10.11.0.101/22
        address 10.11.0.102/22
        address 10.11.0.103/22
        address 10.200.4.200/24
        duplex auto
        hw-id 00:0c:29:fb:a9:34
        speed auto
    }
    ethernet eth1 {
        address 192.168.80.1/24
        duplex auto
        hw-id 00:0c:29:fb:a9:3e
        smp_affinity auto
        speed auto
    }
    ethernet eth2 {
        address 192.168.90.1/24
        duplex auto
        hw-id 00:0c:29:fb:a9:48
        smp_affinity auto
        speed auto
    }
    ethernet eth3 {
        address 192.168.70.1/24
        duplex auto
        hw-id 00:0c:29:fb:a9:52
        smp_affinity auto
        speed auto
    }
    loopback lo {
    }
}
service {
    nat {
        rule 10 {
            outbound-interface eth0
            outside-address {
                address 10.11.0.101
            }
            source {
                address 192.168.90.100
            }
            type source
        }
        rule 11 {
            outbound-interface eth0
            outside-address {
                address 10.11.0.102
            }
            source {
                address 192.168.90.20
            }
            type source
        }
        rule 20 {
            destination {
                address 10.11.0.101
            }
            inbound-interface eth0
            inside-address {
                address 192.168.90.10
            }
            type destination
        }
        rule 21 {
            destination {
                address 10.11.0.102
            }
            inbound-interface eth0
            inside-address {
                address 192.168.90.20
            }
            type destination
        }
        rule 1000 {
            outbound-interface eth0
            outside-address {
                address 10.11.0.103
            }
            source {
                address 192.168.70.2-192.168.80.254
            }
            type source
        }
    }
}
system {
    config-management {
        commit-revisions 20
    }
    console {
        device ttyS0 {
            speed 9600
        }
    }
    gateway-address 10.11.0.1
    host-name vyatta
    login {
        user vyatta {
            authentication {
                encrypted-password $1$gq0oxuRb$Ws4ugSSWsUNSLO4wLKvT01
            }
            level admin
        }
    }
    name-server 8.8.8.8
    ntp {
        server 0.vyatta.pool.ntp.org {
        }
        server 1.vyatta.pool.ntp.org {
        }
        server 2.vyatta.pool.ntp.org {
        }
        server pool.ntp.org {
        }
    }
    package {
        auto-sync 1
        repository community {
            components main
            distribution stable
            password ""
            url http://packages.vyatta.com/vyatta
            username ""
        }
    }
    syslog {
        global {
            facility all {
                level notice
            }
            facility protocols {
                level debug
            }
        }
    }
    time-zone GMT
}
zone-policy {
    zone private {
        default-action drop
        description "Private zone (inside firewall)"
        from public {
            firewall {
                name to-private
            }
        }
        from serverfarm {
            firewall {
                name to-private
            }
        }
        interface eth1
        interface eth3
    }
    zone public {
        default-action drop
        description "Public zone (outside firewall)"
        from private {
            firewall {
                name to-public
            }
        }
        from serverfarm {
            firewall {
                name to-public
            }
        }
        interface eth0
    }
    zone serverfarm {
        default-action drop
        description "Server Farm zone"
        from private {
            firewall {
                name private-to-serverfarm
            }
        }
        from public {
            firewall {
                name public-to-serverfarm
            }
        }
        interface eth2
    }
}


/* Warning: Do not remove the following line. */
/* === vyatta-config-version: "cluster@1:config-management@1:conntrack-sync@1:content-inspection@2:dhcp-relay@1:dhcp-server@4:firewall@4:ipsec@2:nat@3:qos@1:quagga@2:system@4:vrrp@1:wanloadbalance@2:webgui@1:webproxy@1:zone-policy@1" === */
