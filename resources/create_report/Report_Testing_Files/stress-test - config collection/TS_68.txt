## Last commit: 2012-09-24 15:12:27 UTC by root
version 10.0R3.10;
system {
    root-authentication {
        encrypted-password "$1$MP1l0G9Q$TiDTTaRx8Bo9tXZMfZjwP/"; ## SECRET-DATA
    }
    name-server {
        208.67.222.222;
        208.67.220.220;
    }
    login {
        user admin {
            full-name admin;
            uid 200;
            class super-user;
            authentication {
                encrypted-password "$1$uLaQvI0n$ts4rwlTpM9e7UpF.60xSH1"; ## SECRET-DATA
            }
        }
    }
    services {
        ftp;
        ssh;
        telnet;
        web-management {
            http;
            https {
                system-generated-certificate;
            }
            session {
                session-limit 7;
            }
        }
        dhcp {
            router {
                192.168.1.1;
            }
            pool 192.168.1.0/24 {
                address-range low 192.168.1.2 high 192.168.1.254;
            }
            propagate-settings ge-0/0/0.0;
        }
    }
    syslog {
        archive size 100k files 3;
        user * {
            any emergency;
        }
        file messages {
            any critical;
            authorization info;
        }
        file interactive-commands {
            interactive-commands error;
        }
    }
    max-configurations-on-flash 5;
    max-configuration-rollbacks 5;
    license {
        autoupdate {
            url https://ae1.juniper.net/junos/key_retrieval;
        }
    }
}
interfaces {
    interface-range interfaces-trust {
        member ge-0/0/1;
        member fe-0/0/2;
        member fe-0/0/3;
        member fe-0/0/4;
        member fe-0/0/5;
        member fe-0/0/6;
        member fe-0/0/7;
        unit 0 {
            family ethernet-switching {
                vlan {
                    members vlan-trust;
                }
            }
        }
    }
    ge-0/0/0 {
        unit 0 {
            family inet {
                address 10.200.4.222/24;
            }
        }
    }
    ge-0/0/1 {
        mtu 1500;
    }
    vlan {
        unit 0 {
            family inet {
                address 192.168.1.1/24;
            }
        }
    }
}
routing-options {
    static {
        route 0.0.0.0/24 next-hop 0.0.0.0;
    }
}
security {
    nat {
        source {
            rule-set trust-to-untrust {
                from zone trust;
                to zone untrust;
                rule source-nat-rule {
                    match {
                        source-address 0.0.0.0/0;
                    }
                    then {
                        source-nat {
                            interface;
                        }
                    }
                }
            }
        }
        proxy-arp {
            interface fe-0/0/2.0 {
                ##
                ## Warning: Interface must be defined under [interfaces]
                ##
                address {
                    1.2.3.4/32;
                }
            }
        }
    }
    screen {
        ids-option untrust-screen {
            icmp {
                ping-death;
            }
            ip {
                source-route-option;
                tear-drop;
            }
            tcp {
                syn-flood {
                    alarm-threshold 1024;
                    attack-threshold 200;
                    source-threshold 1024;
                    destination-threshold 2048;
                    timeout 20;
                }
                land;
            }
        }
    }
    zones {
        security-zone trust {
            host-inbound-traffic {
                system-services {
                    all;
                }
                protocols {
                    all;
                }
            }
            interfaces {
                vlan.0;
            }
        }
        security-zone untrust {
            screen untrust-screen;
            host-inbound-traffic {
                system-services {
                    all;
                }
            }
            interfaces {
                ge-0/0/0.0 {
                    host-inbound-traffic {
                        system-services {
                            dhcp;
                            tftp;
                            all;
                        }
                    }
                }
            }
        }
        security-zone zone1 {
            address-book {
                address H-NAME1-10-64-4-32 10.64.4.32/32;
                address H-NAME2-10-14-12-64 10.14.12.64/32;
                address H-NAME3-10-14-12-111 10.14.12.111/32;
                address NAME4-10-4-10-38 10.4.10.38/32;
            }
            host-inbound-traffic {
                system-services {
                    all;
                }
                protocols {
                    all;
                }
            }
        }
        security-zone internet;
    }
    policies {
        from-zone zone1 to-zone trust {
            policy policy1 {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone zone1 to-zone internet {
            policy policy5 {
                match {
                    source-address [ H-NAME1-10-64-4-32 H-NAME2-10-14-12-64 H-NAME3-10-14-12-111 ];
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                    log {
                        session-init;
                        session-close;
                    }
                }
            }
        }
        default-policy {
            permit-all;
        }
    }
}
vlans {
    vlan-trust {
        vlan-id 3;
        l3-interface vlan.0;
    }
}

