
#
# Summit300-48 Configuration generated Thu Sep 27 11:02:45 2012
# Software Version 7.7e.2.4 [ssh] by Release_Master on 06/29/07 10:31:26

# Configuration Mode
#
# Config information for VLAN Default.
configure vlan "Default" tag 1     # VLAN-ID=0x1  Global Tag 1
configure stpd s0 add vlan "Default"
configure vlan "Default" ipaddress 192.168.0.18 255.255.255.0
#
# Config information for VLAN MacVlanDiscover.

# Boot information
use image secondary

#Configuration Information
use configuration secondary
disable telnet
disable web http
enable web https
# SNMP Configuration

disable snmp access
config idletimeouts 5
enable cli-prompt-number

# Ports AutoNeg Configuration

# Load Sharing Configuration

# Ports Configuration

# Spanning tree information

# MAC FDB configuration and static entries

configure ipfdb agingtime 0

# -- IP Interface[0] = "Default"

# Global IP settings.
disable icmp access-list
disable icmp access-list
#
# IP ARP Configuration
configure iparp max-entries 8192
#
# IP Route Configuration
configure iproute add default 192.168.0.1 1
# Multicast configuration
enable igmp snooping
enable igmp snooping vlan "Default"
enable igmp snooping vlan "MacVlanDiscover"
disable mvr
# RIP interface configuration
# RIP global parameter configuration









#
# PIM Router Configuration
#

#
# Static MRoute Configuration

# Ospf Area Configuration

# Ospf Range Configuration

# Interface Configuration

# Virtual Link Configuration

# Ospf ASE Summary Configuration

# OSPF Router Configuration




# ESRP Interface Configuration


#ELRP Configuration

# VRRP Configuration

# EAPS configuration

# EAPS shared port configuration

# NAT configuration
configure nat timeout 300

# SNTP client configuration
#
# Radius configuration
#

# TACACS configuration
disable tacacs
configure tacacs primary shared-secret encrypted "cmhkk`cecd`mmhbdbjbcmh"
configure tacacs secondary shared-secret encrypted "rmzy}np`"
disable tacacs-authorization
disable tacacs-accounting

# Mac Vlan Configurations
#
# Access-mask Configuration
#
# Access-list Configuration
#
# Rate-limit Configuration

#
# System Dump Configuration
#

## SNMPV3 EngineID Configuration
#
## SNMPV3 USM Users Configuration
#
#
# SNMPV3 MIB Views Configuration
#
#
# SNMPV3 VACM Access Configuration
#
#
# SNMPV3 USM Groups Configuration
#
#
# SNMPV3 Community Table Configuration
#
config snmpv3 add community encrypted "viz~" name encrypted "viz~" user "v1v2c_ro"
#
# SNMPV3 Target Addr Configuration
#
#
# SNMPV3 Target Params Configuration
#
#
# SNMPV3 Notify Configuration
#
#
# SNMPV3 Notify Filter Profile Configuration
#
#
# SNMPV3 Notify Filter Configuration
#



# System-wide Debug Configuration
#No System-wide debug tracing configured

#Vlan Based Debug Configuration
#
#No Vlan-based debug-tracing configured

#Port Based Debug Configuration
#
#No Port based debug-tracing configured

# IP subnet lookup configuration

# Network Login Configuration
configure netlogin mac auth-retry-count 3
configure netlogin mac reauth-period 1800

# Network Login Dot1x Guest Vlan Configuration

# Event Management System Configuration

# Event Management System Log Filter Configuration

# Event Management System Log Target Configuration
# Enhanced-dos-protect configuration
disable enhanced-dos-protect ipfdb
disable enhanced-dos-protect rate-limit
# Source IP Guard Configuration
#
# Wireless Configuration
#
config wireless Country-code UK
config wireless default-gateway 192.168.0.18
#
# Wireless Redirect Database Configuration
#
#   (num of Redirect DB entries 0)
#
# Wireless Image Configuration
#
#
# RF Profile configuration
#









# Security Profile configuration
#



# Antenna Profile configuration
#



# Wireless Port configuration
#
# There are no non-default wireless ports to display

# Wireless Interface configuration
#

# Wireless AP scan, client scan and client history configuration
#

# SSL configuration
configure ssl certificate pregenerated
-----BEGIN CERTIFICATE-----
MIIDLDCCAhSgAwIBAgIBADANBgkqhkiG9w0BAQQFADBPMQswCQYDVQQGEwJVUzEf
MB0GA1UEChMWRXh0cmVtZSBOZXR3b3JrcywgSW5jLjEfMB0GA1UEAxMWRXh0cmVt
ZSBOZXR3b3JrcywgSW5jLjAeFw0wMzEwMjEwNTMzNDdaFw0wNDEwMjAwNTMzNDda
ME8xCzAJBgNVBAYTAlVTMR8wHQYDVQQKExZFeHRyZW1lIE5ldHdvcmtzLCBJbmMu
MR8wHQYDVQQDExZFeHRyZW1lIE5ldHdvcmtzLCBJbmMuMIIBIjANBgkqhkiG9w0B
AQEFAAOCAQ8AMIIBCgKCAQEAwb1Ijc8GxujcjRy1ILPJL0NIPuWt4qI2hYjJuQM3
dkgZre+GywnBS0u0UA4o+uoRHBLDQACZ+nDcUWOwEOnfXz4tXTd3tX+yon9/slfc
WGTdFoHACP+rOZmStDu6FohlJCtmvEUSUYxUfuKBquMtmv2Tk8PPfu2Y+8dVAv+I
zHLWE6QjGWDF2sHZSYF1O6QBH5dE+g3Hs+aPNAhALTarUYYSI64CBe+w7xkbAOWH
6v96HiN/C2AoinmrhxdYOpRzpCaKMUWkabeLb2vND/LBqUyeN7WNAfzzVTdKNQ6v
as4caxvpixMWNNiLWWvmrIeA0eVaVJhmWbJ7j0VKJl8gMQIDAQABoxMwETAPBgNV
HRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBBAUAA4IBAQA3gbwT2UTO0Sb28XakWYAp
XfcBzQJ4z6P7OM0V+Fv8aFGMVYj7NfX3znSwNPOHAyGA2a9WH+m1dDitq+22Gjkv
ifn6z5bx+uex2ZNXhxYowSkzwTFwS6fCccu8AXt2Tss1Ipq1AAbMIKqzphYuyF02
1qmdKGy0TXoJ6ILv7KZNXihzhX2J5Kg5DZoTooUTPBtN31Odoa2Zd5Pz0P8Wvm0B
2JLH8eKAlwXEj44IgRYZa/hc97TOrcpMbxpgvfMlDvxkcYnOmr8CKaKfEvkys9GQ
SV3fX2hPGciZurN0kGIQjqmyxHG2BmljZYAqAGwSOdNxK/QLmmSRY16ie0rWignT
-----END CERTIFICATE-----

.
configure ssl privkey pregenerated
-----BEGIN RSA PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: DES-EDE3-CBC,5B1F9CE2C40BCBDE

bG8Rws94if4bDkLdOnbD2ywdBwaadiqYyp/8+42r6KnfpVI77adcOQ3g6Jaw/Z5R
b8nAI1bM1NuYQT436NVpfZ99gN1ZBP1g5b1b+O00xssQe3WNxKoVArpwowL6OeRF
wR2uJPWPm4CU2QyfoAVCE14tnyNhFvYcS7n87d8Ruzh1myAxTU1C4e3BvC0cbVu2
CbgdCm607DL+Wc9lPlxACB4UAVjbGJ7F7akD28WoClrbgrLG1Jh1eF2kZtiwWXO8
GBVM8lcZDGqgjPCq+dUkk0M3WVbFu+4x/0hWDaTa3U4BHmWT607mtPN1TqySNUwx
dgwZngBm5pIvycKmbk3yaaqVXDTWarweP2wXxNgV6o7WNuHi4OE0NJ6dOenbGzjl
fSXNGDQllV9kJMrgpKEOLlSqRQ4aIlRtdZYBz3d3wU5BEz70N5QSBdmVm3r0ijCb
MtUJCttaQr9YT418+8pC7hTfAt5u1yHCvm1D2njoDSzPB/xnPFCbkta7EFB1QOK3
R39F74DhFDIsv/kQ7H41ARv3uCw1bCC4VU4zvae3pLi0r8VYupx0pUDBA0HmzKIU
bbM/n6BcfSoI6kzFhoZOFzQjaCFgyyTY6OYWU8RcPCuILhkJ7spbd4ykzxfw9uL9
JZLYCOYxg7BGFQXTZZwK3eUwQJgOMdPrcietl9N5fO0oha6uDWiVcrdYdlbUHCAQ
e2u0RfndIxCHZ9Dq2XY5O1ksS7/D8BOP0TQzhhbYPw9uK+gsAEJ7j1/gSjccl2Vm
3e/M2iVNM0L2y4fyvttuLxT8YlRslVR44D4uw8kN7LaScNIdraYxtDUZiDrIbvnO
OrT+j56plARRjHXYI9Qzijf42l3hzyVffNcC6lIqN8W15Q9YeaLK77a6Eb13Wmng
huChytngKEtlV9ba9bkzPwPXBiTtA216YQPsTT87x86Dsnls3Yj5qIloSg6Cl302
NxOQs8WZWUStfzY98uj57uaQ+DHkokOY3ntq/qJ+DsKfuu6iywBncWO947O6N6/r
QbVIi2VRUdXIz06tBqks/5c5iDZwsmevCVpv2FwUttE6PfoLwwn0GDX01B6ODij+
dk98BHLRrOZ+iP2AovNvdRA+UKRU2URHaunHyFfJeqIWY4tZwm8Xcam0heznTciS
1KxsvdpAcD35v78kvHqY87c9r66l/MKkZgfjjYx8NeYh+BZG0g/CAwsy5px2T+F9
8gMKzAGNyeaNjf1DWANR/EnHdcaI/RmaIhNr5gDMhB9kAqE+8yhuXRrDM8KZiQRT
U9crwzSIdTAzYIIYPYJ4RU5zY1Z3tKm9gAelEfEnwwQssweCEVuYIF+zv/JV5rdI
IWjuQaU6RmMevUlOeAP62gWDBLWzIBw3P2ka+1mqrXi2RbmN5oRb03/q1FjqZbQ/
47YhYZdobY3fxocJoDwNUvAmLwWuPbJFvHQxBpa6/BFUxhNSLU9+bK2kOgtmyiFk
9hM5pHQjRSDL8XuswVkGMngCRH2g7N7ZJKhbxFXpvWnOyBz5Pu1JB9uXkFzpeNEK
0DjSgv9DajJJvJT394Zqs8fAzb3UN3J7DWa6u255nf0GW3CKYy3b+A==
-----END RSA PRIVATE KEY-----
.

# Inline-power configuration
disable inline-power
enable inline-power slot 1
configure inline-power budget 480 slot 1
enable inline-power
# LLDP
configure lldp transmit-interval 30
configure lldp transmit-hold 4
configure lldp transmit-delay 2
configure lldp reinitialize-delay 2
configure lldp snmp-notification-interval 5
disable lldp ports 1:1
disable lldp ports 1:2
disable lldp ports 1:3
disable lldp ports 1:4
disable lldp ports 1:5
disable lldp ports 1:6
disable lldp ports 1:7
disable lldp ports 1:8
disable lldp ports 1:9
disable lldp ports 1:10
disable lldp ports 1:11
disable lldp ports 1:12
disable lldp ports 1:13
disable lldp ports 1:14
disable lldp ports 1:15
disable lldp ports 1:16
disable lldp ports 1:17
disable lldp ports 1:18
disable lldp ports 1:19
disable lldp ports 1:20
disable lldp ports 1:21
disable lldp ports 1:22
disable lldp ports 1:23
disable lldp ports 1:24
disable lldp ports 1:25
disable lldp ports 1:26
disable lldp ports 1:27
disable lldp ports 1:28
disable lldp ports 1:29
disable lldp ports 1:30
disable lldp ports 1:31
disable lldp ports 1:32
disable lldp ports 1:33
disable lldp ports 1:34
disable lldp ports 1:35
disable lldp ports 1:36
disable lldp ports 1:37
disable lldp ports 1:38
disable lldp ports 1:39
disable lldp ports 1:40
disable lldp ports 1:41
disable lldp ports 1:42
disable lldp ports 1:43
disable lldp ports 1:44
disable lldp ports 1:45
disable lldp ports 1:46
disable lldp ports 1:47
disable lldp ports 1:48
disable lldp ports 1:49
disable lldp ports 1:50
disable lldp ports 1:51
disable lldp ports 1:52
# MAC Lockdown with timeout Configuration

# Gratuitous ARP Configuration

#
# End of configuration file for "Summit300-48".
#
