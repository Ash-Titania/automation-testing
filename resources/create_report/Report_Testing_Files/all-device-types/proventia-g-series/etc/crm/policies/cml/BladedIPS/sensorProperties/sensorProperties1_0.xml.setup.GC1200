<?xml version="1.0" encoding="UTF-8"?>
<policy xmlns="http://www.iss.net/cml/BladedIPS/sensorProperties" 
        xmlns:cml="http://www.iss.net/cml" 
        xmlns:common="http://www.iss.net/cml/common" 
        xmlns:cpe="http://www.iss.net/cml/cpe" 
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
        schema-version='1.0.0'>        
        <Alerts>                
                <SensorError enabled="true" notifyConsole="true" sendSNMP="false" snmp="Default" sendEmail="false" email="Default"/>
                <SensorWarning enabled="true" notifyConsole="true" sendSNMP="false" snmp="Default" sendEmail="false" email="Default"/>
                <SensorInformative enabled="true" notifyConsole="true" sendSNMP="false" snmp="Default" sendEmail="false" email="Default"/>
        </Alerts>
        <SensorQueue sensorQueueMaxSize="15000000">
                <WrapAround/>        
        </SensorQueue>        
        <HiddenParameters>        
                <HiddenParameters Enabled='true'
                    Name='sensor.adapter.format'
                    Description='Specifies the adapter number format for logging (letter or number).'>
                    <String Value='number' />
                 </HiddenParameters>
                <HiddenParameters Enabled='true'
                    Name='sensor.adapter.sequence'
                    Description='Specifies the adapter sequence scheme for adapter management.'> 
                    <String Value='sparse' />
                 </HiddenParameters>
                <HiddenParameters Enabled='true'
                    Description='Specifies the adapter pairing scheme for adapter management.'
                    Name='sensor.adapter.pair'>
                    <Boolean Value='False'/>
                </HiddenParameters>
                <HiddenParameters Enabled='true'
                    Name='adapter.Driver' 
                    Description='Crossbeam Switched Data Path Driver [SDP].'>
                    <String Value='isssdp' />
                </HiddenParameters>
                <HiddenParameters Enabled='true'
                    Name='adapter.CaptureBufferSize' 
                    Description='Size of capture buffer in bytes.'>
                    <Number Value='419430400' />
                </HiddenParameters>
                <HiddenParameters Enabled='true'
                    Name='np.quarantine.cache'
                    Description='Determines whether to cache quarantine rules.'>
                    <Boolean Value='True' />
                </HiddenParameters>
                <HiddenParameters Enabled='true'
                    Name='np.quarantine.cache.file' 
                    Description='Name of the quarantine cache file.'>
                    <String Value='/var/cache/iss-netengine/quarantine.cache' />
                </HiddenParameters>
                <HiddenParameters Enabled='true'
                    Name='np.statistics.file.npm'
                    Description='The Protection statistics file name.'>
                    <String Value='/var/iss/npmstats.dat' />
                </HiddenParameters>
        </HiddenParameters>
        <AdvancedParameters>        
                <AdvancedParameters Enabled='false'
                    Name='sensor.trace.level'
                    Description='Proventia-G log level.'>
                    <Number Value='3' />
                 </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='engine.droplog.enabled'
                    Description='Determines whether logging of dropped packets is enabled.'>
                    <Boolean Value='False' />
                 </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='engine.adapter.low-water.default'
                    Description='The minimum number of packets per traffic sampling interval which are expected to flow on each adapter.'>
                    <Number Value='1' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='engine.adapter.high-water.default'
                    Description='The number of packets per traffic sampling interval which are expected to flow on each adapter. The high-water mark is used to prevent multiple low traffic warnings from being issued when the traffic is hovering around low-water mark.'>
                    <Number Value='5' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='pam.traffic.sample'
                    Description='Enables traffic sampling for the purpose of detecting abnormal levels of network activity.'>
                    <Boolean Value='True' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='pam.traffic.sample.interval'
                    Description='The interval, expressed in seconds, at which traffic flow should be sampled for the purpose of detecting abnormal levels of network activity.'>
                    <Number Value='300' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='np.statistics'
                    Description='Determines whether logging of PAM statistics is enabled.'>
                    <String Value='on' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='np.statistics.file.pam'
                    Description='The PAM statistics file name.'>
                    <String Value='/var/iss/pamstats.dat' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='np.log.quarantine.added'
                    Description='Log the details of rules that are added to the quarantine table.'>
                    <String Value='on' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='np.log.quarantine.removed'
                    Description='Log the details of rules that are removed from the quarantine table before they expired.'>
                    <String Value='on' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='np.log.quarantine.expired'
                    Description='Log the details of rules that have expired from quarantine table.'>
                    <String Value='on' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='np.firewall.log'
                    Description='Determines whether to log the details of packets that match firewall rules that are enabled.'>
                    <String Value='on' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='np.firewall.log.prefix'
                    Description='Prefix of firewall log file name.'>
                    <String Value='/var/iss/fw' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='np.firewall.log.suffix'
                    Description='Suffix of firewall log file name.'>
                    <String Value='.log' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='np.firewall.log.size'
                    Description='Maximum size of a firewall log file in bytes.'>
                    <Number Value='1400000' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='np.firewall.log.count'
                    Description='Number of firewall log files.'>
                    <Number Value='10' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='np.log.prefix'
                    Description='Prefix of event log file name.'>
                    <String Value='/var/iss/event' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='np.log.suffix'
                    Description='Suffix of event log file name.'>
                    <String Value='.log' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='np.log.size'
                    Description='Maximum size of event log file in bytes.'>
                    <Number Value='1400000' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='np.log.count'
                    Description='Number of event log files.'>
                    <Number Value='10' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='np.drop.invalid.checksum'
                    Description='Determines whether to block packets with checksum errors in inline protection mode.'>
                    <Boolean Value='True' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='np.drop.invalid.protocol'
                    Description='Determines whether to block packets that violate protocol in inline protection mode.'>
                    <Boolean Value='True' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='np.drop.rogue.tcp.packets'
                    Description='Determines whether to block packets that are not part of a known TCP connection in inline protection mode.'>
                    <Boolean Value='False' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='false'
                    Name='np.drop.resource.error'
                    Description='Determines whether to block packets if there are insufficient resources to inspect them in inline protection mode.'>
                    <Boolean Value='False' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='true'
                    Name='sensor.mode'
                    Description='Specifies the adapter mode for the VND ID.'>
                    <String Value='InlineSimulation' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='true' 
                    Name='sensor.vnd.unanalyzed'
                    Description='Specifies the unanalyzed packet handling policy.'>
                    <String Value='Forward' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='true' 
                    Name='sensor.adapter.reset'
                    Description='Specifies the VND adapter used as the kill port.'>
                    <String Value='provgkill' />
                </AdvancedParameters>
                <AdvancedParameters Enabled='true' 
                    Name='sensor.vnd.killport'
                    Description='Specifies the kill port used for TCP resets for the specified adapter.'>
                    <String Value='thisPort' />
                </AdvancedParameters>
        </AdvancedParameters>                
</policy>
