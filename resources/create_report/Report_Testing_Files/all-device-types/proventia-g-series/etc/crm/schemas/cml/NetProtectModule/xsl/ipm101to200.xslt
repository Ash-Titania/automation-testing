<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ipm="http://www.iss.net/cml/NetProtectModule/ipm"  xmlns:ef="http://www.iss.net/cml/NetProtectModule/eventFilters">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="ipm:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetProtectModule/ipm">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">2.0.0</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="ipm:EventFiltersList">
		<xsl:element name="EventFiltersList" namespace="http://www.iss.net/cml/NetProtectModule/ipm" >	
			<xsl:copy-of select="ef:eventFiltersRuleElement" />
		</xsl:element>
	</xsl:template>

	<xsl:template match="ipm:securityEventsList">
		<xsl:copy-of select="." />
	</xsl:template>
</xsl:stylesheet>
