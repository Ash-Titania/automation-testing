<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sensorProperties="http://www.iss.net/cml/NetworkDefender/sensorProperties">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:template match="sensorProperties:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkDefender/sensorProperties">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">3.0.0</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<!-- Generic Copy everything in anything that is not handled elsewhere -->
	<xsl:template match="@*|node()">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkDefender/sensorProperties">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*" />
			</xsl:element>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="ConvertSpeedDuplex">
		<xsl:param name="originalDuplex" select="Auto"/> <!-- the default -->
		<xsl:choose>
			<xsl:when test="$originalDuplex='Auto'">
				<xsl:text>Auto</xsl:text> 
			</xsl:when>
			<xsl:when test="$originalDuplex='10H'">
				<xsl:text>10 Mb Half Duplex</xsl:text> 
			</xsl:when>			
			<xsl:when test="$originalDuplex='10F'">
				<xsl:text>10 Mb Full Duplex</xsl:text> 
			</xsl:when>				
			<xsl:when test="$originalDuplex='100H'">
				<xsl:text>100 Mb Half Duplex</xsl:text> 
			</xsl:when>				
			<xsl:when test="$originalDuplex='100F'">
				<xsl:text>100 Mb Full Duplex</xsl:text> 
			</xsl:when>
			<xsl:when test="$originalDuplex='1000F'">
				<xsl:text>1000 Mb Full Duplex</xsl:text> 
			</xsl:when>			
		</xsl:choose>
	</xsl:template>
	
	
	<xsl:template name="addHA" >
		<xsl:element name="sensorHAMode" namespace="http://www.iss.net/cml/NetworkDefender/sensorProperties" >
			<xsl:attribute name="adapterHAMode">Normal</xsl:attribute>		
		</xsl:element>
	</xsl:template>
	
	
	<xsl:template match="sensorProperties:CardManagementList" >
	<xsl:element name="CardManagementList"  namespace="http://www.iss.net/cml/NetworkDefender/sensorProperties">
		<xsl:apply-templates />
		<xsl:call-template name="addHA" />		
	</xsl:element>
	</xsl:template>

	<xsl:template match="sensorProperties:CardManagementParameters">
		<xsl:element name="CardManagementParameters" namespace="http://www.iss.net/cml/NetworkDefender/sensorProperties">
			<xsl:copy-of select="@*"/>

			<xsl:attribute name="SpeedDuplexSettings1">
				<xsl:call-template name="ConvertSpeedDuplex">
					<xsl:with-param name="originalDuplex"  select="@SpeedDuplexSettings1" />
				</xsl:call-template>
			</xsl:attribute>
			<xsl:attribute name="SpeedDuplexSettings2">
				<xsl:call-template name="ConvertSpeedDuplex">
					<xsl:with-param name="originalDuplex"  select="@SpeedDuplexSettings2" />
				</xsl:call-template>
			</xsl:attribute>			
			
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="sensorProperties:InlineProtection">
		<xsl:attribute name="AdapterMode">InlineProtection</xsl:attribute>	
	</xsl:template>
	<xsl:template match="sensorProperties:InlineSimulation">
		<xsl:attribute name="AdapterMode">InlineSimulation</xsl:attribute>	
	</xsl:template>	
		<xsl:template match="sensorProperties:Monitoring">
		<xsl:attribute name="AdapterMode">Monitoring</xsl:attribute>	
	</xsl:template>
</xsl:stylesheet>
