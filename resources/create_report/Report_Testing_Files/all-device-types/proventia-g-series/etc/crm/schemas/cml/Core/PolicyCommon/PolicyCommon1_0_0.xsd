<schema xmlns="http://www.w3.org/2001/XMLSchema"
    targetNamespace="http://www.iss.net/cml/Core/PolicyCommon"
    xmlns:PolicyCommon="http://www.iss.net/cml/Core/PolicyCommon"
    xmlns:cpe="http://www.iss.net/cml/cpe"
    elementFormDefault="qualified"
    version="1.0.0">

    <!-- This schema defines basic types common to ISS agents. -->
    <annotation>
        <appinfo><agent name="Common CML types"/></appinfo>
        <documentation>Schema for basic types common to CML.</documentation>
    </annotation>

    <!-- Include response types.  -->
    <include schemaLocation="./Responses2_0_0.xsd"/>

    <!-- Base type for 'enabled' attribute for signatures or checks. -->
    <simpleType name="EnabledSpecifier">
        <restriction base="normalizedString">
            <enumeration value="true"/>
            <enumeration value="false"/>
            <enumeration value="unspecified"/>
        </restriction>
    </simpleType>

    <!-- Base type for checks/signatures. -->
    <complexType name="CheckBase">
        <attribute name="enabled" type="PolicyCommon:EnabledSpecifier" default="unspecified"/>
    </complexType>

    <simpleType name="DayNamesType">
        <restriction base="normalizedString">
            <enumeration value="Sun"/>
            <enumeration value="Mon"/>
            <enumeration value="Tue"/>
            <enumeration value="Wed"/>
            <enumeration value="Thu"/>
            <enumeration value="Fri"/>
            <enumeration value="Sat"/>
        </restriction>
    </simpleType>

    <simpleType name="KL6DateString">
        <restriction base="normalizedString">
            <pattern value="[0-9]{4}-[0-9]{2}-[0-9]{2}"/>
            <!-- yyyy-mm-dd (this date format is susceptible to the Y10K problem). -->
        </restriction>
    </simpleType>

    <!-- CPE uses dateLocalTime to store time using the local timezone -->
    <simpleType name="dateLocalTime" cpe:classname="net.iss.cpe.plugins.DateLocalTimePopupSimpleType">
        <restriction base="dateTime"/>
    </simpleType>

    <!-- Dotted-string representation of IPv4 addresses; currently, only decimal notation for the octets is supported. -->
    <simpleType name="DottedStringIpv4Address">
        <restriction base="normalizedString">
            <pattern value="[0-9]{1,3}(\.[0-9]{1,3}){3}"/>
        </restriction>
    </simpleType>

    <!-- Dotted-string representation of IPv4 address range; currently, only decimal notation for the octets is supported. -->
    <simpleType name="Ipv4AddressRange" cpe:classname="net.iss.cpe.type.IpRangeSimpleType">
        <restriction base="normalizedString">
            <pattern value="[0-9]{1,3}(\.[0-9]{1,3}){3}-[0-9]{1,3}(\.[0-9]{1,3}){3}"/>
        </restriction>
    </simpleType>

    <!-- Dotted-string representation of IPv4 address mask; currently, only decimal notation for the octets is supported. -->
    <simpleType name="Ipv4AddressMask" cpe:classname="net.iss.cpe.type.IpMaskSimpleType">
        <restriction base="normalizedString">
            <pattern value="[0-9]{1,3}(\.[0-9]{1,3}){3}/[0-9]{1,2}"/>
        </restriction>
    </simpleType>

    <attributeGroup name="Ipv4AddressRangeAttributes">
        <attribute name="start" type="PolicyCommon:DottedStringIpv4Address" use="required"/>
        <attribute name="end" type="PolicyCommon:DottedStringIpv4Address" use="required"/>
    </attributeGroup>

    <!-- Type for task scheduling information.  -->
    <attributeGroup name="ScheduleAttributes">
        <attribute name="time" type="dateTime"/>
        <attribute name="duration" type="duration"/>
        <attribute name="frequency" type="duration"/>
    </attributeGroup>

    <!--
        All of the SingleAndRange types below allow the user to type in either a single value, a range of values, or
        select 'ALL'.  For example, the IP type would allow '192.168.2.1', or '192.168.2.1-192.168.2.5', or 'ALL'.
      -->
    <!-- Ipv4 address.  Allows single IP, IP range, or 'ALL'. -->
    <simpleType name="IpSingleAndRangeType" cpe:classname="net.iss.cpe.plugins.network.IpSingleAndRangeSimpleType">
        <restriction base="normalizedString">
            <pattern value="(\d{1,3}(\.\d{1,3}){3}(-\d{1,3}(\.\d{1,3}){3}){0,1})|ALL"/>
        </restriction>
    </simpleType>

    <!-- Udp Port value from 0 to 65535.  Allows single port, port range, or 'ALL'. -->
    <simpleType name="UdpPortSingleAndRangeType" cpe:classname="net.iss.cpe.plugins.network.UdpPortSingleAndRangeSimpleType">
        <restriction base="normalizedString">
            <pattern value="(\d{1,5}(-\d{1,5}){0,1})|ALL"/>
        </restriction>
    </simpleType>

    <!-- Udp Port value from 0 to 65535.  Allows a list of single ports and port ranges, or 'ALL'. -->
    <simpleType name="UdpPortSingleAndRangeListType" cpe:singleAndRangeList="true" cpe:classname="net.iss.cpe.plugins.network.UdpPortSingleAndRangeSimpleType">
        <restriction base="normalizedString">
            <pattern value="((\d{1,5}(-\d{1,5}){0,1})(,(\d{1,5}(-\d{1,5}){0,1}))*)|ALL"/>
        </restriction>
    </simpleType>

    <!-- Tcp Port value from 0 to 65535.  Allows single port, port range, or 'ALL'. -->
    <simpleType name="TcpPortSingleAndRangeType" cpe:classname="net.iss.cpe.plugins.network.TcpPortSingleAndRangeSimpleType">
        <restriction base="normalizedString">
            <pattern value="(\d{1,5}(-\d{1,5}){0,1})|ALL"/>
        </restriction>
    </simpleType>

    <!-- Tcp Port value from 0 to 65535.  Allows a list of single ports and port ranges, or 'ALL'. -->
    <simpleType name="TcpPortSingleAndRangeListType" cpe:singleAndRangeList="true" cpe:classname="net.iss.cpe.plugins.network.TcpPortSingleAndRangeSimpleType">
        <restriction base="normalizedString">
            <pattern value="((\d{1,5}(-\d{1,5}){0,1})(,(\d{1,5}(-\d{1,5}){0,1}))*)|ALL"/>
        </restriction>
    </simpleType>

    <!-- Port range value from 0 to 65535.  Allows only a port range.  -->
    <simpleType name="PortRangeSimpleType" cpe:classname="net.iss.cpe.plugins.network.PortRangeSimpleType">
        <restriction base="normalizedString">
            <pattern value="\d{1,5}-\d{1,5}"/>
        </restriction>
    </simpleType>

    <!--
        Icmp type and code.  Type value from 0 to 255.  Code value from 0 to 255.  Only allows a single type, but
        allows a single code or a range of code values.  For example, a type value of '3' and a code range of '2-5'
        would be represented as '3:2-3:5' in the schema.  Also allows 'ALL', which implies all types and all codes.
      -->
    <simpleType name="IcmpTypeAndCodeSingleAndRangeType" cpe:classname="net.iss.cpe.plugins.network.IcmpTypeAndCodeSingleAndRangeSimpleType">
        <restriction base="normalizedString">
            <pattern value="(\d{1,3}:\d{1,3}(-\d{1,3}:\d{1,3}){0,1})|ALL"/>
        </restriction>
    </simpleType>

    <!-- Protocol value from 0 to 255.  Allows single byte, byte range, or 'ALL'. -->
    <simpleType name="ProtocolSingleAndRangeType" cpe:classname="net.iss.cpe.plugins.network.ProtocolSingleAndRangeSimpleType">
        <restriction base="normalizedString">
            <pattern value="(\d{1,3}(-\d{1,3}){0,1})|ALL"/>
        </restriction>
    </simpleType>

    <!-- Byte value from 0 to 255.  Allows single byte, byte range, or 'ALL'. -->
    <simpleType name="ByteSingleAndRangeType" cpe:classname="net.iss.cpe.plugins.network.ZeroThroughTwoHundredFiftyFiveSingleAndRangeSimpleType">
        <restriction base="normalizedString">
            <pattern value="(\d{1,3}(-\d{1,3}){0,1})|ALL"/>
        </restriction>
    </simpleType>

    <!-- Type for storing an element's position in an ordered list -->
    <simpleType name="OrderedListOrdinal">
        <restriction base="unsignedInt"/>
    </simpleType>

    <!-- Type for specifying that a list item cannot be deleted -->
    <simpleType name="PreventDelete">
        <restriction base="boolean"/>
    </simpleType>

    <!-- Type for specifying that a list item cannot be edited -->
    <simpleType name="PreventEdit">
        <restriction base="boolean"/>
    </simpleType>

    <!-- Type for a task ID. -->
    <simpleType name="TaskIdType">
        <restriction base="normalizedString"/>
    </simpleType>

    <simpleType name="HexString32Bit">
        <restriction base="token">
            <pattern value="0[xX][0-9a-fA-F]{1,8}"/>
        </restriction>
    </simpleType>

    <!-- Type for task status. -->
    <simpleType name="TaskStatusType">
        <restriction base="PolicyCommon:HexString32Bit"/>
    </simpleType>

    <simpleType name="AssetValue">
        <restriction base="unsignedByte">
            <minInclusive value="1"/>
            <maxInclusive value="7"/>
        </restriction>
    </simpleType>

    <simpleType name="MacAddress">
        <restriction base="normalizedString">
            <pattern value="[0-9a-fA-F]{2}(-[0-9a-fA-F]{2}){5}"/>
            <pattern value="[0-9a-fA-F]{2}(:[0-9a-fA-F]{2}){5}"/>
            <pattern value="[0-9a-fA-F]{12}"/>
            <!-- MAC address with dashes.    -->
            <!-- MAC address with colons.    -->
            <!-- MAC address without dashes. -->
        </restriction>
    </simpleType>

    <simpleType name="Guid">
        <restriction base="normalizedString">
            <pattern value="[0-9a-fA-F]{8}(-[0-9a-fA-F]{4}){3}-[0-9a-fA-F]{12}"/>
            <!-- Allow GUIDs specified like this:  "FAA680E0-2872-11d5-8134-009027AF6795".  -->
        </restriction>
    </simpleType>

    <simpleType name="Password" cpe:classname="net.iss.cpe.type.PasswordSimpleType">
        <restriction base="string"/>
    </simpleType>

    <simpleType name="MD5Password" cpe:hashAlgorithm="MD5" cpe:classname="net.iss.cpe.type.PasswordSimpleType">
        <restriction base="string"/>
    </simpleType>

    <simpleType name="FilePath">
        <restriction base="normalizedString">
            <pattern value='(([A-z]:|\\)((\\|/)[^\\/:\*\?"&lt;>|]{1,255})*(\\|/)?)|((/[^/]{1,255})+/?)'/>
        </restriction>
    </simpleType>

    <simpleType name="Url">
        <restriction base="normalizedString">
            <pattern value="(([A-z]+:/)|([Ff][Ii][Ll][Ee]://[A-z]?))(/[^/]{1,255})*/?"/>
        </restriction>
    </simpleType>

    <complexType name="NameValuePair">
        <attribute name="name" type="normalizedString" use="required"/>
        <attribute name="value" type="string" use="required"/>
    </complexType>

    <complexType name="OrderedNameValuePair">
        <attribute name="name" type="normalizedString" use="required"/>
        <attribute name="value" type="string" use="required"/>
        <attribute name="ordinal" type="PolicyCommon:OrderedListOrdinal" use="required" cpe:hidden="true" cpe:displayOnlyList="true"/>
    </complexType>

    <complexType name="NameValuePairList">
        <sequence>
            <element name="param" type="PolicyCommon:NameValuePair" maxOccurs="unbounded"/>
        </sequence>
    </complexType>

    <complexType name="OrderedNameValuePairList">
        <sequence>
            <element name="param" type="PolicyCommon:OrderedNameValuePair" maxOccurs="unbounded"/>
        </sequence>
    </complexType>

    <complexType name="OptionalNameValuePairList">
        <sequence>
            <element name="param" type="PolicyCommon:NameValuePair" minOccurs="0" maxOccurs="unbounded"/>
        </sequence>
    </complexType>

    <!-- Type for mutually exclusive boolean values.  Mutex behavior is only enforced in CPE lists. -->
    <simpleType name="MutexBoolean">
        <restriction base="boolean"/>
    </simpleType>

    <!-- Type for Unique column values in a CPE table.  Also accomodates unique attribute values within a CPE element. -->
    <simpleType name="UniqueString" cpe:classname="net.iss.cpe.plugins.UniqueStringSimpleType">
        <restriction base="normalizedString"/>
    </simpleType>

    <complexType name="OrderedReadOnlyNameValuePair">
        <attribute name="name" type="normalizedString" use="required" cpe:readOnly="true" cpe:allowAdd="false" cpe:allowDelete="false"/>
        <attribute name="value" type="string" use="required" cpe:readOnly="true" cpe:allowAdd="false" cpe:allowDelete="false"/>
        <attribute name="ordinal" type="PolicyCommon:OrderedListOrdinal" use="required" cpe:hidden="true" cpe:displayOnlyList="true"/>
    </complexType>

    <complexType name="OrderedReadOnlyNameValuePairList">
        <sequence>
            <element name="param" type="PolicyCommon:OrderedReadOnlyNameValuePair" maxOccurs="unbounded" cpe:branch="true" cpe:displayOnlyList="true"/>
        </sequence>
    </complexType>
</schema>
