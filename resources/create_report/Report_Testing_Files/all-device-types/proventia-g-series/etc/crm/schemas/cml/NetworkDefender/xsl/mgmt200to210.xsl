<?xml version="1.0" encoding="UTF-8"?>
<!--  This transform converts the mgmt policy from 2.0.0 to 2.1.0
  It also fixes the Proxy Port and EventServerPort attributes by setting them to valid defaults
if they're absent or set to 0
 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:mgmt="http://www.iss.net/cml/NetworkDefender/mgmt"  xmlns:cml="http://www.iss.net/cml" xmlns:cpe="http://www.iss.net/cml/cpe" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:common="http://www.iss.net/cml/common" xmlns:PolicyBase="http://www.iss.net/cml/PolicyBase"  >
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:template match="mgmt:policy">
		<xsl:element name="policy"  namespace="http://www.iss.net/cml/NetworkDefender/mgmt">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">2.1.0</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="mgmt:AgentManagerXXXXX"	>
		<xsl:for-each select="@*|node()">
				  <xsl:value-of select="." />
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template match="mgmt:AgentManager"	>
		<xsl:element name="AgentManager" namespace="http://www.iss.net/cml/NetworkDefender/mgmt">	
			<xsl:copy-of select="@*[name() != 'ProxyPort' and name() != 'EventServerPort' ]"/>	
			<xsl:attribute name="ProxyPort">
			   <xsl:choose>
							<xsl:when test="@ProxyPort='0'">
							   <xsl:value-of select="3128"/>
							</xsl:when>
							<xsl:when test="count(@ProxyPort)='0'">
							  <xsl:value-of select="3128"/>
							</xsl:when>
							<xsl:otherwise>
							  <xsl:value-of select="@ProxyPort" />
							</xsl:otherwise>	
						</xsl:choose>
			</xsl:attribute>
			
			<xsl:attribute name="EventServerPort">
			   <xsl:choose>
							<xsl:when test="@EventServerPort='0'">
							   <xsl:value-of select="3995"/>
							</xsl:when>
							<xsl:when test="count(@EventServerPort)='0'">
							  <xsl:value-of select="3995"/>
							</xsl:when>							
							<xsl:otherwise>
							  <xsl:value-of select="@EventServerPort" />
							</xsl:otherwise>	
						</xsl:choose>
			</xsl:attribute>			
			</xsl:element>	
	</xsl:template>
	
	<xsl:template match="mgmt:Config">
		<xsl:element name="Config" namespace="http://www.iss.net/cml/NetworkDefender/mgmt">
		<xsl:copy-of select="@*" />
		<xsl:apply-templates/>
</xsl:element>		
	</xsl:template>
</xsl:stylesheet>
