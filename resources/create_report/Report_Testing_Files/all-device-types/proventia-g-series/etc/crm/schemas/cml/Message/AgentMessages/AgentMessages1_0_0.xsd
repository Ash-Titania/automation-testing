<schema targetNamespace='http://www.iss.net/cml/Message/AgentMessages'
    xmlns='http://www.w3.org/2001/XMLSchema'
    xmlns:iss='http://www.iss.net'
    xmlns:Message='http://www.iss.net/cml/Message'
    xmlns:MessageCommon='http://www.iss.net/cml/Core/MessageCommon'
    xmlns:AgentMessages='http://www.iss.net/cml/Message/AgentMessages'
    xmlns:PolicyBase='http://www.iss.net/cml/Core/PolicyBase'
    xmlns:AvpSections='http://www.iss.net/cml/CommonDocuments/AvpSections'
    xmlns:RoAvpSections='http://www.iss.net/cml/CommonDocuments/RoAvpSections'
    elementFormDefault='qualified'
    version='1.0.0'>

    <annotation>
        <appinfo><agent name='Standard CML'/></appinfo>
        <documentation>Schema for ISS's Common Messaging Language (CML) agent messages.</documentation>
    </annotation>

    <import namespace='http://www.iss.net'                           schemaLocation='../../../Iss1_0_1.xsd'/>
    <import namespace='http://www.iss.net/cml/Core/PolicyBase'       schemaLocation='../../Core/PolicyBase/PolicyBase2_3_3.xsd'/>
    <import namespace='http://www.iss.net/cml/Core/MessageCommon'    schemaLocation='../../Core/MessageCommon/MessageCommon1_0_0.xsd'/>
    <import namespace='http://www.iss.net/cml/Message'               schemaLocation='../Message1_0_0.xsd'/>

    <!-- Import schemas for common documents.  -->
    <import namespace='http://www.iss.net/cml/CommonDocuments/AvpSections'    schemaLocation='../../CommonDocuments/AvpSections/AvpSections2_0_1.xsd' />
    <import namespace='http://www.iss.net/cml/CommonDocuments/RoAvpSections'  schemaLocation='../../CommonDocuments/RoAvpSections/RoAvpSections2_0_1.xsd' />

    <!-- A NOTE ON MESSAGE IDENTIFIERS:  This schema defines several message identifier attributes
         (request-id, response-to, message-id).  CML treats these as character strings, and does not
         specify their contents.  It is left to the agents to ensure that any message identifiers they
         generate are unique across agents and for sufficient time duration.  This implies that at
         least part of the content of a message identifier should be unique to the generating agent.
         A GUID or a message ID of the form "agent-name.timestamp" would probably suffice.  -->

    <complexType name='SchemaInfoList'>
        <sequence>
            <element name='schema-info' type='Message:SchemaSpecifier' maxOccurs='unbounded'/>
        </sequence>
    </complexType>

    <!-- Request types. -->

    <attributeGroup name='AgentInfoItems'>
        <attribute name='name'                   type='normalizedString'   use='required'/>
        <attribute name='agent-id'               type='MessageCommon:Guid' use='required'/>
        <attribute name='namespace'              type='normalizedString'   use='required'/>
        <attribute name='version'                type='normalizedString'   use='required'/>
        <attribute name='model'                  type='normalizedString'/>
        <attribute name='xpu-version'            type='iss:VersionString'/>
        <attribute name='agent-install-path'     type='normalizedString'/>
        <attribute name='daemon-install-path'    type='normalizedString'/>
        <attribute name='agent-status'           type='MessageCommon:TaskStatusType'/>
        <attribute name='status-text'            type='string'/>
        <attribute name='last-xpu-path'          type='MessageCommon:FilePath'/>
        <attribute name='xpu-status'             type='unsignedInt'/>
        <attribute name='xpu-status-description' type='normalizedString'/>
        <attribute name='groupname-hint'         type='normalizedString'/>
        <attribute name='use-local-settings'     type='boolean' default='false'/>
        <attribute name='send-new-schemas'       type='boolean' default='true'/>
        <attribute name='register-agent'         type='boolean'/>
        <attribute name='licensing-model'        type='unsignedInt' default='0'/>
    </attributeGroup>

    <complexType name='HostInfoType'>
        <attribute name='os'           type='normalizedString' use='required'/>   <!-- OS string                          -->
        <attribute name='version'      type='normalizedString' use='required'/>   <!-- OS version                         -->
        <attribute name='ip-address'   type='MessageCommon:DottedStringIpv4Address'/>    <!-- agent's IP address                 -->
        <attribute name='host-name'    type='normalizedString'/>                  <!-- agent's host name                  -->
        <attribute name='nbname'       type='normalizedString'/>                  <!-- agent's NetBIOS name               -->
        <attribute name='nbdomain'     type='normalizedString'/>                  <!-- agent's NetBIOS domain             -->
        <attribute name='ad-object-id' type='MessageCommon:Guid'/>                       <!-- agent's Active Directory object ID -->
        <attribute name='ad-group-id'  type='MessageCommon:Guid'/>                       <!-- agent's Active Directory group ID  -->
    </complexType>

    <attributeGroup name='ContentDateSpecifier'>
        <attribute name='identity' type='normalizedString'            use='required'/>
        <attribute name='date'     type='MessageCommon:KL6DateString' use='required'/>
    </attributeGroup>

    <complexType name='ContentDateList'>
        <sequence>
            <element name='content-date' maxOccurs='unbounded'>
                <complexType>
                    <attributeGroup ref='AgentMessages:ContentDateSpecifier'/>
                </complexType>
            </element>
        </sequence>
    </complexType>

    <attributeGroup name='ModuleInfoSpecifier'>
        <attribute name='name'        type='normalizedString'  use='required'/>
        <attribute name='version'     type='iss:VersionString' use='required'/>
        <attribute name='status'      type='MessageCommon:TaskStatusType'/>
        <attribute name='status-text' type='string'/>
        <attribute name='licensed'    type='boolean'/>
    </attributeGroup>

    <complexType name='ModuleList'>
        <sequence>
            <element name='module' maxOccurs='unbounded'>
                <complexType>
                    <sequence>
                        <element ref='PolicyBase:policy-base' minOccurs='0' maxOccurs='unbounded'/>
                    </sequence>
                    <attributeGroup ref='AgentMessages:ModuleInfoSpecifier'/>
                </complexType>
            </element>
        </sequence>
    </complexType>

    <attributeGroup name='LicenseInfoAttributes'>
        <attribute name='serial-number'               type='MessageCommon:Guid'          use='required'/>
        <attribute name='ocn'                         type='token'                       use='required'/>
        <attribute name='identity'                    type='normalizedString'            use='required'/>
        <attribute name='current-date'                type='MessageCommon:KL6DateString' use='required'/>
        <attribute name='expiration-date'             type='MessageCommon:KL6DateString' use='required'/>
        <attribute name='maintenance-expiration-date' type='MessageCommon:KL6DateString' use='required'/>
        <attribute name='content-date'                type='MessageCommon:KL6DateString' use='required'/>
        <attribute name='device-limit'                type='unsignedLong'                use='required'/>
        <attribute name='device-count'                type='unsignedLong'/>
    </attributeGroup>

    <complexType name='LicenseStatusSpecifier'>
        <attributeGroup ref='AgentMessages:LicenseInfoAttributes'/>
    </complexType>

    <complexType name='LicenseStatusList'>
        <sequence>
            <element name='license' type='AgentMessages:LicenseStatusSpecifier' maxOccurs='unbounded'/>
        </sequence>
    </complexType>

    <attributeGroup name='UpdateAttributes'>
        <attribute name='module'  type='normalizedString'/>
        <attribute name='type'    type='normalizedString'/>
        <attribute name='version' type='iss:VersionString'/>
    </attributeGroup>

    <complexType name='UpdateSpecifier'>
        <attributeGroup ref='AgentMessages:UpdateAttributes'/>
    </complexType>

    <complexType name='UpdateList'>
        <sequence>
            <element name='update' type='AgentMessages:UpdateSpecifier' maxOccurs='unbounded'/>
        </sequence>
    </complexType>

    <group name='AgentInfoContent'>
        <sequence>
            <element name='host-info'         type='AgentMessages:HostInfoType'      minOccurs='0'/>
            <element name='schema-list'       type='AgentMessages:SchemaInfoList'    minOccurs='0'/>
            <element name='content-date-list' type='AgentMessages:ContentDateList'   minOccurs='0'/>
            <element name='module-list'       type='AgentMessages:ModuleList'        minOccurs='0'/>
            <element name='license-list'      type='AgentMessages:LicenseStatusList' minOccurs='0'/>
            <element name='update-list'       type='AgentMessages:UpdateList'        minOccurs='0'/>
            <element name='copyright-info'    type='string'                          minOccurs='0'/>
            <element name='patent-info'       type='string'                          minOccurs='0'/>
            <sequence>
                <element ref='PolicyBase:policy-base' minOccurs='0' maxOccurs='unbounded'/>
            </sequence>
        </sequence>
    </group>

    <complexType name='AgentStartupAnnouncement'>
        <complexContent>
            <extension base='Message:BasicRequest'>
                <sequence>
                    <group ref='AgentMessages:AgentInfoContent'/>
                </sequence>
                <attributeGroup ref='AgentMessages:AgentInfoItems'/>
            </extension>
        </complexContent>
    </complexType>

    <group name='ModuleNameList'>
        <sequence>
            <element name='module'>
                <complexType>
                    <attribute name='name' type='normalizedString' use='required'/>
                </complexType>
            </element>
        </sequence>
    </group>

    <complexType name='GetAgentStatusType'>
        <complexContent>
            <extension base='Message:BasicRequest'>
                <group ref='AgentMessages:ModuleNameList' minOccurs='0' maxOccurs='unbounded'/>
            </extension>
        </complexContent>
    </complexType>

    <attributeGroup name='AgentStatusItems'>
        <attribute name='status' type='MessageCommon:TaskStatusType' use='required'/>
    </attributeGroup>

    <group name='AgentStatusContent'>
        <sequence>
            <element name='module-list' type='AgentMessages:ModuleList' minOccurs='0'/>
            <sequence>
                <element ref='PolicyBase:policy-base' minOccurs='0' maxOccurs='unbounded'/>
            </sequence>
        </sequence>
    </group>

    <complexType name='AgentStatusAnnouncement'>
        <complexContent>
            <extension base='Message:BasicRequest'>
                <sequence>
                    <group ref='AgentMessages:AgentStatusContent'/>
                </sequence>
                <attributeGroup ref='AgentMessages:AgentStatusItems'/>
            </extension>
        </complexContent>
    </complexType>

    <attributeGroup name='AuthenticationAttributes'>
        <attribute name='user-name' type='normalizedString' use='required'/>
        <attribute name='passwd' type='normalizedString' use='required'/>
    </attributeGroup>

    <complexType name='AttachToSiteRequest'>
        <complexContent>
            <extension base='Message:BasicRequest'>
                <attributeGroup ref='AgentMessages:AuthenticationAttributes'/>
            </extension>
        </complexContent>
    </complexType>

    <!-- Response types. -->

    <complexType name='AgentInfoResponse'>
        <complexContent>
            <extension base='Message:BasicResponse'>
                <sequence>
                    <group ref='AgentMessages:AgentInfoContent'/>
                </sequence>
                <attributeGroup ref='AgentMessages:AgentInfoItems'/>
            </extension>
        </complexContent>
    </complexType>

    <complexType name='GetAgentStatusResponse'>
        <complexContent>
            <extension base='Message:BasicResponse'>
                <sequence>
                    <group ref='AgentMessages:AgentStatusContent'/>
                </sequence>
                <attributeGroup ref='AgentMessages:AgentStatusItems'/>
            </extension>
        </complexContent>
    </complexType>

    <!-- Standard CML agent requests. -->
    <element name='agent-startup-announcement'     type='AgentMessages:AgentStartupAnnouncement' substitutionGroup='Message:request-base'/>
    <element name='agent-shutdown-announcement'    type='Message:BasicRequest'                   substitutionGroup='Message:request-base'/>
    <element name='get-agent-info-request'         type='Message:BasicRequest'                   substitutionGroup='Message:request-base'/>
    <element name='reset-agent-request'            type='Message:BasicRequest'                   substitutionGroup='Message:request-base'/>
    <element name='get-agent-status-request'       type='AgentMessages:GetAgentStatusType'       substitutionGroup='Message:request-base'/>
    <element name='agent-status-announcement'      type='AgentMessages:AgentStatusAnnouncement'  substitutionGroup='Message:request-base'/>
    <element name='attach-to-site-request'         type='AgentMessages:AttachToSiteRequest'      substitutionGroup='Message:request-base'/>

    <!-- Standard CML agent management responses. -->
    <element name='agent-startup-acknowledgement'  type='Message:BasicResponse'                  substitutionGroup='Message:response-base'/>
    <element name='agent-shutdown-acknowledgement' type='Message:BasicResponse'                  substitutionGroup='Message:response-base'/>
    <element name='get-agent-info-response'        type='AgentMessages:AgentInfoResponse'        substitutionGroup='Message:response-base'/>
    <element name='reset-agent-response'           type='Message:BasicResponse'                  substitutionGroup='Message:response-base'/>
    <element name='get-agent-status-response'      type='AgentMessages:GetAgentStatusResponse'   substitutionGroup='Message:response-base'/>
    <element name='agent-status-acknowledgement'   type='Message:BasicResponse'                  substitutionGroup='Message:response-base'/>
    <element name='attach-to-site-response'        type='Message:BasicResponse'                  substitutionGroup='Message:response-base'/>

</schema>
