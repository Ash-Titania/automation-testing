<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:upds="http://www.iss.net/cml/NetworkDefender/upds">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	<xsl:template match="upds:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkDefender/upds">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">2.0.0</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="upds:UpdateSettings">
		<xsl:copy-of select="." />
	</xsl:template>
	<xsl:template match="upds:TuningSetting">
		<xsl:copy-of select="." />
	</xsl:template>


</xsl:stylesheet>
