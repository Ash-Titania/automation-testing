<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:sensorProperties="http://www.iss.net/cml/NetworkDefender/sensorProperties"  xmlns:ha='http://www.iss.net/cml/NetworkDefender/ha'  >
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	
	<xsl:template match="sensorProperties:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetworkDefender/sensorProperties">
			<xsl:copy-of select="@*" />
			 <xsl:apply-templates select="node()" /> 
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="ha:policy">
		 <!--  do nothing -->
			 <xsl:apply-templates select="node()" /> 		 
	</xsl:template>	
	
	<!-- Generic Copy everything in anything that is not handled elsewhere -->
	<xsl:template match="@*|node()">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/NetworkDefender/sensorProperties">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*" />
			</xsl:element>
		</xsl:if>
	</xsl:template>

	<xsl:template match="sensorProperties:sensorHAMode">
		<xsl:apply-templates select="document('ha1_0.xml')"  />
	</xsl:template>	
	

	<xsl:template match="ha:SensorMode" >
		<xsl:apply-templates select="*"	/>
	</xsl:template>
	
	<!--  For each of the 3 modes we support inside of HA,  emit a correspondingly friendly sensorCapabilities element  -->		
	
	<xsl:template match="ha:NormalMode" >
		<xsl:element name="sensorHAMode"  namespace="http://www.iss.net/cml/NetworkDefender/sensorProperties" >
			<xsl:attribute name="adapterHAMode">Normal</xsl:attribute>
	</xsl:element>		
	</xsl:template>
	
	<xsl:template match="ha:HASimulationMode" >
		<xsl:element name="sensorHAMode"  namespace="http://www.iss.net/cml/NetworkDefender/sensorProperties" >
			<xsl:attribute name="adapterHAMode">HA Simulation</xsl:attribute>
	</xsl:element>		
	</xsl:template>	
	
	<xsl:template match="ha:HAProtectionMode" >
		<xsl:element name="sensorHAMode"  namespace="http://www.iss.net/cml/NetworkDefender/sensorProperties" >
			<xsl:attribute name="adapterHAMode">HA Protection</xsl:attribute>
	</xsl:element>		
	</xsl:template>	

</xsl:stylesheet>
