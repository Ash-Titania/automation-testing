<?xml version="1.0" encoding="UTF-8"?>
<!--
	This transform takes an LMILinks document from version 1.0.0 to version 1.0.1.    Note that your input document must meet the following criteria:
	1)  the document namespace must ALREADY be  http://www.iss.net/cml/CommonDocuments/LmiLinks.
	2) the LMILinks document must be valid, meaning that  the lmi and phoneHome elements should be correct.

	Example Xalan invocation:
	Xalan.exe  -o output.xml -i 5 -p agentIP '172.16.249.1 -p agentNameSpace 'https://www.iss.net/cml/NetworkDefender' lmilinks1_0.xml lmiLinks100to101.xslt
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:lmiLinks="http://www.iss.net/cml/CommonDocuments/LmiLinks">
	<xsl:output method="xml" encoding="UTF-8" indent="yes"/>
	
	<xsl:param name="agentNameSpace" select="'!!!ENTER YOUR NAMESPACE ON THE COMMAND LINE!!!'"/>
	<xsl:param name="agentIP" select="'!!!ENTER YOUR IP ON THE COMMAND LINE!!!'"/> 
	<xsl:template match="lmiLinks:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/CommonDocuments/LmiLinks">
		
			<xsl:copy-of select="@*[not(contains(name(), 'version'))]"/>  
			<xsl:attribute name="schema-version">1.0.1</xsl:attribute>
			<xsl:attribute name="version">1.0</xsl:attribute>			
			<xsl:apply-templates/>				
			
			<!--  Append the Notiification Element for this namespace -->
			<xsl:element name="notifications" namespace="http://www.iss.net/cml/CommonDocuments/LmiLinks">
				<xsl:element name="notification" namespace="http://www.iss.net/cml/CommonDocuments/LmiLinks" >
					<xsl:attribute name="URI"><xsl:value-of select="$agentNameSpace"/>/command/applyXpu</xsl:attribute>
					<xsl:element name="link" namespace="http://www.iss.net/cml/CommonDocuments/LmiLinks" >
						<xsl:attribute name="URL">https://<xsl:value-of select="$agentIP"/>/spa/spa_dotaskrequest.php</xsl:attribute>
					</xsl:element>
				</xsl:element>
				<xsl:element name="notification" namespace="http://www.iss.net/cml/CommonDocuments/LmiLinks" >
					<xsl:attribute name="URI"><xsl:value-of select="$agentNameSpace"/>/command/removeXpu</xsl:attribute>
					<xsl:element name="link" namespace="http://www.iss.net/cml/CommonDocuments/LmiLinks" >
						<xsl:attribute name="URL">https://<xsl:value-of select="$agentIP"/>/spa/spa_dotaskrequest.php</xsl:attribute>
					</xsl:element>
				</xsl:element>
			</xsl:element>
		</xsl:element>
	</xsl:template>
	

	<!-- Generic Copy everything in anything that is not handled elsewhere -->
	<xsl:template match="@*|node()">
		<xsl:if test="string-length(name(.)) > 0">
			<xsl:element name="{name(.)}" namespace="http://www.iss.net/cml/CommonDocuments/LmiLinks">
				<xsl:copy-of select="@*"/>
				<xsl:apply-templates select="current()/*" />
			</xsl:element>
		</xsl:if>

	</xsl:template>
	
</xsl:stylesheet>
