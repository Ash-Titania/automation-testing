<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ud="http://www.iss.net/cml/NetProtectModule/userDefined" >
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="ud:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetProtectModule/userDefined">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">1.1.0</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	
<xsl:template match="ud:UserDefinedListElement"  >
		<xsl:element name="UserDefinedListElement" namespace="http://www.iss.net/cml/NetProtectModule/userDefined" >	
			<xsl:attribute name="virtualSensor">Global</xsl:attribute>		
			<xsl:copy-of select="@*|node()"/>
		</xsl:element>	
</xsl:template>

</xsl:stylesheet>

