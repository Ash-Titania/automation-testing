                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:policy="http://www.iss.net/cml/GroupSettings">
	<xsl:output method="xml" encoding="UTF-8"/>
	<xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>
	
	<xsl:template match="policy:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/GroupSettings">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">2.3.1</xsl:attribute>
			
			<xsl:copy-of select="policy:poster-info"/>

			<xsl:element name="controller-list" namespace="http://www.iss.net/cml/GroupSettings">
				<xsl:for-each select="policy:controller-list/policy:controller-info">
 					<xsl:element name="controller-info" namespace="http://www.iss.net/cml/GroupSettings">
						<xsl:for-each select="attribute::node()">
							<xsl:attribute name="{local-name()}">
								<xsl:value-of select="."/>
							</xsl:attribute>
						</xsl:for-each>
						<xsl:attribute name="trust-level">first-time-trust</xsl:attribute>
					</xsl:element>
				</xsl:for-each>
			</xsl:element>
			
			<xsl:copy-of select="policy:adaptive-policy-settings"/>

		
		</xsl:element>
	</xsl:template>
	
</xsl:stylesheet>
