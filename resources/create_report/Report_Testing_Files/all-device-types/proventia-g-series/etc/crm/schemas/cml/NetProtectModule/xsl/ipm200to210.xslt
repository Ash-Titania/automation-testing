<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ipm="http://www.iss.net/cml/NetProtectModule/ipm">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="ipm:policy">
		<xsl:element name="policy" namespace="http://www.iss.net/cml/NetProtectModule/ipm">
			<xsl:copy-of select="@*[name() != 'schema-version']"/>
			<xsl:attribute name="schema-version">2.1.0</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="ipm:EventFiltersList">
		<xsl:copy-of select="." />
	</xsl:template>

	<xsl:template match="ipm:securityEventsList">
		<xsl:copy-of select="." />
	</xsl:template>
</xsl:stylesheet>
