<!-- iss/cml schema -->
<schema targetNamespace='http://www.iss.net/cml'
    xmlns='http://www.w3.org/2001/XMLSchema'
    xmlns:iss='http://www.iss.net'
    xmlns:common='http://www.iss.net/cml/common'
    xmlns:cml='http://www.iss.net/cml'
    xmlns:config='http://www.iss.net/cml/config'
    elementFormDefault='qualified'
    version='1.0.0'>

    <annotation>
        <appinfo><agent name='Standard CML'/></appinfo>
        <documentation>Schema for ISS's Common Messaging Language (CML).</documentation>
    </annotation>

    <!-- Import the 'iss' namespace.  This namespace defines all the
    namespaces, elements and types that are common to all ISS XML documents. -->
    <import namespace='http://www.iss.net' schemaLocation='../Iss1_0_0.xsd'/>

    <!-- Import the 'common' namespace.  This namespace defines all the
    namespaces, elements and types that are common to ISS CML-aware agents. -->
    <import namespace='http://www.iss.net/cml/common' schemaLocation='./common/Common1_0_0.xsd'/>

    <!-- Include policy and configuration types.  -->
    <include schemaLocation='./PolicyBase1_0_0.xsd'/>
    <import namespace='http://www.iss.net/cml/config' schemaLocation='./config/Config1_0_0.xsd'/>

    <!-- A NOTE ON MESSAGE IDENTIFIERS:  This schema defines several message identifier attributes
         (request-id, response-to, message-id).  CML treats these as character strings, and does not
         specify their contents.  It is left to the agents to ensure that any message identifiers they
         generate are unique across agents and for sufficient time duration.  This implies that at
         least part of the content of a message identifier should be unique to the generating agent.
         A GUID or a message ID of the form "agent-name.timestamp" would probably suffice.  -->

    <attributeGroup name='SchemaInfoItems'>
        <attribute name='namespace' type='normalizedString' use='required'/>
        <attribute name='version' type='iss:VersionString'/>
    </attributeGroup>

    <!-- Request types. -->

    <!-- Standard request attributes. -->
    <attributeGroup name='StandardRequestItems'>
        <attribute name='request-id' type='normalizedString' use='required'/>
        <attribute name='schema-version' type='iss:VersionString' use='required'/>
    </attributeGroup>

    <!-- Base type for all requests and responses.  -->
    <complexType name='MessageBase'>
        <attributeGroup ref='cml:StandardRequestItems'/>
    </complexType>

    <!-- Base type for requests. -->
    <complexType name='BasicRequest'>
        <complexContent>
            <extension base='cml:MessageBase'>
                <attribute name='need-reply' type='boolean' default='true'/>
            </extension>
        </complexContent>
    </complexType>

    <!-- Base type for a request that contains a single text element. -->
    <complexType name='BasicTextRequest'>
        <complexContent>
            <extension base='cml:BasicRequest'>
                <sequence>
                    <element name='text' type='normalizedString'/>
                </sequence>
            </extension>
        </complexContent>
    </complexType>

    <complexType name='SchemaRequest'>
        <complexContent>
            <extension base='cml:BasicRequest'>
                <sequence>
                    <element name='schema-info' maxOccurs='unbounded'>
                        <complexType>
                            <attributeGroup ref='cml:SchemaInfoItems'/>
                        </complexType>
                    </element>
                </sequence>
            </extension>
        </complexContent>
    </complexType>

    <complexType name='PolicyRequestBase'>
        <complexContent>
            <extension base='cml:PolicyIdentifierBase'>
                <attribute name='tag' type='normalizedString' default='policy'/>
            </extension>
        </complexContent>
    </complexType>

    <complexType name='GetPolicyRequest'>
        <complexContent>
            <extension base='cml:BasicRequest'>
                <sequence>
                    <element name='policy-item' type='cml:PolicyRequestBase' maxOccurs='unbounded'/>
                </sequence>
            </extension>
        </complexContent>
    </complexType>

    <complexType name='SetPolicyRequest'>
        <complexContent>
            <extension base='cml:BasicRequest'>
                <sequence maxOccurs='unbounded'>
                    <element name='policy-item' type='cml:PolicySpecifierBase'/>
                    <element ref='cml:policy-base'/>
                </sequence>
            </extension>
        </complexContent>
    </complexType>

    <complexType name='ConfigureAgentRequest'>
        <!-- An agent configuration request must contain either a policy (of agent property settings)
        or a reference to a policy. -->
        <complexContent>
            <extension base='cml:BasicRequest'>
                <choice>
                    <element name='policy-ref' type='cml:PolicyIdentifierBase'/>
                    <element ref='cml:policy-base'/>
                </choice>
            </extension>
        </complexContent>
    </complexType>

    <complexType name='TaskDescriptor'>
        <!-- Scheduling information is standard; extend this base type to add agent-specific task items. -->
        <sequence>
            <element name='schedule' minOccurs='0'>
                <complexType>
                    <attributeGroup ref='common:ScheduleAttributes'/>
                </complexType>
            </element>
            <choice>
                <element name='policy-ref' type='cml:PolicyIdentifierBase'/>
                <element ref='cml:policy-base'/>
            </choice>
        </sequence>
        <attribute name='name' type='normalizedString'/>
        <attribute name='description' type='normalizedString'/>
    </complexType>

    <!-- Head element to allow substitution of extended agent-specific task descriptors. -->
    <element name='task' type='cml:TaskDescriptor'/>

    <complexType name='ConfigureTaskRequest'>
        <!-- A task configuration request may contain a task descriptor, and must contain either a policy or
        a reference to a policy. -->
        <complexContent>
            <extension base='cml:BasicRequest'>
                <sequence>
                    <element ref='cml:task' maxOccurs='unbounded'/>
                </sequence>
            </extension>
        </complexContent>
    </complexType>

    <complexType name='TaskStatusRequest'>
        <complexContent>
            <extension base='cml:BasicRequest'>
                <sequence>
                    <element name='task' maxOccurs='unbounded'>
                        <complexType>
                            <attribute name='task-id' type='common:TaskIdType' use='required'/>
                        </complexType>
                    </element>
                </sequence>
            </extension>
        </complexContent>
    </complexType>

    <!-- Response types. -->

    <!-- Standard response attributes. -->
    <attributeGroup name='ResponseCodes'>
        <attribute name='code' type='unsignedInt' default='0'/>
        <attribute name='text' type='normalizedString'/>
    </attributeGroup>

    <!-- Base type for all responses.  -->
    <complexType name='BasicResponse'>
        <complexContent>
            <extension base='cml:MessageBase'>
                <attributeGroup ref='cml:ResponseCodes'/>
            </extension>
        </complexContent>
    </complexType>

    <complexType name='BasicTextResponse'>
        <complexContent>
            <extension base='cml:BasicResponse'>
                <sequence>
                    <element name='text' type='normalizedString'/>
                </sequence>
            </extension>
        </complexContent>
    </complexType>

    <complexType name='SchemaNotSupportedResponse'>
        <complexContent>
            <extension base='cml:BasicResponse'>
                <sequence>
                    <element name='schema-info' maxOccurs='unbounded'>
                        <complexType>
                            <attributeGroup ref='cml:SchemaInfoItems'/>
                        </complexType>
                    </element>
                </sequence>
            </extension>
        </complexContent>
    </complexType>

    <complexType name='SchemaInfoResponseElement'>
        <attributeGroup ref='cml:SchemaInfoItems'/>
        <attributeGroup ref='cml:ResponseCodes'/>
    </complexType>

    <complexType name='GetSchemaInfoResponse'>
        <complexContent>
            <extension base='cml:BasicResponse'>
                <sequence maxOccurs='unbounded'>
                    <element name='schema-info' type='cml:SchemaInfoResponseElement'/>
                </sequence>
            </extension>
        </complexContent>
    </complexType>

    <complexType name='GetSchemaResponse'>
        <complexContent>
            <extension base='cml:BasicResponse'>
                <sequence maxOccurs='unbounded'>
                    <element name='schema-info' type='cml:SchemaInfoResponseElement'/>
                    <element name='schema-content'>
                        <complexType>
                            <sequence>
                                <any namespace='http://www.w3.org/2001/XMLSchema' processContents='lax'/>
                            </sequence>
                        </complexType>
                    </element>
                </sequence>
            </extension>
        </complexContent>
    </complexType>

    <complexType name='AgentInfoResponse'>
        <complexContent>
            <extension base='cml:BasicResponse'>
                <attribute name='name' type='normalizedString' use='required'/>
                <attribute name='type' type='normalizedString' use='required'/>
                <attribute name='version' type='iss:VersionString' use='required'/>
                <attribute name='xpu-version' type='iss:VersionString'/>
            </extension>
        </complexContent>
    </complexType>

    <complexType name='PolicyItemResponseElement'>
        <complexContent>
            <extension base='cml:PolicyRequestBase'>
                <attributeGroup ref='cml:ResponseCodes'/>
            </extension>
        </complexContent>
    </complexType>

    <complexType name='GetPolicyResponse'>
        <complexContent>
            <extension base='cml:BasicResponse'>
                <sequence maxOccurs='unbounded'>
                    <element name='policy-item' type='cml:PolicyItemResponseElement'/>
                    <element ref='cml:policy-base'/>
                </sequence>
            </extension>
        </complexContent>
    </complexType>

    <complexType name='SetPolicyResponse'>
        <complexContent>
            <extension base='cml:BasicResponse'>
                <sequence maxOccurs='unbounded'>
                    <element name='policy-item' type='cml:PolicyItemResponseElement'/>
                </sequence>
            </extension>
        </complexContent>
    </complexType>

    <!-- Type for a single task status response. -->
    <complexType name='TaskStatus'>
        <attribute name='name' type='normalizedString'/>
        <attribute name='task-id' type='common:TaskIdType' use='required'/>
        <attribute name='status' type='common:TaskStatus' use='required'/>
        <attributeGroup ref='cml:ResponseCodes'/>
    </complexType>

    <complexType name='TaskStatusResponse'>
        <complexContent>
            <extension base='cml:BasicResponse'>
                <sequence>
                    <element name='task' type='cml:TaskStatus' maxOccurs='unbounded'/>
                </sequence>
            </extension>
        </complexContent>
    </complexType>

    <!-- Declaration of the CML message elements; specifies an envelope containing
         source and destination elements as well as the message body itself. -->
    <group name='MessageElements'>
        <sequence>
            <element name='message-envelope'>
                <complexType>
                    <sequence>
                        <element name='source' type='config:AgentInfoBase'/>
                        <element name='destination' type='config:AgentInfoBase'/>
                    </sequence>
                    <attribute name='broadcast' type='boolean' default='false'/>
                    <attribute name='message-id' type='normalizedString' use='required'/>
                    <attribute name='response-to' type='normalizedString'/>
                </complexType>
            </element>

            <element name='message-body'>
                <complexType>
                    <sequence maxOccurs='unbounded'>
                        <!-- Requests are children of this element. -->
                        <element name='request' minOccurs='0'>
                            <complexType>
                                <sequence>
                                    <element ref='cml:request-base' minOccurs='1' maxOccurs='unbounded'/>
                                </sequence>
                            </complexType>
                        </element>

                        <!-- Responses are children of this element. -->
                        <element name='response' minOccurs='0'>
                            <complexType>
                                <sequence>
                                    <element ref='cml:response-base' minOccurs='1' maxOccurs='unbounded'/>
                                </sequence>
                            </complexType>
                        </element>
                    </sequence>
                </complexType>
            </element>
        </sequence>
    </group>

    <!-- Abstract head element for CML request elements. -->
    <element name='request-base' type='cml:BasicRequest' abstract='true'/>

    <!-- Standard CML requests. -->
    <annotation>
        <appinfo><begin-requests/></appinfo>
        <documentation>These are the standard CML requests; all CML-aware agents must support this set.</documentation>
    </annotation>
    <element name='agent-startup-announcement' type='cml:BasicRequest' substitutionGroup='cml:request-base'/>
    <element name='agent-shutdown-announcement' type='cml:BasicRequest' substitutionGroup='cml:request-base'/>
    <element name='get-agent-info-request' type='cml:BasicRequest' substitutionGroup='cml:request-base'/>
    <element name='get-copyright-info-request' type='cml:BasicRequest' substitutionGroup='cml:request-base'/>
    <element name='get-patent-info-request' type='cml:BasicRequest' substitutionGroup='cml:request-base'/>
    <element name='get-schema-info-request' type='cml:SchemaRequest' substitutionGroup='cml:request-base'/>
    <element name='get-schema-request' type='cml:SchemaRequest' substitutionGroup='cml:request-base'/>
    <element name='get-policy-request' type='cml:GetPolicyRequest' substitutionGroup='cml:request-base'/>
    <element name='set-policy-request' type='cml:SetPolicyRequest' substitutionGroup='cml:request-base'/>
    <element name='configure-task-request' type='cml:ConfigureTaskRequest' substitutionGroup='cml:request-base'/>
    <element name='configure-agent-request' type='cml:ConfigureAgentRequest' substitutionGroup='cml:request-base'/>
    <element name='get-task-status-request' type='cml:TaskStatusRequest' substitutionGroup='cml:request-base'/>
    <annotation>
        <appinfo><end-requests/></appinfo>
    </annotation>

    <!-- Abstract head element for CML response elements. -->
    <element name='response-base' type='cml:BasicResponse' abstract='true'/>

    <!-- Standard CML responses. -->
    <annotation>
        <appinfo><begin-responses/></appinfo>
        <documentation>These are the standard CML responses; all CML-aware agents must support this set.</documentation>
    </annotation>
    <element name='out-of-band-response' type='cml:BasicResponse' substitutionGroup='cml:response-base'/>
    <element name='agent-startup-acknowledgement' type='cml:BasicResponse' substitutionGroup='cml:response-base'/>
    <element name='agent-shutdown-acknowledgement' type='cml:BasicResponse' substitutionGroup='cml:response-base'/>
    <element name='schema-not-supported-response' type='cml:SchemaNotSupportedResponse' substitutionGroup='cml:response-base'/>
    <element name='get-agent-info-response' type='cml:AgentInfoResponse' substitutionGroup='cml:response-base'/>
    <element name='get-copyright-info-response' type='cml:BasicTextResponse' substitutionGroup='cml:response-base'/>
    <element name='get-patent-info-response' type='cml:BasicTextResponse' substitutionGroup='cml:response-base'/>
    <element name='get-schema-info-response' type='cml:GetSchemaInfoResponse' substitutionGroup='cml:response-base'/>
    <element name='get-schema-response' type='cml:GetSchemaResponse' substitutionGroup='cml:response-base'/>
    <element name='get-policy-response' type='cml:GetPolicyResponse' substitutionGroup='cml:response-base'/>
    <element name='set-policy-response' type='cml:SetPolicyResponse' substitutionGroup='cml:response-base'/>
    <element name='configure-task-response' type='cml:TaskStatusResponse' substitutionGroup='cml:response-base'/>
    <element name='configure-agent-response' type='cml:BasicResponse' substitutionGroup='cml:response-base'/>
    <element name='get-task-status-response' type='cml:TaskStatusResponse' substitutionGroup='cml:response-base'/>
    <annotation>
        <appinfo><end-responses/></appinfo>
    </annotation>

    <!-- 'cml' element:  this is the root element for CML. -->
    <element name='cml' substitutionGroup='iss:iss-base'>
        <complexType>
            <complexContent>
                <extension base='iss:IssBaseType'>
                    <sequence minOccurs='1' maxOccurs='unbounded'>
                        <group ref='cml:MessageElements'/>
                    </sequence>
                    <attribute name='schema-version' type='iss:VersionString' use='required'/>
                </extension>
            </complexContent>
        </complexType>
    </element>

</schema>
