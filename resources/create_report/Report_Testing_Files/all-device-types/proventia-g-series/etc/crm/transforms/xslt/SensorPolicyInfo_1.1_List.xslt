<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:spi="http://www.iss.net/proventiaSensorPolicyInfo">
	<xsl:output method="text"/>
	<xsl:param name="delimeter" select="','"/>
	<!-- Spits out
			1: policyName
			2: relativePath
			3: schemaName
			4: fileName
			5: policyVersion
			6: schemaVersion
			7: cpeEditable
			8: nameSpace
	-->
	
	<xsl:template match="spi:schemaNameSpace">
	<xsl:if test="string-length(@fileName)>0">
<xsl:value-of select="@policyName"/><xsl:value-of select="$delimeter"/><xsl:value-of select="@relativePath"/><xsl:value-of select="$delimeter"/><xsl:value-of select="@schemaName"/><xsl:value-of select="$delimeter"/><xsl:value-of select="@fileName"/><xsl:value-of select="$delimeter"/><xsl:value-of select="@policyVersion"/><xsl:value-of select="$delimeter"/><xsl:value-of select="@schemaVersion"/><xsl:value-of select="$delimeter"/><xsl:value-of select="@cpeEditable"/><xsl:value-of select="$delimeter"/><xsl:value-of select="@nameSpace"/>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
