#!/bin/bash 
set -o nounset
outFile="/tmp/sensorPolicy.xml"
errFile="/tmp/setschema.log"

nameSpace=""
schemaVersion=""
spi=""

while getopts "s:n:p:" opt; do
  case $opt in
    s ) schemaVersion=$OPTARG ;;
    n ) nameSpace=$OPTARG ;;
    p ) spi=$OPTARG ;;
  esac
done

if [ -f $errFile ]; then
  rm $errFile
fi

if [ $nameSpace = "" ] || [ $schemaVersion = "" ] || [ $spi = "" ]; then
	echo "usage $0 -s schemaVersion -n nameSpace -p sensorPolicyInfo_file"
	exit 1
fi


if [ -f $outFile ]; then
  echo "removing $outFile" >> $errFile
  rm $outFile
fi

if [ -f $spi ]; then
	cp $spi $spi.001
	echo "Copying $spi to $spi.001" >> $errFile
fi

#  Run the transform
Xalan -o $outFile -p paramSchemaVersion "'$schemaVersion'" -p paramNameSpace "'$nameSpace'" $spi /etc/crm/transforms/xslt/SensorPolicyInfo_1.1_Mod_Compatible.xslt 2>> $errFile
result=$?
if [ $result == "0" ]; then
   mv $outFile $spi
   echo  "Transforming $spi" >> $errFile
fi

# Test the result.  Ensure that the file can be reread and that the version
# attribute did change.
#
changedVersion=`Xalan $spi /etc/crm/transforms/xslt/SensorPolicyInfo_1.1_List.xslt | grep $nameSpace | awk -F , '{print $6}'`
result=$?
if [ $result != "0" ]; then
  cp $spi.001 $spi
  echo "ERROR:  Test of transform file failed." >> $errFile
  exit 1
fi

if [ $changedVersion != $schemaVersion ]; then
  cp $spi.001 $spi
  echo "ERROR:  Test of transform file failed.  Versions don't match. $changedVersion was read from $spi" >> $errFile
  exit 1
else
  echo "Schema version succesfully changed to $changedVersion for $nameSpace" >> $errFile
fi
# rebuild the schemas.jar file
if [ -f /var/www/html/schemas/schemas.jar ]; then
	echo "Building the jar." >> $errFile
	cp /var/www/html/schemas/schemas.jar /var/www/html/schemas/schemas.jar.001
	cd /etc/crm/schemas
	/usr/bin/zip -qr /var/www/html/schemas/schemas.jar cml Iss1_0_0.xsd Iss1_0_1.xsd
	result=$?
	cd -
	if [ $result != "0" ]; then
		echo "Error building jar" >> $errFile
		cp /var/www/html/schemas/schemas.jar.001 /var/www/html/schemas/schemas.jar
		rm  /var/www/html/schemas/schemas.jar.001
	fi
fi

