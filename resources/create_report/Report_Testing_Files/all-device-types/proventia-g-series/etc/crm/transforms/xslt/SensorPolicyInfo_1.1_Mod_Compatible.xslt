<?xml version="1.0" encoding="UTF-8"?>
<!--
	This script does a compatible revision of a schema version.  It will change the schema version and schema name.  It is generally
	called in response to a set schema request originating from SiteProtector.  Since SP will not issue set-schemas-requests for non-compatible
	revisions, this functionality is not supported within this transform.

-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:spi="http://www.iss.net/proventiaSensorPolicyInfo"  >
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"   />
	<xsl:param name="paramSchemaVersion" select="'0.0.0'"/>
	<xsl:param name="paramNameSpace" select="'unknown.unknown.unknown'" />
	
	<xsl:variable name="policyV" select="substring($paramSchemaVersion,1,3)"/>
	<xsl:variable name="versionWithUnderScores" select="translate($paramSchemaVersion,'.','_')"	/>
	
	<xsl:template match='spi:sensorPolicyInfo'  >
		<xsl:element name="sensorPolicyInfo" namespace="http://www.iss.net/proventiaSensorPolicyInfo">
			<xsl:copy-of select="@*[name()!='xsi:schemaLocation']"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>
	

	<xsl:template match="spi:schemaNameSpace" >
		<xsl:choose>
			<xsl:when test="@nameSpace=$paramNameSpace">
				<xsl:element name="schemaNameSpace" namespace="http://www.iss.net/proventiaSensorPolicyInfo"  >
					<xsl:copy-of select="@*[name() != 'schemaVersion|policyVersion|schemaName'  ]|*"/>
					<xsl:attribute name="schemaVersion"><xsl:value-of select="$paramSchemaVersion"/></xsl:attribute>
					<xsl:attribute name="policyVersion"><xsl:value-of select="$policyV"/></xsl:attribute>
					<xsl:attribute name="schemaName"><xsl:value-of select="substring(@schemaName,1,string-length(@schemaName)-9)"/><xsl:value-of select="$versionWithUnderScores"/>.xsd</xsl:attribute>
				</xsl:element>
			</xsl:when>
			<xsl:otherwise>
					<xsl:copy-of select="."/>
		<xsl:apply-templates/>					
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>
</xsl:stylesheet>
