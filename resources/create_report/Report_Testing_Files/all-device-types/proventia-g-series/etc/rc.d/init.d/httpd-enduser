#!/bin/bash
#
# Startup script for the Apache Web Server
#
# chkconfig: 2345 86 15
# description: Apache is a World Wide Web server.  It is used to serve \
#	       HTML files and CGI.
# processname: httpd
# pidfile: /var/run/httpd-enduser.pid
# config: /etc/httpd-enduser/conf/access.conf
# config: /etc/httpd-enduser/conf/httpd.conf
# config: /etc/httpd-enduser/conf/srm.conf

# Source function library.
. /etc/rc.d/init.d/functions

OPTIONS="-d /etc/httpd-enduser"

# This will prevent initlog from swallowing up a pass-phrase prompt if
# mod_ssl needs a pass-phrase from the user.
INITLOG_ARGS=""

# Path to the server binary, and short-form for messages.
httpd=/usr/sbin/httpd-enduser
prog=httpd-enduser
RETVAL=0

. /etc/sysconfig/httpd-enduser

# Find the installed modules and convert their names into arguments httpd
# can use.
moduleargs() {
	moduledir=/usr/lib/apache
	moduleargs=`
	/usr/bin/find ${moduledir} -type f -perm -0100 -name "*.so" | env -i tr '[:lower:]' '[:upper:]' | awk '{\
	gsub(/.*\//,"");\
	gsub(/^MOD_/,"");\
	gsub(/^LIB/,"");\
	gsub(/\.SO$/,"");\
	print "-DHAVE_" $0}'`
	echo ${moduleargs}
}

# The semantics of these two functions differ from the way apachectl does
# things -- attempting to start while running is a failure, and shutdown
# when not running is also a failure.  So we just do it the way init scripts
# are expected to behave here.
start() {
	ulimit -c 0
	if [ ! -f /etc/httpd-enduser/conf/ssl.key/server.key ] ; then
		echo -n $"Generating new SSL cert for $prog: "
		/usr/sbin/genselfcert `hostname` /etc/httpd-enduser/conf \
			&& success $"$prog genselfcert" \
			|| failure $"$prog genselfcert"
		echo
	fi
        sed -e "s/@HTTP_PORT@/${HTTP_PORT}/" \
            -e "s/@HTTPS_PORT@/${HTTPS_PORT}/" \
            /etc/httpd-enduser/conf/httpd.conf.in > /etc/httpd-enduser/conf/httpd.conf
        echo -n $"Starting $prog: "
        #daemon $httpd `moduleargs` $OPTIONS
        $httpd `moduleargs` $OPTIONS && success $"$prog startup" || failure $"$prog startup"
        RETVAL=$?
        echo
        [ $RETVAL = 0 ] && touch /var/lock/subsys/httpd-enduser
        return $RETVAL
}
stop() {
	echo -n $"Stopping $prog: "
	killproc $httpd
	RETVAL=$?
	echo
	[ $RETVAL = 0 ] && rm -f /var/lock/subsys/httpd-enduser /var/run/httpd-enduser.pid
}
reload() {
	echo -n $"Reloading $prog: "
	killproc $httpd -HUP
	RETVAL=$?
	echo
}

# See how we were called.
case "$1" in
  start)
	start
	;;
  stop)
	stop
	;;
  status)
        status $httpd
	RETVAL=$?
	;;
  restart)
	stop
	start
	;;
  condrestart)
	if [ -f /var/run/httpd-enduser.pid ] ; then
		stop
		start
	fi
	;;
  reload)
        reload
	;;
  *)
	echo $"Usage: $prog {start|stop|restart|condrestart|reload|status|fullstatus}"
	exit 1
esac

exit $RETVAL
