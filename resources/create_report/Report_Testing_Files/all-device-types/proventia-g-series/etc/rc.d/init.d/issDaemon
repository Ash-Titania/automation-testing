#!/bin/bash
#
# Init file for issDaemon
# Copyright(c) 2003 Internet Security Systems, Inc. All rights reserved.
#
# chkconfig: 345 98 02
# description: issDaemon

# source function library
. /etc/rc.d/init.d/functions

RETVAL=0
prog="issDaemon"

checkisskeys()
{
    ShortHost=`hostname | awk -F. '{print $1}'`
    # change hostname to all lowercase
    ShortHost=`echo ${ShortHost} | tr '[A-Z]' '[a-z]'`
    # check for engine keys
    if [ ! -e /etc/iss/Keys/RSA/rs_eng_${ShortHost}_1024.PubKey ] ||
       [ ! -e /etc/iss/Keys/RSA/rs_eng_${ShortHost}_1536.PubKey ] ; then
        if [ -x /usr/bin/makekeys ] ; then
            Failed=0
            echo -n $"Generating new issDaemon keys: "
            cd /etc/iss
            /usr/bin/makekeys -d /etc/iss -c crypt.policy -p /etc/iss -m "rs_eng" >/dev/null 2>/dev/null || Failed=1
            chown -R root:apache /etc/iss/Keys || Failed=1
            chown -R root:apache /etc/iss/KeyContainer || Failed=1
            chmod -R 0770 /etc/iss/Keys || Failed=1
            chmod -R 0770 /etc/iss/KeyContainer || Failed=1
            if [ ! -L /opt/ISS ] && [ -e /etc/iss ] ; then
                [ -d /opt/ISS ] && /bin/mv /opt/ISS /opt/ISS.bak
                ln -s /etc/iss /opt/ISS || Failed=1
            fi
            if [ -d /etc/crm/Keys ] ; then
               rm -fr /etc/crm/Keys.old
               mv /etc/crm/Keys /etc/crm/Keys.old
            fi
            cp -Rp /etc/iss/Keys /etc/crm
            if [ -d /etc/KeyContainer ] ; then
               rm -fr /etc/KeyContainer.old
               mv /etc/KeyContainer /etc/KeyContainer.old
            fi
            cp -Rp /etc/iss/KeyContainer /etc
            [ "$Failed" != "0" ] && failure || success
            echo
        fi
      else
        chown -R root:apache /etc/iss/KeyContainer || Failed=1
        chmod -R 0770 /etc/iss/KeyContainer || Failed=1
    fi
}

# delete cache files to prevent them 
# from being loaded after a reboot
removecachefiles()
{
    if [ -f /var/cache/iss-netengine/quarantine.cache ] ; then
        rm -f /var/cache/iss-netengine/quarantine.cache
    fi
}

start()
{
    echo -n $"Starting $prog: " 
    if [ ! -f /var/lock/subsys/${prog} ] ; then
	checkisskeys
	removecachefiles
	#ulimit -c unlimited
	LD_LIBRARY_PATH=/usr/lib:/usr/lib/crm:$LD_LIBRARY_PATH
	export LD_LIBRARY_PATH
	ISSDAEMON_CRYPTPOLICYFILE=/etc/iss
	export ISSDAEMON_CRYPTPOLICYFILE
	ISS_PUBSIG_PATH=/etc/iss
	export ISS_PUBSIG_PATH
	NLSPATH=/usr/lib/crm/nls/C/LC_MESSAGES/%N.cat
	export NLSPATH
	/usr/bin/issDaemon -d /etc/crm && success || failure
	RETVAL=$?
	[ $RETVAL -eq 0 ] && touch /var/lock/subsys/$prog
    fi
    echo
}

stop()
{
    echo -n $"Stopping ${prog}: "
    killproc issDaemon
    RETVAL=$?
    echo
    if [ $RETVAL -eq 0 ]; then
        # Wait for issCSF to stop (30 seconds max)
        count=0
        while [ -n "`pidof issCSF`" -a $count -lt 30 ] ; do 
            sleep 1 
            count=`expr $count + 1`
        done
        # Make sure they're really stopped
        if [ -n "`pidof issCSF`" ] ; then
            echo -n $"Killing issCSF: "
            killall -q -9 issCSF
            success
            echo
        fi
        if [ -n "`pidof iss-netengine`" ] ; then
            echo -n $"Killing iss-netengine(s): "
            killall -q -9 iss-netengine
            success
            echo
        fi
        if [ -n "`pidof -o $$ issDaemon`" ] ; then
            echo -n $"Killing issDaemon: "
            killall -q -9 issDaemon
            success
            echo
        fi

        # Remove the engine cache files
        removecachefiles
        
        rm -f /var/lock/subsys/${prog}
    fi
}

case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    restart)
        stop
        start
        ;;
    condrestart)
        if [ -f /var/lock/subsys/${prog} ] ; then
            stop
            start
        fi
        ;;
    status)
        status ${prog}
        RETVAL=$?
        ;;
    *)
        echo $"Usage: $0 {start|stop|restart|condrestart|status}"
        RETVAL=1
esac
exit $RETVAL
