interface mgmtEthernet
    ip 10.200.2.222
    mask 255.255.255.0
    route 10.200.2.222 255.255.255.255 192.168.0.1
    route 10.200.2.224 255.255.255.255 192.168.1.1
    exit
interface ethernet 3 1
    negotiate
    duplex full
    linespeed 1000
    no shutdown
    exit
interface ethernet 3 2
    negotiate
    duplex full
    linespeed 1000
    no shutdown
    exit
interface ethernet 3 3
    negotiate
    duplex full
    linespeed 1000
    no shutdown
    exit
interface ethernet 3 4
    negotiate
    duplex full
    linespeed 1000
    no shutdown
    exit
interface ethernet 3 5
    negotiate
    duplex full
    linespeed 1000
    no shutdown
    exit
interface ethernet 3 6
    negotiate
    duplex full
    linespeed 1000
    no shutdown
    exit
interface ethernet 3 7
    negotiate
    duplex full
    linespeed 1000
    no shutdown
    exit
interface ethernet 3 8
    negotiate
    duplex full
    linespeed 1000
    no shutdown
    exit
interface settings poll-interval 2000
interface settings detect-mdi enable
interface settings mdi-mode mdix
host name "tippingpoint2400"
host location "office"
host ip-filter permit any icmp
host ip-filter permit any ip
host no dns
default-gateway 0.0.0.0
no autodv
sntp primary 192.43.244.18
sntp secondary 192.5.41.40
sntp duration 60
sntp offset 1
sntp port 123
sntp timeout 1
sntp retries 3
no sntp
user options max-attempts   5
user options expire-period  90
user options expire-action  expire
user options lockout-period 5
user options attempt-action lockout
user options security-level 0
segment "Segment 1" high-availability permit
segment "Segment 1" link-down hub
segment "Segment 2" high-availability permit
segment "Segment 2" link-down hub
segment "Segment 3" high-availability permit
segment "Segment 3" link-down hub
segment "Segment 4" high-availability permit
segment "Segment 4" link-down hub
high-availability no ip
high-availability disable
clock timezone GMT
clock dst
log audit select general
log audit select login
log audit select logout
log audit select user
log audit select time
log audit select policy
log audit select update
log audit select boot
log audit select report
log audit select host
log audit select configuration
log audit select oam
log audit select sms
log audit select cva
log audit select server
log audit select segment
log audit select high-availability
log audit select monitor
log audit select ip-filter
log audit select conn-table
log audit select host-communication
log audit select tse
category-settings attack-protection enable -action-set "Recommended"
category-settings reconnaissance    enable -action-set "Recommended"
category-settings security-policy   enable -action-set "Recommended"
category-settings informational     enable -action-set "Recommended"
category-settings network-equipment enable -action-set "Recommended"
category-settings traffic-normal    enable -action-set "Recommended"
category-settings misuse-abuse      enable -action-set "Recommended"
notify-contact "SMS" 1
notify-contact "Remote System Log" 1
notify-contact "Management Console" 1
notify-contact "LSM" 1
default-alert-sink to fizz@titania.co.uk
default-alert-sink from fizz@titania.co.uk
default-alert-sink domain titania.co.uk
default-alert-sink server 192.168.0.20
default-alert-sink period 1
server ssh
server telnet
server http
server no https
server browser-check
monitor threshold memory      -major  90 -critical  95
monitor threshold disk        -major  90 -critical  95
monitor threshold temperature -major  67 -critical  69
no service-access
tse adaptive-filter mode automatic
tse afc-severity warning
tse asymmetric-network enable
tse connection-table timeout 1800
tse logging-mode conditional -threshold 1.0 -period 600
tse quarantine duration 60
email-rate-limit 10
lcd-keypad enable
lcd-keypad backlight 50
lcd-keypad contrast 16
nms ip 203.32.34.6 port 232
nms community community1
ramdisk sync-interval block -1
ramdisk sync-interval alert -1
ramdisk sync-interval peer -1
sms community community2
sms v2
sms v3
sms must-be-ip 53.56.120.32
no sms
session timeout 20 -persist
