provision ltm {
   level nominal
}
mgmt 192.168.0.117 {
   netmask 255.255.255.0
}
mgmt route default inet {
   gateway 192.168.0.1
}
interface 1.2 {
   disable
}
trunk trunk-1 {
   interfaces 1.1
}
stp {
   config name none
}
self allow {
   default {
      tcp ssh
      tcp domain
      tcp snmp
      tcp https
      tcp f5-iquery
      udp domain
      udp snmp
      udp efs
      udp cap
      udp f5-iquery
      proto ospf
   }
}
partition Common {
   description "Repository for system objects and shared objects."
}
shell write partition Common
vlan external {
   tag 4093
}
vlan internal {
   tag 4094
}
self 10.0.0.0 {
   netmask 255.255.255.0
   vlan internal
   allow default
}
self 10.1.0.0 {
   netmask 255.255.255.0
   vlan external
   allow tcp https
}
user root {
   password crypt "$1$ey2dogzt$CcWnUlWxHEWj321NDR5Bv0"
}
user admin {
   password crypt "$1$AbaOcON7$9aaZQw5t7TvdLOG.Gfp3A."
   group 500
   home "/home/admin"
   shell "/bin/false"
   role administrator in all
}
dns {
   nameservers 192.168.0.1
   search {
      "localhost.com"
      "titania.co.uk"
   }
}
httpd {
   authpamidletimeout 1300
   maxclients 10
}
ntp {
   servers 0.pool.ntp.org
   timezone "Europe/London"
}
password policy {
   min length 8
   required lowercase 1
   required numeric 1
   required special 1
   required uppercase 1
   strict enable
}
snmpd {
   allow {
      "127."
      "192.168.0.20"
      "10.0.1.0/255.255.255.0"
   }
   include none
   syscontact "SNMP-System-Contact"
   syslocation "SNMP-Machine-Location"
   sysservices 78
   community {
      comm-public {}
      iprivate_1 {
         access rw
         community name "private"
         ipv6 disable
         oid none
         source none
      }
      iprivate_2 {
         access rw
         community name "private"
         ipv6 enable
         oid none
         source none
      }
      ipublic_1 {
         access ro
         community name "public"
         ipv6 enable
         oid none
         source none
      }
   }
   disk {
      root {}
      var {}
   }
   proc {
      bigd {}
      chmand {}
      httpd {}
      mcpd {}
      sod {}
   }
   trapsess i192_168_0_20_1 {
         auth password none
         auth protocol NONE
         community "public"
         engine id none
         host "192.168.0.20"
         port 162
         privacy password none
         privacy protocol NONE
         security level noAuthNoPriv
         security name none
         version 1
      }
   usmuser isnmp3username_1 {
         access ro
         auth password crypt "/\\BCVWgbld:N?9vcRCE[CbF<8TCRU9f@>3cFbYM^.cV2;0["
         auth protocol MD5
         oid ".1.3.6.1.4.1.3375.2.2.10.2.3.1.9"
         privacy password crypt "8ZCJgM8_PM9adh7ru.@bJX6UdiB5?>G9EH<1:ei?3joc;5x"
         privacy protocol DES
         security level authPriv
         username "snmp3username"
      }
}
statemirror {
   addr 10.0.0.0
}
system {
   gui security banner enable
   gui security banner text "Welcome to the BIG-IP Configuration Utility.

Log in with your username and password using the fields on the left.
This has been customized."
   gui setup disable
   hostname "testf5.titania.co.uk"
   mgmt dhcp enable
}
#  No partition
datastor {
   low water mark 80
   high water mark 92
}
deduplication {}
packet filter continue-rule {
   order 5
   action continue
   vlan internal
   log enable
   filter { ( dst port 80 or dst port 443 ) }
}
packet filter nolog {
   order 20
   action accept
   filter { ( proto UDP or proto ICMP ) }
}
packet filter reject-anydest-nolog {
   order 15
   action reject
   filter { ( src net 40.0.0.0/24 ) }
}
packet filter testrule {
   order 10
   action accept
   log enable
   filter { ( proto TCP ) and ( src host 20.0.0.1 or src host 20.0.0.2 ) and ( dst host 30.0.0.1 or dst host 30.0.0.2 ) and ( dst port 80 ) }
}
shell write partition Common
virtual testserver {
   destination 10.0.0.12:http
   ip protocol tcp
   profiles {
      clientssl {
         clientside
      }
      tcp {}
   }
}
