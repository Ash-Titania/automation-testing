Building configuration...
: Saved
:
PIX Version 6.3(5)
interface ethernet0 auto shutdown
interface ethernet1 100full
nameif ethernet0 outside security0
nameif ethernet1 inside security100
enable password NuLKvvWGg.x9HEKO encrypted
passwd NuLKvvWGg.x9HEKO encrypted
hostname chicken
domain-name titania.co.uk
fixup protocol dns maximum-length 512
fixup protocol ftp 21
fixup protocol h323 h225 1720
fixup protocol h323 ras 1718-1719
fixup protocol http 80
fixup protocol rsh 514
fixup protocol rtsp 554
fixup protocol sip 5060
fixup protocol sip udp 5060
fixup protocol skinny 2000
fixup protocol smtp 25
fixup protocol sqlnet 1521
fixup protocol tftp 69
names
access-list inside permit tcp any any eq www
access-list inside permit tcp any any eq https
access-list outside_access_in permit tcp any interface outside
access-list inside_outbound_nat0_acl permit ip any 192.168.1.192 255.255.255.224
access-list outside_cryptomap_dyn_20 permit ip any 192.168.1.192 255.255.255.224
no pager
logging on
logging trap alerts
mtu outside 1500
mtu inside 1500
no ip address outside
ip address inside 10.200.4.121 255.255.255.0
ip verify reverse-path interface inside
ip audit info action alarm
ip audit attack action alarm
pdm history enable
arp timeout 14400
static (inside,outside) tcp interface www 192.168.1.1 www netmask 255.255.255.255 0 0
static (inside,outside) tcp interface 3389 192.168.1.1 3389 netmask 255.255.255.255 0 0
static (inside,outside) tcp interface https 192.168.1.1 https netmask 255.255.255.255 0 0
access-group outside_access_in in interface outside
timeout xlate 3:00:00
timeout conn 1:00:00 half-closed 0:10:00 udp 0:02:00 rpc 0:10:00 h225 1:00:00
timeout h323 0:05:00 mgcp 0:05:00 sip 0:30:00 sip_media 0:02:00
timeout sip-disconnect 0:02:00 sip-invite 0:03:00
timeout uauth 0:05:00 absolute
aaa-server TACACS+ protocol tacacs+
aaa-server TACACS+ max-failed-attempts 3
aaa-server TACACS+ deadtime 10
aaa-server RADIUS protocol radius
aaa-server RADIUS max-failed-attempts 3
aaa-server RADIUS deadtime 10
aaa-server LOCAL protocol local
aaa-server local protocol tacacs+
aaa-server local max-failed-attempts 3
aaa-server local deadtime 10
aaa authentication ssh console LOCAL
ntp server 192.168.0.10 source inside
http server enable
http 192.168.0.20 255.255.255.255 inside
http 192.168.0.0 255.255.255.0 inside
http 172.16.1.0 255.255.255.0 inside
http 10.200.4.0 255.255.255.0 inside
snmp-server host inside 172.16.1.145
snmp-server host inside 192.168.0.100
snmp-server host inside 192.168.198.128
no snmp-server location
no snmp-server contact
snmp-server community public
no snmp-server enable traps
floodguard enable
isakmp enable outside
isakmp policy 10 authentication pre-share
isakmp policy 10 encryption aes-256
isakmp policy 10 hash sha
isakmp policy 10 group 2
isakmp policy 10 lifetime 2800
telnet 192.168.0.20 255.255.255.255 inside
telnet 0.0.0.5 255.255.255.255 inside
telnet 10.200.4.0 255.255.255.0 inside
telnet timeout 5
ssh 10.200.4.0 255.255.255.0 outside
ssh 10.200.4.0 255.255.255.0 inside
ssh 0.0.0.0 0.0.0.0 inside
ssh timeout 5
console timeout 0
username admin password 7KKG/zg/Wo8c.YfN encrypted privilege 15
terminal width 80
banner exec The exec banner
banner login The login banner
banner motd The motd banner
Cryptochecksum:eba8085a65f30e34894475695c08b468
: end
[OK]
