#
# Configuration of gw-35fe5f
# Language version: 10.0v1
#
# Exported by admin on Tue Mar 11 14:09:58 2014
#
set ipv6-state on
set ntp active off
set net-access telnet off
set clienv debug 0
set clienv echo-cmd off
set clienv output pretty
set clienv prompt "%M> "
set clienv rows 0
set clienv syntax-check off


set arp table cache-size 1024
set arp table validity-timeout 60
set password-controls min-password-length 6
set password-controls complexity 2
set password-controls palindrome-check true
set password-controls history-checking true
set password-controls history-length 10
set password-controls password-expiration never
add command fw6 path /opt/CPsuite-R75.40/fw1/bin/fw6 description "Security gateway IPv6 commands"
add command fwaccel6 path /opt/CPsuite-R75.40/fw1/bin/fwaccel6 description "SecureXL IPv6 commands"
add command sim6 path /opt/CPppak-R75.40/bin/sim6 description "SecureXL Implementation Module IPv6 commands"
set hostname gw-35fe5f
set web session-timeout 10
set web ssl-port 443
set web daemon-enable on
set snmp agent off
set snmp agent-version any
set snmp community public read-only
set snmp traps trap authorizationError disable
set snmp traps trap coldStart disable
set snmp traps trap configurationChange disable
set snmp traps trap configurationSave disable
set snmp traps trap fanFailure disable
set snmp traps trap highVoltage disable
set snmp traps trap linkUpLinkDown disable
set snmp traps trap lowDiskSpace disable
set snmp traps trap lowVoltage disable
set snmp traps trap overTemperature disable
set snmp traps trap powerSupplyFailure disable
set snmp traps trap raidVolumeState disable
set inactivity-timeout 10
set interface Mgmt auto-negotiation off
set interface Mgmt link-speed 1000M/full
set interface Mgmt state on
set interface Mgmt auto-negotiation on
set interface Mgmt ipv4-address 10.200.4.130 mask-length 24
add interface Mgmt alias 192.168.1.1/24
set interface eth1 state off
set interface eth2 state off
set interface eth3 state off
set interface eth4 state off
set interface eth5 state off
set interface lo state on
set interface lo ipv4-address 127.0.0.1 mask-length 8
set interface lo ipv6-address ::1 mask-length 128
set static-route default nexthop gateway address 192.168.1.254 on
set igmp interface Mgmt  version 2
set igmp interface Mgmt  router-alert on
set igmp interface Mgmt  last-member-query-interval default
set igmp interface Mgmt  loss-robustness default
set igmp interface Mgmt  query-interval default
set igmp interface Mgmt  query-response-interval default
set igmp interface eth1  version 2
set igmp interface eth1  router-alert on
set igmp interface eth1  last-member-query-interval default
set igmp interface eth1  loss-robustness default
set igmp interface eth1  query-interval default
set igmp interface eth1  query-response-interval default
set igmp interface eth2  version 2
set igmp interface eth2  router-alert on
set igmp interface eth2  last-member-query-interval default
set igmp interface eth2  loss-robustness default
set igmp interface eth2  query-interval default
set igmp interface eth2  query-response-interval default
set igmp interface eth3  version 2
set igmp interface eth3  router-alert on
set igmp interface eth3  last-member-query-interval default
set igmp interface eth3  loss-robustness default
set igmp interface eth3  query-interval default
set igmp interface eth3  query-response-interval default
set igmp interface eth4  version 2
set igmp interface eth4  router-alert on
set igmp interface eth4  last-member-query-interval default
set igmp interface eth4  loss-robustness default
set igmp interface eth4  query-interval default
set igmp interface eth4  query-response-interval default
set igmp interface eth5  version 2
set igmp interface eth5  router-alert on
set igmp interface eth5  last-member-query-interval default
set igmp interface eth5  loss-robustness default
set igmp interface eth5  query-interval default
set igmp interface eth5  query-response-interval default
set igmp interface lo  version 2
set igmp interface lo  router-alert on
set igmp interface lo  last-member-query-interval default
set igmp interface lo  loss-robustness default
set igmp interface lo  query-interval default
set igmp interface lo  query-response-interval default
set rip update-interval default
set rip expire-interval default
set rip auto-summary on
add allowed-client host any-host
set router-id 10.200.4.130
set pim mode sparse
set pim hello-interval default
set pim data-interval default
set pim assert-interval default
set pim assert-limit default
set pim jp-interval default
set pim jp-delay-interval default
set pim jp-suppress-interval default
set pim register-suppress-interval default
set pim bootstrap-candidate priority default
set pim candidate-rp advertise-interval default
set pim candidate-rp priority default
set pim static-rp off
set pim assert-rank protocol direct rank default
set pim assert-rank protocol kernel rank default
set pim assert-rank protocol static rank default
set pim assert-rank protocol ospf rank default
set pim assert-rank protocol ospfase rank default
set pim assert-rank protocol rip rank default
set pim assert-rank protocol bgp rank default
set format date dd-mmm-yyyy
set format time 24-hour
set format netmask Dotted
set user admin shell /etc/cli.sh
set user admin password-hash $1$2.tbAWEF$q.Y97CD5Ig4rNQd5RCeeE.
set user monitor shell /etc/cli.sh
set user monitor password-hash *
set timezone Europe / London
