#config-version=FG500A-3.00-FW-build403-061106:opmode=0:vdom=0
#conf_file_ver=7235700037401564169
#buildno=0403
config system global
    set authtimeout 15
    set hostname "SydneyFWPrimary"
    set ntpserver "132.246.168.148"
    set optimize antivirus
    set syncinterval 60
    set timezone 04
end
config system accprofile
    edit "prof_admin"
        set admingrp read-write
        set authgrp read-write
        set avgrp read-write
        set fwgrp read-write
        set ipsgrp read-write
        set loggrp read-write
        set mntgrp read-write
        set netgrp read-write
        set routegrp read-write
        set spamgrp read-write
        set sysgrp read-write
        set updategrp read-write
        set vpngrp read-write
        set webgrp read-write
    next
    edit "Whitelist_Maintenance"
        set netgrp read
        set sysgrp read
        set webgrp read-write
    next
end
config system admin
    edit "admin"
        set accprofile "super_admin"
        set password ENC AK1ykHygj7JNLYtXmuFQUnc5NMmmTIwLDo6GO+vI+HvQug=
        set vdom "root"
    next
    edit "test"
        set accprofile "super_admin"
        set password ENC AK10K30+yyd7l9LVqhKyPn4kvspzu+fhlBJQE/2rX8JdpY=
        set vdom "root"
    next
end
config system interface
    edit "lan"
        set vdom "root"
        set ip 10.200.20.36 255.255.255.0
        set allowaccess ping https ssh http telnet
        set type physical
    next
    edit "port1"
        set vdom "root"
        set ip 203.94.168.150 255.255.255.252
        set allowaccess ping
        set type physical
        set speed 10full
    next
    edit "port2"
        set vdom "root"
        set dhcp-relay-service enable
        set dhcp-relay-ip "10.80.4.1"
        set ip 192.168.206.65 255.255.255.192
        set allowaccess ping
        set type physical
    next
    edit "port3"
        set vdom "root"
        set ip 192.168.206.129 255.255.255.252
        set allowaccess ping
        set status down
        set type physical
    next
    edit "port4"
        set vdom "root"
        set ip 192.168.206.193 255.255.255.252
        set allowaccess ping
        set type physical
    next
    edit "port5"
        set vdom "root"
        set allowaccess ping
        set status down
        set type physical
    next
    edit "port6"
        set vdom "root"
        set ip 10.80.100.50 255.255.255.0
        set allowaccess ping https ssh snmp
        set type physical
    next
end
config system ha
    set override disable
end
config system dns
    set primary 65.39.139.53
    set secondary 65.39.139.63
end
config system replacemsg mail "email-block"
    set buffer "Potentially Dangerous Attachment Removed. The file \"%%FILE%%\" has been blocked.  File quarantined as: \"%%QUARFILENAME%%\"."
    set format text
    set header 8bit
end
config system replacemsg mail "email-virus"
    set buffer "Dangerous Attachment has been Removed.  The file \"%%FILE%%\" has been removed because of a virus.  It was infected with the \"%%VIRUS%%\" virus.  File quarantined as: \"%%QUARFILENAME%%\"."
    set format text
    set header 8bit
end
config system replacemsg mail "email-filesize"
    set buffer "This email has been blocked.  The email message is larger than the configured file size limit."
    set format text
    set header 8bit
end
config system replacemsg mail "partial"
    set buffer "Fragmented emails are blocked."
    set format text
    set header 8bit
end
config system replacemsg mail "smtp-block"
    set buffer "The file %%FILE%% has been blocked. File quarantined as: %%QUARFILENAME%%"
    set format text
    set header none
end
config system replacemsg mail "smtp-virus"
    set buffer "The file %%FILE%% has been infected with the virus %%VIRUS%% File quarantined as %%QUARFILENAME%%"
    set format text
    set header none
end
config system replacemsg mail "smtp-filesize"
    set buffer "This message is larger than the configured limit and has been blocked."
    set format text
    set header none
end
config system replacemsg http "bannedword"
    set buffer "<HTML><BODY>The page you requested has been blocked because it contains a banned word. URL = http://%%URL%%</BODY></HTML>"
    set format html
    set header http
end
config system replacemsg http "url-block"
    set buffer "<HTML><BODY>The URL you requested has been blocked. URL = %%URL%%</BODY></HTML>"
    set format html
    set header http
end
config system replacemsg http "infcache-block"
    set buffer "<HTML><BODY><H2>High security alert!!!</h2><p>The URL you requested was previously found to be infected.</p><p>URL = http://%%URL%%</p></BODY></HTML>"
    set format html
    set header http
end
config system replacemsg http "http-block"
    set buffer "<HTML> <BODY> <h2>High security alert!!!</h2> <p>You are not permitted to download the file \"%%FILE%%\".</p> <p>URL = http://%%URL%%</p> </BODY> </HTML>"
    set format html
    set header http
end
config system replacemsg http "http-virus"
    set buffer "<HTML><BODY><h2>High security alert!!!</h2><p>You are not permitted to download the file \"%%FILE%%\" because it is infected with the virus \"%%VIRUS%%\". </p><p>URL = http://%%URL%%</p><p>File quarantined as: %%QUARFILENAME%%.</p></BODY></HTML>"
    set format html
    set header http
end
config system replacemsg http "http-filesize"
    set buffer "<HTML><BODY>  <h2>Attention!!!</h2><p>The file \"%%FILE%%\" has been blocked.  The file is larger than the configured file size limit.</p> <p>URL = http://%%URL%%</p> </BODY></HTML>"
    set format html
    set header http
end
config system replacemsg http "http-client-block"
    set buffer "<HTML> <BODY> <h2>High security alert!!!</h2> <p>You are not permitted to upload the file \"%%FILE%%\".</p> <p>URL = http://%%URL%%</p> </BODY> </HTML>"
    set format html
    set header http
end
config system replacemsg http "http-client-virus"
    set buffer "<HTML><BODY><h2>High security alert!!!</h2><p>You are not permitted to upload the file \"%%FILE%%\" because it is infected with the virus \"%%VIRUS%%\". </p><p>URL = http://%%URL%%</p><p>File quarantined as: %%QUARFILENAME%%.</p></BODY></HTML>"
    set format html
    set header http
end
config system replacemsg http "http-client-filesize"
    set buffer "<HTML><BODY>  <h2>Attention!!!</h2><p>Your request has been blocked.  The request is larger than the configured file size limit.</p> <p>URL = http://%%URL%%</p> </BODY></HTML>"
    set format html
    set header http
end
config system replacemsg http "http-client-bannedword"
    set buffer "<HTML><BODY>The page you uploaded has been blocked because it contains a banned word. URL = http://%%URL%%</BODY></HTML>"
    set format html
    set header http
end
config system replacemsg ftp "ftp-dl-infected"
    set buffer "Transfer failed.  The file %%FILE%% is infected with the virus %%VIRUS%%. File quarantined as %%QUARFILENAME%%."
    set format text
    set header none
end
config system replacemsg ftp "ftp-dl-blocked"
    set buffer "Transfer failed.  You are not permitted to transfer the file \"%%FILE%%\"."
    set format text
    set header none
end
config system replacemsg ftp "ftp-dl-filesize"
    set buffer "File size limit exceeded."
    set format text
    set header none
end
config system replacemsg nntp "nntp-dl-infected"
    set buffer "Dangerous Attachment has been Removed.  The file \"%%FILE%%\" has been removed because of a virus.  It was infected with the \"%%VIRUS%%\" virus.  File quarantined as: \"%%QUARFILENAME%%\"."
    set format text
    set header none
end
config system replacemsg nntp "nntp-dl-blocked"
    set buffer "The file %%FILE%% has been blocked. File quarantined as: %%QUARFILENAME%%"
    set format text
    set header none
end
config system replacemsg nntp "nntp-dl-filesize"
    set buffer "This article has been blocked.  The article is larger than the configured file size limit."
    set format text
    set header none
end
config system replacemsg alertmail "alertmail-virus"
    set buffer "Virus/Worm detected: %%VIRUS%% Protocol: %%PROTOCOL%% Source IP: %%SOURCE_IP%% Destination IP: %%DEST_IP%% Email Address From: %%EMAIL_FROM%% Email Address To: %%EMAIL_TO%% "
    set format text
    set header none
end
config system replacemsg alertmail "alertmail-block"
    set buffer "File Block Detected: %%FILE%% Protocol: %%PROTOCOL%% Source IP: %%SOURCE_IP%% Destination IP: %%DEST_IP%% Email Address From: %%EMAIL_FROM%% Email Address To: %%EMAIL_TO%% "
    set format text
    set header none
end
config system replacemsg alertmail "alertmail-nids-event"
    set buffer "The following intrusion was observed: %%NIDS_EVENT%%."
    set format text
    set header none
end
config system replacemsg alertmail "alertmail-crit-event"
    set buffer "The following critical firewall event was detected: %%CRITICAL_EVENT%%."
    set format text
    set header none
end
config system replacemsg alertmail "alertmail-disk-full"
    set buffer "The log disk is Full."
    set format text
    set header none
end
config system replacemsg fortiguard-wf "ftgd-block"
    set buffer "<html><head><title>Web Filter Violation</title></head><body><font size=2><table width=\"100%\"><tr><td>%%FORTIGUARD_WF%%</td><td align=\"right\">%%FORTINET%%</td></tr><tr><td bgcolor=#ff6600 align=\"center\" colspan=2><font color=#ffffff><b>Web Page Blocked</b></font></td></tr></table><br><br>You have tried to access a web page which is in violation of your internet usage policy.<br><br>URL:&nbsp;%%URL%%<br>Category:&nbsp;%%CATEGORY%%<br><br>To have the rating of this web page re-evaluated <u><a href=\"%%FTGD_RE_EVAL%%\">please click here</a></u>.<br>%%OVERRIDE%%<br><hr><br>Powered by %%SERVICE%%.</font></body></html>"
    set format html
    set header http
end
config system replacemsg fortiguard-wf "http-err"
    set buffer "<html><head><title>%%HTTP_ERR_CODE%% %%HTTP_ERR_DESC%%</title></head><body><font size=2><table width=\"100%\"><tr><td>%%FORTIGUARD_WF%%</td><td align=\"right\">%%FORTINET%%</td></tr><tr><td bgcolor=#3300cc align=\"center\" colspan=2><font color=#ffffff><b>%%HTTP_ERR_CODE%% %%HTTP_ERR_DESC%%</b></font></td></tr></table><br><br>The webserver for %%URL%% reported that an error occurred while trying to access the website.  Please click <u><a onclick=\"history.back()\">here</a></u> to return to the previous page.<br><br><hr><br>Powered by %%SERVICE%%.</font></body></html>"
    set format html
    set header http
end
config system replacemsg fortiguard-wf "ftgd-ovrd"
    set buffer "<html><head><title>Web Filter Block Override</title></head><body><font size=2><table width=\"100%\"><tr><td>%%FORTIGUARD_WF%%</td><td align=\"right\">%%FORTINET%%</td></tr><tr><td bgcolor=#3300cc align=\"center\" colspan=2><font color=#ffffff><b>Web Filter Block Override</b></font></td></tr><tr><td colspan=2><br><br>If you have been granted override initiation privileges by your administrator, you can enter your username and password here to gain immediate access to the blocked web-page.  If you do not have these privileges, please contact your administrator to gain access to the web-page.<br><br></td></tr><tr><td align=\"center\" colspan=2>%%OVRD_FORM%%</td></tr></table><br><br><hr><br>Powered by %%SERVICE%%.</font></body></html>"
    set format html
    set header http
end
config system replacemsg spam "ipblocklist"
    set buffer "Mail from this IP address is not allowed and has been blocked."
    set format text
    set header none
end
config system replacemsg spam "smtp-spam-rbl"
    set buffer "This message has been blocked because it is from a RBL/ORDBL IP address."
    set format text
    set header none
end
config system replacemsg spam "smtp-spam-feip"
    set buffer "This message has been blocked because it is from a FortiGuard - AntiSpam black IP address."
    set format text
    set header none
end
config system replacemsg spam "smtp-spam-helo"
    set buffer "This message has been blocked because the HELO/EHLO domain is invalid."
    set format text
    set header none
end
config system replacemsg spam "smtp-spam-emailblack"
    set buffer "Mail from this email address is not allowed and  has been blocked."
    set format text
    set header none
end
config system replacemsg spam "smtp-spam-mimeheader"
    set buffer "This message has been blocked because it contains an invalid header."
    set format text
    set header none
end
config system replacemsg spam "reversedns"
    set buffer "This message has been blocked because the return email domain is invalid."
    set format text
    set header none
end
config system replacemsg spam "smtp-spam-bannedword"
    set buffer "This message has been blocked because it contains a banned word."
    set format text
    set header none
end
config system replacemsg spam "smtp-spam-fsurl"
    set buffer "This message has been blocked because it contains FortiGuard - AntiSpam blocking URL(s)."
    set format text
    set header none
end
config system replacemsg spam "smtp-spam-fschksum"
    set buffer "This message has been blocked because its checksum is in FortiGuard - AntiSpam checksum blacklist."
    set format text
    set header none
end
config system replacemsg spam "submit"
    set buffer "If this email is not spam, click here to submit the signatures to FortiGuard - AntiSpam Service."
    set format text
    set header none
end
config system replacemsg auth "auth-disclaimer-page-1"
    set buffer "<HTML><HEAD><TITLE>Firewall Disclaimer</TITLE></HEAD><BODY><FORM ACTION=\"/\" method=\"POST\"><INPUT TYPE=\"hidden\" NAME=\"%%MAGICID%%\" VALUE=\"%%MAGICVAL%%\"><INPUT TYPE=\"hidden\" NAME=\"%%ANSWERID%%\" VALUE=\"%%DECLINEVAL%%\"><INPUT TYPE=\"hidden\" NAME=\"%%REDIRID%%\" VALUE=\"%%PROTURI%%\"><TABLE ALIGN=\"CENTER\" width=400 height=250 cellpadding=2 cellspacing=0 border=0 bgcolor=\"#008080\"><TR><TD><TABLE border=0 width=\"100%\" height=\"100%\" cellpadding=0 cellspacing=0 bgcolor=\"#9dc8c6\"><TR height=30 bgcolor=\"#008080\"><TD><b><font size=2 face=\"Verdana\" color=\"#ffffff\">Disclaimer Agreement</font></b></TD><TR><TR height=\"100%\"><TD><TABLE border=0 cellpadding=5 cellspacing=0 width=\"320\" align=center><TR><TD colspan=2><font size=2 face=\"Times New Roman\">You are about to access Internet content that is not under the control of the network access provider.  The network access provider is therefore not responsible for any of these sites, their content or their privacy policies. The network access provider and its staff do not endorse nor make any representations about these sites, or any information, software or other products or materials found there, or any results that may be obtained from using them. If you decide to access any Internet content, you do this entirely at your own risk and you are responsible for ensuring that any accessed material does not infringe the laws governing, but not exhaustively covering, copyright, trademarks, pornography, or any other material which is slanderous, defamatory or might cause offence in any other way.</font></TD></TR><TR><TD>Do you agree to the above terms?</TD></TR><TR><TD><INPUT CLASS=\"button\" TYPE=\"button\" VALUE=\"Yes, I agree\" ONCLICK=\"agree()\"><INPUT CLASS=\"button\" TYPE=\"button\" VALUE=\"No, I decline\" ONCLICK=\"decline()\"></TD></TR></TABLE></TD></TR></TABLE></TD></TR></TABLE></FORM><SCRIPT LANGUAGE=\"JavaScript\">function agree(){document.forms[0].%%ANSWERID%%.value=\"%%AGREEVAL%%\";document.forms[0].submit();}function decline(){document.forms[0].submit();}</SCRIPT></BODY></HTML>"
    set format html
    set header http
end
config system replacemsg auth "auth-disclaimer-page-2"
    set buffer ''
    set format html
    set header http
end
config system replacemsg auth "auth-disclaimer-page-3"
    set buffer ''
    set format html
    set header http
end
config system replacemsg auth "auth-reject-page"
    set buffer "<HTML><HEAD><TITLE>Firewall Disclaimer Declined</TITLE></HEAD><BODY><FORM ACTION=\"/\" method=\"POST\"><INPUT TYPE=\"hidden\" NAME=\"%%MAGICID%%\" VALUE=\"%%MAGICVAL%%\"><INPUT TYPE=\"hidden\" NAME=\"%%REDIRID%%\" VALUE=\"%%PROTURI%%\"><TABLE ALIGN=\"CENTER\" width=400 height=250 cellpadding=2 cellspacing=0 border=0 bgcolor=\"#008080\"><TR><TD><TABLE border=0 width=\"100%\" height=\"100%\" cellpadding=0 cellspacing=0 bgcolor=\"#9dc8c6\"><TR height=30 bgcolor=\"#008080\"><TD><b><font size=2 face=\"Verdana\" color=\"#ffffff\">Disclaimer Declined</font></b></TD><TR><TR height=\"100%\"><TD><TABLE border=0 cellpadding=5 cellspacing=0 width=\"320\" align=center><TR><TD colspan=2><font size=2 face=\"Times New Roman\">Sorry, network access cannot be granted unless you agree to the disclaimer.</font></TD><TR><TR><TD></TD><TD><INPUT TYPE=\"submit\" VALUE=\"Return to Disclaimer\"></TD></TR></TABLE></TD></TR></TABLE></TD></TR></TABLE></FORM></BODY></HTML>"
    set format html
    set header http
end
config system replacemsg auth "auth-login-page"
    set buffer "<HTML><HEAD><TITLE>Firewall Authentication</TITLE></HEAD><BODY><FORM ACTION=\"/\" method=\"POST\"><INPUT TYPE=\"hidden\" NAME=\"%%MAGICID%%\" VALUE=\"%%MAGICVAL%%\"><TABLE ALIGN=\"CENTER\" width=400 height=250 cellpadding=2 cellspacing=0 border=0 bgcolor=\"#008080\"><TR><TD><TABLE border=0 cellpadding=0 cellspacing=0 bgcolor=\"#9dc8c6\"><TR height=30 bgcolor=\"#008080\"><TD><b><font size=2 face=\"Verdana\" color=\"#ffffff\">Authentication Required</font></b></TD></TR><TR><TD><TABLE border=0 cellpadding=5 cellspacing=0 width=\"320\" align=center><TR><TD colspan=2><font size=2 face=\"Times New Roman\">%%QUESTION%%</font></TD></TR><TR><TD><font size=2 face=\"Times New Roman\">Username:</font></TD><TD><INPUT TYPE=\"text\" NAME=\"%%USERNAMEID%%\" size=25></TD></TR><TR><TD><font size=2 face=\"Times New Roman\">Password:</font></TD><TD><INPUT TYPE=\"password\" NAME=\"%%PASSWORDID%%\" size=25></TD></TR><TR><TD><INPUT TYPE=\"hidden\" NAME=\"%%REDIRID%%\" VALUE=\"%%PROTURI%%\"><INPUT TYPE=\"submit\" VALUE=\"Continue\"></TD></TR></TABLE></TD></TR></TABLE></TD></TR></TABLE></FORM></BODY></HTML>"
    set format html
    set header http
end
config system replacemsg auth "auth-login-failed-page"
    set buffer "<HTML><HEAD><TITLE>Firewall Authentication</TITLE></HEAD><BODY><FORM ACTION=\"/\" method=\"POST\"><INPUT TYPE=\"hidden\" NAME=\"%%MAGICID%%\" VALUE=\"%%MAGICVAL%%\"><TABLE ALIGN=\"CENTER\" width=400 height=250 cellpadding=2 cellspacing=0 border=0 bgcolor=\"#008080\"><TR><TD><TABLE border=0 cellpadding=0 cellspacing=0 bgcolor=\"#9dc8c6\"><TR height=30 bgcolor=\"#008080\"><TD><b><font size=2 face=\"Verdana\" color=\"#ffffff\">Authentication Failed</font></b></TD></TR><TR><TD><TABLE border=0 cellpadding=5 cellspacing=0 width=\"320\" align=center><TR><TD colspan=2><font size=2 face=\"Times New Roman\">%%FAILED_MESSAGE%%</font></TD></TR><TR><TD><font size=2 face=\"Times New Roman\">Username:</font></TD><TD><INPUT TYPE=\"text\" NAME=\"%%USERNAMEID%%\" size=25></TD></TR><TR><TD><font size=2 face=\"Times New Roman\">Password:</font></TD><TD><INPUT TYPE=\"password\" NAME=\"%%PASSWORDID%%\" size=25></TD></TR><TR><TD><INPUT TYPE=\"hidden\" NAME=\"%%REDIRID%%\" VALUE=\"%%PROTURI%%\"><INPUT TYPE=\"submit\" VALUE=\"Continue\"></TD></TR></TABLE></TD></TR></TABLE></TD></TR></TABLE></FORM></BODY></HTML>"
    set format html
    set header http
end
config system replacemsg auth "auth-challenge-page"
    set buffer "<HTML><HEAD><TITLE>Firewall Authentication</TITLE></HEAD><BODY><FORM ACTION=\"/\" method=\"POST\"><INPUT TYPE=\"hidden\" NAME=\"%%MAGICID%%\" VALUE=\"%%MAGICVAL%%\"><TABLE ALIGN=\"CENTER\" width=400 height=250 cellpadding=2 cellspacing=0 border=0 bgcolor=\"#008080\"><TR><TD><TABLE border=0 cellpadding=0 cellspacing=0 bgcolor=\"#9dc8c6\"><TR height=30 bgcolor=\"#008080\"><TD><b><font size=2 face=\"Verdana\" color=\"#ffffff\">Authentication Required</font></b></TD></TR><TR><TD><TABLE border=0 cellpadding=5 cellspacing=0 width=\"320\" align=center><TR><TD colspan=2><font size=2 face=\"Times New Roman\">%%QUESTION%%</font></TD></TR><TR><TD><font size=2 face=\"Times New Roman\">Answer:</font></TD><TD><INPUT TYPE=\"password\" NAME=\"%%PASSWORDID%%\" size=25></TD></TR><TR><TD><INPUT TYPE=\"hidden\" NAME=\"%%USERNAMEID%%\" VALUE=\"%%USERNAMEVAL%%\"><INPUT TYPE=\"hidden\" NAME=\"%%REQUESTID%%\" VALUE=\"%%REQUESTVAL%%\"><INPUT TYPE=\"hidden\" NAME=\"%%REDIRID%%\" VALUE=\"%%PROTURI%%\"><INPUT TYPE=\"hidden\" NAME=\"%%USERGROUPID%%\" VALUE=\"%%USERGROUPVAL%%\"><INPUT TYPE=\"submit\" VALUE=\"Continue\"></TD></TR></TABLE></TD></TR></TABLE></TD></TR></TABLE></FORM></BODY></HTML>"
    set format html
    set header http
end
config system replacemsg auth "auth-keepalive-page"
    set buffer "<HTML>
<HEAD>
<TITLE>Firewall Authentication Keepalive Window</TITLE>
</HEAD>
<BODY>
<SCRIPT LANGUAGE=\"JavaScript\">
var countDownTime=%%TIMEOUT%% + 1;
function countDown(){
countDownTime--;
if (countDownTime <= 0){
    location.href=\"%%KEEPALIVEURL%%\";
    return;
}
document.getElementById(\'countdown\').innerHTML = countDownTime;
counter=setTimeout(\"countDown()\", 1000);
}
function startit(){
    countDown();
}
window.onload=startit
</SCRIPT>
<table width=\"100%\" height=\"100%\"><tr><td align=\"center\">
<H3>This browser window is used to keep your authentication session active.</H3>
<H3>Please leave it open in the background and open a <a href=\"%%AUTH_REDIR_URL%%\" target=\"_blank\">new window</a> to continue.</H3>
<p>Authentication Refresh in <b id=countdown>%%TIMEOUT%%</b> seconds</p>
<p><a href=\"%%AUTH_LOGOUT%%\">logout</a></p>
</td></tr></table>
</BODY>
</HTML>
"
    set format html
    set header http
end
config system replacemsg im "im-file-xfer-block"
    set buffer "Transfer failed.  You are not permitted to transfer the file \"%%FILE%%\"."
    set format text
    set header none
end
config system replacemsg im "im-file-xfer-name"
    set buffer "Transfer %%ACTION%%.  The file name \"%%FILE%%\" matches the configured file name block list."
    set format text
    set header none
end
config system replacemsg im "im-file-xfer-infected"
    set buffer "Transfer %%ACTION%%.  The file \"%%FILE%%\" is infected with the virus %%VIRUS%%.  File quarantined as %%QUARFILENAME%%."
    set format text
    set header none
end
config system replacemsg im "im-file-xfer-size"
    set buffer "Transfer %%ACTION%%.  The \"%%FILE%%\" is larger than the configured limit."
    set format text
    set header none
end
config system replacemsg im "im-voice-chat-block"
    set buffer "Connection failed.  You are not permitted to use voice chat."
    set format text
    set header none
end
config system replacemsg im "im-photo-share-block"
    set buffer "Photo sharing failed.  You are not permitted to share photo."
    set format text
    set header none
end
config system replacemsg sslvpn "sslvpn-login"
    set buffer "<html><head><title>login</title><meta http-equiv=\"Pragma\" content=\"no-cache\"><meta http-equiv=\"cache-control\" content=\"no-cache\"><meta http-equiv=\"cache-control\" content=\"must-revalidate\"><link href=\"/ssl_style.css\" rel=\"stylesheet\" type=\"text/css\"><script language=\"JavaScript\"><!--if (top && top.location != window.location) top.location = top.location;if (window.opener && window.opener.top) { window.opener.top.location = window.opener.top.location; self.close(); }//--></script></head><body class=\"main\"><center><table width=\"100%\" height=\"100%\" align=\"center\" class=\"container\" valign=\"middle\" cellpadding=\"0\" cellspacing=\"0\"><tr valign=middle><td><form action=\"%%SSL_ACT%%\" method=\"%%SSL_METHOD%%\" name=\"f\"><table class=\"list\" cellpadding=10 cellspacing=0 align=center width=400 height=180>%%SSL_LOGIN%%</table>%%SSL_HIDDEN%%</td></tr></table></form></center></body><script>document.forms[0].username.focus();</script></html>"
    set format html
    set header http
end
config system snmp sysinfo
end
config firewall profile
    edit "strict"
        set log-web-ftgd-err enable
        set ftp block oversize quarantine scan splice
        set http block oversize quarantine scan activexfilter bannedword cookiefilter javafilter urlfilter
        unset https
        set imap block oversize quarantine scan bannedword spamemailbwl spamfsip spamfschksum spamfssubmit spamfsurl spamhdrcheck spamraddrdns
        set pop3 block oversize quarantine scan bannedword spamemailbwl spamfsip spamfschksum spamfssubmit spamfsurl spamhdrcheck spamraddrdns
        set smtp block oversize quarantine scan bannedword spamemailbwl spamfsip spamfschksum spamfssubmit spamfsurl spamhdrcheck spamhelodns spamipbwl spamraddrdns spamrbl splice
        set nntp block oversize quarantine scan
        set im block oversize quarantine scan
        set ftgd-wf-options strict-blocking
        set ftgd-wf-https-options strict-blocking
    next
    edit "scan"
        set log-web-ftgd-err enable
        set ftp quarantine scan splice
        set http quarantine scan
        unset https
        set imap quarantine scan
        set pop3 quarantine scan
        set smtp quarantine scan splice
        set nntp quarantine scan
        set im quarantine scan
        set ftgd-wf-options strict-blocking
        set ftgd-wf-https-options strict-blocking
    next
    edit "web"
        set log-web-ftgd-err enable
        set ftp splice
        set http scan bannedword urlfilter
        unset https
        set imap fragmail
        set pop3 fragmail
        set smtp fragmail splice
        unset nntp
        unset im
        set ftgd-wf-options strict-blocking
        set ftgd-wf-https-options strict-blocking
    next
    edit "unfiltered"
        set log-web-ftgd-err enable
        set ftp splice
        unset http
        unset https
        set imap fragmail
        set pop3 fragmail
        set smtp fragmail splice
        unset nntp
        unset im
        set ftgd-wf-options strict-blocking
        set ftgd-wf-https-options strict-blocking
    next
end
config webfilter bword
end
config webfilter exmword
end
config webfilter urlfilter
end
config vpn certificate ca
end
config vpn certificate local
end
config spamfilter bword
end
config spamfilter ipbwl
end
config spamfilter rbl
end
config spamfilter emailbwl
end
config spamfilter mheader
end
config spamfilter iptrust
end
config system session-helper
    edit 1
        set name pptp
        set port 1723
        set protocol 6
    next
    edit 2
        set name h323
        set port 1720
        set protocol 6
    next
    edit 3
        set name ras
        set port 1719
        set protocol 17
    next
    edit 4
        set name tns
        set port 1521
        set protocol 6
    next
    edit 5
        set name tftp
        set port 69
        set protocol 17
    next
    edit 6
        set name rtsp
        set port 554
        set protocol 6
    next
    edit 7
        set name rtsp
        set port 7070
        set protocol 6
    next
    edit 8
        set name ftp
        set port 21
        set protocol 6
    next
    edit 9
        set name mms
        set port 1863
        set protocol 6
    next
    edit 10
        set name pmap
        set port 111
        set protocol 6
    next
    edit 11
        set name pmap
        set port 111
        set protocol 17
    next
    edit 12
        set name sip
        set port 5060
        set protocol 17
    next
    edit 13
        set name dns-udp
        set port 53
        set protocol 17
    next
end
config system auto-install
    set auto-install-config enable
    set auto-install-image enable
end
config system console
    set output more
end
config antivirus filepattern
    edit 1
            config entries
                edit "*.bat"
                next
                edit "*.com"
                next
                edit "*.dll"
                next
                edit "*.doc"
                next
                edit "*.exe"
                next
                edit "*.gz"
                next
                edit "*.hta"
                next
                edit "*.ppt"
                next
                edit "*.rar"
                next
                edit "*.scr"
                next
                edit "*.tar"
                next
                edit "*.tgz"
                next
                edit "*.vb?"
                next
                edit "*.wps"
                next
                edit "*.xl?"
                next
                edit "*.zip"
                next
                edit "*.pif"
                next
                edit "*.cpl"
                next
            end
        set name "builtin-patterns"
    next
end
config antivirus service "http"
    set port 80
    set scan-bzip2 disable
    set uncompnestlimit 12
    set uncompsizelimit 10
end
config antivirus service "https"
    set port 443
    set scan-bzip2 disable
    set uncompnestlimit 0
    set uncompsizelimit 0
end
config antivirus service "ftp"
    set port 21
    set scan-bzip2 disable
    set uncompnestlimit 12
    set uncompsizelimit 10
end
config antivirus service "pop3"
    set port 110
    set scan-bzip2 disable
    set uncompnestlimit 12
    set uncompsizelimit 10
end
config antivirus service "imap"
    set port 143
    set scan-bzip2 disable
    set uncompnestlimit 12
    set uncompsizelimit 10
end
config antivirus service "smtp"
    set port 25
    set scan-bzip2 disable
    set uncompnestlimit 12
    set uncompsizelimit 10
end
config antivirus service "nntp"
    set port 119
    set scan-bzip2 disable
    set uncompnestlimit 12
    set uncompsizelimit 10
end
config antivirus service "im"
    set scan-bzip2 disable
    set uncompnestlimit 12
    set uncompsizelimit 10
end
config antivirus grayware "Adware"
end
config antivirus grayware "Dial"
end
config antivirus grayware "Game"
end
config antivirus grayware "Joke"
end
config antivirus grayware "P2P"
end
config antivirus grayware "Spy"
end
config antivirus grayware "Keylog"
end
config antivirus grayware "Hijacker"
end
config antivirus grayware "Plugin"
end
config antivirus grayware "NMT"
end
config antivirus grayware "RAT"
end
config antivirus grayware "Misc"
end
config antivirus grayware "BHO"
end
config antivirus grayware "Toolbar"
end
config antivirus grayware "Download"
end
config antivirus grayware "HackerTool"
end
#config-version=FG500A-3.00-FW-build403-061106:opmode=1:vdom=0
#conf_file_ver=7235700037401564169
#buildno=0403
config firewall address
    edit "all"
    next
end
config system zone
    edit "test1"
            set interface "lan"
        set intrazone allow
    next
end
config firewall schedule recurring
    edit "always"
        set day sunday monday tuesday wednesday thursday friday saturday
    next
end
config firewall policy
end
config firewall policy6
end
config router rip
        config redistribute "connected"
        end
        config redistribute "static"
        end
        config redistribute "ospf"
        end
        config redistribute "bgp"
        end
end
config router static
    edit 1
        set device "port2"
        set gateway 192.168.100.1
    next
end
config router ospf
        config redistribute "connected"
        end
        config redistribute "static"
        end
        config redistribute "rip"
        end
        config redistribute "bgp"
        end
end
config router bgp
        config redistribute "connected"
        end
        config redistribute "rip"
        end
        config redistribute "ospf"
        end
        config redistribute "static"
        end
end
config router multicast
end
