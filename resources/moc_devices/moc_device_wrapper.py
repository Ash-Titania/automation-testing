from multiprocessing import Process


"""
A context manager wrapper class for moc device servers
"""
class MockDeviceWrapper:
    def __init__(self, start_method, ip = None, port = None, username = 'user', password = 'password'):
        self.server_process = None
        self.start_method = start_method
        self.ip = ip
        self.port = port
        self.username = username
        self.password = password

    def __enter__(self):
        if not self.server_process or not self.server_process.is_alive():
            self.server_process = Process(target=self.start_method, args=(self.ip, self.port, self.username, self.password))
            self.server_process.start()

    def __exit__(self, type, value, traceback):
        if self.server_process.is_alive():
            self.server_process.terminate()
            self.server_process.join()
