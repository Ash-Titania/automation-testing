mock_show_simple_gateways_response_body = """
{
  "objects": [
    {
      "uid": "110d9aa4-7931-a048-9231-37e2e8c9d326",
      "name": "gw-aeea33",
      "type": "simple-gateway",
      "domain": {
        "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "name": "SMC User",
        "domain-type": "domain"
      },
      "interfaces": [
        {
          "name": "eth0",
          "ipv4-address": "10.200.8.177",
          "ipv4-network-mask": "255.255.255.0",
          "ipv4-mask-length": 24,
          "ipv6-address": "",
          "comments": "",
          "color": "black",
          "icon": "NetworkObjects/network",
          "topology": "internal",
          "topology-settings": {
            "ip-address-behind-this-interface": "specific",
            "specific-network": "8 Network",
            "interface-leads-to-dmz": false
          },
          "anti-spoofing": true,
          "anti-spoofing-settings": {
            "action": "detect"
          },
          "security-zone": false
        },
        {
          "name": "Test Network",
          "ipv4-address": "1.1.1.1",
          "ipv4-network-mask": "255.255.255.0",
          "ipv4-mask-length": 24,
          "ipv6-address": "",
          "comments": "",
          "color": "black",
          "icon": "NetworkObjects/network",
          "topology": "automatic",
          "topology-automatic-calculation": "internal",
          "topology-settings": {
            "ip-address-behind-this-interface": "not defined",
            "interface-leads-to-dmz": false
          },
          "anti-spoofing": false,
          "security-zone": false
        }
      ],
      "ipv4-address": "127.0.0.1",
      "dynamic-ip": false,
      "version": "R80.30",
      "os-name": "Gaia",
      "hardware": "Open server",
      "sic-name": "cn=cp_mgmt,o=gw-aeea33.uk.titania.com.xduvup",
      "sic-state": "communicating",
      "firewall": true,
      "firewall-settings": {
        "auto-maximum-limit-for-concurrent-connections": true,
        "maximum-limit-for-concurrent-connections": 25000,
        "auto-calculate-connections-hash-table-size-and-memory-pool": true,
        "connections-hash-size": 32768,
        "memory-pool-size": 6,
        "maximum-memory-pool-size": 30
      },
      "vpn": true,
      "vpn-settings": {
        "maximum-concurrent-ike-negotiations": 1000,
        "maximum-concurrent-tunnels": 10000
      },
      "application-control": false,
      "url-filtering": false,
      "ips": true,
      "content-awareness": false,
      "anti-bot": true,
      "anti-virus": true,
      "threat-emulation": false,
      "threat-extraction": false,
      "save-logs-locally": true,
      "send-alerts-to-server": [
        "gw-aeea33"
      ],
      "send-logs-to-server": [
        "gw-aeea33"
      ],
      "send-logs-to-backup-server": [],
      "logs-settings": {
        "rotate-log-by-file-size": false,
        "rotate-log-file-size-threshold": 1000,
        "rotate-log-on-schedule": false,
        "alert-when-free-disk-space-below-metrics": "percent",
        "alert-when-free-disk-space-below": true,
        "alert-when-free-disk-space-below-threshold": 25,
        "alert-when-free-disk-space-below-type": "popup alert",
        "delete-when-free-disk-space-below-metrics": "percent",
        "delete-when-free-disk-space-below": true,
        "delete-when-free-disk-space-below-threshold": 40,
        "before-delete-keep-logs-from-the-last-days": false,
        "before-delete-keep-logs-from-the-last-days-threshold": 0,
        "before-delete-run-script": false,
        "before-delete-run-script-command": "",
        "stop-logging-when-free-disk-space-below-metrics": "percent",
        "stop-logging-when-free-disk-space-below": false,
        "stop-logging-when-free-disk-space-below-threshold": 5,
        "reject-connections-when-free-disk-space-below-threshold": false,
        "reserve-for-packet-capture-metrics": "mbytes",
        "reserve-for-packet-capture-threshold": 500,
        "delete-index-files-when-index-size-above-metrics": "percent",
        "delete-index-files-when-index-size-above": false,
        "delete-index-files-when-index-size-above-threshold": 100000,
        "delete-index-files-older-than-days": true,
        "delete-index-files-older-than-days-threshold": 14,
        "forward-logs-to-log-server": false,
        "perform-log-rotate-before-log-forwarding": false,
        "update-account-log-every": 3600,
        "detect-new-citrix-ica-application-names": false,
        "turn-on-qos-logging": true
      },
      "comments": "",
      "color": "black",
      "icon": "NetworkObjects/gateway",
      "tags": [],
      "meta-info": {
        "lock": "locked by other session",
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1583768612635,
          "iso-8601": "2020-03-09T15:43+0000"
        },
        "last-modifier": "System",
        "creation-time": {
          "posix": 1571064589117,
          "iso-8601": "2019-10-14T15:49+0100"
        },
        "creator": "System"
      },
      "read-only": false
    }
  ],
  "from": 1,
  "to": 1,
  "total": 1
}"""

mock_show_simple_gateways_details_level_standard_response_body = """
{
  "objects": [
    {
      "uid": "110d9aa4-7931-a048-9231-37e2e8c9d326",
      "name": "gw-aeea33",
      "type": "simple-gateway",
      "domain": {
        "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "name": "SMC User",
        "domain-type": "domain"
      }
    }
  ],
  "from": 1,
  "to": 1,
  "total": 1
}
"""

mock_show_simple_gateway_response_body = """
{
    "uid": "110d9aa4-7931-a048-9231-37e2e8c9d326",
    "name": "gw-aeea33",
    "type": "simple-gateway",
    "domain": {
    "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
    "name": "SMC User",
    "domain-type": "domain"
    },
    "interfaces": [
    {
        "name": "eth0",
        "ipv4-address": "10.200.8.177",
        "ipv4-network-mask": "255.255.255.0",
        "ipv4-mask-length": 24,
        "ipv6-address": "",
        "comments": "",
        "color": "black",
        "icon": "NetworkObjects/network",
        "topology": "internal",
        "topology-settings": {
        "ip-address-behind-this-interface": "specific",
        "specific-network": "8 Network",
        "interface-leads-to-dmz": false
        },
        "anti-spoofing": true,
        "anti-spoofing-settings": {
        "action": "detect"
        },
        "security-zone": false
    },
    {
        "name": "Test Network",
        "ipv4-address": "1.1.1.1",
        "ipv4-network-mask": "255.255.255.0",
        "ipv4-mask-length": 24,
        "ipv6-address": "",
        "comments": "",
        "color": "black",
        "icon": "NetworkObjects/network",
        "topology": "automatic",
        "topology-automatic-calculation": "internal",
        "topology-settings": {
        "ip-address-behind-this-interface": "not defined",
        "interface-leads-to-dmz": false
        },
        "anti-spoofing": false,
        "security-zone": false
    }
    ],
    "ipv4-address": "127.0.0.1",
    "dynamic-ip": false,
    "version": "R80.30",
    "os-name": "Gaia",
    "hardware": "Open server",
    "sic-name": "cn=cp_mgmt,o=gw-aeea33.uk.titania.com.xduvup",
    "sic-state": "communicating",
    "firewall": true,
    "firewall-settings": {
    "auto-maximum-limit-for-concurrent-connections": true,
    "maximum-limit-for-concurrent-connections": 25000,
    "auto-calculate-connections-hash-table-size-and-memory-pool": true,
    "connections-hash-size": 32768,
    "memory-pool-size": 6,
    "maximum-memory-pool-size": 30
    },
    "vpn": true,
    "vpn-settings": {
    "maximum-concurrent-ike-negotiations": 1000,
    "maximum-concurrent-tunnels": 10000
    },
    "application-control": false,
    "url-filtering": false,
    "ips": true,
    "content-awareness": false,
    "anti-bot": true,
    "anti-virus": true,
    "threat-emulation": false,
    "threat-extraction": false,
    "save-logs-locally": true,
    "send-alerts-to-server": [
    "gw-aeea33"
    ],
    "send-logs-to-server": [
    "gw-aeea33"
    ],
    "send-logs-to-backup-server": [],
    "logs-settings": {
    "rotate-log-by-file-size": false,
    "rotate-log-file-size-threshold": 1000,
    "rotate-log-on-schedule": false,
    "alert-when-free-disk-space-below-metrics": "percent",
    "alert-when-free-disk-space-below": true,
    "alert-when-free-disk-space-below-threshold": 25,
    "alert-when-free-disk-space-below-type": "popup alert",
    "delete-when-free-disk-space-below-metrics": "percent",
    "delete-when-free-disk-space-below": true,
    "delete-when-free-disk-space-below-threshold": 40,
    "before-delete-keep-logs-from-the-last-days": false,
    "before-delete-keep-logs-from-the-last-days-threshold": 0,
    "before-delete-run-script": false,
    "before-delete-run-script-command": "",
    "stop-logging-when-free-disk-space-below-metrics": "percent",
    "stop-logging-when-free-disk-space-below": false,
    "stop-logging-when-free-disk-space-below-threshold": 5,
    "reject-connections-when-free-disk-space-below-threshold": false,
    "reserve-for-packet-capture-metrics": "mbytes",
    "reserve-for-packet-capture-threshold": 500,
    "delete-index-files-when-index-size-above-metrics": "percent",
    "delete-index-files-when-index-size-above": false,
    "delete-index-files-when-index-size-above-threshold": 100000,
    "delete-index-files-older-than-days": true,
    "delete-index-files-older-than-days-threshold": 14,
    "forward-logs-to-log-server": false,
    "perform-log-rotate-before-log-forwarding": false,
    "update-account-log-every": 3600,
    "detect-new-citrix-ica-application-names": false,
    "turn-on-qos-logging": true
    },
    "comments": "",
    "color": "black",
    "icon": "NetworkObjects/gateway",
    "tags": [],
    "meta-info": {
    "lock": "locked by other session",
    "validation-state": "ok",
    "last-modify-time": {
        "posix": 1583768612635,
        "iso-8601": "2020-03-09T15:43+0000"
    },
    "last-modifier": "System",
    "creation-time": {
        "posix": 1571064589117,
        "iso-8601": "2019-10-14T15:49+0100"
    },
    "creator": "System"
    },
    "read-only": false
}"""

mock_show_gateways_and_servers_response = """
{
  "objects": [
    {
      "uid": "c9e7aad3-e121-4c39-beb1-1856932dc9b9",
      "name": "Cluster",
      "type": "CpmiGatewayCluster",
      "domain": {
        "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "name": "SMC User",
        "domain-type": "domain"
      },
      "policy": {
        "cluster-members-access-policy-revision": [
          {
            "name": "Secondary_Cluster_Gateway",
            "uid": "2e22e708-b91c-4f4a-903c-a2fa94a785de",
            "policy-name": "Cluster_Policy",
            "revision": {
              "uid": "42209c0c-1597-42b6-a04d-42f1210080a7",
              "name": "admin@5/15/2020",
              "type": "session",
              "domain": {
                "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
                "name": "SMC User",
                "domain-type": "domain"
              },
              "state": "published",
              "user-name": "admin",
              "description": "",
              "last-login-time": {
                "posix": 1589560425630,
                "iso-8601": "2020-05-15T17:33+0100"
              },
              "publish-time": {
                "posix": 1589560539503,
                "iso-8601": "2020-05-15T17:35+0100"
              },
              "expired-session": false,
              "application": "SmartConsole",
              "changes": 1,
              "in-work": false,
              "ip-address": "10.200.63.65",
              "locks": 17,
              "connection-mode": "read write",
              "session-timeout": 600,
              "comments": "",
              "color": "black",
              "icon": "Objects/worksession",
              "tags": [],
              "meta-info": {
                "lock": "unlocked",
                "validation-state": "ok",
                "last-modify-time": {
                  "posix": 1589560539508,
                  "iso-8601": "2020-05-15T17:35+0100"
                },
                "last-modifier": "admin",
                "creation-time": {
                  "posix": 1589560425635,
                  "iso-8601": "2020-05-15T17:33+0100"
                },
                "creator": "admin"
              },
              "read-only": true
            }
          },
          {
            "name": "Primary_Cluster_Gateway",
            "uid": "9701b239-9dde-4ef7-9f61-3ef17b86a08e",
            "policy-name": "Cluster_Policy",
            "revision": {
              "uid": "42209c0c-1597-42b6-a04d-42f1210080a7",
              "name": "admin@5/15/2020",
              "type": "session",
              "domain": {
                "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
                "name": "SMC User",
                "domain-type": "domain"
              },
              "state": "published",
              "user-name": "admin",
              "description": "",
              "last-login-time": {
                "posix": 1589560425630,
                "iso-8601": "2020-05-15T17:33+0100"
              },
              "publish-time": {
                "posix": 1589560539503,
                "iso-8601": "2020-05-15T17:35+0100"
              },
              "expired-session": false,
              "application": "SmartConsole",
              "changes": 1,
              "in-work": false,
              "ip-address": "10.200.63.65",
              "locks": 17,
              "connection-mode": "read write",
              "session-timeout": 600,
              "comments": "",
              "color": "black",
              "icon": "Objects/worksession",
              "tags": [],
              "meta-info": {
                "lock": "unlocked",
                "validation-state": "ok",
                "last-modify-time": {
                  "posix": 1589560539508,
                  "iso-8601": "2020-05-15T17:35+0100"
                },
                "last-modifier": "admin",
                "creation-time": {
                  "posix": 1589560425635,
                  "iso-8601": "2020-05-15T17:33+0100"
                },
                "creator": "admin"
              },
              "read-only": true
            }
          }
        ],
        "access-policy-installed": true,
        "access-policy-name": "Cluster_Policy",
        "access-policy-installation-date": {
          "posix": 1589560799113,
          "iso-8601": "2020-05-15T17:39+0100"
        },
        "threat-policy-installed": false
      },
      "operating-system": "Gaia",
      "hardware": "Open server",
      "version": "R80.30",
      "ipv4-address": "10.200.8.1",
      "interfaces": [
        {
          "interface-name": "eth0",
          "ipv4-address": "10.200.8.1",
          "ipv4-network-mask": "255.255.255.0",
          "ipv4-mask-length": 24,
          "dynamic-ip": false,
          "topology": {
            "leads-to-internet": true,
            "security-zone": {
              "uid": "237a4cbc-7fb6-4d50-872a-4904468271c4",
              "name": "ExternalZone",
              "type": "security-zone",
              "domain": {
                "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef",
                "name": "Check Point Data",
                "domain-type": "data domain"
              }
            }
          }
        }
      ],
      "network-security-blades": {
        "firewall": true
      },
      "management-blades": {},
      "cluster-member-names": [
        "Primary_Cluster_Gateway",
        "Secondary_Cluster_Gateway"
      ],
      "vpn-encryption-domain": "addresses_behind_gw",
      "sic-status": "uninitialized",
      "tags": [],
      "icon": "NetworkObjects/cluster",
      "groups": [],
      "comments": "",
      "color": "firebrick",
      "meta-info": {
        "lock": "unlocked",
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1589559461950,
          "iso-8601": "2020-05-15T17:17+0100"
        },
        "last-modifier": "admin",
        "creation-time": {
          "posix": 1589556519930,
          "iso-8601": "2020-05-15T16:28+0100"
        },
        "creator": "admin"
      },
      "read-only": true
    },
    {
      "uid": "1642deeb-9a99-4d60-8a89-92d2257417d2",
      "name": "gw-ae2d0d",
      "type": "simple-gateway",
      "domain": {
        "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "name": "SMC User",
        "domain-type": "domain"
      },
      "policy": {},
      "operating-system": "Gaia",
      "hardware": "Open server",
      "version": "R80.30",
      "ipv4-address": "10.200.8.6",
      "interfaces": [
        {
          "interface-name": "eth0",
          "ipv4-address": "10.200.8.6",
          "ipv4-network-mask": "255.255.255.0",
          "ipv4-mask-length": 24,
          "dynamic-ip": false,
          "topology": {
            "leads-to-internet": true
          }
        },
        {
          "interface-name": "eth1",
          "ipv4-address": "10.200.69.1",
          "ipv4-network-mask": "255.255.255.0",
          "ipv4-mask-length": 24,
          "dynamic-ip": false,
          "topology": {
            "leads-to-internet": false,
            "ip-address-behind-this-interface": "specific",
            "leads-to-specific-network": {
              "uid": "185161b7-794b-40a5-9ffe-3fab2f369b9b",
              "name": "Titania Networks",
              "type": "group",
              "domain": {
                "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
                "name": "SMC User",
                "domain-type": "domain"
              }
            },
            "leads-to-dmz": false
          }
        }
      ],
      "network-security-blades": {
        "firewall": true,
        "site-to-site-vpn": true
      },
      "management-blades": {
        "network-policy-management": true,
        "secondary": true,
        "logging-and-status": true
      },
      "vpn-encryption-domain": "addresses_behind_gw",
      "sic-status": "communicating",
      "tags": [],
      "icon": "NetworkObjects/gateway",
      "groups": [],
      "comments": "",
      "color": "lemon chiffon",
      "meta-info": {
        "lock": "unlocked",
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1589471231342,
          "iso-8601": "2020-05-14T16:47+0100"
        },
        "last-modifier": "admin",
        "creation-time": {
          "posix": 1589460816324,
          "iso-8601": "2020-05-14T13:53+0100"
        },
        "creator": "admin"
      },
      "read-only": true
    },
    {
      "uid": "110d9aa4-7931-a048-9231-37e2e8c9d326",
      "name": "gw-aeea33",
      "type": "simple-gateway",
      "domain": {
        "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "name": "SMC User",
        "domain-type": "domain"
      },
      "policy": {
        "access-policy-revision": {
          "uid": "fae56360-4e53-4841-89e3-f0c37bb7d061",
          "name": "admin@3/30/2020",
          "type": "session",
          "domain": {
            "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
            "name": "SMC User",
            "domain-type": "domain"
          },
          "state": "published",
          "user-name": "admin",
          "description": "",
          "last-login-time": {
            "posix": 1585563505787,
            "iso-8601": "2020-03-30T11:18+0100"
          },
          "last-logout-time": {
            "posix": 1585563246537,
            "iso-8601": "2020-03-30T11:14+0100"
          },
          "publish-time": {
            "posix": 1585568072215,
            "iso-8601": "2020-03-30T12:34+0100"
          },
          "expired-session": false,
          "application": "SmartConsole",
          "changes": 0,
          "in-work": false,
          "ip-address": "10.200.63.65",
          "locks": 0,
          "connection-mode": "read write",
          "session-timeout": 600,
          "comments": "",
          "color": "black",
          "icon": "Objects/worksession",
          "tags": [],
          "meta-info": {
            "lock": "unlocked",
            "validation-state": "ok",
            "last-modify-time": {
              "posix": 1585568072218,
              "iso-8601": "2020-03-30T12:34+0100"
            },
            "last-modifier": "admin",
            "creation-time": {
              "posix": 1585553908079,
              "iso-8601": "2020-03-30T08:38+0100"
            },
            "creator": "admin"
          },
          "read-only": false
        },
        "threat-policy-revision": {
          "uid": "c68d02de-179c-4746-bc2a-039759190c23",
          "name": "admin@3/24/2020",
          "type": "session",
          "domain": {
            "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
            "name": "SMC User",
            "domain-type": "domain"
          },
          "state": "published",
          "user-name": "admin",
          "description": "",
          "last-login-time": {
            "posix": 1585053510178,
            "iso-8601": "2020-03-24T12:38+0000"
          },
          "publish-time": {
            "posix": 1585053710805,
            "iso-8601": "2020-03-24T12:41+0000"
          },
          "expired-session": false,
          "application": "SmartConsole",
          "changes": 0,
          "in-work": false,
          "ip-address": "10.200.63.90",
          "locks": 0,
          "connection-mode": "read write",
          "session-timeout": 600,
          "comments": "",
          "color": "black",
          "icon": "Objects/worksession",
          "tags": [],
          "meta-info": {
            "lock": "unlocked",
            "validation-state": "ok",
            "last-modify-time": {
              "posix": 1585053710809,
              "iso-8601": "2020-03-24T12:41+0000"
            },
            "last-modifier": "admin",
            "creation-time": {
              "posix": 1585053270293,
              "iso-8601": "2020-03-24T12:34+0000"
            },
            "creator": "admin"
          },
          "read-only": false
        },
        "access-policy-installed": true,
        "access-policy-name": "Standard",
        "access-policy-installation-date": {
          "posix": 1585568120790,
          "iso-8601": "2020-03-30T12:35+0100"
        },
        "threat-policy-installed": true,
        "threat-policy-name": "Standard",
        "threat-policy-installation-date": {
          "posix": 1585053822681,
          "iso-8601": "2020-03-24T12:43+0000"
        }
      },
      "operating-system": "Gaia",
      "hardware": "Open server",
      "version": "R80.30",
      "ipv4-address": "10.200.8.177",
      "interfaces": [
        {
          "interface-name": "eth01",
          "ipv4-address": "10.200.2.0",
          "ipv4-network-mask": "255.255.255.0",
          "ipv4-mask-length": 24,
          "dynamic-ip": false,
          "topology": {
            "leads-to-internet": false,
            "ip-address-behind-this-interface": "specific",
            "leads-to-specific-network": {
              "uid": "a325d073-7fb7-43f9-99d2-177e82083363",
              "name": "Build Servers",
              "type": "network",
              "domain": {
                "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
                "name": "SMC User",
                "domain-type": "domain"
              },
              "subnet4": "10.200.2.0",
              "mask-length4": 24,
              "subnet-mask": "255.255.255.0"
            },
            "leads-to-dmz": false,
            "security-zone": {
              "uid": "d9e7db2a-6974-4f85-b1b3-03cee9db52f7",
              "name": "BuildServers",
              "type": "security-zone",
              "domain": {
                "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
                "name": "SMC User",
                "domain-type": "domain"
              }
            }
          }
        },
        {
          "interface-name": "Test Network",
          "ipv4-address": "0.0.0.0",
          "ipv4-network-mask": "128.0.0.0",
          "ipv4-mask-length": 1,
          "dynamic-ip": false,
          "topology": {
            "leads-to-internet": true
          }
        },
        {
          "interface-name": "eth0",
          "ipv4-address": "10.200.8.177",
          "ipv4-network-mask": "255.255.255.255",
          "ipv4-mask-length": 32,
          "dynamic-ip": false,
          "topology": {
            "leads-to-internet": false,
            "ip-address-behind-this-interface": "specific",
            "leads-to-specific-network": {
              "uid": "fbf0b4d1-1611-4368-a45a-b893adc6daae",
              "name": "8 Network",
              "type": "network",
              "domain": {
                "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
                "name": "SMC User",
                "domain-type": "domain"
              },
              "subnet4": "10.200.8.0",
              "mask-length4": 24,
              "subnet-mask": "255.255.255.0"
            },
            "leads-to-dmz": false
          }
        },
        {
          "interface-name": "Eth02",
          "ipv4-address": "10.200.92.0",
          "ipv4-network-mask": "255.255.255.0",
          "ipv4-mask-length": 24,
          "dynamic-ip": false,
          "topology": {
            "leads-to-internet": false,
            "ip-address-behind-this-interface": "not defined",
            "leads-to-dmz": false
          }
        }
      ],
      "network-security-blades": {
        "firewall": true,
        "site-to-site-vpn": true,
        "monitoring": true
      },
      "management-blades": {
        "network-policy-management": true,
        "logging-and-status": true
      },
      "vpn-encryption-domain": "addresses_behind_gw",
      "sic-status": "communicating",
      "tags": [],
      "icon": "NetworkObjects/gateway",
      "groups": [],
      "comments": "",
      "color": "black",
      "meta-info": {
        "lock": "unlocked",
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1589201207125,
          "iso-8601": "2020-05-11T13:46+0100"
        },
        "last-modifier": "admin",
        "creation-time": {
          "posix": 1571064589117,
          "iso-8601": "2019-10-14T15:49+0100"
        },
        "creator": "System"
      },
      "read-only": true
    },
    {
      "uid": "9701b239-9dde-4ef7-9f61-3ef17b86a08e",
      "name": "Primary_Cluster_Gateway",
      "type": "CpmiClusterMember",
      "domain": {
        "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "name": "SMC User",
        "domain-type": "domain"
      },
      "policy": {},
      "ipv4-address": "10.200.8.172",
      "interfaces": [
        {
          "interface-name": "eth0",
          "ipv4-address": "10.200.8.172",
          "ipv4-network-mask": "255.255.255.0",
          "ipv4-mask-length": 24,
          "dynamic-ip": false,
          "topology": {
            "leads-to-internet": true,
            "security-zone": {
              "uid": "237a4cbc-7fb6-4d50-872a-4904468271c4",
              "name": "ExternalZone",
              "type": "security-zone",
              "domain": {
                "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef",
                "name": "Check Point Data",
                "domain-type": "data domain"
              }
            }
          }
        },
        {
          "interface-name": "eth1",
          "ipv4-address": "192.168.171.1",
          "ipv4-network-mask": "255.255.255.0",
          "ipv4-mask-length": 24,
          "dynamic-ip": false,
          "topology": {
            "leads-to-internet": false,
            "ip-address-behind-this-interface": "network defined by the interface ip and net mask",
            "leads-to-dmz": false,
            "security-zone": {
              "uid": "e8131db2-8388-42a5-924a-82de32db20f7",
              "name": "InternalZone",
              "type": "security-zone",
              "domain": {
                "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef",
                "name": "Check Point Data",
                "domain-type": "data domain"
              }
            }
          }
        }
      ],
      "sic-status": "communicating",
      "tags": [],
      "icon": "NetworkObjects/Cluster_member",
      "groups": [],
      "comments": "",
      "color": "violet red",
      "meta-info": {
        "lock": "unlocked",
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1589559456918,
          "iso-8601": "2020-05-15T17:17+0100"
        },
        "last-modifier": "admin",
        "creation-time": {
          "posix": 1589556537144,
          "iso-8601": "2020-05-15T16:28+0100"
        },
        "creator": "admin"
      },
      "read-only": true
    },
    {
      "uid": "2e22e708-b91c-4f4a-903c-a2fa94a785de",
      "name": "Secondary_Cluster_Gateway",
      "type": "CpmiClusterMember",
      "domain": {
        "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "name": "SMC User",
        "domain-type": "domain"
      },
      "policy": {},
      "ipv4-address": "10.200.8.2",
      "interfaces": [
        {
          "interface-name": "eth0",
          "ipv4-address": "10.200.8.2",
          "ipv4-network-mask": "255.255.255.0",
          "ipv4-mask-length": 24,
          "dynamic-ip": false,
          "topology": {
            "leads-to-internet": true,
            "security-zone": {
              "uid": "237a4cbc-7fb6-4d50-872a-4904468271c4",
              "name": "ExternalZone",
              "type": "security-zone",
              "domain": {
                "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef",
                "name": "Check Point Data",
                "domain-type": "data domain"
              }
            }
          }
        },
        {
          "interface-name": "eth1",
          "ipv4-address": "192.168.171.2",
          "ipv4-network-mask": "255.255.255.0",
          "ipv4-mask-length": 24,
          "dynamic-ip": false,
          "topology": {
            "leads-to-internet": false,
            "ip-address-behind-this-interface": "network defined by the interface ip and net mask",
            "leads-to-dmz": false,
            "security-zone": {
              "uid": "e8131db2-8388-42a5-924a-82de32db20f7",
              "name": "InternalZone",
              "type": "security-zone",
              "domain": {
                "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef",
                "name": "Check Point Data",
                "domain-type": "data domain"
              }
            }
          }
        }
      ],
      "sic-status": "communicating",
      "tags": [],
      "icon": "NetworkObjects/Cluster_member",
      "groups": [],
      "comments": "",
      "color": "blue",
      "meta-info": {
        "lock": "unlocked",
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1589559459606,
          "iso-8601": "2020-05-15T17:17+0100"
        },
        "last-modifier": "admin",
        "creation-time": {
          "posix": 1589556814474,
          "iso-8601": "2020-05-15T16:33+0100"
        },
        "creator": "admin"
      },
      "read-only": true
    }
  ],
  "from": 1,
  "to": 5,
  "total": 5
}
"""