mock_show_administrators_response = """
{
    "from": 1,
    "objects": [
        {
        "domain": {
            "domain-type": "mds",
            "name": "System Data",
            "uid": "a0eebc99-afed-4ef8-bb6d-fedfedfedfed"
        },
        "name": "admin",
        "type": "administrator",
        "uid": "d728a0aa-df30-4af6-8ab4-a5fa68270cf8"
        },
        {
        "domain": {
            "domain-type": "mds",
            "name": "System Data",
            "uid": "a0eebc99-afed-4ef8-bb6d-fedfedfedfed"
        },
        "name": "admin2",
        "type": "administrator",
        "uid": "81e105f5-b869-4be3-9755-a7b4635dedac"
        },
        {
        "domain": {
            "domain-type": "mds",
            "name": "System Data",
            "uid": "a0eebc99-afed-4ef8-bb6d-fedfedfedfed"
        },
        "name": "OS Password Authentication User",
        "type": "administrator",
        "uid": "d2d17a4c-ea17-446d-85a9-a330186e9bc9"
        },
        {
        "domain": {
            "domain-type": "mds",
            "name": "System Data",
            "uid": "a0eebc99-afed-4ef8-bb6d-fedfedfedfed"
        },
        "name": "RADIUS Authentication User",
        "type": "administrator",
        "uid": "f6ecdd56-2623-4e88-b52e-281b0ec2db90"
        },
        {
        "domain": {
            "domain-type": "mds",
            "name": "System Data",
            "uid": "a0eebc99-afed-4ef8-bb6d-fedfedfedfed"
        },
        "name": "SecurID Authentication User",
        "type": "administrator",
        "uid": "1eb2dc23-01ee-4b5e-97a1-73655879e4b2"
        },
        {
        "domain": {
            "domain-type": "mds",
            "name": "System Data",
            "uid": "a0eebc99-afed-4ef8-bb6d-fedfedfedfed"
        },
        "name": "TACACS Authentication User",
        "type": "administrator",
        "uid": "3f5c58c6-bd5c-41d4-8688-d3417f2c5130"
        },
        {
        "domain": {
            "domain-type": "mds",
            "name": "System Data",
            "uid": "a0eebc99-afed-4ef8-bb6d-fedfedfedfed"
        },
        "name": "Undefined Authentication User",
        "type": "administrator",
        "uid": "c86b2a39-4c5b-4e90-9417-0b44178f07c7"
        }
    ],
    "to": 7,
    "total": 7
}
"""