
mock_show_hosts_response_body = """
{
  "objects": [
    {
      "uid": "984865d9-4333-4cb8-8d45-3cefa3967761",
      "name": "Devices_Server",
      "type": "host",
      "domain": {
        "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "name": "SMC User",
        "domain-type": "domain"
      },
      "ipv4-address": "10.10.1.10"
    }
  ],
  "from": 1,
  "to": 1,
  "total": 1
}
"""


mock_show_networks_response_body = """
{
  "objects": [
    {
      "uid": "7172a352-383c-4d8b-9fd3-b5d26a39de1c",
      "name": "10.200 network",
      "type": "network",
      "domain": {
        "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "name": "SMC User",
        "domain-type": "domain"
      },
      "subnet4": "10.200.0.0",
      "mask-length4": 16,
      "subnet-mask": "255.255.0.0"
    }
  ],
  "from": 1,
  "to": 1,
  "total": 1
}
"""

mock_show_address_ranges_response_body = """
{
  "objects": [
    {
      "uid": "dca14d82-d473-471c-805d-5dd1ffece5af",
      "name": "LocalMachine_Loopback",
      "type": "address-range",
      "domain": {
        "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "name": "SMC User",
        "domain-type": "domain"
      },
      "ipv4-address-first": "127.0.0.1",
      "ipv4-address-last": "127.255.255.255"
    }
  ],
  "from": 1,
  "to": 1,
  "total": 1
}
"""


mock_show_multicast_address_ranges_response_body = """
{
  "objects": [
    {
      "uid": "1a53c1c6-7d3e-44dd-aa77-52d6e497b1fa",
      "name": "All_DHCPv6_Servers",
      "type": "multicast-address-range",
      "domain": {
        "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef",
        "name": "Check Point Data",
        "domain-type": "data domain"
      },
      "ipv6-address-first": "ff05::1:3",
      "ipv6-address-last": "ff05::1:3"
    }
  ],
  "from": 1,
  "to": 1,
  "total": 1
}
"""

mock_show_simple_gateways_response_body = """
{
  "objects": [
    {
      "uid": "110d9aa4-7931-a048-9231-37e2e8c9d326",
      "name": "gw-aeea33",
      "type": "simple-gateway",
      "domain": {
        "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "name": "SMC User",
        "domain-type": "domain"
      },
      "interfaces": [
        {
          "name": "eth0",
          "ipv4-address": "10.200.8.177",
          "ipv4-network-mask": "255.255.255.0",
          "ipv4-mask-length": 24,
          "ipv6-address": "",
          "comments": "",
          "color": "black",
          "icon": "NetworkObjects/network",
          "topology": "internal",
          "topology-settings": {
            "ip-address-behind-this-interface": "specific",
            "specific-network": "8 Network",
            "interface-leads-to-dmz": false
          },
          "anti-spoofing": true,
          "anti-spoofing-settings": {
            "action": "detect"
          },
          "security-zone": false
        },
        {
          "name": "Test Network",
          "ipv4-address": "1.1.1.1",
          "ipv4-network-mask": "255.255.255.0",
          "ipv4-mask-length": 24,
          "ipv6-address": "",
          "comments": "",
          "color": "black",
          "icon": "NetworkObjects/network",
          "topology": "automatic",
          "topology-automatic-calculation": "internal",
          "topology-settings": {
            "ip-address-behind-this-interface": "not defined",
            "interface-leads-to-dmz": false
          },
          "anti-spoofing": false,
          "security-zone": false
        }
      ],
      "ipv4-address": "10.200.8.177",
      "dynamic-ip": false,
      "version": "R80.30",
      "os-name": "Gaia",
      "hardware": "Open server",
      "sic-name": "cn=cp_mgmt,o=gw-aeea33.uk.titania.com.xduvup",
      "sic-state": "communicating",
      "firewall": true,
      "firewall-settings": {
        "auto-maximum-limit-for-concurrent-connections": true,
        "maximum-limit-for-concurrent-connections": 25000,
        "auto-calculate-connections-hash-table-size-and-memory-pool": true,
        "connections-hash-size": 32768,
        "memory-pool-size": 6,
        "maximum-memory-pool-size": 30
      },
      "vpn": true,
      "vpn-settings": {
        "maximum-concurrent-ike-negotiations": 1000,
        "maximum-concurrent-tunnels": 10000
      },
      "application-control": false,
      "url-filtering": false,
      "ips": true,
      "content-awareness": false,
      "anti-bot": true,
      "anti-virus": true,
      "threat-emulation": false,
      "threat-extraction": false,
      "save-logs-locally": true,
      "send-alerts-to-server": [
        "gw-aeea33"
      ],
      "send-logs-to-server": [
        "gw-aeea33"
      ],
      "send-logs-to-backup-server": [],
      "logs-settings": {
        "rotate-log-by-file-size": false,
        "rotate-log-file-size-threshold": 1000,
        "rotate-log-on-schedule": false,
        "alert-when-free-disk-space-below-metrics": "percent",
        "alert-when-free-disk-space-below": true,
        "alert-when-free-disk-space-below-threshold": 25,
        "alert-when-free-disk-space-below-type": "popup alert",
        "delete-when-free-disk-space-below-metrics": "percent",
        "delete-when-free-disk-space-below": true,
        "delete-when-free-disk-space-below-threshold": 40,
        "before-delete-keep-logs-from-the-last-days": false,
        "before-delete-keep-logs-from-the-last-days-threshold": 0,
        "before-delete-run-script": false,
        "before-delete-run-script-command": "",
        "stop-logging-when-free-disk-space-below-metrics": "percent",
        "stop-logging-when-free-disk-space-below": false,
        "stop-logging-when-free-disk-space-below-threshold": 5,
        "reject-connections-when-free-disk-space-below-threshold": false,
        "reserve-for-packet-capture-metrics": "mbytes",
        "reserve-for-packet-capture-threshold": 500,
        "delete-index-files-when-index-size-above-metrics": "percent",
        "delete-index-files-when-index-size-above": false,
        "delete-index-files-when-index-size-above-threshold": 100000,
        "delete-index-files-older-than-days": true,
        "delete-index-files-older-than-days-threshold": 14,
        "forward-logs-to-log-server": false,
        "perform-log-rotate-before-log-forwarding": false,
        "update-account-log-every": 3600,
        "detect-new-citrix-ica-application-names": false,
        "turn-on-qos-logging": true
      },
      "comments": "",
      "color": "black",
      "icon": "NetworkObjects/gateway",
      "tags": [],
      "meta-info": {
        "lock": "locked by other session",
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1583768612635,
          "iso-8601": "2020-03-09T15:43+0000"
        },
        "last-modifier": "System",
        "creation-time": {
          "posix": 1571064589117,
          "iso-8601": "2019-10-14T15:49+0100"
        },
        "creator": "System"
      },
      "read-only": false
    }
  ],
  "from": 1,
  "to": 1,
  "total": 1
}
"""

mock_show_security_zones_response_body = """
{
  "objects": [
    {
      "uid": "8c4041ea-ff14-4e4b-a9d9-4183d18c790a",
      "name": "DMZZone",
      "type": "security-zone",
      "domain": {
        "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef",
        "name": "Check Point Data",
        "domain-type": "data domain"
      }
    }
  ],
  "from": 1,
  "to": 1,
  "total": 1
}
"""

mock_show_dynamic_objects_response_body = """
{
  "objects": [
    {
      "uid": "d67128b1-bdba-4724-93e8-336e45853b0a",
      "name": "LocalMachine_All_Interfaces",
      "type": "dynamic-object",
      "domain": {
        "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef",
        "name": "Check Point Data",
        "domain-type": "data domain"
      }
    }
  ],
  "from": 1,
  "to": 1,
  "total": 1
}
"""

mock_show_groups_response_body = """
{
  "objects": [
    {
      "uid": "185161b7-794b-40a5-9ffe-3fab2f369b9b",
      "name": "Titania Networks",
      "type": "group",
      "domain": {
        "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "name": "SMC User",
        "domain-type": "domain"
      }
    }
  ],
  "from": 1,
  "to": 1,
  "total": 1
}
"""

mock_show_group_response_body = """
{
  "uid": "185161b7-794b-40a5-9ffe-3fab2f369b9b",
  "name": "Titania Networks",
  "type": "group",
  "domain": {
    "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
    "name": "SMC User",
    "domain-type": "domain"
  },
  "members": [
    {
      "uid": "7172a352-383c-4d8b-9fd3-b5d26a39de1c",
      "name": "10.200 network",
      "type": "network",
      "domain": {
        "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "name": "SMC User",
        "domain-type": "domain"
      },
      "broadcast": "allow",
      "subnet4": "10.200.0.0",
      "mask-length4": 16,
      "subnet-mask": "255.255.0.0",
      "nat-settings": {
        "auto-rule": false
      },
      "groups": [
        "185161b7-794b-40a5-9ffe-3fab2f369b9b"
      ],
      "comments": "",
      "color": "black",
      "icon": "NetworkObjects/network",
      "tags": [],
      "meta-info": {
        "lock": "unlocked",
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1572280727148,
          "iso-8601": "2019-10-28T16:38+0000"
        },
        "last-modifier": "admin",
        "creation-time": {
          "posix": 1572280727148,
          "iso-8601": "2019-10-28T16:38+0000"
        },
        "creator": "admin"
      },
      "read-only": false
    },
    {
      "uid": "a325d073-7fb7-43f9-99d2-177e82083363",
      "name": "Build Servers",
      "type": "network",
      "domain": {
        "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "name": "SMC User",
        "domain-type": "domain"
      },
      "broadcast": "allow",
      "subnet4": "10.200.2.0",
      "mask-length4": 24,
      "subnet-mask": "255.255.255.0",
      "nat-settings": {
        "auto-rule": true,
        "hide-behind": "gateway",
        "install-on": "All",
        "method": "hide"
      },
      "groups": [
        "185161b7-794b-40a5-9ffe-3fab2f369b9b"
      ],
      "comments": "",
      "color": "black",
      "icon": "NetworkObjects/network",
      "tags": [],
      "meta-info": {
        "lock": "unlocked",
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1586270708167,
          "iso-8601": "2020-04-07T15:45+0100"
        },
        "last-modifier": "admin",
        "creation-time": {
          "posix": 1586270708167,
          "iso-8601": "2020-04-07T15:45+0100"
        },
        "creator": "admin"
      },
      "read-only": false
    }
  ],
  "groups": [],
  "comments": "This is a group of all titania networks  ",
  "color": "cyan",
  "icon": "General/group",
  "tags": [],
  "meta-info": {
    "lock": "unlocked",
    "validation-state": "ok",
    "last-modify-time": {
      "posix": 1586349151004,
      "iso-8601": "2020-04-08T13:32+0100"
    },
    "last-modifier": "admin",
    "creation-time": {
      "posix": 1586349103079,
      "iso-8601": "2020-04-08T13:31+0100"
    },
    "creator": "admin"
  },
  "read-only": false
}
"""