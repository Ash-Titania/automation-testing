mock_show_services_tcp_response_body = """
{
  "objects": [
    {
      "uid": "97aeb44f-9aea-11d5-bd16-0090272ccb30",
      "name": "AOL",
      "type": "service-tcp",
      "domain": {
        "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef",
        "name": "Check Point Data",
        "domain-type": "data domain"
      },
      "port": "5190",
      "match-by-protocol-signature": false,
      "override-default-settings": false,
      "session-timeout": 3600,
      "use-default-session-timeout": true,
      "match-for-any": true,
      "sync-connections-on-cluster": true,
      "aggressive-aging": {
        "enable": true,
        "timeout": 600,
        "use-default-timeout": true,
        "default-timeout": 0
      },
      "keep-connections-open-after-policy-installation": false,
      "comments": "AOL Instant Messenger. Also used by: ICQ & Apple iChat",
      "color": "red",
      "icon": "Services/TCPService",
      "tags": [],
      "meta-info": {
        "lock": "unlocked",
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1554685758865,
          "iso-8601": "2019-04-08T02:09+0100"
        },
        "last-modifier": "System",
        "creation-time": {
          "posix": 1554685758865,
          "iso-8601": "2019-04-08T02:09+0100"
        },
        "creator": "System"
      },
      "read-only": false
    }
  ],
  "from": 1,
  "to": 1,
  "total": 1
}"""


mock_show_services_udp_response_body = """
{
  "objects": [
    {
      "uid": "97aeb3d6-9aea-11d5-bd16-0090272ccb30",
      "name": "archie",
      "type": "service-udp",
      "domain": {
        "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef",
        "name": "Check Point Data",
        "domain-type": "data domain"
      },
      "accept-replies": true,
      "port": "1525",
      "match-by-protocol-signature": false,
      "override-default-settings": false,
      "session-timeout": 40,
      "use-default-session-timeout": true,
      "match-for-any": true,
      "sync-connections-on-cluster": true,
      "aggressive-aging": {
        "enable": true,
        "timeout": 15,
        "use-default-timeout": true,
        "default-timeout": 0
      },
      "keep-connections-open-after-policy-installation": false,
      "comments": "Archie Internet Protocol, search for files over FTP servers",
      "color": "forest green",
      "icon": "Services/UDPService",
      "tags": [],
      "meta-info": {
        "lock": "unlocked",
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1554685756333,
          "iso-8601": "2019-04-08T02:09+0100"
        },
        "last-modifier": "System",
        "creation-time": {
          "posix": 1554685756333,
          "iso-8601": "2019-04-08T02:09+0100"
        },
        "creator": "System"
      },
      "read-only": false
    }
  ],
  "from": 1,
  "to": 1,
  "total": 1
}"""


mock_show_services_icmp_response_body = """
{
  "objects": [
    {
      "uid": "97aeb410-9aea-11d5-bd16-0090272ccb30",
      "name": "info-reply",
      "type": "service-icmp",
      "domain": {
        "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef",
        "name": "Check Point Data",
        "domain-type": "data domain"
      },
      "icmp-type": 16,
      "keep-connections-open-after-policy-installation": false,
      "comments": "ICMP, info reply",
      "color": "orchid",
      "icon": "Services/ICMPService",
      "tags": [],
      "meta-info": {
        "lock": "unlocked",
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1554685757808,
          "iso-8601": "2019-04-08T02:09+0100"
        },
        "last-modifier": "System",
        "creation-time": {
          "posix": 1554685757808,
          "iso-8601": "2019-04-08T02:09+0100"
        },
        "creator": "System"
      },
      "read-only": false
    }
  ],
  "from": 1,
  "to": 1,
  "total": 1
}"""


mock_show_services_icmp6_response_body = """
{
  "objects": [
    {
      "uid": "6f23dae7-0b9a-44f6-8515-b24690cd996c",
      "name": "home-agent-address-discovery2",
      "type": "service-icmp6",
      "domain": {
        "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef",
        "name": "Check Point Data",
        "domain-type": "data domain"
      },
      "icmp-type": 145,
      "keep-connections-open-after-policy-installation": false,
      "comments": "",
      "color": "blue",
      "icon": "Services/ICMPV6Service",
      "tags": [],
      "meta-info": {
        "lock": "unlocked",
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1554685755583,
          "iso-8601": "2019-04-08T02:09+0100"
        },
        "last-modifier": "System",
        "creation-time": {
          "posix": 1554685755583,
          "iso-8601": "2019-04-08T02:09+0100"
        },
        "creator": "System"
      },
      "read-only": false
    }
  ],
  "from": 1,
  "to": 1,
  "total": 1
}"""


mock_show_services_sctp_response_body = """
{
  "objects": [
    {
      "uid": "602d75d3-7563-475f-a71b-22ef797cb598",
      "name": "test-sctp",
      "type": "service-sctp",
      "domain": {
        "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "name": "SMC User",
        "domain-type": "domain"
      },
      "port": "1818",
      "session-timeout": 3600,
      "use-default-session-timeout": true,
      "match-for-any": false,
      "sync-connections-on-cluster": true,
      "aggressive-aging": {
        "enable": true,
        "timeout": 600,
        "use-default-timeout": true,
        "default-timeout": 0
      },
      "keep-connections-open-after-policy-installation": false,
      "comments": "This is a comment",
      "color": "black",
      "icon": "Services/SCTPService",
      "tags": [],
      "meta-info": {
        "lock": "unlocked",
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1586344884416,
          "iso-8601": "2020-04-08T12:21+0100"
        },
        "last-modifier": "admin",
        "creation-time": {
          "posix": 1586344884416,
          "iso-8601": "2020-04-08T12:21+0100"
        },
        "creator": "admin"
      },
      "read-only": false
    }
  ],
  "from": 1,
  "to": 1,
  "total": 1
}"""


mock_show_services_dce_rpc_response_body = """
{
  "objects": [
    {
      "uid": "3d0d46b6-4ddb-43e0-9faa-c969dbc3e19f",
      "name": "ALL_DCE_RPC",
      "type": "service-dce-rpc",
      "domain": {
        "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef",
        "name": "Check Point Data",
        "domain-type": "data domain"
      },
      "interface-uuid": "00000000-0000-0000-0000-000000000000",
      "keep-connections-open-after-policy-installation": false,
      "comments": "Special Service For Allowing All DCE-RPC Services",
      "color": "red",
      "icon": "Services/DCEService",
      "tags": [],
      "meta-info": {
        "lock": "unlocked",
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1554685759078,
          "iso-8601": "2019-04-08T02:09+0100"
        },
        "last-modifier": "System",
        "creation-time": {
          "posix": 1554685759078,
          "iso-8601": "2019-04-08T02:09+0100"
        },
        "creator": "System"
      },
      "read-only": false
    }
  ],
  "from": 1,
  "to": 1,
  "total": 1
}"""


mock_show_services_rpc_response_body = """
{
  "objects": [
    {
      "uid": "f4c95e3e-a820-4b35-b35e-0afb703eb11e",
      "name": "cachefsd",
      "type": "service-rpc",
      "domain": {
        "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef",
        "name": "Check Point Data",
        "domain-type": "data domain"
      },
      "program-number": 100235,
      "keep-connections-open-after-policy-installation": false,
      "comments": "SUN NFS/RPC file system cachefs daemon",
      "color": "burlywood",
      "icon": "Services/RPCService",
      "tags": [],
      "meta-info": {
        "lock": "unlocked",
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1554685759341,
          "iso-8601": "2019-04-08T02:09+0100"
        },
        "last-modifier": "System",
        "creation-time": {
          "posix": 1554685759341,
          "iso-8601": "2019-04-08T02:09+0100"
        },
        "creator": "System"
      },
      "read-only": false
    }
  ],
  "from": 1,
  "to": 1,
  "total": 1
}"""


mock_show_service_groups_response_body = """
{
  "objects": [
    {
      "uid": "1e8e7ba9-df91-cc44-b001-bb86bac79463",
      "name": "AD_Dcerpc_services",
      "type": "service-group",
      "domain": {
        "uid": "a0bbbc99-adef-4ef8-bb6d-cebcebcebceb",
        "name": "IPS Data",
        "domain-type": "data domain"
      },
      "members": [
        "294f3536-1ada-5345-ad3a-2ee6d3962828",
        "3c04ede1-cefd-3c43-a731-d3b56950a8ef",
        "9964758d-ea11-4442-b66e-9dd9ac764812",
        "d9a971be-f8cb-994b-9273-0f13fefbc14e",
        "4932b631-d524-e64c-8656-b949a9c5d711",
        "0c09c87d-393c-ef4d-89fe-09c5aaa61812"
      ],
      "comments": "AD Dcerpc Service Group",
      "color": "blue",
      "icon": "General/group",
      "tags": [],
      "meta-info": {
        "lock": "unlocked",
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1571064651798,
          "iso-8601": "2019-10-14T15:50+0100"
        },
        "last-modifier": "System",
        "creation-time": {
          "posix": 1571064651798,
          "iso-8601": "2019-10-14T15:50+0100"
        },
        "creator": "System"
      },
      "read-only": false
    }
  ],
  "from": 1,
  "to": 1,
  "total": 1
}"""

mock_show_service_group_response_body = """
{
    "color": "blue",
    "comments": "AD Dcerpc Service Group",
    "domain": {
	    "domain-type": "data domain",
	    "name": "IPS Data",
	    "uid": "a0bbbc99-adef-4ef8-bb6d-cebcebcebceb"
    },
    "groups": [],
    "icon": "General/group",
    "members": [
	    {
		    "domain": {
			    "domain-type": "data domain",
			    "name": "IPS Data",
			    "uid": "a0bbbc99-adef-4ef8-bb6d-cebcebcebceb"
		    },
		    "name": "DCOM-IWbemLevel1Login",
		    "type": "service-dce-rpc",
		    "uid": "294f3536-1ada-5345-ad3a-2ee6d3962828"
	    },
	    {
		    "domain": {
			    "domain-type": "data domain",
			    "name": "IPS Data",
			    "uid": "a0bbbc99-adef-4ef8-bb6d-cebcebcebceb"
		    },
		    "name": "DCOM-IRemUnknown",
		    "type": "service-dce-rpc",
		    "uid": "3c04ede1-cefd-3c43-a731-d3b56950a8ef"
	    },
	    {
		    "domain": {
			    "domain-type": "data domain",
			    "name": "IPS Data",
			    "uid": "a0bbbc99-adef-4ef8-bb6d-cebcebcebceb"
		    },
		    "name": "IWbemServices",
		    "type": "service-dce-rpc",
		    "uid": "9964758d-ea11-4442-b66e-9dd9ac764812"
	    },
	    {
		    "domain": {
			    "domain-type": "data domain",
			    "name": "IPS Data",
			    "uid": "a0bbbc99-adef-4ef8-bb6d-cebcebcebceb"
		    },
		    "name": "IWbemFetchSmartEnum",
		    "type": "service-dce-rpc",
		    "uid": "d9a971be-f8cb-994b-9273-0f13fefbc14e"
	    },
	    {
		    "domain": {
			    "domain-type": "data domain",
			    "name": "IPS Data",
			    "uid": "a0bbbc99-adef-4ef8-bb6d-cebcebcebceb"
		    },
		    "name": "IWbemWCOSmartEnum",
		    "type": "service-dce-rpc",
		    "uid": "4932b631-d524-e64c-8656-b949a9c5d711"
	    },
	    {
		    "domain": {
			    "domain-type": "data domain",
			    "name": "IPS Data",
			    "uid": "a0bbbc99-adef-4ef8-bb6d-cebcebcebceb"
		    },
		    "name": "IenumWbemClassObject",
		    "type": "service-dce-rpc",
		    "uid": "0c09c87d-393c-ef4d-89fe-09c5aaa61812"
	    }
    ],
    "meta-info": {
	    "creation-time": {
		    "iso-8601": "2019-10-14T15:50+0100",
		    "posix": 1571064651798
	    },
	    "creator": "System",
	    "last-modifier": "System",
	    "last-modify-time": {
		    "iso-8601": "2019-10-14T15:50+0100",
		    "posix": 1571064651798
	    },
	    "lock": "unlocked",
	    "validation-state": "ok"
    },
    "name": "AD_Dcerpc_services",
    "read-only": false,
    "tags": [],
    "type": "service-group",
    "uid": "1e8e7ba9-df91-cc44-b001-bb86bac79463"
}"""