mock_show_vpn_communities_meshed_response_body = """
{
  "objects": [
    "cbfed2ed-04d7-49b7-950c-a182d872fca8"
  ],
  "from": 1,
  "to": 1,
  "total": 1
}
"""

mock_show_vpn_communities_star_response_body = """
{
  "objects": [
    "cfbbb4d3-3ab1-4aff-844c-e31044490eec"
  ],
  "from": 1,
  "to": 1,
  "total": 1
}
"""

mock_show_generic_object_response_body_my_intranet = """
{
  "objectValidationState": null,
  "color": "BLACK",
  "automaticRimSatellites": false,
  "customerScriptSatellites": false,
  "supportWireMode": false,
  "customerScriptCenter": false,
  "routeRetPackets": false,
  "enableMep": false,
  "participantsDomains": [],
  "type": "intranet_community",
  "id": 1,
  "satelliteGateways": [],
  "disableNat": false,
  "mepMechanism": "SRC",
  "allowAllEncryptedTraffic": false,
  "topology": "MESHED",
  "extGatewaysSharedSecret": [],
  "participantGateways": [
    "110d9aa4-7931-a048-9231-37e2e8c9d326"
  ],
  "disableNatOn": "BOTH",
  "permanentTunnelsDef": "NONE",
  "routeInjectionTrack": "LOG",
  "routeThroughCenter": "NONE",
  "selMechanism": "FIRST",
  "backupStickiness": false,
  "meshedInCenter": false,
  "permanentTunnelParticipantList": [],
  "cryptography": {
    "objId": "934c8ede-b422-44ff-aca1-139dce9e652f",
    "checkPointObjId": null,
    "domainsPreset": null,
    "domainId": "41e821a0-3720-11e3-aa6e-0800200c9fde",
    "cryptographyTypeSupport": "IKE_V1_ONLY",
    "cryptographyProfile": "CUSTOM_PROFILE",
    "folderPath": "072e125f-94bb-4876-9335-1f74cb53383a",
    "text": null,
    "folder": "072e125f-94bb-4876-9335-1f74cb53383a",
    "is_owned": false,
    "ownedName": ""
  },
  "automaticRim": true,
  "tunnelGranularity": "PER_SUBNET",
  "defaultMepRule": {
    "objId": "7afe960c-5a3d-4cb1-b23e-ee042d433908",
    "checkPointObjId": null,
    "domainsPreset": null,
    "domainId": "41e821a0-3720-11e3-aa6e-0800200c9fde",
    "icon": "Unknown",
    "source": [],
    "color": "BLACK",
    "name": "",
    "priority3": [],
    "priority2": [],
    "priority1": [],
    "displayName": "",
    "comments": "",
    "folderPath": "072e125f-94bb-4876-9335-1f74cb53383a",
    "text": null,
    "folder": "072e125f-94bb-4876-9335-1f74cb53383a",
    "is_owned": false,
    "ownedName": "",
    "tags": [],
    "customFields": [],
    "metaInfo": null,
    "features": [],
    "systemTags": []
  },
  "vpnMepResolverNotification": "LOG",
  "addRoutedDomain": null,
  "permanentTunnelList": [],
  "allowAllEncryptedTrafficOn": "BOTH",
  "supportWireModeRouting": false,
  "permanentTunnelUpTrack": "LOG",
  "permanentTunnelParticipants": "ALL_MEMBERS",
  "ikeP2": {
    "objId": "3cbfd027-a233-471a-aa65-1507c248f692",
    "checkPointObjId": null,
    "domainsPreset": null,
    "domainId": "41e821a0-3720-11e3-aa6e-0800200c9fde",
    "ikeP2UseSubnets": true,
    "ikeP2UseRekeyKbytes": false,
    "ikeP2EnableSupernetFromR8020": "BY_GLOBAL",
    "ikeP2RekeyTime": 3600,
    "ikeP2UsePfs": false,
    "ikeP2EncAlg": "AES_MINUS_128",
    "ikeP2RekeyKbytes": 50000,
    "ikeP2HashAlg": "SHA1",
    "ikeP2Ipcomp": "NONE",
    "ikeP2PfsDhGrp": "97aeb629-9aea-11d5-bd16-0090272ccb30",
    "folderPath": "072e125f-94bb-4876-9335-1f74cb53383a",
    "text": null,
    "folder": "072e125f-94bb-4876-9335-1f74cb53383a",
    "is_owned": false,
    "ownedName": ""
  },
  "ikeP1": {
    "objId": "0d144317-d7f6-424b-8efc-727310ec69db",
    "checkPointObjId": null,
    "domainsPreset": null,
    "domainId": "41e821a0-3720-11e3-aa6e-0800200c9fde",
    "ikeP1EncAlg": "AES_MINUS_256",
    "ikeP1UseAggressive": false,
    "ikeP1UseSharedSecret": false,
    "ikeP1UseSharedSecretForDaip": false,
    "ikeP1UseAggressiveForDaip": false,
    "ikeP1RekeyTime": 1440,
    "ikeP1DhGrp": "97aeb629-9aea-11d5-bd16-0090272ccb30",
    "ikeP1HashAlg": "SHA1",
    "folderPath": "072e125f-94bb-4876-9335-1f74cb53383a",
    "text": null,
    "folder": "072e125f-94bb-4876-9335-1f74cb53383a",
    "is_owned": false,
    "ownedName": ""
  },
  "manualMepRules": [],
  "excludeSrv": [],
  "permanentTunnelDownTrack": "LOG",
  "uid": "cbfed2ed-04d7-49b7-950c-a182d872fca8",
  "folder": {
    "uid": "072e125f-94bb-4876-9335-1f74cb53383a",
    "name": "Global Objects"
  },
  "domain": {
    "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
    "name": "SMC User"
  },
  "meta-info": {
    "metaOwned": false,
    "lockStateResponse": null,
    "validationState": "OK",
    "deletable": true,
    "renameable": true,
    "newObject": false,
    "lastModifytime": 1587732103365,
    "lastModifier": "admin",
    "creationTime": 1554686069698,
    "creator": "System"
  },
  "tags": [],
  "name": "MyIntranet",
  "icon": "VPNCommunities/Meshed",
  "comments": "",
  "display-name": "",
  "customFields": [],
  "_original_type": "MeshedCommunity"
}
"""

mock_show_generic_object_response_body_test_vpn_community = """
{
  "objectValidationState": null,
  "color": "BLACK",
  "automaticRimSatellites": false,
  "customerScriptSatellites": false,
  "supportWireMode": false,
  "customerScriptCenter": false,
  "routeRetPackets": false,
  "enableMep": false,
  "participantsDomains": [],
  "type": "intranet_community",
  "id": 6,
  "satelliteGateways": [],
  "disableNat": false,
  "mepMechanism": "SRC",
  "allowAllEncryptedTraffic": true,
  "topology": "STAR",
  "extGatewaysSharedSecret": [],
  "participantGateways": [
    "110d9aa4-7931-a048-9231-37e2e8c9d326"
  ],
  "disableNatOn": "BOTH",
  "permanentTunnelsDef": "NONE",
  "routeInjectionTrack": "LOG",
  "routeThroughCenter": "NONE",
  "selMechanism": "FIRST",
  "backupStickiness": false,
  "meshedInCenter": false,
  "permanentTunnelParticipantList": [],
  "cryptography": {
    "objId": "db9117b6-5a39-4a38-bc31-5a6b0307458b",
    "checkPointObjId": null,
    "domainsPreset": null,
    "domainId": "41e821a0-3720-11e3-aa6e-0800200c9fde",
    "cryptographyTypeSupport": "PREFER_IKEV2_SUPPORT_IKEV1",
    "cryptographyProfile": "CUSTOM_PROFILE",
    "folderPath": "072e125f-94bb-4876-9335-1f74cb53383a",
    "text": null,
    "folder": "072e125f-94bb-4876-9335-1f74cb53383a",
    "is_owned": false,
    "ownedName": ""
  },
  "automaticRim": true,
  "tunnelGranularity": "PER_SUBNET",
  "defaultMepRule": {
    "objId": "b265685b-4e26-418d-8b5f-12a99cf144db",
    "checkPointObjId": null,
    "domainsPreset": null,
    "domainId": "41e821a0-3720-11e3-aa6e-0800200c9fde",
    "icon": "Unknown",
    "source": [],
    "color": "BLACK",
    "name": "",
    "priority3": [],
    "priority2": [],
    "priority1": [],
    "displayName": "",
    "comments": "",
    "folderPath": "072e125f-94bb-4876-9335-1f74cb53383a",
    "text": null,
    "folder": "072e125f-94bb-4876-9335-1f74cb53383a",
    "is_owned": false,
    "ownedName": "",
    "tags": [],
    "customFields": [],
    "metaInfo": null,
    "features": [],
    "systemTags": []
  },
  "vpnMepResolverNotification": "LOG",
  "addRoutedDomain": null,
  "permanentTunnelList": [],
  "allowAllEncryptedTrafficOn": "BOTH",
  "supportWireModeRouting": false,
  "permanentTunnelUpTrack": "LOG",
  "permanentTunnelParticipants": "ALL_MEMBERS",
  "ikeP2": {
    "objId": "038b2a9f-4503-4ada-909a-32c18c603ebe",
    "checkPointObjId": null,
    "domainsPreset": null,
    "domainId": "41e821a0-3720-11e3-aa6e-0800200c9fde",
    "ikeP2UseSubnets": true,
    "ikeP2UseRekeyKbytes": false,
    "ikeP2EnableSupernetFromR8020": "BY_GLOBAL",
    "ikeP2RekeyTime": 3600,
    "ikeP2UsePfs": false,
    "ikeP2EncAlg": "DES_MINUS_40CP",
    "ikeP2RekeyKbytes": 50000,
    "ikeP2HashAlg": "SHA1",
    "ikeP2Ipcomp": "NONE",
    "ikeP2PfsDhGrp": "97aeb629-9aea-11d5-bd16-0090272ccb30",
    "folderPath": "072e125f-94bb-4876-9335-1f74cb53383a",
    "text": null,
    "folder": "072e125f-94bb-4876-9335-1f74cb53383a",
    "is_owned": false,
    "ownedName": ""
  },
  "ikeP1": {
    "objId": "c1a06020-9bda-4d67-bbc7-2b38831cef83",
    "checkPointObjId": null,
    "domainsPreset": null,
    "domainId": "41e821a0-3720-11e3-aa6e-0800200c9fde",
    "ikeP1EncAlg": "AES_MINUS_256",
    "ikeP1UseAggressive": false,
    "ikeP1UseSharedSecret": false,
    "ikeP1UseSharedSecretForDaip": false,
    "ikeP1UseAggressiveForDaip": false,
    "ikeP1RekeyTime": 1440,
    "ikeP1DhGrp": "86ee63a3-cb9a-478e-add4-857aff8a7ab3",
    "ikeP1HashAlg": "SHA256",
    "folderPath": "072e125f-94bb-4876-9335-1f74cb53383a",
    "text": null,
    "folder": "072e125f-94bb-4876-9335-1f74cb53383a",
    "is_owned": false,
    "ownedName": ""
  },
  "manualMepRules": [],
  "excludeSrv": [],
  "permanentTunnelDownTrack": "LOG",
  "uid": "cfbbb4d3-3ab1-4aff-844c-e31044490eec",
  "folder": {
    "uid": "072e125f-94bb-4876-9335-1f74cb53383a",
    "name": "Global Objects"
  },
  "domain": {
    "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
    "name": "SMC User"
  },
  "meta-info": {
    "metaOwned": false,
    "lockStateResponse": null,
    "validationState": "OK",
    "deletable": true,
    "renameable": true,
    "newObject": false,
    "lastModifytime": 1587755673093,
    "lastModifier": "admin",
    "creationTime": 1587476575689,
    "creator": "admin"
  },
  "tags": [],
  "name": "TestVPNCommunity",
  "icon": "VPNCommunities/Star",
  "comments": "",
  "display-name": "",
  "customFields": [],
  "_original_type": "StarCommunity"
}
"""

mock_show_generic_objects_response_body_remote_access = """
{
  "objects": [
    {
      "domainsPreset": null,
      "objectValidationState": null,
      "color": "BLACK",
      "routeInjectionTrack": "LOG",
      "automaticRimSatellites": false,
      "customerScriptSatellites": false,
      "selMechanism": "FIRST",
      "backupStickiness": false,
      "customerScriptCenter": false,
      "routeRetPackets": false,
      "enableMep": false,
      "participantsDomains": [],
      "type": "sr_community",
      "cryptography": {
        "objId": "a99cb8ee-eec2-417d-9136-b8079398c189",
        "checkPointObjId": null,
        "domainsPreset": null,
        "domainId": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "cryptographyTypeSupport": "IKE_V1_ONLY",
        "cryptographyProfile": "CUSTOM_PROFILE",
        "folderPath": "072e125f-94bb-4876-9335-1f74cb53383a",
        "text": null,
        "folder": "072e125f-94bb-4876-9335-1f74cb53383a",
        "is_owned": false,
        "ownedName": ""
      },
      "automaticRim": true,
      "id": 2,
      "defaultMepRule": {
        "objId": "8508354c-06e9-4ca4-8125-f70c761e312e",
        "checkPointObjId": null,
        "domainsPreset": null,
        "domainId": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "icon": "Unknown",
        "source": [],
        "color": "BLACK",
        "name": "",
        "priority3": [],
        "priority2": [],
        "priority1": [],
        "displayName": "",
        "comments": "",
        "folderPath": "072e125f-94bb-4876-9335-1f74cb53383a",
        "text": null,
        "folder": "072e125f-94bb-4876-9335-1f74cb53383a",
        "is_owned": false,
        "ownedName": "",
        "tags": [],
        "customFields": [],
        "metaInfo": null,
        "features": [],
        "systemTags": []
      },
      "vpnMepResolverNotification": "LOG",
      "vpnSrGwTunnelPriority": "S2S_TUNNEL",
      "addRoutedDomain": null,
      "mepMechanism": "SRC",
      "participantUsersGroups": [
        "97aeb36a-9aeb-11d5-bd16-0090272ccb30"
      ],
      "ikeP2": {
        "objId": "7cf82929-b017-4615-8366-140d4fe3c762",
        "checkPointObjId": null,
        "domainsPreset": null,
        "domainId": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "ikeP2UseSubnets": true,
        "ikeP2UseRekeyKbytes": false,
        "ikeP2EnableSupernetFromR8020": "BY_GLOBAL",
        "ikeP2RekeyTime": 3600,
        "ikeP2UsePfs": false,
        "ikeP2EncAlg": "AES_MINUS_128",
        "ikeP2RekeyKbytes": 50000,
        "ikeP2HashAlg": "SHA1",
        "ikeP2Ipcomp": "NONE",
        "ikeP2PfsDhGrp": "97aeb629-9aea-11d5-bd16-0090272ccb30",
        "folderPath": "072e125f-94bb-4876-9335-1f74cb53383a",
        "text": null,
        "folder": "072e125f-94bb-4876-9335-1f74cb53383a",
        "is_owned": false,
        "ownedName": ""
      },
      "ikeP1": {
        "objId": "74a75074-c448-4fe0-b831-16d659f855db",
        "checkPointObjId": null,
        "domainsPreset": null,
        "domainId": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "ikeP1EncAlg": "AES_MINUS_256",
        "ikeP1UseAggressive": false,
        "ikeP1UseSharedSecret": false,
        "ikeP1UseSharedSecretForDaip": false,
        "ikeP1UseAggressiveForDaip": false,
        "ikeP1RekeyTime": 1440,
        "ikeP1DhGrp": "97aeb629-9aea-11d5-bd16-0090272ccb30",
        "ikeP1HashAlg": "SHA1",
        "folderPath": "072e125f-94bb-4876-9335-1f74cb53383a",
        "text": null,
        "folder": "072e125f-94bb-4876-9335-1f74cb53383a",
        "is_owned": false,
        "ownedName": ""
      },
      "participantGateways": [],
      "manualMepRules": [],
      "uid": "9d4fc945-9016-4832-a2e0-48785e6e2768",
      "folder": {
        "uid": "072e125f-94bb-4876-9335-1f74cb53383a",
        "name": "Global Objects"
      },
      "domain": {
        "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "name": "SMC User"
      },
      "meta-info": {
        "metaOwned": false,
        "lockStateResponse": null,
        "validationState": "OK",
        "deletable": true,
        "renameable": true,
        "newObject": false,
        "lastModifytime": 1554686070772,
        "lastModifier": "System",
        "creationTime": 1554686070772,
        "creator": "System"
      },
      "tags": [],
      "name": "RemoteAccess",
      "icon": "VPNCommunities/Remote",
      "comments": "",
      "display-name": "",
      "customFields": [],
      "_original_type": "CpmiSrCommunity"
    }
  ],
  "from": 1,
  "to": 1,
  "total": 1
}
"""

mock_show_generic_object_response_body_dh_group_2 = """
{
  "domainsPreset": null,
  "objectValidationState": null,
  "color": "BLACK",
  "dhGroupNumber": 2,
  "modsize": 1024,
  "rootsize": 2,
  "root": {
    "objId": "00e5d483-2d4d-4549-8636-e6bd37f50725",
    "checkPointObjId": null,
    "domainsPreset": null,
    "domainId": "41e821a0-3720-11e3-aa6e-0800200c9fde",
    "value": "02",
    "folderPath": "a3a104fc-3987-4d22-9bf1-3fdbec0af39b",
    "text": null,
    "folder": "a3a104fc-3987-4d22-9bf1-3fdbec0af39b",
    "is_owned": false,
    "ownedName": ""
  },
  "mod": {
    "objId": "9daa1cf1-5249-4d76-ae0e-62a280494359",
    "checkPointObjId": null,
    "domainsPreset": null,
    "domainId": "41e821a0-3720-11e3-aa6e-0800200c9fde",
    "value": "ffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece65381ffffffffffffffff",
    "folderPath": "a3a104fc-3987-4d22-9bf1-3fdbec0af39b",
    "text": null,
    "folder": "a3a104fc-3987-4d22-9bf1-3fdbec0af39b",
    "is_owned": false,
    "ownedName": ""
  },
  "type": "IKE_DH_parameters",
  "privateKeyLength": 192,
  "uid": "97aeb629-9aea-11d5-bd16-0090272ccb30",
  "folder": {
    "uid": "a3a104fc-3987-4d22-9bf1-3fdbec0af39b",
    "name": "Global Objects"
  },
  "domain": {
    "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef",
    "name": "Check Point Data"
  },
  "meta-info": {
    "metaOwned": false,
    "lockStateResponse": null,
    "validationState": "OK",
    "deletable": false,
    "renameable": false,
    "newObject": false,
    "lastModifytime": 1554685729880,
    "lastModifier": "System",
    "creationTime": 1554685729880,
    "creator": "System"
  },
  "tags": [],
  "name": "Group 2 (1024 bit)",
  "icon": "General/globalsNa",
  "comments": "",
  "display-name": "",
  "customFields": [],
  "_original_type": "CpmiIkeDiffieHellmanParametersObject"
}
"""

mock_show_generic_object_response_body_dh_group_14 = """
{
  "domainsPreset": null,
  "objectValidationState": null,
  "color": "BLACK",
  "dhGroupNumber": 14,
  "modsize": 2048,
  "rootsize": 2,
  "root": {
    "objId": "fd86d4ab-ca0a-42e1-8aa5-25351dfde68e",
    "checkPointObjId": null,
    "domainsPreset": null,
    "domainId": "41e821a0-3720-11e3-aa6e-0800200c9fde",
    "value": "02",
    "folderPath": "a3a104fc-3987-4d22-9bf1-3fdbec0af39b",
    "text": null,
    "folder": "a3a104fc-3987-4d22-9bf1-3fdbec0af39b",
    "is_owned": false,
    "ownedName": ""
  },
  "mod": {
    "objId": "470115c7-6f7f-4a2f-b7c5-4d2845848e8b",
    "checkPointObjId": null,
    "domainsPreset": null,
    "domainId": "41e821a0-3720-11e3-aa6e-0800200c9fde",
    "value": "FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF6955817183995497CEA956AE515D2261898FA051015728E5A8AACAA68FFFFFFFFFFFFFFFF",
    "folderPath": "a3a104fc-3987-4d22-9bf1-3fdbec0af39b",
    "text": null,
    "folder": "a3a104fc-3987-4d22-9bf1-3fdbec0af39b",
    "is_owned": false,
    "ownedName": ""
  },
  "type": "IKE_DH_parameters",
  "privateKeyLength": 256,
  "uid": "86ee63a3-cb9a-478e-add4-857aff8a7ab3",
  "folder": {
    "uid": "a3a104fc-3987-4d22-9bf1-3fdbec0af39b",
    "name": "Global Objects"
  },
  "domain": {
    "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef",
    "name": "Check Point Data"
  },
  "meta-info": {
    "metaOwned": false,
    "lockStateResponse": null,
    "validationState": "OK",
    "deletable": false,
    "renameable": false,
    "newObject": false,
    "lastModifytime": 1554685729888,
    "lastModifier": "System",
    "creationTime": 1554685729888,
    "creator": "System"
  },
  "tags": [],
  "name": "Group 14 (2048 bit)",
  "icon": "General/globalsNa",
  "comments": "From RFC 3526",
  "display-name": "",
  "customFields": [],
  "_original_type": "CpmiIkeDiffieHellmanParametersObject"
}
"""

my_response_body = """
{
  "debug-variable": "This is 'my_response_body'",
  "field1": "value1",
  "field2": "value2"
}
"""
