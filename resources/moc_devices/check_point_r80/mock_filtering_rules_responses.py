
mock_response_headers = {
    "Connection": "Keep-Alive",
    "Content-Type": "application/json",
    "Date": "Wed, 25 Mar 2020 17:02:10 GMT",
    "Keep-Alive": "timeout=15, max=99",
    "Server": "CPWS",
    "Strict-Transport-Security": "max-age=31536000; includeSubDomains",
    "Transfer-Encoding": "",
    "X-Forwarded-Host-Port": "443",
    "X-Frame-Options": "",
    "X-UA-Compatible": "IE=EmulateIE8"
}


mock_show_access_layers_response_body = """
{
    "access-layers": [
        {
            "domain": {
                "domain-type": "domain",
                "name": "SMC User",
                "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde"
            },
            "name": "Development_Layer",
            "type": "access-layer",
            "uid": "b50d42a7-d1fe-4f8c-84a7-5d02f32c3010"
        },
        {
            "domain": {
                "domain-type": "domain",
                "name": "SMC User",
                "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde"
            },
            "name": "Firewall_policy Network",
            "type": "access-layer",
            "uid": "e34d73a0-07fb-4152-9ed5-743d52aac93f"
        },
        {
            "domain": {
                "domain-type": "domain",
                "name": "SMC User",
                "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde"
            },
            "name": "Network",
            "type": "access-layer",
            "uid": "c0264a80-1832-4fce-8a90-d0849dc4ba33"
        },
        {
            "domain": {
                "domain-type": "domain",
                "name": "SMC User",
                "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde"
            },
            "name": "Other_policy Network",
            "type": "access-layer",
            "uid": "c74513ea-b390-42b3-a4e3-3f54f469b948"
        },
        {
            "domain": {
                "domain-type": "domain",
                "name": "SMC User",
                "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde"
            },
            "name": "test_layer",
            "type": "access-layer",
            "uid": "65fe5ffd-4c1d-4b9c-af3c-28ecec802a32"
        }
    ],
    "from": 1,
    "to": 5,
    "total": 5
}"""


mock_show_access_rulebase_response_body = """
{
    "from": 1,
    "name": "",
    "objects-dictionary": [
        {
            "domain": {
                "domain-type": "data domain",
                "name": "Check Point Data",
                "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef"
            },
            "name": "Any",
            "type": "CpmiAnyObject",
            "uid": "97aeb369-9aea-11d5-bd16-0090272ccb30"
        },
        {
            "domain": {
                "domain-type": "data domain",
                "name": "Check Point Data",
                "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef"
            },
            "name": "Drop",
            "type": "RulebaseAction",
            "uid": "6c488338-8eec-4103-ad21-cd461ac2c473"
        },
        {
            "domain": {
                "domain-type": "data domain",
                "name": "Check Point Data",
                "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef"
            },
            "name": "None",
            "type": "Track",
            "uid": "29e53e3d-23bf-48fe-b6b1-d59bd88036f9"
        },
        {
            "domain": {
                "domain-type": "data domain",
                "name": "Check Point Data",
                "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef"
            },
            "name": "Policy Targets",
            "type": "Global",
            "uid": "6c488338-8eec-4103-ad21-cd461ac2c476"
        }
    ],
    "rulebase": [
        {
            "action": "6c488338-8eec-4103-ad21-cd461ac2c473",
            "action-settings": {},
            "comments": "",
            "content": [
                "97aeb369-9aea-11d5-bd16-0090272ccb30"
            ],
            "content-direction": "any",
            "content-negate": false,
            "custom-fields": {
                "field-1": "",
                "field-2": "",
                "field-3": ""
            },
            "destination": [
                "97aeb369-9aea-11d5-bd16-0090272ccb30"
            ],
            "destination-negate": false,
            "domain": {
                "domain-type": "domain",
                "name": "SMC User",
                "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde"
            },
            "enabled": true,
            "install-on": [
                "6c488338-8eec-4103-ad21-cd461ac2c476"
            ],
            "meta-info": {
                "creation-time": {
                    "iso-8601": "2019-11-08T16:33+0000",
                    "posix": 1573230796397
                },
                "creator": "admin",
                "last-modifier": "admin",
                "last-modify-time": {
                    "iso-8601": "2019-11-08T16:33+0000",
                    "posix": 1573230796397
                },
                "lock": "unlocked",
                "validation-state": "ok"
            },
            "name": "Cleanup rule",
            "rule-number": 1,
            "service": [
                "97aeb369-9aea-11d5-bd16-0090272ccb30"
            ],
            "service-negate": false,
            "source": [
                "97aeb369-9aea-11d5-bd16-0090272ccb30"
            ],
            "source-negate": false,
            "time": [
                "97aeb369-9aea-11d5-bd16-0090272ccb30"
            ],
            "track": {
                "accounting": false,
                "alert": "none",
                "enable-firewall-session": false,
                "per-connection": false,
                "per-session": false,
                "type": "29e53e3d-23bf-48fe-b6b1-d59bd88036f9"
            },
            "type": "access-rule",
            "uid": "1975c66e-9b8b-4e1d-84f5-9495c18ac44b",
            "vpn": [
                "97aeb369-9aea-11d5-bd16-0090272ccb30"
            ]
        }
    ],
    "to": 1,
    "total": 1,
    "uid": "b50d42a7-d1fe-4f8c-84a7-5d02f32c3010"
}"""
