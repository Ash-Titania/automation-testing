mock_good_login_response = """
{
  "uid": "bbae2a71-494c-429a-b992-c51ce7a17bec",
  "sid": "_OCWUs6yWIIKhF6dJMd0MBhItmvjv-HhPtlbdQBKBeI",
  "url": "https://10.200.8.177:443/web_api",
  "session-timeout": 600,
  "last-login-was-at": {
    "posix": 1589562162436,
    "iso-8601": "2020-05-15T18:02+0100"
  },
  "login-message": {
    "header": "ATTENTION",
    "message": "This system is to be used carefully, know what you are changing and what will be affected before publishing changes.\nIf you are unsure, ask."
  },
  "api-server-version": "1.5"
}
"""

mock_good_domain_login_response = """
{
  "uid": "55caafea-df8d-4119-961f-ed3e5767f7f6",
  "sid": "CwugBjDd-pUZGX0TBzo_vQT7wJ8MNbggY5xRcu-IYBk",
  "url": "https://10.200.8.177:443/web_api",
  "session-timeout": 600,
  "last-login-was-at": {
    "posix": 1592354045012,
    "iso-8601": "2020-06-17T01:34+0100"
  },
  "api-server-version": "1.5"
}
"""

mock_bad_login_response = """
{
  "code": "err_login_failed",
  "message": "Authentication to server failed."
}
"""

mock_logout_response = """
{
  "message": "OK"
}
"""

mock_bad_session_id = """
{
  "code": "generic_err_wrong_session_id",
  "message": "Wrong session id [56456456]. Session may be expired. Please check session id and resend the request."
}
"""

mock_show_login_message_response = """
{
  "domain": {
    "domain-type": "domain",
    "name": "SMC User",
    "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde"
  },
  "header": "ATTENTION",
  "message": "This system is to be used carefully, know what you are changing and what will be affected before publishing changes.\nIf you are unsure, ask.",
  "show-message": true,
  "type": "login-message",
  "warning": true
}
"""

mock_incorrect_domain_response = """
{
  "code": "err_inappropriate_domain_type",
  "message": "This command can work only on domains of type MDS. Cannot execute it in the current domain (current domain type is Domain)."
}
"""