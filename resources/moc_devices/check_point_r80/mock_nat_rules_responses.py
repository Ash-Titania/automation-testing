mock_show_nat_rules_response_body = """
{
  "uid": "1bb6c2ad-dc96-4858-972c-8759441070dc",
  "rulebase": [
    {
      "uid": "a36b0c4c-c3ea-4748-bbb2-04c354e4f782",
      "name": "Automatic Generated Rules : Machine Static NAT",
      "type": "nat-section",
      "rulebase": []
    },
    {
      "uid": "febdf845-b502-4899-9fc2-5ed176746d46",
      "name": "Automatic Generated Rules : Machine Hide NAT",
      "type": "nat-section",
      "rulebase": []
    },
    {
      "uid": "1572533b-243c-4e57-9cff-d361e8ac9800",
      "name": "Automatic Generated Rules : Address Range Static NAT",
      "type": "nat-section",
      "rulebase": []
    },
    {
      "uid": "5c70bb4b-0ea0-41f5-b5cc-3c65b0503270",
      "name": "Automatic Generated Rules : Network Static NAT",
      "type": "nat-section",
      "rulebase": []
    },
    {
      "uid": "eb29221e-c530-47e3-9b12-1f4e528abef3",
      "name": "Automatic Generated Rules : Address Range Hide NAT",
      "type": "nat-section",
      "rulebase": []
    },
    {
      "uid": "aeff8732-e5ed-4f5f-bcda-02474298fed9",
      "name": "Automatic Generated Rules : Network Hide NAT",
      "type": "nat-section",
      "from": 1,
      "to": 4,
      "rulebase": [
        {
          "uid": "4be0fe24-8de6-44e8-8d9b-9fb03b955dff",
          "type": "nat-rule",
          "domain": {
            "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
            "name": "SMC User",
            "domain-type": "domain"
          },
          "rule-number": 1,
          "method": "hide",
          "auto-generated": true,
          "original-destination": "a325d073-7fb7-43f9-99d2-177e82083363",
          "translated-destination": "85c0f50f-6d8a-4528-88ab-5fb11d8fe16c",
          "original-source": "a325d073-7fb7-43f9-99d2-177e82083363",
          "translated-source": "85c0f50f-6d8a-4528-88ab-5fb11d8fe16c",
          "original-service": "97aeb369-9aea-11d5-bd16-0090272ccb30",
          "translated-service": "85c0f50f-6d8a-4528-88ab-5fb11d8fe16c",
          "meta-info": {
            "lock": "unlocked",
            "validation-state": "ok",
            "last-modify-time": {
              "posix": 1586270708167,
              "iso-8601": "2020-04-07T15:45+0100"
            },
            "last-modifier": "admin",
            "creation-time": {
              "posix": 1586270708167,
              "iso-8601": "2020-04-07T15:45+0100"
            },
            "creator": "admin"
          },
          "comments": "",
          "enabled": true,
          "install-on": [
            "97aeb368-9aea-11d5-bd16-0090272ccb30"
          ]
        },
        {
          "uid": "5d64ff93-e744-4f4e-9110-76a178fe5e7f",
          "type": "nat-rule",
          "domain": {
            "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
            "name": "SMC User",
            "domain-type": "domain"
          },
          "rule-number": 2,
          "method": "hide",
          "auto-generated": true,
          "original-destination": "97aeb369-9aea-11d5-bd16-0090272ccb30",
          "translated-destination": "85c0f50f-6d8a-4528-88ab-5fb11d8fe16c",
          "original-source": "a325d073-7fb7-43f9-99d2-177e82083363",
          "translated-source": "a325d073-7fb7-43f9-99d2-177e82083363",
          "original-service": "97aeb369-9aea-11d5-bd16-0090272ccb30",
          "translated-service": "85c0f50f-6d8a-4528-88ab-5fb11d8fe16c",
          "meta-info": {
            "lock": "unlocked",
            "validation-state": "ok",
            "last-modify-time": {
              "posix": 1586270708167,
              "iso-8601": "2020-04-07T15:45+0100"
            },
            "last-modifier": "admin",
            "creation-time": {
              "posix": 1586270708167,
              "iso-8601": "2020-04-07T15:45+0100"
            },
            "creator": "admin"
          },
          "comments": "",
          "enabled": true,
          "install-on": [
            "97aeb368-9aea-11d5-bd16-0090272ccb30"
          ]
        },
        {
          "uid": "6104aab6-392a-4202-93a0-68f5a02f9734",
          "type": "nat-rule",
          "domain": {
            "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
            "name": "SMC User",
            "domain-type": "domain"
          },
          "rule-number": 3,
          "method": "hide",
          "auto-generated": true,
          "original-destination": "0cbdc1c4-b05d-414a-8d6f-92fdfcb53dcc",
          "translated-destination": "85c0f50f-6d8a-4528-88ab-5fb11d8fe16c",
          "original-source": "0cbdc1c4-b05d-414a-8d6f-92fdfcb53dcc",
          "translated-source": "85c0f50f-6d8a-4528-88ab-5fb11d8fe16c",
          "original-service": "97aeb369-9aea-11d5-bd16-0090272ccb30",
          "translated-service": "85c0f50f-6d8a-4528-88ab-5fb11d8fe16c",
          "meta-info": {
            "lock": "unlocked",
            "validation-state": "ok",
            "last-modify-time": {
              "posix": 1554693313596,
              "iso-8601": "2019-04-08T04:15+0100"
            },
            "last-modifier": "System",
            "creation-time": {
              "posix": 1554693313596,
              "iso-8601": "2019-04-08T04:15+0100"
            },
            "creator": "System"
          },
          "comments": "",
          "enabled": true,
          "install-on": [
            "97aeb368-9aea-11d5-bd16-0090272ccb30"
          ]
        },
        {
          "uid": "c8f8d462-9029-423c-81fa-3ce80adce70f",
          "type": "nat-rule",
          "domain": {
            "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
            "name": "SMC User",
            "domain-type": "domain"
          },
          "rule-number": 4,
          "method": "hide",
          "auto-generated": true,
          "original-destination": "97aeb369-9aea-11d5-bd16-0090272ccb30",
          "translated-destination": "85c0f50f-6d8a-4528-88ab-5fb11d8fe16c",
          "original-source": "0cbdc1c4-b05d-414a-8d6f-92fdfcb53dcc",
          "translated-source": "0cbdc1c4-b05d-414a-8d6f-92fdfcb53dcc",
          "original-service": "97aeb369-9aea-11d5-bd16-0090272ccb30",
          "translated-service": "85c0f50f-6d8a-4528-88ab-5fb11d8fe16c",
          "meta-info": {
            "lock": "unlocked",
            "validation-state": "ok",
            "last-modify-time": {
              "posix": 1554693313614,
              "iso-8601": "2019-04-08T04:15+0100"
            },
            "last-modifier": "System",
            "creation-time": {
              "posix": 1554693313614,
              "iso-8601": "2019-04-08T04:15+0100"
            },
            "creator": "System"
          },
          "comments": "",
          "enabled": true,
          "install-on": [
            "97aeb368-9aea-11d5-bd16-0090272ccb30"
          ]
        }
      ]
    },
    {
      "uid": "c9c3423f-c3a5-4355-a695-c5a4e6160605",
      "name": "Manual Lower Rules",
      "type": "nat-section",
      "rulebase": []
    }
  ],
  "objects-dictionary": [
    {
      "uid": "97aeb368-9aea-11d5-bd16-0090272ccb30",
      "name": "All",
      "type": "CpmiAnyObject",
      "domain": {
        "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef",
        "name": "Check Point Data",
        "domain-type": "data domain"
      },
      "color": "black",
      "meta-info": {
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1554685737993,
          "iso-8601": "2019-04-08T02:08+0100"
        },
        "last-modifier": "System",
        "creation-time": {
          "posix": 1554685737993,
          "iso-8601": "2019-04-08T02:08+0100"
        },
        "creator": "System"
      },
      "tags": [],
      "icon": "General/globalsAny",
      "comments": null,
      "display-name": "",
      "customFields": null
    },
    {
      "uid": "97aeb369-9aea-11d5-bd16-0090272ccb30",
      "name": "Any",
      "type": "CpmiAnyObject",
      "domain": {
        "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef",
        "name": "Check Point Data",
        "domain-type": "data domain"
      },
      "color": "black",
      "meta-info": {
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1554685737997,
          "iso-8601": "2019-04-08T02:08+0100"
        },
        "last-modifier": "System",
        "creation-time": {
          "posix": 1554685737997,
          "iso-8601": "2019-04-08T02:08+0100"
        },
        "creator": "System"
      },
      "tags": [],
      "icon": "General/globalsAny",
      "comments": null,
      "display-name": "",
      "customFields": null
    },
    {
      "uid": "a325d073-7fb7-43f9-99d2-177e82083363",
      "name": "Build Servers",
      "type": "network",
      "domain": {
        "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "name": "SMC User",
        "domain-type": "domain"
      },
      "broadcast": "allow",
      "subnet4": "10.200.2.0",
      "mask-length4": 24,
      "subnet-mask": "255.255.255.0",
      "nat-settings": {
        "auto-rule": true,
        "hide-behind": "gateway",
        "install-on": "All",
        "method": "hide"
      },
      "comments": "",
      "color": "black",
      "icon": "NetworkObjects/network",
      "tags": [],
      "meta-info": {
        "lock": "unlocked",
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1586270708167,
          "iso-8601": "2020-04-07T15:45+0100"
        },
        "last-modifier": "admin",
        "creation-time": {
          "posix": 1586270708167,
          "iso-8601": "2020-04-07T15:45+0100"
        },
        "creator": "admin"
      },
      "read-only": false
    },
    {
      "uid": "0cbdc1c4-b05d-414a-8d6f-92fdfcb53dcc",
      "name": "CP_default_Office_Mode_addresses_pool",
      "type": "network",
      "domain": {
        "uid": "41e821a0-3720-11e3-aa6e-0800200c9fde",
        "name": "SMC User",
        "domain-type": "domain"
      },
      "broadcast": "allow",
      "subnet4": "172.16.10.0",
      "mask-length4": 24,
      "subnet-mask": "255.255.255.0",
      "nat-settings": {
        "auto-rule": true,
        "hide-behind": "gateway",
        "install-on": "All",
        "method": "hide"
      },
      "comments": "Used as a default for Office Mode. If deleted, it must be specified for each gateway individually (in the VPN Clients page Advanced section)",
      "color": "black",
      "icon": "NetworkObjects/network",
      "tags": [],
      "meta-info": {
        "lock": "unlocked",
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1554686096471,
          "iso-8601": "2019-04-08T02:14+0100"
        },
        "last-modifier": "System",
        "creation-time": {
          "posix": 1554686096471,
          "iso-8601": "2019-04-08T02:14+0100"
        },
        "creator": "System"
      },
      "read-only": false
    },
    {
      "uid": "85c0f50f-6d8a-4528-88ab-5fb11d8fe16c",
      "name": "Original",
      "type": "Global",
      "domain": {
        "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef",
        "name": "Check Point Data",
        "domain-type": "data domain"
      },
      "color": "none",
      "meta-info": {
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1554685770210,
          "iso-8601": "2019-04-08T02:09+0100"
        },
        "last-modifier": "System",
        "creation-time": {
          "posix": 1554685770210,
          "iso-8601": "2019-04-08T02:09+0100"
        },
        "creator": "System"
      },
      "tags": [],
      "icon": "NAT/NatOriginal",
      "comments": "This object was created automatically",
      "customFields": null
    },
    {
      "uid": "6c488338-8eec-4103-ad21-cd461ac2c476",
      "name": "Policy Targets",
      "type": "Global",
      "domain": {
        "uid": "a0bbbc99-adef-4ef8-bb6d-defdefdefdef",
        "name": "Check Point Data",
        "domain-type": "data domain"
      },
      "color": "none",
      "meta-info": {
        "validation-state": "ok",
        "last-modify-time": {
          "posix": 1554685769366,
          "iso-8601": "2019-04-08T02:09+0100"
        },
        "last-modifier": "System",
        "creation-time": {
          "posix": 1554685769366,
          "iso-8601": "2019-04-08T02:09+0100"
        },
        "creator": "System"
      },
      "tags": [],
      "icon": "General/globalsAny",
      "comments": "The policy target gateways",
      "customFields": null
    }
  ],
  "from": 1,
  "to": 4,
  "total": 4
}"""
