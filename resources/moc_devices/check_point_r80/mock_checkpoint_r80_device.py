import json

from flask import Flask, request, make_response
from resources.moc_devices.check_point_r80.mock_filtering_rules_responses import *
from resources.moc_devices.check_point_r80.mock_network_objects_responses import *
from resources.moc_devices.check_point_r80.mock_gateways_responses import *
from resources.moc_devices.check_point_r80.mock_services_responses import *
from resources.moc_devices.check_point_r80.mock_nat_rules_responses import *
from resources.moc_devices.check_point_r80.mock_vpn_responses import *
from resources.moc_devices.check_point_r80.mock_session_responses import *
from resources.moc_devices.check_point_r80.mock_administrators_response import *


mockCheckPointR80Api = Flask(__name__)
"""
Flask app to act as a mock of a Check Point R80 device for testing
"""

USER_NAME = None
PASSWORD = None
LOGGED_INTO_SYSTEM_DATA = False

@mockCheckPointR80Api.route('/web_api/login', methods=['POST', '{}POST'])
def login():
    """
    Sends hard coded response with session ID
    """
    global LOGGED_INTO_SYSTEM_DATA

    req = json.loads(request.data)
    expected_req = {
        "user": USER_NAME,
        "password": PASSWORD
    }

    expected_req_domain = {
        "user": USER_NAME,
        "password": PASSWORD,
        "domain": "System Data"
    }

    if req == expected_req:
        LOGGED_INTO_SYSTEM_DATA = False
        body = json.loads(mock_good_login_response)
        return make_mock_api_response(json.dumps(body))
    elif req == expected_req_domain:
        LOGGED_INTO_SYSTEM_DATA = True
        body = json.loads(mock_good_domain_login_response)
        return make_mock_api_response(json.dumps(body))

    return make_mock_api_response(mock_bad_login_response)

@mockCheckPointR80Api.route('/web_api/logout', methods=['POST', '{}POST'])
def logout():
    """
    Sends hard coded response and resets session ID
    """
    return make_mock_api_response(mock_logout_response)

@mockCheckPointR80Api.route('/web_api/show-login-message', methods=['POST'])
def show_login_message():
    """
    Sends a hard coded response with configured login message
    """
    if matches_empty_body(json.loads(request.data)):
        return make_mock_api_response(mock_show_login_message_response)

    return make_bad_request_response()

@mockCheckPointR80Api.route('/web_api/show-gateways-and-servers', methods=['POST'])
def show_gateways_and_servers():
    """
    Sends a hard coded response with configured gateways and clusters
    """
    return handle_500_limit_full_details_level_response(json.loads(request.data), mock_show_gateways_and_servers_response)

@mockCheckPointR80Api.route('/web_api/show-administrators', methods=['POST'])
def show_administrators():
    """
    Sends a hard coded response with configured users
    """
    global LOGGED_INTO_SYSTEM_DATA
    
    if LOGGED_INTO_SYSTEM_DATA:
        return make_mock_api_response(mock_show_administrators_response)
    else:
        return make_mock_api_response(mock_incorrect_domain_response)

@mockCheckPointR80Api.route('/web_api/show-access-layers', methods=['POST'])
def show_access_layers():
    """
    sends hard-coded response when the a post is made to the show-access-layers endpoint;
    returns an empty response if the request body does not match a hard-coded expected body
    :return: mock rulebase response or an empty response if the request body has the wrong data
    """
    return handle_500_limit_standard_details_level_response(json.loads(request.data), mock_show_access_layers_response_body)


@mockCheckPointR80Api.route('/web_api/show-access-rulebase', methods=['POST'])
def show_access_rulebase():
    """
    sends hard-coded response when the post is made to the show-access-rulebase endpoint;
    the same rulebase data is sent back whatever name is provided, but the 'name' field is set to
    the value sent in the request
    Returns an empty response if the body does not match a hard-coded expected body or if the rulebase name
    is not in the hard-coded list of names
    :return: mock rulebase response or an empty response if the request body has the wrong data
    """
    request_body_json = json.loads(request.data)
    if not matches_expected_show_access_rulebase_body(request_body_json):
        return make_bad_request_response()
    rulebase_name = request_body_json["name"]
    if rulebase_name.lower() not in [name.lower() for name in known_access_layers]:
        return make_bad_request_response()
    body = json.loads(mock_show_access_rulebase_response_body)
    body["name"] = rulebase_name
    return make_mock_api_response(json.dumps(body))


@mockCheckPointR80Api.route('/web_api/show-nat-rulebase', methods=['POST'])
def show_nat_rulebase():
    return handle_500_limit_full_details_level_response(json.loads(request.data), mock_show_nat_rules_response_body)


@mockCheckPointR80Api.route('/web_api/show-hosts', methods=['POST', '{"uid":"1e8e7ba9-df91-cc44-b001-bb86bac79463"}POST'])
def show_hosts():
    print("actual method - " + str(request.method))
    return handle_500_limit_standard_details_level_response(json.loads(request.data), mock_show_hosts_response_body)


@mockCheckPointR80Api.route('/web_api/show-networks', methods=['POST'])
def show_networks():
    return handle_500_limit_standard_details_level_response(json.loads(request.data), mock_show_networks_response_body)


@mockCheckPointR80Api.route('/web_api/show-address-ranges', methods=['POST'])
def show_address_ranges():
    return handle_500_limit_standard_details_level_response(json.loads(request.data), mock_show_address_ranges_response_body)


@mockCheckPointR80Api.route('/web_api/show-multicast-address-ranges', methods=['POST'])
def show_multicast_address_ranges():
    return handle_500_limit_standard_details_level_response(json.loads(request.data), mock_show_multicast_address_ranges_response_body)


@mockCheckPointR80Api.route('/web_api/show-simple-gateways', methods=['POST'])
def show_simple_gateways():
    """
    sends hard-coded response when the post is made to the endpoint;
    :return: mock simple gateways response
    """
    if matches_limit_500_details_level_full_body(json.loads(request.data)):
        return make_mock_api_response(mock_show_simple_gateways_response_body)
    elif matches_limit_500_details_level_standard_body(json.loads(request.data)):
        return make_mock_api_response(mock_show_simple_gateways_details_level_standard_response_body)
    else:
        return make_bad_request_response()

@mockCheckPointR80Api.route('/web_api/show-security-zones', methods=['POST'])
def show_security_zones():
    return handle_500_limit_standard_details_level_response(json.loads(request.data), mock_show_security_zones_response_body)


@mockCheckPointR80Api.route('/web_api/show-dynamic-objects', methods=['POST'])
def show_show_dynamic_objects():
    return handle_500_limit_standard_details_level_response(json.loads(request.data), mock_show_dynamic_objects_response_body)


@mockCheckPointR80Api.route('/web_api/show-groups', methods=['POST'])
def show_groups():
    return handle_500_limit_standard_details_level_response(json.loads(request.data), mock_show_groups_response_body)


@mockCheckPointR80Api.route('/web_api/show-group', methods=['POST'])
def show_group():
    if not matches_expected_show_group_body(json.loads(request.data)):
        return make_bad_request_response()
    return make_mock_api_response(mock_show_group_response_body)


@mockCheckPointR80Api.route('/web_api/show-services-tcp', methods=['POST'])
def show_show_services_tcp():
    return handle_500_limit_full_details_level_response(json.loads(request.data), mock_show_services_tcp_response_body)


@mockCheckPointR80Api.route('/web_api/show-services-udp', methods=['POST'])
def show_show_services_udp():
    return handle_500_limit_full_details_level_response(json.loads(request.data), mock_show_services_udp_response_body)


@mockCheckPointR80Api.route('/web_api/show-services-icmp', methods=['POST'])
def show_show_services_icmp():
    return handle_500_limit_full_details_level_response(json.loads(request.data), mock_show_services_icmp_response_body)


@mockCheckPointR80Api.route('/web_api/show-services-icmp6', methods=['POST'])
def show_show_services_icmp6():
    return handle_500_limit_full_details_level_response(json.loads(request.data), mock_show_services_icmp6_response_body)


@mockCheckPointR80Api.route('/web_api/show-services-sctp', methods=['POST'])
def show_show_services_sctp():
    return handle_500_limit_full_details_level_response(json.loads(request.data), mock_show_services_sctp_response_body)


@mockCheckPointR80Api.route('/web_api/show-services-dce-rpc', methods=['POST'])
def show_show_services_dce_rpc():
    return handle_500_limit_full_details_level_response(json.loads(request.data), mock_show_services_dce_rpc_response_body)


@mockCheckPointR80Api.route('/web_api/show-services-rpc', methods=['POST'])
def show_show_services_rpc():
    return handle_500_limit_full_details_level_response(json.loads(request.data), mock_show_services_rpc_response_body)


@mockCheckPointR80Api.route('/web_api/show-service-groups', methods=['POST'])
def show_show_service_groups():
    return handle_500_limit_standard_details_level_response(json.loads(request.data), mock_show_service_groups_response_body)


@mockCheckPointR80Api.route('/web_api/show-service-group', methods=['POST'])
def show_show_service_group():
    return make_mock_api_response(mock_show_service_group_response_body)

@mockCheckPointR80Api.route('/web_api/show-simple-gateway', methods=['POST'])
def show_simple_gateway():
    """
    sends hard-coded response when the post is made to the endpoint;
    :return: mock simple gateway response
    """
    if not matches_expected_show_simple_gateway_body(json.loads(request.data)):
        return make_bad_request_response()
    return make_mock_api_response(mock_show_simple_gateway_response_body)

@mockCheckPointR80Api.route('/web_api/show-vpn-communities-meshed', methods=['POST'])
def show_vpn_communities_meshed():
    return handle_500_limit_uid_details_level_response(json.loads(request.data), mock_show_vpn_communities_meshed_response_body)

@mockCheckPointR80Api.route('/web_api/show-vpn-communities-star', methods=['POST'])
def show_vpn_communities_star():
    return handle_500_limit_uid_details_level_response(json.loads(request.data), mock_show_vpn_communities_star_response_body)

@mockCheckPointR80Api.route('/web_api/show-generic-object', methods=['POST'])
def show_generic_object():
    request_body_json = json.loads(request.data)
    if not matches_expected_show_generic_object_body(request_body_json):
        return make_bad_request_response()
    my_intranet_uid = "cbfed2ed-04d7-49b7-950c-a182d872fca8"
    test_vpn_community_uid = "cfbbb4d3-3ab1-4aff-844c-e31044490eec"
    dh_group_2_uid = "97aeb629-9aea-11d5-bd16-0090272ccb30"
    dh_group_14_uid = "86ee63a3-cb9a-478e-add4-857aff8a7ab3"
    requested_uid = request_body_json["uid"]
    if requested_uid == my_intranet_uid:
        return make_mock_api_response(mock_show_generic_object_response_body_my_intranet)
    elif requested_uid == test_vpn_community_uid:
        return make_mock_api_response(mock_show_generic_object_response_body_test_vpn_community)
    elif requested_uid == dh_group_2_uid:
        return make_mock_api_response(mock_show_generic_object_response_body_dh_group_2)
    elif requested_uid == dh_group_14_uid:
        return make_mock_api_response(mock_show_generic_object_response_body_dh_group_14)
    else:
        return make_bad_request_response()

@mockCheckPointR80Api.route('/web_api/show-generic-objects', methods=['POST'])
def show_generic_objects():
    request_body_json = json.loads(request.data)
    if not matches_expected_show_generic_objects_body(request_body_json):
        return make_bad_request_response()
    return make_mock_api_response(mock_show_generic_objects_response_body_remote_access)

def handle_500_limit_uid_details_level_response(request_data_json, response_body):
    if not matches_limit_500_details_level_uid_body(request_data_json):
        return make_bad_request_response()
    return make_mock_api_response(response_body)

def handle_500_limit_standard_details_level_response(request_data_json, response_body):
    if not matches_limit_500_details_level_standard_body(request_data_json):
        return make_bad_request_response()
    return make_mock_api_response(response_body)


def handle_500_limit_full_details_level_response(request_data_json, response_body):
    if not matches_limit_500_details_level_full_body(request_data_json):
        return make_bad_request_response()
    return make_mock_api_response(response_body)

def make_mock_api_response(response_body):
    response = make_response("")
    response.headers = mock_response_headers
    response.data = response_body
    return response

def make_bad_request_response():
    """
    makes an empty 400 "bad request" response
    :return: empty 400 response
    """
    response_body = {
        "message": "Invalid request"
    }
    bad_request_response = make_response(json.dumps(response_body), 400)
    bad_request_response.headers = {"Content-Type": "application/json"}
    return bad_request_response


known_access_layers = ["Development_layer", "Firewall_policy Network", "Network", "Other_policy Network", "test_layer"]


def matches_limit_500_details_level_uid_body(request_body_object):
    """
    Checks the request body matches limit 500, offset 0, details-level uid
    :param request_body_object: python object representing the request body
    :return: true if the body matches, false if the body does not match
    """
    expected_body = {
        "limit": 500,
        "offset": 0,
        "details-level": "uid"
    }
    return request_body_object == expected_body


def matches_limit_500_details_level_standard_body(request_body_object):
    """
    Checks the request body matches that expected for the show-access-layers command
    :param request_body_object: python object representing the request body
    :return: true if the body matches, false if the body does not match
    """
    expected_body = {
        "limit": 500,
        "offset": 0,
        "details-level": "standard"
    }
    return request_body_object == expected_body


def matches_expected_show_generic_object_body(request_body_object):
    """
    Checks the request body matches that expected for the show-generic-object command;
    The match is made ignorin the value of the "uid" parameter because this value is changed when different objects
    are required
    :param request_body_object: python object representing the request body
    :return: true if the body matches, false if the body does not match
    """
    expected_body_stub = {
        "details-level": "full"
    }
    request_body_object_copy = request_body_object.copy()
    request_body_object_copy.pop("uid")
    return request_body_object_copy == expected_body_stub

def matches_expected_show_generic_objects_body(request_body_object):
    """
    Checks the request body matches that expected for the show-generic-objects command;
    The match is made ignorin the value of the "uid" parameter because this value is changed when different objects
    are required
    :param request_body_object: python object representing the request body
    :return: true if the body matches, false if the body does not match
    """
    expected_body = {
        "details-level": "full",
        "name": "RemoteAccess"
    }
    return request_body_object == expected_body

def matches_expected_show_access_rulebase_body(request_body_object):
    """
    Checks the request body matches that expected for the show-access-rulebase command;
    The match is made ignoring the "name" parameter because this value is changed when different rulebases
    are queried
    :param request_body_object: python object representing the request body
    :return: true if the body matches, false if the body does not match
    """
    expected_body_stub = {
      "offset": 0,
      "limit": 500,
      "details-level": "standard",
      "use-object-dictionary": True
    }
    request_body_object_copy = request_body_object.copy()
    request_body_object_copy.pop("name")
    return request_body_object_copy == expected_body_stub


def matches_limit_500_details_level_full_body(request_body_object):
    """
    Checks the request body matches one of a hardcoded list of expected bodies
    :param request_body_object: python object representing the request body
    :return: true if the body matches one of the hardcoded list, false if the body does not match
    """
    allowed_bodies = [
        {
            "details-level": "full"
        },
        {
            "details-level": "full",
            "limit": 500,
            "offset": 0
        },
        {
          "offset" : 0,
          "limit" : 500,
          "details-level" : "full",
          "use-object-dictionary" : "true",
          "package" : "standard"
        }
    ]
    return request_body_object in allowed_bodies


def matches_expected_show_simple_gateway_body(request_body_object):
    """
    Checks the request body matches that expected for the command
    :param request_body_object: python object representing the request body
    :return: true if the body matches, false if the body does not match
    """
    expected_body = {
        "name": "gw-aeea33"
    }
    return request_body_object == expected_body

def matches_expected_show_group_body(request_body_object):
    """
    Checks the request body matches that expected for the show-group command;
    :param request_body_object: python object representing the request body
    :return: true if the body matches, false if the body does not match
    """
    expected_body = {
        "details-level": "standard",
        "name": "Titania Networks"
    }
    return request_body_object == expected_body

def matches_empty_body(request_body_object):
    """
    Checks the request body matches the expected empty Json body
    """
    expected_body = {}
    return request_body_object == expected_body

def start_server(ip, port, user, password):
    global USER_NAME
    global PASSWORD

    USER_NAME = user
    PASSWORD = password

    mockCheckPointR80Api.run(host=ip, port=port, ssl_context='adhoc')
