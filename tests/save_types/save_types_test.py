import os
import tempfile
import unittest

from utility.config_paths import get_cisco_demo_path
from utility.nipper2_interface import NipperInterface
from utility.nipper2_interface import NipperReportInterface
from utility.nipper2_interface import NipperProfileInterface
from utility.temporary_license import TempLicense


class TestSaveTypes(unittest.TestCase):
    def setUp(self):
        self.nipper = NipperInterface()
        self.license = TempLicense()
        self._nipper_report = NipperReportInterface(self.nipper)
        self._temp_dir_context = tempfile.TemporaryDirectory()
        self._temp_dir = self._temp_dir_context.name
        NipperProfileInterface(self.nipper).reset_default_settings()
        self._demo_file = get_cisco_demo_path()

    def tearDown(self):
        self._temp_dir_context.cleanup()

    def test_save_types(self):
        """ Save a nipper report as every save type, and check the resultant files """

        self._report_location = f"{self._temp_dir}/report"
        if os.path.exists(self._demo_file):
            self._nipper_report.generate_report(self._demo_file, self._report_location, '--all-formats')
            # (file_extension, required_data, file_size)
            test_cases = [
                ('.txt', ['CiscoIOS15 | IOS 15.0', 'Nipper', 'Users With Dictionary-Based Passwords'], 200000),
                ('.csv', ['CiscoIOS15', 'Users With Dictionary-Based Passwords'], 3000),
                # This value can possibly fail if a new version of serialisation is used (e.g. updating boost)
                ('.fbl', [], 55000),
                ('.html', ['CiscoIOS15', 'Audit Report - Nipper', 'Users With Dictionary-Based Passwords'],
                 1500000),
                ('.tex', ['CiscoIOS15', 'Nipper', 'Users With Dictionary-Based Passwords'], 250000),
                ('.xml', ['CiscoIOS15', '<author>Nipper</author>', 'Users With Dictionary-Based Passwords'],
                 350000),
                ('-CiscoIOS15.passwd', ['admin:$1$spr6$R9GYbviV7MFKSwoAsb0MD0'], 500),
                ('-sum.html', ['content="Nipper"', 'CiscoIOS15'], 250000)
            ]
            for save_extension, required_data, file_size in test_cases:
                with self.subTest(f"{save_extension} save type"):
                    report_name = self._report_location + save_extension
                    self.assertTrue(os.path.exists(report_name), save_extension + ' file should exist')
                    if required_data:
                        file = open(report_name, "r")
                        try:
                            report_contents = file.read()
                            for data in required_data:
                                self.assertIn(data, report_contents, data + ' was not found in ' + save_extension +
                                              ' file')
                        finally:
                            file.close()

                    self.assertGreater(os.stat(report_name).st_size, file_size, save_extension +
                                       ' file should be more than ' + str(file_size) + ' bytes')
        else:
            self.skipTest('Could not access demo file.')
