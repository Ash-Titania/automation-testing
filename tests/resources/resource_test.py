"""Tests for adding Resource file successfully with pre-conditions they are not in the resource list
    and with a subset test to remove the same resource file successfully"""

import unittest
from utility.nipper2_interface.nipper_resource_interface import NipperResourceInterface
from resources.resource_data.resource_file_data import *


class TestResource(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestResource, self).__init__(*args, **kwargs)
        self.add_resource_success = "Resource added successfully."
        self.add_resource_fail = "Error: Invalid file format. Please ensure you are providing a resource "
        self.nipper_resource = NipperResourceInterface()

    def test_resource_addition(self):
        """Subtests the adding of each resource type"""

        # Test action, resource name, resource category, resource file, expected output
        test_cases = [
            ("Adding STIG", STIG_NAME, STIG_CATEGORY, STIG_FILE, self.add_resource_success),
            ("Adding Word_list", WORD_LIST_NAME, WORD_LIST_CATEGORY, WORD_LIST_FILE, self.add_resource_success),
            ("Adding CPE", CPE_NAME, CPE_CATEGORY, CPE_FILE, self.add_resource_success),
            ("Adding NVD", NVD_NAME, NVD_CATEGORY, NVD_FILE, self.add_resource_success),
            ("Adding CSS", CSS_NAME, CSS_CATEGORY, CSS_FILE, self.add_resource_success),
            ("Adding Bad_input_file", BAD_FILE_NAME, BAD_FILE_CATEGORY, BAD_FILE, self.add_resource_fail)

        ]

        for test_action, resource_name, resource_category, resource_file, expected_output in test_cases:
            with self.subTest(f"'{test_action} resource' expects '{expected_output}'"):
                output = self.nipper_resource.resource_add(resource_name, resource_category, resource_file).stdout
                try:
                    self.assertIn(expected_output, output, f"'{expected_output}' must be in output")
                finally:
                    self.nipper_resource.resource_cleanup(resource_name)
