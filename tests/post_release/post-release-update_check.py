import unittest
from utility.nipper2_interface import nipper_interface


class TestPostReleaseTest(unittest.TestCase):

    @unittest.skip('post-release-test')
    def test_update_check_post_release(self):

        output = nipper_interface.NipperInterface().run_nipper("--update-check")

        lines = output.stdout.splitlines()
        version_info_lines = []
        for line in lines:
            if ':' in line:
                version_info_lines.append(line.split(":", 1))
        version_info = {
            key.strip(): value.strip()
            for key, value in version_info_lines
        }
        self.assertEqual('UPDATE 1', 'Version' in version_info)

