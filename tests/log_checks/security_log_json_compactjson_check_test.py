import json
import unittest
import os.path
import tempfile
from utility.config_paths import get_cisco_demo_path
from utility.nipper2_interface.nipper_report_interface import NipperReportInterface
from utility.nipper2_interface.nipper_logging_interface import NipperLoggingInterface
from utility.nipper2_interface import NipperInterface
from utility.temporary_license import TempLicense


class TestJsonSecurityAudit(unittest.TestCase):

    def setUp(self):
        """
        this setup will initialize nipper interfaces for performing operations on nipper
        :return: interfaces and test data
        """
        self._nipper = NipperInterface()
        self.license = TempLicense()
        self._nipper_report_generation = NipperReportInterface(self._nipper)
        self._nipper_log_settings = NipperLoggingInterface(self._nipper)
        self._temp_dir_context = tempfile.TemporaryDirectory()
        self._temp_dir = self._temp_dir_context.name
        self._report_location = os.path.join(self._temp_dir, "report.txt")
        self._temp_log_dir = self._temp_dir_context.name
        self._temp_json_log_file = os.path.join(self._temp_dir, "json_log.txt")
        self._temp_compact_json_log_file = os.path.join(self._temp_dir, "compact_json_log.txt")

        self._temp_json_log_file = os.path.join(self._temp_dir, "json_log.txt")
        self._temp_compact_json_log_file = os.path.join(self._temp_dir, "compact_json_log.txt")

        self.device_config_file = get_cisco_demo_path()
        """
        Required set of related data for the top keys,sub_keys and values needs to be verified in the actual data produced
        """
        self.expected_audit_type = "Security Audit"
        """
        the elements in the tag represent Check_id= "finding_id", title="title", finding = "finding", impact = "impact",
        ease = "ease", Overall rating = "message_level", category(s) = "types", recommendation = "recommendation"
        """
        self.top_tags = ["finding_id", "title", "finding", "impact", "ease", "message_level", "types", "recommendation"]
        """
        the elements in the sub_impact_tags represent impact_rating = "rating", impact_description = "description".
        the element in sub_ease_tags represent ease_of_exploit_rating = "rating", ease_description = "description".
        the elements in sub_finding_tags represent finding_description = "description".                                                   .
        the elements in sub_recommendation_tags represent recommendation_description = "description",
                                                         fix_complexity_rating = "rating".

        """
        self.sub_impact_tags = ["description", "rating"]
        self.sub_ease_tags = ["description", "rating"]
        self.sub_recommendation_tags = ["description", "rating"]
        self.sub_finding_tags = ["description"]
        """
        variables for json log data to store and perform tests.
        """
        self.json_log_data = []
        self.actual_sub_tags_in_json_finding = []
        self.actual_sub_tags_in_json_ease = []
        self.actual_sub_tags_in_json_impact = []
        self.actual_sub_tags_in_json_recommendation = []
        """
        variables for compact-json log data to store and perform tests.
        """
        self.compact_json_log_data = []
        self.actual_sub_tags_in_compact_json_finding = []
        self.actual_sub_tags_in_compact_json_ease = []
        self.actual_sub_tags_in_compact_json_impact = []
        self.actual_sub_tags_in_compact_json_recommendation = []
        self._nipper_log_settings.enable_file_logging(enable=True)
        self._nipper_report_generation.run_nipper("--all-reports=off")
        self._nipper_report_generation.run_nipper("--security=on")
        self._nipper_log_settings.set_file_logging_path(self._temp_json_log_file)
        self._nipper_log_settings.set_file_logging_format("json")
        self._nipper_log_settings.enable_log_file_json_stream(False)
        self._nipper_report_generation.generate_report(self.device_config_file, self._report_location)
        self._nipper_log_settings.set_file_logging_path(self._temp_compact_json_log_file)
        self._nipper_log_settings.set_file_logging_format("compact json")
        self._nipper_report_generation.generate_report(self.device_config_file, self._report_location)
        with open(self._temp_json_log_file, encoding='utf-8') as json_open:
            self.json_log_data = json.load(json_open)
            for top_tags in self.json_log_data:
                if "count" not in top_tags and "audits" not in top_tags:
                    self.actual_sub_tags_in_json_finding.append(top_tags.get("finding"))
                    self.actual_sub_tags_in_json_ease.append(top_tags.get("ease"))
                    self.actual_sub_tags_in_json_impact.append(top_tags.get("impact"))
                    self.actual_sub_tags_in_json_recommendation.append(top_tags.get("recommendation"))
        with open(self._temp_compact_json_log_file, encoding='utf-8') as compact_json_open:
            self.compact_json_log_data = json.load(compact_json_open)
            for top_tags in self.compact_json_log_data:
                if "count" not in top_tags and "audits" not in top_tags:
                    self.actual_sub_tags_in_compact_json_finding.append(top_tags.get("finding"))
                    self.actual_sub_tags_in_compact_json_ease.append(top_tags.get("ease"))
                    self.actual_sub_tags_in_compact_json_impact.append(top_tags.get("impact"))
                    self.actual_sub_tags_in_compact_json_recommendation.append(top_tags.get("recommendation"))

    def tearDown(self):
        """
        this method cleans the temp directory after each test for re-usability and tidy up.
        :return:
        """
        self._temp_dir_context.cleanup()

    def test_create_security_audit_log_file(self):
        """
        this methods checks security log file is created with preferences json and compact-json
        :return: Pass or Fail
        """
        with self.subTest("test to check json log file created"):
            self.assertTrue(os.path.exists(self._temp_json_log_file), msg='json log file not created')
        with self.subTest("test to check compact-json log file created"):
            self.assertTrue(os.path.exists(self._temp_compact_json_log_file), msg='compact-json log file not created')


    def test_json_format_and_compact_json_format_security_audit_report_log(self):
        """
        this methods checks the log file created in the above test are valid json and the audit_type key has
        value Security Audit
        :return: Pass or Fail
        """
        with self.subTest("check for json log"):
            for top_tags_json in self.json_log_data:
                if "audit_type" in top_tags_json:
                    self.assertEqual("Security Audit", top_tags_json.get("audit_type"),
                                     msg="audit_type is not Security Audit")
        with self.subTest("check for compact json log"):
            for top_tags_compact_json in self.compact_json_log_data:
                if "audit_type" in top_tags_compact_json:
                    self.assertEqual("Security Audit", top_tags_compact_json.get("audit_type"),
                                     msg="audit_type is not Security Audit")

    def test_top_tags_available_in_log_data(self):
        """
        this test check for the top keys in json log and compact-json log
        :return: Pass or Fail
        """
        with self.subTest("test for top tags in jsonlogdata"):
            for top_tags_json in self.json_log_data:
                if "count" not in top_tags_json and "audits" not in top_tags_json:
                    for sub_tags in self.top_tags:
                        self.assertTrue(sub_tags in top_tags_json, msg=f'{sub_tags} not in {top_tags_json}')

        with self.subTest("test for top tags in compact_json_log_data"):
            for top_tags_compact_json in self.compact_json_log_data:
                if "count" not in top_tags_compact_json and "audits" not in top_tags_compact_json:
                    for sub_tags in self.top_tags:
                        self.assertTrue(sub_tags in top_tags_compact_json, msg=f'{sub_tags} not in '
                                                                               f'{top_tags_compact_json}')

    def test_finding_subtag_in_log_data_and_not_empty(self):
        """
        this test checks for sub keys for the key "finding" is available and has got value stored
        :return: Pass or Fail
        """
        with self.subTest("test for finding subtags in jsonlog and not empty"):
            for sub_tags in self.actual_sub_tags_in_json_finding:
                for top_tags in self.sub_finding_tags:
                    self.assertTrue(top_tags in sub_tags, msg=f'{sub_tags} not in {top_tags}')
            for expected_sub_tags in self.sub_finding_tags:
                for tags in self.json_log_data:
                    if "count" not in tags and "audits" not in tags:
                        self.assertTrue((tags.get("finding").get(expected_sub_tags)) is not None,
                                        msg=f'{expected_sub_tags} is empty in {tags}')
        with self.subTest("test for finding subtags in compactjsonlog and not empty"):
            for sub_tags in self.actual_sub_tags_in_compact_json_finding:
                for top_tags in self.sub_finding_tags:
                    self.assertTrue(top_tags in sub_tags, msg=f'{sub_tags} not in {top_tags}')
            for expected_sub_tags in self.sub_finding_tags:
                for tags in self.compact_json_log_data:
                    if "count" not in tags and "audits" not in tags:
                        self.assertTrue((tags.get("finding").get(expected_sub_tags)) is not None,
                                        msg=f'{expected_sub_tags} is empty in {tags}')

    def test_impact_subtag_in_log_data_and_not_empty(self):
        """
        this test checks for sub keys for the key "impact" is available and has got value stored
        :return: Pass or Fail
        """
        with self.subTest("test for Impact subtags in jsonlog and not empty"):
            for sub_tags in self.actual_sub_tags_in_json_impact:
                for top_tags in self.sub_impact_tags:
                    self.assertTrue(top_tags in sub_tags, msg=f'{sub_tags} not in {top_tags}')
            for expected_sub_tags in self.sub_impact_tags:
                for tags in self.json_log_data:
                    if "count" not in tags and "audits" not in tags:
                        self.assertTrue((tags.get("impact").get(expected_sub_tags)) is not None,
                                        msg=f'{expected_sub_tags} is empty in {tags}')
        with self.subTest("test for Impact subtags in compactjsonlog and not empty"):
            for sub_tags in self.actual_sub_tags_in_compact_json_impact:
                for top_tags in self.sub_impact_tags:
                    self.assertTrue(top_tags in sub_tags, msg=f'{sub_tags} not in {top_tags}')
            for expected_sub_tags in self.sub_impact_tags:
                for tags in self.compact_json_log_data:
                    if "count" not in tags and "audits" not in tags:
                        self.assertTrue((tags.get("impact").get(expected_sub_tags)) is not None,
                                        msg=f'{expected_sub_tags} is empty in {tags}')

    def test_ease_subtag_in_log_data_and_not_empty(self):
        """
        this test checks for sub keys for the key "ease" is available and has got value stored
        :return: Pass or Fail
        """
        with self.subTest("test for ease subtags in jsonlog and not empty"):
            for sub_tags in self.actual_sub_tags_in_json_ease:
                for top_tags in self.sub_ease_tags:
                    self.assertTrue(top_tags in sub_tags, msg=f'{sub_tags} not in {top_tags}')
            for expected_sub_tags in self.sub_ease_tags:
                for tags in self.json_log_data:
                    if "count" not in tags and "audits" not in tags:
                        self.assertTrue((tags.get("ease").get(expected_sub_tags)) is not None,
                                        msg=f'{expected_sub_tags} is empty in {tags}')
        with self.subTest("test for ease subtags in compactjsonlog and not empty"):
            for sub_tags in self.actual_sub_tags_in_compact_json_ease:
                for top_tags in self.sub_ease_tags:
                    self.assertTrue(top_tags in sub_tags, msg=f'{sub_tags} not in {top_tags}')
            for expected_sub_tags in self.sub_ease_tags:
                for tags in self.compact_json_log_data:
                    if "count" not in tags and "audits" not in tags:
                        self.assertTrue((tags.get("ease").get(expected_sub_tags)) is not None,
                                        msg=f'{expected_sub_tags} is empty in {tags}')

    def test_recommendation_subtag_in_log_data_and_not_empty(self):
        """
        this test checks for sub keys for the key "recommendation" is available and has got value stored
        :return: Pass or Fail
        """
        with self.subTest("test for recommendation subtags in jsonlog and not empty"):
            for sub_tags in self.actual_sub_tags_in_json_recommendation:
                for top_tags in self.sub_recommendation_tags:
                    self.assertTrue(top_tags in sub_tags, msg=f'{sub_tags} not in {top_tags}')
            for expected_sub_tags in self.sub_recommendation_tags:
                for tags in self.json_log_data:
                    if "count" not in tags and "audits" not in tags:
                        self.assertTrue((tags.get("recommendation").get(expected_sub_tags)) is not None,
                                        msg=f'{expected_sub_tags} is empty in {tags}')
        with self.subTest("test for recommendation subtags in compactjsonlog and not empty"):
            for sub_tags in self.actual_sub_tags_in_compact_json_recommendation:
                for top_tags in self.sub_recommendation_tags:
                    self.assertTrue(top_tags in sub_tags, msg=f'{sub_tags} not in {top_tags}')
            for expected_sub_tags in self.sub_recommendation_tags:
                for tags in self.compact_json_log_data:
                    if "count" not in tags and "audits" not in tags:
                        self.assertTrue((tags.get("recommendation").get(expected_sub_tags)) is not None,
                                        msg=f'{expected_sub_tags} is empty in {tags}')

    def test_to_check_log_for_conclusions_log_and_overall_report_log_for_security_log(self):
        """
        this test checks the two sub json data which gives log for conclusion and overall report
        :return: Pass or Fail
        """
        for tags in self.json_log_data:
            """Report conclusion log contents"""
            if "count" in tags:
                self.assertEqual("Security Audit", tags.get("audit_type"))
                self.assertTrue(tags.get("message_level") is not None,
                                msg="summary security audit level is not mentioned")
                """overall report log contents"""
            elif "audits" in tags:
                for sub_tags in tags.get("audits"):
                    self.assertEqual("Security Audit", sub_tags)
                    self.assertTrue(tags.get("message_level") is not None,
                                    msg="summary security audit level is not mentioned")

        for tags in self.compact_json_log_data:
            """Report conclusion log contents"""
            if "count" in tags:
                self.assertEqual("Security Audit", tags.get("audit_type"))
                self.assertTrue(tags.get("message_level") is not None,
                                msg="summary security audit level is not mentioned")
                """overall report log contents"""
            elif "audits" in tags:
                for sub_tags in tags.get("audits"):
                    self.assertEqual("Security Audit", sub_tags)
                    self.assertTrue(tags.get("message_level") is not None,
                                    msg="summary security audit level is not mentioned")
