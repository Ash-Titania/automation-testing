import time
import unittest
import os
import tempfile
import re
from utility.nipper2_interface import NipperInterface
from utility.nipper2_interface import NipperProfileInterface
from utility.nipper2_interface import NipperReportInterface
from utility.temporary_license import TempLicense
from resources.create_report.create_report_data import stress_test


class TestStress(unittest.TestCase):
    """ Test running the stress test folder """

    def setUp(self):
        self._nipper = NipperInterface()
        self.license = TempLicense()
        self._nipper_report = NipperReportInterface(self._nipper)
        self._temp_dir_context = tempfile.TemporaryDirectory()
        self._temp_dir = self._temp_dir_context.name
        NipperProfileInterface(self._nipper).reset_default_settings()
        self.set_checkpoint_settings()

    def tearDown(self):
        self.reset_checkpoint_settings()
        self._temp_dir_context.cleanup()

    def test_stress(self):
        """ Audit the stress test folder, and check the returned file """
        if os.path.exists(stress_test):
            report_location = os.path.join(self._temp_dir, "stress-test.txt")
            start = time.time()
            self._nipper_report.generate_report(stress_test, report_location, '--text')
            duration = time.time() - start
            self.assertLess(duration, 600, 'Report should take less than 10 minutes')
            self.check_results(report_location)
        else:
            self.skipTest("Can't access the stress test folder")

    def check_results(self, output_location):
        """
        :param output_location: The location of the resulting txt report
        Check the results of the returned file
        """
        self.assertTrue(os.path.exists(output_location), 'An output file was created')
        self.assertGreater(os.stat(output_location).st_size, 3500000, 'Output file must be more than 3.5mb')

    def set_checkpoint_settings(self):
        """ Store the user's current settings, then turn off interactive questions """
        devices_list = self._nipper.run_nipper('--help=devices').stdout
        checkpoint_regex = re.compile("([0-9]+)\\s*--checkpoint-management\\s*")
        checkpoint_id = checkpoint_regex.search(devices_list).group(1)
        checkpoint_settings = self._nipper.run_nipper('--help=devices' + checkpoint_id).stdout
        checkpoint_mode_regex = re.compile(
            "Parameter:\\s*--checkpoint-mgmt-mode=<option>\\n.*\\n.*\\n.*\\n.*\\n\\s*Setting\\s*:\\s*([^ ]*)")
        self._current_mode = checkpoint_mode_regex.search(checkpoint_settings).group(1)
        checkpoint_device_regex = re.compile(
            "Parameter:\\s*--checkpoint-mgmt-device-selection=<option>\\n.*\\n.*\\n\\s*Setting\\s*:\\s*([^ ]*)")
        self._current_device = checkpoint_device_regex.search(checkpoint_settings).group(1)
        self._nipper.run_nipper('--checkpoint-mgmt-mode=everything', '--checkpoint-mgmt-device-selection=all')

    def reset_checkpoint_settings(self):
        """ Reset the interactive question settings to the user's previous set values """
        self._nipper.run_nipper('--checkpoint-mgmt-mode=' + self._current_mode, '--checkpoint-mgmt-device-selection=' +
                                self._current_device)
