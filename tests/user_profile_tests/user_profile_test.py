from utility.nipper2_interface.nipper_profile_interface import NipperProfileInterface
from utility.nipper2_interface import NipperInterface
from resources.user_profile.user_profile_data import *
import unittest
import os.path
import winreg
import tempfile


class TestNipperProfile(unittest.TestCase):

    def setUp(self, *args, **kwargs):
        self._nipper = NipperInterface()
        self._nipper_profile = NipperProfileInterface(self._nipper)
        self._temp_dir_context = tempfile.TemporaryDirectory()
        self._temp_dir = self._temp_dir_context.name
        self.user1_file_path = self._temp_dir + user1_append

    def tearDown(self):
        self._temp_dir_context.cleanup()

    def test_save_and_delete_profile(self):
        self._nipper_profile.save_profile(user1_append)
        self.assertIn(user1_append, self._nipper_profile.get_profiles(), f"{user1_append} was not found in the profile list")
        self._nipper_profile.delete_profile(user1_append)
        self.assertNotIn(user1_append, self._nipper_profile.get_profiles())

    def test_export_and_import_profile(self):
        self._nipper_profile.save_profile(user1_append)
        self._nipper_profile.export_profile(user1_append, self.user1_file_path)
        self.assertTrue(os.path.isfile(self.user1_file_path), ' file does not exist')
        self._nipper_profile.import_profile(user1_append, self.user1_file_path)
        self.assertIn(user1_append, self._nipper_profile.get_profiles())
        self._nipper_profile.delete_profile(user1_append)

        self._nipper_profile.delete_profile(user1_append)

    def test_use_and_profile_check(self):
        """this test will use import profile which changes the value in registry directory with creating sub_key for
        user_profile.(skipping maintenance key value check as it is not getting effected until developed)"""

        self._nipper_profile.import_profile(user2_append, nipper_profile_2_path)
        self._nipper_profile.use_profile(user2_append)
        key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, r'Software\Titania\Nipper\profiles\user2.xml\Settings')
        self.assertIn('off', (winreg.QueryValueEx(key, 'Show Passwords In Report')))
        self.assertIn('on', (winreg.QueryValueEx(key, 'Enable logging to Event Log')))

        key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, r'Software\Titania\Nipper\profiles\user2.xml\Plugins'
                                                       r'\Alteon Switched Firewall\Settings')
        self.assertIn('Smart', (winreg.QueryValueEx(key, 'Policy Mode')))

        key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, r'Software\Titania\Nipper'
                                                       r'\profiles\user2.xml\Plugins\Security Audit\Settings')
        self.assertIn('15', (winreg.QueryValueEx(key, 'Minimum Password Length')))

        key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, r'Software\Titania\Nipper\profiles'
                                                       r'\user2.xml\Plugins\ASCII Text\Settings')
        self.assertIn('Red', (winreg.QueryValueEx(key, 'Report Text Color')))

        self._nipper_profile.use_profile('Default')
        self._nipper_profile.delete_profile(user2_append)
        winreg.CloseKey(key)

    @unittest.skip("NIPPER-4636 maintenance settings doesn't save in user_profile needs to be developed and integrated")
    def test_incomplete_test_use_and_profile_check(self):
        """ this code needs to be added to the above test and also new user2 profile saved in
        t_drive_location = r'T:/Technical Services Team/Application Testing/1 Nipper Studio/Test Data/user profile testing folder/'
        mock_t_drive = r'../../../Mock-T-Drive/user profile testing folder/'  having subkeys and values for maintenance"""

        key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, r'Software\Titania\Nipper\profiles\user2.xml\Settings')
        self.assertIn('Every Use', (winreg.QueryValueEx(key, 'Check Interval')))


