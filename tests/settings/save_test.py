# Test that NipperStudio's exported settings are not equal
# to the default Nipper Studio settings after being changed

import unittest
import os
import platform
from utility.settings_profile import SettingsProfile
from .logging_default_settings import *
from utility.nipper2_interface import NipperProfileInterface
from utility.nipper2_interface import NipperInterface
from utility.nipper2_interface import NipperMaintenanceInterface
from utility.nipper2_interface import NipperLoggingInterface
from utility.temporary_license import TempLicense


def settings_match(nipper_dict, setting_dict):
    """
    compare two dictionaries, where keys inside of nipper_dict that are not
    inside of setting_dict are ignored
    :param nipper_dict: dictionary with every Nipper Studio setting inside
    :param setting_dict: dictionary with a subset of Nipper Studio settings
    :return: True when all key: values match each other from both dictionaries
    """
    nipper_set = set(nipper_dict.items())
    required_set = set(setting_dict.items())
    return required_set.issubset(nipper_set)


def settings_do_not_match(nipper_dict, setting_dict):
    """
    compare two dictionaries, where keys inside of nipper_dict that are not
    inside of setting_dict are ignored
    :param nipper_dict: dictionary with every Nipper Studio setting inside
    :param setting_dict: dictionary with a subset of Nipper Studio settings
    :return: True when no dictionary key: values match from both dictionaries
    """
    comparison_dict = [k for k, v in setting_dict.items() if nipper_dict[k] != v]
    return len(comparison_dict) == len(setting_dict)


def get_settings_matched(nipper_dict, setting_dict):
    """
    get a list of keys where the respective values were equal in both dictionaries
    :param nipper_dict: dictionary with every Nipper Studio setting inside
    :param setting_dict: dictionary with a subset of Nipper Studio settings
    :return: list of keys whose values are equal in both dictionaries
    """
    return [k for k, v in setting_dict.items() if nipper_dict[k] == v]


def get_settings_not_matched(nipper_dict, setting_dict):
    """
    get a list of keys where the respective values were not equal in both dictionaries
    :param nipper_dict: dictionary with every Nipper Studio setting inside
    :param setting_dict: dictionary with a subset of Nipper Studio settings
    :return: list of keys whose values are not equal in both dictionaries
    """
    return [k for k, v in setting_dict.items() if nipper_dict[k] != v]


def export_nipper_and_get_settings(profile_name="tempprofile", delete=True):
    """
    save a profile to Nipper Studio, export this profile, and parse it to a dictionary,
    optionally deleting the profile that was created
    :param profile_name: name of the profile created in Nipper Studio
    :param delete: delete the profile created in Nipper Studio
    :return: dictionary of Nipper Studio's parsed exported settings
    """
    nipper_profile = NipperProfileInterface(NipperInterface())
    nipper_profile.save_profile(profile_name)
    nipper_profile.export_profile(profile_name, "nipper_profile.xml")
    if delete:
        nipper_profile.delete_profile(profile_name)
    current_settings_profile = SettingsProfile("nipper_profile.xml")
    parsed_profile = current_settings_profile.parse_profile()
    os.remove("nipper_profile.xml")
    return parsed_profile


def get_all_default_logging_settings_dict():
    """
    get all default logging settings in a single dictionary,
    taking into account platform specific settings
    :return: dictionary containing all default logging settings for a specific platform
    """
    platform_specific_settings = {}
    if platform.system() == "Windows":
        platform_specific_settings = EVENT_LOGGING_DEFAULT_SETTINGS
    elif platform.system() == "Linux":
        platform_specific_settings = SYSLOG_LOGGING_DEFAULT_SETTINGS

    return {
        **GENERAL_LOGGING_DEFAULT_SETTINGS,
        **FILE_LOGGING_DEFAULT_SETTINGS,
        **TCP_LOGGING_DEFAULT_SETTINGS,
        **UDP_LOGGING_DEFAULT_SETTINGS,
        **EMAIL_LOGGING_DEFAULT_SETTINGS,
        **platform_specific_settings,
    }


class TestLoggingSaving(unittest.TestCase):

    nipper_profile_name = "automationtest"

    def __init__(self, *args, **kwargs):
        super(TestLoggingSaving, self).__init__(*args, **kwargs)
        nipper = NipperInterface()
        self.license = TempLicense()
        self._nipper_logging = NipperLoggingInterface(nipper)
        self._nipper_profile = NipperProfileInterface(nipper)
        self._nipper_profile.reset_default_settings()

        # Prevents Nipper from syncing and taking
        # years to run cli commands
        NipperMaintenanceInterface(nipper).set_update_interval("never")

    @classmethod
    def tearDownClass(cls):
        NipperProfileInterface(NipperInterface()).reset_default_settings()

    def test01_logging_settings_save(self):
        """
        test that logging settings persist after being changed from a default state
        """
        change_settings_test_cases = [
            (self.change_general_logging_settings, GENERAL_LOGGING_DEFAULT_SETTINGS),
            (self.change_file_logging_settings, FILE_LOGGING_DEFAULT_SETTINGS),
            (self.change_tcp_logging_settings, TCP_LOGGING_DEFAULT_SETTINGS),
            (self.change_udp_logging_settings, UDP_LOGGING_DEFAULT_SETTINGS),
            (self.change_email_logging_settings, EMAIL_LOGGING_DEFAULT_SETTINGS),
        ]

        if platform.system() == "Windows":
            change_settings_test_cases.append((self.change_event_logging_settings, EVENT_LOGGING_DEFAULT_SETTINGS))
        elif platform.system() == "Linux":
            change_settings_test_cases.append((self.change_syslog_logging_settings, SYSLOG_LOGGING_DEFAULT_SETTINGS))

        for change_settings, default_settings in change_settings_test_cases:
            with self.subTest(change_settings=change_settings, default_settings=default_settings):
                change_settings()
                current_settings = export_nipper_and_get_settings()

                failed_matches = get_settings_matched(current_settings, default_settings)
                self.assertTrue(settings_do_not_match(current_settings, default_settings), failed_matches)

    def test02_reset_defaults(self):
        """
        test that Nipper Studio can reset settings from a non-default state, to a default state.
        this relies on previous tests changing the settings. we also save a settings profile for
        test03.
        """
        current_settings = export_nipper_and_get_settings(TestLoggingSaving.nipper_profile_name, delete=False)
        all_logging_settings = get_all_default_logging_settings_dict()

        # Test before and after resetting default settings, to confirm a change was made
        self.assertFalse(settings_match(current_settings, all_logging_settings))
        self._nipper_profile.reset_default_settings()

        reset_settings = export_nipper_and_get_settings()
        settings_not_matched = get_settings_not_matched(reset_settings, all_logging_settings)
        self.assertTrue(settings_match(reset_settings, all_logging_settings), settings_not_matched)

    def test03_load_profiles(self):
        """
        tests that Nipper Studio can load settings profiles, using the settings profile saved for us
        in test01, to go from a default state, to a non-default state.
        """
        reset_settings = export_nipper_and_get_settings()
        all_logging_settings = get_all_default_logging_settings_dict()

        # Assert we are currently using default Nipper Studio settings
        self.assertTrue(settings_match(reset_settings, all_logging_settings), "Failed to reset to default settings")
        self._nipper_profile.use_profile(TestLoggingSaving.nipper_profile_name)

        # Assert changing profiles now gives us non-default settings
        profile_settings = export_nipper_and_get_settings()
        settings_matched = get_settings_matched(reset_settings, all_logging_settings)
        self.assertTrue(settings_do_not_match(profile_settings, all_logging_settings), settings_matched)

    # Methods to change Nipper Settings from their defaults
    def change_general_logging_settings(self):
        """
        change Nipper Studio general logging settings to a non-default state
        """
        self._nipper_logging.add_logging_tag("Unit", "Test")
        self._nipper_logging.set_report_generation_successful_log_level("Critical")
        self._nipper_logging.set_report_generation_error_log_level("Informational")

    def change_event_logging_settings(self):
        """
        change Nipper Studio event logging settings to a non-default state
        """
        self._nipper_logging.enable_event_logging(True)
        self._nipper_logging.enable_all_eventlog_logging_levels(False)

    def change_syslog_logging_settings(self):
        """
        change Nipper Studio syslog logging settings to a non-default state
        """
        self._nipper_logging.enable_syslog_logging(True)
        self._nipper_logging.set_syslog_logging_format("json")
        self._nipper_logging.enable_all_syslog_logging_levels(False)

    def change_file_logging_settings(self):
        """
        change Nipper Studio file logging settings to a non-default state
        """
        self._nipper_logging.enable_file_logging(True)
        self._nipper_logging.set_file_logging_format("json")
        self._nipper_logging.set_cef_eol_logging_format("none")
        self._nipper_logging.set_file_logging_path("./nipper_logs.txt")
        self._nipper_logging.enable_all_file_logging_levels(False)

    def change_tcp_logging_settings(self):
        """
        change Nipper Studio tcp logging settings to a non-default state
        """
        self._nipper_logging.enable_tcp_logging(True)
        self._nipper_logging.set_tcp_logging_format("json")
        self._nipper_logging.set_tcp_logging_ip("1.2.3.4")
        self._nipper_logging.set_tcp_logging_port("12")
        self._nipper_logging.enable_all_tcp_logging_levels(False)

    def change_udp_logging_settings(self):
        """
        change Nipper Studio udp logging settings to a non-default state
        """
        self._nipper_logging.enable_udp_logging(True)
        self._nipper_logging.set_udp_logging_format("json")
        self._nipper_logging.set_udp_logging_ip("1.2.3.4")
        self._nipper_logging.set_udp_logging_port("12")
        self._nipper_logging.enable_all_udp_logging_levels(False)

    def change_email_logging_settings(self):
        """
        change Nipper Studio email logging settings to a non-default state
        """
        self._nipper_logging.enable_email_logging(True)
        self._nipper_logging.set_email_logging_attachment(True)
        self._nipper_logging.set_email_logging_encryption("unencrypted")
        self._nipper_logging.set_email_logging_format("json")
        self._nipper_logging.set_email_logging_username("emanresu")
        self._nipper_logging.set_email_logging_password("drowssap")
        self._nipper_logging.set_email_logging_recipient("Bob@Email.com")
        self._nipper_logging.set_email_logging_sender("Billy@Email.com")
        self._nipper_logging.set_email_logging_subject("Unit Test")
        self._nipper_logging.set_email_logging_smtp_port("101")
        self._nipper_logging.set_email_logging_smtp_server("4.3.2.1")
        self._nipper_logging.enable_all_email_logging_levels(False)


