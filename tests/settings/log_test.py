import unittest
import os
import platform
import time
import smtpd
import asyncore
import tempfile
import sys
from threading import Thread
from utility.socket_server import TcpServer, UdpServer
from utility.nipper2_interface import NipperInterface, NipperProfileInterface, NipperReportInterface, \
    NipperLoggingInterface
from utility.config_paths import get_cisco_demo_path
from utility.temporary_license import TempLicense
from getpass import getuser

if platform.system() == "Windows":
    try:
        import win32evtlog
    except ModuleNotFoundError:
        print("Unable to import win32evtlog")


class CustomSMTPServer(smtpd.SMTPServer):
    """
    Custom implementation of SMTP server, which closes the asyncore loop when an email is received
    """
    def process_message(self, peer, mailfrom, rcpttos, data, **kwargs):
        asyncore.close_all()


def start_smtp_and_wait_for_message(port):
    """
    blocking function that will only return once an email has been received by smtp_server
    :param port: port number to start server on localhost
    """
    smtp_server = CustomSMTPServer(("127.0.0.1", port), None)
    asyncore.loop()


def monitor_syslog_for_nipper_studio():
    """
    blocking function that will only return when the syslog has been populated with new Nipper logs
    :return: True when new Nipper logs have been found
    """
    syslog_location = "/var/log/messages"
    with open(syslog_location) as syslog_file:
        syslog_file.seek(0, 2)  # Goto end of file
        # Loop will never end, this function is expected to be called with a timeout
        while True:
            line = syslog_file.readline()
            if not line:
                time.sleep(0.1)
            else:
                if "Nipper Studio" in line:
                    return True


class TestLoggingOutputTypes(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestLoggingOutputTypes, self).__init__(*args, **kwargs)
        nipper = NipperInterface()
        self.license = TempLicense()
        self._nipper_logging = NipperLoggingInterface(nipper)
        self._nipper_report = NipperReportInterface(nipper)

    def setUp(self):
        self._temp_dir_context = tempfile.TemporaryDirectory()
        self._temp_dir = self._temp_dir_context.name
        NipperProfileInterface(NipperInterface()).reset_default_settings()

    def run_ios_report(self):
        """
        runs a report on a demo ios-router device using Nipper
        """
        input_file = get_cisco_demo_path()

        report_location = f"{self._temp_dir}/cisco-ios.html"
        self._nipper_report.generate_report(input_file, report_location, "--ios-router")

    def test_file_logging(self):
        """
        tests that Nipper can create files for file logging
        """

        file_log_location = f"{self._temp_dir}/log-file.txt"
        self._nipper_logging.enable_file_logging(True)
        self._nipper_logging.set_file_logging_path(file_log_location)

        self.assertFalse(os.path.isfile(file_log_location))
        self.run_ios_report()
        time.sleep(1)
        self.assertTrue(os.path.isfile(file_log_location))

    def test_tcp_logging(self):
        """
        tests that Nipper can connect over TCP to send logs
        """
        self._nipper_logging.enable_tcp_logging(True)
        self._nipper_logging.set_tcp_logging_ip("127.0.0.1")
        port = 4003
        self._nipper_logging.set_tcp_logging_port(port)

        tcp_server = TcpServer()
        thread = Thread(target=tcp_server.get_tcp_data, args=[port], daemon = True)
        thread.start()
        self.run_ios_report()
        thread.join(timeout=10.0)

        # If the thread is alive, it terminated, and did not end naturally.
        self.assertFalse(thread.is_alive())

    def test_udp_logging(self):
        """
        tests that Nipper can connect over UDP to send logs
        """
        self._nipper_logging.enable_udp_logging(True)
        self._nipper_logging.set_udp_logging_ip("127.0.0.1")
        port = 4004
        self._nipper_logging.set_udp_logging_port(port)

        udp_server = UdpServer()
        thread = Thread(target=udp_server.get_udp_data, args=[port], daemon=True)
        thread.start()
        self.run_ios_report()
        thread.join(timeout=10.0)

        # If the thread is alive, it terminated, and did not end naturally.
        self.assertFalse(thread.is_alive())

    @unittest.skipUnless("bamboo" not in getuser(),
                         "Test fails due to Bamboo restrictions that cause Nipper to not be able to log into "
                         "the Windows event log")
    @unittest.skipUnless(platform.system() == "Windows", "Event logging is Windows specific")
    @unittest.skipUnless("win32evtlog" in sys.modules, "Failed to import win32evtlog")
    def test_event_log_logging(self):
        """
        tests that Nipper can add logs to the Windows Event Logger
        """
        self._nipper_logging.enable_event_logging(True)
        self.run_ios_report()

        handle = win32evtlog.OpenEventLog(None, "Nipper")
        flags = win32evtlog.EVENTLOG_BACKWARDS_READ | win32evtlog.EVENTLOG_SEQUENTIAL_READ
        logs = win32evtlog.ReadEventLog(handle, flags, 0, 10240)
        nipper_logs = [log for log in logs if log.SourceName == "Nipper"]
        self.assertTrue(len(nipper_logs) > 0)

    @unittest.skipUnless(platform.system() == "Linux", "System logging is Linux specific")
    def test_syslog_logging(self):
        """
        tests that Nipper can add logs to the Linux syslog file
        """
        syslog_monitor_thread = Thread(target=monitor_syslog_for_nipper_studio, daemon=True)
        syslog_monitor_thread.start()
        self.run_ios_report()
        syslog_monitor_thread.join(timeout=10.0)
        self.assertFalse(syslog_monitor_thread.is_alive())

    @unittest.skipUnless(platform.system() == "Windows", "NIPPER-4143")
    def test_email_logging(self):
        """
        tests that Nipper can log by sending emails, by creating an SMTP server
        and pointing Nipper to it
        """
        self._nipper_logging.enable_email_logging(True)
        self._nipper_logging.set_email_logging_sender("TestSender")
        self._nipper_logging.set_email_logging_recipient("TestRecipient")
        self._nipper_logging.set_email_logging_encryption("unencrypted")
        self._nipper_logging.set_email_logging_smtp_server("127.0.0.1")
        port = 4005
        self._nipper_logging.set_email_logging_smtp_port(port)

        smtp_thread = Thread(target=start_smtp_and_wait_for_message, args=[port], daemon = True)
        smtp_thread.start()
        self.run_ios_report()
        smtp_thread.join(timeout=10.0)

        self.assertFalse(smtp_thread.is_alive())

