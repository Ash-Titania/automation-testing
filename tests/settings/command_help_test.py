"""
Test the help file with list of all
logging commands is generated
"""

import unittest
import os
import platform
from utility.nipper2_interface import NipperInterface
from utility.nipper2_interface import NipperProfileInterface
from utility.temporary_license import TempLicense


class LoggingHelpTest(unittest.TestCase):

    def setUp(self):
        nipper = NipperInterface()
        self.license = TempLicense()
        directory_path = os.path.dirname(os.path.realpath(__file__))

        NipperProfileInterface(nipper).reset_default_settings()
        actual_output = nipper.run_nipper('--help=logging').stdout
        all_lines = actual_output.splitlines()

        if platform.system() == "Windows":
            self.extracted_command_output = all_lines[23:]
            self.comparison_file_path = os.path.join(directory_path, 'commands_list_windows')
        elif platform.system() == "Linux":
            self.extracted_command_output = all_lines[12:]
            self.comparison_file_path = os.path.join(directory_path, 'commands_list_linux')
        else:
            raise Exception("unsupported platform for running tests")

    @unittest.skip("NIPPER-4253")
    def test_help_command_exact(self):
        """
        generate help command for logging topic and compare the output with standard help text file
        """
        with open(self.comparison_file_path, 'r') as file_handle:
            expected_output = file_handle.read().splitlines()
            self.assertListEqual(expected_output, self.extracted_command_output)

    @unittest.skip("NIPPER-4253")
    def test_help_command_blank_space_tolerant(self):
        """
        generate help command for logging topic and compare the output with standard help text file
        """
        def remove_blank_lines_and_trim_each_line(lines):
            return [line.strip() for line in lines if line.strip()]

        extracted_command_output = remove_blank_lines_and_trim_each_line(self.extracted_command_output)

        with open(self.comparison_file_path, 'r') as file_handle:
            expected_output = remove_blank_lines_and_trim_each_line(file_handle.read().splitlines())
            self.assertListEqual(expected_output, extracted_command_output)

