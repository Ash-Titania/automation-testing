GENERAL_LOGGING_DEFAULT_SETTINGS = {
    'Report Generation Successful Log Level': 'Informational',
    'Report Generation Error Log Level': 'Critical',
    'Logging Tags': None,
}

EVENT_LOGGING_DEFAULT_SETTINGS = {
    'Enable logging to Event Log': 'off',
    'Event Log Trigger Levels': '''
<TreeViewSettings>
    <category name="Report" tooltip="Events related to the creation and execution of a report." expand="true">
        <category name="Audit Fail" tooltip="Raised when one or more checks in an audit has failed." expand="">
            <setting name="Critical" type="3" value="4" default="true">true</setting>
            <setting name="High" type="3" value="3" default="true">true</setting>
            <setting name="Informational" type="3" value="0" default="true">true</setting>
            <setting name="Low" type="3" value="1" default="true">true</setting>
            <setting name="Medium" type="3" value="2" default="true">true</setting>
        </category>
        <category name="Audit Pass" tooltip="Raised when every check in an audit has passed." expand="">
            <setting name="Critical" type="2" value="4" default="true">true</setting>
            <setting name="High" type="2" value="3" default="true">true</setting>
            <setting name="Informational" type="2" value="0" default="true">true</setting>
            <setting name="Low" type="2" value="1" default="true">true</setting>
            <setting name="Medium" type="2" value="2" default="true">true</setting>
        </category>
        <category name="Check Fail" tooltip="Raised when a check has failed during report generation." expand="">
            <setting name="Critical" type="5" value="4" default="true">true</setting>
            <setting name="High" type="5" value="3" default="true">true</setting>
            <setting name="Informational" type="5" value="0" default="true">true</setting>
            <setting name="Low" type="5" value="1" default="true">true</setting>
            <setting name="Medium" type="5" value="2" default="true">true</setting>
        </category>
        <category name="Check Pass" tooltip="Raised when a check has passed during report generation." expand="">
            <setting name="Critical" type="4" value="4" default="true">true</setting>
            <setting name="High" type="4" value="3" default="true">true</setting>
            <setting name="Informational" type="4" value="0" default="true">true</setting>
            <setting name="Low" type="4" value="1" default="true">true</setting>
            <setting name="Medium" type="4" value="2" default="true">true</setting>
        </category>
        <category name="Generation" tooltip="Events raised when a report is generated." expand="">
            <setting name="Error" type="0" value="ALL" default="true" tooltip="Raised when a report has failed to create and an error is raised. When checked this will catch all error events regardless of log level.">true</setting>
            <setting name="Successful" type="1" value="ALL" default="true" tooltip="Raised when a report is successfully created.When checked this will catch all successful events regardless of log level.">true</setting>
        </category>
    </category>
    <category name="System" tooltip="General system events." expand="true">
        <setting name="Error" type="8" value="ALL" default="true" tooltip="Events that are raised when an error occurs.">true</setting>
    </category>
</TreeViewSettings>'''
}

SYSLOG_LOGGING_DEFAULT_SETTINGS = {
    'Enable logging to Syslog': 'off',
    'Format to use as output for syslogs': 'Plain Text',
    'Syslog Trigger Levels': '''
<TreeViewSettings>
  <category name="Report" tooltip="Events related to the creation and execution of a report." expand="true">
	  <category name="Generation" tooltip="Events raised when a report is generated.">
		  <setting name="Successful" type="1" value="ALL" default="true" tooltip="Raised when a report is successfully created.When checked this will catch all successful events regardless of log level."/>
		  <setting name="Error" type="0" value="ALL" default="true" tooltip="Raised when a report has failed to create and an error is raised. When checked this will catch all error events regardless of log level."/>
	  </category>
	  <category name="Audit Pass" tooltip="Raised when every check in an audit has passed.">
          <setting name="Critical" type="2" value="4" default="true"/>
		  <setting name="High" type="2" value="3" default="true"/>
		  <setting name="Medium" type="2" value="2" default="true"/>
		  <setting name="Low" type="2" value="1" default="true"/>
		  <setting name="Informational" type="2" value="0" default="true"/>
	  </category>
	  <category name="Audit Fail" tooltip="Raised when one or more checks in an audit has failed.">
		  <setting name="Critical" type="3" value="4" default="true"/>
		  <setting name="High" type="3" value="3" default="true"/>
		  <setting name="Medium" type="3" value="2" default="true"/>
		  <setting name="Low" type="3" value="1" default="true"/>
		  <setting name="Informational" type="3" value="0" default="true"/>
	  </category>
	  <category name="Check Pass" tooltip="Raised when a check has passed during report generation.">
		  <setting name="Critical" type="4" value="4" default="true"/>
		  <setting name="High" type="4" value="3" default="true"/>
		  <setting name="Medium" type="4" value="2" default="true"/>
		  <setting name="Low" type="4" value="1" default="true"/>
		  <setting name="Informational" type="4" value="0" default="true"/>
	  </category>
	  <category name="Check Fail" tooltip="Raised when a check has failed during report generation.">
		<setting name="Critical" type="5" value="4" default="true"/>
		<setting name="High" type="5" value="3" default="true"/>
		<setting name="Medium" type="5" value="2" default="true"/>
		<setting name="Low" type="5" value="1" default="true"/>
		<setting name="Informational" type="5" value="0" default="true"/>
	  </category>
  </category>

  <category name="System" tooltip="General system events." expand="true">
	  <setting name="Error" type="8" value="ALL" default="true" tooltip="Events that are raised when an error occurs."/>
  </category>
</TreeViewSettings>
'''
}

FILE_LOGGING_DEFAULT_SETTINGS = {
    'Enable logging to File': 'off',
    'Format to use as output for file logs': 'Plain Text',
    'File Logging Trigger Levels': '''
<TreeViewSettings>
    <category name="Report" tooltip="Events related to the creation and execution of a report." expand="true">
        <category name="Audit Fail" tooltip="Raised when one or more checks in an audit has failed." expand="">
            <setting name="Critical" type="3" value="4" default="true">true</setting>
            <setting name="High" type="3" value="3" default="true">true</setting>
            <setting name="Informational" type="3" value="0" default="true">true</setting>
            <setting name="Low" type="3" value="1" default="true">true</setting>
            <setting name="Medium" type="3" value="2" default="true">true</setting>
        </category>
        <category name="Audit Pass" tooltip="Raised when every check in an audit has passed." expand="">
            <setting name="Critical" type="2" value="4" default="true">true</setting>
            <setting name="High" type="2" value="3" default="true">true</setting>
            <setting name="Informational" type="2" value="0" default="true">true</setting>
            <setting name="Low" type="2" value="1" default="true">true</setting>
            <setting name="Medium" type="2" value="2" default="true">true</setting>
        </category>
        <category name="Check Fail" tooltip="Raised when a check has failed during report generation." expand="">
            <setting name="Critical" type="5" value="4" default="true">true</setting>
            <setting name="High" type="5" value="3" default="true">true</setting>
            <setting name="Informational" type="5" value="0" default="true">true</setting>
            <setting name="Low" type="5" value="1" default="true">true</setting>
            <setting name="Medium" type="5" value="2" default="true">true</setting>
        </category>
        <category name="Check Pass" tooltip="Raised when a check has passed during report generation." expand="">
            <setting name="Critical" type="4" value="4" default="true">true</setting>
            <setting name="High" type="4" value="3" default="true">true</setting>
            <setting name="Informational" type="4" value="0" default="true">true</setting>
            <setting name="Low" type="4" value="1" default="true">true</setting>
            <setting name="Medium" type="4" value="2" default="true">true</setting>
        </category>
        <category name="Generation" tooltip="Events raised when a report is generated." expand="">
            <setting name="Error" type="0" value="ALL" default="true" tooltip="Raised when a report has failed to create and an error is raised. When checked this will catch all error events regardless of log level.">true</setting>
            <setting name="Successful" type="1" value="ALL" default="true" tooltip="Raised when a report is successfully created.When checked this will catch all successful events regardless of log level.">true</setting>
        </category>
    </category>
    <category name="System" tooltip="General system events." expand="true">
        <setting name="Error" type="8" value="ALL" default="true" tooltip="Events that are raised when an error occurs.">true</setting>
    </category>
</TreeViewSettings>''',
}

TCP_LOGGING_DEFAULT_SETTINGS = {
    'Enable logging via TCP protocol': 'off',
    'TCP IP Address': None,
    'TCP Port': '0',
    'Format to use as output for TCP logs': 'Plain Text',
    'TCP Logging Trigger Levels': '''
<TreeViewSettings>
    <category name="Report" tooltip="Events related to the creation and execution of a report." expand="true">
        <category name="Audit Fail" tooltip="Raised when one or more checks in an audit has failed." expand="">
            <setting name="Critical" type="3" value="4" default="true">true</setting>
            <setting name="High" type="3" value="3" default="true">true</setting>
            <setting name="Informational" type="3" value="0" default="true">true</setting>
            <setting name="Low" type="3" value="1" default="true">true</setting>
            <setting name="Medium" type="3" value="2" default="true">true</setting>
        </category>
        <category name="Audit Pass" tooltip="Raised when every check in an audit has passed." expand="">
            <setting name="Critical" type="2" value="4" default="true">true</setting>
            <setting name="High" type="2" value="3" default="true">true</setting>
            <setting name="Informational" type="2" value="0" default="true">true</setting>
            <setting name="Low" type="2" value="1" default="true">true</setting>
            <setting name="Medium" type="2" value="2" default="true">true</setting>
        </category>
        <category name="Check Fail" tooltip="Raised when a check has failed during report generation." expand="">
            <setting name="Critical" type="5" value="4" default="true">true</setting>
            <setting name="High" type="5" value="3" default="true">true</setting>
            <setting name="Informational" type="5" value="0" default="true">true</setting>
            <setting name="Low" type="5" value="1" default="true">true</setting>
            <setting name="Medium" type="5" value="2" default="true">true</setting>
        </category>
        <category name="Check Pass" tooltip="Raised when a check has passed during report generation." expand="">
            <setting name="Critical" type="4" value="4" default="true">true</setting>
            <setting name="High" type="4" value="3" default="true">true</setting>
            <setting name="Informational" type="4" value="0" default="true">true</setting>
            <setting name="Low" type="4" value="1" default="true">true</setting>
            <setting name="Medium" type="4" value="2" default="true">true</setting>
        </category>
        <category name="Generation" tooltip="Events raised when a report is generated." expand="">
            <setting name="Error" type="0" value="ALL" default="true" tooltip="Raised when a report has failed to create and an error is raised. When checked this will catch all error events regardless of log level.">true</setting>
            <setting name="Successful" type="1" value="ALL" default="true" tooltip="Raised when a report is successfully created.When checked this will catch all successful events regardless of log level.">true</setting>
        </category>
    </category>
    <category name="System" tooltip="General system events." expand="true">
        <setting name="Error" type="8" value="ALL" default="true" tooltip="Events that are raised when an error occurs.">true</setting>
    </category>
</TreeViewSettings>'''
}

UDP_LOGGING_DEFAULT_SETTINGS = {
    'Enable logging via UDP protocol': 'off',
    'UDP IP Address': None,
    'UDP Port': '0',
    'Format to use as output for UDP logs': 'Plain Text',
    'UDP Logging Trigger Levels': '''
<TreeViewSettings>
    <category name="Report" tooltip="Events related to the creation and execution of a report." expand="true">
        <category name="Audit Fail" tooltip="Raised when one or more checks in an audit has failed." expand="">
            <setting name="Critical" type="3" value="4" default="true">true</setting>
            <setting name="High" type="3" value="3" default="true">true</setting>
            <setting name="Informational" type="3" value="0" default="true">true</setting>
            <setting name="Low" type="3" value="1" default="true">true</setting>
            <setting name="Medium" type="3" value="2" default="true">true</setting>
        </category>
        <category name="Audit Pass" tooltip="Raised when every check in an audit has passed." expand="">
            <setting name="Critical" type="2" value="4" default="true">true</setting>
            <setting name="High" type="2" value="3" default="true">true</setting>
            <setting name="Informational" type="2" value="0" default="true">true</setting>
            <setting name="Low" type="2" value="1" default="true">true</setting>
            <setting name="Medium" type="2" value="2" default="true">true</setting>
        </category>
        <category name="Check Fail" tooltip="Raised when a check has failed during report generation." expand="">
            <setting name="Critical" type="5" value="4" default="true">true</setting>
            <setting name="High" type="5" value="3" default="true">true</setting>
            <setting name="Informational" type="5" value="0" default="true">true</setting>
            <setting name="Low" type="5" value="1" default="true">true</setting>
            <setting name="Medium" type="5" value="2" default="true">true</setting>
        </category>
        <category name="Check Pass" tooltip="Raised when a check has passed during report generation." expand="">
            <setting name="Critical" type="4" value="4" default="true">true</setting>
            <setting name="High" type="4" value="3" default="true">true</setting>
            <setting name="Informational" type="4" value="0" default="true">true</setting>
            <setting name="Low" type="4" value="1" default="true">true</setting>
            <setting name="Medium" type="4" value="2" default="true">true</setting>
        </category>
        <category name="Generation" tooltip="Events raised when a report is generated." expand="">
            <setting name="Error" type="0" value="ALL" default="true" tooltip="Raised when a report has failed to create and an error is raised. When checked this will catch all error events regardless of log level.">true</setting>
            <setting name="Successful" type="1" value="ALL" default="true" tooltip="Raised when a report is successfully created.When checked this will catch all successful events regardless of log level.">true</setting>
        </category>
    </category>
    <category name="System" tooltip="General system events." expand="true">
        <setting name="Error" type="8" value="ALL" default="true" tooltip="Events that are raised when an error occurs.">true</setting>
    </category>
</TreeViewSettings>'''
}

EMAIL_LOGGING_DEFAULT_SETTINGS = {
    'Enable logging via Email': 'off',
    'Server Username': None,
    'Server Password': None,
    'Sender': None,
    'Recipient': None,
    'Subject': None,
    'Smtp server': None,
    'Smtp port': '587',
    'Encryption': 'TLS',
    'Send email as an attachment': 'off',
    'Format to use as output for Email logs': 'Plain Text',
    'Email Logging Trigger Levels': '''
<TreeViewSettings>
    <category name="Report" tooltip="Events related to the creation and execution of a report." expand="true">
        <category name="Audit Fail" tooltip="Raised when one or more checks in an audit has failed." expand="">
            <setting name="Critical" type="3" value="4" default="true">true</setting>
            <setting name="High" type="3" value="3" default="true">true</setting>
            <setting name="Informational" type="3" value="0" default="true">true</setting>
            <setting name="Low" type="3" value="1" default="true">true</setting>
            <setting name="Medium" type="3" value="2" default="true">true</setting>
        </category>
        <category name="Audit Pass" tooltip="Raised when every check in an audit has passed." expand="">
            <setting name="Critical" type="2" value="4" default="true">true</setting>
            <setting name="High" type="2" value="3" default="true">true</setting>
            <setting name="Informational" type="2" value="0" default="true">true</setting>
            <setting name="Low" type="2" value="1" default="true">true</setting>
            <setting name="Medium" type="2" value="2" default="true">true</setting>
        </category>
        <category name="Check Fail" tooltip="Raised when a check has failed during report generation." expand="">
            <setting name="Critical" type="5" value="4" default="true">true</setting>
            <setting name="High" type="5" value="3" default="true">true</setting>
            <setting name="Informational" type="5" value="0" default="true">true</setting>
            <setting name="Low" type="5" value="1" default="true">true</setting>
            <setting name="Medium" type="5" value="2" default="true">true</setting>
        </category>
        <category name="Check Pass" tooltip="Raised when a check has passed during report generation." expand="">
            <setting name="Critical" type="4" value="4" default="true">true</setting>
            <setting name="High" type="4" value="3" default="true">true</setting>
            <setting name="Informational" type="4" value="0" default="true">true</setting>
            <setting name="Low" type="4" value="1" default="true">true</setting>
            <setting name="Medium" type="4" value="2" default="true">true</setting>
        </category>
        <category name="Generation" tooltip="Events raised when a report is generated." expand="">
            <setting name="Error" type="0" value="ALL" default="true" tooltip="Raised when a report has failed to create and an error is raised. When checked this will catch all error events regardless of log level.">true</setting>
            <setting name="Successful" type="1" value="ALL" default="true" tooltip="Raised when a report is successfully created.When checked this will catch all successful events regardless of log level.">true</setting>
        </category>
    </category>
    <category name="System" tooltip="General system events." expand="true">
        <setting name="Error" type="8" value="ALL" default="true" tooltip="Events that are raised when an error occurs.">true</setting>
    </category>
</TreeViewSettings>'''
}