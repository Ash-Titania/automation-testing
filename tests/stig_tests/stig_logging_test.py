import os
import tempfile
import unittest
import json
from utility.nipper2_interface import *
from utility.temporary_license import TempLicense


class TestSTIGEventLogging(unittest.TestCase):

    def setUp(self):
        self.nipper = NipperInterface()
        self.license = TempLicense()
        self._nipper_report = NipperReportInterface(self.nipper)
        self._nipper_logging = NipperLoggingInterface(self.nipper)
        self._temp_dir_context = tempfile.TemporaryDirectory()
        self._temp_dir = self._temp_dir_context.name
        NipperProfileInterface(self.nipper).reset_default_settings()

    def tearDown(self):
        NipperProfileInterface(self.nipper).reset_default_settings()
        self._temp_dir_context.cleanup()

    def run_demo_stig_with_file_logging(self, log_location, file_format):
        """ Runs a report on a demo ios-router device using Nipper """
        self._nipper_logging.set_file_logging_path(log_location)
        self._nipper_logging.set_file_logging_format(file_format)
        self._nipper_logging.enable_log_file_json_stream(False)
        self._nipper_report.run_demo_stig(os.path.join(self._temp_dir, "{file_format}report.html"))

    def check_for_key_value_json(self, json_object, key, value):
        self.assertIn(key, json_object, f"expected key '{key}' to be in JSON object")
        if value is not None:
            actual_value = json_object[key]
            self.assertEqual(actual_value, value, f"expected value to be {value}, got {actual_value}")

    def json_and_compact_json_validator_stig(self, log_location):
        key_val_dictionary = {"finding_id": None,
                              "version": None,
                              "rule_id": None,
                              "ia_controls": None,
                              "severity": None,
                              "responsibility": None,
                              "stig": None,
                              "title": None,
                              "summary": None,
                              "description": None,
                              "check": None,
                              "fix": None,
                              "findings": None,
                              "audit_type": "STIG Compliance",
                              "date_time": None,
                              "message_level": None,
                              "message_type": None,
                              "manual": None,
                              "product": "Nipper"}
        with open(log_location, encoding="utf-8") as file:
            try:
                json_log_data = json.load(file)
            except json.JSONDecodeError as ex:
                self.fail(ex.msg)
        for json_obj in json_log_data:
            for key, val in key_val_dictionary.items():
                if "passed_count" not in json_obj and "failed_count" not in json_obj and "manual_count" not in json_obj\
                        and "audits" not in json_obj:
                        self.check_for_key_value_json(json_obj, key, val)

    def test_save_types(self):
        test_cases = [
            ('json', '.json'),
            ('compact json', '-cmp.json')
        ]

        """ Run a stig report with file logging turned on """
        self._nipper_logging.enable_file_logging(True)
        for file_format, extension in test_cases:
            with self.subTest(f"{file_format} logging type"):
                log_location = os.path.join(self._temp_dir, f"{file_format}{extension}")
                self.run_demo_stig_with_file_logging(log_location, file_format)
                self.assertTrue(os.path.isfile(log_location), f"{file_format}{extension} file does not exist")
                self.json_and_compact_json_validator_stig(log_location)
