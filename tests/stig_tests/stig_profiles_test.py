import os
import tempfile
import unittest

from utility.nipper2_interface import *
from utility.temporary_license import TempLicense
from resources.stig.stig_data import profile_report_data, stig_profiles
from utility.file_check import file_contains_all_items


class TestSTIGBenchmarks(unittest.TestCase):
    def setUp(self):
        self.nipper = NipperInterface()
        self.license = TempLicense()
        self.nipper_report = NipperReportInterface(self.nipper)
        self.temp_dir_context = tempfile.TemporaryDirectory()
        self.report_location = os.path.join(self.temp_dir_context.name, "report.html")
        NipperProfileInterface(self.nipper).reset_default_settings()
        self.default_profile = "I - Mission Critical Public"

    def tearDown(self):
        self.temp_dir_context.cleanup()

    def test_default_automatic(self):
        """ Test automatic stig profile """
        self.nipper_report.run_demo_stig(self.report_location)
        with open(self.report_location, encoding = "utf8") as report_file:
            self.assertIn(self.default_profile, report_file.read())

    def test_set_specific_STIG(self):
        """ Test specific stig profiles """
        for cli_command, profile_title in stig_profiles.items():
            with self.subTest(profile_title):
                out = self.nipper.run_nipper(f'--stig-audit-def-profile={cli_command}')
                invalid_choice = "An invalid choice was selected"
                self.assertNotIn(invalid_choice, out.stdout)
                self.nipper_report.run_demo_stig(self.report_location)
                self.assertTrue(*file_contains_all_items(self.report_location, profile_report_data))
                with open(self.report_location, encoding = "utf8") as report_file:
                    self.assertIn(profile_title, report_file.read())
