import random
import re
import os
import tempfile
import unittest

from utility.config_paths import get_cisco_demo_path
from utility.nipper2_interface import *
from utility.temporary_license import TempLicense


class TestSTIGBenchmarks(unittest.TestCase):
    def setUp(self):
        self.nipper = NipperInterface()
        self.license = TempLicense()
        self.nipper_report = NipperReportInterface(self.nipper)
        self.temp_dir_context = tempfile.TemporaryDirectory()
        self.report_location = os.path.join(self.temp_dir_context.name, "report.html")
        NipperProfileInterface(self.nipper).reset_default_settings()

        self.demo_file = get_cisco_demo_path()
        self.demo_stig_name = "Infrastructure Router Security Technical Implementation Guide  Cisco"
        self.bench_used_re = "<td>CiscoIOS15</td><td>(.*?Security Technical.*?)</td><td>"
        self.invalid_choice = "An invalid choice was selected"

    def tearDown(self):
        self.temp_dir_context.cleanup()

    def get_stig_benchmarks(self):
        # Matches the help text for the default benchmark setting
        #benchmark_help_reg = re.compile(r"^\s*SETTING:\s*Default\s*Benchmark\n+(?:(?:.*\n){2})((?:[^:]+\n)*)", re.M)
        benchmark_help_reg = re.compile(str(b"\s*SETTING:\s*[\\x1b[0m]*Default\s*Benchmark\n+(?:(?:.*\n){2})((?:[^:]+\n)*)", encoding='utf-8'), re.M)
        benchmark_help = re.search(benchmark_help_reg, self.nipper.run_nipper("--help=report12").stdout).group(1)
        # Matches each stig title in the help text
        stig_title_reg = re.compile(r"^\s*(\"[^\[]+?\")\s*\[\s*(.+?)\s*\]$", re.M)
        return [[re.search(stig_title_reg, bench).group(1), re.search(stig_title_reg, bench).group(2)] for bench in
                benchmark_help.splitlines()]

    def test_default_automatic(self):
        """ Test automatic stig selection """
        self.nipper_report.run_demo_stig(self.report_location)
        with open(self.report_location, encoding = "utf8") as report_file:
            self.assertEqual(re.compile(self.bench_used_re).search(report_file.read()).group(1), self.demo_stig_name)

    def test_set_specific_STIG(self):
        """ Test specific stig selection """
        benchmarks = self.get_stig_benchmarks()
        for cli_command, title in benchmarks:
            cli_command = cli_command.strip('\"')
            with self.subTest(title):
                out = self.nipper.run_nipper(f"--stig-audit-def-benchmark={cli_command}")
                self.assertNotIn(self.invalid_choice, out.stdout)
                self.nipper_report.run_demo_stig(self.report_location)
                with open(self.report_location, encoding="utf8") as report_file:
                    self.assertIn(title, report_file.read())

    def test_set_to_none(self):
        """ Test setting benchmark back to none"""
        benchmarks = self.get_stig_benchmarks()
        random_bench = random.randrange(len(benchmarks))
        bench = benchmarks[random_bench][0].strip('\"')
        self.assertNotIn(self.invalid_choice, self.nipper.run_nipper(f'--stig-audit-def-benchmark={bench}').stdout)
        self.assertNotIn(self.invalid_choice, self.nipper.run_nipper('--stig-audit-def-benchmark=None').stdout)
        self.nipper_report.run_demo_stig(self.report_location)

        with open(self.report_location, encoding="utf8") as report_file:
            self.assertEqual(re.compile(self.bench_used_re).search(report_file.read()).group(1), self.demo_stig_name)
