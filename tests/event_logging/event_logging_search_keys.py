vuln_log_keys_ascii = ["advisories", "audit_type", "cpe", "device", "collection_ip",
                       "filename", "hostname", "manufacturer", "model", "operating_system", "operation_type", "name", "version",
                       "finding_id", "rating", "CVSSv2 Base", "CVSSv2 Environmental", "CVSSv2 Overall", "CVSSv2 Score",
                       "CVSSv2 Temporal", "Published_Date", "references", "summary", "audits", "devices"]

vuln_log_keys_json = ["advisories", "source", "url", "audit_type", "cpe", "date_time", "device",
                      "collection_ip", "filename", "hostname", "manufacturer", "model", "operating_system", "operation_type",
                      "name", "version", "finding_id", "message_level", "message_type", "product", "rating",
                      "CVSSv2 Base", "CVSSv2 Environmental", "CVSSv2 Overall", "CVSSv2 Score", "CVSSv2 Temporal",
                      "Published_Date", "references", "summary", "audits", "devices", "product"]

vuln_log_keys_gelf = ["advisories", "audit_type", "cpe", "device_collection_ip",
                      "device_filename", "device_hostname", "device_manufacturer", "device_model",
                      "device_operating_system_name", "device_operating_system_version", "device_operation_type", "finding_id", "Product",
                      "references", "summary", "audits", "devices", "DateTime", "MessageLevel", "MessageType",
                      "rating_CVSSv2 Base", "rating_CVSSv2 Environmental", "rating_CVSSv2 Overall",
                      "rating_CVSSv2 Score", "rating_CVSSv2 Temporal", "rating_Published_Date", "full_message", "host",
                      "short_message", "version"]

