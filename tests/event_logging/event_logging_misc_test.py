import unittest
import tempfile
import os

from utility.nipper2_interface import NipperInterface
from utility.nipper2_interface import NipperLoggingInterface
from utility.nipper2_interface import NipperReportInterface
from utility.nipper2_interface import NipperProfileInterface
from utility.config_paths import get_cisco_demo_path
from utility.temporary_license import TempLicense


class TestLoggingEol(unittest.TestCase):
    def setUp(self):
        nipper = NipperInterface()
        self.license = TempLicense()
        self._nipper_logging = NipperLoggingInterface(nipper)
        self._nipper_report = NipperReportInterface(nipper)
        self._temp_dir_context = tempfile.TemporaryDirectory()
        self._temp_dir = self._temp_dir_context.name
        NipperProfileInterface(NipperInterface()).reset_default_settings()
        self._nipper_logging.enable_file_logging(True)
        self._log_location = f"{self._temp_dir}/log_file.cef"
        self._nipper_logging.set_file_logging_path(self._log_location)
        self._nipper_logging.set_file_logging_format('cef')
        self._test_cases = [
            ('Windows (CR LF)', "\r\n"),
            ('Unix (LF)', "\n"),
            ('none', "")
        ]

    def tearDown(self):
        self._temp_dir_context.cleanup()

    def determine_line_endings(self, file_path):
        with open(file_path, newline = '') as file:
            contents = file.read()
            return [case[0] for case in self._test_cases if case[1] in contents][0]

    def test_logging_eol(self):
        for eol_format, eol in self._test_cases:
            with self.subTest(f"{eol_format} line endings test"):
                self._nipper_report.generate_report(get_cisco_demo_path(), f"{self._temp_dir}/report.html",
                                                    f'--log-file-eol={eol_format}')
                self.assertTrue(os.path.isfile(self._log_location), f'{eol_format} file does not exist')
                try:
                    line_endings = self.determine_line_endings(self._log_location)
                    self.assertTrue(line_endings is eol_format,
                                    f'{eol_format} file contains incorrect line endings, {line_endings} was found')
                finally:
                    os.remove(self._log_location)
