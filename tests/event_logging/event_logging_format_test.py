import unittest
import tempfile
import os
import json
import re
import time

from utility.nipper2_interface import NipperInterface
from utility.nipper2_interface import NipperLoggingInterface
from utility.nipper2_interface import NipperReportInterface
from utility.nipper2_interface import NipperProfileInterface
from utility.config_paths import get_cisco_demo_path
from utility.temporary_license import TempLicense


class TestLoggingOutput(unittest.TestCase):
    def setUp(self):
        self.nipper = NipperInterface()
        self.license = TempLicense()
        self._nipper_logging = NipperLoggingInterface(self.nipper)
        self._nipper_report = NipperReportInterface(self.nipper)
        self._temp_dir_context = tempfile.TemporaryDirectory()
        self._temp_dir = self._temp_dir_context.name
        NipperProfileInterface(NipperInterface()).reset_default_settings()
        self._nipper_logging.enable_file_logging(True)

    def tearDown(self):
        self._temp_dir_context.cleanup()

    def run_report(self, log_location, file_format, eol_format):
        """ Runs a report on a demo ios-router device using Nipper """
        return_out = self._nipper_logging.set_file_logging_path(log_location).stdout
        return_out += self._nipper_logging.set_file_logging_format(file_format).stdout
        if eol_format is not None:
            return_out += self._nipper_logging.set_cjson_eol_logging_format(eol_format).stdout
        return_out += self._nipper_report.generate_report(get_cisco_demo_path(), f"{self._temp_dir}/report.html",
                                                          "--ios-router").stdout
        return return_out

    def plain_validator(self, file):
        """ Check that the file only contains valid ascii characters """
        self.assertTrue(file.read().isascii(), 'File contains non-ascii characters')
        file.seek(0)

    def json_validator(self, file):
        """
        Checks if the braces are within the json log file and validates the JSON if they are
        Also covers compact json
        """
        file_content = file.read().rstrip()
        self.assertTrue(file_content.startswith("["), f"Should start with a '[.' Found a {file_content[0]}")
        self.assertTrue(file_content.endswith("]"), f"Should end with a '].' Found a {file_content[-1]}")
        try:
            json_file = json.loads(file_content)
        except json.JSONDecodeError as e:
            self.fail(e.msg)
        finally:
            file.seek(0)
        return json_file

    def json_stream_validator(self, file):
        """ Check that each json object is valid json """
        self.plain_validator(file)
        json_object = ''
        brace_count = 0
        json_list = []
        for char in file.read():
            brace_count += char.count('{')
            brace_count -= char.count('}')
            json_object += char
            if brace_count == 0 and len(json_object) > 1:
                try:
                    json_list.append(json.loads(json_object))
                    json_object = ''
                except json.JSONDecodeError as ex:
                    self.fail(ex.msg)
        file.seek(0)
        return json_list

    def compact_json_stream_validator(self, file):
        """ Check that each line is valid JSON"""
        self.plain_validator(file)
        for line in file:
            try:
                json.loads(line)
            except json.JSONDecodeError as e:
                self.fail(e.msg)
        file.seek(0)

    def gelf_validator(self, file):
        """ Check that each gelf object is valid json and has a valid title, or starts with '_' """
        json_list = []
        message = ""
        for line in file:
            message += line
            if line == "}\n":
                try:
                    json_obj = json.loads(message)
                    json_list.append(json_obj)
                except json.JSONDecodeError as e:
                    self.fail(e.msg)
                finally:
                    message = ""
        valid_titles = [
            'version', 'host', 'short_message', 'full_message', 'timestamp', 'level', 'facility', 'line', 'file'
        ]
        for json_obj in json_list:
            for failed_key in [key for key in json_obj if key not in valid_titles and not key.startswith('_')]:
                self.fail(f'Title is not a valid gelf title: {failed_key}')
            file.seek(0)

    def cef_validator(self, file):
        """ Check that the file is valid cef """
        self.plain_validator(file)
        cef_regex = re.compile(R'^CEF:\d+(\|[^|\n\r]+){6}.*$')
        for failed_line in [line for line in file if not cef_regex.match(line)]:
            self.fail(f'Line does not match cef regex: {failed_line}')
        file.seek(0)
    """
    For unstreamed json log files, this will wait for the log file size to not be 0 before proceeding to 
    check it's validity
    :param filename: sets a filename to search for
    :param file_to_check: sets a file whose size will be checked
    :var timeout: sets a timeout timer where the test will fail provided that filesize is still zero.
    :var time_in_seconds: refers to the time, in seconds, that should be added to the system time (at time of running
    method) to act as a timeout
    """
    def filename_check(self, file, filename, file_to_check):
        time_in_seconds = 30
        if file == filename:
            timeout = time.time() + time_in_seconds
            while time.time() < timeout:
                while os.stat(file_to_check).st_size == 0:
                    time.sleep(1)
                    if os.stat(file_to_check).st_size != 0:
                        break

    def test_logging_formats(self):
        """ Test each logging format """
        test_cases = [
            ('plain', '.txt', self.plain_validator, None, True),
            ('json', '.json', self.json_stream_validator, None, True),
            ('gelf', '.gelf', self.gelf_validator, None, True),
            ('cef', '.cef', self.cef_validator, "windows (cr lf)", True),
            ('compact json', '-cmp.json', self.json_stream_validator, "none", True),
            ('compact json', '-delim.json', self.compact_json_stream_validator, "windows (cr lf)", True),
            ('json', '.json', self.json_validator, "none", False),
            ('compact json', '-cmp.json', self.json_validator, "none", False)
        ]
        for file_format, extension, validator, eol_format, stream_output in test_cases:
            with self.subTest(f"{extension} logging. Stream={stream_output}. Eol={eol_format}"):
                log_location = f"{self._temp_dir}/log-file{extension}"
                if os.path.exists(log_location):
                    os.remove(log_location)
                if "json" in file_format:
                    self._nipper_logging.enable_log_file_json_stream(stream_output)
                self.run_report(log_location, file_format, eol_format)
                if not stream_output:
                    for path, subdirs, files in os.walk(tempfile.gettempdir()):
                        for file in files:
                            self.filename_check(file, "log-file", "log-file.json")
                            self.filename_check(file, "log-file-cmp", "log-file-cmp.json")

                self.assertTrue(os.path.isfile(log_location), f"{file_format} file does not exist")
                with open(log_location) as file:
                    validator(file)
