import unittest
import tempfile
import os

from utility.nipper2_interface import NipperInterface, NipperLoggingInterface, NipperReportInterface, \
    NipperProfileInterface
from utility.config_paths import get_cisco_demo_path
from utility.temporary_license import TempLicense


class TestConfigFilenameInLog(unittest.TestCase):
    def setUp(self):
        nipper = NipperInterface()
        self.license = TempLicense()
        self._nipper_logging = NipperLoggingInterface(nipper)
        self._nipper_report = NipperReportInterface(nipper)
        self._temp_dir_context = tempfile.TemporaryDirectory()
        self._temp_dir = self._temp_dir_context.name
        NipperProfileInterface(NipperInterface()).reset_default_settings()
        self._nipper_logging.enable_file_logging(True)

    def tearDown(self):
        self._temp_dir_context.cleanup()

    def create_log(self, log_location, file_format):
        """ Runs a report on a demo ios-router device using Nipper """
        self._nipper_logging.set_file_logging_path(log_location)
        self._nipper_logging.set_file_logging_format(file_format)
        self._nipper_report.generate_report(get_cisco_demo_path(), f"{self._temp_dir}/report.html", "--ios-router")

    def create_network_log(self, log_location, file_format):
        """ Runs a report against a Cisco Nexus on the network """
        self._nipper_logging.set_file_logging_path(log_location)
        self._nipper_logging.set_file_logging_format(file_format)
        self._nipper_report.generate_remote_device_report("10.200.8.195", "--cisco-nexus", "telnet", "23", "admin",
                                                          "password", "password", f"{self._temp_dir}/remote.html")

    def test_filename_in_log(self):
        """ Creates a report on the Demo IOS 15 + a JSON Log, checks that the file name is in the log """
        extension = "json"
        log_location = f"{self._temp_dir}/log-file.{extension}"
        self.create_log(log_location, extension)
        self.assertTrue(os.path.isfile(log_location))
        with open(log_location) as file:
            self.assertIn('"filename": "cisco-router-ios15.txt"', file.read())

    def test_filename_in_network_log(self):
        """ Creates a report using a network device + JSON Log, checks that no filename is in the log """
        extension = "json"
        log_location = f"{self._temp_dir}/log-file.{extension}" #Change to trigger a pipeline build
        self.create_network_log(log_location, extension)

        print(f"{log_location}")
        os.path.isfile(log_location)

        try:
            st = os.stat(log_location)
        except OSError:
            print("OSError")
            return False

        res = os.path.isfile(log_location)
        self.assertTrue(os.path.isfile(log_location))
        with open(log_location) as file:

            print("File opened")
            print(f" filename= {file.name}, file readable {file.readable()}")
            created_log = file.read()
            print(f"file read")
            print(f"{created_log}")
            self.assertIn('"collection_ip": "10.200.8.195"', created_log)
            print("self.assertIn('collection_ip: 10.200.8.195', created_log)")
            self.assertIn('"filename": ""', created_log)
            print("self.assertIn('filename: ', created_log)")
