import unittest
import tempfile
import re
import os

from utility.nipper2_interface import NipperInterface, NipperLoggingInterface, NipperReportInterface, \
    NipperProfileInterface
from utility.temporary_license import TempLicense
from utility.config_paths import get_cisco_demo_path
from tests.event_logging.event_logging_search_keys import vuln_log_keys_ascii, vuln_log_keys_gelf, vuln_log_keys_json


class TestLoggingContent(unittest.TestCase):
    def setUp(self):
        self.nipper = NipperInterface()
        self.license = TempLicense()
        self._nipper_logging = NipperLoggingInterface(self.nipper)
        self._nipper_report = NipperReportInterface(self.nipper)
        self._temp_dir_context = tempfile.TemporaryDirectory()
        self._temp_dir = self._temp_dir_context.name
        NipperProfileInterface(NipperInterface()).reset_default_settings()

    def tearDown(self):
        self._temp_dir_context.cleanup()

    def test_vuln_report_content(self):
        """Test content of each logging format for a Vulnerability Report"""

        test_cases = [
            ("plain", ".txt", vuln_log_keys_ascii, R"\s*([A-z\d\s]+):\s"),
            ("json", ".json", vuln_log_keys_json, R'"([A-z\d\s]+)":'),
            ("gelf", ".gelf", vuln_log_keys_gelf, R'"_*([A-z\d\s]+)":'),
            ("cef", ".cef", vuln_log_keys_gelf, R'"_*([A-z\d\s]+)":'),
            ("compact json", "-cmp.json", vuln_log_keys_json, R'"([A-z\d\s]+)":')
        ]

        for logging_type, file_suffix, search_keys, regex in test_cases:
            with self.subTest(f"{logging_type} content test"):
                log_loc = os.path.join(self._temp_dir, f"log{file_suffix}")

                self._nipper_logging.set_file_logging_path(log_loc)
                self._nipper_logging.set_file_logging_format(logging_type)

                self._nipper_report.all_reports_status_off(True)
                self._nipper_report.enable_single_report_type('vulnaudit', True)

                self._nipper_report.generate_report(get_cisco_demo_path(), os.path.join(self._temp_dir, "tempOut.html"))

                with open(log_loc) as file:
                    for line in file:
                        if line != '\n':
                            match = re.findall(regex, line)
                            if match:
                                line_key = match[0]
                                self.assertIn(line_key, search_keys)
