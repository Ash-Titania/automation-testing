""" Automation tests for switching between licenses, within Nipper
    -Known working license
    -Empty serial number
    -Empty activation code
    -Bad license credentials
"""
import re
import unittest
from utility.nipper2_interface import NipperLicenseInterface
from utility.nipper2_interface import NipperInterface
from resources.licenses.license_keys import *


class TestSwitchLicense(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestSwitchLicense, self).__init__(*args, **kwargs)
        self.nipper_licensing = NipperLicenseInterface(NipperInterface())
        self.switch_license_success = "Successful"
        self.switch_license_failed = "Failed"
        self.switch_license_not_found = "License not found"
        self.serial_number_capture_regex = "Serial\\s*:\\s*(\\d+)"
        self.current_license_status = "Current (Active)"

    def get_current_active_license_serial(self):
        """ Extracts the serial number of the license marked current and active
        from the list of licenses produced by --show-licenses
        """
        show_output = self.nipper_licensing.show_licenses().stdout
        serial_regex = re.compile(self.serial_number_capture_regex)
        captured_serial = ""

        for line in show_output.splitlines():
            search_result = serial_regex.search(line)
            if search_result:
                captured_serial = search_result.group(1)  # first captured group

            if self.current_license_status in line:
                return captured_serial

    def test_switch_license_success(self):
        """Successfully switch the current license of Nipper from the most recently added home license,
        to the automation test enterprise license.
        """
        self.nipper_licensing.add_license(ENTERPRISE_LICENSE_SERIAL, ENTERPRISE_LICENSE_ACTIVATION)
        self.nipper_licensing.add_license(HOME_LICENSE_SERIAL, HOME_LICENSE_ACTIVATION)
        switch_output = self.nipper_licensing.switch_license(ENTERPRISE_LICENSE_SERIAL).stdout
        current_license_serial = self.get_current_active_license_serial()
        try:
            self.assertIn(self.switch_license_success, switch_output, "Success message must be in output")
            self.assertEqual(current_license_serial, ENTERPRISE_LICENSE_SERIAL, "Current license serial must be the "
                                                                                "first license added")
        finally:
            self.nipper_licensing.remove_license(ENTERPRISE_LICENSE_SERIAL)
            self.nipper_licensing.remove_license(HOME_LICENSE_SERIAL)

    def test_switch_license_failures(self):
        """Fail to switch to a license with inadequate credentials"""

        # Test action, serial, activation, expected output
        test_cases = [
            ("Switching to Empty Serial", "", ENTERPRISE_LICENSE_ACTIVATION, self.switch_license_not_found),
            ("Switching to Empty Activation", ENTERPRISE_LICENSE_SERIAL, "", self.switch_license_not_found),
            ("Switching to Bad", BAD_LICENSE_SERIAL, BAD_LICENSE_ACTIVATION, self.switch_license_not_found)
        ]
        for test_action, serial, activation, expected_output in test_cases:
            with self.subTest(f"'{test_action} license' expects '{expected_output}'"):
                self.nipper_licensing.add_license(serial, activation)
                self.nipper_licensing.add_license(HOME_LICENSE_SERIAL, HOME_LICENSE_ACTIVATION)
                output = self.nipper_licensing.switch_license(serial).stdout
                self.assertIn(expected_output, output,  f"'{expected_output}' must be in output")