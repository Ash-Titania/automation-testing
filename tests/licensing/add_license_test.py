""" Automation tests for adding license to Nipper.
    -Each license type.
    -Empty serial
    -Empty activation
    -Bad license credentials
"""
import unittest
from utility.nipper2_interface import NipperLicenseInterface
from utility.nipper2_interface import NipperInterface
from resources.licenses.license_keys import *


class TestAddLicense(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestAddLicense, self).__init__(*args, **kwargs)
        self.nipper_licensing = NipperLicenseInterface(NipperInterface())
        self.add_license_success = "Successful"
        self.add_license_invalid = "The license is invalid"
        self.empty_serial = "not provided a --serial=<SerialCode>"
        self.empty_activation = "not provided a --activation=<ActivationCode>"
        self.success_in_msg = "Success string must be in output"

    def test_license_addition(self):
        """Subtests the adding of each license type"""

        # Test action, Serial, activation, expected output
        test_cases = [
            ("Adding Enterprise", ENTERPRISE_LICENSE_SERIAL, ENTERPRISE_LICENSE_ACTIVATION, self.add_license_success),
            ("Adding Home", HOME_LICENSE_SERIAL, HOME_LICENSE_ACTIVATION, self.add_license_success),
            ("Adding OEM", OEM_LICENSE_SERIAL, OEM_LICENSE_ACTIVATION, self.add_license_success),
            ("Adding Auditor", AUDITOR_LICENSE_SERIAL, AUDITOR_LICENSE_ACTIVATION, self.add_license_success),
            ("Adding Training", TRAINING_LICENSE_SERIAL, TRAINING_LICENSE_ACTIVATION, self.add_license_success),
            ("Adding Bad", BAD_LICENSE_SERIAL, BAD_LICENSE_ACTIVATION, self.add_license_invalid),
            ("Adding Empty Serial", "", ENTERPRISE_LICENSE_ACTIVATION, self.empty_serial),
            ("Adding Empty Activation", ENTERPRISE_LICENSE_SERIAL, "", self.empty_activation)
        ]
        for test_action, serial, activation, expected_output in test_cases:
            with self.subTest(f"'{test_action} license' expects '{expected_output}'"):
                output = self.nipper_licensing.add_license(serial, activation).stdout
                try:
                    self.assertIn(expected_output, output, f"'{expected_output}' must be in output")
                finally:
                    self.nipper_licensing.remove_license(serial)