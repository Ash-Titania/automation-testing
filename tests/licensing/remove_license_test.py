""" Automation tests for removing licenses from Nipper.
    -Each license type.
    -Empty serial
    -Empty activation
    -Bad license credentials
"""
import unittest
from utility.nipper2_interface import NipperLicenseInterface
from utility.nipper2_interface import NipperInterface
from resources.licenses.license_keys import *


class TestRemoveLicense(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestRemoveLicense, self).__init__(*args, **kwargs)
        self.nipper_licensing = NipperLicenseInterface(NipperInterface())
        self.remove_license_success = "Successful"
        self.remove_license_not_found = "License not found"

    def test_license_removal(self):
        """Subtests the removal of each license type"""

        # Test action, serial, activation, expected output
        test_cases = [
            ("Removing Enterprise", ENTERPRISE_LICENSE_SERIAL, ENTERPRISE_LICENSE_ACTIVATION,
             self.remove_license_success),
            ("Removing Home", HOME_LICENSE_SERIAL, HOME_LICENSE_ACTIVATION, self.remove_license_success),
            ("Removing OEM", OEM_LICENSE_SERIAL, OEM_LICENSE_ACTIVATION, self.remove_license_success),
            ("Removing Auditor", AUDITOR_LICENSE_SERIAL, AUDITOR_LICENSE_ACTIVATION, self.remove_license_success),
            ("Removing Training", TRAINING_LICENSE_SERIAL, TRAINING_LICENSE_ACTIVATION, self.remove_license_success),
            ("Removing Bad", BAD_LICENSE_SERIAL, BAD_LICENSE_ACTIVATION, self.remove_license_not_found),
            ("Removing Empty Serial", "", ENTERPRISE_LICENSE_ACTIVATION, self.remove_license_not_found),
            ("Removing Empty Activation", ENTERPRISE_LICENSE_SERIAL, "", self.remove_license_not_found)
        ]
        for test_action, serial, activation, expected_output in test_cases:
            with self.subTest(f"'{test_action} license' expects '{expected_output}'"):
                self.nipper_licensing.add_license(serial, activation)
                output = self.nipper_licensing.remove_license(serial).stdout
                self.assertIn(expected_output, output, f"'{expected_output}' must be in output")