from resources.licenses.license_keys import TINY_ENTERPRISE_ACTIVATION, TINY_ENTERPRISE_SERIAL
from utility.nipper2_interface import *
from utility.nipper2_interface.nipper_license_interface import nuke_registry
from utility.config_paths import *
import unittest
import re
import tempfile
import os


class TestLicenseUsage(unittest.TestCase):
    """ Tests license usage limits """
    def setUp(self):
        self.temp_dir_context = tempfile.TemporaryDirectory()
        self.temp_dir = self.temp_dir_context.name
        nuke_registry()
        nipper = NipperInterface()
        self.nipper_licensing = NipperLicenseInterface(nipper)
        self.nipper_report = NipperReportInterface(nipper)
        NipperMaintenanceInterface(nipper).set_update_interval("never")
        self.usage_regex = re.compile(r'Usage\s*:\s*(\d+) out of (\d+)')

    def tearDown(self):
        self.temp_dir_context.cleanup()

    def test_add_single_usage(self):
        """ Running a new config should increase the usage count """
        starting_usage = self.add_tiny_license()
        self.assertEqual(starting_usage, 0)
        output_file = os.path.join(self.temp_dir, 'report.html')
        input_file = get_test_cisco_path()
        self.nipper_report.generate_report(input_file, output_file)
        self.assertTrue(os.path.exists(output_file), 'Report not found - ' + output_file)
        end_usage = self.get_usages()
        self.assertEqual(end_usage, starting_usage + 1)

    def test_usage_limit(self):
        """
        Once the usage limit has been reached, new configs should not be able to be used,
        old configs should run fine, as should demo configs
        """
        output_template = os.path.join(self.temp_dir, 'report%d.html')

        self.assertEqual(self.add_tiny_license(), 0)

        self.nipper_report.generate_report(get_test_cisco_path(), output_template % 1)
        self.assertTrue(os.path.exists(output_template % 1), 'Report 1 not found - ' + output_template % 1)
        current_usage = self.get_usages()

        self.nipper_report.generate_report(get_cisco_demo_path(), output_template % 2)
        self.assertTrue(os.path.exists(output_template % 2), 'Report 2 not found - ' + output_template % 2)
        self.assertEqual(current_usage, self.get_usages())

        self.nipper_report.generate_report(get_test_3com_path(), output_template % 3)
        self.assertTrue(os.path.exists(output_template % 3), 'Report 3 not found - ' + output_template % 3)
        current_usage = self.get_usages()
        self.assertEqual(current_usage, self.use_limit)

        # Usage limit should be reached
        limit_exceeded = self.nipper_report.generate_report(get_test_brocade_path(), output_template % 4).stdout
        self.assertFalse(os.path.exists(output_template % 4), 'Usage limit exceeded')
        self.assertIn("There are not enough device licenses remaining", limit_exceeded)
        current_usage = self.get_usages()
        self.assertEqual(current_usage, self.use_limit)

        # Previously run config should still run
        self.nipper_report.generate_report(get_test_cisco_path(), output_template % 5)
        self.assertTrue(os.path.exists(output_template % 5), 'Report 5 not found - ' + output_template % 5)
        current_usage = self.get_usages()
        self.assertEqual(current_usage, self.use_limit)

        # Demo files should still run
        self.nipper_report.generate_report(get_cisco_demo_path(), output_template % 6)
        self.assertTrue(os.path.exists(output_template % 6), 'Report 6 not found - ' + output_template % 6)
        current_usage = self.get_usages()
        self.assertEqual(current_usage, self.use_limit)

    def test_no_license_config(self):
        """ If there is no current license, configs should not be able to be run """
        shown_license = self.nipper_licensing.show_licenses().stdout
        self.assertIn('Nipper does not currently have any licenses', shown_license)
        input_file = get_test_cisco_path()
        output_file = os.path.join(self.temp_dir, 'report.html')
        no_license = self.nipper_report.generate_report(input_file, output_file).stdout
        self.assertIn("There is no active license", no_license)
        self.assertFalse(os.path.exists(output_file), 'Report was found')

    @unittest.skip("NIPPER-4216")
    def test_no_license_demo(self):
        """ If there is no current license, demo files should still run"""
        shown_license = self.nipper_licensing.show_licenses().stdout
        self.assertIn('Nipper does not currently have any licenses', shown_license)
        output_file = os.path.join(self.temp_dir, 'report.html')
        self.nipper_report.generate_report(get_cisco_demo_path(), output_file)
        self.assertTrue(os.path.exists(output_file), 'demo report not found - ' + output_file)

    def add_tiny_license(self) -> int:
        """ Add a 2 device license and return the current usage on it """
        self.nipper_licensing.add_license(TINY_ENTERPRISE_SERIAL, TINY_ENTERPRISE_ACTIVATION)
        return self.get_usages()

    def get_usages(self) -> int:
        """ Return the current usage on the license """
        shown_license = self.nipper_licensing.show_licenses().stdout
        self.use_limit = int(self.usage_regex.search(shown_license).group(2))
        return int(self.usage_regex.search(shown_license).group(1))
