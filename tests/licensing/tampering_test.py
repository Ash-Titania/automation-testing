""" Automation tests for license tampering in Nipper """
import unittest
import tempfile
import winreg
import os
from utility.nipper2_interface import NipperReportInterface
from utility.nipper2_interface.nipper_license_interface import NipperLicenseInterface, nuke_registry
from utility.nipper2_interface import NipperInterface
from resources.licenses.license_keys import *
from utility.config_paths import get_test_cisco_path


class TestLicenseTampering(unittest.TestCase):
    """ Test that Nipper fails to audit after tampering which
    each of a pre-defined set of license registry key values """

    def __init__(self, *args, **kwargs):
        super(TestLicenseTampering, self).__init__(*args, **kwargs)
        nipper_interface = NipperInterface()
        self.nipper_licensing = NipperLicenseInterface(nipper_interface)
        self.nipper_reporting = NipperReportInterface(nipper_interface)

        self.temp_dir = tempfile.TemporaryDirectory()
        self.temp_report_path = os.path.join(self.temp_dir.name, "tamperReport")
        self.suffix = '.html'
        self.license_path = R"Software\\Titania\\NipperStudio LM3\\L\\1"

    def tearDown(self):
        self.temp_dir.cleanup()
        nuke_registry()
        self.nipper_licensing.add_license(ENTERPRISE_LICENSE_SERIAL, ENTERPRISE_LICENSE_ACTIVATION)

    def test_license_tamper(self):
        # Start with a clean registry slate.
        nuke_registry()

        license_reg_entries = [("01", winreg.REG_SZ, "serial", "99999999"),
                               ("02", winreg.REG_SZ, "activation", "ABCDEF-123456-GHIJKL-789000"),
                               ("06", winreg.REG_DWORD, "type", 5),
                               ("07", winreg.REG_DWORD, "status", 3),
                               ("08", winreg.REG_DWORD, "duration months", 500),
                               ("09", winreg.REG_DWORD, "usage limit", 10001),
                               ("10", winreg.REG_SZ, "owner name", "Tampered With"),
                               ("11", winreg.REG_SZ, "owner email", "tampered@tamper.com")]

        self.nipper_licensing.add_license(ENTERPRISE_LICENSE_SERIAL, ENTERPRISE_LICENSE_ACTIVATION)

        for reg_key, reg_type, license_var, tamper_val in license_reg_entries:
            with self.subTest(f"Nipper must not audit successfully when '{license_var}' is tampered with"):

                key_to_write = winreg.OpenKey(winreg.HKEY_CURRENT_USER, self.license_path, 0, winreg.KEY_WRITE)
                winreg.SetValueEx(key_to_write, reg_key, 0, reg_type, tamper_val)
                winreg.CloseKey(key_to_write)

                unique_test_filename = self.temp_report_path + reg_key + self.suffix
                nipper_subprocess = self.nipper_reporting.generate_report(get_test_cisco_path(), unique_test_filename)

                try:
                    self.assertFalse(os.path.isfile(unique_test_filename))
                    self.assertFalse(os.path.exists(unique_test_filename))
                    self.assertIn("Run-time process manipulation detected", nipper_subprocess.stdout)
                finally:
                    nuke_registry()
                    self.nipper_licensing.add_license(ENTERPRISE_LICENSE_SERIAL, ENTERPRISE_LICENSE_ACTIVATION)

