import unittest
from utility.rpm_handler import RPMHandler
from utility.nipper2_interface import NipperInterface
from utility.os import LinuxDistribution


class TestInstallAndRun(unittest.TestCase):

    def test_install_and_run_rpm(self):
        """
        Checks if OS is centos or red hat, then tries to install latest Nipper rpm
        """

        distro = LinuxDistribution.get_distribution()

        if "centos linux" in distro or "red hat enterprise linux client" in distro:

            rpm = RPMHandler()

            self.assertTrue(rpm.install(), "Install should succeed")

            nipper = NipperInterface()
            self.assertEqual(nipper.run_nipper("--help").returncode, 0)

        else:
            self.skipTest("Non-compatible OS")
