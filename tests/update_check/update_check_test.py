import unittest
from utility.nipper2_interface import nipper_interface


class TestUpdateCheck(unittest.TestCase):

    def test_update_check_when_no_update_available(self):
        output = nipper_interface.NipperInterface().run_nipper("--update-check")
        expected_output_up_to_date = "You are currently running the latest version of software and all resources."
        expected_output_update = "UPDATE 1: Nipper"
        actual_output = output.stdout
        self.assertTrue(expected_output_up_to_date in actual_output or expected_output_update in actual_output)
