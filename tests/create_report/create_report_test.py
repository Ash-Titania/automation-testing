import os.path
import tempfile
import unittest

from resources.create_report.create_report_data import *
from utility.nipper2_interface import NipperInterface
from utility.file_check import file_contains_all_items
from utility.nipper2_interface.nipper_report_interface import NipperReportInterface
from utility.temporary_license import TempLicense
from utility.network_ping import skip_if_device_not_reachable


class TestReportGeneration(unittest.TestCase):

    def setUp(self):
        """
        this method sets the environment for the test to perform, adding license setting nipper to default state,
        creating temp directory, creating report path and name and variable report_order which is used in two different
        tests.
        :return: environment suitable to run the test.
        """
        self._nipper = NipperInterface()
        self.license = TempLicense()
        self._nipper_report_generation = NipperReportInterface(self._nipper)
        self._nipper_report_generation.report_default_settings()
        self._temp_dir_context = tempfile.TemporaryDirectory()
        self._temp_dir = self._temp_dir_context.name
        self._report_location = os.path.join(self._temp_dir, "report.txt")
        self.report_order = ['2 Security Audit', '3 Vulnerability Audit', '4 Cisco PSIRT audit', '5 CIS Benchmark',
                             '6 DISA STIG Compliance',
                             '7 SANS Policy Compliance', '8 Filtering Complexity Report', '9 Configuration Report',
                             '10 Raw Configuration', '11 Raw Configuration Changes', '12 Filtering Differences Report',
                             '13 Appendix']

    def tearDown(self):
        """
        this method cleans the temp directory after each test for re-usability and tidy up.
        :return:
        """
        self._temp_dir_context.cleanup()

    def test_add_device(self):
        """
        this test will add a single device config file and generate a report with default settings and verify the file
        exist in the path given
        :return: pass or fail according to check
        """
        self._nipper_report_generation.generate_report(cisco_nexus_config, self._report_location, '--text')
        self.assertTrue(os.path.exists(self._report_location))
        self.assertTrue(*file_contains_all_items(self._report_location, ['Cisco Nexus', 'Nexus5010', 'Security Audit Summary',
                                                              'Vulnerability Audit Summary', 'Configuration Report',
                                                              'Appendix']))

    def test_add_device_directory(self):
        """
        this test will add a directory with multiple config files and generate a report using default report settings
         and verify the report existence
        :return: pass or fail according to check
        """
        self._nipper_report_generation.generate_report(file_dir, self._report_location, '--text')
        self.assertTrue(os.path.exists(self._report_location))
        self.assertTrue(*file_contains_all_items(self._report_location, dir_report_data))

    def test_add_device_network(self):
        """
        this test will add a network device with input
         list of device network information in the order given below:
        network_device = [ip_address, device_type, protocol, port, username, password, privilege_password]
        here *network_device is used to pass all the parameters and generates a report using default report settings
        :return: pass or fail according to check
        """
        devices = [["Cisco Nexus", cisco_nexus_device, nexus_report_data],
                   ["Cisco IOS XE", cisco_ios_xe_device, cisco_xe_report_data]]
        for device_name, device, required_report_data in devices:
            with self.subTest(f"{device_name} network test"):
                skip_if_device_not_reachable(self, device_name, device[0])
                temp_device_dir = tempfile.TemporaryDirectory()
                temp_device_file = os.path.join(temp_device_dir.name, "report.html")
                self._nipper_report_generation.generate_remote_device_report(*device, temp_device_file)
                self.assertTrue(os.path.exists(temp_device_file))
                self.assertTrue(*file_contains_all_items(temp_device_file, required_report_data))
                temp_device_dir.cleanup()

    def test_all_report_generation(self):
        """
        this test will take a device config along with baseline-file,compare-file to generate all report types. the
        report file generated is then checked for all report types available
        :return: pass or fail according to check
        """
        self._nipper_report_generation.run_nipper("--all-reports=on")
        self._nipper_report_generation.run_nipper(f"--input={cisco_nexus_config}", f"--baseline={baseline_file_path}",
                                                  f"--compare={compare_file_path}", f"--stig-audit-selection=automatic",
                                                  "--text", f"--report={self._report_location}",
                                                  "--disable-interactive-audit=on")
        self.assertTrue(os.path.exists(self._report_location))
        self.assertTrue(*file_contains_all_items(self._report_location, self.report_order))

    def test_reports_indv(self):
        """
        this test is taking a device config file and generating isolated report-type and checking the report type is in
        the generated report. Filtering_differences_report, Raw_Change_Tracking_report, stig-compliance_report are not
         in the loop as the CLI command is different than the other to generate the report.
        :return: pass or fail according to check
        """
        test_cases = [
            ('--appendix-report', "Appendix"),
            ('--cis-benchmarks', "CIS Benchmark Audit"),
            ('--configuration', "Configuration Report"),
            ('--filtering-complexity', "Filtering Complexity Report"),
            ('--raw-configuration', "Raw Configuration"),
            ('--sans-policy-compliance', "SANS Policy Compliance"),
            ('--security', "Security Audit Summary"),
            ('--vulnaudit', "Vulnerability Audit")
        ]
        for report_type, required_data in test_cases:
            with self.subTest(f'{report_type} report'):
                self._nipper_report_generation.run_nipper("--all-reports=off")
                self._nipper_report_generation.run_nipper(f"{report_type}=on")
                self._nipper_report_generation.run_nipper(f"--input={cisco_nexus_config}", "--text",
                                                          f"--report={self._report_location}")
                self.assertTrue(os.path.exists(self._report_location))
                self.assertTrue(*file_contains_all_items(self._report_location, [required_data]))
                os.remove(self._report_location)
                self._nipper_report_generation.report_default_settings()

        with self.subTest('pci_report'):
            self._nipper_report_generation.run_nipper("--pci=on")
            self._nipper_report_generation.generate_report(cisco_nexus_config, self._report_location)
            self.assertTrue(os.path.exists(self._report_location))
            self.assertTrue(*file_contains_all_items(self._report_location, ['Security Audit Summary', 'Vulnerability Audit',
                                                              'CIS Benchmark Audit', 'Configuration Report']))

        with self.subTest('Filtering_differences_report'):
            self._nipper_report_generation.run_nipper("--filter-differences=on")
            self._nipper_report_generation.filterbaseline_report(cisco_nexus_config, baseline_file_path,
                                                                 self._report_location)
            self.assertTrue(os.path.exists(self._report_location))
            self.assertTrue(*file_contains_all_items(self._report_location, ['Filtering Differences Report']))
            os.remove(self._report_location)
            self._nipper_report_generation.report_default_settings()

        with self.subTest('Raw_Change_Tracking_report'):
            self._nipper_report_generation.run_nipper("--raw-config-changes=on")
            self._nipper_report_generation.raw_change_tracking_report(cisco_nexus_config, compare_file_path,
                                                                      self._report_location)
            self.assertTrue(os.path.exists(self._report_location))
            self.assertTrue(*file_contains_all_items(self._report_location, ['Raw Configuration Changes']))
            os.remove(self._report_location)
            self._nipper_report_generation.report_default_settings()

        with self.subTest('stig-compliance_report'):
            self._nipper_report_generation.run_nipper('--stig-compliance=on')
            self._nipper_report_generation.run_nipper(f"--input={cisco_nexus_config}", "--stig-audit-selection=automatic",
                                                      "--text", f"--report={self._report_location}")
            self.assertTrue(os.path.exists(self._report_location))
            self.assertTrue(*file_contains_all_items(self._report_location, ['DISA STIG Compliance']))
            os.remove(self._report_location)
            self._nipper_report_generation.report_default_settings()

    def test_report_order(self):
        """
        this test takes a device config file with all report-type on default report type order and generates a report
        and checks the order of the report type by getting the index value where the report type placed in the starting
        of the report and checks they are in ascending order.
        :return: pass or fail according to check
        """
        self.test_all_report_generation()
        index_location = []
        with open(self._report_location, 'r+', encoding = 'utf-8') as file_open:
            content = file_open.readlines()
        for report_type in self.report_order:
            for lines in content:
                if report_type in lines:
                    index_location.append(content.index(lines))
                    break

        self.assertTrue(
            index_location[0] < index_location[1] < index_location[2] < index_location[3] < index_location[4]
            < index_location[5] < index_location[6] < index_location[7] < index_location[8]
            < index_location[9] < index_location[10])

    def test_export_import_csv_report(self):
        """
        this test will generate a csv file with adding three network devices Juniper SRX Firewall, Juniper SSG Firewall,
        Cisco Nexus and export it and checks the file exits the place it is exported with the same file path is imported
        and reported generated and report is checked with device names available in the report.
        :return: pass or fail according to check
        """
        devices = [cisco_nexus_device, juniper_ssg_device, juniper_srx_device]
        [skip_if_device_not_reachable(self, "A network device", device[0]) for device in devices]
        csv_export_path = os.path.join(self._temp_dir, "export.csv")
        self._nipper_report_generation.export_network_csv(csv_export_path, devices, self._report_location)
        self.assertTrue(*file_contains_all_items(csv_export_path, ['Juniper SRX Firewall', 'Juniper SSG', 'Cisco Nexus']))
        os.remove(self._report_location)
        self._nipper_report_generation.import_network_csv(csv_export_path, self._report_location)
        self.assertTrue(*file_contains_all_items(self._report_location,
                                                ['Juniper SRX Firewall', 'Juniper SSG Firewall', 'Cisco Nexus']))

    @unittest.skip('cis benchmark profile configuration from command line needs to be developed Nipper-4760')
    def test_cis_and_profiles(self):
        """

        this test generates cis-report and in the report checks the profile used is reflected or not. the sub-test
        covers each profile.
        :return: pass or fail result for different profile checks.
        """
        with open("output-file profile path for level 1", "r+", encoding = 'utf-8') as ios_or_asa_level_1_report:
            self.report_contents_1 = ios_or_asa_level_1_report.readlines()
        with open("output-file profile for level 2", "r+", encoding = 'utf-8') as ios_level_2_report:
            self.report_contents_2 = ios_level_2_report.readlines()
        self.required_ios15_level_1 = ['| Device | Profile | Issues | ', '| Router IOS 15.0 | Level 1 |']
        self.required_ios15_level_2 = ['Device | Profile | Issues |', '| Router IOS 15.0 | Level 2 |']
        self.required_ios12_level_1 = ['| Device | Profile | Issues | ', '| Router IOS 12.0 | Level 1 |']
        self.required_ios12_level_2 = ['Device | Profile | Issues |', '| Router IOS 12.0 | Level 2 |']
        self.required_asa_level_1 = ['| Device | Profile | Issues | ',
                                     '| ciscoasa ASA 8.4(2) | Level 1 - Cisco ASA 8.x |']

        with self.subTest("IOS15 profile level-1"):
            data = self.required_ios15_level_1[0]
            file_contents = "".join(self.report_contents_1)
            self.assertIn(data, file_contents)
            for lines in self.report_contents_1:
                if data in lines:
                    profile_index = (self.report_contents_1.index(lines) + 1)
                    self.assertIn(self.required_ios15_level_1[1], self.report_contents_1[profile_index].strip())
                    break

        with self.subTest("IOS15 profile level-2"):
            data = self.required_ios15_level_2[0]
            file_contents = "".join(self.report_contents_2)
            self.assertIn(data, file_contents)
            for lines in self.report_contents_2:
                if data in lines:
                    j = (self.report_contents_2.index(lines) + 1)
                    self.assertIn(self.required_ios15_level_2[1], self.report_contents_2[j].strip())
                    break

        with self.subTest("IOS12 profile level-1"):
            data = self.required_ios12_level_1[0]
            file_contents = "".join(self.report_contents_1)
            self.assertIn(data, file_contents)
            for lines in self.report_contents_1:
                if data in lines:
                    j = (self.report_contents_1.index(lines) + 1)
                    self.assertIn(self.required_ios12_level_1[1], self.report_contents_1[j].strip())
                    break

        with self.subTest("IOS12 profile level-2"):
            data = self.required_ios12_level_2[0]
            file_contents = "".join(self.report_contents_2)
            self.assertIn(data, file_contents)
            for lines in self.report_contents_2:
                if data in lines:
                    j = (self.report_contents_2.index(lines) + 1)
                    self.assertIn(self.required_ios12_level_2[1], self.report_contents_2[j].strip())
                    break

        with self.subTest("ASA profile level-1"):
            data = self.required_asa_level_1[0]
            file_contents = "".join(self.report_contents_1)
            self.assertIn(data, file_contents)
            for lines in self.report_contents_1:
                if data in lines:
                    j = (self.report_contents_1.index(lines) + 1)
                    self.assertIn(self.required_asa_level_1[1], self.report_contents_1[j].strip())
                    break

        ios_or_asa_level_1_report.close()
        ios_level_2_report.close()


