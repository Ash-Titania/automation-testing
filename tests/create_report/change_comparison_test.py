import os
import unittest
import tempfile

from utility.nipper2_interface import NipperInterface
from utility.nipper2_interface.nipper_report_interface import NipperReportInterface
from utility.temporary_license import TempLicense
from utility.config_paths import get_cisco_a_path, get_cisco_b_path


class TestReportGeneration(unittest.TestCase):

    def setUp(self):
        self.nipper = NipperInterface()
        self.license = TempLicense()
        self.nipper_report = NipperReportInterface(self.nipper)

    def test_change_comparison(self):
        """ Run a security comparison report and assert that certain required lines are present"""
        with tempfile.TemporaryDirectory() as temp_dir:
            output_file = os.path.join(temp_dir, "output.txt")
            self.nipper_report.generate_security_comparison(get_cisco_a_path(), get_cisco_b_path(), output_file)
            self.assertTrue(os.path.exists(output_file))
            with open(output_file, "r", encoding = "utf8") as file:
                content = file.read()
                required_data = {
                    "including a comparison against a previous audit report",
                    "| Found in old report | Found in this report | Issue Title | Device Name | Device Type |",
                    "| Yes | No | Administrative Line Login With No Password | Router | Cisco Router |"
                }
                [self.assertIn(line, content) for line in required_data]
