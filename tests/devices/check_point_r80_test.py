import unittest
import tempfile
import os

from utility.temporary_license import TempLicense
from utility.config_paths import get_check_point_r80_path
from utility.validator.html_report.html_report_validator import validate_html_report
from utility.nipper_wrapper.nipper_report_wrapper import NipperReportWrapper
from utility.nipper_wrapper.nipper_wrapper import NipperWrapper
from utility.nipper_wrapper.nipper_profile_wrapper import NipperProfileWrapper
from resources.moc_devices.check_point_r80.mock_checkpoint_r80_device import start_server
from resources.moc_devices.moc_device_wrapper import MockDeviceWrapper


class TestCheckPointR80(unittest.TestCase):
    def setUp(self):
        self.nipper_wrapper = NipperWrapper()
        self.license = TempLicense()
        self.nipper_report = NipperReportWrapper()
        NipperProfileWrapper().reset_default_settings()

    def test_file_audit(self):
        """
        Test auditing a Check Point R80 device from file
        """

        with tempfile.TemporaryDirectory() as temp_dir:
            report_output = os.path.join(temp_dir, "r80.html")
            self.nipper_report.generate_non_interactive_report(get_check_point_r80_path(), report_output)
            self.assertTrue(validate_html_report(report_output), 'HTML output file not valid')

    def test_remote_audit(self):
        """
        Test remote auditing a moc Check Point R80 device
        """
        
        ip = '127.0.0.1'
        port = 5000
        device_type = '--checkpoint'
        remote_version = 'R80'
        protocol = 'https'
        username = 'user'
        password = 'password'
        
        with MockDeviceWrapper(start_server, ip, port, username, password) as r80_device:
            with tempfile.TemporaryDirectory() as temp_dir:
                report_output = os.path.join(temp_dir, "r80.html")
                self.nipper_report.generate_non_interactive_specific_version_remote_device_report(
                    ip, device_type, remote_version, protocol, port, username, password, '', report_output)

                self.assertTrue(validate_html_report(report_output), 'HTML output file not valid')
        
        
