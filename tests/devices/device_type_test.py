import unittest
import os
import tempfile

from utility.temporary_license import TempLicense
from utility.nipper_wrapper.nipper_wrapper import NipperWrapper
from utility.nipper_wrapper.nipper_report_wrapper import NipperReportWrapper
from utility.nipper_wrapper.nipper_profile_wrapper import NipperProfileWrapper
from resources.create_report.create_report_data import all_device_types
from utility.validator.html_report.html_report_validator import validate_html_report
from concurrent import futures


class TestDeviceTypes(unittest.TestCase):

    def setUp(self):
        self.nipper_wrapper = NipperWrapper()
        self.license = TempLicense()
        self.nipper_report = NipperReportWrapper()
        NipperProfileWrapper().reset_default_settings()

    def generate_and_validate(self, input_file, report_name, device_type):
        with tempfile.TemporaryDirectory() as temp_dir:
            report_path = os.path.join(temp_dir, report_name)
            self.nipper_report.generate_report(input_file, report_path, device_type)
            self.assertTrue(validate_html_report(report_path), 'HTML report not valid')

    def test_device_types(self):
        """
        Audits each device type specified in the regression tests
        """
        config_dir = all_device_types

        input_list = []
        report_list = []
        device_list = []

        for config in os.listdir(config_dir):
            input_file = os.path.join(config_dir, config)
            device = config.split('.')[0]
            report_name = device + '.html'
            device_type = '--' + device

            input_list.append(input_file)
            report_list.append(report_name)
            device_list.append(device_type)

        with futures.ThreadPoolExecutor() as pool:
            results = pool.map(self.generate_and_validate, input_list, report_list, device_list)
