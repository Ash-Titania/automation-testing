import os
import tempfile
import unittest

from resources.create_report.create_report_data import panorama_9_device, panorama_9_managed_devices
from utility.file_check import file_contains_all_items
from utility.network_ping import skip_if_device_not_reachable
from utility.nipper2_interface import NipperInterface, NipperReportInterface
from utility.temporary_license import TempLicense


class TestPanorama9(unittest.TestCase):
    def setUp(self):
        self.license = TempLicense()
        self.nipper_report_generation = NipperReportInterface(NipperInterface())
        self.nipper_report_generation.report_default_settings()
        self.temp_dir_context = tempfile.TemporaryDirectory()
        self.report_location = os.path.join(self.temp_dir_context.name, "report.txt")

    def test_all_managed_devices(self):
        """
        Test that an audited Panorama device also audits all managed firewalls
        :return:
        """
        skip_if_device_not_reachable(self, "Panorama 9", panorama_9_device[0])
        temp_device_dir = tempfile.TemporaryDirectory()
        temp_device_file = os.path.join(temp_device_dir.name, "report.html")
        self.nipper_report_generation.generate_panorama_report(*panorama_9_device, temp_device_file,
                                                               "--all-reports=off", "--configuration=on")
        self.assertTrue(os.path.exists(temp_device_file))
        self.assertTrue(*file_contains_all_items(temp_device_file, panorama_9_managed_devices))
        temp_device_dir.cleanup()

