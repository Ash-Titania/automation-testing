import asyncio
import socket
import time

LOCALHOST_IP = '127.0.0.1'  # Localhost
BUFFER_SIZE = 4096


class TcpServer:

    def __init__(self):
        self._tcp_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def __del__(self):
        self._tcp_sock.close()

    def get_tcp_data(self, port):
        """
        blocking function that will only return once something has connected to the server and sent data
        :param port: port number to listen on localhost
        :return: True when data is received
        """
        self._tcp_sock.bind((LOCALHOST_IP, port))
        self._tcp_sock.listen(1)
        conn, addr = self._tcp_sock.accept()  # Blocking call waiting for connection
        while True:
            data = conn.recv(BUFFER_SIZE)
            if data:
                conn.close()
                return True


class UdpServer:

    def __init__(self):
        self._udp_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def __del__(self):
        self._udp_sock.close()

    def get_udp_data(self, port):
        """
        blocking function that will only return once something has connected to the server and sent data
        :param port: port number to listen on localhost
        :return: True when data is received
        """
        self._udp_sock.bind((LOCALHOST_IP, port))

        while True:
            data, addr = self._udp_sock.recvfrom(BUFFER_SIZE)
            if data:
                self._udp_sock.close()
                return True
