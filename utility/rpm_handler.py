import subprocess
import urllib.request
import ssl
from html.parser import HTMLParser


class MyHTMLParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.href = [""]
        self.found_href = False

    def handle_starttag(self, tag, attrs):
        if tag == 'a':
            self.href = attrs
            self.found_href = True


def sanitise_string(string, characters=None):
    """
    Sanitises a given string of characters
    :param string: The string to be sanitised
    :param characters: The characters that should be removed and replaced with blank
    :return: Returns the sanitised string
    """
    if characters is None:
        characters = [" ", ")", "(", "'"]

    for c in characters:
        string = string.replace(c, "")
    return string


def run_command(command):
    """
    Uses subprocess to run commands
    :param command: The command to be run
    :return: False when exception is raised when trying to run command, otherwise returns the CompletedProcess object
    """
    try:
        complete_process = subprocess.run(command, stdout=subprocess.PIPE, text=True, stderr=subprocess.DEVNULL)
    except subprocess.CalledProcessError:
        return False

    return complete_process


class RPMHandler:

    RPMFile = "NipperRPM.rpm"

    def clean_up(self):
        """"
        Checks for existing Nipper installation and removes
        """
        existing_install = run_command(["rpm", "-qa", "nipperstudio"]).stdout
        existing_install = existing_install.strip()

        if existing_install:
            run_command(["rpm", "-e", existing_install])

    def retrieve_rpm(self):
        """
        Uses bamboo to retrieve the latest successful Nipper RPM artifact
        :return: True when the rpm has been successfully retrieved
        """
        bamboo_address = "https://twukbamboo01.uk.titania.com:8443"
        link_address = bamboo_address + "/browse/NS-NSDCI/latestSuccessful/artifact/shared/nipperstudio-devel-centos7-rpm"

        # Ideally we would like to have a way to disable SSL verification per url request rather than globally like this
        ssl._create_default_https_context = ssl._create_unverified_context
        urllib.request.getproxies = lambda: {}
        html_data = str(urllib.request.urlopen(link_address).read())

        parser = MyHTMLParser()
        parser.feed(html_data)

        if not parser.found_href:
            return False

        # expected url returned from this should
        # look something like '/artifact/NS-NSDCI/shared/build-latestSuccessful/
        # nipperstudio-devel-centos7-rpm/nipperstudio-2.5.18.2019-04-24-centos-7-x86_64.rpm'
        link_split = str(parser.href[0]).split(",")
        build_address = link_split[1]
        build_address = sanitise_string(build_address)

        package_address = bamboo_address + build_address
        urllib.request.urlretrieve(package_address, RPMHandler.RPMFile)

        return True

    def install(self):
        """
        Facade that handles clean up, retrieval and installation
        :return: True when rpm has been successfully installed
        """
        self.clean_up()

        if self.retrieve_rpm():
            yum_install = run_command(["yum", "install", RPMHandler.RPMFile, "-y"])
        else:
            return False

        if yum_install.returncode != 0:
            return False

        return True

