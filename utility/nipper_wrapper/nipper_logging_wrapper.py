from utility.nipper_interface.i_nipper_logging import INipperLogging
from utility.nipper_wrapper.get_nipper_version import get_nipper_version
from interface import implements

class NipperLoggingWrapper(implements(INipperLogging)):
    def __init__(self):
        nipper_version = get_nipper_version()

        if nipper_version == "nipper2":
            from utility.nipper2_interface import NipperLoggingInterface
            from utility.nipper2_interface import NipperInterface
            
            nipper = NipperInterface()
            self.nipper_logging = NipperLoggingInterface(nipper)
        elif nipper_version == "nipper3":
            #Set to nipper3
            pass

    def run_nipper(self, *args):
        return self.nipper_logging.run_nipper(*args)

    def add_logging_tag(self, name, value):
        return self.nipper_logging.add_logging_tag(name, value)

    def set_report_generation_successful_log_level(self, level):
        return self.nipper_logging.set_report_generation_successful_log_level(level)

    def set_report_generation_error_log_level(self, level):
        return self.nipper_logging.set_report_generation_error_log_level(level)

    def enable_event_logging(self, enable):
        return self.nipper_logging.enable_event_logging(enable)

    def disable_generate_log_messages(self):
        self.nipper_logging.disable_generate_log_messages()

    def disable_generate_error(self):
        return self.nipper_logging.disable_generate_error()

    def disable_generate_success(self):
        return self.nipper_logging.disable_generate_success()

    def enable_file_logging(self, enable):
        return self.nipper_logging.enable_file_logging(enable)

    def enable_tcp_logging(self, enable):
        return self.nipper_logging.enable_tcp_logging(enable)

    def enable_udp_logging(self, enable):
        return self.nipper_logging.enable_udp_logging(enable)

    def enable_email_logging(self, enable):
        return self.nipper_logging.enable_email_logging(enable)

    def enable_syslog_logging(self, enable):
        return self.nipper_logging.enable_syslog_logging(enable)

    def enable_all_eventlog_logging_levels(self, enable):
        return self.nipper_logging.enable_all_eventlog_logging_levels(enable)

    def set_file_logging_path(self, path):
        return self.nipper_logging.set_file_logging_path(path)

    def set_file_logging_format(self, log_format):
        return self.nipper_logging.set_file_logging_format(log_format)

    def set_cef_eol_logging_format(self, cef_format):
        return self.nipper_logging.set_cef_eol_logging_format(cef_format)

    def set_cjson_eol_logging_format(self, eol_format):
        return self.nipper_logging.set_cjson_eol_logging_format(eol_format)

    def enable_all_file_logging_levels(self, enable):
        return self.nipper_logging.enable_all_file_logging_levels(enable)

    def set_tcp_logging_ip(self, ip):
        return self.nipper_logging.set_tcp_logging_ip(ip)

    def set_tcp_logging_port(self, port):
        return self.nipper_logging.set_tcp_logging_port(port)

    def set_tcp_logging_format(self, log_format):
        return self.nipper_logging.set_tcp_logging_format(log_format)

    def enable_all_tcp_logging_levels(self, enable):
        return self.nipper_logging.enable_all_tcp_logging_levels(enable)

    def set_udp_logging_ip(self, ip):
        return self.nipper_logging.set_udp_logging_ip(ip)

    def set_udp_logging_port(self, port):
        return self.nipper_logging.set_udp_logging_port(port)

    def set_udp_logging_format(self, log_format):
        return self.nipper_logging.set_udp_logging_format(log_format)

    def enable_all_udp_logging_levels(self, enable):
        return self.nipper_logging.enable_all_udp_logging_levels(enable)

    def set_email_logging_username(self, username):
        return self.nipper_logging.set_email_logging_username(username)

    def set_email_logging_password(self, password):
        return self.nipper_logging.set_email_logging_password(password)

    def set_email_logging_sender(self, sender):
        return self.nipper_logging.set_email_logging_sender(sender)

    def set_email_logging_recipient(self, recipient):
        return self.nipper_logging.set_email_logging_recipient(recipient)

    def set_email_logging_smtp_server(self, smtp_server):
        return self.nipper_logging.set_email_logging_smtp_server(smtp_server)

    def set_email_logging_smtp_port(self, port):
        return self.nipper_logging.set_email_logging_smtp_port(port)

    def set_email_logging_encryption(self, encryption):
        return self.nipper_logging.set_email_logging_encryption(encryption)

    def set_email_logging_attachment(self, attachment):
        return self.nipper_logging.set_email_logging_attachment(attachment)

    def email_logging_send_test(self):
        return self.nipper_logging.email_logging_send_test()

    def set_email_logging_format(self, log_format):
        return self.nipper_logging.set_email_logging_format(log_format)

    def set_email_logging_subject(self, subject):
        return self.nipper_logging.set_email_logging_subject(subject)

    def enable_all_email_logging_levels(self, enable):
        return self.nipper_logging.enable_all_email_logging_levels(enable)

    def set_syslog_logging_format(self, log_format):
        return self.nipper_logging.set_syslog_logging_format(log_format)

    def enable_all_syslog_logging_levels(self, enable):
        return self.nipper_logging.enable_all_syslog_logging_levels(enable)

    def enable_log_file_json_stream(self, enable):
        return self.nipper_logging.enable_log_file_json_stream(enable)
