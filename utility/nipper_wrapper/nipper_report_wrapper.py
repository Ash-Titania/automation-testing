from utility.nipper_interface.i_nipper_report import INipperReport
from utility.nipper_wrapper.get_nipper_version import *
from interface import implements

class NipperReportWrapper(implements(INipperReport)):
    def __init__(self):

        nipper_version = get_nipper_version()

        if nipper_version == "nipper2":
            from utility.nipper2_interface import NipperReportInterface
            from utility.nipper2_interface import NipperInterface
            
            nipper = NipperInterface()
            self.nipper_report = NipperReportInterface(nipper)
        elif nipper_version == "nipper3":
            #Set to nipper3
            pass

    def run_nipper(self, *args):
        return self.nipper_report.run_nipper(*args)

    def generate_report(self, file_input, file_output, *args):
        """
        this method takes network device file and generate a report.
        :param file_input: device config file
        :param file_output: path to generate report file with file name to be saved as.
        :param args: file type the report should be generated
        :return: nipper subprocess
        """
        return self.nipper_report.generate_report(file_input, file_output, *args)

    def generate_non_interactive_report(self, file_input, file_output, *args):
        """
        this method takes a file based configuration and generates a report without interaction.
        :param file_input: device config file
        :param file_output: path to generate report file with file name to be saved as.
        :param args: file type the report should be generated
        :return: nipper subprocess
        """
        return self.nipper_report.generate_non_interactive_report(file_input, file_output, *args)

    def generate_remote_device_report(self, ip_address, device_type, protocol, port, username, password,
                                      privilege_password, file_output, *args):
        """
        this method generate a report for network device with given information and if else condition checks if
        privilege_password given
        :param ip_address: ip address for network device
        :param device_type: device type for network device
        :param protocol: protocol for network device
        :param port: port for network device
        :param username: username for network device
        :param password: password for network device
        :param privilege_password: privilege password for network device
        :param file_output: path to generate a report file with file name to be saved
        :return: nipper subprocess
        """
        return self.nipper_report.generate_remote_device_report(ip_address, device_type, protocol, port, username, password,
                                      privilege_password, file_output, *args)

    def generate_non_interactive_remote_device_report(self, ip_address, device_type, protocol, port, username, password,
                                      privilege_password, file_output):
        """Generate a remote report without any interaction"""
        return self.nipper_report.generate_non_interactive_remote_device_report(ip_address, device_type, protocol, port, username, password,
                                      privilege_password, file_output)

    def generate_non_interactive_specific_version_remote_device_report(self, ip_address, device_type, device_version, protocol, port, username, password,
                                      privilege_password, file_output):
        """Generate a remote report without any interaction"""
        return self.nipper_report.generate_non_interactive_specific_version_remote_device_report(
            ip_address, device_type, device_version, protocol, port, username, password, privilege_password, file_output)

    def enable_single_report_type(self, report_command, state):
        """
        This method can be used to enable or disable a single report type.
        :param report_command: report_command: command for the individual report e.g security, vulnaudit
        :param state: state: can be True or False to enable or disable a single report type
        :return: nipper subprocess
        """
        return self.nipper_report.enable_single_report_type(report_command, state)

    def filterbaseline_report(self, file_input, baseline_file_path, file_output):
        """ in this method a filter difference report is generated with previous filter baseline report in .

        :param file_input: device config file
        :param baseline_file_path: .fbl file generated for the device previously
        :param file_output: path to generate report file with file name to be saved as.
        :return: nipper subprocess
        """
        return self.nipper_report.filterbaseline_report(file_input, baseline_file_path, file_output)

    def raw_change_tracking_report(self, file_input, compare_file_path, file_output):
        """
        takes device input file along with the device previous report in xml format for comparison and generates
         raw_change_tracking report file
        :param file_input:
        :param compare_file_path:
        :param file_output:  path to generate report file with file name to be saved as.
        :return: nipper subprocess
        """
        return self.nipper_report.raw_change_tracking_report(file_input, compare_file_path, file_output)

    def generate_security_comparison(self, file_input1, file_input2, file_output):
        """
        generates a security comparison report on the two input config files
        :param file_input1: config 1
        :param file_input2: config 2
        :param file_output: path to save the final report to
        :return: nipper subprocess
        """
        return self.nipper_report.generate_security_comparison(file_input1, file_input2, file_output)

    def export_network_csv(self, csv_export_path, network_devices, file_output):
        """
        takes one or more network device information using add_network_device and generates a csv report which then be
        exported
        :param csv_export_path: path to store the csv file
        :param network_devices: network device list with relevant information
        :param file_output: path to generate report file with file name to be saved as.
        :return: nipper subprocess
        """
        return self.nipper_report.export_network_csv(csv_export_path, network_devices, file_output)

    def import_network_csv(self, csv_import_path, file_output):
        """
        imports csv file which can have one or more network device information and generates a report
        :param csv_import_path: the path where csv file is available
        :param file_output:  path to generate report file with file name to be saved as.
        :return: nipper subprocess
        """
        return self.nipper_report.import_network_csv(csv_import_path, file_output)

    def report_default_settings(self):
        """changes all report-type settings to default state"""
        self.nipper_report.report_default_settings()

    def all_reports_status_off(self, enable):
        """
        enables or disables all report-types
        :return: nipper subprocess
        """
        return self.nipper_report.all_reports_status_off(enable)

    def run_demo_stig(self, file_output):
        return self.nipper_report.run_demo_stig(file_output)

    
