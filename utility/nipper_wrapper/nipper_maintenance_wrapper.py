from utility.nipper_interface.i_nipper_maintenance import INipperMaintenance
from interface import implements

class NipperMaintenanceWrapper(implements(INipperMaintenance)):
    def __init__(self):
        if nipper_version == "nipper2":
            from utility.nipper2_interface import NipperMaintenanceInterface
            from utility.nipper2_interface import NipperInterface
            
            nipper = NipperInterface()
            self.nipper_maintenance = NipperMaintenanceInterface(nipper)
        elif nipper_version == "nipper3":
            #Set to nipper3
            pass

    def run_nipper(self, *args):
        self.nipper_maintenance.run_nipper(*args)

    def set_update_interval(self, interval):
        self.nipper_maintenance.set_update_interval(interval)
