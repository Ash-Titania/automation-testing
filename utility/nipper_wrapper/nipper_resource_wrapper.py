from utility.nipper_interface.i_nipper_resource import INipperResource
from utility.nipper_wrapper.get_nipper_version import get_nipper_version
from interface import implements

class NipperResourceWrapper(implements(INipperResource)):
    def __init__(self):
        nipper_version = get_nipper_version()

        if nipper_version == "nipper2":
            from utility.nipper2_interface import NipperResourceInterface
            from utility.nipper2_interface import NipperInterface
            
            nipper = NipperInterface()
            self.nipper_resource = NipperResourceInterface(nipper)
        elif nipper_version == "nipper3":
            #Set to nipper3
            pass

    def resource_add(resource_name, resource_category, resource_file):
        return self.nipper_resource(resource_name, resource_category, resource_file)

    def resource_cleanup(resource_name):
        return self.nipper_resource(resource_name)
