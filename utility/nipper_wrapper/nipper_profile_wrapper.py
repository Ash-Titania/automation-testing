from interface import implements
from utility.nipper_interface.i_nipper_profile import INipperProfile
from utility.nipper_wrapper.get_nipper_version import get_nipper_version

class NipperProfileWrapper(implements(INipperProfile)):
    def __init__(self):
        nipper_version = get_nipper_version()

        if nipper_version == "nipper2":
            from utility.nipper2_interface import NipperProfileInterface
            from utility.nipper2_interface import NipperInterface
            
            nipper = NipperInterface()
            self.nipper_profile = NipperProfileInterface(nipper)
        elif nipper_version == "nipper3":
            #Set to nipper3
            pass

    def run_nipper(self, *args):
        return self.nipper_profile.run_nipper(*args)

    def get_profiles(self):
        return self.nipper_profile.get_profiles()

    def reset_default_settings(self):
        self.nipper_profile.reset_default_settings()

    def use_profile(self, name):
        self.nipper_profile.use_profile(name)

    def save_profile(self, name):
        self.nipper_profile.save_profile(name)

    def export_profile(self, name, filename):
        self.nipper_profile.export_profile(name, filename)

    def import_profile(self, name, filename):
        self.nipper_profile.export_profile(name, filename)
    
    def delete_profile(self, name):
        self.nipper_profile.delete_profile(name)
