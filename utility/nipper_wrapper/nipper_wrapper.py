import subprocess

from utility.nipper_wrapper.get_nipper_version import *
from utility.nipper_interface.i_nipper import INipper
from interface import implements

class NipperWrapper(implements(INipper)):
    def __init__(self):
        nipper_version = get_nipper_version()

        if nipper_version == "nipper2":
            from utility.nipper2_interface import NipperInterface
            
            self.nipper = NipperInterface()
        elif nipper_version == "nipper3":
            #Set to nipper3
            pass

    @property
    def nipper_location(self):
        return self.nipper.nipper_location()

    @nipper_location.setter
    def nipper_location(self, nipper_location):
        self.nipper.nipper_location = nipper_location

    def run_nipper(self, *args) -> subprocess.CompletedProcess:
        """
        method to run commands to Nipper Studio
        :param args: variadic arguments to pass to Nipper
        :return: result of executing nipper, such as exit codes and stdout, etc.
        """
        return self.nipper.run_nipper(*args)

    def run_interactive_nipper(self, *args):
        """
        Run nipper cli interactively using pexpect. Interact using `pexpect.expect()` and `pexpect.sendline()`
        :param args: args to run nipper with
        :return: pexpect.spawn subprocess
        """
        return self.nipper.run_interactive_nipper(*args)
