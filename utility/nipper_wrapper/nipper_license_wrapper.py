from utility.nipper_interface.i_nipper_license import INipperLicense
from utility.nipper_wrapper.get_nipper_version import get_nipper_version
from interface import implements

class NipperLicenseWrapper(implements(INipperLicense)):
    def __init__(self):
        nipper_version = get_nipper_version()

        if nipper_version == "nipper2":
            from utility.nipper2_interface import NipperLicenseInterface
            from utility.nipper2_interface import NipperInterface
            
            nipper = NipperInterface()
            self.nipper_license = NipperLicenseInterface(nipper)
        elif nipper_version == "nipper3":
            #Set to nipper3
            pass

    def run_nipper(self, *args):
        return self.nipper_license.run_nipper(*args)

    def add_license(self, serial, activation):
        return self.nipper_license.add_license(serial, activation)

    def remove_license(self, serial):
        return self.nipper_license.remove_license(serial)

    def show_licenses(self):
        return self.nipper_license.show_licenses()

    def switch_license(self, serial):
        return self.nipper_license.switch_license(serial)
