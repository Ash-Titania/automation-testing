import sys


def get_nipper_version():

    for arg in sys.argv:
        if arg == "--nipper2":
            return 'nipper2'

        elif arg == "--nipper3":
            return 'nipper3'

    # Default to return nipper2 - might want this to be nipper3
    return 'nipper2'
