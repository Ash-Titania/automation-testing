import platform
import os
import sys

""" Nipper Studio Demo Files """


CONFIG_LOCATION = os.path.join("resources", "configs")

def get_cisco_demo_path():
    """ :return: Platform independent Cisco ios router demo path """
    if platform.system() == "Linux":
        return "/opt/nipper/demo-files/cisco-router-ios15.txt"
    if platform.system() == "Windows":
        for arg in sys.argv:
            nipper_arg = '--nipper-location='
            if nipper_arg in arg:
                return os.path.join(arg.replace(nipper_arg, ''), os.path.join('demo-files', 'cisco-router-ios15.txt'))
        return "C:/Program Files/NipperStudio/demo-files/cisco-router-ios15.txt"


class FileLocationError(Exception):
    pass


def determine_working_dir(file_path):
    """ should return correct path if running 'runtests.py' as well as running from an individual test file """
    if not os.path.exists(file_path):
        file_path = os.path.join(os.path.join('..', '..'), file_path)
        if not os.path.exists(file_path):
            raise FileLocationError(f"Can't find file: {file_path}")
    return file_path


def get_test_cisco_path():
    """ get path for a test Cisco config """
    return determine_working_dir(os.path.join(CONFIG_LOCATION, "testCisco.txt"))


def get_test_3com_path():
    """ get path for a test 3Com config """
    return determine_working_dir(os.path.join(CONFIG_LOCATION, "test3Com.txt"))


def get_test_brocade_path():
    """ get path for a test Brocade config """
    return determine_working_dir(os.path.join(CONFIG_LOCATION, "testBrocade.txt"))


def get_cisco_a_path():
    """
    Get the path for a test Cisco config.
    :note cisco a and b are the same device with small changes to be suitable for a security comparison report
    """
    return determine_working_dir(os.path.join(CONFIG_LOCATION, "security_comparison_cisco_one.txt"))


def get_cisco_b_path():
    """ get path for a test Cisco config """
    return determine_working_dir(os.path.join(CONFIG_LOCATION, "security_comparison_cisco_two.txt"))

def get_check_point_r80_path():
    """ get path for the check point r80 file config"""
    return determine_working_dir(os.path.join(CONFIG_LOCATION, "check_point_r80.json"))
