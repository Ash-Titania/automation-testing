import xml.etree.ElementTree as ET
import os


class SettingsProfile:

    SettingsTagIndex = 1

    def __init__(self, file_location):
        """
        parses Nipper Studio exported profiles from XML into a dictionary
        :param file_location: path to Nipper Studio exported settings file
        """
        self.file_location = file_location

    @property
    def file_location(self):
        return self._file_location

    @file_location.setter
    def file_location(self, file_location):
        if not file_location:
            raise ValueError("File location must not be empty")
        if not os.path.isfile(file_location):
            raise ValueError("Must be a valid file location")
        self._file_location = file_location

    def parse_profile(self):
        """
        parse XML inside the file at file_location and return a dictionary of Nipper settings
        :return: dictionary of Nipper Studio settings
        """
        xml_tree = ET.parse(self.file_location)
        xml_root = xml_tree.getroot()
        xml_settings = xml_root[self.SettingsTagIndex]
        return {setting.attrib['name']: setting.text for setting in xml_settings}
