import platform


class LinuxDistribution:

    @staticmethod
    def get_distribution():
        """
        Checks if test is being run from linux, then
        attempts to verify the current Linux Distribution
        :return: The name of the linux distribution, or "Non-Linux OS" else
        """
        if platform.system() == "Linux":
            os_release = LinuxDistribution.find_and_parse_os_release()
            distro = os_release.get("NAME")
            if distro is not None:
                distro = distro.lower()
            return distro

        return "Non-Linux OS"

    @staticmethod
    def find_and_parse_os_release():
        """
        Tries to find the os-release file and returns the parsed
        key-value dictionary
        :return: Result from calling parse_os_release with the files lines
        """
        try:
            with open("/etc/os-release") as file:
                return LinuxDistribution.parse_os_release(file.readlines())
        except IOError:
            try:
                with open("/usr/lib/os-release") as file:
                    return LinuxDistribution.parse_os_release(file.readlines())
            except IOError:
                return None

    @staticmethod
    def parse_os_release(os_release_lines):
        """
        Parses the list of lines passed in
        :param: List of lines to be parsed
        :return: Dictionairy of key value pairs found in file
        """
        result = {}

        for line in os_release_lines:
            line = line.strip()

            #skip line if empty or a comment
            if not line or line.startswith("#"):
                continue

            key, value = line.split("=", maxsplit=1)
            value = value.strip('"')
            result[key] = value

        return result
