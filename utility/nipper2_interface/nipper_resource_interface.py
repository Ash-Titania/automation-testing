from utility.nipper2_interface import nipper_interface
from interface import implements
from utility.nipper_interface.i_nipper_resource import INipperResource


class NipperResourceInterface(implements(INipperResource)):
    def resource_add(self, resource_name, resource_category, resource_file):
        return nipper_interface.NipperInterface().run_nipper("--resource-add", "--rname={}".format(resource_name),
                                                             "--rcategory={}".format(resource_category),
                                                             "--rfilename={}".format(resource_file))

    def resource_cleanup(self, resource_name):
        return nipper_interface.NipperInterface().run_nipper("--resource-remove", "--rname={}".format(resource_name))
