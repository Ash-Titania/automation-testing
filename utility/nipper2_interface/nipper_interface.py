# NipperInterface acts as a way to talk to Nipper on the machine
import platform
import subprocess
import os
import sys
import pexpect.popen_spawn

from interface import implements
from utility.nipper_interface.i_nipper import INipper


class NipperInterface(implements(INipper)):

    def __init__(self, nipper_location=""):
        """
        interface to a Nipper Studio executable
        :param nipper_location: directory where a nipper executable can be found
        """
        self.nipper_location = nipper_location

    @property
    def nipper_location(self):
        return self._nipper_location

    @nipper_location.setter
    def nipper_location(self, nipper_location):
        for arg in sys.argv:
            nipper_arg = '--nipper-location='
            if nipper_arg in arg:
                nipper_location = arg.replace(nipper_arg, '')
        if not nipper_location:
            try:
                subprocess.check_call("nipper", stderr=subprocess.PIPE, stdout=subprocess.PIPE)
                self._nipper_location = ""
                return
            except FileNotFoundError:
                raise FileNotFoundError("Nipper cannot be found by the program") from None
            except Exception as e:
                print(f"Exception: {e}")

        self._nipper_location = nipper_location

    def run_nipper(self, *args) -> subprocess.CompletedProcess:
        """
        method to run commands to Nipper Studio
        :param args: variadic arguments to pass to Nipper Studio
        :return: result of executing nipper, such as exit codes and stdout, etc.
        """
        nipper_exe = os.path.join(self.nipper_location, "nipper")
        nipper_executable = nipper_exe if self.nipper_location else "nipper"

        if [arg for arg in args if type(arg) is str and '--serial=' in arg]:
            return subprocess.run([nipper_executable, *args], stdout=subprocess.PIPE, timeout=15, text=True)
        else:
            return subprocess.run([nipper_executable, *args], stdout=subprocess.PIPE, text=True)

    def run_interactive_nipper(self, *args):
        """
        Run nipper cli interactively using pexpect. Interact using `pexpect.expect()` and `pexpect.sendline()`
        :param args: args to run nipper with
        :return: pexpect.spawn subprocess
        """
        nipper_exe = os.path.join(self.nipper_location, "nipper")
        nipper_executable = nipper_exe if self.nipper_location else "nipper"
        command_string = f"{nipper_executable} " + " ".join(args)

        if platform.system() == "Windows":
            return pexpect.popen_spawn.PopenSpawn(command_string)
        else:
            return pexpect.spawn(command_string)
