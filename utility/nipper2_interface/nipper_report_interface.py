import tempfile
import os
import pexpect.popen_spawn

from utility.config_paths import get_cisco_demo_path
from utility.nipper_interface.i_nipper_report import INipperReport
from interface import implements


class NipperReportInterface(implements(INipperReport)):
    def __init__(self, nipper_interface=None):
        self._nipper_interface = nipper_interface

    def run_nipper(self, *args):
        return self._nipper_interface.run_nipper(*args)

    @staticmethod
    def get_commands_for_device(ip_address, device_type, protocol, port, username, password, privilege_password):
        """
        this method generates the complete statement of network device
        :param ip_address: ip address for network device
        :param device_type: device type for network device
        :param protocol: protocol for network device
        :param port: port for network device
        :param username: username for network device
        :param password: password for network device
        :param privilege_password: privilege password for network device
        :return: single line statement usable to generate csv file for network devices
        """
        if privilege_password:
            return [f"--remote-device={ip_address}", device_type, f"--protocol={protocol}", f"--port={port}",
                    f"--username={username}", f"--password={password}", f"--priv-password={privilege_password}"]

        else:
            return [f"--remote-device={ip_address}", device_type, f"--protocol={protocol}", f"--port={port}",
                    f"--username={username}", f"--password={password}"]

    @staticmethod
    def get_commands_for_specific_device_version(ip_address, device_type, device_version, protocol, port, username,
                                                 password, privilege_password, output):
        if privilege_password:
            return (f"--remote-device={ip_address}", f"--protocol={protocol}", f"--port={port}", device_type,
                    f'--remote-version={device_version}', f"--username={username}", f"--password={password}",
                    f"--priv-password={privilege_password}", f'--report={output}')
        else:
            return (f"--remote-device={ip_address}", f"--protocol={protocol}", f"--port={port}", device_type,
                    f'--remote-version={device_version}', f"--username={username}", f"--password={password}",
                    f'--report={output}')

    def generate_report(self, file_input, file_output, *args):
        """
        this method takes network device file and generate a report.
        :param file_input: device config file
        :param file_output: path to generate report file with file name to be saved as.
        :param args: file type the report should be generated
        :return: nipper subprocess
        """
        return self.run_nipper(f"--input={file_input}", f"--report={file_output}", *args)

    def generate_non_interactive_report(self, file_input, file_output, *args):
        """
        this method takes a file based configuration and generates a report without interaction.
        :param file_input: device config file
        :param file_output: path to generate report file with file name to be saved as.
        :param args: file type the report should be generated
        :return: nipper subprocess
        """
        return self.generate_report(file_input, file_output, "--disable-interactive-audit=on", *args)

    def generate_remote_device_report(self, ip_address, device_type, protocol, port, username, password,
                                      privilege_password, file_output, *args):
        """
        this method generate a report for network device with given information and if else condition checks if
        privilege_password given
        :param ip_address: ip address for network device
        :param device_type: device type for network device
        :param protocol: protocol for network device
        :param port: port for network device
        :param username: username for network device
        :param password: password for network device
        :param privilege_password: privilege password for network device
        :param file_output: path to generate a report file with file name to be saved
        :return: nipper subprocess
        """
        if privilege_password:
            process = self._nipper_interface.run_interactive_nipper(f"--remote-device={ip_address}"
                                                                    f" --protocol={protocol} --port={port} {device_type} "
                                                                    f"--username={username} --password={password} "
                                                                    f"--priv-password={privilege_password} "
                                                                    f"--output={file_output}", *args)
        else:
            process = self._nipper_interface.run_interactive_nipper(f"--remote-device={ip_address} {device_type}"
                                                                    f" --protocol={protocol} --port={port}"
                                                                    f" --username={username} --password={password}"
                                                                    f" --output={file_output}", *args)
        return self.continue_report(process)

    @staticmethod
    def continue_report(self, process):
        try:
            process.expect("Do you want to continue?", timeout=30)
            process.sendline("y")
        except pexpect.TIMEOUT:
            pass
        finally:
            process.wait()
            return process

    def generate_non_interactive_remote_device_report(self, ip_address, device_type, protocol, port, username, password,
                                                      privilege_password, file_output):
        return self.generate_remote_device_report(ip_address, device_type, protocol, port, username, password,
                                                  privilege_password, file_output, "--disable-interactive-audit=on")

    def generate_non_interactive_specific_version_remote_device_report(self, ip_address, device_type, device_version,
                                                                       protocol, port, username, password,
                                                                       privilege_password, file_output):
        all_args = self.get_commands_for_specific_device_version(
            ip_address, device_type, device_version, protocol, port, username, password, None, file_output)
        process = self._nipper_interface.run_interactive_nipper(' '.join(all_args), '--disable-interactive-audit=on')

        self.continue_report(process)

        return process

    def enable_single_report_type(self, report_command, state):
        """
        This method can be used to enable or disable a single report type.
        :param report_command: report_command: command for the individual report e.g security, vulnaudit
        :param state: state: can be True or False to enable or disable a single report type
        :return: nipper subprocess
        """
        return self.run_nipper(f"--{report_command}=on" if state else f"--{report_command}=off")

    def filterbaseline_report(self, file_input, baseline_file_path, file_output):
        """ in this method a filter difference report is generated with previous filter baseline report in .

        :param file_input: device config file
        :param baseline_file_path: .fbl file generated for the device previously
        :param file_output: path to generate report file with file name to be saved as.
        :return: nipper subprocess
        """

        return self.run_nipper(f"--input={file_input}", f"--baseline={baseline_file_path}", "--text",
                               f"--report={file_output}")

    def raw_change_tracking_report(self, file_input, compare_file_path, file_output):
        """
        takes device input file along with the device previous report in xml format for comparison and generates
         raw_change_tracking report file
        :param file_input:
        :param compare_file_path:
        :param file_output:  path to generate report file with file name to be saved as.
        :return: nipper subprocess
        """

        return self.run_nipper(f"--input={file_input}", f"--compare={compare_file_path}", "--text",
                               f"--report={file_output}")

    def generate_security_comparison(self, file_input1, file_input2, file_output):
        """
        generates a security comparison report on the two input config files
        :param file_input1: config 1
        :param file_input2: config 2
        :param file_output: path to save the final report to
        :return: nipper subprocess
        """
        with tempfile.TemporaryDirectory() as temp_dir:
            report_out_1 = os.path.join(temp_dir, "report.xml")
            self.run_nipper('--all-reports=off', '--security=on', f'--input={file_input1}',
                            f'--output={report_out_1}', '--xml')
            return self.run_nipper('--all-reports=off', '--security=on', f'--input={file_input2}',
                                   f'--compare={report_out_1}', f'--output={file_output}', '--text')

    def export_network_csv(self, csv_export_path, network_devices, file_output):
        """
        takes one or more network device information using add_network_device and generates a csv report which then be
        exported
        :param csv_export_path: path to store the csv file
        :param network_devices: network device list with relevant information
        :param file_output: path to generate report file with file name to be saved as.
        :return: nipper subprocess
        """
        network_commands = ' '.join([' '.join(commands) for commands in [self.get_commands_for_device(*device)
                                                                         for device in network_devices]])
        process = self._nipper_interface.run_interactive_nipper(
            f"--network-device-csv={csv_export_path} {network_commands} --text --report={file_output}")
        try:
            process.expect("Do you want to continue?", timeout=30)
            process.sendline("y" * len(network_devices))
        except pexpect.TIMEOUT:
            pass
        finally:
            process.wait()
            return process

    def import_network_csv(self, csv_import_path, file_output):
        """
        imports csv file which can have one or more network device information and generates a report
        :param csv_import_path: the path where csv file is available
        :param file_output:  path to generate report file with file name to be saved as.
        :return: nipper subprocess
        """
        return self.run_nipper(f"--input={csv_import_path}", "--text", f"--report={file_output}")

    def report_default_settings(self):
        """changes all report-type settings to default state"""
        self.run_nipper("--use-profile=default")

    def all_reports_status_off(self, enable):
        """
        enables or disables all report-types
        :return: nipper subprocess
        """
        return self.run_nipper(f"--all-reports=off" if enable else f"--all-reports=on")

    def run_demo_stig(self, file_output):
        return self.generate_report(get_cisco_demo_path(), file_output, '--configuration=off',
                                    '--appendix-report=off', '--vulnaudit=off', '--security=off',
                                    '--stig-compliance=on', '--stig-audit-mode=silent',
                                    "--stig-audit-selection=automatic", '--html')

    def generate_panorama_report(self, ip_address, protocol, username, password, file_output, *args):
        return self.run_nipper(f"--remote-device={ip_address}", "--panorama", f"--username={username}",
                               f"--password={password}", f"--protocol={protocol}", f"--output={file_output}", *args)
