from interface import implements
from utility.nipper_interface.i_nipper_profile import INipperProfile

class NipperProfileInterface(implements(INipperProfile)):

    def __init__(self, nipper_interface=None):
        self._nipper_interface = nipper_interface

    def run_nipper(self, *args):
        return self._nipper_interface.run_nipper(*args)

    def get_profiles(self):
        return self.run_nipper("--show-profiles").stdout

    def reset_default_settings(self):
        self.use_profile("Default")

    def use_profile(self, name):
        self.run_nipper("--use-profile={0}".format(name))

    def save_profile(self, name):
        self.run_nipper("--save-profile={0}".format(name))

    def export_profile(self, name, filename):
        self.run_nipper("--export-profile={0}".format(name), "--filename={0}".format(filename))

    def import_profile(self, name, filename):
        self.run_nipper("--import-profile={0}".format(name), "--filename={0}".format(filename))

    def delete_profile(self, name):
        self.run_nipper("--delete-profile={0}".format(name))
