from interface import implements
from utility.nipper_interface.i_nipper_logging import INipperLogging

class NipperLoggingInterface(implements(INipperLogging)):

    log_level_types = ["Report"]
    log_level_categories = ["Audit Fail", "Audit Pass", "Check Fail", "Check Pass"]
    log_level_severities = ["Critical", "High", "Informational", "Low", "Medium"]

    def __init__(self, nipper_interface=None):
        self._nipper_interface = nipper_interface

    def run_nipper(self, *args):
        return self._nipper_interface.run_nipper(*args)

    def add_logging_tag(self, name, value):
        return self.run_nipper("--logging-tags={0}:{1}".format(name, value))

    def set_report_generation_successful_log_level(self, level):
        return self.run_nipper("--log-successful={0}".format(level))

    def set_report_generation_error_log_level(self, level):
        return self.run_nipper("--log-failure={0}".format(level))

    def enable_event_logging(self, enable):
        return self.run_nipper("--log-eventlog-enable={0}".format("on" if enable else "off"))

    def disable_generate_log_messages(self):
        self.disable_generate_error()
        self.disable_generate_success()

    def disable_generate_error(self):
        return self.run_nipper("--log-file-level=Generation/Error=false")

    def disable_generate_success(self):
        return self.run_nipper("--log-file-level=Generation/Successful=false")

    def enable_file_logging(self, enable):
        return self.run_nipper("--log-file-enable={0}".format("on" if enable else "off"))

    def enable_tcp_logging(self, enable):
        return self.run_nipper("--log-tcp-enable={0}".format("on" if enable else "off"))

    def enable_udp_logging(self, enable):
        return self.run_nipper("--log-udp-enable={0}".format("on" if enable else "off"))

    def enable_email_logging(self, enable):
        return self.run_nipper("--log-email-enable={0}".format("on" if enable else "off"))

    def enable_syslog_logging(self, enable):
        return self.run_nipper("--log-syslog-enable={0}".format("on" if enable else "off"))

    def enable_all_eventlog_logging_levels(self, enable):
        for log_type in NipperLoggingInterface.log_level_types:
            for category in NipperLoggingInterface.log_level_categories:
                for severity in NipperLoggingInterface.log_level_severities:
                    return self.run_nipper(
                        "--log-eventlog-level={0}/{1}/{2}={3}"
                        .format(log_type, category, severity, "true" if enable else "false")
                    )

    def set_file_logging_path(self, path):
        self.enable_file_logging(True)
        return self.run_nipper("--log-file-name={0}".format(path))

    def set_file_logging_format(self, log_format):
        return self.run_nipper("--log-file-format={0}".format(log_format))

    def set_cef_eol_logging_format(self, cef_format):
        return self.run_nipper("--log-file-eol={0}".format(cef_format))

    def set_cjson_eol_logging_format(self, eol_format):
        return self.run_nipper("--log-file-eol-cjson={0}".format(eol_format))

    def enable_all_file_logging_levels(self, enable):
        for log_type in NipperLoggingInterface.log_level_types:
            for category in NipperLoggingInterface.log_level_categories:
                for severity in NipperLoggingInterface.log_level_severities:
                    return self.run_nipper(
                        "--log-file-level={0}/{1}/{2}={3}"
                        .format(log_type, category, severity, "true" if enable else "false")
                    )

    def set_tcp_logging_ip(self, ip):
        return self.run_nipper("--log-tcp-address={0}".format(ip))

    def set_tcp_logging_port(self, port):
        return self.run_nipper("--log-tcp-port={0}".format(port))

    def set_tcp_logging_format(self, log_format):
        return self.run_nipper("--log-tcp-format={0}".format(log_format))

    def enable_all_tcp_logging_levels(self, enable):
        for log_type in NipperLoggingInterface.log_level_types:
            for category in NipperLoggingInterface.log_level_categories:
                for severity in NipperLoggingInterface.log_level_severities:
                    return self.run_nipper(
                        "--log-tcp-level={0}/{1}/{2}={3}"
                        .format(log_type, category, severity, "true" if enable else "false")
                    )

    def set_udp_logging_ip(self, ip):
        return self.run_nipper("--log-udp-address={0}".format(ip))

    def set_udp_logging_port(self, port):
        return self.run_nipper("--log-udp-port={0}".format(port))

    def set_udp_logging_format(self, log_format):
        return self.run_nipper("--log-udp-format={0}".format(log_format))

    def enable_all_udp_logging_levels(self, enable):
        for log_type in NipperLoggingInterface.log_level_types:
            for category in NipperLoggingInterface.log_level_categories:
                for severity in NipperLoggingInterface.log_level_severities:
                    return self.run_nipper(
                        "--log-udp-level={0}/{1}/{2}={3}"
                        .format(log_type, category, severity, "true" if enable else "false")
                    )

    def set_email_logging_username(self, username):
        return self.run_nipper("--log-email-serverusername={0}".format(username))

    def set_email_logging_password(self, password):
        return self.run_nipper("--log-email-server-password={0}".format(password))

    def set_email_logging_sender(self, sender):
        return self.run_nipper("--log-email-from={0}".format(sender))

    def set_email_logging_recipient(self, recipient):
        return self.run_nipper("--log-email-to={0}".format(recipient))

    def set_email_logging_smtp_server(self, smtp_server):
        return self.run_nipper("--log-email-server={0}".format(smtp_server))

    def set_email_logging_smtp_port(self, port):
        return self.run_nipper("--log-email-port={0}".format(port))

    def set_email_logging_encryption(self, encryption):
        return self.run_nipper("--log-email-encrypted={0}".format(encryption))

    def set_email_logging_attachment(self, attachment):
        return self.run_nipper("--log-email-attachment={0}".format("on" if attachment else "off"))

    def email_logging_send_test(self):
        # Nipper doesn't have any options for this
        return self.run_nipper("--email-test=")

    def set_email_logging_format(self, log_format):
        return self.run_nipper("--log-email-format={0}".format(log_format))

    def set_email_logging_subject(self, subject):
        return self.run_nipper("--log-email-subject={0}".format(subject))

    def enable_all_email_logging_levels(self, enable):
        for log_type in NipperLoggingInterface.log_level_types:
            for category in NipperLoggingInterface.log_level_categories:
                for severity in NipperLoggingInterface.log_level_severities:
                    return self.run_nipper(
                        "--log-email-level={0}/{1}/{2}={3}"
                        .format(log_type, category, severity, "true" if enable else "false")
                    )

    def set_syslog_logging_format(self, log_format):
        return self.run_nipper("--log-syslog-format={0}".format(log_format))

    def enable_all_syslog_logging_levels(self, enable):
        for log_type in NipperLoggingInterface.log_level_types:
            for category in NipperLoggingInterface.log_level_categories:
                for severity in NipperLoggingInterface.log_level_severities:
                    return self.run_nipper(
                        "--log-syslog-level={0}/{1}/{2}={3}"
                        .format(log_type, category, severity, "true" if enable else "false")
                    )

    def enable_log_file_json_stream(self, enable):
        return self.run_nipper(f"--log-file-json-stream=on" if enable else f"--log-file-json-stream=off")
