import subprocess

from interface import implements
from utility.nipper_interface.i_nipper_license import INipperLicense

from sys import platform
if platform.lower().startswith('win'):
    from third_party.yarw import yarw

class NipperLicenseInterface(implements(INipperLicense)):
    def __init__(self, nipper_interface=None):
        self._nipper_interface = nipper_interface

    def run_nipper(self, *args):
        return self._nipper_interface.run_nipper(*args)

    def add_license(self, serial, activation):
        max_attempts = 3
        attempt = 0
        while attempt < max_attempts:
            try:
                attempt += 1
                process = self.run_nipper("--serial={}".format(serial), "--activation={}".format(activation))
                return process
            except subprocess.TimeoutExpired:
                print(f'Failed to add license: attempt {attempt}/{max_attempts}')

    def remove_license(self, serial):
        return self.run_nipper("--delete-license={}".format(serial))

    def show_licenses(self):
        return self.run_nipper("--show-licenses")

    def switch_license(self, serial):
        return self.run_nipper("--active-license={}".format(serial))


def nuke_registry() -> bool:
    """
    Delete the entire Titania registry key
    :return: bool of the success
    """
    if platform.lower().startswith('win'):
        try:
            yarw.Registry().recursive_delete("HKEY_CURRENT_USER", r"Software\Titania")
            return True
        except yarw.RegistryError:
            return False

    # WHAT SHOULD BE DONE HERE FOR LINUX?
    return False



