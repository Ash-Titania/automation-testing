from utility.nipper_interface.i_nipper_maintenance import INipperMaintenance
from interface import implements

class NipperMaintenanceInterface(implements(INipperMaintenance)):
    def __init__(self, nipper_interface=None):
        self._nipper_interface = nipper_interface

    def run_nipper(self, *args):
        self._nipper_interface.run_nipper(*args)

    def set_update_interval(self, interval):
        self.run_nipper("--check-interval={0}".format(interval))

