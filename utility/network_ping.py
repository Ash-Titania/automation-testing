from ping3 import ping


def skip_if_device_not_reachable(self, device_name, device_address):
    """
    Skips the current test if the device is not reachable with a ping
    :param self: Test to skip
    :param device_name: Name of the device for logging purposes
    :param device_address: Address to attempt to reach
    """
    if not ping(device_address):
        self.skipTest(f"{device_name} was not reachable at {device_address} - NIPPER-6708")

