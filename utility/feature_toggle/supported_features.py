""" Known supported features that can be toggled on or off via the feature toggle system.
    http://twukwiki01.uk.titania.com/index.php/Feature_toggle
 """

supported_features = ["ip-scoping-tags", "json-vuln-audit"]
