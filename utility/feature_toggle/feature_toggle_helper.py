from third_party.yarw import yarw
from utility.feature_toggle.supported_features import *


def enable_feature(feature):
    if feature in supported_features:
        yarw.Registry().set_value("HKEY_CURRENT_USER", R"Software\Titania\Nipper", feature, "")


def disable_feature(feature):
    if feature in supported_features:
        yarw.Registry().delete_value("HKEY_CURRENT_USER", R"Software\Titania\Nipper", feature)
