from interface import Interface

class INipperMaintenance(Interface):
    def run_nipper(self, *args):
        pass

    def set_update_interval(self, interval):
        pass