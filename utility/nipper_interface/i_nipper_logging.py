from interface import Interface

class INipperLogging(Interface):
    def run_nipper(self, *args):
        pass

    def add_logging_tag(self, name, value):
        pass

    def set_report_generation_successful_log_level(self, level):
        pass

    def set_report_generation_error_log_level(self, level):
        pass

    def enable_event_logging(self, enable):
        pass

    def disable_generate_log_messages(self):
        pass

    def disable_generate_error(self):
        pass

    def disable_generate_success(self):
        pass

    def enable_file_logging(self, enable):
        pass

    def enable_tcp_logging(self, enable):
        pass

    def enable_udp_logging(self, enable):
        pass

    def enable_email_logging(self, enable):
        pass

    def enable_syslog_logging(self, enable):
        pass

    def enable_all_eventlog_logging_levels(self, enable):
        pass

    def set_file_logging_path(self, path):
        pass

    def set_file_logging_format(self, log_format):
        pass

    def set_cef_eol_logging_format(self, cef_format):
        pass

    def set_cjson_eol_logging_format(self, eol_format):
        pass

    def enable_all_file_logging_levels(self, enable):
        pass

    def set_tcp_logging_ip(self, ip):
        pass

    def set_tcp_logging_port(self, port):
        pass

    def set_tcp_logging_format(self, log_format):
        pass

    def enable_all_tcp_logging_levels(self, enable):
        pass

    def set_udp_logging_ip(self, ip):
        pass

    def set_udp_logging_port(self, port):
        pass

    def set_udp_logging_format(self, log_format):
        pass

    def enable_all_udp_logging_levels(self, enable):
        pass

    def set_email_logging_username(self, username):
        pass

    def set_email_logging_password(self, password):
        pass

    def set_email_logging_sender(self, sender):
        pass

    def set_email_logging_recipient(self, recipient):
        pass

    def set_email_logging_smtp_server(self, smtp_server):
        pass

    def set_email_logging_smtp_port(self, port):
        pass

    def set_email_logging_encryption(self, encryption):
        pass

    def set_email_logging_attachment(self, attachment):
        pass

    def email_logging_send_test(self):
        pass

    def set_email_logging_format(self, log_format):
        pass

    def set_email_logging_subject(self, subject):
        pass

    def enable_all_email_logging_levels(self, enable):
        pass

    def set_syslog_logging_format(self, log_format):
        pass

    def enable_all_syslog_logging_levels(self, enable):
        pass

    def enable_log_file_json_stream(self, enable):
        pass