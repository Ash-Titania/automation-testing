import subprocess
from interface import Interface

class INipper(Interface):

    @property
    def nipper_location(self):
        pass

    @nipper_location.setter
    def nipper_location(self, nipper_location):
        pass

    def run_nipper(self, *args) -> subprocess.CompletedProcess:
        """
        method to run commands to Nipper Studio
        :param args: variadic arguments to pass to Nipper
        :return: result of executing nipper, such as exit codes and stdout, etc.
        """
        pass

    def run_interactive_nipper(self, *args):
        """
        Run nipper cli interactively using pexpect. Interact using `pexpect.expect()` and `pexpect.sendline()`
        :param args: args to run nipper with
        :return: pexpect.spawn subprocess
        """
        pass