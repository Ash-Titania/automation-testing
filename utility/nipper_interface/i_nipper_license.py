from interface import Interface

class INipperLicense(Interface):
    def run_nipper(self, *args):
        pass

    def add_license(self, serial, activation):
        pass

    def remove_license(self, serial):
        pass

    def show_licenses(self):
        pass

    def switch_license(self, serial):
        pass