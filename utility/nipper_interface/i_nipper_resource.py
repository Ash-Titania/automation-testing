from interface import Interface

class INipperResource(Interface):
    def resource_add(self, resource_name, resource_category, resource_file):
        pass

    def resource_cleanup(self, resource_name):
        pass
