from interface import Interface

class INipperProfile(Interface):
    def run_nipper(self, *args):
        pass

    def get_profiles(self):
        pass

    def reset_default_settings(self):
        pass

    def use_profile(self, name):
        pass

    def save_profile(self, name):
        pass

    def export_profile(self, name, filename):
        pass

    def import_profile(self, name, filename):
        pass
    
    def delete_profile(self, name):
        pass