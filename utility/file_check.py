def file_contains_all_items(file_path, required_items):
    """
    Determine if all items in list_of_statements appears in file_input
    :param file_path: Path to file to inspect
    :param required_items: List of string items to check through
    :return: If all items appear in the file, and an error message it not
    """
    with open(file_path, mode='r', encoding='utf-8') as file:
        file_contents = file.read()
        missing_data = [data for data in required_items if data not in file_contents]
        return not missing_data, f"Missing data: {', '.join(missing_data)}"

