import os

from bs4 import BeautifulSoup

def validate_html_report(pathToFile):
    """
    Helper function for doing basic HTML output file validation
    :return: boolean for whether the file has passed validation
    """
    # Check the file exists
    if not os.path.isfile(pathToFile):
        return False

    # Check the file is populated
    if os.path.getsize(pathToFile) <= 0:
        return False

    with open(pathToFile) as output:
        # Check the file contains valid HTML
        if not BeautifulSoup(output.read(), 'html.parser').find():
            return False

    return True