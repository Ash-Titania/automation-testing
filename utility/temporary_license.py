import re

from utility.nipper2_interface.nipper_license_interface import nuke_registry
from utility.nipper_wrapper.nipper_license_wrapper import NipperLicenseWrapper
from resources.licenses.license_keys import ENTERPRISE_LICENSE_ACTIVATION, ENTERPRISE_LICENSE_SERIAL


class TempLicense:
    """ RAII style class for adding a temporary license """
    def __init__(self, clear=True):
        self.clear = clear
        self._license_interface = NipperLicenseWrapper()
        if clear:
            nuke_registry()
        output = self._license_interface.add_license(ENTERPRISE_LICENSE_SERIAL, ENTERPRISE_LICENSE_ACTIVATION)
        if 'Successful' not in output.stdout:
            print(f'Error while adding default license!: {output.stdout}')
        license_usage = re.compile(r'Usage\s*:\s*(\d+) out of (\d+)').search(self._license_interface.show_licenses()
                                                                             .stdout)
        if license_usage:
            if int(license_usage.group(1)) / int(license_usage.group(2)) > 0.75:
                print('Default license is nearly full please clear usages on the staging license website!!!')

    def __del__(self):
        if self.clear:
            nuke_registry()

